module.exports = {
  roots: ['<rootDir>/src/'],
  transform: { '^.+\\.jsx?$': 'babel-jest' },
  transformIgnorePatterns: ['[/\\\\]node_modules[/\\\\].+\\.(js|jsx|mjs)$'],
  testRegex: '(/__tests__/.*|(\\.|/)test)\\.js?$',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  collectCoverage: true,
  clearMocks: true,
  verbose: true,
  setupFilesAfterEnv: ['<rootDir>/setupEnzyme.js'],
  coverageDirectory: 'coverage',
  snapshotSerializers: ['enzyme-to-json/serializer'],
  moduleDirectories: [
    'node_modules',
  ],
  moduleNameMapper: {
    '^.+\\.(css|less|scss|png|svg)$': 'babel-jest',
  },
};
