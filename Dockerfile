FROM node:14-alpine

RUN apk add --no-cache \
  autoconf \
  automake \
  bash \
  gcc \
  g++ \
  libtool \
  libc6-compat \
  libjpeg-turbo-dev \
  libpng-dev \
  make \
  nasm \
  python

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Installing dependencies
COPY package.json yarn.lock ./
RUN yarn install

COPY ./src ./src
COPY ./.babelrc ./.babelrc

RUN yarn babel src/backend/client --out-dir build/backend/client --source-maps

RUN yarn babel src/backend/admin --out-dir build/backend/admin --source-maps

RUN yarn babel src/backend/common --out-dir build/backend/common

COPY ./src/backend/admin/resources build/backend/admin/resources

# Copying source files
COPY ./static ./static
COPY ./jest.config.js ./jest.config.js
COPY ./next.config.js ./next.config.js
COPY ./webpack.config.js ./webpack.config.js

# Building app
RUN yarn build

# Building admin frontend
RUN yarn client:build

# Building apidoc
RUN yarn apidoc