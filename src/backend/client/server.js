import path from 'path';

import express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import connectRedis from 'connect-redis';
import responseTime from 'response-time';
import csrf from 'csurf';
import Joi from '@hapi/joi';
import httpContext from 'express-http-context';

import logger from '../common/services/LoggerService';
import routes from './routes';
import integration from './routes/integration';
import RedisService from '../common/services/RedisService';
import {
  memoResponseBody, logRequest, isIntegrator, errorHandler,
} from '../common/utils/middlewares';
import asyncErrorHandler from '../common/libs/asyncErrorHandler';
import validator from '../common/libs/requestValidator';
import FileService from '../common/services/FileService';
import passportInstance from './passportInstance';
import { checkRequiredId } from '../common/utils/validation';
import { COOKIE_HTTP_ONLY, COOKIE_SECURE } from '../common/constants/env';
import is from '../common/libs/is';

process.on('unhandledRejection', (err) => {
  logger.error(err);
});

FileService.initMainFileFolders();

const server = express();

server.disable('x-powered-by');
if (!is.env.development()) server.use(compression());

server.use('/apidoc', express.static(path.resolve(__dirname, '../../static/apidoc')));
server.use(cookieParser());
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json({ limit: '10mb' }));

server.use(session({
  store: new (connectRedis(session))({ client: new RedisService().client }),
  name: 'marketplace-app.sid',
  cookie: {
    maxAge: process.env.SESSION_EXPIRE || 30 * 60 * 1000,
    httpOnly: COOKIE_HTTP_ONLY,
    secure: COOKIE_SECURE,
  },
  resave: false,
  unset: 'destroy',
  proxy: true,
  rolling: true,
  saveUninitialized: false,
  secret: process.env.SESSION_SECRET || 'sessionsecret123',
}));

server.use((req, res, next) => {
  const oldJson = res.json;
  res.defaultJson = oldJson;

  res.json = (data = null, success = true, error = null, errorCode = null) => {
    const response = {
      data,
      success,
      error,
      errorCode,
    };

    oldJson.call(res, response);
  };

  return next();
});

server.use(passportInstance.initialize());
server.use(passportInstance.session());

server.use(responseTime({ suffix: false }));

server.use(memoResponseBody);
server.use(asyncErrorHandler(logRequest));

server.use(httpContext.middleware);

/* API integration */
const keySchemaIntegrator = Joi.object({
  apikey: checkRequiredId,
});

server.use(
  '/api/integration',
  validator.headers(keySchemaIntegrator),
  asyncErrorHandler(isIntegrator),
  integration,
);

/* add csrf token */
if (!is.env.development()) {
  const csrfApp = csrf({
    cookie: {
      key: '_csrf-app',
      httpOnly: true,
      secure: is.env.production(),
      sameSite: 'strict',
    },
  });
  server.use((req, res, next) => {
    if (['/api/autotest/delete-client', '/api/autotest/get-autorization-code'].includes(req.path)) return next();
    csrfApp(req, res, next);
  });
}

/* API routes */
server.use('/api', routes);

/* handle CSRF token errors */
server.use((err, req, res, next) => {
  if (err.code !== 'EBADCSRFTOKEN') return next(err);

  return res.status(400).json({
    data: null,
    success: false,
    error: 'Ошибка валидации CSRF-токена',
    errorCode: 'CSRFTokenValidationFailure',
  });
});

/* routes error handler */
server.use(errorHandler);

export default server;
