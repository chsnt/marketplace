import httpStatus from 'http-status';

import COMMON_ERRORS from '../../common/constants/errors';

const ERRORS = {
  ...COMMON_ERRORS,
  SBBID: {
    NONCE_IS_NOT_VALID: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Повторное обращение к SBBID',
      errorCode: 'NONCE_IS_NOT_VALID',
    },
  },
  ORDER: {
    ...COMMON_ERRORS.ORDER,
    PAYMENT_NOT_FOUND: {
      statusCode: httpStatus.NOT_FOUND,
      error: 'Нет информации по оплате заказа',
      errorCode: 'PAYMENT_NOT_FOUND',
    },
    ELEMENTS_ARE_DUPLICATED: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Тарифы или услуги дублируются в заказе для одного оборудования',
      errorCode: 'ELEMENTS_ARE_DUPLICATED',
    },
    CART_AND_ORDER_ARE_MISMATCH: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Тарифы или услуги в корзине и в заказе не совпадают',
      errorCode: 'CART_AND_ORDER_ARE_MISMATCH',
    },
    REGISTER_PAYMENT: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Не удалось зарегистрировать платеж',
      errorCode: 'REGISTER_PAYMENT',
    },
    ORDER_OFFER_NOT_FOUND: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Не найден документ договора-оферты. Обратитесь к администраторам системы',
      errorCode: 'ORDER_OFFER_NOT_FOUND',
    },
    CART_DOES_NOT_EXISTS: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Корзина не существует. Добавьте товары в корзину и попробуйте снова',
      errorCode: 'CART_DOES_NOT_EXISTS',
    },

    INDIVIDUAL_NOT_AVAILABLE_PAYMENT(name) {
      return {
        statusCode: httpStatus.BAD_REQUEST,
        error: `${name} доступна только для юридических лиц`,
        errorCode: 'INDIVIDUAL_NOT_AVAILABLE_PAYMENT',
      };
    },
    PAYMENT_ONLY_BY_INVOICE(name) {
      return {
        statusCode: httpStatus.BAD_REQUEST,
        error: `${name} можно оплатить только по счету`,
        errorCode: 'PAYMENT_ONLY_BY_INVOICE',
      };
    },
    CONFLICT_AVAILABLE_PAYMENT: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'В заказе не может быть нескольких услуг с разными видами оплаты',
      errorCode: 'CONFLICT_AVAILABLE_PAYMENT',
    },
  },
  AUTOTEST: {
    DELETE_CLIENT: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Невозможно удалить клиента',
      errorCode: 'DELETE_CLIENT',
    },
  },
};

export default ERRORS;
