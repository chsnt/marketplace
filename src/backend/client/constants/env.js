export const GET_ORGS_URL = process.env.GET_ORGS_URL || 'http://45.12.73.226:8080/getOrgs';
export const GET_ORGS_USERNAME = process.env.GET_ORGS_USERNAME || 'sbercrm';
export const GET_ORGS_PASSWORD = process.env.GET_ORGS_PASSWORD || 'sbercrm';

export const MAX_ATTEMPTS = {
  LOGIN: process.env.MAX_LOGIN_ATTEMPTS || 2,
  GET_CODE: process.env.MAX_GET_CODE_ATTEMPTS || 1,
  CHECK_CODE: process.env.MAX_CHECK_CODE_ATTEMPTS || 3,
  REGISTER: process.env.MAX_REGISTER_ATTEMPTS || 3,
  ORGANIZATION_INFO: process.env.MAX_ORGANIZATION_INFO_ATTEMPTS || 3,
};
