import { Router } from 'express';

import OrderController from '../controllers/OrderController';
import asyncErrorHandler from '../../common/libs/asyncErrorHandler';

const router = new Router();

/* Эндпоинты используются для обработки ответа от эквайринга */
router.get(
  '/complete',
  asyncErrorHandler(OrderController.completeOrder),
);

router.get(
  '/reset-to-cart',
  asyncErrorHandler(OrderController.resetOrderToCart),
);

export default router;
