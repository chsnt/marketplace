import { Router } from 'express';

import DeviceController from '../controllers/DeviceController';
import asyncErrorHandler from '../../common/libs/asyncErrorHandler';

const router = new Router();

/**
 * @api {get} /api/device/get-device-types Получить список типов оборудования
 * @apiVersion 1.0.0
 * @apiName GetDeviceTypes
 * @apiGroup Device
 * @apiSuccessExample {json} Пример ответа:
 *  HTTP/1.1 200 OK
 *  {
 *    data: [
 *      {
 *        id: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *        name: 'ККТ',
 *        alias: 'ККТ',
 *      },
 *      {
 *        id: '14eeadde-dbcb-44ac-9d72-e1cc11617c98',
 *        name: 'Поддержка АРМ',
 *        alias: 'ARM',
 *      }
 *    ],
 *    success: true,
 *    error: 'error message' | null,
 *    errorCode: 'ErrorCode' | null
 *  }
 */
router.get(
  '/get-device-types',
  asyncErrorHandler(DeviceController.getDeviceTypes),
);

export default router;
