import { Router } from 'express';

import AuthController from '../../controllers/AuthController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';
import validator from '../../../common/libs/requestValidator';
import rateLimiter from '../../../common/libs/rateLimiter';
import register from './register';

import { MAX_ATTEMPTS } from '../../constants/env';

const router = new Router();

router.use('/register', register);

/**
 * @api {post} /api/auth/login Аутентификация пользователя
 * @apiVersion 1.0.0
 * @apiName Login
 * @apiGroup Auth
 * @apiParam (Body) {Object} request
 * @apiParam (Body) {String} request.emailOrPhone Email или телефон пользователя
 * @apiParam (Body) {String} request.password Пароль
 * @apiParam (Body) {Boolean} request.shouldSave Запомнить пароль на неделю в противном случае на день
 * @apiParamExample {json} Body
 *    {
 *      emailOrPhone: 'foo@yandex.ru',
 *      password: 'myaw3somep@ssworD',
 *    }
 * @apiSuccess (Success 200) {Object} request
 * @apiSuccess (Success 200) {UUID} request.id Идентификатор созданного пользователя
 * @apiSuccess (Success 200) {String} request.email Email
 * @apiSuccess (Success 200) {String} request.phone Телефон
 * @apiSuccess (Success 200) {String} request.isLegal Флаг, обозначающий, что пользователь - юр. лицо
 * @apiSuccess (Success 200) {String} request.isTemporaryPassword Пользователь зашел под временным паролем
 * @apiSuccessExample {json} Пример ответа:
 *  HTTP/1.1 200 OK
 *    {
 *      data: {
 *        id: '7b5fcf18-88ee-4820-bc4b-8a1e36d7294a',
 *        email: 'foo@yandex.ru',
 *        phone: '79250347824',
 *        isLegal: true,
 *        isEmailConfirmed: true,
 *        isTemporaryPassword: true
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null
 *    }
 */
router.post('/login',
  rateLimiter({ max: MAX_ATTEMPTS.LOGIN, skipSuccessfulRequests: true }),
  asyncErrorHandler(AuthController.localLogin),
  (req, res) => res.json({
    ...req.user?.profile,
    isTemporaryPassword: req.user?.isTemporaryPassword,
  }));

/**
 * @api {get} /api/auth/logout Выйти из системы
 * @apiVersion 1.0.0
 * @apiName Logout
 * @apiGroup Auth
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: null
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null
 *    }
 */
router.get('/logout', (req, res) => {
  req.logout();
  req.session = null;
  res.json();
});

/**
 * @api {post} /api/auth/get-code Получить код подтверждения телефона
 * @apiVersion 1.0.0
 * @apiName GetCode
 * @apiGroup Auth
 * @apiParam (Body) {Object} request
 * @apiParam (Body) {String} request.phone Номер телефона
 * @apiParamExample {json} Body
 *    {
 *      'phone': '89231341212'
 *    }
 * @apiSuccessExample Пример ответа:
 *  HTTP/1.1 200 OK
 *    {
 *      data: null
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null
 *    }
 */
router.post(
  '/get-code',
  rateLimiter({ max: MAX_ATTEMPTS.GET_CODE }),
  validator.body(AuthController.validators.getCode),
  asyncErrorHandler(AuthController.getCode),
);

/**
 * @api {post} /api/auth/check-code Проверить код подтверждения телефона и получить токен регистрации
 * @apiVersion 1.0.0
 * @apiName CheckCode
 * @apiGroup Auth
 * @apiParam (Body) {Object} request
 * @apiParam (Body) {String} request.phone Номер телефона
 * @apiParam (Body) {String} request.code Код подтверждения
 * @apiParamExample {json} Body
 *    {
 *      phone: '89231341212',
 *      code: '3434'
 *    }
 * @apiSuccessExample {json} Пример ответа:
 *  HTTP/1.1 200 OK
 *    {
 *      data: {
 *        'token': '33b52f4d-32d0-41e1-b52d-17e9b2b32174'
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null
 *    }
 */
router.post(
  '/check-code',
  rateLimiter({ max: MAX_ATTEMPTS.CHECK_CODE, skipSuccessfulRequests: true }),
  validator.body(AuthController.validators.checkCode),
  asyncErrorHandler(AuthController.checkCode),
);

/**
 * @api {post} /api/auth/reset-password Сбросить пароль
 * @apiVersion 1.0.0
 * @apiName ResetPassword
 * @apiGroup Auth
 * @apiParam (Body) {Object} request
 * @apiParam (Body) {String} request.email почта
 * @apiDescription Временный пароль не обнуляет прежний пароль.
 *                По умолчанию временный пароль 'живет' одни сутки.
 *                Прежний пароль действителен до его смены.
 *                Временный пароль разовый и сбрасывается после первой аутентификации.
 *                Если был запрошен временный пароль, но пользователь прошел аутентификация под прежним временный сбрасывается.
 *                Для замены пароля используется эндпоинт `/api/user/me/settings` в котором доступны только `user.password` и `user.confirmPassword`.
 * @apiParamExample {json} Body
 *    {
 *      email: 'foуz@yandex.ru'
 *    }
 * @apiSuccessExample Пример ответа:
 *  HTTP/1.1 200 OK
 *    {
 *      data: null
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null
 *    }
 */
router.post(
  '/reset-password',
  validator.body(AuthController.validators.prepareResetPassword),
  asyncErrorHandler(AuthController.prepareResetPassword),
);

/**
 * @api {post} /api/auth/is-authenticated Проверка авторизации
 * @apiVersion 1.0.0
 * @apiName IsAuthenticated
 * @apiGroup Auth
 * @apiDescription Возвращает true, если пользователь авторизован, и false, если нет.
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      data: {
 *        id: '7b5fcf18-88ee-4820-bc4b-8a1e36d7294a',
 *        email: 'foo@yandex.ru',
 *        phone: '79250347824',
 *        isLegal: true,
 *        isEmailConfirmed: true,
 *        isTemporaryPassword: true
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null
 *    }
 */
router.get(
  '/is-authenticated',
  (req, res) => res.json(req.isAuthenticated && req.isAuthenticated() && {
    ...req.user?.profile,
    isTemporaryPassword: req.user?.isTemporaryPassword,
  }),
);


/**
 * @api {get} /api/auth/sbbid Аутентификация через СберБизнес ID
 * @apiVersion 1.0.0
 * @apiName sbbid
 * @apiGroup Auth
 * @apiDescription Делает редирект на СберБизнес ID.
 */
router.get('/sbbid',
  asyncErrorHandler(AuthController.provider));

/**
 * @api {get} /api/auth/callback-sbbid Аутентификация через СберБизнес ID
 * @apiVersion 1.0.0
 * @apiName CallbackSbbid
 * @apiGroup Auth
 * @apiParam (Query) {String} code Код полученный от SBBID
 * @apiParam (Query) {String} state Токен стейта
 * @apiParam (Query) {String} nonce Токен SBBID для предотвращения атака повторного воспроизведения
 * @apiDescription После обратного редиректа нужно вызвать этот метод.
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      data: {
 *        id: '3823a358-fb0d-4b3d-8221-f3a11b1d414c',
 *        email: 'Partner_Test50601@test.ru',
 *        phone: '79057305061',
 *        isLegal: false,
 *        newUser: true,
 *        organization: {
 *          inn: '5033990017',
 *          kpp: '770701001',
 *          name: 'Общество с ограниченной ответственностью ПАРТНЕР-506'',
 *          orgLawFormShort: 'ИП',
 *          orgOkpo: '777777777',
 *          realAddress: 'РОССИЙСКАЯ ФЕДЕРАЦИЯ, 121351, г.Москва, ул. Коцюбинского, дом 4, стр. 3, оф. 12Ц',
 *        },
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null
 *    }
 */
router.get('/callback-sbbid',
  validator.query(AuthController.validators.providerCallback),
  asyncErrorHandler(AuthController.providerCallback));

export default router;
