import { Router } from 'express';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';
import validator from '../../../common/libs/requestValidator';
import rateLimiter from '../../../common/libs/rateLimiter';
import { MAX_ATTEMPTS } from '../../constants/env';
import AuthController from '../../controllers/AuthController';

const router = new Router();

/**
 * @api {post} /api/auth/register/contact-data Шаг 'Контактные данные' в процессе регистрации
 * @apiVersion 1.0.0
 * @apiName RegisterContactData
 * @apiGroup Auth
 * @apiParam (Body) {String} [firstName] Имя клиента
 * @apiParam (Body) {String} [patronymicName] Отчество клиента
 * @apiParam (Body) {String} [lastName] Фамилия клиента
 * @apiParam (Body) {String} [phone] Телефон клиента. Обязателен, если не указан email
 * @apiParam (Body) {String} [email] Email клиента. Обязателен, если не указан phone
 * @apiSuccessExample Пример ответа:
 *  HTTP/1.1 200 OK
 *    {
 *      data: '49dcfaa4-b961-4b86-91cc-062974d35b56',
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null
 *    }
 */
router.post(
  '/contact-data',
  rateLimiter({ max: MAX_ATTEMPTS.REGISTER }),
  validator.body(AuthController.validators.registerContactData),
  asyncErrorHandler(AuthController.registerContactData),
);

/**
 * @api {post} /api/auth/register/organization Шаг 'Юридическое лицо / ИП' в процессе регистрации
 * @apiVersion 1.0.0
 * @apiName RegisterOrganization
 * @apiGroup Auth
 * @apiParam (Body) {UUID} leadId Идентификатор лида с шага "Контактные данные"
 * @apiParam (Body) {Object} user Представитель клиента
 * @apiParam (Body) {String} user.firstName Имя представителя
 * @apiParam (Body) {String} user.lastName Фамилия представителя
 * @apiParam (Body) {String} [user.patronymicName] Отчество представителя
 * @apiParam (Body) {String} [user.email] Email
 * @apiParam (Body) {String} [user.phone] Телефон
 * @apiParam (Body) {String} [user.birthday] День рождения в формате 'DD.MM.YYYY'
 *
 * @apiParam (Body) {Object} organization Данные организации
 * @apiParam (Body) {String{2..}} organization.name Наименование
 * @apiParam (Body) {String{10..12}} organization.inn ИНН
 * @apiParam (Body) {String{9}} organization.kpp КПП
 * @apiParam (Body) {UUID} organization.electronicDocumentCirculationId Идентификатор оператора ЭДО
 * @apiParam (Body) {String{..1024}} [organization.electronicDocumentCirculationDescription] Если выбрано "Моего оператора нет в списке", нужно указать его тут
 *
 * @apiParam (Body) {Boolean} organization.isSameAddress Флаг одинаковых юридического и фактического адресов
 *
 * @apiParam (Body) {Object} organization.legalAddress Юридический адрес
 * @apiParam (Body) {UUID} organization.legalAddress.region Субъект РФ
 * @apiParam (Body) {String} [organization.legalAddress.city] Город строкой (должно быть только что-то одно)
 * @apiParam (Body) {UUID} [organization.legalAddress.cityId] Город из списка (должно быть только что-то одно)
 * @apiParam (Body) {String} organization.legalAddress.street Улица
 * @apiParam (Body) {String} organization.legalAddress.house Дом
 * @apiParam (Body) {String} organization.legalAddress.apartment Квартира/офис
 * @apiParam (Body) {String} [organization.legalAddress.postalCode] Почтовый индекс
 *
 * @apiParam (Body) {Object} [organization.realAddress] Фактический адрес
 * @apiParam (Body) {UUID} [organization.realAddress.region] Субъект РФ
 * @apiParam (Body) {String} [organization.realAddress.city] Город строкой (должно быть только что-то одно)
 * @apiParam (Body) {UUID} [organization.realAddress.cityId] Город из списка (должно быть только что-то одно)
 * @apiParam (Body) {String} organization.realAddress.street Улица
 * @apiParam (Body) {String} organization.realAddress.house Дом
 * @apiParam (Body) {String} [organization.realAddress.apartment] Квартира/офис
 * @apiParam (Body) {String} [organization.realAddress.postalCode] Почтовый индекс

 * @apiParam (Body) {Object} organization.bankDetail Банковские реквизиты
 * @apiParam (Body) {String{2..100}} organization.bankDetail.name Название банка
 * @apiParam (Body) {String{20}} organization.bankDetail.bankAccount Расчетный счет
 * @apiParam (Body) {String{20}} organization.bankDetail.correspondentAccount Кор. счет
 * @apiParam (Body) {String{9}} organization.bankDetail.rcbic БИК
 * @apiParam (Body) {UUID} organization.bankDetail.taxTypeId Идентификатор системы налогообложения
 *
 * @apiParam (Body) {String} registrationToken Токен регистрации, полученный после подтверждения кода из смс
 * @apiSuccessExample Пример ответа:
 *  HTTP/1.1 200 OK
 *    {
 *      data: null
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null
 *    }
 */
router.post(
  '/organization',
  rateLimiter({ max: MAX_ATTEMPTS.REGISTER }),
  validator.body(AuthController.validators.registerOrganization),
  asyncErrorHandler(AuthController.registerClient),
);

/**
 * @api {post} /api/auth/register/individual Шаг 'Физическое лицо' в процессе регистрации
 * @apiVersion 1.0.0
 * @apiName RegisterIndividual
 * @apiGroup Auth
 * @apiParam (Body) {UUID} leadId Идентификатор лида с шага "Контактные данные"
 * @apiParam (Body) {Object} user Клиент
 * @apiParam (Body) {String} user.firstName Имя клиента
 * @apiParam (Body) {String} [user.patronymicName] Отчество клиента
 * @apiParam (Body) {String} user.lastName Фамилия клиента
 * @apiParam (Body) {String} user.phone Телефон клиента
 * @apiParam (Body) {String} user.email Email клиента
 * @apiParam (Body) {String} registrationToken Токен регистрации, полученный после подтверждения кода из смс
 * @apiSuccessExample Пример ответа:
 *  HTTP/1.1 200 OK
 *    {
 *      data: null
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null
 *    }
 */
router.post(
  '/individual',
  rateLimiter({ max: MAX_ATTEMPTS.REGISTER }),
  validator.body(AuthController.validators.registerIndividual),
  asyncErrorHandler(AuthController.registerClient),
);

export default router;
