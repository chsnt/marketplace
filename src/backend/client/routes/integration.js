import { Router } from 'express';

import IntegrationController from '../controllers/IntegrationController';
import asyncErrorHandler from '../../common/libs/asyncErrorHandler';
import validator from '../../common/libs/requestValidator';
import FileStorage from '../../common/libs/multerFileStorage';

const DOCUMENTS_PATH = 'documents';

const fileStorageInstance = new FileStorage({ storagePath: DOCUMENTS_PATH, isSecure: false });

const router = new Router();

/**
 * @api {post} /api/integration/import-banks Импорт банков
 * @apiVersion 1.0.0
 * @apiName ImportBanks
 * @apiGroup Integrations
 * @apiParam (Body) {Object[]} banks Массив банков
 * @apiParam (Body) {String} banks.id Идентификатор багка во внейшней системе
 * @apiParam (Body) {String} banks.name Наименование банка
 * @apiParam (Body) {String{9}} banks.rcbic БИК банка
 * @apiParam (Body) {String{20}} banks.coorrespondentAccount Корсчет
 * @apiDescription Импорт устройств и их производителей из CRM
 * @apiParamExample {json} Request-Example:
 * [
 *  {
 *    "id": "some external id",
 *    "name": "Сбербанк",
 *    "rcbic": "000000000",
 *    "correspondentAccount": "00000000000000000000"
 *  }
 * ]
 * @apiSuccessExample Пример ответа:
 * HTTP/1.1 200 OK
 * {
 *   "data": null,
 *   "success":true,
 *   "error":null,
 *   "errorCode":null
 * }
 *
 */

router.post(
  '/import-banks',
  validator.body(IntegrationController.validators.importBanks),
  asyncErrorHandler(IntegrationController.importBanks),
);

/**
 * @api {post} /api/integration/import-tax-types Импорт видов налогообложения
 * @apiVersion 1.0.0
 * @apiName ImportTaxTypes
 * @apiGroup Integrations
 * @apiParam (Body) {Object[]} taxTypes Массив видов налогообложения
 * @apiParam (Body) {String} taxTypes.id Идентификатор налогообложения во внейшней системе
 * @apiParam (Body) {String} taxTypes.name Наименование налогообложения
 * @apiDescription Импорт видов налогообложения из CRM
 * @apiParamExample {json} Request-Example:
 * [
 *   {
 *     "id": "some external id",
 *     "name": "test tax type"
 *   }
 * ]
 *
 * @apiSuccessExample Пример ответа:
 * HTTP/1.1 200 OK
 * {
 *   "data": null,
 *   "success":true,
 *   "error":null,
 *   "errorCode":null
 * }
 *
 */

router.post(
  '/import-tax-types',
  validator.body(IntegrationController.validators.importTaxTypes),
  asyncErrorHandler(IntegrationController.importTaxTypes),
);

/**
 * @api {post} /api/integration/import-cities Импорт городов
 * @apiVersion 1.0.0
 * @apiName ImportCities
 * @apiGroup Integrations
 * @apiParam (Body) {Object[]} cities Массив городов обслуживания
 * @apiParam (Body) {String} cities.id Идентификатор города обслуживания во внейшней системе
 * @apiParam (Body) {String} cities.name Наименование города
 * @apiDescription Импорт городов из CRM
 * @apiParamExample {json} Request-Example:
 * [
 *   {
 *     "id": "some external id",
 *     "name": "test city"
 *   }
 * ]
 *
 * @apiSuccessExample Пример ответа:
 * HTTP/1.1 200 OK
 * {
 *   "data": null,
 *   "success":true,
 *   "error":null,
 *   "errorCode":null
 * }
 *
 */

router.post(
  '/import-cities',
  validator.body(IntegrationController.validators.importCities),
  asyncErrorHandler(IntegrationController.importCities),
);

/**
 * @api {post} /api/integration/import-nomenclature Импорт номенклатуры (тарифа или услуги)
 * @apiVersion 1.0.0
 * @apiName ImportNomenclature
 * @apiGroup Integrations
 * @apiParam (Body) {Object[]} [tariffs] Массив тарифов
 * @apiParam (Body) {String} tariffs.id Идентификатор тарифа во внешней системе
 * @apiParam (Body) {String} tariffs.name Наименование тарифа
 * @apiParam (Body) {String} tariffs.cost Стоимость тарифа
 * @apiParam (Body) {Object[]} [services] Массив услуг
 * @apiParam (Body) {String} services.id Идентификатор услуги во внешней системе
 * @apiParam (Body) {String} services.name Наименование услуги
 * @apiParam (Body) {String} services.cost Стоимость услуги
 * @apiParam (Body) {Boolean} [services.forTariffOnly] Признак применения услуги только в рамках тарифа
 * @apiParam (Body) {Object[]} [tariffServiceRelations] Массив услуг
 * @apiParam (Body) {String} tariffServiceRelations.tariffId Идентификатор тарифа во внешней системе
 * @apiParam (Body) {String} tariffServiceRelations.serviceId Идентификатор услуги во внешней системе
 * @apiParam (Body) {Number} tariffServiceRelations.limit Количество возможных использований услуги в рамках тарифа
 * @apiDescription Если услуга или тариф новые, то они создается в системе, в противном случае данные обновляется.
 *                 Если услуга или тариф существует но их нет в интеграции, то они удаляются.
 * @apiParamExample {json} Request-Example:
 * {
 *   "tariffs": [
 *     {
 *       "id": "ext-tariff-id-1",
 *       "name": "Тариф 1",
 *       "cost": 100500,
 *     },
 *     {
 *       "id": "ext-tariff-id-2",
 *       "name": "Тариф 2",
 *       "cost": 100,
 *     }
 *   ],
 *   "services": [
 *     {
 *        "id": "ext-service-id-1",
 *        "name": "Услуга 1",
 *        "cost": 100500,
 *        "forTariffOnly": false
 *     }
 *   ],
 *   "tariffServiceRelations": [
 *     {
 *        "tariffId": "ext-tariff-id-1",
 *        "serviceId": "ext-service-id-1",
 *        "limit": 10
 *     },
 *     {
 *        "tariffId": "ext-tariff-id-2",
 *        "serviceId": "ext-service-id-1",
 *        "limit": 100
 *     }
 *   ]
 * }
 *
 * @apiSuccessExample Пример ответа:
 * HTTP/1.1 200 OK
 * {
 *   "data": null,
 *   "success":true,
 *   "error":null,
 *   "errorCode":null
 * }
 *
 */
router.post(
  '/import-nomenclature',
  validator.body(IntegrationController.validators.importNomenclature),
  asyncErrorHandler(IntegrationController.importNomenclature),
);

/**
 * @api {post} /api/integration/import-devices Импорт устройств и их производителей
 * @apiVersion 1.0.0
 * @apiName ImportDevices
 * @apiGroup Integrations
 * @apiParam (Body) {Object} _ Body
 * @apiParam (Body) {Object[]} _.deviceTypes Типы устройств
 * @apiParam (Body) {String} _.deviceTypes.id Идентификатор типа устройств во внешней системе
 * @apiParam (Body) {String} _.deviceTypes.name Наименование типа устройств
 * @apiParam (Body) {Object[]} _.deviceManufacturers Производители устройств
 * @apiParam (Body) {String} _.deviceManufacturers.id Идентификатор производителя во внешней системе
 * @apiParam (Body) {String} _.deviceManufacturers.name Наименование производителя
 * @apiParam (Body) {Object[]} _.deviceManufacturers.deviceModels Модели устройств
 * @apiParam (Body) {String} _.deviceManufacturers.deviceModels.id Идентификатора устройства во внешней системе
 * @apiParam (Body) {String} _.deviceManufacturers.deviceModels.name Наименование модели устройства
 * @apiParam (Body) {String} _.deviceManufacturers.deviceModels.deviceTypeId Тип устройства
 * @apiDescription Импорт устройств и их производителей из CRM
 * @apiParamExample {json} Request-Example:
 * {
 *   "deviceTypes": [
 *     {
 *        "id": "ext-device-type-id",
 *        "name": "device type name"
 *     }
 *   ],
 *   "deviceManufacturers": [
 *     {
 *       "id": "ext-device-manufacturer-id",
 *       "name": "device manufacturer name",
 *       "deviceModels": [
 *         {
 *           "id": "ext-device-model-id",
 *           "deviceTypeId": "ext-device-type-id",
 *           "name": "device model name"
 *         }
 *       ]
 *     }
 *   ],
 * }
 * @apiSuccessExample Пример ответа:
 * HTTP/1.1 200 OK
 * {
 *   "data": null,
 *   "success":true,
 *   "error":null,
 *   "errorCode":null
 * }
 *
 */

router.post(
  '/import-devices',
  validator.body(IntegrationController.validators.importDevices),
  asyncErrorHandler(IntegrationController.importDevices),
);

/**
 * @api {post} /api/integration/import-document Импорт устройств и их производителей
 * @apiVersion 1.0.0
 * @apiName ImportDocument
 * @apiGroup Integrations
 * @apiParam (FormData) {UUDI} id ID у интегратора
 * @apiParam (FormData) {UUDI} typeId ID типа документа
 * @apiParam (FormData) {UUDI} orderId ID заказа
 * @apiDescription Импорт документов
 * @apiSuccessExample Пример ответа:
 * HTTP/1.1 200 OK
 * {
 *   "data": null,
 *   "success":true,
 *   "error":null,
 *   "errorCode":null
 * }
 *
 */
router.post(
  '/import-document',
  fileStorageInstance.upload.single('file'),
  validator.body(IntegrationController.validators.importDocument),
  asyncErrorHandler(IntegrationController.importDocument),
);

export default router;
