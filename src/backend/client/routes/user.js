import { Router } from 'express';
import UserController from '../controllers/UserController';
import asyncErrorHandler from '../../common/libs/asyncErrorHandler';
import validator from '../../common/libs/requestValidator';

const router = new Router();

/**
 * @api {get} /api/user/me/confirmation-email Подтверждение почты
 * @apiVersion 1.0.0
 * @apiName ConfirmationEmail
 * @apiGroup User
 * @apiParam (Query) {String} codeMail Код подтверждения
 * @description Пользователю на почту приходит ссылка http://localhost:3000/signin?codeMail=48HBC971BDB8G51
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "data": null,
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 *    }
 */
router.get(
  '/me/confirmation-email',
  validator.query(UserController.validators.confirmationEmail),
  asyncErrorHandler(UserController.confirmationEmail),
);

export default router;
