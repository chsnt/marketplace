import { Router } from 'express';

import ServiceController from '../../common/controllers/ServiceController';
import asyncErrorHandler from '../../common/libs/asyncErrorHandler';
import validator from '../../common/libs/requestValidator';

const router = new Router();

/**
 * @api {get} /api/service/get-list Получить список разовых услуг
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup Service
 * @apiParam (Query) {String} [search] Поиск
 * @apiParam (Query) {UUID} [deviceTypeId] Идентификатор типа оборудования
 * @apiSuccessExample {json} Пример ответа:
 *  HTTP/1.1 200 OK
 * {
 *      'data': [
 *        {
 *          id: '7b5fcf18-88ee-4820-bc4b-8a1e36d7294a',
 *          name: 'tariff name',
 *          cost: 100500,
 *          label: 'some label',
 *          icon: 'icon name',
 *          alias: 'name',
 *          serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *          deviceType: {
 *            id: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *            name: 'ККТ',
 *            alias: 'KKT'
 *          } | null,
 *        }
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null
 * }
 */
router.get(
  '/get-list',
  validator.query(ServiceController.validators.getList),
  asyncErrorHandler(ServiceController.getList),
);

/**
 * @api {get} /api/service/get-available-payment-methods Получить список
 * @apiVersion 1.0.0
 * @apiName GetAvailablePaymentMethods
 * @apiGroup Service
 * @apiSuccessExample {json} Пример ответа:
 *  HTTP/1.1 200 OK
 * {
 *      data: [
 *        {
 *          id: '86536609-fb7d-4831-846a-d32fd581978a',
 *          name: 'Все способы',
 *          alias: 'ALL',
 *        },
 *        {
 *          id: '8871a1bc-c482-42ef-b5b3-3e66f5c73771',
 *          name: 'Банковская карта',
 *          alias: 'CARD',
 *        },
 *        {
 *          id: '87cb562f-d5c7-4650-bcec-5429cbacc2cb',
 *          name: 'Счет на оплату',
 *          alias: 'INVOICE',
 *        },
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null
 * }
 */
router.get(
  '/get-available-payment-methods',
  asyncErrorHandler(ServiceController.getAvailablePaymentMethods),
);

export default router;
