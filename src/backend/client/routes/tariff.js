import { Router } from 'express';

import TariffController from '../controllers/TariffController';
import asyncErrorHandler from '../../common/libs/asyncErrorHandler';
import validator from '../../common/libs/requestValidator';

const router = new Router();

/**
 * @api {get} /api/tariff/get-list Получить список тарифов
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup Tariff
 * @apiParam (Query) {String} [search] Поиск
 * @apiParam (Query) {UUID} [deviceTypeId] Идентификатор типа оборудования
 * @apiSuccessExample {json} Пример ответа:
 *  HTTP/1.1 200 OK
 *  {
 *    data: [
 *      {
 *        id: '7b5fcf18-88ee-4820-bc4b-8a1e36d7294a',
 *        name: 'tariff name',
 *        cost: 100500,
 *        costMonth: 20100,
 *        expirationMonths: 6,
 *        label: 'some label',
 *        icon: 'icon name',
 *        alias: 'name',
 *        description: 'large description',
 *        services: [
 *          {
 *            id: '7b5fcf18-88ee-4820-bc4b-8a1e36d7294a',
 *            name: 'service name',
 *            alias: 'name',
 *            description: 'Описание услуги'
 *          },
 *        ],
 *        deviceType: {
 *          id: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *          name: 'ККТ',
 *          alias: 'KKT',
 *        } | null,
 *      },
 *    ],
 *    success: true,
 *    error: 'error message' | null,
 *    errorCode: 'ErrorCode' | null,
 *  }
 */
router.get(
  '/get-list',
  validator.query(TariffController.validators.getList),
  asyncErrorHandler(TariffController.getList),
);

/**
 * @api {get} /api/tariff/:id/get-info Данные о тарифе вместе с услугами
 * @apiVersion 1.0.0
 * @apiName GetInfo
 * @apiGroup Tariff
 * @apiParam (Params) {UUID} id Идентификатор тарифа
 * @apiParamExample Пример запроса:
 *    http://host/api/tariff/779b0a45-590e-41ca-9f1f-e4562f97bfc5/get-info
 * @apiSuccessExample {json} Пример ответа:
 *  HTTP/1.1 200 OK
 * {
 *      "data": {
 *        id: '779b0a45-590e-41ca-9f1f-e4562f97bfc5',
 *        name: 'Удаленная помощь на полгода',
 *        cost: 3303,
 *        costMonth: 551,
 *        expirationMonths: 6,
 *        description: 'large description'
 *        alias: 'udalennaya-pomoshch-6',
 *        services: [
 *          {
 *            id: '85db8d1a-d35d-43c3-91b1-7290be621af6',
 *            name: 'Консультация технической поддержки',
 *            forTariffOnly: true,
 *            alias: 'konsultaciya-tekhnicheskoj-podderzhki',
 *            description: 'Описание услуги'
 *          },
 *          {
 *            id: '64da2bbc-5519-4b34-baad-3930c48f441f',
 *            name: 'Регистрация ККТ в ФНС',
 *            forTariffOnly: false,
 *            alias: 'registraciya-kkt-v-fns',
 *            description: 'Описание услуги'
 *          },
 *          {
 *            id: '40203727-0ac5-4e14-9240-fff0b9b556dc',
 *            name: 'Удаленное обновление ПО ККТ (без стоимости обновления)',
 *            forTariffOnly: false,
 *            alias: 'udalennoe-obnovlenie-po-kkt-bez-stoimosti-obnovleniya',
 *            description: 'Описание услуги'
 *          },
 *        ],
 *      },
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 * }
 */
router.get(
  '/:id/get-info',
  validator.params(TariffController.validators.getTariff),
  asyncErrorHandler(TariffController.getTariff),
);

export default router;
