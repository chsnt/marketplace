import { Router } from 'express';

import EdoController from '../../common/controllers/EdoController';
import asyncErrorHandler from '../../common/libs/asyncErrorHandler';

const router = new Router();

/**
 * @api {get} /api/edo/get-list Получить список оператора ЭДО
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup EDO
 * @apiSuccessExample {json} Пример ответа:
 *  HTTP/1.1 200 OK
 *  {
 *    data: [{
 *      id: 'cfb4588a-bdf1-4d43-9d83-6f2b9bd6d5a0',
 *      name: 'Корус консалтинг СНГ (Сфера Курьер)',
 *      alias: 'KORUS_KONSALTING_SNG_SFERA_KURER',
 *    },
 *    {
 *      id: '30ffaaa1-1c4f-42a5-b9de-87fa2426dc1f',
 *      name: 'СКБ Контур (Контур.Диадок)',
 *      alias: 'SKB_KONTUR_KONTUR_DIADOK',
 *    },
 *    {
 *      id: '9575b473-bb84-471b-a20e-090e56f402da',
 *      name: 'TaxCom',
 *      alias: 'TAX_COM',
 *    },
 *    {
 *      id: 'e6514477-fcb0-42a4-a411-1d956cca7bd0',
 *      name: 'E-COM',
 *      alias: 'E_COM',
 *    },
 *    {
 *      id: 'a8a42118-24e5-4ce8-9b97-bc17b4c2f45b',
 *      name: 'Калуга.Астрал (Калуга Онлайн)',
 *      alias: 'KALUGA_ASTRAL_KALUGA-ONLAJN',
 *    },
 *    {
 *      id: 'abe383da-dd10-4944-8ca7-96b5c55b7c18',
 *      name: 'Моего оператора нет в списке',
 *      alias: 'EDO_IS_MISSING',
 *    },
 *    {
 *      id: '79f09207-352e-4ae0-86dc-bd77ee0086b3',
 *      name: 'Нет ЭДО',
 *      alias: 'NOT_IN_LIST',
 *    }],
 *    success: true,
 *    error: 'error message' | null,
 *    errorCode: 'ErrorCode' | null
 *  }
 */
router.get(
  '/get-list',
  asyncErrorHandler(EdoController.getList),
);

export default router;
