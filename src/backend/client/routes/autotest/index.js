import { Router } from 'express';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';
import ClientController from '../../controllers/ClientController';
import validator from '../../../common/libs/requestValidator';

const router = new Router();

/**
 * @api {post} /api/autotest/delete-client удалить клиента и связанные данные
 * @apiVersion 1.0.0
 * @apiName PostDeleteClient
 * @apiGroup Autotest
 * @apiParam (Body) {String} phone номер телефон
 * @apiSuccessExample {json} Пример ответа:
 *  HTTP/1.1 200 OK
 *  {
 *     data: null,
 *     success: true,
 *     error: 'error message' | null,
 *     errorCode: 'ErrorCode' | null,
 *   }
 */
router.post(
  '/delete-client',
  validator.body(ClientController.validators.deleteClient),
  asyncErrorHandler(ClientController.deleteClient),
);

/**
 * @api {get} /api/autotest/get-autorization-code получить код подтверждения номера телефона
 * @apiVersion 1.0.0
 * @apiName GetAutorizationCode
 * @apiGroup Autotest
 * @apiParam (Query) {String} phone номер телефон
 * @apiSuccessExample {json} Пример ответа:
 *  HTTP/1.1 200 OK
 *  {
 *     data: 4586,
 *     success: true,
 *     error: 'error message' | null,
 *     errorCode: 'ErrorCode' | null,
 *   }
 */
router.get(
  '/get-autorization-code',
  validator.query(ClientController.validators.getAutorizationCode),
  asyncErrorHandler(ClientController.getAutorizationCode),
);


export default router;
