import { Router } from 'express';
import validator from '../../../common/libs/requestValidator';

import OrderController from '../../controllers/OrderController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';

const router = new Router();

/**
 * @api {post} /api/order/add-to-cart Добавить в корзину
 * @apiVersion 1.0.0
 * @apiName AddToCart
 * @apiGroup Order
 * @apiParam (Body) {Object} [cart] Корзина.
 * @apiParam (Body) {String[]} [cart.tariffs] Тарифы, добавленные в корзину.
 * @apiParam (Body) {String[]} [cart.services] Услуги, добавленные в корзину.
 * @apiSuccess (data) {UUID} orderId id заказа
 * @apiParamExample {json}
 *   {
 *       "tariffs": ["81e580c8-6d3d-4723-82de-f3775d435877"],
 *   }
 *
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *   {
 *     data: {
 *       orderId: 'e218bd63-c026-43c1-be6b-06dc8cd980b3',
 *       "totalCost": 10514,
 *       number: 5,
 *       tariffs: [
 *         {
 *           id: '567bd464-516f-4401-95c8-c54d60ba0ac8',
 *           tariff: {
 *             id: '83d3f635-349e-41b4-827e-8790ae67fd5c',
 *             name: 'Удаленная помощь на год',
 *             cost: 4444,
 *             label: null,
 *             icon: null,
 *             expirationMonths: 12,
 *             alias: 'udalennaya-pomoshch-12',
 *             description: 'large description',
 *             services: [
 *               {
 *                 id: '354edd5a-d5e4-4738-b7a9-81a5eb981594',
 *                 name: 'Мониторинг передачи чеков в ОФД',
 *                 label: null,
 *                 icon: null,
 *                 alias: 'monitoring-peredachi-chekov-v-ofd',
 *                 forTariffOnly: true,
 *                 cost: 0,
 *               },
 *               {
 *                 id: 'c9b006d4-73ba-4dd2-96ac-9cc8bea0ae3d',
 *                 name: 'Перерегистрация ККТ в ФНС',
 *                 label: null,
 *                 icon: null,
 *                 alias: 'pereregistraciya-kkt-v-fns',
 *                 forTariffOnly: false,
 *                 cost: 500,
 *               },
 *             ],
 *           },
 *         },
 *       ],
 *       services: [
 *         {
 *           id: 'b563a040-4788-4a35-804a-340c5bcc6e2d',
 *           service: {
 *             id: '40203727-0ac5-4e14-9240-fff0b9b556dc',
 *             name: 'Удаленное обновление ПО ККТ (без стоимости обновления)',
 *             cost: 600,
 *             label: null,
 *             icon: null,
 *             alias: 'udalennoe-obnovlenie-po-kkt-bez-stoimosti-obnovleniya',
 *           },
 *         },
 *         {
 *           id: 'b9611a6a-c51e-452e-8ab6-19d001643eb1',
 *           service: {
 *             id: '40203727-0ac5-4e14-9240-fff0b9b556dc',
 *             name: 'Удаленное обновление ПО ККТ (без стоимости обновления)',
 *             cost: 600,
 *             label: null,
 *             icon: null,
 *             alias: 'udalennoe-obnovlenie-po-kkt-bez-stoimosti-obnovleniya',
 *           },
 *         },
 *         {
 *           id: 'e9087c81-3f3a-42ff-a7f8-00c5a98fa155',
 *           service: {
 *             id: 'b40ace3b-76f5-4037-8bd2-7e146aa30e80',
 *             name: 'ЭЦП без носителя',
 *             cost: 900,
 *             label: null,
 *             icon: null,
 *             alias: 'ehcp-bez-nositelya',
 *           },
 *         },
 *         {
 *           id: 'b61ad482-1ca1-4c3d-9e85-7d524d6161f4',
 *           service: {
 *             id: '63e02fdf-343a-449f-bb20-1d00c8addfca',
 *             name: 'ЭЦП для ЕГАИС с носителем',
 *             cost: 2500,
 *             label: null,
 *             icon: null,
 *             alias: 'ehcp-dlya-egais-s-nositelem',
 *           },
 *         },
 *       ],
 *     },
 *     success: true,
 *     error: 'error message' | null,
 *     errorCode: 'ErrorCode' | null,
 *   }
 *
 */
router.post(
  '/add-to-cart',
  validator.body(OrderController.validators.addToCart),
  asyncErrorHandler(OrderController.addToCart),
);

/**
 * @api {post} /api/order/remove-from-cart Удалить с корзины
 * @apiVersion 1.0.0
 * @apiName RemoveFromCart
 * @apiGroup Order
 * @apiParam (Body) {Object} [cart] Корзина.
 * @apiParam (Body) {String[]} [cart.tariffs] Удаление тарифа из корзины.
 * @apiParam (Body) {String[]} [cart.services] Удаление сервиса из корзины.
 * @apiParamExample {json}
 *   {
 *       "services": ["81e580c8-6d3d-4723-82de-f3775d435877"],
 *   }
 *
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      "data": {
 *        "id": "e218bd63-c026-43c1-be6b-06dc8cd980b3",
 *        "number": 5,
 *        "tariffs": [
 *            {
 *                "id": "f3a672e8-9557-4e63-aadc-acf1165c3b36",
 *                "tariffId": "14eeadde-dbcb-44ac-9d72-e1cc11617c98"
 *            },
 *            {
 *                "id": "0a4bd588-f9f4-4ff7-bd83-99ea7c8544fd",
 *                "tariffId": "14eeadde-dbcb-44ac-9d72-e1cc11617c98"
 *            }
 *        ],
 *        "services": [
 *            {
 *                "id": "206ea7cc-735b-411d-b654-8149c61ebee6",
 *                "serviceId": "a64d568f-142d-45c3-a58d-da9a8e414e12"
 *            },
 *            {
 *                "id": "feb5c12c-a875-4c13-914e-f508e08ed744",
 *                "serviceId": "a64d568f-142d-45c3-a58d-da9a8e414e12"
 *            }
 *        ]
 *      },
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 *    }
 */
router.post(
  '/remove-from-cart',
  validator.body(OrderController.validators.removeFromCart),
  asyncErrorHandler(OrderController.removeFromCart),
);

/**
 * @api {get} /api/order/get-cart Список тарифов с услугами и услуг в корзине
 * @apiVersion 1.0.0
 * @apiName GetCart
 * @apiGroup Order
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *   {
 *     data: {
 *       id: '6d8f5449-bf71-4e49-9fd6-311360e2869d',
 *       number: 5,
 *       tariffs: [
 *         {
 *           id: '567bd464-516f-4401-95c8-c54d60ba0ac8',
 *           tariff: {
 *             id: '83d3f635-349e-41b4-827e-8790ae67fd5c',
 *             name: 'Удаленная помощь на год',
 *             cost: 4444,
 *             withoutDiscountCost: 7000
 *             label: null,
 *             icon: null,
 *             expirationMonths: 12,
 *             alias: 'udalennaya-pomoshch-12',
 *             description: 'large description',
 *             services: [
 *               {
 *                 id: '354edd5a-d5e4-4738-b7a9-81a5eb981594',
 *                 name: 'Мониторинг передачи чеков в ОФД',
 *                 label: null,
 *                 icon: null,
 *                 alias: 'monitoring-peredachi-chekov-v-ofd',
 *                 forTariffOnly: true,
 *               },
 *               {
 *                 id: 'c9b006d4-73ba-4dd2-96ac-9cc8bea0ae3d',
 *                 name: 'Перерегистрация ККТ в ФНС',
 *                 label: null,
 *                 icon: null,
 *                 alias: 'pereregistraciya-kkt-v-fns',
 *                 forTariffOnly: false,
 *               },
 *             ],
 *           },
 *         },
 *       ],
 *       services: [
 *         {
 *           id: 'b563a040-4788-4a35-804a-340c5bcc6e2d',
 *           service: {
 *             id: '40203727-0ac5-4e14-9240-fff0b9b556dc',
 *             name: 'Удаленное обновление ПО ККТ (без стоимости обновления)',
 *             cost: 600,
 *             withoutDiscountCost: 683
 *             label: null,
 *             icon: null,
 *             alias: 'udalennoe-obnovlenie-po-kkt-bez-stoimosti-obnovleniya',
 *           },
 *         },
 *       ],
 *       totalCost: 6736.51,
 *       totalWithoutDiscountCost: 14333,
 *       discountCod: "СКИДКА11", //название купона
 *       specialDiscount: 15 // дополнительная скидка в процентах назаченная через адменку
 *     },
 *     success: true,
 *     error: 'error message' | null,
 *     errorCode: 'ErrorCode' | null,
 *   }
 */
router.get(
  '/get-cart',
  asyncErrorHandler(OrderController.getCart),
);

/**
 * @api {post} /api/order/create-order-from-cart Сформировать заказ из корзины
 * @apiVersion 1.0.0
 * @apiName CreateOrderFromCart
 * @apiGroup Order
 * @apiParam (Body) {String} paymentType alias типа оплаты ('CARD', 'INVOICE')
 * @apiParam (Body) {String{0..1024}} [description] Комментарий к заказу
 * @apiParam (Body) {Array} order Массив услуг и тарифов для выбранного устройства
 * @apiParam (Body) {String} [order.serviceAddressId] UUID адреса устройства привязанного к организации. Если него нет, нужно отправить newDevice
 * @apiParam (Body) {Object} [order.newDevice] Данные о новом устройстве. Взаимоисключающий параметр с `order.id`
 * @apiParam (Body) {String} order.newDevice.clientId UUID клиента
 * @apiParam (Body) {String} order.newDevice.model UUID модели устройства
 * @apiParam (Body) {Array} order.newDevice.parameters Коллекция параметров
 * @apiParam (Body) {String[]} order.newDevice.parameters.id UUID параметра
 * @apiParam (Body) {String[]} order.newDevice.parameters.value Значение параметра
 * @apiParam (Body) {String[]} order.newDevice.address Адрес обслуживания
 * @apiParam (Body) {UUID} order.newDevice.address.region Субъект РФ
 * @apiParam (Body) {UUID} order.newDevice.address.cityId Город из списка обслуживаемых городов
 * @apiParam (Body) {String} order.newDevice.address.street Улица
 * @apiParam (Body) {String} order.newDevice.address.house Дом
 * @apiParam (Body) {String} order.newDevice.address.apartment Квартира/офис
 * @apiParam (Body) {String} order.tariff UUID тарифа
 * @apiParam (Body) {Array} order.services Список уникальных UUID сервисов для устройства
 * @apiDescription Если у услуги статус адреса обслуживания "При создании ЗНО" или "Не запрашивается", то serviceAddressId и newDevice передавать не требуется<br/>
 *                  Если в тариф входит услуга со статусом адреса "При заказе", то для тарифа требуется передать, или serviceAddressId, или newDevice
 *
 * @apiSuccess (data) {UUID} orderId id заказа
 *
 * @apiParamExample {json} Пример запроса, если адрес обслуживания не требуется:
 * {
 *   paymentType: 'CARD',
 *   order: [
 *     {
 *       services: ['85914e76-060d-4f29-b057-55cf85612cb3']
 *     }
 *   ]
 * }
 *
 * @apiParamExample {json} Пример запроса с выбранном устройством:
 * {
 *   paymentType: 'CARD',
 *   order: [
 *     {
 *       serviceAddressId: '3380c985-665c-4011-a674-72b187c531a1',
 *       tariff: 'feb5c12c-a875-4c13-914e-f508e08ed744',
 *       services: [
 *         '206ea7cc-735b-411d-b654-8149c61ebee6',
 *         '85914e76-060d-4f29-b057-55cf85612cb3',
 *         '470259fb-bce3-4891-91ff-3cde6e4510a0',
 *       ],
 *     },
 *     {
 *       newDevice: {
 *         model: 'bc14c5c1-0c09-427d-8f79-82f5c74483a9',
 *         clientId: '85914e76-060d-4f29-b057-55cf85612cb3',
 *         parameters: [
 *           {
 *             id: '31cb14f6-a421-4b58-85f2-90f643a50790',
 *             value: '888555000123',
 *           },
 *           {
 *             id: 'd0dae0b0-0dd4-4c9b-8f70-8eb519a0cda1',
 *             value: 'Аналоговая клавиатура',
 *           },
 *           {
 *             id: 'd2707b66-b9e8-4cf4-8600-d9db716dd32e',
 *             value: 'Эльбрус',
 *           },
 *         ],
 *         address: {
 *           region: '59637637-4bbc-49b8-91cc-58c2cb43e51e',
 *           cityId: '21918605-05f3-42e9-bde4-eaa44a5c8eb1',
 *           street: 'Невский пр.',
 *           house: '1',
 *           apartment: '123',
 *           postalCode: '000555',
 *         },
 *       },
 *       tariff: 'feb5c12c-a875-4c13-914e-f508e08ed744',
 *       services: [
 *         '206ea7cc-735b-411d-b654-8149c61ebee6',
 *         '85914e76-060d-4f29-b057-55cf85612cb3',
 *         '470259fb-bce3-4891-91ff-3cde6e4510a0',
 *       ],
 *     },
 *   ],
 * }
 *
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *   {
 *     data: { orderId: 'a6ea850b-2c06-4e1f-ad31-712e54f6c77c' },
 *     success: true,
 *     error: 'error message' | null,
 *     errorCode: 'ErrorCode' | null,
 *   }
 *
 */
router.post(
  '/create-order-from-cart',
  validator.body(OrderController.validators.createOrderFromCart),
  asyncErrorHandler(OrderController.createOrderFromCart),
);

/**
 * @api {post} /api/order/check-payment Проверка платежа на стороне банка
 * @apiVersion 1.0.0
 * @apiName CheckPayment
 * @apiGroup Order
 * @apiParam (Body) {UUID} orderId id заказа в системе.
 * @apiParam (Body) {String} acquiringOrderNumber id заказа эквайринга.
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      "data": true,
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 *    }
 */
router.post(
  '/check-payment',
  validator.body(OrderController.validators.checkPayment),
  asyncErrorHandler(OrderController.checkPayment),
);

/**
 * @api {get} /api/order/:id/get-document-list Список документов заказа
 * @apiVersion 1.0.0
 * @apiName GetDocumentList
 * @apiGroup Order
 * @apiParam (Params) {String} id UUID заказа
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      "data": [{
 *        id: '8d8ba2a3-92dd-4483-a49b-a1410077b0d3',
 *        name: 'Universalnii_peredatochnii_dokument_(UPD)_№_IT-1_ot_15.10.2020.pdf',
 *        date: '2020-11-12',
 *        type: {
 *          name: 'УПД (статус 1)',
 *          alias: 'UTD_STATUS_ONE',
 *        },
 *      },
 *      {
 *        id: '3554c29f-b262-470f-958a-d5fe1c00b8f0',
 *        name: 'Universalnii_peredatochnii_dokument_(UPD)_№_IT-1_ot_15.10.2020.pdf',
 *        date: '2020-11-12',
 *        type: {
 *          name: 'УПД (статус 1)',
 *          alias: 'UTD_STATUS_ONE',
 *        },
 *      }],
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 *    }
 */
router.get(
  '/:id/get-document-list',
  validator.params(OrderController.validators.getDocumentList),
  asyncErrorHandler(OrderController.getDocumentList),
);

/**
 * @api {get} /api/order/document/:documentId/get-document Скачать документ
 * @apiVersion 1.0.0
 * @apiName GetDocument
 * @apiGroup Order
 * @apiParam (Params) {UUID} documentId ID документа
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      "data": null,
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 *    }
 */
router.get(
  '/document/:documentId/get-document',
  validator.params(OrderController.validators.getDocument),
  asyncErrorHandler(OrderController.getDocument),
);

/**
 * @api {post} /api/order/add-coupon
 * @apiVersion 1.0.0
 * @apiName AddCoupon
 * @apiGroup Order
 * @apiParam (Body) {String} code Имя купона
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      "data": {
 *       id: '6d8f5449-bf71-4e49-9fd6-311360e2869d',
 *       number: 5,
 *       tariffs: [
 *         {
 *           id: '567bd464-516f-4401-95c8-c54d60ba0ac8',
 *           tariff: {
 *             id: '83d3f635-349e-41b4-827e-8790ae67fd5c',
 *             name: 'Удаленная помощь на год',
 *             cost: 4444,
 *             withoutDiscountCost: 7000
 *             label: null,
 *             icon: null,
 *             expirationMonths: 12,
 *             alias: 'udalennaya-pomoshch-12',
 *             description: 'large description',
 *             services: [
 *               {
 *                 id: '354edd5a-d5e4-4738-b7a9-81a5eb981594',
 *                 name: 'Мониторинг передачи чеков в ОФД',
 *                 label: null,
 *                 icon: null,
 *                 alias: 'monitoring-peredachi-chekov-v-ofd',
 *                 forTariffOnly: true,
 *               },
 *               {
 *                 id: 'c9b006d4-73ba-4dd2-96ac-9cc8bea0ae3d',
 *                 name: 'Перерегистрация ККТ в ФНС',
 *                 label: null,
 *                 icon: null,
 *                 alias: 'pereregistraciya-kkt-v-fns',
 *                 forTariffOnly: false,
 *               },
 *             ],
 *           },
 *         },
 *       ],
 *       services: [
 *         {
 *           id: 'b563a040-4788-4a35-804a-340c5bcc6e2d',
 *           service: {
 *             id: '40203727-0ac5-4e14-9240-fff0b9b556dc',
 *             name: 'Удаленное обновление ПО ККТ (без стоимости обновления)',
 *             cost: 600,
 *             withoutDiscountCost: 683
 *             label: null,
 *             icon: null,
 *             alias: 'udalennoe-obnovlenie-po-kkt-bez-stoimosti-obnovleniya',
 *           },
 *         },
 *       ],
 *       totalCost: 6736.51,
 *       totalWithoutDiscountCost: 14333,
 *       discountCod: "СКИДКА11", //название купона
 *       specialDiscount: 15 // дополнительная скидка в процентах назаченная через адменку
 *     },
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 *    }
 */
router.post(
  '/add-coupon',
  validator.body(OrderController.validators.addCoupon),
  asyncErrorHandler(OrderController.addCoupon),
);

/**
 * @api {post} /api/order/remove-coupon
 * @apiVersion 1.0.0
 * @apiName RemoveCoupon
 * @apiGroup Order
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      "data": {
 *       id: '6d8f5449-bf71-4e49-9fd6-311360e2869d',
 *       number: 5,
 *       tariffs: [
 *         {
 *           id: '567bd464-516f-4401-95c8-c54d60ba0ac8',
 *           tariff: {
 *             id: '83d3f635-349e-41b4-827e-8790ae67fd5c',
 *             name: 'Удаленная помощь на год',
 *             cost: 5950,
 *             withoutDiscountCost: 7000
 *             label: null,
 *             icon: null,
 *             expirationMonths: 12,
 *             alias: 'udalennaya-pomoshch-12',
 *             description: 'large description',
 *             services: [
 *               {
 *                 id: '354edd5a-d5e4-4738-b7a9-81a5eb981594',
 *                 name: 'Мониторинг передачи чеков в ОФД',
 *                 label: null,
 *                 icon: null,
 *                 alias: 'monitoring-peredachi-chekov-v-ofd',
 *                 forTariffOnly: true,
 *               },
 *               {
 *                 id: 'c9b006d4-73ba-4dd2-96ac-9cc8bea0ae3d',
 *                 name: 'Перерегистрация ККТ в ФНС',
 *                 label: null,
 *                 icon: null,
 *                 alias: 'pereregistraciya-kkt-v-fns',
 *                 forTariffOnly: false,
 *               },
 *             ],
 *           },
 *         },
 *       ],
 *       services: [
 *         {
 *           id: 'b563a040-4788-4a35-804a-340c5bcc6e2d',
 *           service: {
 *             id: '40203727-0ac5-4e14-9240-fff0b9b556dc',
 *             name: 'Удаленное обновление ПО ККТ (без стоимости обновления)',
 *             cost: 283.05,
 *             withoutDiscountCost: 333
 *             label: null,
 *             icon: null,
 *             alias: 'udalennoe-obnovlenie-po-kkt-bez-stoimosti-obnovleniya',
 *           },
 *         },
 *       ],
 *       totalCost: 12183.05,
 *       totalWithoutDiscountCost: 14333,
 *       specialDiscount: 15 // дополнительная скидка в процентах назаченная через адменку
 *     },
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 *    }
 */
router.post(
  '/remove-coupon',
  asyncErrorHandler(OrderController.removeCoupon),
);

export default router;
