import { Router } from 'express';

import SupportRequestController from '../../controllers/SupportRequestController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';
import validator from '../../../common/libs/requestValidator';
import FileStorage from '../../../common/libs/multerFileStorage';

const router = new Router();

const DOCUMENTS_PATH = 'support';

const fileStorageInstance = new FileStorage({ storagePath: DOCUMENTS_PATH });


/**
 * @api {get} /api/support-request/get-list Список запросов помощь
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup SupportRequest
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: '6bffe39f-2505-4e86-9999-70b61652a1dd',
 *          createdAt: '2020-11-25T20:25:09.249Z',
 *          number: 31,
 *          solution: null,
 *          resolvedAt: '2020-12-07T18:49:31.824Z',
 *          representative: '123123123',
 *          topic: 'sd',
 *          status: {
 *            name: 'Решено',
 *            alias: 'SOLVED'
 *          }
 *        },
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-list',
  asyncErrorHandler(SupportRequestController.getList),
);


/**
 * @api {get} /api/support-request/get-topics Список тем обращений
 * @apiVersion 1.0.0
 * @apiName GetTopics
 * @apiGroup SupportRequest
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: 'fc74f22b-f0f2-4bf3-bda3-2ebd78628050',
 *          name: 'Просьба помочь',
 *          alias: 'HELP',
 *        }
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-topics',
  asyncErrorHandler(SupportRequestController.getTopics),
);

/**
 * @api {post} /api/support-request/create Создать обращение
 * @apiVersion 1.0.0
 * @apiName CreateRequest
 * @apiGroup Support Request
 * @apiParam (FormData) {String} topic UUID темы обращения
 * @apiParam (FormData) {String{..1024}} description Описание проблемы
 * @apiParam (FormData) {Array} documents Прикреплённые файлы
 * @apiDescription Принимает форму с заголовком Content-Type: multipart/form-data
 * @apiParamExample Request-Example:
 *    http://host/api/support-request/create
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      'data': null,
 *      'success': true,
 *      'error': 'error message' | null,
 *      'errorCode': 'ErrorCode' | null
 *    }
 */
router.post(
  '/create',
  fileStorageInstance.upload.array('documents', 10),
  validator.body(SupportRequestController.validators.createRequest),
  asyncErrorHandler(SupportRequestController.createRequest),
);

/**
 * @api {get} /api/support-request/document/:documentId/get-document Скачать документ
 * @apiVersion 1.0.0
 * @apiName GetDocument
 * @apiGroup SupportRequest
 * @apiParam (Params) {UUID} documentId ID документа
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      'data': null,
 *      'success': true,
 *      'error': 'error message' | null,
 *      'errorCode': 'ErrorCode' | null
 *    }
 */
router.get(
  '/document/:documentId/get-document',
  validator.params(SupportRequestController.validators.getDocument),
  asyncErrorHandler(SupportRequestController.getDocument),
);

/**
 * @api {post} /api/support-request/create-review Оставить отзыв обращению
 * @apiVersion 1.0.0
 * @apiName CreateSupportRequestReview
 * @apiGroup SupportRequest
 * @apiParam (FormData) {String} supportRequestId UUID заявки.
 * @apiParam (FormData) {String{..255}} text Отзыв.
 * @apiParam (FormData) {Number{0..10}} mark Оценка обращения.
 * @apiParam (FormData) {Array} documents Прикреплённые файлы.
 * @apiDescription Отзыв и оценку можно оставить для обращений с типом "Прочее"
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "data": null,
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 *    }
 */
router.post(
  '/create-review',
  fileStorageInstance.upload.array('documents', 10),
  validator.body(SupportRequestController.validators.createReview),
  asyncErrorHandler(SupportRequestController.createReview),
);

export default router;
