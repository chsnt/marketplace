import { Router } from 'express';

import validator from '../../../common/libs/requestValidator';
import DeviceController from '../../controllers/DeviceController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';

const router = new Router();

/**
 * @api {post} /api/device/create-client-device Создание оборудование
 * @apiVersion 1.0.0
 * @apiName CreateClientDevice
 * @apiGroup Device
 * @apiParam (Body) {String} model UUID модели устройства
 * @apiParam (Body) {String} clientId UUID клиента
 * @apiParam (Body) {Object[]} parameters Коллекция параметров
 * @apiParam (Body) {String} parameters.id UUID параметра
 * @apiParam (Body) {String} parameters.value Значение параметра
 * @apiParam (Body) {Object} address Адрес обслуживания устройства
 * @apiParam (Body) {UUID} address.region Субъект РФ
 * @apiParam (Body) {UUID} address.cityId Город из списка обслуживаемых городов
 * @apiParam (Body) {String} address.street Улица
 * @apiParam (Body) {String} address.house Дом
 * @apiParam (Body) {String} address.apartment Квартира/офис
 * @apiParam (Body) {String} [address.postalCode] Почтовый индекс
 * @apiParamExample {json} Пример запроса:
 * {
 *   model: 'bc14c5c1-0c09-427d-8f79-82f5c74483a9',
 *   parameters: [
 *     {
 *       id: '31cb14f6-a421-4b58-85f2-90f643a50790',
 *       value: '888555000123',
 *     },
 *     {
 *       id: 'd0dae0b0-0dd4-4c9b-8f70-8eb519a0cda1',
 *       value: '7.5 В, встроенный аккумулятор 2600 мАч',
 *     },
 *     {
 *       id: 'd2707b66-b9e8-4cf4-8600-d9db716dd32e',
 *       value: 'ARM 9 Zatara 180 МГц',
 *     },
 *   ],
 *   address: {
 *     region: '59637637-4bbc-49b8-91cc-58c2cb43e51e',
 *     cityId: '21918605-05f3-42e9-bde4-eaa44a5c8eb1',
 *     street: 'Невский пр.',
 *     house: '1',
 *     apartment: '123',
 *     postalCode: '000555',
 *   }
 * }
 *
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: {
 *        id: '31cb14f6-a421-4b58-85f2-90f643a50790'
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.post(
  '/create-client-device',
  validator.body(DeviceController.validators.createClientDevice),
  asyncErrorHandler(DeviceController.createClientDevice),
);

/**
 * @api {get} /api/device/get-devices-with-parameters Список производителей с моделями и их параметрами
 * @apiVersion 1.0.0
 * @apiName GetDevicesWithParameters
 * @apiGroup Device
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *          {
 *            id: '45a863fb-8e42-4dd5-992c-f668d812a26f'
 *            manufacturer: 'Эвотор',
 *            models: [
 *               {
 *                 id: '47e6ec4e-89ce-4af5-8eed-bfb5b1af2239'
 *                 model: 'эвотор 7.0',
 *                 type: 'Кассы',
 *                 parameters: [
 *                   {
 *                     id: '31cb14f6-a421-4b58-85f2-90f643a50790',
 *                     name: 'Серийный номер',
 *                     isRequired: true,
 *                     type: 'NUMBER'
 *                   },
 *                   {
 *                     id: 'd0dae0b0-0dd4-4c9b-8f70-8eb519a0cda1',
 *                     name: 'Клавиатура',
 *                     isRequired: false,
 *                     type: 'STRING'
 *                   },
 *                   {
 *                     id: '34d315d6-d8f0-46fb-9744-1c0058632049',
 *                     name: 'Описание',
 *                     isRequired: false,
 *                     type: 'STRING'
 *                   }
 *                 ]
 *               },
 *               {
 *                 id: '54cb77b0-0f3b-4ed8-bfe3-4d7612aaafff'
 *                 model: 'эвотор 5.0',
 *                 'type': 'Кассы',
 *                 parameters: [
 *                   {
 *                     id: '31cb14f6-a421-4b58-85f2-90f643a50790',
 *                     name: 'Серийный номер',
 *                     isRequired: true
 *                     type: 'NUMBER'
 *                   }
 *                 ]
 *               }
 *            ]
 *          },
 *          {
 *            id: 'ed68c066-6970-4e5d-8ad7-2740768aaf39'
 *            manufacturer: 'VeriFone',
 *            models: [
 *               {
 *                 id: '47e6ec4e-89ce-4af5-8eed-bfb5b1af2239'
 *                 model: 'Vx520',
 *                 type: 'Кассы',
 *                 parameters: [
 *                   {
 *                     id: '31cb14f6-a421-4b58-85f2-90f643a50790',
 *                     name: 'Серийный номер',
 *                     isRequired: true,
 *                     type: 'NUMBER'
 *                   },
 *                   {
 *                     id: 'd2707b66-b9e8-4cf4-8600-d9db716dd32e',
 *                     name: 'Микропроцессор',
 *                     isRequired: false,
 *                     type: 'STRING'
 *                   }
 *                 ]
 *               }
 *            ]
 *          }
 *        ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-devices-with-parameters',
  asyncErrorHandler(DeviceController.getDeviceParameters),
);

/**
 * @api {get} /api/device/get-client-devices Список устройств организации
 * @apiVersion 1.0.0
 * @apiName GetClientDevices
 * @apiGroup Device
 * @apiParam (Query) {String} [deviceModelName] Поиск по наименованию модели устройства
 * @apiParam (Query) {String} [parameterValue] Поиск по значению параметра устройства (например, серийник)
 * @apiParam (Query) {String} [type] UUID тип устройства
 * @apiParam (Query) {String} [model] UUID модель устройства
 * @apiParam (Query) {String} [manufacture] UUID производитель устройства устройства
 * @apiParamExample Пример запроса:
 * http://localhost/api/device/info?deviceType=e94e55e1-28f5-48cf-881c-779ddf5d31ce&deviceModel=47e6ec4e-89ce-4af5-8eed-bfb5b1af2239
 * @apiSuccessExample {json} Пример ответа:
 * HTTP/1.1 200 OK
 * {
 *   data: [
 *     {
 *       client: 'ИП Пупкович',
 *       id: '353d0ab3-2195-4e41-a554-f1444233f1ed',
 *       deviceType: 'ККТ',
 *       deviceManufacturer: 'Эвотор',
 *       deviceModel: {
 *         id: '93226545-999e-4dba-b9a0-2293592a7e0c',
 *         name: 'Эвотор 7.3',
 *       },
 *       address: {
 *         id: '05c21fa9-bc52-4359-a47f-7b226f4a49d1',
 *         street: 'Ленина',
 *         house: '17',
 *         apartment: '17',
 *         postalCode: null,
 *         city: {
 *           id: '88903722-56be-46e3-8b32-94737856812f',
 *           name: 'Барнаул',
 *           utcOffset: 0,
 *         },
 *         region: {
 *           id: 'd5ec8435-8910-479c-ba60-c3919fce1ac9',
 *           name: 'Алтайский край',
 *         },
 *       },
 *       parameters: [
 *         {
 *           id: 'a3b8b98b-d3a5-4fd3-8dd5-570cfd2a5304',
 *           value: '018156',
 *           name: 'Сенсорный дисплей',
 *           alias: 'SENSORNYJ-DISPLEJ',
 *         },
 *       ],
 *     },
 *     {
 *       client: 'ИП Пупкович',
 *       id: 'e60bc9ea-577c-401d-85c1-7ed964d5fe51',
 *       deviceType: 'Поддержка АРМ',
 *       deviceModel: {
 *         id: 'deb43204-0448-4cd9-8d1a-4b8681ff630c',
 *         name: 'ПК',
 *       },
 *       address: {
 *         id: '7ebdec1f-edd6-4c9c-943d-1c70d8db5ffa',
 *         street: 'улица Ленина',
 *         house: '1',
 *         apartment: '123',
 *         postalCode: '555888',
 *         city: {
 *           id: 'fbfc5bc7-6a43-4b07-b2b7-17960dc81bc9',
 *           name: 'Саров',
 *           utcOffset: 0,
 *         },
 *         region: {
 *           id: '4680dce7-ac13-410c-8b7d-a467845fe7ed',
 *           name: 'Республика Удмуртия',
 *         },
 *       },
 *       parameters: [
 *         {
 *           id: '69ebcb85-c205-475c-ab57-561e05acde11',
 *           value: 'Windows10',
 *           name: 'Операционная система',
 *           alias: 'OPERATION_SYSTEM',
 *         },
 *         {
 *           id: '4ba712dd-e3f2-43b1-ad43-1df340392090',
 *           value: 'Zver',
 *           name: 'Модель',
 *           alias: 'MODEL',
 *         },
 *         {
 *           id: '28336e0f-4354-419a-8114-b1b4b543891a',
 *           value: 'Asus',
 *           name: 'Производитель',
 *           alias: 'MANUFACTURER',
 *         },
 *       ],
 *     },
 *   ],
 *   success: true,
 *   error: 'error message' | null,
 *   errorCode: 'ErrorCode' | null,
 * }
 */
router.get(
  '/get-client-devices',
  validator.query(DeviceController.validators.getClientDevices),
  asyncErrorHandler(DeviceController.getClientDevices),
);

/**
 * @api {get} /api/device/:id/get-parameters Список параметров модели оборудования
 * @apiVersion 1.0.0
 * @apiName GetDeviceParameters
 * @apiGroup Device
 * @apiParam (Param) {UUID} id Идентификатор модели оборудования
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: '31cb14f6-a421-4b58-85f2-90f643a50790',
 *          name: 'Серийный номер',
 *          alias: 'SERIAL_NUMBER'
 *          isRequired: true,
 *          type: 'STRING'
 *        }
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/:id/get-parameters',
  validator.params(DeviceController.validators.getDeviceModelParameters),
  asyncErrorHandler(DeviceController.getDeviceModelParameters),
);

/**
 * @api {get} /api/device/get-manufacturer-list Список производителей оборудования
 * @apiVersion 1.0.0
 * @apiName GetManufacturerList
 * @apiGroup Device
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: 'd57b4392-bfd8-48cb-8e54-a55b1be9e790',
 *          name: 'Искра',
 *        },
 *        {
 *          id: 'ed68c066-6970-4e5d-8ad7-2740768aaf39',
 *          name: 'VeriFone',
 *        },
 *        {
 *          id: '6ce58016-7e1a-4f86-a446-8b4d668535e9',
 *          name: 'СчетМаш',
 *        },
 *        {
 *          id: '45a863fb-8e42-4dd5-992c-f668d812a26f',
 *          name: 'Эвотор',
 *        },
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-manufacturer-list',
  asyncErrorHandler(DeviceController.getManufacturerList),
);

/**
 * @api {get} /api/device/type-manufacturer-model Дерево с типами, производителями и их моделями
 * @apiVersion 1.0.0
 * @apiName Device Type Manufacturer Model
 * @apiGroup Device
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *          name: 'ККТ',
 *          manufacturers: [
 *            {
 *              id: '45a863fb-8e42-4dd5-992c-f668d812a26f',
 *              name: 'Эвотор',
 *              models: [
 *                {
 *                  id: '47e6ec4e-89ce-4af5-8eed-bfb5b1af2239',
 *                  name: 'Э 7.0',
 *                },
 *                {
 *                  id: '54cb77b0-0f3b-4ed8-bfe3-4d7612aaafff',
 *                  name: 'Э 5.0',
 *                },
 *              ],
 *            },
 *            {
 *              id: 'ed68c066-6970-4e5d-8ad7-2740768aaf39',
 *              name: 'VeriFone',
 *              models: [
 *                {
 *                  id: 'bc14c5c1-0c09-427d-8f79-82f5c74483a9',
 *                  name: 'Vx520',
 *                },
 *              ],
 *            },
 *          ],
 *        },
 *        {
 *          id: '14eeadde-dbcb-44ac-9d72-e1cc11617c98',
 *          name: 'Поддержка АРМ',
 *          manufacturers: [],
 *        },
 *        {
 *          id: 'b57e113d-cb78-4313-a039-24e80d53a223',
 *          name: 'Не распределенные',
 *          manufacturers: [],
 *        },
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/type-manufacturer-model',
  asyncErrorHandler(DeviceController.getTypeManufacturerModel),
);

/**
 * @api {post} /api/device/delete-client-device Удаление оборудования
 * @apiVersion 1.0.0
 * @apiName DeleteClientDevice
 * @apiGroup Device
 * @apiParam (Body) {UUID} id UUID устройства.
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: null
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.post(
  '/delete-client-device',
  validator.body(DeviceController.validators.deleteClientDevice),
  asyncErrorHandler(DeviceController.deleteClientDevice),
);

/**
 * @api {post} /api/device/update-client-device Обновить оборудование
 * @apiVersion 1.0.0
 * @apiName UpdateClientDevice
 * @apiGroup Device
 * @apiParam (Body) {UUID} id UUID устройства организации.
 * @apiParam (Body) {Object[]} [parameters] Коллекция параметров.
 * @apiParam (Body) {String} parameters.id UUID параметра устройства организации.
 * @apiParam (Body) {String} parameters.value Значение параметра.
 * @apiParam (Body) {Object} [address] Адрес обслуживания устройства.
 * @apiParam (Body) {UUID} [address.region] Субъект РФ (если меняется субъект, то должен измениться город)
 * @apiParam (Body) {UUID} [address.cityId] Город из списка обслуживаемых городов
 * @apiParam (Body) {String} [address.street] Улица
 * @apiParam (Body) {String} [address.house] Дом
 * @apiParam (Body) {String} [address.apartment] Квартира/офис
 * @apiParam (Body) {String} [address.postalCode] Почтовый индекс
 * @apiParamExample {json} Пример запроса:
 * {
 *   id: 'bc14c5c1-0c09-427d-8f79-82f5c74483a9',
 *   parameters: [
 *     {
 *       id: '31cb14f6-a421-4b58-85f2-90f643a50790',
 *       value: '888555000123',
 *     },
 *     {
 *       id: 'd0dae0b0-0dd4-4c9b-8f70-8eb519a0cda1',
 *       value: '7.5 В, встроенный аккумулятор 2600 мАч',
 *     },
 *     {
 *       id: 'd2707b66-b9e8-4cf4-8600-d9db716dd32e',
 *       value: 'ARM 9 Zatara 180 МГц',
 *     },
 *   ],
 *   address: {
 *     region: '59637637-4bbc-49b8-91cc-58c2cb43e51e',
 *     cityId: '21918605-05f3-42e9-bde4-eaa44a5c8eb1',
 *     street: 'Невский пр.',
 *     house: '1',
 *     apartment: '123',
 *     postalCode: '000555',
 *   }
 * }
 *
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: null,
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */

router.post(
  '/update-client-device',
  validator.body(DeviceController.validators.updateClientDevice),
  asyncErrorHandler(DeviceController.updateClientDevice),
);

/**
 * @api {get} /api/device/:id/get-device-services Получение списка услуг привязанных к оборудованию
 * @apiVersion 1.0.0
 * @apiName GetDeviceServices
 * @apiGroup Device
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *         {
 *           id: "7db9aa02-7f43-4462-a677-0c1973ef462e",
 *           name: "Название услуги",
 *         }
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/:id/get-device-services',
  validator.params(DeviceController.validators.getDeviceServices),
  asyncErrorHandler(DeviceController.getDeviceServices),
);

export default router;
