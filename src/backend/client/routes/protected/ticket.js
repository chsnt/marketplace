import { Router } from 'express';

import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';
import validator from '../../../common/libs/requestValidator';
import TicketController from '../../controllers/TicketController';
import FileStorage from '../../../common/libs/multerFileStorage';

const router = new Router();

const DOCUMENTS_PATH = 'ticket';

const fileStorageInstance = new FileStorage({ storagePath: DOCUMENTS_PATH });

/**
 * @api {post} /api/ticket/create Создать заявку
 * @apiVersion 1.0.0
 * @apiName Create
 * @apiGroup Ticket
 * @apiParam (FormData) {String{..255}} description Описание к заявке
 * @apiParam (FormData) {String[]} services UUID услуги
 * @apiParam (FormData) {UUID} [serviceAddressId] UUID адреса
 * @apiParam (FormData) {Array} [documents] Прикреплённые файлы
 * @apiParam (FormData) {String} [nextCallDate] Дата звонка ('YYYY-MM-DD HH:mm:ss')
 * @apiParam (FormData) {String} [device] Новое оборудование с адресом обслуживания
 * @apiParam (FormData) {String} device.model UUID модели устройства
 * @apiParam (FormData) {Object[]} device.parameters Коллекция параметров
 * @apiParam (FormData) {String} device.parameters.id UUID параметра
 * @apiParam (FormData) {String} device.parameters.value Значение параметра
 * @apiParam (FormData) {Object} device.address Адрес обслуживания устройства
 * @apiParam (FormData) {UUID} device.address.region Субъект РФ
 * @apiParam (FormData) {UUID} device.address.cityId Город из списка обслуживаемых городов
 * @apiParam (FormData) {String} device.address.street Улица
 * @apiParam (FormData) {String} device.address.house Дом
 * @apiParam (FormData) {String} device.address.apartment Квартира/офис
 * @apiParam (FormData) {String} [device.address.postalCode] Почтовый индекс
 * @apiDescription В запросе должен быть, или id единоразовой услуги, или id услуги тарифа</br>
 *                  Принимает форму с заголовком Content-Type: multipart/form-data</br>
 *                  Если у услуг выбран адрес "При заказе", то нужно передавать serviceAddressId</br>
 *                  Если у услуг выбран адрес "При создании ЗНО", то нужно передавать serviceAddressId или device</br>
 *                  Если у услуг выбран адрес  "Не запрашивается", то ни serviceAddressId, ни device передавать не обязательно</br>
 * @apiParamExample {json} Пример запроса с единоразовой услуги
 *    {
 *      description: 'что хочу не знаю',
 *      service: 'f12d1350-8375-4f5e-8833-a1c0037cbd88',
 *      device: 'eab890c2-6bb6-4aab-87d3-c0d65783d711',
 *    }
 *
 * @apiParamExample {json} Пример запроса с услуги тарифа
 *    {
 *      description: 'почистить карму',
 *      service: 'f12d1350-8375-4f5e-8833-a1c0037cbd88',
 *    }
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      data: null,
 *      success: {
 *        id: '85919b25-46da-410b-b4cc-c72c90b9f587',
 *        serial: 'KKT:60',
 *        description: 'У кассы залипает кнопка и 50% чека не пропечатывается.',
 *        clientId: '1d0fcf5a-58dc-4584-b1c1-283283f841e2',
 *        ticketStatusId: 'f12d1350-8375-4f5e-8833-a1c0037cbd88',
 *        clientDeviceId: 'eab890c2-6bb6-4aab-87d3-c0d65783d711',
 *        createdAt: '2020-09-28T12:48:11.889Z',
 *      },
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.post(
  '/create',
  fileStorageInstance.upload.array('documents', 10),
  validator.body(TicketController.validators.createTicket),
  asyncErrorHandler(TicketController.createTicket),
);

/**
 * @api {get} /api/ticket/get-list Список заявок
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup Ticket
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: 'ce2cad40-e01a-4472-b1a4-c6c1a8c831ac',
 *          serial: 'KKT:60',
 *          solution: 'Чистка устройства',
 *          createdAt: '2020-11-08T21:05:07.403Z',
 *          resolvedAt: '2020-11-08T21:05:07.403Z',
 *          laborCosts: 17 | null, //трудозатраты
 *          services: [
 *            {
 *              id: '6fb4f5a4-5b47-40c5-ad2a-12657d8e56f8',,
 *              name: 'Услуга 1'
 *            }
 *          ],
 *          ticketStatus: {
 *            name: 'Выполнена',
 *          },
 *          serviceAddress: {
 *            street: 'Розовый бульвар',
 *            house: '77',
 *            apartment: '0',
 *            postalCode: '123123',
 *            city: {
 *              id: 'f1d705e6-76d5-4b31-9092-0181a5d60599',
 *              name: 'Бор',
 *              utcOffset: 3
 *            },
 *            region: {
 *              id: '276729a5-2e63-4586-a367-c3bb9a655b3e',
 *              name: 'Нижегородская область',
 *            },
 *          } | null,
 *          ticketReview: {
 *            text: 'Я доволен!!!!',
 *            mark: 7,
 *            ticketReviewDocuments: [
 *              {
 *                id: '5357990b-4e79-4969-88eb-d10b5c5bad61',
 *                name: 'milk_into_chocolate.jpg',
 *              },
 *            ],
 *          },
 *        },
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 *
 */
router.get(
  '/get-list',
  asyncErrorHandler(TicketController.getTicketList),
);

/**
 * @api {get} /api/ticket/get-list-with-documents Список заказов вместе с документами
 * @apiVersion 1.0.0
 * @apiName GetListDocuments
 * @apiGroup Ticket
 * @apiDescription Список заказов отсортированных по дате. В каждом заказе присутствую списки файлов и ссылки на скачивание.
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      "data": [
 *      {
 *        "id": "321347e6-9fa1-455f-b14c-ae7c22dc594e",
 *        "number": 4324679,
 *        "createdAt": "2020-09-22T10:08:48.000Z",
 *        "ticketCloseDocuments":
 *          [
 *            {
 *              "id": "333333a1-1aa1-425c-b14c-ae7c33dc666e",
 *              "name": "Счет №5 на предоставление услуг на перепрошивку онлайн-кассы",
 *              "link": "/api/ticket/document/333333a1-1aa1-425c-b14c-ae7c33dc666e"
 *            }
 *          ]
 *        },
 *        {
 *          "id": "321347e1-9fa1-425f-b14c-ae7c33dc584e",
 *          "number": 4317596,
 *          "createdAt": "2020-09-21T23:22:25.000Z",
 *          "ticketCloseDocuments":
 *          [
 *            {
 *              "id": "555777e1-9fa1-425f-b14c-ae7c33dc555e",
 *              "name": "Акт о выполненных работах",
 *              "link": "/api/ticket/document/555777e1-9fa1-425f-b14c-ae7c33dc555e"
 *            },
 *            {
 *              "id": "666777e1-9fa1-425f-b14c-ae7c33dc666e",
 *              "name": "Договор-оферта по оказанию услуг",
 *              "link": "/api/ticket/document/666777e1-9fa1-425f-b14c-ae7c33dc666e"
 *            }
 *          ]
 *        }
 *      ],
 *      "success": true,
 *      "error": null,
 *      "errorCode": null
 *    }
 */
router.get(
  '/get-list-with-documents',
  asyncErrorHandler(TicketController.getCloseDocumentsList),
);

/**
 * @api {post} /api/ticket/create-ticket-review Оставить отзыв заявке
 * @apiVersion 1.0.0
 * @apiName CreateTicketReview
 * @apiGroup Ticket
 * @apiParam (FormData) {String} ticket UUID заявки.
 * @apiParam (FormData) {String{..255}} description Отзыв.
 * @apiParam (FormData) {Number{0..10}} mark Оценка выполненной услуги.
 * @apiParam (FormData) {Array} documents Прикреплённые файлы.
 * @apiDescription Принимает форму с заголовком Content-Type: multipart/form-data
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "data": null,
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 *    }
 */
router.post(
  '/create-ticket-review',
  fileStorageInstance.upload.array('documents', 10),
  validator.body(TicketController.validators.createTicketReview),
  asyncErrorHandler(TicketController.createTicketReview),
);

/**
 * @api {post} /api/ticket/:id/get-ticket-review Получить отзыв по заявке
 * @apiVersion 1.0.0
 * @apiName GetTicketReview
 * @apiGroup Ticket
 * @apiParam (Params) {UUID} id UUID заявки.
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      data: {
 *        text: 'Я доволен!!!!',
 *        mark: 7,
 *        ticketReviewDocuments: [
 *          {
 *            id: '5357990b-4e79-4969-88eb-d10b5c5bad61',
 *            name: 'milk_into_chocolate.jpg',
 *          },
 *        ],
 *      },
 *      success: true,
 *      error: null,
 *      errorCode: null,
 *    }
 */
router.get(
  '/:id/get-ticket-review',
  validator.params(TicketController.validators.getTicketReview),
  asyncErrorHandler(TicketController.getTicketReview),
);

/**
 * @api {get} /api/ticket/document/:documentId/get-document Скачать документ
 * @apiVersion 1.0.0
 * @apiName GetDocument
 * @apiGroup Ticket
 * @apiParam (Params) {UUID} documentId ID документа
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      "data": null,
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 *    }
 */
router.get(
  '/document/:documentId/get-document',
  validator.params(TicketController.validators.getDocument),
  asyncErrorHandler(TicketController.getDocument),
);

export default router;
