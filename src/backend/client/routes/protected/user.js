import { Router } from 'express';

import UserController from '../../controllers/UserController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';
import validator from '../../../common/libs/requestValidator';
import AuthController from '../../controllers/AuthController';

const router = new Router();

/**
 * @api {get} /api/user/me/get-cart-and-notifications Получить корзину и уведомления
 * @apiVersion 1.0.0
 * @apiName GetCartAndNotifications
 * @apiGroup User
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      'data': {
 *        'notifications': [
 *          {
 *            'id': '800cd96b-cb52-4613-a6c2-b3711c39654b',
 *            'title': 'some title',
 *            'message': 'some message',
 *            'payload': {
 *              'key': 'value'
 *            },
 *            'createdAt': '2020-09-18T08:06:18.000Z',
 *            'isRead': false
 *          }
 *        ],
 *        'cart': {
 *          'id': '800cd96b-cb52-4613-a6c2-b3711c39654b',
 *          'number': 3,
 *          'orderElements': [
 *            {
 *              'id': '800cd96b-cb52-4613-a6c2-b3711c39654b',
 *              'element': {
 *                'id': '14eeadde-dbcb-44ac-9d72-e1cc11617c98',
 *                'name': 'test tariff 1',
 *                'cost': 300
 *              },
 *              'orderElementType': {
 *                'name': 'Тариф',
 *                'alias': 'TARIFF'
 *              }
 *            }
 *          ]
 *        }
 *      },
 *      'success': true,
 *      'error': 'error message' | null,
 *      'errorCode': 'ErrorCode' | null
 *    }
 */
router.get(
  '/me/get-cart-and-notifications',
  asyncErrorHandler(UserController.getCartAndNotifications),
);


/**
 * @api {get} /api/user/me/get-order-list Получить список заказов
 * @apiVersion 1.0.0
 * @apiName GetOrderList
 * @apiGroup User
 * @apiDescription Заказы со статусом оплачено и ожидает оплаты
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [{
 *        id: 'a6aadf31-6cdf-4042-8101-5d06ad9fd00a',
 *        number: 4,
 *        totalCost: 3303,
 *        status: {
 *          name: 'Оплачен',
 *          alias: 'PAID',
 *        },
 *        date: {
 *          created: '2020-11-15T20:11:24.024Z',
 *          paid: '2020-11-15T20:13:17.317Z',
 *        },
 *        services: [
 *          {
 *            name: 'Работы по активации ключа ОФД на срок действия договора',
 *            cost: 125, //текущая стоимость, если товар доступен
 *            clientServiceCost: 125, //цена покупки
 *            label: null,
 *            icon: null,
 *            alias: 'raboty-po-aktivacii-klyucha-ofd-na-srok-dejstviya-dogovora',
 *          },
 *        ],
 *        documents: [{
 *          id: '8d8ba2a3-92dd-4483-a49b-a1410077b0d3',
 *          name: 'Universalnii_peredatochnii_dokument_(UPD)_№_IT-1_ot_15.10.2020.pdf',
 *          date: '2020-11-12',
 *          type: {
 *            name: 'УПД (статус 1)',
 *            alias: 'UTD_STATUS_ONE',
 *          },
 *        },
 *        {
 *          id: '3554c29f-b262-470f-958a-d5fe1c00b8f0',
 *          name: 'Universalnii_peredatochnii_dokument_(UPD)_№_IT-1_ot_15.10.2020.pdf',
 *          date: '2020-11-12',
 *          type: {
 *            name: 'УПД (статус 1)',
 *            alias: 'UTD_STATUS_ONE',
 *          },
 *        }],
 *        tariffs: [
 *          {
 *            expirationAt: '2020-10-31T00:42:24.616Z',
 *            name: 'Все включено на полгода',
 *            cost: 7244, //текущая стоимость, если товар доступен
 *            clientTariffCost: 5000, //цена покупки
 *            label: null,
 *            icon: null,
 *            expirationMonths: 6,
 *            alias: 'vse-vklyucheno-6',
 *            description: 'large description',
 *            services: [
 *              {
 *                name: 'Гарантийный ремонт ККТ',
 *                label: null,
 *                icon: null,
 *                alias: 'garantijnyj-remont-kkt',
 *              },
 *              {
 *                name: 'Консультация технической поддержки',
 *                label: null,
 *                icon: null,
 *                alias: 'konsultaciya-tekhnicheskoj-podderzhki',
 *              },
 *            ],
 *          },
 *        ],
 *      }],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null
 *    }
 */
router.get(
  '/me/get-order-list',
  asyncErrorHandler(UserController.getOrderList),
);

/**
 * @api {get} /api/user/me/settings Получить пользовательские настройки
 * @apiVersion 1.0.0
 * @apiName GetUserSettings
 * @apiGroup User
 * @apiDescription Сервер возвращает информацию о пользователе и об его организации.
 * @apiParamExample Request-Example:
 *    http://host/api/user/me/settings/78666545-cfc0-417a-a951-43638d3aab76
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      data: {
 *        user: {
 *          firstName: 'Новый',
 *          lastName: 'ФЛ',
 *          patronymicName: 'Тестовый',
 *          email: 'v.test@gmail.com',
 *          phone: '79101239779',
 *          birthday: null
 *        },
 *        organization: {
 *          id: 'c7bbae9e-cdf0-4931-83cb-f3894de298dc',
 *          name: 'ООО Пироги',
 *          inn: '1111113333',
 *          kpp: '121333555',
 *          externalId: null,
 *          sbbidId: null,
 *          clientId: 'b0b42799-01c7-4c25-8a50-c9006ca86433',
 *          legalAddress: {
 *            city: 'Кольчугино',
 *            street: 'Тестовая',
 *            house: '34а',
 *            apartment: null,
 *            postalCode: null,
 *            id: 'f2ee8191-b269-48fb-8b9b-c1c461da6b99',
 *            region: {
 *              id: 'b281935e-8569-4052-80a1-0d8cdf0dd46d',
 *              name: 'Владимирская область'
 *            }
 *          },
 *          realAddress: {
 *            city: 'Владимир',
 *            street: 'Тестовая перетестовая',
 *            house: '34а',
 *            apartment: '123',
 *            postalCode: '352374',
 *            id: '61d74d72-3afe-4ba5-9b74-66baa1ec57ca',
 *            region: {
 *              id: 'b281935e-8569-4052-80a1-0d8cdf0dd46d',
 *              name: 'Владимирская область'
 *            }
 *          },
 *          electronicDocumentCirculation: {
 *            id: 'abe383da-dd10-4944-8ca7-96b5c55b7c18',
 *            name: 'Моего оператора нет в списке',
 *            alias: 'NOT_IN_LIST'
 *          },
 *          electronicDocumentCirculationDescription: {
 *            description: 'тестовый ЭДООООО'
 *          },
 *          taxType: {
 *            id: '9f10486d-5f51-4771-922f-c562ed333b36',
 *            name: 'Упрощенная (включая ЕНВД, ЕСХН, ПСН и НПД)',
 *            alias: 'USN'
 *          },
 *          digitalSignature: {
 *            from: '12.08.1257',
 *            to: '12.08.2157'
 *          },
 *          bank: {
 *            bankAccount: '22334445555665433432'
 *          }
 *        },
 *      },
 *      success: true,
 *      error: null,
 *      errorCode: null,
 *    }
 */
router.get(
  '/me/settings',
  asyncErrorHandler(UserController.getInfo),
);

/**
 * @api {post} /api/user/me/settings Обновления настроек
 * @apiVersion 1.0.0
 * @apiName UpdateUserSettings
 * @apiGroup User
 * @apiParam (Body) {Object} [user] Пользовательские данные
 * @apiParam (Body) {String} [user.firstName] Имя
 * @apiParam (Body) {String} [user.lastName] Фамилия
 * @apiParam (Body) {String} [user.patronymicName] Отчество
 * @apiParam (Body) {String} [user.email] Почта
 * @apiParam (Body) {String} [user.birthday] День рождения в формате 'DD.MM.YYYY
 * @apiParam (Body) {Number} [user.phone] Номер телефона
 * @apiParam (Body) {String} [user.oldPassword] Текущей пароль
 * @apiParam (Body) {String{6..15}} [user.password] Пароль не менее 6 и не более 15 символов
 * @apiParam (Body) {String} [user.confirmPassword] Если свойство password заполнено, то confirmPassword должен присутствовать
 *
 * @apiParam (Body) {Object} [organization] Данные организации
 * @apiParam (Body) {String{2..}} [organization.name] Название организации
 * @apiParam (Body) {String{10..12}} [organization.inn] ИНН
 * @apiParam (Body) {String{9}} [organization.kpp] КПП
 *
 * @apiParam (Body) {Object} [organization.digitalSignature] Срок действия ЭЦП
 * @apiParam (Body) {String} organization.digitalSignature.from Начало срока действия ЭЦП в формате 'YYYY-MM-DD'
 * @apiParam (Body) {String} organization.digitalSignature.to Окончание срока действия ЭЦП в формате 'YYYY-MM-DD'
 *
 * @apiParam (Body) {Object} [organization.legalAddress] Юридический адрес
 * @apiParam (Body) {UUID} [organization.legalAddress.region] Регион
 * @apiParam (Body) {String{2..}} [organization.legalAddress.city] Город
 * @apiParam (Body) {UUID} [organization.legalAddress.cityId] Город
 * @apiParam (Body) {String{2..}} [organization.legalAddress.street] Улица
 * @apiParam (Body) {UUID} [organization.legalAddress.fiasId] UUID фиас
 * @apiParam (Body) {String} [organization.legalAddress.house] Номер дома
 * @apiParam (Body) {String} [organization.legalAddress.apartment] Номер помещения
 * @apiParam (Body) {String} [organization.legalAddress.postalCode] Индекс
 *
 * @apiParam (Body) {Object} [organization.realAddress] Фактический адрес
 * @apiParam (Body) {UUID} [organization.realAddress.region] Регион
 * @apiParam (Body) {String{2..}} [organization.realAddress.city] Город
 * @apiParam (Body) {UUID} [organization.realAddress.cityId] Город
 * @apiParam (Body) {String{2..}} [organization.realAddress.street] Улица
 * @apiParam (Body) {String} [organization.realAddress.house] Номер дома
 * @apiParam (Body) {String} [organization.realAddress.apartment] Номер помещения
 * @apiParam (Body) {String} [organization.realAddress.postalCode] Индекс
 *
 * @apiParam (Body) {Object} [organization.bankDetail] Банковские реквизиты
 * @apiParam (Body) {String{2..100}} [organization.bankDetail.name] Название банка
 * @apiParam (Body) {String{20}} [organization.bankDetail.bankAccount] Расчетный счет
 * @apiParam (Body) {String{20}} [organization.bankDetail.correspondentAccount] Кор. счет
 * @apiParam (Body) {String{9}} [organization.bankDetail.rcbic] БИК
 * @apiParam (Body) {UUID} [organization.bankDetail.taxType] Система налогообложения
 * @apiParam (Body) {UUID} [organization.electronicDocumentCirculationId] Оператор электронного документа оборота
 * @apiParam (Body) {String{..1024}} [organization.electronicDocumentCirculationDescription] Если оператора нет, нужно указать его в описании
 *
 * @apiDescription При использовании временного пароля для сброса основного пароля доступны только `user.password` и `user.confirmPassword`
 *
 * @apiParamExample Request-Example:
 *    http://host/api/user/me/settings
 *    {
 *      user: {
 *        firstName: 'ИваньКо',
 *        patronymicName: 'Патронович',
 *        password: '123456',
 *        confirmPassword: '123456'
 *      },
 *      bankDetail: {
 *        taxType: 'ENVD'
 *      },
 *      realAddress: {
 *        region: 'Якутия'
 *      }
 *    }
 *
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      data: {
 *        user: {
 *          firstName: 'Новый',
 *          lastName: 'ФЛ',
 *          patronymicName: 'Тестовый',
 *          email: 'v.test@gmail.com',
 *          phone: '79101239779',
 *          birthday: null
 *        },
 *        organization: {
 *          id: 'c7bbae9e-cdf0-4931-83cb-f3894de298dc',
 *          name: 'ООО Пироги',
 *          inn: '1111113333',
 *          kpp: '121333555',
 *          externalId: null,
 *          sbbidId: null,
 *          clientId: 'b0b42799-01c7-4c25-8a50-c9006ca86433',
 *          legalAddress: {
 *            city: 'Кольчугино',
 *            street: 'Тестовая',
 *            house: '34а',
 *            apartment: null,
 *            postalCode: null,
 *            id: 'f2ee8191-b269-48fb-8b9b-c1c461da6b99',
 *            region: {
 *              id: 'b281935e-8569-4052-80a1-0d8cdf0dd46d',
 *              name: 'Владимирская область'
 *            }
 *          },
 *          realAddress: {
 *            city: 'Владимир',
 *            street: 'Тестовая перетестовая',
 *            house: '34а',
 *            apartment: '123',
 *            postalCode: '352374',
 *            id: '61d74d72-3afe-4ba5-9b74-66baa1ec57ca',
 *            region: {
 *              id: 'b281935e-8569-4052-80a1-0d8cdf0dd46d',
 *              name: 'Владимирская область'
 *            }
 *          },
 *          electronicDocumentCirculation: {
 *            id: 'abe383da-dd10-4944-8ca7-96b5c55b7c18',
 *            name: 'Моего оператора нет в списке',
 *            alias: 'NOT_IN_LIST'
 *          },
 *          electronicDocumentCirculationDescription: {
 *            description: 'тестовый ЭДООООО'
 *          },
 *          taxType: {
 *            id: '9f10486d-5f51-4771-922f-c562ed333b36',
 *            name: 'Упрощенная (включая ЕНВД, ЕСХН, ПСН и НПД)',
 *            alias: 'USN'
 *          },
 *          digitalSignature: {
 *            from: '12.08.1257',
 *            to: '12.08.2157'
 *          },
 *          bank: {
 *            bankAccount: '22334445555665433432'
 *          }
 *        },
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null
 *    }
 */
router.post(
  '/me/settings',
  validator.body(UserController.validators.updateUser),
  asyncErrorHandler(UserController.updateUser),
);

/**
 * @api {get} /api/user/me/get-completed-list Список завершенных тарифов
 * @apiVersion 1.0.0
 * @apiName GetCompletedList
 * @apiGroup User
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: {
 *        tariffs: [
 *          {
 *            device: {
 *              id: 'b6affe31-a92a-412c-a954-4a8532683ba8',
 *              type: 'ККТ',
 *              alias: 'KKT,
 *              manufacturer: 'Атол',
 *              model: '15Ф (мобильный)',
 *              parameters: [
 *                {
 *                  value: '888yyy0123',
 *                  name: 'Серийный номер',
 *                  alias: 'SERIAL_NUMBER'
 *                },
 *              ],
 *            },
 *            serviceAddress: {
 *                id: '4ca45504-2b86-42e9-90f8-39c29a5a63d9',
 *                street: 'улица Ленина',
 *                house: '1',
 *                apartment: '123',
 *                postalCode: '555888',
 *                fiasId: null,
 *                city: {
 *                  id: 'a02cd33a-b7de-41c6-b191-b41168054bc1',
 *                  name: 'Зеленоградск',
 *                  utcOffset: 0,
 *                },
 *                region: {
 *                  id: '4680dce7-ac13-410c-8b7d-a467845fe7ed',
 *                  name: 'Республика Удмуртия',
 *                },
 *              },
 *            tariff: {
 *              id: '9c1c86ad-7a0d-4a8a-a2f1-8ee859bc9e68',
 *              clientTariffId: '957c99e2-1c19-40f7-890c-c3a087e0c5dd',
 *              name: 'Удаленная помощь+ на год',
 *              alias: 'udalennaya-pomoshch-plyus-12',
 *              label: null,
 *              icon: null,
 *              expirationAt: '2019-10-01T00:00:00.000Z',
 *              services: [
 *                {
 *                  id: '51e9de63-1aed-46a5-bc3e-71b9a8b7e49a',
 *                  value: null, //всего
 *                  progress: 0, //в процессе выполнения (есть ЗНО
 *                  used: 17,//использовано
 *                  service: {
 *                    id: '85db8d1a-d35d-43c3-91b1-7290be621af6',
 *                    name: 'Консультация технической поддержки',
 *                    alias: 'konsultaciya-tekhnicheskoj-podderzhki',
 *                    label: null,
 *                    icon: null,
 *                    limit: null,
 *                    limitMeasure: 'Количество работ в месяц',
 *                    serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *                  },
 *                },
 *                {
 *                  id: 'e1c282ae-c8d9-4bb7-b73b-44fbc34c4829',
 *                  value: 0,
 *                  progress: 1,
 *                  used: 0,
 *                  service: {
 *                    id: '38dc53a1-dbb8-4037-826e-97deb43a6dd5',
 *                    name: 'Контроль сроков действия ФН и договора ОФД',
 *                    alias: 'kontrol-srokov-dejstviya-fn-i-dogovora-ofd',
 *                    label: null,
 *                    icon: null,
 *                    limit: 1,
 *                    limitMeasure: 'Количество работ в месяц',
 *                    serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *                  },
 *                },
 *                {
 *                  id: '64f91fd3-da92-4b01-b703-9ddf2389cd24',
 *                  value: 5,
 *                  progress: 0,
 *                  used: 1,
 *                  service: {
 *                    id: '354edd5a-d5e4-4738-b7a9-81a5eb981594',
 *                    name: 'Мониторинг передачи чеков в ОФД',
 *                    alias: 'monitoring-peredachi-chekov-v-ofd',
 *                    label: null,
 *                    icon: null,
 *                    limit: 6,
 *                    limitMeasure: 'Количество работ в месяц',
 *                    serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *                  },
 *                },
 *              ],
 *            },
 *          },
 *        ],
 *        services: [
 *          {
 *            clientServiceId: '7b4dbc80-6f11-45c0-94ff-76b66a3c804c',
 *            completed: '2019-07-20T03:13:37.000Z',
 *            id: '514ebac5-9c00-4692-ae86-820961348cd8',
 *            name: 'Заключение договора с ОФД на 15 месяцев',
 *            alias: 'zaklyuchenie-dogovora-s-ofd-na-15-mes',
 *            label: null,
 *            icon: 'iconPossTerminal',
 *            serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *            device: {
 *              id: 'c978925d-c45d-4696-9f93-e3dff42500bc',
 *              type: 'Кассы',
 *              manufacturer: 'Штрих-М',
 *              model: 'ЭЛВЕС-МФ (мод. 02, корпус 'Микро')',
 *              parameters: [
 *                {
 *                  value: '888xyz888',
 *                  name: 'Серийный номер',
 *                  alias: 'SERIAL_NUMBER'
 *                },
 *              ],
 *              address: {
 *                street: 'улица Ленина',
 *                house: '1',
 *                apartment: '123',
 *                postalCode: '555888',
 *                city: 'Ижевск',
 *                region: 'Республика Удмуртия',
 *              },
 *            } | null,
 *          },
 *        ]
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/me/get-completed-list',
  asyncErrorHandler(UserController.getCompletedList),
);

/**
 * @api {post} /api/user/me/get-mail-confirmation-url Запрос на подтверждения почты
 * @apiVersion 1.0.0
 * @apiName GetMailConfirmationUrl
 * @apiGroup User
 * @apiDescription Пользователю на почту приходит ссылка http://localhost:3333/api/user/me/confirmation-email?codeMail=C4DH9D684FH8FBJ
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      data: null,
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null
 *    }
 */
router.get(
  '/me/get-mail-confirmation-url',
  asyncErrorHandler(AuthController.sendEmailConfirmationUrl),
);

export default router;
