import { Router } from 'express';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';
import ClientController from '../../controllers/ClientController';
import UserController from '../../controllers/UserController';

const router = new Router();

/**
 * @api {get} /api/client/allowed-services Список доступных услуг клиента для создания ЗНО
 * @apiVersion 1.0.0
 * @apiName GetAllowedServices
 * @apiGroup Client
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *     data: [
 *       {
 *         serviceAddressType: 'inOrder',
 *         services: [
 *           {
 *             id: '85db8d1a-d35d-43c3-91b1-7290be621af6',
 *             name: 'Неограниченные консультации службы технической поддержки',
 *             alias: 'konsultaciya-tekhnicheskoj-podderzhki',
 *             deviceType: 'kkt',
 *             serviceAddressType: 'inOrder',
 *             value: null,
 *           },
 *           {
 *             id: '64da2bbc-5519-4b34-baad-3930c48f441f',
 *             name: 'Регистрация онлайн-кассы',
 *             alias: 'registratsiya-onlajn-kassy',
 *             deviceType: 'kkt',
 *             serviceAddressType: 'inOrder',
 *             value: 5,
 *           },
 *         ],
 *         serviceAddress: {
 *           id: '29ecb4f9-e89d-460a-95c2-bbbb84eb0688',
 *           street: 'Уральская',
 *           house: '17',
 *           apartment: null,
 *           postalCode: null,
 *           fiasId: null,
 *           city: {
 *             id: '88903722-56be-46e3-8b32-94737856812f',
 *             name: 'Барнаул',
 *             utcOffset: 0,
 *           },
 *           region: {
 *             id: 'd5ec8435-8910-479c-ba60-c3919fce1ac9',
 *             name: 'Алтайский край',
 *           },
 *         },
 *       },
 *       {
 *         serviceAddressType: 'inOrder',
 *         services: [
 *           {
 *             id: '8d9d7dea-8e88-4139-9097-442286fcc1d7',
 *             name: 'Снятие с учета онлайн-кассы',
 *             alias: 'snyatie-onlajn-kassy-s-ucheta',
 *             deviceType: 'kkt',
 *             serviceAddressType: 'inOrder',
 *             value: 8,
 *           },
 *           {
 *             id: '38dc53a1-dbb8-4037-826e-97deb43a6dd5',
 *             name: 'Контроль сроков действия фискального накопителя (ФН) и договора ОФД',
 *             alias: 'kontrol-srokov-dejstviya-fn-i-dogovora-ofd',
 *             deviceType: 'kkt',
 *             serviceAddressType: 'inOrder',
 *             value: 1,
 *           },
 *         ],
 *         serviceAddress: {
 *           id: '66ecb6f6-e66d-660a-95c2-bbbb84eb0666',
 *           street: 'Красная',
 *           house: '25',
 *           apartment: null,
 *           postalCode: null,
 *           fiasId: null,
 *           city: {
 *             id: '88903722-56be-46e3-8b32-94737856812f',
 *             name: 'Москва',
 *             utcOffset: 0,
 *           },
 *           region: {
 *             id: 'd5ec8435-8910-479c-ba60-c3919fce1ac9',
 *             name: 'Москва',
 *           },
 *         },
 *       },
 *       {
 *         serviceAddressType: 'notRequested',
 *         services: [
 *           {
 *             id: '923a5fab-1c6d-466d-9df3-068d2b2f58b2',
 *             name: 'Код активации для продления ОФД на 15 месяцев',
 *             alias: 'kod-aktivatsii-ofd-15-mes',
 *             deviceType: 'kkt',
 *             serviceAddressType: 'notRequested',
 *             value: 2,
 *           },
 *         ],
 *         serviceAddress: null,
 *       },
 *       {
 *         serviceAddressType: 'inTicket',
 *         services: [
 *           {
 *             id: '6571bc04-2ddb-49df-a4b9-c8e2e206fbf9',
 *             name: 'Техосмотр ноутбука или компьютера',
 *             alias: 'diagnostika-pk',
 *             deviceType: 'arm',
 *             serviceAddressType: 'inTicket',
 *             value: 6,
 *           },
 *         ],
 *         serviceAddress: null,
 *       },
 *     ],
 *     success: true,
 *     error: null,
 *     errorCode: null,
 *   }
 */
router.get(
  '/allowed-services',
  asyncErrorHandler(ClientController.getClientAllowedServices),
);

/**
 * @api {get} /api/client/purchased-tariffs-and-services Получить список подключенных услуг и тарифов с привязкой к адресу и устройству
 * @apiVersion 1.0.0
 * @apiName getDeviceTariffsAndServices
 * @apiGroup User
 * @apiDescription Отображаются только оплаченные тарифы и услуги. Если у тарифа закончился 'срок годности', он не отображается в списке
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *   {
 *     data: {
 *       tariffs: [
 *         {
 *           serviceAddress: {
 *             id: '4ca45504-2b86-42e9-90f8-39c29a5a63d9',
 *             street: 'улица Ленина',
 *             house: '1',
 *             apartment: '123',
 *             postalCode: '555888',
 *             fiasId: null,
 *             city: {
 *               id: 'a02cd33a-b7de-41c6-b191-b41168054bc1',
 *               name: 'Зеленоградск',
 *               utcOffset: 0,
 *             },
 *             region: {
 *               id: '4680dce7-ac13-410c-8b7d-a467845fe7ed',
 *               name: 'Республика Удмуртия',
 *             },
 *           },
 *           device: {
 *             id: 'd82bca88-9359-40ec-b7c0-baaf405ab9d6',
 *             type: 'ККТ',
 *             alias: 'KKT',
 *             manufacturer: 'Эвотор',
 *             model: 'Эвотор 7.2',
 *             parameters: [
 *               {
 *                 value: '12',
 *                 name: 'Гарантия',
 *                 alias: 'GARANTIYAM',
 *               },
 *               {
 *                 value: 'Да',
 *                 name: 'Работа от аккумулятора',
 *                 alias: 'RABOTA-OT-AKKUMULYATORA',
 *               },
 *               {
 *                 value: 'Нет',
 *                 name: 'Сенсорный дисплей',
 *                 alias: 'SENSORNYJ-DISPLEJ',
 *               },
 *               {
 *                 value: 'zzz888yyy000123',
 *                 name: 'Серийный номер',
 *                 alias: 'SERIAL_NUMBER',
 *               },
 *             ],
 *           },
 *           tariff: {
 *             id: '779b0a45-590e-41ca-9f1f-e4562f97bfc5',
 *             clientTariffId: 'd5be0405-7059-41d1-ad8f-0dba18616b1c',
 *             name: 'Удаленная помощь на полгода',
 *             alias: 'udalennaya-pomoshch-6',
 *             label: null,
 *             icon: null,
 *             expirationAt: '2021-08-26T00:00:00.000Z',
 *             services: [
 *             {
 *                 id: 'beb0c0b4-f8b8-4494-afeb-ddab7a0f6110',
 *                 value: null, //всего
 *                 progress: 0, //в процессе выполнения (есть ЗНО)
 *                 used: 0, //использовано
 *                 service: {
 *                   id: '85db8d1a-d35d-43c3-91b1-7290be621af6',
 *                   name: 'Неограниченные консультации службы технической поддержки',
 *                   alias: 'konsultaciya-tekhnicheskoj-podderzhki',
 *                   label: null,
 *                   icon: null,
 *                   deviceTypeId: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *                   serviceAddressType: 'inOrder', // доступны inOrder, inTicket, notRequested
 *                   limit: null,
 *                   limitMeasure: 'Количество работ в месяц',
 *                 },
 *               },
 *               {
 *                 id: 'f517d269-0dd3-489a-9d99-1a0dc182b480',
 *                 value: 1,
 *                 progress: 1,
 *                 used: 0,
 *                 service: {
 *                   id: '64da2bbc-5519-4b34-baad-3930c48f441f',
 *                   name: 'Регистрация онлайн-кассы',
 *                   alias: 'registratsiya-onlajn-kassy',
 *                   label: null,
 *                   icon: 'iconPossTerminalSuccess',
 *                   deviceTypeId: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *                   serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *                   limit: 1,
 *                   limitMeasure: 'Количество работ в месяц',
 *                 },
 *               }
 *             ],
 *           },
 *         }
 *       ],
 *       services: [
 *         {
 *           clientServiceId: '97b0d1b3-93ba-4c21-8cf2-de7925dacea6',
 *           completed: null,
 *           id: '923a5fab-1c6d-466d-9df3-068d2b2f58b2',
 *           name: 'Код активации для продления ОФД на 15 месяцев',
 *           alias: 'kod-aktivatsii-ofd-15-mes',
 *           label: null,
 *           icon: 'iconOfd',
 *           serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *           deviceTypeId: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *           serviceAddress: {
 *             id: '7ebdec1f-edd6-4c9c-943d-1c70d8db5ffa',
 *             street: 'улица Ленина',
 *             house: '1',
 *             apartment: '123',
 *             postalCode: '555888',
 *             fiasId: null,
 *             city: {
 *               id: 'fbfc5bc7-6a43-4b07-b2b7-17960dc81bc9',
 *               name: 'Саров',
 *               utcOffset: 0,
 *             },
 *             region: {
 *               id: '4680dce7-ac13-410c-8b7d-a467845fe7ed',
 *               name: 'Республика Удмуртия',
 *             },
 *           } | null,
 *           device: {
 *             id: 'e60bc9ea-577c-401d-85c1-7ed964d5fe51',
 *             type: 'Поддержка АРМ',
 *             alias: 'ARM',
 *             model: 'ПК',
 *             parameters: [
 *               {
 *                 value: 'Asus',
 *                 name: 'Производитель',
 *                 alias: 'MANUFACTURER',
 *               },
 *               {
 *                 value: 'Zver',
 *                 name: 'Модель',
 *                 alias: 'MODEL',
 *               },
 *               {
 *                 value: 'Windows10',
 *                 name: 'Операционная система',
 *                 alias: 'OPERATION_SYSTEM',
 *               },
 *             ],
 *           } | null,
 *         }
 *       ],
 *     },
 *     success: true,
 *     error: 'error message' | null,
 *     errorCode: 'ErrorCode' | null,
 *   }
 *
 */
router.get(
  '/purchased-tariffs-and-services',
  asyncErrorHandler(UserController.getDeviceTariffsAndServices),
);

export default router;
