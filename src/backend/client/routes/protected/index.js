import { Router } from 'express';
import user from './user';
import notification from './notification';
import supportRequest from './supportRequest';
import order from './order';
import ticket from './ticket';
import device from './device';
import client from './client';

const router = new Router();

router.use('/user', user);
router.use('/notification', notification);
router.use('/support-request', supportRequest);
router.use('/order', order);
router.use('/ticket', ticket);
router.use('/device', device);
router.use('/client', client);

export default router;
