import { Router } from 'express';
import validator from '../../../common/libs/requestValidator';

import NotificationController from '../../controllers/NotificationController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';

const router = new Router();

/**
 * @api {post} /api/notification/read Пометить уведомления как прочитанные
 * @apiVersion 1.0.0
 * @apiName Read
 * @apiGroup Notification
 * @apiParam (Body) {String[]} notificationIds UUID
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      "data": null
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 *    }
 */
router.post(
  '/read',
  validator.body(NotificationController.validators.readNotifications),
  asyncErrorHandler(NotificationController.readNotifications),
);

export default router;
