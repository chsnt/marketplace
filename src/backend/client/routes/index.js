import { Router } from 'express';

import auth from './auth';
import organization from './organization';
import device from './device';
import tariff from './tariff';
import service from './service';
import requestPhoneCall from './requestPhoneCall';
import city from './city';
import taxType from './taxType';
import order from './order';
import edo from './edo';
import user from './user';
import protectedRoutes from './protected';
import { isAuthenticated } from '../../common/utils/middlewares';
import ERRORS from '../constants/errors';
import is from '../../common/libs/is';
import autotest from './autotest';

const router = new Router();

router.use('/auth', auth);
router.use('/device', device);
router.use('/organization', organization);
router.use('/tariff', tariff);
router.use('/service', service);
router.use('/request-phone-call', requestPhoneCall);
router.use('/city', city);
router.use('/tax-type', taxType);
router.use('/order', order);
router.use('/edo', edo);
router.use('/user', user);

if (is.env.development() || is.env.test()) {
  router.use('/autotest', autotest);
}

router.use('/', isAuthenticated, protectedRoutes);

/* non - existing routes */
router.use('/', (req, res, next) => {
  res.status(ERRORS.COMMON.ROUTE_NOT_FOUND.statusCode).json(
    null, false, ERRORS.COMMON.ROUTE_NOT_FOUND.error, ERRORS.COMMON.ROUTE_NOT_FOUND.errorCode,
  );
  next();
});

export default router;
