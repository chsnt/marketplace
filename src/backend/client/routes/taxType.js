import { Router } from 'express';

import TaxTypeController from '../../common/controllers/TaxTypeController';
import asyncErrorHandler from '../../common/libs/asyncErrorHandler';

const router = new Router();

/**
 * @api {get} /api/tax-type/get-list Получить список типов налогообложения
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup Tax Type
 * @apiSuccessExample {json} Пример ответа:
 *  HTTP/1.1 200 OK
 *  {
 *    data: [
 *      {
 *        id: 'ad24d66f-eda3-4844-a1de-1c6d055bb4b8',
 *        name: 'Общая система налогообложения (ОСНО)',
 *        alias: 'OSNO',
 *      }
 *      {
 *        id: '9f10486d-5f51-4771-922f-c562ed333b36',
 *        name: 'Упрощенная система налогообложения (УСН)',
 *        alias: 'USN'
 *      }
 *    ],
 *    success: true,
 *    error: 'error message' | null,
 *    errorCode: 'ErrorCode' | null
 *  }
 */
router.get(
  '/get-list',
  asyncErrorHandler(TaxTypeController.getList),
);

export default router;
