import { Router } from 'express';


import RequestPhoneCallController from '../controllers/RequestPhoneCallController';
import asyncErrorHandler from '../../common/libs/asyncErrorHandler';
import validator from '../../common/libs/requestValidator';

const router = new Router();

/**
 * @api {post} /api/request-phone-call Оставить заявку на обратный звонок.
 * @apiVersion 1.0.0
 * @apiName RequestPhoneCall
 * @apiGroup Request Phone Call
 * @apiDescription Создание заявки.
 * @apiParam {String} name Имя заказавшего обратный звонок.
 * @apiParam {Number{79000000000-79999999999}} phone Номер телефона.
 * @apiParam {Boolean} confirmation=false Подтверждение на обработку данных.
 * @apiParam {String} [email] Электронный адрес. Если пользователь не указал почту, то параметра не должно быть. Значение null или иное будет считаться как значение и проходить проверку на валидность.
 * @apiParam {String} [formId] Идентификатор формы
 * @apiParamExample {json} Request-Example:
 * {
 *     name: 'Вася Пупкин',
 *     number: '70001112233',
 *     confirmation: true,
 *     email: 'vasy@pyp.com'
 *     formId: 'callback-main'
 * }
 *
 * @apiSuccessExample Пример ответа:
 * HTTP/1.1 200 OK
 * {
 *   data: '459c75cb-db71-4788-bef0-77d5d9a5c3c7',
 *   success: true,
 *   error: null,
 *   errorCode: null
 * }
 *
 */

router.post(
  '/',
  validator.body(RequestPhoneCallController.validators.createNewRequest),
  asyncErrorHandler(RequestPhoneCallController.createNewRequest),
);

export default router;
