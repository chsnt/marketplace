import CityControllerCommon from '../../common/controllers/CityController';
import CityService from '../../common/services/CityService';

export default class CityController extends CityControllerCommon {
  static async getList(req, res) {
    const list = await CityService.getList(req.query);

    const result = {
      list,
      current: null,
    };

    return res.json(result);
  }
}
