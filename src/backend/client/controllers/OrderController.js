import Joi from '@hapi/joi';
import moment from 'moment';
import OrderService from '../services/OrderService';
import PaymentService from '../services/PaymentService';
import DeviceService from '../../common/services/DeviceService';
import {
  checkRequiredId, limitOffset, checkTariffsOrServices, checkId, createClientDevice,
} from '../../common/utils/validation';
import MailService from '../../common/services/MailService';
import OrderControllerCommon from '../../common/controllers/OrderController';
import { INTEGRATOR, SENDPULSE } from '../../common/constants/env';
import MailClient from '../../common/libs/MailClient';
import UserService from '../services/UserService';
import RabbitClient from '../../common/libs/RabbitClient';
import logger from '../../common/services/LoggerService';
import APIError from '../../common/utils/APIError';
import ERRORS from '../constants/errors';
import CouponService from '../../common/services/CouponService';
import { ServiceModel, TariffModel } from '../../common/database/models/Tariff';
import ClientService from '../../common/services/ClientService';
import is from '../../common/libs/is';

const toCart = Joi.object({
  tariffs: checkTariffsOrServices,
  services: checkTariffsOrServices,
}).or('services', 'tariffs');


export default class OrderController extends OrderControllerCommon {
  static get validators() {
    return {
      ...super.validators,
      limitOffset: Joi.object(limitOffset),
      addToCart: toCart,
      removeFromCart: toCart,
      getDocumentList: Joi.object({ id: checkRequiredId }),
      getDocument: Joi.object({ documentId: checkRequiredId }),
      payment: Joi.object({
        order: checkRequiredId,
        paymentType: Joi.string().valid('CARD', 'INVOICE'),
        totalCost: Joi.number().required(),
        cost: Joi.number().required(),
      }),
      checkPayment: Joi.object({
        orderId: checkRequiredId,
        acquiringOrderNumber: Joi.string().required(),
      }),
      createOrderFromCart: Joi.object({
        paymentType: Joi.string().required().valid('CARD', 'INVOICE'),
        description: Joi.string().allow('', null),
        order: Joi.array().items(Joi.object({
          serviceAddressId: checkId,
          tariff: checkId,
          services: checkTariffsOrServices.unique(),
          newDevice: Joi.object(createClientDevice),
        }).oxor('serviceAddressId', 'newDevice').or('tariff', 'services')),
      }),
      addCoupon: Joi.object({
        code: Joi.string().required().min(1).max(255),
      }),
    };
  }

  static async addToCart(req, res) {
    await OrderService.addToCart({
      ...req.body,
      clientId: req.user.clientId,
    });

    const result = await OrderService.getCart(req.user.clientId);
    return res.json(result);
  }

  static async getCart(req, res) {
    const result = await OrderService.getCart(req.user.clientId);
    return res.json(result);
  }

  static async removeFromCart(req, res) {
    const result = await OrderService.removeFromCart({
      ...req.body,
      clientId: req.user.clientId,
    });
    return res.json(result);
  }

  static async createOrderFromCart(req, res) {
    const { user: { clientId, id: userId }, body: { description, paymentType } } = req;
    try {
      const clientType = await ClientService.getClientType(clientId);
      const preparedOrder = await Promise.all(req.body.order.map(async (orderItem) => {
        let serviceAddressId = orderItem.serviceAddressId || '';

        if (orderItem.newDevice) {
          const result = await DeviceService.createClientDevice({
            device: orderItem.newDevice,
            clientId,
          });

          serviceAddressId = result.serviceAddress.id;
        }
        return { ...orderItem, serviceAddressId };
      }));

      const isFirstOrder = await OrderService.checkFirstOrder({ clientId });

      const result = await OrderService.createOrderFromCart({
        orders: preparedOrder,
        clientId,
        paymentType,
        clientType,
        description,
      });

      /* Этой ошибки не будет в боевых условиях, но из постмана можно поймать непонятную 500-ку */
      if (!result) throw new APIError(ERRORS.ORDER.CART_DOES_NOT_EXISTS);

      const { order, formUrl, offer } = result;

      const counterparty = await UserService.getInfo(userId);

      const {
        id: orderId,
        number: orderNumber,
        orderDate,
        clientTariffs,
        clientServices,
      } = order;

      if (is.payments.types.alias.invoice(paymentType)) {
        try {
          const mailData = await MailService.prepareInvoiceForUser({
            clientTariffs,
            clientServices,
          });

          const template = MailService.createInvoiceTemplate(mailData.goods);

          await MailClient.order({
            name: 'issuedAndNotPaid',
            recipients: [{
              email: SENDPULSE.MAIL_RECIPIENT,
            }, {
              name: `${counterparty.user.lastName} ${counterparty.user.firstName} ${counterparty.user.patronymicName}`,
              email: counterparty.user.email,
            }],
            variables: {
              orderNumber,
              totalCost: mailData.totalCost,
              data: template,
            },
          });
        } catch (error) {
          logger.error(error);
        }

        if (INTEGRATOR.IS_SENDING_ENABLED) {
          const services = clientServices.map((service) => ({
            externalId: service.service.externalId,
            cost: ServiceModel.getCurrentCost(service.costs),
            clientDeviceId: service.clientDeviceId,
          }));
          const tariffs = clientTariffs.map((tariff) => ({
            externalId: tariff.tariff.externalId,
            cost: TariffModel.getCurrentCost(tariff.costs),
            clientDeviceId: tariff.clientDeviceId,
          }));

          try {
            const rabbitClient = new RabbitClient();
            await rabbitClient.sendToQueue({
              queue: rabbitClient.queues.integrationCreateOrder,
              message: {
                orderId,
                orderNumber,
                clientId,
                isFirstOrder,
                userId,
                offer,
                createdAt: moment(orderDate).format('YYYY.MM.DD.HH.mm.ss'),
                tariffs,
                services,
              },
            });
          } catch (error) {
            logger.error(error);
          }
        }
      }

      return res.json({ orderId, formUrl });
    } catch (error) {
      if (!(error instanceof APIError)) throw new APIError(ERRORS.ORDER.CREATE_ORDER, { originalError: error });
      throw error;
    }
  }

  static async checkPayment(req, res) {
    const result = await PaymentService.checkPayment(req.body.orderId, req.body.acquiringOrderNumber);

    return res.json(result);
  }

  static async getDocumentList(req, res) {
    const result = await OrderService.getDocumentList(req.params.id);

    return res.json(result);
  }

  static async resetOrderToCart(req, res) {
    await OrderService.resetOrderToCart(req.query.orderId);

    return res.redirect('/basket');
  }

  static async completeOrder(req, res) {
    await super.completeOrder(req.query.orderId);
    return res.redirect('/private-office');
  }

  static async addCoupon(req, res) {
    const coupon = await CouponService.getCoupon({ ...req.body, ...req.user });
    await OrderService.addCoupon({ ...coupon, ...req.user });
    const result = await OrderService.getCart(req.user.clientId);
    return res.json(result);
  }

  static async removeCoupon(req, res) {
    await OrderService.removeCoupon({ ...req.user });
    const result = await OrderService.getCart(req.user.clientId);
    return res.json(result);
  }
}
