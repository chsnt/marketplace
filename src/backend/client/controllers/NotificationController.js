import Joi from '@hapi/joi';
import { checkId } from '../../common/utils/validation';
import NotificationService from '../services/NotificationService';

export default class NotificationController {
  static get validators() {
    return {
      readNotifications: Joi.object({
        notificationIds: Joi.array().items(checkId).required(),
      }),
    };
  }

  static async readNotifications(req, res) {
    await NotificationService.readNotifications({
      clientId: req.user.clientId,
      notificationIds: req.body.notificationIds,
    });
    res.json(null);
  }
}
