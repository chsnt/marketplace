import Joi from '@hapi/joi';

import LeadService from '../../common/services/LeadService';

import { checkPhone } from '../../common/utils/validation';


export default class {
  static get validators() {
    return {
      createNewRequest: Joi.object({
        firstName: Joi.string().required(),
        phone: checkPhone().required(),
        confirmation: Joi.boolean().valid(true).required(),
        email: Joi.string().email(),
        formId: Joi.string().allow('', null),
      })
        .rename('number', 'phone')
        .rename('name', 'firstName'),
    };
  }

  /* Запрос на обратный звонок - это новый лид в системе */
  static async createNewRequest(req, res) {
    const result = await LeadService.createLead(req.body);
    return res.json(result.id);
  }
}
