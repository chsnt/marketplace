import Joi from '@hapi/joi';

import bcrypt from 'bcrypt';
import AuthService from '../services/AuthService';
import UserService from '../services/UserService';
import {
  checkPhone, checkRequiredId, organization, userRegistration,
} from '../../common/utils/validation';
import { authMiddleware, authOpenIDMiddleware } from '../../common/utils/middlewares';
import passportInstance from '../passportInstance';
import MailClient from '../../common/libs/MailClient';
import { generateNewPassword, getPasswordHash } from '../../common/utils/auth';
import APIError from '../../common/utils/APIError';
import ERRORS from '../constants/errors';
import ClientService from '../../common/services/ClientService';
import LeadService from '../../common/services/LeadService';

export default class {
  static get validators() {
    const registrationToken = checkRequiredId;
    const leadId = checkRequiredId;

    return {
      getCode: Joi.object({ phone: checkPhone({ mobileOnly: true }) }),
      checkCode: Joi.object({
        phone: checkPhone({ mobileOnly: true }).required(),
        code: Joi.string().required().min(4).max(4),
      }),
      prepareResetPassword: Joi.object({
        email: Joi.string().email().required(),
      }),
      providerCallback: Joi.object({
        code: Joi.string().required(),
        state: Joi.string().required(),
        nonce: Joi.string().required(),
      }),
      registerContactData: Joi.object({
        email: Joi.string().email(),
        phone: checkPhone({ mobileOnly: true }),
        firstName: Joi.string(),
        patronymicName: Joi.string().allow(null, ''),
        lastName: Joi.string(),
      }).or('email', 'phone'),
      registerOrganization: Joi.object({
        leadId,
        registrationToken,
        user: userRegistration,
        organization,
      }),
      registerIndividual: Joi.object({
        leadId,
        user: userRegistration,
        registrationToken,
      }),
    };
  }

  static async getCode(req, res) {
    const code = await AuthService.getCode(req.body.phone);
    res.json(code ? { code } : null);
  }

  static async checkCode(req, res) {
    const { phone, code } = req.body;
    const token = await AuthService.checkCode({ phone, code });
    return res.json({ token });
  }

  static async localLogin(req, res, next) {
    const { password, emailOrPhone } = req.body;
    passportInstance.authenticate('local', authMiddleware({
      req, res, next, emailOrPhone, password,
    }))(req, res, next);
  }

  /**
   * @description nonce это требование безопасности SBBID для предотвращения атака повторного воспроизведения.
   */
  static async provider(req, res, next) {
    const nonce = generateNewPassword({ pattern: 'aA0', length: 15 });
    req.session.referer = req.headers.referer;
    req.session.nonce = getPasswordHash(nonce);
    passportInstance.authenticate('provider', { nonce })(req, res, next);
  }

  /**
   * @description nonce выдается только на сессию.
   */
  static async providerCallback(req, res, next) {
    if (!req.query.nonce || !bcrypt.compareSync(req.query.nonce, req.session.nonce)) {
      throw new APIError(ERRORS.SBBID.NONCE_IS_NOT_VALID);
    }

    req.session.nonce = '';

    passportInstance.authenticate('provider', authOpenIDMiddleware({
      req, res, next,
    }))(req, res, next);
  }

  static async prepareResetPassword(req, res) {
    const { body: { email } } = req;
    const newPass = await UserService.prepareResetPassword(email);

    await MailClient.registration({
      name: 'resetPassword',
      recipients: [{
        email,
      }],
      variables: {
        email,
        resetPassword: newPass,
      },
    });

    return res.json(null);
  }

  static async registerContactData(req, res) {
    const leadInstance = await LeadService.createLead({
      formId: 'REGISTER',
      firstName: 'Лид при регистрации',
      ...req.body,
    });

    /* Возвращаем id созданного лида */
    return res.json(leadInstance.id);
  }

  /* Метод регистрации клиента. Процессно регистрация ФЛ и ЮЛ одинакова, поэтому реализован общий метод используемый с разными валидаторами */
  static async registerClient(req, res) {
    const {
      registrationToken, user, ...data
    } = req.body;

    /* Проверяем валидность токена регистрации */
    await AuthService.checkRegistrationToken({
      phone: user.phone,
      registrationToken,
    });

    const { clientUser } = await ClientService.registerClient({ ...data, user });

    /* Логиним пользователя */
    await AuthService.loginUser(req, clientUser);

    /* Удаляем токен регистрации */
    AuthService.deleteRegistrationToken(user.phone);

    return res.json(null);
  }

  static async sendEmailConfirmationUrl(req, res) {
    await AuthService.sendEmailConfirmationUrl({ email: req.user.email });

    return res.json(null);
  }
}
