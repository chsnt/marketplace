import Joi from '@hapi/joi';

import TicketService from '../services/TicketService';
import {
  baseDateValidation, checkId, checkRequiredId, createClientDevice,
} from '../../common/utils/validation';
import TicketControllerCommon from '../../common/controllers/TicketController';

export default class TicketController extends TicketControllerCommon {
  static get validators() {
    return {
      ...super.validators,
      createTicket: Joi.object({
        description: Joi.string().max(255).required(),
        services: Joi.array().items(checkId),
        serviceAddressId: checkId,
        nextCallDate: baseDateValidation.allow(null),
        device: Joi.object(createClientDevice),
      }),
      createTicketReview: Joi.object({
        ticket: checkRequiredId,
        mark: Joi.number().max(10).required(),
        description: Joi.string().max(255).required(),
      }),
      getTicketReview: Joi.object({
        id: checkRequiredId,
      }),
    };
  }

  static async createTicket(req, res) {
    const docNames = req.files?.map((file) => ({ fsName: file.filename, name: file.originalname }));
    const { user } = req;
    const result = await super.createTicket({
      user, docNames, ...req.body,
    });

    return res.json(result);
  }

  static async getTicketList(req, res) {
    const result = await TicketService.getTicketList(req.user.clientId);
    return res.json(result);
  }

  static async getCloseDocumentsList(req, res) {
    const result = await TicketService.getTicketsWithCloseDocuments(req.user.clientId);
    return res.json(result);
  }

  static async createTicketReview(req, res) {
    const docNames = req.files.map((file) => ({ fsName: file.filename, name: file.originalname }));

    const result = await TicketService.createTicketReview({
      ...req.body,
      docNames,
    });
    return res.json(result);
  }

  static async getTicketReview(req, res) {
    const result = await TicketService.getTicketReview(req.params.id);
    return res.json(result);
  }
}
