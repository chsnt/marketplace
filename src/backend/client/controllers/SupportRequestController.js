import SupportRequestControllerCommon from '../../common/controllers/SupportRequestController';
import SupportRequestService from '../services/SupportRequestService';

export default class SupportRequestController extends SupportRequestControllerCommon {
  /**
   * Метод получения ЗНО и запросов "Помощь" одним списком
   */
  static async getList(req, res) {
    const { clientId } = req.user;

    const supportRequests = await SupportRequestService.getList(clientId);

    return res.json(supportRequests);
  }

  static async createReview(req, res) {
    const docNames = req.files.map((file) => ({ fsName: file.filename, name: file.originalname }));
    const result = await SupportRequestService.createReview({
      ...req.body,
      docNames,
    });
    return res.json(result);
  }
}
