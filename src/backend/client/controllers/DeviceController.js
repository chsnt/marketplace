import Joi from '@hapi/joi';
import {
  checkAddress, createClientDevice, clientDevice, checkId, checkRequiredId,
} from '../../common/utils/validation';
import DeviceService from '../../common/services/DeviceService';
import DeviceControllerCommon from '../../common/controllers/DeviceController';

export default class DeviceController extends DeviceControllerCommon {
  static get validators() {
    return {
      ...super.validators,
      createClientDevice: Joi.object(createClientDevice),
      getDeviceModelParameters: Joi.object({
        id: checkRequiredId,
      }),
      getClientDevices: Joi.object({
        type: checkId,
        model: checkId,
        manufacture: checkId,
      }),
      deleteClientDevice: Joi.object({
        id: checkId,
      }),
      updateClientDevice: Joi.object({
        id: checkId,
        parameters: clientDevice.parameters,
        address: Joi.object({
          ...checkAddress,
          apartment: Joi.string().allow(null, ''),
          postalCode: Joi.string().allow(null, ''),
        })
          .with('region', 'cityId')
          .rename('region', 'federationSubjectId'),
      }),
    };
  }

  static async createClientDevice(req, res) {
    const device = await DeviceService.createClientDevice({
      device: req.body,
      clientId: req.user.clientId,
    });
    return res.json({ id: device.id });
  }

  static async getDeviceParameters(req, res) {
    const result = await DeviceService.getDeviceParameters(req.query);
    return res.json(result);
  }

  static async getClientDevices(req, res) {
    const result = await DeviceService.getClientDevices({
      ...req.query,
      clientId: req.user.clientId,
      isFiasRequired: false,
    });
    return res.json(result);
  }

  static async getClientDevicesWithFias(req, res) {
    const result = await DeviceService.getClientDevices({
      ...req.query,
      clientId: req.user.clientId,
      isFiasRequired: true,
    });
    return res.json(result);
  }

  static async getDeviceModelParameters(req, res) {
    const result = await DeviceService.getDeviceModelParameters(req.params.id);
    return res.json(result);
  }

  static async getManufacturerList(req, res) {
    const result = await DeviceService.getManufacturerList();
    return res.json(result);
  }

  static async getTypeManufacturerModel(req, res) {
    const result = await DeviceService.getTypeManufacturerModel();
    return res.json(result);
  }

  static async deleteClientDevice(req, res) {
    await DeviceService.deleteClientDevice(req.body?.id);
    return res.json(null);
  }

  static async updateClientDevice(req, res) {
    const result = await DeviceService.updateClientDevice(req.body);
    return res.json(result);
  }
}
