import Joi from '@hapi/joi';
import IntegrationImportService from '../../common/services/IntegrationImportService';

export default class IntegrationController {
  static get validators() {
    const commonImportItem = {
      id: Joi.string().required(),
      name: Joi.string().required(),
    };

    const commonImportValidator = Joi.array().items(Joi.object(commonImportItem));

    const tariffServiceBaseParams = {
      ...commonImportItem,
      cost: Joi.number().required(),
    };

    return {
      importTaxTypes: commonImportValidator,
      importBanks: Joi.array().items(Joi.object({
        ...commonImportItem,
        rcbic: Joi.string().length(9).required(),
        correspondentAccount: Joi.string().max(20).required(),
      })),
      importCities: Joi.array().items(Joi.object({
        ...commonImportItem,
        isTestingParticipant: Joi.boolean().required(),
      })),
      importNomenclature: Joi.object({
        tariffs: Joi.array().items(Joi.object(tariffServiceBaseParams)),
        services: Joi.array().items(Joi.object({
          ...tariffServiceBaseParams,
          forTariffOnly: Joi.boolean().default(false),
        })),
        tariffServiceRelations: Joi.array().items(Joi.object({
          tariffId: Joi.string().required(),
          serviceId: Joi.string().required(),
          limit: Joi.number().required(),
        })),
      }),
      importDevices: Joi.object({
        deviceTypes: Joi.array().items(Joi.object(commonImportItem)).required(),
        deviceManufacturers: Joi.array().items(Joi.object({
          ...commonImportItem,
          deviceModels: Joi.array().items(Joi.object({
            ...commonImportItem,
            deviceTypeId: Joi.string().required(),
          })).required(),
        })).required(),
      }),
      importDocument: Joi.object({
        ...commonImportItem,
        typeId: Joi.string().required(),
        orderId: Joi.string().required(),
      }).required(),
    };
  }

  static async importBanks(req, res) {
    await IntegrationImportService.importBanks(req.body);
    return res.json(null);
  }

  static async importTaxTypes(req, res) {
    await IntegrationImportService.importTaxTypes(req.body);
    return res.json(null);
  }

  static async importCities(req, res) {
    await IntegrationImportService.importCities(req.body);
    return res.json(null);
  }

  static async importNomenclature(req, res) {
    await IntegrationImportService.importNomenclature(req.body);
    return res.json(null);
  }

  static async importDevices(req, res) {
    await IntegrationImportService.importDevices(req.body);
    return res.json(null);
  }

  static async importDocument(req, res) {
    await IntegrationImportService.importDocument({
      ...req.body,
      file: {
        fsName: req.file.filename,
        name: req.file.originalname,
      },
    });
    return res.json(null);
  }
}
