import Joi from '@hapi/joi';
import ClientService from '../services/ClientService';
import ClientControllerCommon from '../../common/controllers/ClientController';
import UserService from '../../common/services/UserService';
import { checkPhone } from '../../common/utils/validation';
import RedisService from '../../common/services/RedisService';

const redis = new RedisService();

export default class ClientController extends ClientControllerCommon {
  static get validators() {
    return {
      deleteClient: Joi.object({
        phone: checkPhone(),
      }),
      getAutorizationCode: Joi.object({
        phone: checkPhone(),
      }),
    };
  }

  static async getClientAllowedServices(req, res) {
    const { user } = req;
    const clientId = ClientService.getClientId(user);

    const services = await super.getClientAllowedServices(clientId);

    return res.json(services);
  }

  static async deleteClient(req, res) {
    const { phone } = req.body;

    const userData = await UserService.getUserData({ emailOrPhone: phone });

    const clientId = await ClientService.getClientId(userData);

    await ClientService.deleteClient({ clientId, phone });
    return res.json(null);
  }

  static async getAutorizationCode(req, res) {
    const { phone } = req.query;
    const result = await redis.getQuery(`${phone}:phone-code`);
    return res.json(result);
  }
}
