import UserService from '../services/UserService';
import OrganizationService from '../../common/services/OrganizationService';
import AuthService from '../services/AuthService';
import NotificationService from '../services/NotificationService';
import OrderService from '../services/OrderService';
import TariffService from '../../common/services/TariffService';
import ServiceService from '../../common/services/ServiceService';
import IntegrationExportService from '../../common/services/IntegrationExportService';
import UserControllerCommon from '../../common/controllers/UserController';
import MailClient from '../../common/libs/MailClient';
import { INTEGRATOR } from '../../common/constants/env';
import RabbitClient from '../../common/libs/RabbitClient';
import logger from '../../common/services/LoggerService';

/**
 *
 * @param {String} type Название шаблона
 * @param {Object} contactPerson Данные пользователя
 * @param {String} contactPerson.lastName Фамилия
 * @param {String} contactPerson.firstName Имя
 * @param {String} contactPerson.patronymicName Отчество
 * @param {String} contactPerson.email Почтовый адрес
 * @return {Promise<void>}
 */
const sendEmail = ({ type, contactPerson = {} }) => MailClient.registration({
  name: type,
  recipients: [{
    name: contactPerson?.fullName,
    email: contactPerson?.email,
  }],
  variables: {
    email: contactPerson?.email,
    phone: contactPerson?.phone,
  },
});

export default class UserController extends UserControllerCommon {
  static async getCartAndNotifications(req, res) {
    const { clientId } = req.user;

    const [notifications, cart] = await Promise.all([
      NotificationService.getForClient(clientId),
      OrderService.getCart(clientId),
    ]);

    return res.json({ notifications, cart });
  }

  static async getDeviceTariffsAndServices(req, res) {
    const { clientId } = req.user;

    const [tariffs, services] = await Promise.all([
      TariffService.getClientTariffs({ clientId }),
      ServiceService.getClientServices({ clientId }),
    ]);

    return res.json({ tariffs, services });
  }

  static async getOrderList(req, res) {
    const result = await OrderService.getOrderList(req.user.clientId);

    return res.json(result);
  }

  static async updateUser(req, res) {
    const {
      user: { id, organizationId },
    } = req;
    const userAndOrganization = await UserService.updateInfo({ ...req.body, user: { id, ...req.body.user } });

    const {
      user: contactPerson,
      organization,
    } = userAndOrganization;

    if (req.body.isTemporaryPassword) {
      await AuthService.deleteTemporaryAuthentication(id);
    }

    /* По ФЛ данные в 1С не отправляем */
    if (organizationId) {
      const bankDetails = await OrganizationService.getBankInfo({ organizationId });

      if (INTEGRATOR.IS_SENDING_ENABLED && IntegrationExportService.checkChangedBankDetails({
        oldBankDetails: bankDetails,
        newBankDetails: organization.bank,
      })) {
        try {
          const client = new RabbitClient();
          await client.sendToQueue({
            queue: client.queues.integrationChangedBankDetails,
            message: {
              id: organizationId,
              bank: organization.bank,
            },
          });
        } catch (error) {
          logger.error(error);
        }
      }
    }

    try {
      const preSend = ({ type }) => sendEmail({
        type, contactPerson,
      });


      if (req.body.user?.password) {
        await preSend({ type: 'newPassword' });
      }

      if (req.body.user?.email) {
        await preSend({ type: 'newEmail' });
      }
    } catch (error) {
      logger.error(error);
    }

    return res.json(userAndOrganization);
  }

  static async getCompletedList(req, res) {
    const { clientId } = req.user;

    const [tariffs, services] = await Promise.all([
      TariffService.getClientTariffs({ clientId, used: true }),
      ServiceService.getClientServices({ clientId, used: true }),
    ]);

    res.json({ tariffs, services });
  }

  static async confirmationEmail(req, res) {
    try {
      const userInstance = await AuthService.checkMailCode(req.query);
      /* Если сессия существовала логиним пользователя для обновления сессии */
      if (req.user) {
        req.logout();
        await AuthService.loginUser(req, userInstance);
      }
      return res.redirect('/?emailConfirmed=true');
    } catch (error) {
      logger.error(error);
      return res.redirect('/?emailConfirmed=false');
    }
  }
}
