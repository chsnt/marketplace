import { ElectronicDocumentCirculationModel } from '../../common/database/models/Edo';

export default class EdoService {
  static getList() {
    return ElectronicDocumentCirculationModel.findAll({
      attributes: ['id', 'name', 'alias'],
    });
  }
}
