import UserServiceCommon from '../../common/services/UserService';
import RedisService from '../../common/services/RedisService';
import ERRORS from '../constants/errors';
import APIError from '../../common/utils/APIError';
import AuthService from './AuthService';
import { TEMPORARY_PASSWORD_EXPIRE } from '../../common/constants';
import { getPasswordHash } from '../../common/utils/auth';
import RoleService from '../../common/services/RoleService';
import { UserModel } from '../../common/database/models/User';

const redis = new RedisService();

export default class UserService extends UserServiceCommon {
  static async prepareResetPassword(email) {
    const userInstance = await UserModel.findByEmailOrPhoneOrLogin(email, {
      include: [{
        association: 'groups',
        attributes: ['id'],
      }],
    });

    if (!userInstance) {
      throw new APIError(ERRORS.AUTH.USER_NOT_FOUND);
    }

    const hasUserAccessToAdmin = RoleService.hasUserAccess({
      groups: RoleService.ACCESS_TO_ADMIN_PANEL,
      userGroups: userInstance.groups,
    });

    /* Не даем админу сбрасывать пароль через клиентский интерфейс */
    if (hasUserAccessToAdmin) throw new APIError(ERRORS.AUTH.RESET_PASS_BY_ADMIN);

    const previousResetPassAttemptTimestamp = parseInt(
      await redis.getQuery(`${userInstance.id}:prev-reset-pass-attempt`) || '0',
      10,
    );

    // Проверяем момент последнего сброса пароля. Если прошло меньше 60 секунд - выдаем ошибку
    if (previousResetPassAttemptTimestamp) {
      throw new APIError(ERRORS.AUTH.RESET_PASS_TIMEOUT_ERROR);
    }

    // Если нашли такого пользователя, создаём ему новый пароль, который кладём в Redis на 24 часа
    const newPassword = await this.genNewPassword(userInstance.id);
    await AuthService.deleteTemporaryPassword(userInstance.id);
    await AuthService.setTemporaryPassword(userInstance.id, getPasswordHash(newPassword), TEMPORARY_PASSWORD_EXPIRE);
    await AuthService.deleteTemporaryAuthentication(userInstance.id);
    return newPassword;
  }
}
