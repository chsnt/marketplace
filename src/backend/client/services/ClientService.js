import {
  ClientModel, ClientTariffModel, ClientServiceModel, ClientDeviceModel,
} from '../../common/database/models/Client';
import { OrderModel } from '../../common/database/models/Order';
import { UserModel } from '../../common/database/models/User';
import { LeadModel } from '../../common/database/models/Lead';
import { AddressModel } from '../../common/database/models/Client/Organization';
import ClientServiceCommon from '../../common/services/ClientService';

import db from '../../common/database';
import APIError from '../../common/utils/APIError';
import ERRORS from '../constants/errors';

export default class ClientService extends ClientServiceCommon {
  static async deleteClient({ clientId, phone, transaction }) {
    const localTransaction = transaction || await db.sequelize.transaction();

    try {
      const clientInstance = await ClientModel.findByPk(clientId, {
        include: [
          {
            association: 'organization',
            paranoid: false,
            attributes: ['id'],
            include: [
              {
                association: 'users',
                paranoid: false,
              },
              {
                association: 'realAddress',
                attributes: ['id'],
                paranoid: false,
              },
              {
                association: 'legalAddress',
                attributes: ['id'],
                paranoid: false,
              },
            ],
          },
          {
            association: 'users',
            paranoid: false,
            attributes: ['id'],
          },
          {
            association: 'clientDevices',
            paranoid: false,
            attributes: ['id'],
            include: 'serviceAddress',
          },
          {
            association: 'clientTariffs',
            paranoid: false,
            attributes: ['id'],
          },
          {
            association: 'clientServices',
            paranoid: false,
            attributes: ['id'],
          },
          {
            association: 'orders',
            paranoid: false,
            attributes: ['id'],
          },
          {
            association: 'leads',
            paranoid: false,
            attributes: ['id'],
          },
          {
            association: 'supportRequests',
            paranoid: false,
            attributes: ['id'],
          },
        ],
        paranoid: false,
        attributes: ['id'],
        transaction,
      });

      if (phone) {
        await LeadModel.destroy({
          where: { phone },
          force: true,
          transaction,
        });
      }

      if (clientInstance?.users) {
        await UserModel.destroy({
          where: {
            id: clientInstance.users.map((user) => user.id),
          },
          force: true,
          transaction,
        });
      }

      if (clientInstance?.clientDevices) {
        const { idClientDevices, idAddressModel } = clientInstance.clientDevices.reduce(((acc, device) => {
          acc.idClientDevices.push(device.id);
          acc.idAddressModel.push(device.serviceAddress.id);
          return acc;
        }), {
          idClientDevices: [],
          idAddressModel: [],
        });

        await ClientDeviceModel.destroy({
          where: {
            id: idClientDevices,
          },
          force: true,
          transaction,
        });

        await AddressModel.destroy({
          where: {
            id: idAddressModel,
          },
          force: true,
          transaction,
        });
      }

      if (clientInstance?.clientTariffs) {
        await ClientTariffModel.destroy({
          where: {
            id: clientInstance.clientTariffs.map((tariff) => tariff.id),
          },
          force: true,
          transaction,
        });
      }

      if (clientInstance?.clientServices) {
        await ClientServiceModel.destroy({
          where: {
            id: clientInstance.clientServices.map((service) => service.id),
          },
          force: true,
          transaction,
        });
      }

      if (clientInstance?.orders) {
        await OrderModel.destroy({
          where: {
            id: clientInstance.orders.map((order) => order.id),
          },
          force: true,
          transaction,
        });
      }

      if (clientInstance?.organization) {
        await UserModel.destroy({
          where: {
            id: clientInstance.organization.users.map((user) => user.id),
          },
          force: true,
          transaction,
        });
        await clientInstance.organization.realAddress?.destroy({
          force: true,
          transaction,
        });
        await clientInstance.organization.legalAddress.destroy({
          force: true,
          transaction,
        });
        await clientInstance.organization.destroy({
          force: true,
          transaction,
        });
      }

      await clientInstance.destroy({
        force: true,
        transaction,
      });

      if (!transaction) await localTransaction.commit();
    } catch (error) {
      if (!transaction) await localTransaction.rollback();
      throw new APIError(ERRORS.AUTOTEST.DELETE_CLIENT, { originalError: error });
    }
  }
}
