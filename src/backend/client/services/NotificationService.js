import { NotificationModel } from '../../common/database/models/Client';

export default class NotificationService {
  static async getForClient(clientId) {
    const notifications = await NotificationModel.findAll({
      where: { clientId },
      attributes: ['id', 'title', 'message', 'payload', 'createdAt', 'isRead'],
      order: [['createdAt', 'desc']],
    });

    return notifications;
  }

  static async readNotifications({ clientId, notificationIds }) {
    return NotificationModel.update({ isRead: true }, { where: { clientId, id: notificationIds } });
  }
}
