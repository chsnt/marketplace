import { v4 } from 'uuid';

import RedisService from '../../common/services/RedisService';
import NotifierService from '../../common/services/NotifierService';
import { UserModel } from '../../common/database/models/User';

import is from '../../common/libs/is';

import APIError from '../../common/utils/APIError';

import ERRORS from '../constants/errors';

import AuthServiceCommon from '../../common/services/AuthService';

const redis = new RedisService();

export default class AuthService extends AuthServiceCommon {
  static async getCode(phone) {
    const existingPhone = await UserModel.findOne({
      where: { phone },
    });

    if (existingPhone) throw new APIError(ERRORS.AUTH.PHONE_ALREADY_REGISTERED);

    const code = Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000;
    /* Сохраняем код в редис на 15 минут */
    await redis.setQuery(`${phone}:phone-code`, code, 900);

    /* Для локальной разработки просто возвращаем код в ответе */
    if (!is.env.production()) return String(code);

    /* Отправляем СМС */
    await NotifierService.sendSMS({ phone, text: `Ваш код для регистрации: ${code}` });
  }

  static async checkCode({ phone, code }) {
    const result = await redis.getQuery(`${phone}:phone-code`);
    if (!result) {
      throw new APIError(ERRORS.AUTH.BAD_VERIFICATION_CODE);
    }
    if (result !== code) {
      throw new APIError(ERRORS.AUTH.INCORRECT_VERIFICATION_CODE);
    }
    const token = v4();
    /* Создаем токен регистрации */
    await redis.setQuery(`${phone}:registration-token`, token, 3600);
    /* Удаляем код из редиса */
    await redis.delQuery(`${phone}:phone-code`);
    return token;
  }

  static async checkRegistrationToken({ phone, registrationToken }) {
    const token = await redis.getQuery(`${phone}:registration-token`);
    if (!token) {
      throw new APIError(ERRORS.AUTH.REGISTRATION_TOKEN_IS_EXPIRED);
    }
    if (token !== registrationToken) {
      throw new APIError(ERRORS.USER.REGISTRATION_ERROR);
    }
  }

  static async deleteRegistrationToken(phone) {
    await redis.delQuery(`${phone}:registration-token`);
  }

  static async checkMailCode({ codeMail }) {
    let userInstance;
    try {
      const email = await redis.getQuery(`${codeMail}:email`);

      if (!email) {
        throw new APIError(ERRORS.USER.MAIL_VERIFICATION_CODE_IS_FAILED);
      }

      userInstance = await UserModel.findOne({
        where: { email },
        attributes: ['id', 'isEmailConfirmed'],
      });

      if (!userInstance) {
        throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(email));
      }

      await userInstance.update({ isEmailConfirmed: true });
    } catch (error) {
      throw new APIError(ERRORS.USER.MAIL_VERIFICATION_CODE_IS_FAILED, { originalError: error });
    }

    await redis.delQuery(`${codeMail}:email`);

    return userInstance;
  }
}
