import path from 'path';
import { SupportRequestModel, SupportRequestReviewModel } from '../../common/database/models/SupportRequest';
import SupportRequestServiceCommon from '../../common/services/SupportRequestService';
import db from '../../common/database';
import FileService from '../../common/services/FileService';
import APIError from '../../common/utils/APIError';
import ERRORS from '../constants/errors';

export default class SupportRequestService extends SupportRequestServiceCommon {
  static async getList(clientId) {
    const supportRequestInstances = await SupportRequestModel.findAll({
      where: { clientId },
      attributes: ['id', 'createdAt', 'number', 'firstName', 'patronymicName', 'lastName', 'solution', 'resolvedAt'],
      include: [
        {
          association: 'supportRequestTopic',
          attributes: ['name'],
        },
        {
          association: 'supportRequestStatus',
          attributes: ['name', 'alias'],
        },
        {
          association: 'supportRequestReview',
          attributes: ['text', 'mark'],
          include: {
            association: 'supportRequestReviewDocuments',
            attributes: ['id', 'name'],
          },
        },
      ],
      order: [['createdAt', 'DESC']],
    });

    return supportRequestInstances.map((instance) => {
      const {
        firstName, patronymicName, lastName, supportRequestTopic,
        supportRequestStatus, supportRequestReview, ...supportRequest
      } = instance.toJSON();

      return {
        ...supportRequest,
        representative: `${lastName ? `${lastName} ` : ''}${firstName}${patronymicName ? ` ${patronymicName}` : ''}`,
        topic: supportRequestTopic.name,
        status: supportRequestStatus,
        review: {
          ...supportRequestReview,
          supportRequestReviewDocuments: undefined,
          documents: supportRequestReview?.supportRequestReviewDocuments,
        },
      };
    });
  }

  /**
   * create
   * @param {Object} data
   * @param {String} data.supportRequestId UUID обращения
   * @param {String} data.text отзыв
   * @param {number} data.mark оценка
   * @param {Array.<Object>} [data.docNames] Список файлов
   * @param {String} data.docNames.fsName Имя файла на диске
   * @param {String} data.docNames.name Оригинальное имя файла
   * @returns {Promise<void>}
   */
  static async createReview({
    supportRequestId, mark, text, docNames,
  }) {
    const transaction = await db.sequelize.transaction();

    try {
      const supportRequestInstance = await SupportRequestModel.findByPk(supportRequestId, { transaction });

      if (!supportRequestInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(supportRequestId));

      const supportRequestReview = await SupportRequestReviewModel.create({
        text,
        mark,
        supportRequestReviewDocuments: docNames,
      }, {
        include: 'supportRequestReviewDocuments',
        transaction,
      });

      await supportRequestInstance.update({
        supportRequestReviewId: supportRequestReview.id,
      }, { transaction });

      supportRequestReview.supportRequestReviewDocuments.forEach((fileAttachment) => {
        FileService.renameFile({
          dir: path.join(FileService.DEFAULT_FILE_PATH, 'support'),
          oldName: docNames.find((doc) => doc.name === fileAttachment.name).fsName,
          newName: fileAttachment.id,
        });
      });

      await transaction.commit();
      return null;
    } catch (error) {
      await transaction.rollback();
      if (!(error instanceof APIError)) {
        throw new APIError(ERRORS.SUPPORT_REQUEST.CREATE_REVIEW, { originalError: error });
      }
      throw error;
    }
  }
}
