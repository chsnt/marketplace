import path from 'path';
import { Op } from 'sequelize';
import { TicketModel, TicketReviewModel } from '../../common/database/models/Ticket';
import FileService from '../../common/services/FileService';
import ServiceService from '../../common/services/ServiceService';
import TicketServiceCommon from '../../common/services/TicketService';
import db from '../../common/database';
import APIError from '../../common/utils/APIError';
import ERRORS from '../constants/errors';

const ID = 'id';
const NAME = 'name';
const COST = 'cost';
const VALUE = 'value';
const EXPIRATION_AT = 'expirationAt';
const CREATED_AT = 'createdAt';

const CLIENT_SERVICES = 'clientServices';
const CLIENT_TARIFF_SERVICES = 'clientTariffServices';
const TARIFF = 'tariff';
const CLIENT_TARIFF = 'clientTariff';
const SERVICE = 'service';
const ALIAS = 'alias';
const TICKET_STATUS = 'ticketStatus';
const TICKET_REVIEW = 'ticketReview';

const ORGANIZATION_SERVICES_ATR = [COST, EXPIRATION_AT];
const TICKET_REVIEW_ATR = ['text', 'mark'];
const TICKET_ATR = [ID, 'number', 'solution', CREATED_AT, 'resolvedAt', 'laborCosts'];

const ORDER = [[CREATED_AT, 'desc']];


export default class TicketService extends TicketServiceCommon {
  static async getTicketList(clientId) {
    const list = await TicketModel.findAll({
      attributes: TICKET_ATR,
      where: {
        [Op.or]: [
          { '$clientServices.client_id$': clientId },
          { '$clientTariffServices.clientTariff.client_id$': clientId },
        ],
      },
      order: ORDER,
      include: [
        {
          association: 'serviceAddress',
          paranoid: false,
          attributes: {
            exclude: [
              'id',
              'createdAt',
              'updatedAt',
              'deletedAt',
              'city',
              'cityId',
              'city_id',
              'federationSubjectId'],
          },
          include: [
            {
              association: 'cityRelationship',
              paranoid: false,
              attributes: ['id', 'name'],
            },
            {
              association: 'federationSubject',
              paranoid: false,
              attributes: ['id', 'name'],
            },
          ],
        },
        {
          association: CLIENT_SERVICES,
          attributes: [COST],
          include: [
            {
              association: SERVICE,
              attributes: [NAME, ALIAS],
            },
          ],
        },
        {
          association: CLIENT_TARIFF_SERVICES,
          attributes: [VALUE],
          include: [
            {
              association: CLIENT_TARIFF,
              attributes: ORGANIZATION_SERVICES_ATR,
              include: [
                {
                  association: TARIFF,
                  attributes: [NAME, ALIAS],
                },
              ],
            },
            {
              association: 'tariffServiceRelation',
              paranoid: false,
              attributes: ['id'],
              include: [
                {
                  association: 'service',
                  paranoid: false,
                  attributes: ['id', 'name'],
                },
              ],
            },
          ],
        },
        {
          association: TICKET_STATUS,
          attributes: [NAME],
        },
        {
          association: TICKET_REVIEW,
          attributes: TICKET_REVIEW_ATR,
          include: {
            association: 'ticketReviewDocuments',
            attributes: ['id', 'name'],
          },
        },
        {
          association: 'client',
          include: [
            {
              association: 'organization',
              attributes: ['id'],
              include: {
                association: 'users',
                attributes: ['firstName', 'patronymicName', 'lastName', 'fullName'],
              },
            },
            {
              association: 'users',
              attributes: ['firstName', 'patronymicName', 'lastName', 'fullName'],
            },
          ],
        },
      ],
    });
    return list.map((ticket) => {
      const alias = this.getTicketNumberPrefix(ticket.clientDevice?.deviceModel?.deviceType?.alias);

      let ticketReviewDocuments = null;
      const {
        ticketReview,
        serviceAddress,
        number,
        clientServices,
        clientTariffServices,
        client: { maintainer: user },
        ...data
      } = ticket.toJSON();

      /* Формируем массив услуг, входящих в ЗНО */
      const services = ServiceService.getServicesForTicket({
        clientServices,
        clientTariffServices,
      });

      if (ticketReview && ticketReview.ticketReviewDocuments) {
        ticketReviewDocuments = ticketReview.ticketReviewDocuments.map((document) => ({
          id: document.id,
          name: `${FileService.removeTimestamp(document.name)}`,
        }));
      }

      return {
        ...data,
        serial: `${alias}${number}`,
        representative:
          `${user.lastName ? `${user.lastName} `
            : ''}${user.firstName}${user.patronymicName ? ` ${user.patronymicName}`
            : ''}`,
        services,
        serviceAddress: serviceAddress && {
          ...serviceAddress,
          cityRelationship: undefined,
          federationSubject: undefined,
          city: serviceAddress.cityRelationship,
          region: serviceAddress.federationSubject,
        },
        ...(ticketReview && {
          ticketReview: {
            ...ticketReview,
            ticketReviewDocuments,
          },
        }),
      };
    });
  }

  static async getTicketsWithCloseDocuments(clientId) {
    const tickets = await TicketModel.findAll({
      where: { clientId },
      attributes: ['id', 'number', 'createdAt'],
      order: [['createdAt', 'desc']],
      include: [
        {
          association: 'ticketCloseDocuments',
          attributes: ['id', 'name'],
          right: true,
        },
      ],
    });

    return tickets.map((ticketInstance) => {
      const { ticketCloseDocuments, ...ticket } = ticketInstance.toJSON();
      return {
        ...ticket,
        ticketCloseDocuments: ticketCloseDocuments.map((doc) => ({
          ...doc,
          link: `/api/ticket/document/${doc.id}`,
        })),
      };
    });
  }

  /**
   * create
   * @param {Object} data
   * @param {String} data.description Описание
   * @param {String} data.ticket UUID зно
   * @param {number} data.mark оценка
   * @param {Object[]} [data.docNames] Список файлов
   * @param {String} data.docNames.fsName Имя файла на диске
   * @param {String} data.docNames.name Оригинальное имя файла
   * @returns {Promise<void>}
   */
  static async createTicketReview(data) {
    const transaction = await db.sequelize.transaction();

    try {
      const ticketInstance = await TicketModel.findByPk(data.ticket, { transaction });

      if (!ticketInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(data.ticket));

      const ticketReview = await TicketReviewModel.create({
        text: data.description,
        mark: data.mark,
        ticketReviewDocuments: data.docNames,
      }, {
        include: 'ticketReviewDocuments',
        transaction,
      });

      await ticketInstance.update({
        ticketReviewId: ticketReview.id,
      }, { transaction });

      ticketReview.ticketReviewDocuments.forEach((fileAttachment) => {
        FileService.renameFile({
          dir: path.join(FileService.DEFAULT_FILE_PATH, 'ticket'),
          oldName: data.docNames.find((doc) => doc.name === fileAttachment.name).fsName,
          newName: fileAttachment.id,
        });
      });

      await transaction.commit();
      return ticketReview;
    } catch (error) {
      await transaction.rollback();
      if (!(error instanceof APIError)) {
        throw new APIError(ERRORS.TICKET.CREATE_REVIEW, { originalError: error });
      }
      throw error;
    }
  }

  static async getTicketReview(id) {
    const ticketReview = await TicketReviewModel.findOne({
      attributes: ['text', 'mark'],
      include: [{
        association: 'ticket',
        attributes: [],
        where: { id },
      },
      {
        association: 'ticketReviewDocuments',
        attributes: ['id', 'name'],
      }],
    });

    if (!ticketReview) return null;

    const {
      ticketReviewDocuments,
      ...review
    } = ticketReview.toJSON();

    return {
      ...review,
      ticketReviewDocuments: ticketReviewDocuments?.map((document) => ({
        id: document.id,
        name: `${FileService.removeTimestamp(document.name)}`,
      })),
    };
  }
}
