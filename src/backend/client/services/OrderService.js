import { Op } from 'sequelize';
import {
  OrderModel, OrderStatusModel, PaymentModel,
} from '../../common/database/models/Order';
import {
  ClientServiceModel, ClientTariffModel,
} from '../../common/database/models/Client';
import { DiscountByCardModel } from '../../common/database/models/Discount';
import db from '../../common/database';
import APIError from '../../common/utils/APIError';
import OrderServiceCommon from '../../common/services/OrderService';
import { getTotalCostFromOrder } from '../../common/utils/prepareResponse';
import FileService from '../../common/services/FileService';
import ERRORS from '../constants/errors';
import PaymentService from './PaymentService';
import HelperService from '../../common/services/HelperService';
import is from '../../common/libs/is';
import { ServiceModel, TariffModel } from '../../common/database/models/Tariff';
import Discount from '../../common/utils/Discount';
import PaymentTypeModel from '../../common/database/models/Order/PaymentTypeModel';

export default class OrderService extends OrderServiceCommon {
  static async removeFromCart({ tariffs = [], services = [], clientId }) {
    const transaction = await db.sequelize.transaction();
    try {
      const cartInstance = await OrderModel.findOne({
        where: {
          clientId,
          orderStatusId: OrderStatusModel.STATUSES.CART,
        },
        transaction,
      });

      /* Если нет корзины - ничего не делаем */
      if (!cartInstance) {
        await transaction.commit();
        return null;
      }

      /* Удаление элементов */
      await Promise.all([
        ClientTariffModel.destroy({
          where: { orderId: cartInstance.id, id: tariffs },
          force: true,
          transaction,
        }),
        ClientServiceModel.destroy({
          where: { orderId: cartInstance.id, id: services },
          force: true,
          transaction,
        }),
      ]);
      await transaction.commit();
      return this.getCart(clientId);
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }

  /* Метод формирования заказа из корзины */
  static async createOrderFromCart({
    orders = [], clientId, paymentType, clientType, description,
  }) {
    // TODO: добавить проверку услуг, входящих в заказ на предмет адреса
    const transaction = await db.sequelize.transaction();
    try {
      const offerDocumentAndCount = await this.getLastOfferDocumentAndCount({ transaction });

      /* Получим услуги, входящие в заказ */
      const clientServicesInOrder = await ClientServiceModel.findAll({
        where: { id: orders.map(({ services }) => services).flat() },
        attributes: ['id', 'serviceId'],
        transaction,
      });
      const clientTariffInOrder = await ClientTariffModel.findAll({
        where: { id: orders.map(({ tariff }) => tariff) },
        attributes: ['id', 'tariffId'],
        transaction,
      });
      const clientServicesMap = HelperService.createMapFromArray(clientServicesInOrder);
      const clientTariffMap = HelperService.createMapFromArray(clientTariffInOrder);

      /* Подготовим данные для проверки доступности услуг в оборудовании организации */
      const { services, tariffs } = orders.reduce((acc,
        { tariff, serviceAddressId, services: deviceServices = [] }) => {
        if (tariff) acc.tariffs.push({ serviceAddressId, id: clientTariffMap[tariff].tariffId });
        acc.services.push(...deviceServices.map((id) => ({ serviceAddressId, id: clientServicesMap[id].serviceId })));
        return acc;
      }, { tariffs: [], services: [] });

      await this.checkAllowedProducts({
        services,
        tariffs,
        clientType,
        shouldAvailablePayment: true,
        transaction,
      });

      const cartInstance = await OrderModel.findOne({
        where: {
          clientId,
          orderStatusId: OrderStatusModel.STATUSES.CART,
        },
        attributes: ['id', 'number', 'discount', 'description'],
        include: [
          {
            association: 'clientTariffs',
            attributes: ['id', 'serviceAddressId'],
            separate: true,
            include: {
              association: 'tariff',
              attributes: ['id', 'name'],
              include: [
                {
                  association: 'services',
                  through: { attributes: [] },
                  attributes: ['id', 'serviceAddressAccountingTypeId'],
                },
                {
                  ...TariffModel.includeCost(true),
                  separate: true,
                  required: false,
                },
                {
                  association: 'discountService',
                  required: false,
                  where: {
                    expirationAt: {
                      [Op.or]: [
                        { [Op.gte]: new Date() },
                        { [Op.is]: null },
                      ],
                    },
                  },
                  include: {
                    association: 'discountTemplate',
                    include: 'discountTariffServiceRelations',
                  },
                },
              ],
            },
          },
          {
            association: 'clientServices',
            attributes: ['id', 'serviceAddressId'],
            include: [
              {
                association: 'service',
                attributes: ['id', 'name', 'serviceAddressAccountingTypeId'],
                include: [
                  {
                    ...ServiceModel.includeCost(true),
                    separate: true,
                    required: false,
                  },
                  {
                    association: 'discountService',
                    required: false,
                    where: {
                      expirationAt: {
                        [Op.or]: [
                          { [Op.gte]: new Date() },
                          { [Op.is]: null },
                        ],
                      },
                    },
                    include: {
                      association: 'discountTemplate',
                      include: 'discountTariffServiceRelations',
                    },
                  },
                ],
              },
            ],
          },
          {
            association: 'coupon',
            attributes: ['code'],
            include: {
              association: 'discountTemplate',
              include: 'discountTariffServiceRelations',
            },
          },
        ],
        transaction,
      });

      if (!cartInstance) {
        await transaction.rollback();
        return null;
      }

      const discountByCardInstance = await DiscountByCardModel.findOne({
        attributes: ['id', 'value'],
        order: [['createdAt', 'desc']],
        transaction,
      });

      const {
        id: orderId, clientTariffs, clientServices, coupon, discount,
      } = cartInstance.toJSON();

      /* Метод проверки несоответствия тарифов или услуг в корзине и в заказе */
      const checkCartAndOrderMismatch = ({ cartTariffsOrServices, orderTariffsOrServicesSet }) => {
        if (orderTariffsOrServicesSet.size !== cartTariffsOrServices.length) return false;
        return cartTariffsOrServices.every((tariffOrService) => orderTariffsOrServicesSet.has(tariffOrService.id));
      };

      const isInvoicePayment = is.payments.types.alias.invoice(paymentType);

      /* Проверим соответствие товаров в корзине и заказе */
      /* Получим тарифы и услуги из заказа */
      const elementsToCheck = orders.reduce((acc, data) => {
        if (data.tariff) acc.tariffs.push(data.tariff);
        if (data.services?.length) acc.services.push(...data.services);
        return acc;
      }, { tariffs: [], services: [] });

      /* Проверим наличие дублей тарифов или услуг в заказе. Их быть не должно */
      if (
        HelperService.hasDuplicates(elementsToCheck.tariffs)
        || HelperService.hasDuplicates(elementsToCheck.services)
      ) {
        throw new APIError(ERRORS.ORDER.ELEMENTS_ARE_DUPLICATED);
      }

      /* Определим наличие расхождений в корзине и заказе */
      const isCartAndOrderAreMismatch = !checkCartAndOrderMismatch({
        cartTariffsOrServices: clientTariffs,
        orderTariffsOrServicesSet: new Set(elementsToCheck.tariffs),
      }) || !checkCartAndOrderMismatch({
        cartTariffsOrServices: clientServices,
        orderTariffsOrServicesSet: new Set(elementsToCheck.services),
      });

      if (isCartAndOrderAreMismatch) {
        throw new APIError(ERRORS.ORDER.CART_AND_ORDER_ARE_MISMATCH);
      }

      let totalCost = 0;
      let cost = 0;
      let positionCounter = 1;

      const acquiringOrderItemsMap = {};

      const discountObj = new Discount({
        coupon, clientTariffs, clientServices, discount, discountByCard: discountByCardInstance?.value,
      });

      /* Метод добавления продукта в карту */
      const addItemToMap = ({ id, name, costs = [] }) => {
        const currentCost = TariffModel.getCurrentCost(costs) || 0;
        const discountCost = discountObj.getDiscountCost({
          currentCost, id, isInvoicePayment,
        });
        const baseAcquiringOrderItem = {
          tax: {
            taxType: 6,
          },
          itemAttributes: {
            attributes: [
              { name: 'paymentMethod', value: 1 },
              { name: 'paymentObject', value: 4 },
            ],
          },
        };

        totalCost += discountCost || 0;
        cost += currentCost || 0;

        if (acquiringOrderItemsMap[id]) {
          acquiringOrderItemsMap[id].quantity.value += 1;
          return;
        }
        acquiringOrderItemsMap[id] = {
          ...baseAcquiringOrderItem,
          positionId: positionCounter,
          name,
          quantity: {
            value: 1,
            measure: 'шт.',
          },
          itemCode: id,
          /* Цена должна быть в копейках */
          itemPrice: discountCost * 100,
        };

        positionCounter += 1;
      };

      const clientTariffsMap = HelperService.createMapFromArray(cartInstance.clientTariffs);
      const cartClientServicesMap = HelperService.createMapFromArray(cartInstance.clientServices);

      /* Проставляем цену и оборудование клиента для купленного тарифа или услуги */
      /* Для удобства сформируем здесь массив товаров и услуг для отправки в эквайринг */
      /* Попутно проверим обязательность наличия адреса обслуживания при заказе */
      await Promise.all(orders.map((order) => {
        let clientTariff;

        if (order.tariff) {
          clientTariff = clientTariffsMap[order.tariff];

          const { services: tariffServices, ...tariff } = clientTariff.tariff.toJSON();

          if (TariffModel.mustTariffHasServiceAddress(tariffServices) && !order.serviceAddressId) {
            throw new APIError(ERRORS.ORDER.PRODUCT_MUST_HAS_SERVICE_ADDRESS({ name: tariff.name }));
          }

          addItemToMap({ ...tariff });
        }

        return Promise.all([
          /* Запрос обновления тарифа, если тариф есть в заказе */
          ...(clientTariff ? [
            clientTariff.update({
              serviceAddressId: order.serviceAddressId || null,
              cost: discountObj.getDiscountCost({
                currentCost: TariffModel.getCurrentCost(clientTariff.tariff.costs),
                id: clientTariff.tariff.id,
                isInvoicePayment,
              }),
            }, { transaction }),
          ] : []),

          /* Запросы обновления услуг */
          ...(order.services || []).map((serviceId) => {
            const clientService = cartClientServicesMap[serviceId];

            const { serviceAddressAccountingTypeId, ...service } = clientService.service.toJSON();

            if (
              is.service.serviceAddressAccountingType.inOrder(serviceAddressAccountingTypeId)
              && !order.serviceAddressId
            ) {
              throw new APIError(ERRORS.ORDER.PRODUCT_MUST_HAS_SERVICE_ADDRESS({ name: service.name }));
            }

            addItemToMap({ ...service });

            return clientService.update({
              serviceAddressId: order.serviceAddressId || null,

              cost: discountObj.getDiscountCost({
                currentCost: ServiceModel.getCurrentCost(clientService.service.costs),
                id: clientService.service.id,
                isInvoicePayment,
              }),
            }, { transaction });
          }),
        ]);
      }));

      /* Меняем статус заказа с корзины на неоплаченный заказ */
      cartInstance.set({
        orderStatusId: OrderStatusModel.STATUSES.NOT_PAID,
        orderOfferDocumentId: offerDocumentAndCount.document?.id || null,
        /* Всегда ставим дату заказа и сбрасываем в случае ошибки в эквайринге */
        orderDate: new Date(),
        description,
      });

      await cartInstance.save({ transaction });

      let formUrl = null;

      const paymentInstance = await PaymentModel.create({
        orderId,
        cost,
        totalCost,
        paymentTypeId: PaymentTypeModel.TYPES[paymentType].id,
        discountByCardId: discountByCardInstance?.id,
      }, { transaction });

      if (is.payments.enabled && is.payments.types.alias.card(paymentType)) {
        formUrl = await PaymentService.registerPayment({
          paymentInstance,
          amount: totalCost,
          clientId,
          orderDate: cartInstance.orderDate,
          items: Object.values(acquiringOrderItemsMap),
          transaction,
        });
      }

      const order = cartInstance.toJSON();

      await transaction.commit();

      return {
        order,
        formUrl,
        offer: {
          id: offerDocumentAndCount.document?.id || null,
          createdAt: offerDocumentAndCount.document?.createdAt || null,
          count: offerDocumentAndCount.count,
        },
      };
    } catch (error) {
      await transaction.rollback();
      if (!(error instanceof APIError)) throw new APIError(ERRORS.ORDER.CREATE_ORDER, { originalError: error });
      throw error;
    }
  }

  /**
   *
   * @param {String} id UUID заказа
   * @returns {Promise<Array>} Список документов
   */
  static async getDocumentList(id) {
    const order = await OrderModel.findByPk(id, {
      include: [
        {
          association: 'orderOfferDocument',
          attributes: ['id', 'name', 'alias', 'createdAt'],
        },
        {
          association: 'orderDocuments',
          attributes: ['id', 'name', 'date', 'createdAt'],
          include: {
            association: 'orderDocumentType',
            attributes: ['name', 'alias'],
          },
        },
      ],
    });

    const {
      orderOfferDocument,
      orderDocuments,
    } = order.toJSON();

    return [
      ...orderDocuments,
      ...(orderOfferDocument ? [{
        ...orderOfferDocument,
        orderDocumentType: {
          name: 'Оферта',
          alias: 'OFFER',
        },
      }] : []),
    ].map((documentInstance) => this.prepareDocuments(documentInstance));
  }

  static async getOrderList(clientId) {
    const orders = await OrderModel.findAll({
      where: {
        clientId,
        orderStatusId: [OrderStatusModel.STATUSES.NOT_PAID, OrderStatusModel.STATUSES.PAID],
      },
      order: [['number', 'ASC']],
      attributes: ['id', 'number', 'orderStatusId', 'createdAt', 'updatedAt', 'description'],
      include: [
        {
          association: 'orderStatus',
          attributes: ['name', 'alias'],
        },
        {
          association: 'payments',
          attributes: ['createdAt', 'paymentDate'],
        },
        {
          association: 'clientTariffs',
          attributes: ['cost', 'expirationAt'],
          separate: true,
          include: {
            association: 'tariff',
            attributes: ['id', 'name', 'label', 'icon', 'expirationMonths', 'alias', 'description'],
            include: {
              association: 'services',
              attributes: ['id', 'name', 'label', 'icon', 'nameInTariff', 'alias', 'forTariffOnly'],
            },
          },
        },
        {
          association: 'clientServices',
          attributes: ['cost'],
          include: {
            association: 'service',
            attributes: ['id', 'name', 'label', 'icon', 'alias'],
          },
        },
        {
          association: 'orderOfferDocument',
          attributes: ['id', 'name', 'createdAt'],
        },
        {
          association: 'orderDocuments',
          attributes: ['id', 'name', 'date'],
          include: {
            association: 'orderDocumentType',
            attributes: ['name', 'alias'],
          },
        },
      ],
    });

    return orders.map((order) => {
      const {
        id, number, orderStatus: status, clientServices, orderDocuments, clientTariffs, orderOfferDocument,
      } = order.toJSON();

      const payment = PaymentModel.getCurrentPayment(order.payments);

      const date = {
        created: order.createdAt,
        paid: payment?.createdAt,
      };

      const totalCost = getTotalCostFromOrder({ clientTariffs, clientServices });

      return {
        id,
        number,
        status,
        totalCost,
        date,
        services: clientServices.map(({
          cost: clientServiceCost,
          service,
        }) => ({ ...service, clientServiceCost })),
        documents: [
          ...orderDocuments,
          ...(orderOfferDocument ? [{
            ...orderOfferDocument,
            orderDocumentType: {
              name: 'Оферта',
              alias: 'OFFER',
            },
          }] : []),
        ].map((documentInstance) => this.prepareDocuments(documentInstance)),
        tariffs: clientTariffs.map(({
          cost: clientTariffCost, expirationAt, tariff,
        }) => ({
          clientTariffCost,
          expirationAt,
          ...tariff,
          services: tariff.services.map(({
            tariffServiceRelation, name, nameInTariff, ...data
          }) => ({ ...data, name: nameInTariff || name })),
        })),
      };
    });
  }

  /**
   *
   * @param {object} documents
   * @param {String} documents.id UUID документа
   * @param {String} documents.name Название документа
   * @param {String} documents.date Дата создание
   * @param {Object} documents.orderDocumentType Тип документа
   * @param {String} documents.orderDocumentType.name Название типа документа
   * @param {String} documents.orderDocumentType.alias алиас типа документа
   */
  static prepareDocuments(documents) {
    const { createdAt, orderDocumentType, ...document } = documents;

    return {
      ...document,
      created: createdAt,
      name: FileService.removeTimestamp(document.name),
      type: orderDocumentType,
    };
  }
}
