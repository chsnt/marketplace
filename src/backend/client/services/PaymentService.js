import axios from 'axios';
import moment from 'moment';

import { PaymentModel, OrderModel, OrderStatusModel } from '../../common/database/models/Order';

import { SECURE_PAYMENTS_API, SERVICE_DOMAIN } from '../../common/constants/env';
import db from '../../common/database';
import APIError from '../../common/utils/APIError';
import ERRORS from '../constants/errors';
import { Logger } from '../../common/services/LoggerService';
import asyncPool from '../../common/libs/asyncPools';

import PaymentServiceCommon from '../../common/services/PaymentService';
import { DATE_FORMAT } from '../../common/constants';
import { ClientModel } from '../../common/database/models/Client';
import { getTotalCostFromOrder } from '../../common/utils/prepareResponse';
import PaymentTypeModel from '../../common/database/models/Order/PaymentTypeModel';
import is from '../../common/libs/is';

const logger = new Logger('PAYMENT_SERVICE');

/* Система налогообложения по-умолчанию для отправки в эквайринг */
const DEFAULT_TAX_SYSTEM = 0;
/* Время доступности сессии в эквайринге */
const ACQUIRING_SESSION_TIMEOUT = 600;

const BASE_ACQUIRING_PARAMS = {
  userName: SECURE_PAYMENTS_API.USER,
  password: SECURE_PAYMENTS_API.PASSWORD,
};

export default class PaymentService extends PaymentServiceCommon {
  static async checkPayment(orderId, acquiringOrderNumber) {
    const transaction = await db.sequelize.transaction();

    try {
      const url = `${SECURE_PAYMENTS_API.URL}/${SECURE_PAYMENTS_API.GET_ORDER_STATUS}`;

      const result = await axios.get(
        `${url}
      ?userName=${SECURE_PAYMENTS_API.USER}
      &password=${SECURE_PAYMENTS_API.PASSWORD}
      &orderNumber=${acquiringOrderNumber}`.replace(/[\s+\r\n]/g, ''),
      );

      const isAcquiringOrder = result?.data?.actionCode === 0;

      if (!isAcquiringOrder) {
        logger.error(JSON.stringify(result?.data));
        throw new APIError(ERRORS.ORDER.PAYMENT_NOT_FOUND);
      }

      const order = await OrderModel.findOne({
        where: {
          id: orderId,
          orderStatusId: OrderStatusModel.STATUSES.NOT_PAID,
        },
        include: [
          'payments',
          'clientServices',
          {
            association: 'clientTariffs',
            separate: true,
            include: 'tariff',
          },
        ],
        transaction,
      });

      const { clientTariffs, clientServices } = order;

      const totalCost = getTotalCostFromOrder({ clientTariffs, clientServices });

      if (result?.data?.amount !== totalCost && (result?.data?.amount / 100) !== totalCost) {
        logger.error({ ...ERRORS.ORDER.PAYMENT_NOT_FOUND, data: { result: result?.data, totalCost } });
        throw new APIError(ERRORS.ORDER.PAYMENT_NOT_FOUND);
      }

      await asyncPool(order.clientTariffs, (tariff) => {
        const expirationAt = moment()
          .add(tariff.tariff.expirationMonths, 'M')
          .add(1, 'day')
          .startOf('day');

        tariff.update({
          expirationAt,
        }, { transaction });
      });

      await order.update({ orderStatusId: OrderStatusModel.STATUSES.PAID }, { transaction });

      /* TODO подсчитать cost и totalCost */
      const paymentInstance = await PaymentModel.findOne({
        where: { orderId: order.id },
        limit: 1,
        order: [['createdAt', 'DESC']],
      }, { transaction });

      await paymentInstance.update({
        data: result?.data,
        paymentOrderId: acquiringOrderNumber,
        paymentTypeId: (result?.data?.cardAuthInfo && JSON.stringify(result?.data?.cardAuthInfo) !== JSON.stringify({}))
          ? PaymentTypeModel.TYPES.CARD.id : PaymentTypeModel.TYPES.INVOICE.id,
        cost: totalCost,
        totalCost,
        paymentDate: new Date(),
      }, { transaction });

      await transaction.commit();
      return isAcquiringOrder;
    } catch (error) {
      await transaction.rollback();
      logger.error(error);
      if (error.isAxiosError) {
        throw new APIError(ERRORS.ORDER.PAYMENT_NOT_FOUND);
      }
      throw error;
    }
  }

  static getPaymentMethod(payment) {
    const { paymentTypeId, paymentDate } = payment || {};

    if (!paymentDate) return null;
    return is.payments.types.id.card(paymentTypeId);
  }

  /**
   * registerPayment
   * Возвращает URL для оплаты
   * @param {String} orderId Идентификатор заказа
   * @param {Number} amount Сумма заказа в рублях
   * @param {String} clientId Идентификатор клиента
   * @param {Object<Date>} [orderDate] Дата платежа
   * @param {Number} cost Стоимость без учета скидки
   * @param {String} discountByCardId UUID скидки при оплате по карте
   * @param {Object[]} items Массив продуктов
   * @param {String} items.positionId Идентификатор позиции
   * @param {String} items.name Наименование позиции
   * @param {Object} items.quantity Объект количества
   * @param {Number} items.quantity.value Количество элементов позиции
   * @param {String} items.quantity.measure Единица измерения количества
   * @param {String} items.itemCode Идентификатор позиции
   * @param {Number} items.itemPrice Стоимость одной позиции в минимальных единицах валюты
   * @param {Object} items.tax Атрибут описания налога
   * @param {Number} items.tax.taxType Ставка налога (По-умолчанию проставляем 6)
   * @param {Object} items.itemAttributes Дополнительные атрибуты позиции
   * @param {Number} items.itemAttributes.paymentMethod Тип оплаты (По-умолчанию проставляем 1)
   * @param {Number} items.itemAttributes.paymentObject Тип оплачиваемой позиции (По-умолчанию проставляем 4)
   * @param {Object} transaction Внешняя транзакция
   * @returns {Promise<{formUrl, paymentNumber}>}
   */
  static async registerPayment({
    amount, clientId, orderDate, items, paymentInstance, transaction,
  }) {
    const localTransaction = transaction || await db.sequelize.transaction();
    try {
      const url = `${SECURE_PAYMENTS_API.URL}/${SECURE_PAYMENTS_API.REGISTER_ORDER}`;

      /* Получаем данные о клиенте для отправки в эквайринг */
      const clientInstance = await ClientModel.findByPk(clientId, {
        attributes: ['id'],
        include: [
          {
            association: 'organization',
            attributes: ['id'],
            include: [
              {
                association: 'users',
                attributes: [
                  'id', 'email', 'phone', 'firstName',
                  'patronymicName', 'lastName', 'fullName', 'isMaintainer',
                ],
              },
            ],
          },
          {
            association: 'users',
            attributes: ['id', 'email', 'phone', 'firstName', 'patronymicName', 'lastName', 'fullName', 'isMaintainer'],
          },
        ],
        transaction,
      });

      const user = clientInstance.maintainer;

      const correctedAmount = parseInt((amount * 100).toFixed(0), 10);

      /* Формируем объект с данным для передачи в эквайринг */
      const acquiringData = {
        ...BASE_ACQUIRING_PARAMS,
        orderNumber: paymentInstance.number,
        amount: correctedAmount,
        returnUrl: `${SERVICE_DOMAIN}/api/order/complete`,
        failUrl: `${SERVICE_DOMAIN}/api/order/reset-to-cart`,
        sessionTimeoutSecs: ACQUIRING_SESSION_TIMEOUT,
        taxSystem: DEFAULT_TAX_SYSTEM,
        orderBundle: JSON.stringify({
          orderCreationDate: moment(orderDate || new Date()).format(DATE_FORMAT.KEBAB_FULL_T_DELIMITER),
          customerDetails: {
            ...(user?.email && { email: user.email }),
            ...(user?.phone && { phone: user.phone }),
            ...(user?.fullName && { fullName: user.fullName }),
          },
          cartItems: { items },
        }),
      };

      /* Стоимость заказа передается в копейках */
      const result = await axios.get(url, {
        params: acquiringData,
      });

      const { data: { orderId: acquiringOrderId, formUrl, errorMessage } } = result;

      if (!acquiringOrderId && !formUrl) {
        throw new APIError(
          ERRORS.ORDER.REGISTER_PAYMENT,
          { originalError: `Ответ от эквайринга: ${errorMessage || 'Ошибка не указана'}` },
        );
      }

      await paymentInstance.update({
        paymentOrderId: acquiringOrderId,
        data: result?.data,
      }, { transaction: localTransaction });

      if (!transaction) localTransaction.commit();

      return formUrl;
    } catch (error) {
      if (!transaction) localTransaction.rollback();

      if (error.isAxiosError) {
        throw new APIError(ERRORS.ORDER.REGISTER_PAYMENT);
      }
      throw error;
    }
  }
}
