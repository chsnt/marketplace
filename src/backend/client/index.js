import nextjs from 'next';
import server from './server';
import logger from '../common/services/LoggerService';
import db from '../common/database';
import is from '../common/libs/is';
import redirects from './constants/redirects';
import asyncErrorHandler from '../common/libs/asyncErrorHandler';
import OrderController from '../common/controllers/OrderController';

const { databaseConnect } = db;

const app = nextjs({ dev: is.env.development(), quiet: false });

app.prepare().then(() => {
  server.get('*', (req, res) => {
    // eslint-disable-next-line global-require
    const { parse } = require('url');
    const parsedUrl = parse(req.url, true);
    const handle = app.getRequestHandler();

    if (!is.env.development()) {
      res.cookie('XSRF-TOKEN', req.csrfToken());
    }

    const { url: urlRedirect, shouldAuthenticate } = redirects.get(req.url.replace(/\/$/, '')) || {};

    // eslint-disable-next-line no-nested-ternary
    const isAuthenticated = (typeof shouldAuthenticate === 'boolean' && shouldAuthenticate)
      ? (req.isAuthenticated && req.isAuthenticated())
      : (typeof shouldAuthenticate === 'boolean' && !shouldAuthenticate)
        ? (!req.isAuthenticated || !req.isAuthenticated())
        : true;

    if (urlRedirect && isAuthenticated) {
      return res.redirect(urlRedirect);
    }

    if (req.url.includes('oferta')) {
      return asyncErrorHandler(OrderController.getLastOffer(req, res));
    }

    return handle(req, res, parsedUrl);
  });

  const startServer = () => server.listen(process.env.PORT || 3000, (err) => {
    if (err) throw err;
    logger.info(`> App started on http://localhost:${process.env.PORT}`);
  });

  return databaseConnect()
    .then(startServer)
    .catch((err) => {
      logger.error('App start failed: ', err);
      process.exit(1);
    });
});
