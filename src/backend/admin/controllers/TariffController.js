import TariffService from '../../common/services/TariffService';
import TariffControllerCommon from '../../common/controllers/TariffController';
import AvailableCatalogStatusModel from '../../common/database/models/Tariff/AvailableCatalogStatusModel';

export default class TariffController extends TariffControllerCommon {
  static async getList(req, res) {
    const result = await TariffService.getList(
      { ...req.query, availableCatalogStatusId: AvailableCatalogStatusModel.STATUSES.ADMIN },
    );
    res.json(result);
  }

  static async getAllList(req, res) {
    const result = await TariffService.getList(
      { ...req.query, isCostRequired: false },
    );
    res.json(result);
  }

  static async getTariff(req, res) {
    const result = await TariffService.getTariff({
      ...req.params,
      isCostRequired: false,
    });
    res.json(result);
  }
}
