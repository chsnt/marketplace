import Joi from '@hapi/joi';
import moment from 'moment';
import {
  checkId, checkPhone, checkRequiredId, dateOnly, innValidator, limitOffset,
} from '../../common/utils/validation';
import SupportRequestService from '../services/SupportRequestService';
import SupportRequestControllerCommon from '../../common/controllers/SupportRequestController';
import MailClient from '../../common/libs/MailClient';
import { DATE_FORMAT } from '../../common/constants';
import { SupportRequestStatusModel, SupportRequestTopicModel } from '../../common/database/models/SupportRequest';
import logger from '../../common/services/LoggerService';

export default class SupportRequestController extends SupportRequestControllerCommon {
  static get validators() {
    return {
      ...super.validators,
      getList: Joi.object({
        ...limitOffset,
        number: Joi.number(),
        supportRequestStatusId: checkId,
        supportRequestTopicId: checkId,
        clientId: checkId,
        inn: innValidator,
        responsibleId: checkId,
        dateFrom: dateOnly,
        dateTo: dateOnly,
      }).unknown(true),
      createRequest: super.validators.createRequest.append({
        clientId: checkId,
        firstName: Joi.string().required(),
        patronymicName: Joi.string().allow(null, ''),
        lastName: Joi.string(),
        phone: checkPhone(),
        email: Joi.string().email(),
        comment: Joi.string().allow('', null),
      }).or('phone', 'email'),
      updateRequest: Joi.object({
        id: checkRequiredId,
        supportRequestStatusId: checkId,
        solution: Joi.string(),
        comment: Joi.string(),
        responsibleId: checkId,
      }),
      getInfo: Joi.object({
        id: checkRequiredId,
      }),
    };
  }


  static async getList(req, res) {
    const result = await SupportRequestService.getList(req.query);

    return res.json(result);
  }

  static async getInfo(req, res) {
    const result = await SupportRequestService.getInfo(req.params.id);

    return res.json(result);
  }

  static async getTopics(req, res) {
    const result = await SupportRequestService.getTopics();

    return res.json(result);
  }

  static async getStatuses(req, res) {
    const result = await SupportRequestService.getStatuses();

    return res.json(result);
  }

  static async createRequest(req, res) {
    const docNames = req.files.map((file) => ({ fsName: file.filename, name: file.originalname }));
    const result = await SupportRequestService.createRequest({
      ...req.body,
      docNames,
    });

    if (result.supportRequestTopicId === SupportRequestTopicModel.TOPICS.ETC && result.email) {
      try {
        await MailClient.supportRequest({
          name: 'created',
          recipients: [{
            name: `${result.lastName || ''} ${result.firstName || ''} ${result.patronymicName || ''}`,
            email: result.email,
          }],
          variables: {
            number: result.number,
            createdAt: moment(result.createdAt).format(DATE_FORMAT.KEBAB_NORMAL),
            topic: result.topicName,
          },
        });
      } catch (error) {
        logger.error(error);
      }
    }

    return res.json(result.id);
  }

  static async updateRequest(req, res) {
    const result = await SupportRequestService.updateRequest(req.body);

    if (result.email
      && req.body.supportRequestStatusId
      && result.supportRequestTopicId === SupportRequestTopicModel.TOPICS.ETC
    ) {
      try {
        await MailClient.supportRequest({
          name: (req.body.supportRequestStatusId === SupportRequestStatusModel.STATUSES.SOLVED)
            ? 'completed' : 'status',
          recipients: [{
            name: `${result.lastName || ''} ${result.firstName || ''} ${result.patronymicName || ''}`,
            email: result.email,
          }],
          variables: {
            number: result.number,
            createdAt: moment(result.createdAt).format(DATE_FORMAT.KEBAB_NORMAL),
            status: result.statusName,
            solution: result.solution,
          },
        });
      } catch (error) {
        logger.error(error);
      }
    }

    return res.json(result.id);
  }
}
