import Joi from '@hapi/joi';
import { checkRequiredId } from '../../common/utils/validation';
import IntegratorService from '../services/IntegratorService';

export default class IntegratorController {
  static get validators() {
    const integrator = Joi.object({
      token: Joi.string().uuid(),
      name: Joi.string().min(2).max(20),
    });

    return {
      requiredId: Joi.object({ id: checkRequiredId }),
      createIntegrator: integrator.fork(['token', 'name'], (schema) => schema.required()),
      updateIntegrator: integrator.min(1),
    };
  }

  static async createIntegrator(req, res) {
    await IntegratorService.createIntegrator(req.body);
    return res.json(null);
  }

  static async updateIntegrator(req, res) {
    await IntegratorService.updateIntegrator({ ...req.body, ...req.params });
    return res.json(null);
  }

  static async getInfo(req, res) {
    const integrator = await IntegratorService.getInfo(req.params);
    return res.json(integrator);
  }

  static async getList(req, res) {
    const integrators = await IntegratorService.getList();
    return res.json(integrators);
  }
}
