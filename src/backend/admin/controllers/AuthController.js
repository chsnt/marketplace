import passportInstance from '../passportInstance';

import { authMiddleware } from '../../common/utils/middlewares';

export default class {
  static async localLogin(req, res, next) {
    const { password, emailOrPhone } = req.body;

    passportInstance.authenticate('local', authMiddleware({
      req, res, next, emailOrPhone, password,
    }))(req, res, next);
  }
}
