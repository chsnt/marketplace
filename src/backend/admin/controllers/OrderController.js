import Joi from '@hapi/joi';
import moment from 'moment';
import fs from 'fs';
import path from 'path';
import {
  baseDateValidation, checkId, checkRequiredId, dateOnly, orderSort, limitOffset, queryWithNull,
} from '../../common/utils/validation';
import OrderService from '../services/OrderService';
import is from '../../common/libs/is';
import MailService from '../../common/services/MailService';
import UserService from '../../common/services/UserService';
import OrderControllerCommon from '../../common/controllers/OrderController';
import { INTEGRATOR, SENDPULSE } from '../../common/constants/env';
import MailClient from '../../common/libs/MailClient';
import FileService from '../../common/services/FileService';
import { addressToString } from '../../common/utils/prepareResponse';
import { ElectronicDocumentCirculationModel as edoModel } from '../../common/database/models/Edo';
import { TaxTypeModel } from '../../common/database/models/Client/Organization';
import { OrderDocumentTypeModel } from '../../common/database/models/Order';
import COMMON_ERRORS from '../../common/constants/errors';
import APIError from '../../common/utils/APIError';
import RabbitClient from '../../common/libs/RabbitClient';
import logger from '../../common/services/LoggerService';
import PaymentService from '../../common/services/PaymentService';

const validTariffOrService = Joi.array().items(Joi.object({
  id: checkRequiredId,
  serviceAddressId: checkId,
}));

/**
 *
 * @param {String} type Название шаблона
 * @param {String} [address] Адрес в виде строки
 * @param {Object} attachment Документ для отправки
 * @param {String} attachment.name Имя документа
 * @param {String} attachment.document Данные документа
 * @param {Object} contactPerson Данные пользователя
 * @param {String} contactPerson.lastName Фамилия
 * @param {String} contactPerson.firstName Имя
 * @param {String} contactPerson.patronymicName Отчество
 * @param {String} contactPerson.email Почтовый адрес
 * @param {Number} orderNumber
 * @returns {Promise<void>}
 */
const sendEmail = ({
  type, address, attachment, contactPerson, orderNumber,
}) => MailClient.document({
  attachmentsBinary: {
    [attachment?.name]: attachment?.document,
  },
  name: type,
  recipients: [{
    name: contactPerson?.fullName,
    email: contactPerson?.email,
  }],
  variables: {
    orderNumber,
    address,
  },
});

export default class OrderController extends OrderControllerCommon {
  static get validators() {
    return {
      ...super.validators,
      getInfo: Joi.object({ id: checkRequiredId }),
      addDocumentToOrder: Joi.object({
        typeId: checkRequiredId,
        orderId: checkRequiredId,
      }),
      createOrder: Joi.object({
        clientId: checkRequiredId,
        tariffs: validTariffOrService,
        services: validTariffOrService,
        description: Joi.string().allow('', null),
        paymentType: Joi.string().required().valid('CARD', 'INVOICE'),
      }).or('tariffs', 'services'),
      updateOrder: Joi.object({
        id: checkRequiredId,
        paymentDate: baseDateValidation,
        description: Joi.string().allow('', null),
        discount: Joi.number().min(0).max(100).integer()
          .positive(),
      }),
      getList: Joi.object({
        ...limitOffset,
        number: Joi.number(),
        dateFrom: dateOnly,
        dateTo: dateOnly,
        paymentType: queryWithNull(Joi.string().valid('CARD', 'INVOICE')),
        paymentDateFrom: dateOnly,
        paymentDateTo: dateOnly,
        responsibleId: checkId,
        sort: orderSort,
      }).unknown(true),
      getDocumentTypes: Joi.object(limitOffset),
      getStatus: Joi.object({ orderId: checkRequiredId }),
    };
  }

  static async getList(req, res) {
    const result = await OrderService.getList(req.query);

    return res.json(result);
  }

  static async getInfo(req, res) {
    const result = await OrderService.getInfo(req.params.id);

    return res.json(result);
  }

  static async addDocumentToOrder(req, res) {
    if (!req.file) throw new APIError(COMMON_ERRORS.FILE.FILE_IS_REQUIRED);
    const orderData = await OrderService.addDocumentToOrder({
      id: null,
      ...req.body,
      file: {
        fsName: req.file?.filename,
        name: req.file?.originalname,
      },
    });

    const allowTypeDocument = new Set([
      OrderDocumentTypeModel.TYPES.UTD_STATUS_ONE,
      OrderDocumentTypeModel.TYPES.UTD_STATUS_TWO,
    ]);

    if (!allowTypeDocument.has(req.body.typeId)) return res.json(null);


    try {
      const {
        order: {
          organization,
          ...order
        },
        document: orderDocument,
      } = orderData;

      const { extension } = FileService.getFilenameWithExtension(req.file.originalname);

      const document = fs.readFileSync(
        path.join(FileService.DEFAULT_FILE_PATH, 'order',
          `${orderDocument.id}.${extension}`), 'base64',
      );


      const attachment = {
        name: orderDocument.name,
        document,
      };

      const user = await UserService.getClientMaintainer({ clientId: order.clientId });

      const preSend = ({ type, address }) => sendEmail({
        type, address, attachment, contactPerson: user, orderNumber: order.number,
      });

      if (organization.electronicDocumentCirculation.id !== edoModel.STATUSES.EDO_IS_MISSING) {
        await preSend({ type: 'operator' });
      } else {
        if (organization.bankDetail.taxTypeId === TaxTypeModel.TYPES.OSNO) {
          const { legalAddress } = organization;

          const address = addressToString({
            ...legalAddress,
            subject: legalAddress.federationSubject.name,
          });

          await preSend({ type: 'address', address });
        }

        if (organization.bankDetail.taxTypeId === TaxTypeModel.TYPES.USN) {
          await preSend({ type: 'local' });
        }
      }
    } catch (error) {
      logger.error(error);
    }

    return res.json(null);
  }

  static async addOfferDocument(req, res) {
    const result = await OrderService.addOfferDocument({
      fsName: req.file.filename,
      name: req.file.originalname,
    });

    return res.json(result);
  }

  static async getDocumentTypes(req, res) {
    const result = await OrderService.getDocumentTypes(req.query);

    return res.json(result);
  }

  static async createOrder(req, res) {
    const { clientId } = req.body;
    const isFirstOrder = await OrderService.checkFirstOrder({ clientId });

    const {
      orderInstance,
      tariffsWithCost,
      servicesWithCost,
      offer,
    } = await OrderService.createOrder(req.body);

    const {
      id: orderId, number: orderNumber, orderDate,
    } = orderInstance.toJSON();

    res.json(orderInstance.id);

    const user = await UserService.getClientMaintainer({ clientId });

    if (SENDPULSE.IS_MAIL_SENDING_ENABLED) {
      try {
        const mailData = await MailService.prepareInvoiceForUser({
          clientId,
          clientTariffs: tariffsWithCost,
          clientServices: servicesWithCost,
          createdAt: orderDate,
        });

        const template = MailService.createInvoiceTemplate(mailData.goods);

        await MailClient.order({
          name: 'issuedAndNotPaid',
          recipients: [{
            email: SENDPULSE.MAIL_RECIPIENT,
          }, {
            name: `${user.lastName} ${user.firstName} ${user.patronymicName}`,
            email: user.email,
          }],
          variables: {
            orderNumber,
            totalCost: mailData.totalCost,
            data: template,
          },
        });
      } catch (error) {
        logger.error(error);
      }
    }

    if (INTEGRATOR.IS_SENDING_ENABLED) {
      try {
        const client = new RabbitClient();
        await client.sendToQueue({
          queue: client.queues.integrationCreateOrder,
          message: {
            orderId,
            orderNumber,
            clientId,
            isFirstOrder,
            userId: user?.id,
            offer,
            createdAt: moment(orderDate).format('YYYY.MM.DD.HH.mm.ss'),
            services: servicesWithCost,
            tariffs: tariffsWithCost,
          },
        });
      } catch (error) {
        logger.error(error);
      }
    }
  }

  static async updateOrder(req, res) {
    const { paymentDate } = req.body;
    const {
      id,
      number: orderNumber,
      client,
    } = await OrderService.updateOrder(req.body);

    if (!paymentDate) return res.json(id);

    const payment = await PaymentService.getInfoByOrder(req.body.id);

    const text = `Поступила оплата по заказу ${orderNumber} клиента ${client.organization?.name}`;

    const user = await UserService.getClientMaintainer({ clientId: client.id });

    if (!payment?.paymentDate && user) {
      try {
        await MailClient.sendMessage({
          subject: text,
          text,
          recipients: [{
            email: SENDPULSE.MAIL_RECIPIENT_CLIENT_MANAGER,
          }],
        });

        if (paymentDate && moment().isSameOrAfter(moment(paymentDate), 'day')) {
          await MailClient.order({
            name: 'paid',
            recipients: [{
              name: `${user.lastName} ${user.firstName} ${user.patronymicName}`,
              email: user.email,
            }],
            variables: {
              orderNumber,
            },
          });
        }
      } catch (error) {
        logger.error(error);
      }
    }
    return res.json(id);
  }

  static async getStatus(req, res) {
    const paymentInstance = await OrderService.getPayment(req.params.orderId);
    const { order, paymentOrderId } = paymentInstance.toJSON();
    if (!is.order.status.notPaid(order.orderStatusId)) return res.json(order.orderStatusId);
    const statusId = await OrderService.requestStatus({ order: paymentInstance.order, paymentOrderId });
    if (is.order.status.paid(statusId)) {
      await OrderController.completeOrder(paymentOrderId);
    }
    return res.json(statusId);
  }
}
