import Joi from '@hapi/joi';
import {
  checkId, checkPhone, checkRequiredId, dateOnly, innValidator,
  limitOffset, organization, userRegistration, clientSort,
} from '../../common/utils/validation';
import ClientService from '../services/ClientService';
import ClientControllerCommon from '../../common/controllers/ClientController';

export default class ClientController extends ClientControllerCommon {
  static get validators() {
    const baseFilterParameters = {
      number: Joi.number(),
      dateFrom: dateOnly,
      dateTo: dateOnly,
      responsibleId: checkId,
    };

    const leadId = checkId;

    const baseClientRegistration = {
      leadId,
      user: userRegistration,
    };

    const individualAndOrganizationParameters = {
      phone: checkPhone(),
      email: Joi.string().allow(''),
    };
    return {
      getAll: Joi.object({
        ...limitOffset,
        ...baseFilterParameters,
        name: Joi.string().allow(''),
        client: Joi.string().valid('all', 'individual', 'organization', ''),
        sort: clientSort,
      }).unknown(true),
      getOrganizations: Joi.object({
        ...limitOffset,
        ...baseFilterParameters,
        ...individualAndOrganizationParameters,
        name: Joi.string().allow(''),
        inn: innValidator,
        representative: Joi.string().allow(''),
      }).unknown(true),
      getIndividuals: Joi.object({
        ...limitOffset,
        ...baseFilterParameters,
        ...individualAndOrganizationParameters,
        phone: checkPhone(),
        firstName: Joi.string().allow(''),
        lastName: Joi.string().allow(''),
      }).unknown(true),
      getInfo: Joi.object({ id: checkRequiredId }),
      registerOrganization: Joi.object({ ...baseClientRegistration, organization }),
      registerIndividual: Joi.object(baseClientRegistration),
      getClientAllowedServices: Joi.object({
        clientId: checkRequiredId,
      }),
    };
  }

  static async getAll(req, res) {
    const result = await ClientService.getAll(req.query);
    return res.json(result);
  }

  static async getOrganizations(req, res) {
    const result = await ClientService.getOrganizations(req.query);
    return res.json(result);
  }

  static async getIndividuals(req, res) {
    const result = await ClientService.getIndividuals(req.query);
    return res.json(result);
  }

  static async registerClient(req, res) {
    const clientInstance = await ClientService.registerClient(req.body);
    return res.json(clientInstance.client.id);
  }

  static async getInfo(req, res) {
    const result = await ClientService.getInfo(req.params.id);
    return res.json(result);
  }

  static async getClientAllowedServices(req, res) {
    const services = await super.getClientAllowedServices(req.query.clientId);

    return res.json(services);
  }
}
