import CallCategoryService from '../services/CallCategoryService';

export default class CallCategoryController {
  static async getList(req, res) {
    const result = await CallCategoryService.getList();

    return res.json(result);
  }
}
