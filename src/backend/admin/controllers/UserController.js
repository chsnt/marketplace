import Joi from '@hapi/joi';

import UserService from '../services/UserService';
import UserControllerCommon from '../../common/controllers/UserController';

import {
  search, checkId, checkRequiredId, limitOffset, dateOnly, checkPhone,
} from '../../common/utils/validation';
import OrganizationService from '../../common/services/OrganizationService';

import APIError from '../../common/utils/APIError';
import ERRORS from '../constants/errors';

export default class UserController extends UserControllerCommon {
  static get validators() {
    return {
      ...super.validators,
      resetPassword: Joi.object({ id: checkRequiredId }),
      getUsers: Joi.object({
        ...limitOffset,
        search,
        userGroupId: checkId,
        type: Joi.string().valid('admin', 'any', 'user'),
      }).oxor('userGroupId', 'type'),
      updateUser: Joi.object({
        id: checkRequiredId,
        firstName: Joi.string(),
        lastName: Joi.string(),
        patronymicName: Joi.string().allow(null, ''),
        email: Joi.string().email(),
        birthday: dateOnly,
        phone: checkPhone(),
        delete: Joi.boolean(),
      }),
    };
  }

  static async resetPassword(req, res) {
    await UserService.resetPasswordByAdmin(req.params.id);

    return res.json(null);
  }

  static async updateUser(req, res) {
    const isOrganizationMaintainer = await OrganizationService.checkUserMaintainer({ userId: req.body.id });

    if (isOrganizationMaintainer && req.body.delete) {
      throw new APIError(ERRORS.USER.DELETE_MAINTAINER);
    }

    await UserService.updateData(req.body);

    return res.json(null);
  }

  static async getUserGroups(req, res) {
    const userGroups = await UserService.getUserGroups(req.body);
    return res.json(userGroups);
  }

  static async getUsers(req, res) {
    const users = await UserService.getUsers(req.query);
    return res.json(users);
  }

  static async getInfo(req, res) {
    const {
      fullName,
      lastName,
      patronymicName,
      email,
      phone,
      birthday,
    } = req.user;
    return res.json({
      fullName,
      lastName,
      patronymicName,
      email,
      phone,
      birthday,
    });
  }
}
