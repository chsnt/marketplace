import Joi from '@hapi/joi';
import moment from 'moment';
import {
  baseDateValidation, checkId, checkRequiredId, createClientDevice,
  dateOnly, limitOffset, queryWithNull,
} from '../../common/utils/validation';
import TicketService from '../services/TicketService';
import { TicketStatusModel } from '../../common/database/models/Ticket';
import TicketControllerCommon from '../../common/controllers/TicketController';
import is from '../../common/libs/is';
import MailClient from '../../common/libs/MailClient';
import UserService from '../../common/services/UserService';
import { DATE_FORMAT } from '../../common/constants';
import DeviceTypeModel from '../../common/database/models/Device/DeviceTypeModel';
import logger from '../../common/services/LoggerService';
import ServiceService from '../../common/services/ServiceService';

/**
 *
 * @param {String} type Название шаблона
 * @param {String} description Дополнительное описание
 * @param {Object.<Date>} createdAt Дата создания заявки
 * @param {Object} contactPerson Данные пользователя
 * @param {String} contactPerson.lastName Фамилия
 * @param {String} contactPerson.firstName Имя
 * @param {String} contactPerson.patronymicName Отчество
 * @param {String} contactPerson.email Почтовый адрес
 * @param {String} serial Номер ЗНО
 * @return {Promise<void>}
 */
const sendEmail = ({
  type, description, createdAt, contactPerson = {}, serial,
}) => MailClient.ticket({
  name: type,
  recipients: [{
    name: contactPerson?.fullName,
    email: contactPerson?.email,
  }],
  variables: {
    serial,
    createdAt: moment(createdAt).format(DATE_FORMAT.KEBAB_NORMAL),
    description,
  },
});


export default class TicketController extends TicketControllerCommon {
  static get validators() {
    return {
      ...super.validators,
      getList: Joi.object({
        ...limitOffset,
        serial: Joi.string().allow(''),
        inn: Joi.number(),
        statusId: checkId,
        services: Joi.array().items(checkId),
        clientId: checkId,
        responsibleId: queryWithNull(checkId),
        clientName: Joi.string().allow(''),
        serviceDeskId: Joi.string().allow(''),
        dateFrom: dateOnly,
        dateTo: dateOnly,
        nextCallDateFrom: baseDateValidation,
        nextCallDateTo: baseDateValidation,
        deviceTypeId: Joi.string().valid(DeviceTypeModel.TYPES.KKT, DeviceTypeModel.TYPES.ARM, ''),
        client: Joi.string().valid('all', 'individual', 'organization', 'empty', ''),
      }).unknown(true),
      getInfo: Joi.object({ id: checkRequiredId }),
      createTicket: Joi.object({
        statusId: checkId.default(TicketStatusModel.STATUSES.NEW),
        serviceAddressId: checkId,
        clientId: checkRequiredId,
        services: Joi.array().items(checkId).min(1).required(),
        supportRequestId: checkId,
        description: Joi.string().allow('').max(255),
        solution: Joi.string().allow('').max(255),
        comment: Joi.string().allow('').max(255),
        nextCallDate: baseDateValidation.allow(null),
        device: Joi.object(createClientDevice),
        laborCosts: Joi.number(),
      }),
      updateTicket: Joi.object({
        ticketId: checkRequiredId,
        statusId: checkId,
        responsibleId: checkId,
        serviceDeskId: Joi.string().empty('').default(null),
        services: Joi.array().items(checkId).min(1).required(),
        solution: Joi.string().allow(''),
        comment: Joi.string().allow(''),
        userId: checkId,
        nextCallDate: baseDateValidation.allow(null),
        laborCosts: Joi.number(),
      }),
    };
  }

  static async getList(req, res) {
    const result = await TicketService.getList(req.query);

    return res.json(result);
  }

  static async getInfo(req, res) {
    const result = await TicketService.getInfo(req.params.id);

    return res.json(result);
  }

  static async createTicket(req, res) {
    const docNames = req.files.map((file) => ({ fsName: file.filename, name: file.originalname }));
    const user = await UserService.getClientMaintainer({ clientId: req.body.clientId });

    const result = await super.createTicket({
      user: { ...user.toJSON(), clientId: req.body.clientId }, docNames, ...req.body,
    });

    return res.json(result.id);
  }

  static async getStatuses(req, res) {
    return res.json(await TicketService.getStatuses());
  }

  static async updateTicket(req, res) {
    const { statusId, services } = req.body;
    const docNames = req.files.map((file) => ({ fsName: file.filename, name: file.originalname }));

    await ServiceService.checkSameDeviceType(services);

    const ticketInstance = await TicketService.updateTicket({
      ...req.body,
      docNames,
    });

    try {
      const {
        clientDevice: { deviceModel: { ...deviceModel }, client: { organization, users: clientUsers } },
        ticketStatus: { name: statusName },
        number,
        comment,
        solution,
        createdAt,
      } = ticketInstance.toJSON();

      const users = organization?.users || clientUsers;
      const contactPerson = users.find((user) => user.isMaintainer) || users[0];

      const alias = TicketService.getTicketNumberPrefix(deviceModel?.deviceType.alias);

      const serial = `${alias}${number}`;

      const preSend = ({ type, description }) => sendEmail({
        type, description, createdAt, contactPerson, serial,
      });

      if (is.ticket.status.cancelled(statusId)) {
        await preSend({ type: 'cancelled', description: comment });
      } else if (is.ticket.status.completed(statusId)) {
        await preSend({ type: 'completed', description: solution });
      } else {
        await preSend({ type: 'status', description: statusName });
      }
    } catch (error) {
      logger.error(error);
    }


    return res.json(ticketInstance.id);
  }
}
