import ServiceService from '../../common/services/ServiceService';
import ServiceControllerCommon from '../../common/controllers/ServiceController';
import AvailableCatalogStatusModel from '../../common/database/models/Tariff/AvailableCatalogStatusModel';

export default class ServiceController extends ServiceControllerCommon {
  static async getList(req, res) {
    const result = await ServiceService.getList({
      ...req.query,
      isCostRequired: true,
      forTariffOnly: false,
      availableCatalogStatusId: AvailableCatalogStatusModel.STATUSES.ADMIN,
    });
    res.json(result);
  }

  static async getAllList(req, res) {
    const result = await ServiceService.getList({
      ...req.query,
      isCostRequired: false,
    });
    res.json(result);
  }

  static async getService(req, res) {
    const result = await ServiceService.getService({ ...req.params, isCostRequired: true });
    res.json(result);
  }
}
