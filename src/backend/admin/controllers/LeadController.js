import Joi from '@hapi/joi';
import {
  checkRequiredId, checkId, checkPhone, search, queryWithNull,
  dateOnly, baseDateValidation, limitOffset, leadSort,
} from '../../common/utils/validation';
import LeadService from '../services/LeadService';

export default class LeadController {
  static get validators() {
    const leadValidation = {
      isOrganization: Joi.boolean().required(),
      deviceTypeId: checkRequiredId,
      clientId: checkId,
      firstName: Joi.string().required(),
      patronymicName: Joi.string().allow(null, ''),
      lastName: Joi.string().allow(null, ''),
      phone: checkPhone({ mobileOnly: false }).required(),
      email: Joi.string().email().allow(null),
      cityId: checkId.allow(null),
      leadNeedId: checkRequiredId,
      deviceManufacturerId: checkId.allow(null),
      deviceModelId: checkId.allow(null),
      comment: Joi.string().allow(null, ''),
      leadStatusId: checkId,
      leadConsultationResultId: checkId,
      nextCallDate: baseDateValidation.allow(null),
      operationSystem: Joi.string().allow(null),
      leadSourceId: checkId,
      tariffs: Joi.array().items(checkId),
      services: Joi.array().items(checkId),
    };

    return {
      getList: Joi.object({
        ...limitOffset,
        number: Joi.number(),
        leadNeedId: checkId,
        deviceType: checkId,
        clientId: queryWithNull(checkId),
        phone: checkPhone({ mobileOnly: false }),
        email: Joi.string().email(),
        cityId: queryWithNull(checkId),
        deviceManufacturerId: queryWithNull(checkId),
        deviceModelId: queryWithNull(checkId),
        leadStatusId: checkId,
        leadConsultationResultId: checkId,
        responsibleId: checkId,
        dateFrom: dateOnly,
        dateTo: dateOnly,
        representative: search,
        nextCallDateFrom: baseDateValidation,
        nextCallDateTo: baseDateValidation,
        client: Joi.string().valid('all', 'individual', 'organization', 'empty', ''),
        sort: leadSort,
      }).unknown(true),
      getInfo: Joi.object({ id: checkRequiredId }),
      createLead: Joi.object(leadValidation),
      updateLead: Joi.object({
        id: checkRequiredId,
        ...leadValidation,
      }),
    };
  }

  static async getList(req, res) {
    const result = await LeadService.getList(req.query);
    return res.json(result);
  }

  static async getInfo(req, res) {
    const result = await LeadService.getInfo(req.params.id);
    return res.json(result);
  }

  static async getStatuses(req, res) {
    const result = await LeadService.getStatuses();
    return res.json(result);
  }

  static async getSources(req, res) {
    const result = await LeadService.getSources();
    return res.json(result);
  }

  static async getConsultationResults(req, res) {
    const result = await LeadService.getConsultationResults();
    return res.json(result);
  }

  static async getNeeds(req, res) {
    const result = await LeadService.getNeeds();
    return res.json(result);
  }

  static async createLead(req, res) {
    const result = await LeadService.createLead({
      ...req.body,
      formId: 'ADMIN',
    });

    return res.json(result.id);
  }

  static async updateLead(req, res) {
    const result = await LeadService.updateLead(req.body);

    return res.json(result.id);
  }
}
