import MenuService from '../services/MenuService';

const prepareListResult = (items) => items.map(({
  pageComponent, isCustomPage, defaultIcon, activeIcon, children, ...item
}) => ({
  ...item,
  pageComponent: pageComponent || 'default',
  isCustomPage: Boolean(isCustomPage),
  defaultIcon: defaultIcon || '',
  activeIcon: activeIcon || '',
  children: children ? prepareListResult(children) : [],
  views: undefined,
}));

export default class MenuController {
  static async getList(req, res) {
    const items = await MenuService.getList();
    res.json(prepareListResult(items));
  }
}
