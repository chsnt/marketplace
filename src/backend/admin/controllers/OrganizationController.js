import Joi from '@hapi/joi';
import IntegrationExportService from '../../common/services/IntegrationExportService';
import { INTEGRATOR } from '../../common/constants/env';
import RabbitClient from '../../common/libs/RabbitClient';
import logger from '../../common/services/LoggerService';
import OrganizationService from '../../common/services/OrganizationService';

import {
  bankDetail, checkAddress, checkId, organization, checkRequiredId,
} from '../../common/utils/validation';

export default class OrganizationController {
  static get validators() {
    return {
      updateOrganization: Joi.object({
        id: checkRequiredId,
        ...organization,
        maintainerId: checkId,
        users: Joi.array().items(checkId),
        legalAddress: Joi.object(checkAddress).oxor('cityId', 'city', 'fiasId').rename('region', 'federationSubjectId'),
        realAddress: Joi.object(checkAddress).oxor('cityId', 'city', 'fiasId').rename('region', 'federationSubjectId'),
        bankDetail: Joi.object(bankDetail),
      }),
    };
  }

  static async updateOrganization(req, res) {
    const bankDetails = await OrganizationService.getBankInfo({ organizationId: req.body.id });
    const organizationData = await OrganizationService.updateInfo(req.body);

    if (INTEGRATOR.IS_SENDING_ENABLED && IntegrationExportService.checkChangedBankDetails({
      oldBankDetails: bankDetails,
      newBankDetails: organizationData.bank,
    })) {
      try {
        const client = new RabbitClient();
        await client.sendToQueue({
          queue: client.queues.integrationChangedBankDetails,
          message: {
            id: organizationData.id,
            bank: organizationData.bank,
          },
        });
      } catch (error) {
        logger.error(error);
      }
    }

    return res.json(organizationData);
  }
}
