import Joi from '@hapi/joi';
import { dateValidationWithTimeZone, checkRequiredId } from '../../common/utils/validation';
import CostService from '../services/CostService';

export default class CostController {
  static get validators() {
    return {
      addCost: Joi.object({
        id: checkRequiredId,
        cost: Joi.number(),
        startAt: dateValidationWithTimeZone.required(),
        expirationAt: dateValidationWithTimeZone,
      }),
    };
  }

  static async addCost(req, res) {
    await CostService.addCost(req.body);
    res.json(null);
  }
}
