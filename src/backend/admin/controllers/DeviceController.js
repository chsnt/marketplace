import Joi from '@hapi/joi';
import {
  createClientDevice, checkRequiredId, checkId,
} from '../../common/utils/validation';
import DeviceService from '../../common/services/DeviceService';
import DeviceControllerCommon from '../../common/controllers/DeviceController';

export default class DeviceController extends DeviceControllerCommon {
  static get validators() {
    return {
      ...super.validators,
      createClientDevice: Joi.object({
        ...createClientDevice,
        clientId: checkRequiredId,
      }),
      getList: Joi.object({
        deviceModelName: Joi.string().allow(''),
        clientName: Joi.string().allow(''),
        parameterValue: Joi.string().allow(''),
        clientId: checkId,
        deviceTypeId: checkId,
        deviceModelId: checkId,
        deviceManufacturerId: checkId,
      }),
      getDeviceModels: Joi.object({
        deviceManufacturerId: checkId.allow('', null),
      }),
      getParameterList: Joi.object({
        id: checkRequiredId,
      }),
    };
  }

  static async createClientDevice(req, res) {
    const { clientId, ...device } = req.body;

    const result = await DeviceService.createClientDevice({
      device,
      clientId,
    });

    return res.json(result);
  }

  static async getList(req, res) {
    const result = await DeviceService.getClientDevices(req.query);

    return res.json(result);
  }

  static async getDeviceManufacturers(req, res) {
    const result = await DeviceService.getManufacturerList();

    return res.json(result);
  }

  static async getDeviceModels(req, res) {
    const result = await DeviceService.getDeviceModels(req.query.deviceManufacturerId);

    return res.json(result);
  }

  static async getParameterList(req, res) {
    const result = await DeviceService.getDeviceModelParameters(req.params.id);
    return res.json(result);
  }

  static async getDeviceTypes(req, res) {
    const result = await DeviceService.getDeviceTypes({ skipDefault: true });

    return res.json(result);
  }
}
