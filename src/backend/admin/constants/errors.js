import httpStatus from 'http-status';

import COMMON_ERRORS from '../../common/constants/errors';

const ERRORS = {
  MENU: {
    MENU_NOT_FOUND: (id) => ({
      error: `Меню "${id}" не найдено`,
      errorCode: 'MenuNotFound',
      statusCode: httpStatus.NOT_FOUND,
    }),
  },
  ORGANIZATION: {
    CLIENT_EXISTS: (name) => ({
      statusCode: httpStatus.BAD_REQUEST,
      error: `В системе уже есть клиент с указанными реквизитами. ${name}`,
      errorCode: 'CLIENT_EXISTS',
    }),
  },
  COST: {
    CREATE_COST: {
      error: 'Цена не создана',
      errorCode: 'CREATE_COST',
      statusCode: httpStatus.NOT_FOUND,
    },
  },
  ...COMMON_ERRORS,
};

export default ERRORS;
