import { Passport } from 'passport';
import { setup } from '../common/utils/passport';

const passportInstance = new Passport();

setup({ passportInstance, isAdmin: true });

export default passportInstance;
