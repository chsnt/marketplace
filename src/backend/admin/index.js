import server from './server';
import logger from '../common/services/LoggerService';
import db from '../common/database';

const { databaseConnect } = db;

const startServer = () => server.listen(process.env.PORT || 3000, (err) => {
  if (err) throw err;
  logger.info(`> App started on http://localhost:${process.env.PORT}`);
});

databaseConnect(process.env.SHOULD_MIGRATE === 'true')
  .then(startServer)
  .catch((err) => {
    logger.error('App start failed: ', err);
    process.exit(1);
  });
