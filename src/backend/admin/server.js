import express from 'express';
import path from 'path';
import ejs from 'ejs';
import compression from 'compression';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import httpContext from 'express-http-context';
import session from 'express-session';
import connectRedis from 'connect-redis';
import responseTime from 'response-time';
import RedisService from '../common/services/RedisService';
import logger from '../common/services/LoggerService';
import { errorHandler, logRequest, memoResponseBody } from '../common/utils/middlewares';
import routes from './routes';
import passportInstance from './passportInstance';
import asyncErrorHandler from '../common/libs/asyncErrorHandler';
import webRoute from './routes/web';
import { COOKIE_HTTP_ONLY, COOKIE_SECURE } from '../common/constants/env';
import is from '../common/libs/is';


process.on('unhandledRejection', (err) => {
  logger.error(err);
});

const server = express();

if (!is.env.development()) server.use(compression());

server.engine('html', ejs.renderFile);

server.set('view engine', 'html');
server.set('views', path.join(__dirname, '/resources/pages'));

server.use('/apidoc', express.static(path.resolve(__dirname, '../../static/apidoc-admin')));

server.use(cookieParser());
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json({ limit: '10mb' }));

server.use(express.static('build/public'));

server.use(session({
  store: new (connectRedis(session))({ client: new RedisService().client }),
  name: 'marketplace-admin.sid',
  cookie: {
    maxAge: process.env.SESSION_EXPIRE || 30 * 60 * 1000,
    httpOnly: COOKIE_HTTP_ONLY,
    secure: COOKIE_SECURE,
    sameSite: 'strict',
  },
  resave: false,
  unset: 'destroy',
  proxy: true,
  rolling: true,
  saveUninitialized: false,
  secret: process.env.SESSION_SECRET || 'sessionsecret123',
}));

server.use(passportInstance.initialize());
server.use(passportInstance.session());

server.use(responseTime({ suffix: false }));

server.use((req, res, next) => {
  const oldJson = res.json;
  res.defaultJson = oldJson;

  res.json = (data = null, success = true, error = null, errorCode = null) => {
    const response = {
      data,
      success,
      error,
      errorCode,
    };

    oldJson.call(res, response);
  };

  return next();
});

server.use(memoResponseBody);
server.use(asyncErrorHandler(logRequest));

server.use(httpContext.middleware);

/* API routes */
server.use('/api', routes);

server.get('*', webRoute);

/* routes error handler */
server.use(errorHandler);

export default server;
