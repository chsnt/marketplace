import { Router } from 'express';
import is from '../../common/libs/is';

const router = new Router();

const isDevelopment = is.env.development();


router.get('/signin', (req, res) => {
  if (req.isAuthenticated && req.isAuthenticated()) {
    res.redirect('/main');
  }
  return res.render('main.html', { isDevelopment });
});

router.get('/main', (req, res) => {
  if (req.isAuthenticated && req.isAuthenticated()) {
    return res.render('main.html', { isDevelopment });
  }
  return res.redirect('/signin');
});

/* Редиректим на страницу авторизации */
router.use(/\/$/, (req, res) => res.redirect('/signin'));

export default router;
