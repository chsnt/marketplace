import { Router } from 'express';

import auth from './auth';
import protectedRoutes from './protected';
import { isAuthenticated } from '../../common/utils/middlewares';
import ERRORS from '../constants/errors';

const router = new Router();

router.use('/auth', auth);

router.use('/', isAuthenticated, protectedRoutes);

/* non - existing routes */
router.use('/', (req, res, next) => {
  res.status(ERRORS.COMMON.ROUTE_NOT_FOUND.statusCode).json(
    null, false, ERRORS.COMMON.ROUTE_NOT_FOUND.error, ERRORS.COMMON.ROUTE_NOT_FOUND.errorCode,
  );
  next();
});

export default router;
