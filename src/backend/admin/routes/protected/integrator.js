import { Router } from 'express';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';
import validator from '../../../common/libs/requestValidator';
import IntegratorController from '../../controllers/IntegratorController';

const router = new Router();

/**
 *  @api {post} /api/integrator/create Создание интегратора
 *  @apiVersion 1.0.0
 *  @apiName Create
 *  @apiGroup Integrator
 *  @apiParam (Body) {UUID} token Токен интегратора
 *  @apiParam (Body) {String{2..20}} name Название интегратора
 *  @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *   {
 *     data: null,
 *     success: true,
 *     error: 'error message' | null,
 *     errorCode: 'ErrorCode' | null,
 *   }
 */
router.post(
  '/create',
  validator.body(IntegratorController.validators.createIntegrator),
  asyncErrorHandler(IntegratorController.createIntegrator),
);


/**
 *  @api {post} /api/integrator/:id/update Обновить данные интегратора
 *  @apiVersion 1.0.0
 *  @apiName Update
 *  @apiGroup Integrator
 *  @apiParam (Params) {UUID} id UUID Идентификатор токена интегратора
 *  @apiParam (Body) {UUID} [token] Токен интегратора
 *  @apiParam (Body) {String{2..20}} [name] Название интегратора
 *  @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *   {
 *     data: null,
 *     success: true,
 *     error: 'error message' | null,
 *     errorCode: 'ErrorCode' | null,
 *   }
 */
router.post(
  '/:id/update',
  validator.params(IntegratorController.validators.requiredId),
  validator.body(IntegratorController.validators.updateIntegrator),
  asyncErrorHandler(IntegratorController.updateIntegrator),
);


/**
 *  @api {get} /api/integrator/:id/get-info Получить данные интегратора
 *  @apiVersion 1.0.0
 *  @apiName getInfo
 *  @apiGroup Integrator
 *  @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *   {
 *     data: {
 *       id: '54649c2b-c239-4601-b8d6-b943d5517a78',
 *       token: 'b85f807a-5db7-4df4-9506-408f54f48caf',
 *       name: '2C',
 *     },
 *     success: true,
 *     error: 'error message' | null,
 *     errorCode: 'ErrorCode' | null,
 *   }
 */
router.get(
  '/:id/get-info',
  validator.params(IntegratorController.validators.requiredId),
  asyncErrorHandler(IntegratorController.getInfo),
);


/**
 *  @api {post} /api/integrator/get-list Получить список интеграторов
 *  @apiVersion 1.0.0
 *  @apiName getList
 *  @apiGroup Integrator
 *  @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *   {
 *     data: {
 *       count: 1,
 *       row: [{
 *         id: '8fb790dc-c42c-4e29-b007-33cb488a5953',
 *         token: 'cbb1db17-bd47-46bb-9e53-bdb9bcfd66c8',
 *         name: '1C'
 *       }]
 *     },
 *     success: true,
 *     error: 'error message' | null,
 *     errorCode: 'ErrorCode' | null,
 *   }
 */
router.get(
  '/get-list',
  asyncErrorHandler(IntegratorController.getList),
);

export default router;
