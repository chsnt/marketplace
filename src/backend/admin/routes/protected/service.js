import { Router } from 'express';
import validator from '../../../common/libs/requestValidator';

import ServiceController from '../../controllers/ServiceController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';

const router = new Router();

/**
 * @api {get} /api/service/get-list Список услуг доступных к приобретению
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup Service
 * @apiParam (Query) {String} search Поиск
 * @apiParam (Query) {UUID} [deviceTypeId] Идентификатор типа оборудования.
 * @apiDescription если у свойства `cost` значение null, значит услуга недоступна к приобретению
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *         {
 *           id: '7db9aa02-7f43-4462-a677-0c1973ef462e',
 *           name: 'Название услуги',
 *           cost: 500 | null,
 *           label: 'some label',
 *           icon: 'some icon',
 *           alias: 'asd',
 *           description: "Описание услуги",
 *           serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *           deviceType: {
 *             id: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *             name: 'ККТ',
 *             alias: 'KKT'
 *           } | null,
 *         }
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-list',
  validator.query(ServiceController.validators.getList),
  asyncErrorHandler(ServiceController.getList),
);

/**
 * @api {get} /api/service/get-all-list Список всех услуг
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup Service
 * @apiParam (Query) {String} search Поиск
 * @apiDescription если у свойства `cost` значение null, значит услуга недоступна к приобретению
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *         {
 *           id: "7db9aa02-7f43-4462-a677-0c1973ef462e",
 *           name: "Название услуги",
 *           cost: 500 | null,
 *           label: "some label",
 *           icon: "some icon",
 *           alias: "asd",
 *           description: "Описание услуги",
 *           serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *         }
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-all-list',
  validator.query(ServiceController.validators.getAllList),
  asyncErrorHandler(ServiceController.getAllList),
);

/**
 * @api {get} /api/service/get-completed-list Список завершенных услуг
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup Service
 * @apiParam (Query) {UUID} clientId ID организации
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          clientServiceId: 'b7789e4b-9006-4b60-9d55-3e7a46d00022',
 *          completed: '2021-03-22T08:48:00.697Z',
 *          id: '923a5fab-1c6d-466d-9df3-068d2b2f58b2',
 *          name: 'Код активации для продления ОФД на 15 месяцев',
 *          alias: 'kod-aktivatsii-ofd-15-mes',
 *          label: null,
 *          icon: 'iconOfd',
 *          serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *          deviceTypeId: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *          serviceAddress: {
 *            id: '7ebdec1f-edd6-4c9c-943d-1c70d8db5ffa',
 *            street: 'улица Ленина',
 *            house: '1',
 *            apartment: '123',
 *            postalCode: '555888',
 *            fiasId: null,
 *            city: {
 *              id: 'fbfc5bc7-6a43-4b07-b2b7-17960dc81bc9',
 *              name: 'Саров',
 *              utcOffset: 0,
 *            },
 *            region: {
 *              id: '4680dce7-ac13-410c-8b7d-a467845fe7ed',
 *              name: 'Республика Удмуртия',
 *            },
 *          },
 *          device: {
 *            id: 'e60bc9ea-577c-401d-85c1-7ed964d5fe51',
 *            type: 'Поддержка АРМ',
 *            model: 'ПК',
 *            parameters: [
 *              {
 *                value: 'Windows10',
 *                name: 'Операционная система',
 *                alias: 'OPERATION_SYSTEM',
 *              },
 *              {
 *                value: 'Zver',
 *                name: 'Модель',
 *                alias: 'MODEL',
 *              },
 *              {
 *                value: 'Asus',
 *                name: 'Производитель',
 *                alias: 'MANUFACTURER',
 *              },
 *            ],
 *          },
 *        },
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-completed-list',
  validator.query(ServiceController.validators.getCompletedList),
  asyncErrorHandler(ServiceController.getCompletedList),
);

/**
 * @api {get} /api/service/:id/get-info Данные услуги
 * @apiVersion 1.0.0
 * @apiName GetInfo
 * @apiGroup Service
 * @apiParam (Params) {UUID} id Идентификатор услуги
 * @apiParamExample Пример запроса:
 *    http://host/api/service/779b0a45-590e-41ca-9f1f-e4562f97bfc5/get-info
 * @apiSuccessExample {json} Пример ответа:
 *  HTTP/1.1 200 OK
 * {
 *      'data': {
 *        id: 'a4e9ffe6-4190-4c2e-9496-54844829cc0f',
 *        name: 'Экстренный выезд инженера',
 *        label: null,
 *        icon: 'iconEngineer',
 *        alias: 'ehkstrennyj-vyezd-inzhenera',
 *        description: 'Описание услуги',
 *        serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *        costs: [
 *          {
 *            value: 1500,
 *            startAt: '2021-02-02T16:17:31.000Z',
 *            expirationAt: null,
 *          },
 *          {
 *            value: 1700,
 *            startAt: '2020-10-20T15:07:55.812Z',
 *            expirationAt: '2021-02-02T00:29:37.000Z',
 *          },
 *          {
 *            value: 1800,
 *            startAt: '2021-02-20T15:07:55.812Z',
 *            expirationAt: '2021-02-26T00:29:37.000Z',
 *          },
 *        ],
 *      },
 *      'success': true,
 *      'error': 'error message' | null,
 *      'errorCode': 'ErrorCode' | null
 * }
 */
router.get(
  '/:id/get-info',
  validator.params(ServiceController.validators.getService),
  asyncErrorHandler(ServiceController.getService),
);


/**
 * @api {get} /api/service/update Обновить данные услуги
 * @apiVersion 1.0.0
 * @apiName Update
 * @apiGroup Service
 * @apiParam (Body) {UUID} id Идентификатор услуги
 * @apiParam (Body) {UUID} [availableCatalogStatusId] ID статуса доступа
 * @apiParam (Body) {UUID} [availablePaymentMethodId] ID доступного способа оплаты
 * @apiParam (Body) {UUID} [externalId] ID в системе интегратора
 * @apiParam (Body) {Boolean} [forTariffOnly] true - если доступен только в рамках тарифа
 * @apiParam (Body) {String{1..255}} [name] Название тарифа
 * @apiParam (Body) {String{1..255}} [label] Лейбл тарифа
 * @apiParam (Body) {String{1..255}} [icon] Название файла ико
 * @apiParam (Body) {String{1..255}} [alias] URL тарифа
 * @apiParam (Body) {String{1..255}} [nameInTariff] Название в списке тарифа
 * @apiParam (Body) {String{..1024}} [description] Название в списке тарифа
 * @apiSuccessExample {json} Пример ответа:
 *  HTTP/1.1 200 OK
 * {
 *      "data": null,
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 * }
 */
router.post(
  '/update',
  validator.body(ServiceController.validators.updateService),
  asyncErrorHandler(ServiceController.updateService),
);

export default router;
