import { Router } from 'express';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';
import DiscountController from '../../../common/controllers/DiscountController';
import validator from '../../../common/libs/requestValidator';

const router = new Router();

/**
 * @api {get} /api/discount/get-template-list Получить список шаблонов
 * @apiVersion 1.0.0
 * @apiName GetTemplateList
 * @apiGroup Discount
 * @apiParam (Query) {Number} [limit=10] Кол-во записей
 * @apiParam (Query) {Number} [offset=0] Сдвиг
 * @apiParam (Query) {Array=["date","DESC"]} [sort=["date","DESC"]] Первым элементом идет название сортировки, вторым направление сортировки
 *
 * @apiSuccess (Success 200) {Number} count Общее кол-во записей
 * @apiSuccess (Success 200) {Object[]} rows Массив с шаблонами
 * @apiSuccess (Success 200) {UUID} row.id Id шаблона
 * @apiSuccess (Success 200) {String} row.description Описание
 * @apiSuccess (Success 200) {Number} row.timeToLive Кол-во дней действия. Если значение null, то безлимитным по времени
 * @apiSuccess (Success 200) {Number} row.limit Кол-во раз использования для одного клиента. Если значение null, то считается безлимитным по кол-во раз использования
 * @apiSuccess (Success 200) {Number} row.value Значение скидки
 * @apiSuccess (Success 200) {Object} row.discountType Тип шаблона
 * @apiSuccess (Success 200) {UUID} row.discountType.id Id типа
 * @apiSuccess (Success 200) {String} row.discountType.name Имя
 * @apiSuccess (Success 200) {String} row.discountType.alias Алиас
 *
 * @apiParamExample Пример запроса:
 * http://host/api/discount/get-template-list?sort=["date", "ASC"]&limit=15&offset=1
 *
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: {
 *        count: 2,
 *        rows: [
 *          {
 *            id: '8efd468d-aab2-4c4a-b29b-9ab098ab528c',
 *            description: 'Шаблон для купона "OPORA"',
 *            timeToLive: null,
 *            limit: 1,
 *            value: 5,
 *            discountType: {
 *              id: 'c764b9a8-45ec-44de-bba7-b68c580d478b',
 *              name: 'Скидка',
 *              alias: 'DISCOUNT',
 *            },
 *          },
 *          {
 *            id: 'aaff4b37-f6f7-45b7-aa8f-26381cb64a7c',
 *            description: 'Важное описание шаблона',
 *            timeToLive: null,
 *            limit: 20,
 *            value: 50,
 *            discountType: {
 *              id: 'c764b9a8-45ec-44de-bba7-b68c580d478b',
 *              name: 'Скидка',
 *              alias: 'DISCOUNT',
 *            },
 *          },
 *        ],
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-template-list',
  validator.query(DiscountController.validators.getTemplateList),
  asyncErrorHandler(DiscountController.getTemplateList),
);

/**
 * @api {post} /api/discount/create-template создать шаблон
 * @apiVersion 1.0.0
 * @apiName CreateTemplate
 * @apiGroup Discount
 * @apiParam (Body) {String} description Описание
 * @apiParam (Body) {Number} [timeToLive] Кол-во дней действия скидки. Если значение отсутствует, то считается безлимитным по времени
 * @apiParam (Body) {Number} [limit]  Кол-во раз использования для одного клиента. Если значение отсутствует, то считается безлимитным по кол-во раз использования
 * @apiParam (Body) {UUID} discountTypeId Id типа использования шаблона (скидка или фиксированная цена)
 * @apiParam (Body) {Number} [value] Значение скидки, если тип скидка
 * @apiParam (Body) {Object[]} [serviceRelations] массив с объектами услуги для которых будет применена фиксированная цена. Данная опция доступна в случае выбора шаблона с фиксированной ценой
 * @apiParam (Body) {UUID} serviceRelation.id Id услуги у которой будет другая, фиксированная цена
 * @apiParam (Body) {Number} serviceRelation.value Значение новой цены
 * @apiParam (Body) {Object[]} [tariffRelations] массив с объектами тарифов для которых будет применена фиксированная цена. Данная опция доступна в случае выбора шаблона с фиксированной ценой
 * @apiParam (Body) {UUID} tariffRelations.id Id тариф у которой будет другая, фиксированная цена
 * @apiParam (Body) {Number} tariffRelations.value Значение новой цены
 *
 * @apiSuccess (Success 200) {UUID} data Id созданного шаблона
 *
 * @apiParamExample {json} Пример запроса:
 *    {
 *      description: 'Важное описание шаблона',
 *      timeToLive: 17,
 *      limit: 1,
 *      discountTypeId: 'c764b9a8-45ec-44de-bba7-b68c580d478b',
 *      value: '50',
 *    }
 *
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: 'c7812675-35ab-4b79-8cb9-e1c1c6b2d7c3',
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.post(
  '/create-template',
  validator.body(DiscountController.validators.createTemplate),
  asyncErrorHandler(DiscountController.createTemplate),
);

/**
 * @api {get} /api/discount/get-discount-type-list получить список типов шаблонов
 * @apiVersion 1.0.0
 * @apiName GetDiscountTypeList
 * @apiGroup Discount
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: 'c764b9a8-45ec-44de-bba7-b68c580d478b',
 *          name: 'Скидка',
 *          alias: 'DISCOUNT',
 *        },
 *        {
 *          id: '9d879343-6d34-45e7-9467-1dcd13c23dd0',
 *          name: 'Фиксированная цена',
 *          alias: 'FIXED_PRICE',
 *        },
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-discount-type-list',
  asyncErrorHandler(DiscountController.getDiscountTypeList),
);

/**
 * @api {post} /api/discount/create-coupon создать шаблон
 * @apiVersion 1.0.0
 * @apiName CreateCoupon
 * @apiGroup Discount
 * @apiParam (Body) {UUID} discountTemplateId Id шаблона
 * @apiParam (Body) {String} code Значение скидки
 *
 * @apiSuccess (Success 200) {UUID} data Id созданного купона
 *
 * @apiParamExample {json} Пример запроса:
 *    {
 *      discountTemplateId: 'c764b9a8-45ec-44de-bba7-b68c580d478b',
 *      code: 'СКИДКА',
 *    }
 *
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: 'c7812675-35ab-4b79-8cb9-e1c1c6b2d7c3',
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.post(
  '/create-coupon',
  validator.body(DiscountController.validators.createCoupon),
  asyncErrorHandler(DiscountController.createCoupon),
);


/**
 * @api {get} /api/discount/get-coupon-list список купонов
 * @apiVersion 1.0.0
 * @apiName GetCouponList
 * @apiGroup Discount
 * @apiParam (Query) {Number} [limit=10] Кол-во записей
 * @apiParam (Query) {Number} [offset=0] Сдвиг
 * @apiParam (Query) {Array=["date","DESC"]} [sort=["date","DESC"]] Первым элементом идет название сортировки, вторым направление сортировки
 *
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: {
 *        count: 2,
 *        rows: [
 *          {
 *            id: '3180b935-d3c8-447f-a28f-03103686a7ae',
 *            value: 5,
 *            code: 'OPORA',
 *            limit: 1,
 *            expirationAt: null,
 *            createdAt: '2021-04-01T18:57:24.082Z',
 *            discountType: {
 *              id: 'c764b9a8-45ec-44de-bba7-b68c580d478b',
 *              name: 'Скидка',
 *              alias: 'DISCOUNT',
 *            },
 *          },
 *          {
 *            id: '65412772-4f03-4a89-b0d0-04d20598af16',
 *            value: 5,
 *            code: 'СКИДКА',
 *            limit: 1,
 *            expirationAt: '2022-04-09T16:40:04.000Z',
 *            createdAt: '2021-04-09T12:46:08.992Z',
 *            discountType: {
 *              id: 'c764b9a8-45ec-44de-bba7-b68c580d478b',
 *              name: 'Скидка',
 *              alias: 'DISCOUNT',
 *            },
 *          },
 *        ],
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-coupon-list',
  validator.query(DiscountController.validators.getCouponList),
  asyncErrorHandler(DiscountController.getCouponList),
);


/**
 * @api {get} /api/discount/get-coupon данные купона
 * @apiVersion 1.0.0
 * @apiName GetCoupon
 * @apiGroup Discount
 * @apiParam (Query) {UUID} Id купона
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: {
 *        value: 0, //это значение для общей скидки в процентах
 *        code: 'FIXPRICE',
 *        limit: 1,
 *        expirationAt: '2021-04-15T00:00:00.000Z',
 *        createdAt: '2021-04-14T13:43:25.796Z',
 *        discountType: {
 *          id: '9d879343-6d34-45e7-9467-1dcd13c23dd0',
 *          name: 'Фиксированная цена',
 *          alias: 'FIXED_PRICE',
 *        },
 *        discounts: {
 *          services: [
 *            {
 *              id: '0ed52b96-7e96-4573-bf98-02262d3a6a46',
 *              name: 'Увеличение места на жестком диске',
 *              withoutDiscountCost: 1190,
 *              cost: 272,
 *            },
 *          ],
 *          tariffs: [],
 *        },
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-coupon',
  validator.query(DiscountController.validators.getCoupon),
  asyncErrorHandler(DiscountController.getCoupon),
);

/**
 * @api {post} /api/discount/delete-coupon удалить купона
 * @apiVersion 1.0.0
 * @apiName DeleteCoupon
 * @apiGroup Discount
 * @apiParam (Body) {UUID} Id купона
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: null,
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.post(
  '/delete-coupon',
  validator.body(DiscountController.validators.deleteCoupon),
  asyncErrorHandler(DiscountController.deleteCoupon),
);

/**
 * @api {post} /api/discount/create-discount-service создать скидку в зависимости от добавленной услуги
 * @apiVersion 1.0.0
 * @apiName CreateDiscountService
 * @apiGroup Discount
 * @apiParam (Body) {UUID} discountTemplateId Id шаблона
 * @apiParam (Body) {UUID[]} services Массив Id услуг при добавлении которых будет применяться скидка
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: 'f40262d6-8567-4824-8cdf-02e3525f97f4',
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.post(
  '/create-discount-service',
  validator.body(DiscountController.validators.createDiscountService),
  asyncErrorHandler(DiscountController.createDiscountService),
);

/**
 * @api {get} /api/discount/get-discount-service-list список скидок услуг
 * @apiVersion 1.0.0
 * @apiName GetDiscountServiceList
 * @apiGroup Discount
 * @apiParam (Query) {Number} [limit=10] Кол-во записей
 * @apiParam (Query) {Number} [offset=0] Сдвиг
 * @apiParam (Query) {Array=["date","DESC"]} [sort=["date","DESC"]] Первым элементом идет название сортировки, вторым направление сортировки
 *
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: {
 *        count: 2,
 *        rows: [
 *          {
 *            id: '2fe86cdf-f4ef-4f74-ba63-0a5ed500f091',
 *            value: 47,
 *            limit: null,
 *            expirationAt: null,
 *            createdAt: '2021-04-13T22:06:19.538Z',
 *            discountType: {
 *              id: 'c764b9a8-45ec-44de-bba7-b68c580d478b',
 *              name: 'Скидка',
 *              alias: 'DISCOUNT',
 *            },
 *            services: [
 *              {
 *                id: '0ed52b96-7e96-4573-bf98-02262d3a6a46',
 *                name: 'Увеличение места на жестком диске',
 *              },
 *            ],
 *          },
 *          {
 *            id: 'f40262d6-8567-4824-8cdf-02e3525f97f4',
 *            value: 0,
 *            limit: null,
 *            expirationAt: null,
 *            createdAt: '2021-04-13T22:27:34.060Z',
 *            discountType: {
 *              id: '9d879343-6d34-45e7-9467-1dcd13c23dd0',
 *              name: 'Фиксированная цена',
 *              alias: 'FIXED_PRICE',
 *            },
 *            services: [
 *              {
 *                id: '6571bc04-2ddb-49df-a4b9-c8e2e206fbf9',
 *                name: 'Техосмотр ноутбука или компьютера',
 *              },
 *            ],
 *          },
 *        ],
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-discount-service-list',
  validator.query(DiscountController.validators.getDiscountServiceList),
  asyncErrorHandler(DiscountController.getDiscountServiceList),
);

/**
 * @api {get} /api/discount/get-discount-service данные о скидки услуги
 * @apiVersion 1.0.0
 * @apiName GetDiscountService
 * @apiGroup Discount
 * @apiParam (Query) {UUID} Id услуги (не самой услуги, а discount-service)
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: null,
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-discount-service',
  validator.query(DiscountController.validators.getDiscountService),
  asyncErrorHandler(DiscountController.getDiscountService),
);

/**
 * @api {post} /api/discount/delete-discount-service удалить скидку услуги
 * @apiVersion 1.0.0
 * @apiName DeleteDiscountService
 * @apiGroup Discount
 * @apiParam (Body) {UUID} Id услуги (не самой услуги, а discount-service)
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: null,
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.post(
  '/delete-discount-service',
  validator.body(DiscountController.validators.deleteDiscountService),
  asyncErrorHandler(DiscountController.deleteDiscountService),
);

/**
 * @api {post} /api/discount/create-discount-by-card добавить скидку по карте
 * @apiVersion 1.0.0
 * @apiName CreateDiscountByCard
 * @apiGroup Discount
 * @apiParam (Body) {Number} value скидка
 * @apiParam (Body) {String} [expirationAt] Дата окончания действия скидки в формате YYYY-MM-DDTHH:mm:ssZ (2021-02-24T17:00:00+03:00)
 * @apiDescription при добавлении новой скидки прежняя закрывается текущем временем
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: '2fe86cdf-f4ef-4f74-ba63-0a5ed500f091',
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.post(
  '/create-discount-by-card',
  validator.body(DiscountController.validators.createDiscountByCard),
  asyncErrorHandler(DiscountController.createDiscountByCard),
);

/**
 * @api {get} /api/discount/get-current-discount-by-card получить текущую скидку по карте
 * @apiVersion 1.0.0
 * @apiName GetCurrentDiscountByCard
 * @apiGroup Discount
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: {
 *        value: 20,
 *        expirationAt: '2022-12-24T17:00:00.000Z' | null,
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-current-discount-by-card',
  asyncErrorHandler(DiscountController.getCurrentDiscountByCard),
);

/**
 * @api {post} /api/discount/delete-discount-by-card удалить скидку по карте
 * @apiVersion 1.0.0
 * @apiName DeleteDiscountByCard
 * @apiGroup Discount
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: null,
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.post(
  '/delete-discount-by-card',
  asyncErrorHandler(DiscountController.deleteDiscountByCard),
);

export default router;
