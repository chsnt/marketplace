import { Router } from 'express';

import validator from '../../../common/libs/requestValidator';
import CityController from '../../../common/controllers/CityController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';

const router = new Router();

/**
 * @api {get} /api/city/get-list Получить список городов
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup City
 * @apiParam (Query) {String} [region] Поиск по регион
 * @apiParam (Query) {String} [city] Поиск по городу
 * @apiParam (Query) {Boolean} [onlyTestingParticipants] Признак участия города в пилоте
 * @apiDescription Получения списка городов (если onlyTestingParticipants установлен в true - вернет список городов в пилоте)
 * @apiParamExample Пример запроса:
 * http://address/api/city/get-list
 * @apiSuccessExample {json} Пример ответа:
 *  HTTP/1.1 200 OK
 *  {
 *    data: [
 *      {
 *        id: '4680dce7-ac13-410c-8b7d-a467845fe7ed',
 *        name: 'Республика Удмуртия',
 *        cities: [
 *          {
 *            id: '19265160-8358-4e45-8b5c-0688c7a1711d',
 *            name: 'Ижевск',
 *            isTestingParticipant: false,
 *            utcOffset: 3,
 *          },
 *          {
 *            id: '6cc78970-6242-408d-b349-b414fe507af6',
 *            name: 'Глазов',
 *            isTestingParticipant: false,
 *            utcOffset: 3,
 *          },
 *        ],
 *      },
 *      {
 *        id: '276729a5-2e63-4586-a367-c3bb9a655b3e',
 *        name: 'Нижегородская область',
 *        cities: [
 *          {
 *            id: '4b665516-7cf3-42de-b2e6-11ae4167d8b1',
 *            name: 'Нижний Новгород',
 *            isTestingParticipant: true,
 *            utcOffset: 3,
 *          },
 *          {
 *            id: 'eca95667-aa0a-4468-8f0f-ba12efc4899c',
 *            name: 'Арзамас',
 *            isTestingParticipant: false,
 *            utcOffset: 3,
 *          },
 *        ],
 *      },
 *    ],
 *    success: true,
 *    error: 'error message' | null,
 *    errorCode: 'ErrorCode' | null,
 *  }
 *
 */
router.get(
  '/get-list',
  validator.query(CityController.validators.getList),
  asyncErrorHandler(CityController.getList),
);

export default router;
