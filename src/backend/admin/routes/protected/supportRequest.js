import { Router } from 'express';

import SupportRequestController from '../../controllers/SupportRequestController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';
import validator from '../../../common/libs/requestValidator';
import FileStorage from '../../../common/libs/multerFileStorage';

const DOCUMENTS_PATH = 'support';

const fileStorageInstance = new FileStorage({ storagePath: DOCUMENTS_PATH, isSecure: false });

const router = new Router();

/**
 * @api {get} /api/support-request/get-list Список обращений
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup SupportRequest
 * @apiParam (Query) {Number} [number] Номер обращения
 * @apiParam (Query) {UUID} [supportRequestStatusId] Идентификатор статуса обращения
 * @apiParam (Query) {UUID} [supportRequestTopicId] Идентификатор темы обращения
 * @apiParam (Query) {UUID} [clientId] Идентификатор клиента
 * @apiParam (Query) {String{10..12}} [inn] ИНН клиента
 * @apiParam (Query) {UUID} [responsibleId] Идентификатор ответственного
 * @apiParam (Query) {String} [dateFrom] Старт интервала даты для фильтрации
 * @apiParam (Query) {String} [dateTo] Конец интервала даты для фильтрации
 * @apiParam (Query) {number} [limit=10] Кол-во записей
 * @apiParam (Query) {number} [offset=0] Сдвиг
 * @apiParam (Query) {String} [sortByDate=desc] Сортировка по дате. `asc` или `desc`
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: {
 *        count: 983327,
 *        rows: [
 *        {
 *          id: 'fc74f22b-f0f2-4bf3-bda3-2ebd78628050',
 *          number: 1,
 *          text: 'Текст обращения',
 *          client: 'ООО Рога Гопника',
 *          topic: 'Тема обращения',
 *          status: 'Новый',
 *          representative: 'ФИО представителя' | null,
 *          responsible: 'ФИО ответственного' | null
 *        }
 *      ]
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-list',
  validator.query(SupportRequestController.validators.getList),
  asyncErrorHandler(SupportRequestController.getList),
);

/**
 * @api {get} /api/support-request/:id/get-info Объект обращения
 * @apiVersion 1.0.0
 * @apiName GetInfo
 * @apiGroup SupportRequest
 * @apiParam (Params) {UUID} id Идентификатор обращения
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: {
 *        id: '20778dac-6556-4503-bd72-59e6a96dd726',
 *        text: 'Шо то ничего не работает',
 *        comment: 'Комментарий',
 *        firstName: 'Лопушок',
 *        patronymicName: null,
 *        number: 123,
 *        lastName: null,
 *        phone: '79101111111',
 *        email: 'test@mail.ru',
 *        supportRequestStatus: {
 *          id: '708ae989-0f82-4371-9e3c-33463eb960e4',
 *          name: 'Новый',
 *          alias: 'NEW'
 *        },
 *        client: {
 *          id: 'a1ce22e8-afc6-4e62-a102-1f984fca5aed',
 *          name: 'ООО Рога Гопника123'
 *        },
 *        topic: {
 *          id: '9bbed448-3b67-409b-88f0-95c307b33388',
 *          name: 'Какая-то проблема',
 *          alias: 'SOME_PROBLEM'
 *        },
 *        "responsible": {
 *          id: 'e3e45d9e-65fb-4462-9267-9d7a21473d49',
 *          fullName: 'Очень ответственный сотрудник'
 *        },
 *        attachments: [
 *          {
 *            id: '4e83dfae-b1f8-499b-af6f-33c2898a33df',
 *            name: 'filename.pdf'
 *          }
 *        ]
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/:id/get-info',
  validator.params(SupportRequestController.validators.getInfo),
  asyncErrorHandler(SupportRequestController.getInfo),
);

/**
 * @api {get} /api/support-request/get-topics Список тем обращений
 * @apiVersion 1.0.0
 * @apiName GetTopics
 * @apiGroup SupportRequest
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: 'fc74f22b-f0f2-4bf3-bda3-2ebd78628050',
 *          name: 'Просьба помочь',
 *          alias: 'HELP',
 *        }
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-topics',
  asyncErrorHandler(SupportRequestController.getTopics),
);

/**
 * @api {get} /api/support-request/get-statuses Список статусов обращений
 * @apiVersion 1.0.0
 * @apiName GetStatuses
 * @apiGroup SupportRequest
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: 'fc74f22b-f0f2-4bf3-bda3-2ebd78628050',
 *          name: 'Новый',
 *          alias: 'NEW',
 *        }
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-statuses',
  asyncErrorHandler(SupportRequestController.getStatuses),
);

/**
 * @api {post} /api/support-request/create Создать обращение
 * @apiVersion 1.0.0
 * @apiName Create
 * @apiGroup Support Request
 * @apiParam (Body) {UUID} [clientId] Клиент
 * @apiParam {String} topic UUID темы обращения
 * @apiParam {String{..1024}} description Описание проблемы
 * @apiParam {String} firstName Имя обратившегося
 * @apiParam {String} [patronymicName] Отчество обратившегося
 * @apiParam {String} [lastName] Фамилия обратившегося
 * @apiParam {String} phone Контактный телефон обратившегося
 * @apiParam {String} email Email обратившегося
 * @apiParam {Array} [documents] Прикреплённые файлы
 * @apiParam (Body) {String{..1024}} [comment] Комментарий к обращению
 * @apiDescription Принимает форму с заголовком Content-Type: multipart/form-data
 * @apiParamExample Request-Example:
 *    http://host/api/support-request/create
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "data": null,
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 *    }
 */
router.post(
  '/create',
  fileStorageInstance.upload.array('documents', 10),
  validator.body(SupportRequestController.validators.createRequest),
  asyncErrorHandler(SupportRequestController.createRequest),
);

/**
 * @api {post} /api/support-request/update Обновить обращение
 * @apiVersion 1.0.0
 * @apiName Update
 * @apiGroup Support Request
 * @apiParam (Body) {UUID} id Идентификатор обращения
 * @apiParam (Body) {UUID} [clientId] Клиент
 * @apiParam (Body) {UUID} [supportRequestStatusId] Идентификатор статуса обращения
 * @apiParam (Body) {String{..1024}} [solution] Описание решения
 * @apiParam (Body) {String{..1024}} [comment] Комментарий к обращению
 * @apiParam (Body) {UUID} [responsibleId] Ответственный
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "data": null,
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 *    }
 */
router.post(
  '/update',
  validator.body(SupportRequestController.validators.updateRequest),
  asyncErrorHandler(SupportRequestController.updateRequest),
);

/**
 * @api {get} /api/support-request/document/:documentId/get-document Скачать документ
 * @apiVersion 1.0.0
 * @apiName GetDocument
 * @apiGroup SupportRequest
 * @apiParam (Params) {UUID} documentId ID документа
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      "data": null,
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 *    }
 */
router.get(
  '/document/:documentId/get-document',
  validator.params(SupportRequestController.validators.getDocument),
  asyncErrorHandler(SupportRequestController.getDocument),
);

export default router;
