import { Router } from 'express';
import validator from '../../../common/libs/requestValidator';

import CostController from '../../controllers/CostController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';

const router = new Router();

/**
 * @api {post} /api/cost/add-cost Добавить цену услуге
 * @apiVersion 1.0.0
 * @apiName AddCost
 * @apiGroup Cost
 * @apiParam (Body) {UUID} id Идентификатор тарифа или услуги
 * @apiParam (Body) {Number} cost Цена
 * @apiParam (Body) {String} startAt Дата начала действия цены в формате YYYY-MM-DDTHH:mm:ssZ (2021-02-24T17:00:00+03:00)
 * @apiParam (Body) {String} [expirationAt] Дата окончания действия цены в формате YYYY-MM-DDTHH:mm:ssZ (2021-02-24T17:00:00+03:00)
 * @apiSuccessExample {json} Пример ответа:
 *  HTTP/1.1 200 OK
 * {
 *      "data": null,
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 * }
 */
router.post(
  '/add-cost',
  validator.body(CostController.validators.addCost),
  asyncErrorHandler(CostController.addCost),
);

export default router;
