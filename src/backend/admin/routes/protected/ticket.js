import { Router } from 'express';
import validator from '../../../common/libs/requestValidator';

import TicketController from '../../controllers/TicketController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';
import FileStorage from '../../../common/libs/multerFileStorage';

const DOCUMENTS_PATH = 'ticket';

const fileStorageInstance = new FileStorage({ storagePath: DOCUMENTS_PATH, isSecure: false });

const router = new Router();

/**
 * @api {get} /api/ticket/get-list Список ЗНО
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup Ticket
 * @apiParam (Query) {String} [serial] Номер ЗНО
 * @apiParam (Query) {UUID} [statusId] ID статуса ЗНО
 * @apiParam (Query) {UUID} [clientId] Идентификатор клиента
 * @apiParam (Query) {UUID} [inn] ИНН организации
 * @apiParam (Query) {UUID} services ID услуг
 * @apiParam (Query) {UUID} [responsibleId] Ответственный. Может принимать значение null
 * @apiParam (Query) {String} [clientName] Наименование клиента.
 * @apiParam (Query) {String} [serviceDeskId] ID Service desk.
 * @apiParam (Query) {String} [dateFrom] С какой даты в формате 'YYYY-MM-DD'
 * @apiParam (Query) {String} [dateTo] По какую дату в формате 'YYYY-MM-DD'
 * @apiParam (Query) {String} [nextCallDateFrom] Дата следующего звонка 'От' (YYYY-MM-DD HH:mm:ss)
 * @apiParam (Query) {String} [nextCallDateTo] Дата следующего звонка 'До' (YYYY-MM-DD HH:mm:ss)
 * @apiParam (Query) {String} [deviceTypeId] Тип оборудования
 * @apiParam (Query) {String} [sortByDate=desc] Сортировка по дате. `asc` или `desc`
 * @apiParam (Query) {Number} [limit=10] Кол-во записей
 * @apiParam (Query) {Number} [offset=0] Сдвиг
 * @apiParam (Query) {String=all, individual, organization} [client='all'] Фильтрация по клиенту <br/>
 *                    <b>all</b> - Все<br/>
 *                    <b>individual</b> - Физические лица<br/>
 *                    <b>organization</b> - Юридический лица<br/>
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: {
 *        count: 44,
 *        rows: [
 *        {
 *          id: '80e5558d-3ffd-4cfc-b851-4e067c11edde',
 *          serviceDeskId: null,
 *          createdAt: '2001-03-16T20:56:27.861Z',
 *          nextCallDate: '2025-03-22T14:40:54.000Z' | null,
 *          serial: 'АРМ338',
 *          laborCosts: 17 | null, //трудозатраты
 *          client: {
 *            id: '4482ce9c-5a64-4605-93a9-196be1f7d2d5',
 *            name: 'ИП Пупкович',
 *          },
 *          inn: '7736663045',
 *          services: [
 *            {
 *              id: 'd7ad6c6a-6cb7-4bdc-b4fe-9b5a5ba887be',
 *              name: 'Договор с ОФД на срок действия договора',
 *              deviceTypeId: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *            },
 *          ],
 *          status: 'Новый',
 *          responsible: {
 *            id: 'd758d71c-77d8-4941-b262-e612ea934e17',
 *            fullName: 'Пупкин-Основной Вася-Основной Сергеевич',
 *          },
 *          serviceAddress: {
 *            id: '7ebdec1f-edd6-4c9c-943d-1c70d8db5ffa',
 *            street: 'улица Ленина',
 *            house: '1',
 *            apartment: '123',
 *            postalCode: '555888',
 *            fiasId: null,
 *            city: {
 *              id: 'fbfc5bc7-6a43-4b07-b2b7-17960dc81bc9',
 *              name: 'Саров',
 *              utcOffset: 0,
 *            },
 *            region: {
 *              id: '4680dce7-ac13-410c-8b7d-a467845fe7ed',
 *              name: 'Республика Удмуртия',
 *            },
 *          },
 *          device: {
 *            id: 'e60bc9ea-577c-401d-85c1-7ed964d5fe51',
 *            type: 'Поддержка АРМ',
 *            alias: 'ARM',
 *            model: 'ПК',
 *            parameters: [
 *              {
 *                value: 'Asus',
 *                name: 'Производитель',
 *                alias: 'MANUFACTURER',
 *              },
 *              {
 *                value: 'Zver',
 *                name: 'Модель',
 *                alias: 'MODEL',
 *              },
 *              {
 *                value: 'Windows10',
 *                name: 'Операционная система',
 *                alias: 'OPERATION_SYSTEM',
 *              },
 *            ],
 *          },
 *        },
 *      ]
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-list',
  validator.query(TicketController.validators.getList),
  asyncErrorHandler(TicketController.getList),
);

/**
 * @api {get} /api/ticket/:id/get-info Информация о ЗНО
 * @apiVersion 1.0.0
 * @apiName GetInfo
 * @apiGroup Ticket
 * @apiParam (Params) {number} id Идентификатор ЗНО
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: {
 *        id: '0c488b0b-6559-4ab7-a59c-9e1134ff67b6',
 *        serial: '336',
 *        number: 336,
 *        solution: 'Описание решения',
 *        description: 'Описание к заявке',
 *        comment: 'Комментарий',
 *        serviceDeskId: null,
 *        createdAt: '2021-03-15T14:49:26.251Z',
 *        nextCallDate: '2020-11-02T08:09:46.854Z' | null,
 *        laborCosts: 17 | null, //трудозатраты
 *        ticketStatus: {
 *          id: 'f12d1350-8375-4f5e-8833-a1c0037cbd88',
 *          name: 'Новый',
 *          alias: 'NEW',
 *        },
 *        ticketCloseDocuments: [],
 *        responsible: {
 *          id: 'd758d71c-77d8-4941-b262-e612ea934e17',
 *          fullName: 'Пупкин-Основной Вася-Основной Сергеевич',
 *        },
 *        client: {
 *          id: '474368e4-0010-49f7-a3a0-7ce7442590ce',
 *          name: 'Физиков Физик Физикович',
 *          organizations: {
 *               inn: '123456789012' | null,
 *           },
 *        },
 *        contactPerson: {
 *          id: 'd500c3db-ad4e-4c8a-a57c-eac3331e13e0',
 *          firstName: 'Физик',
 *          patronymicName: 'Физикович',
 *          lastName: 'Физиков',
 *          email: 'fizz@yandex.ru',
 *          phone: '79993330000',
 *        },
 *        services: [
 *          {
 *            id: 'c8071d8d-d614-4d96-993b-d3725439b9ed',
 *            name: 'Пакет времени (3 часа)',
 *          },
 *          {
 *            id: '14c98950-a7dd-4d0c-b0b7-a741397ab924',
 *            name: 'Консультация технической поддержки',
 *          },
 *        ],
 *        serviceAddress: {
 *          id: '7ebdec1f-edd6-4c9c-943d-1c70d8db5ffa',
 *          street: 'улица Ленина',
 *          house: '1',
 *          apartment: '123',
 *          postalCode: '555888',
 *          fiasId: null,
 *          city: {
 *            id: 'fbfc5bc7-6a43-4b07-b2b7-17960dc81bc9',
 *            name: 'Саров',
 *            utcOffset: 0,
 *          },
 *          region: {
 *            id: '4680dce7-ac13-410c-8b7d-a467845fe7ed',
 *            name: 'Республика Удмуртия',
 *          },
 *        } | null,
 *        device: {
 *          id: 'e60bc9ea-577c-401d-85c1-7ed964d5fe51',
 *          alias: 'ARM',
 *          model: 'ПК',
 *          parameters: [
 *            {
 *              value: 'Asus',
 *              id: 'bd9795c5-1295-4b92-a29d-e2e1170bc63a',
 *              name: 'Производитель',
 *              alias: 'MANUFACTURER',
 *            },
 *            {
 *              value: 'Zver',
 *              id: 'f3a5ad3f-a3b4-47fd-a82d-8eff9da69f67',
 *              name: 'Модель',
 *              alias: 'MODEL',
 *            },
 *            {
 *              value: 'Windows10',
 *              id: 'dc9577f9-c810-4ddc-9075-72514cb59ccb',
 *              name: 'Операционная система',
 *              alias: 'OPERATION_SYSTEM',
 *            },
 *          ],
 *        } | null,
 *        documents: [],
 *      },
 *      success: true,
 *      error: null,
 *      errorCode: null,
 *    }
 */
router.get(
  '/:id/get-info',
  validator.params(TicketController.validators.getInfo),
  asyncErrorHandler(TicketController.getInfo),
);

/**
 * @api {post} /api/ticket/create Создать заявку
 * @apiVersion 1.0.0
 * @apiName CreateTicket
 * @apiGroup Ticket
 * @apiParam (FormData) {UUID} clientId ID клиента
 * @apiParam (FormData) {UUID} [statusId] ID статуса
 * @apiParam (FormData) {String{..255}} [description] Описание заявки
 * @apiParam (FormData) {String{..255}} [solution] Описание решения
 * @apiParam (FormData) {String{..255}} [comment] Комментарий
 * @apiParam (FormData) {String} [nextCallDate] Дата и время следующего звонка по ЗНО (YYYY-MM-DD HH:mm:ss)
 * @apiParam (FormData) {UUID} [supportRequestId] Идентификатор обращения
 * @apiParam (FormData) {UUID} [services] массив с ID услугами
 * @apiParam (FormData) {UUID} [serviceAddressId] UUID адреса
 * @apiParam (FormData) {Array} [documents] Прикреплённые файлы
 * @apiParam (FormData) {Number} [laborCosts] Трудозатраты
 * @apiParam (FormData) {String} [device] Новое оборудование с адресом обслуживания
 * @apiParam (FormData) {String} device.model UUID модели устройства
 * @apiParam (FormData) {Object[]} device.parameters Коллекция параметров
 * @apiParam (FormData) {String} device.parameters.id UUID параметра
 * @apiParam (FormData) {String} device.parameters.value Значение параметра
 * @apiParam (FormData) {Object} device.address Адрес обслуживания устройства
 * @apiParam (FormData) {UUID} device.address.region Субъект РФ
 * @apiParam (FormData) {UUID} device.address.cityId Город из списка обслуживаемых городов
 * @apiParam (FormData) {String} device.address.street Улица
 * @apiParam (FormData) {String} device.address.house Дом
 * @apiParam (FormData) {String} device.address.apartment Квартира/офис
 * @apiParam (FormData) {String} [device.address.postalCode] Почтовый индекс
 * @apiDescription В запросе должен быть, или id единоразовой услуги, или id услуги тарифа</br>
 *                  Принимает форму с заголовком Content-Type: multipart/form-data</br>
 *                  Если у услуг выбран адрес "При заказе", то нужно передавать serviceAddressId</br>
 *                  Если у услуг выбран адрес "При создании ЗНО", то нужно передавать serviceAddressId или device</br>
 *                  Если у услуг выбран адрес  "Не запрашивается", то ни serviceAddressId, ни device передавать не обязательно</br>
 * @apiParamExample {json} Пример запроса с единоразовой услуги
 *    {
 *      clientId: 'f12d1350-8375-4f5e-8833-a1c0037cbd88',
 *      description: 'что хочу не знаю',
 *      services: ['f12d1350-8375-4f5e-8833-a1c0037cbd88'],
 *      serviceAddressId: 'eab890c2-6bb6-4aab-87d3-c0d65783d711',
 *    }
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      data: 'b76ea2c1-8531-48e2-93fb-b8b77e94145a',
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.post(
  '/create',
  fileStorageInstance.upload.array('documents', 10),
  validator.body(TicketController.validators.createTicket),
  asyncErrorHandler(TicketController.createTicket),
);

/**
 * @api {get} /api/ticket/get-statuses Список статусов ЗНО
 * @apiVersion 1.0.0
 * @apiName GetStatuses
 * @apiGroup Ticket
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: 'f12d1350-8375-4f5e-8833-a1c0037cbd88',
 *          name: 'Новая',
 *          alias: 'NEW',
 *        },
 *        {
 *          id: 'c02e88f5-6873-4d80-9bf7-2d10f3b86507',
 *          name: 'В работе',
 *          alias: 'IN_PROGRESS',
 *        },
 *        {
 *          id: '28a69c44-5c6b-4bd1-9b00-993d0434a5f5',
 *          name: 'Уточнение информации',
 *          alias: 'UPDATE_INFORMATION',
 *        },
 *        {
 *          id: 'b539214b-89e3-4cc9-9f73-0f7c080e1afe',
 *          name: 'Решен',
 *          alias: 'COMPLETED',
 *        },
 *        {
 *          id: 'a45f44b5-31c6-430b-a781-508032447940',
 *          name: 'Отменен',
 *          alias: 'CANCELLED',
 *        },
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-statuses',
  asyncErrorHandler(TicketController.getStatuses),
);

/**
 * @api {get} /api/ticket/update Обновить ЗНО
 * @apiVersion 1.0.0
 * @apiName UpdateTicket
 * @apiGroup Ticket
 * @apiParam (FormData) {UUID} ticketId ID статуса
 * @apiParam (FormData) {UUID} [statusId] ID статуса
 * @apiParam (FormData) {UUID} [responsibleId] ID сотрудника взявшего в работу ЗНО
 * @apiParam (FormData) {String} [serviceDeskId] ID в сервис деске
 * @apiParam (FormData) {String{..255}} [solution] Описание решения
 * @apiParam (FormData) {String{..255}} [comment] Комментарий к заявке
 * @apiParam (Body) {String} [nextCallDate] Дата и время следующего звонка по ЗНО (YYYY-MM-DD HH:mm:ss)
 * @apiParam (FormData) {UUID} [userId] Контактное лицо
 * @apiParam (FormData) {Array{..10}} [documents] Прикреплённые файлы
 * @apiParam (FormData) {Array{..10}} services Список услуг ЗНО, если ранее добавленные услуги отсутствуют, то они будут удалены
 * @apiParam (FormData) {Number} [laborCosts] Трудозатраты
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.post(
  '/update',
  fileStorageInstance.upload.array('documents', 10),
  validator.body(TicketController.validators.updateTicket),
  asyncErrorHandler(TicketController.updateTicket),
);

/**
 * @api {get} /api/ticket/document/:documentId/get-document Скачать документ
 * @apiVersion 1.0.0
 * @apiName GetDocument
 * @apiGroup Ticket
 * @apiParam (Params) {UUID} documentId ID документа
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      "data": null,
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 *    }
 */
router.get(
  '/document/:documentId/get-document',
  validator.params(TicketController.validators.getDocument),
  asyncErrorHandler(TicketController.getDocument),
);

export default router;
