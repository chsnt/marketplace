import { Router } from 'express';
import validator from '../../../common/libs/requestValidator';

import OrganizationControllerCommon from '../../../common/controllers/OrganizationController';
import OrganizationController from '../../controllers/OrganizationController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';

const router = new Router();

/**
 * @api {get} /api/organization/info Получить информацию об организации из внешней системы
 * @apiVersion 1.0.0
 * @apiName Info
 * @apiGroup Organization
 * @apiDescription В качестве query-параметра можно указать либо inn, либо ogrn, но не оба
 * @apiParam (Query) inn ИНН организации
 * @apiParam (Query) ogrn ОГРН организации
 * @apiParamExample Пример запроса:
 * http://address/api/organization/info?inn=1231231231
 * @apiSuccessExample {json} Пример ответа:
 *  HTTP/1.1 200 OK
 * {
 *   data: {
 *     inn: '5029190963',
 *     kpp: '502901001',
 *     fullname: 'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "КОНЬ-ОГОНЬ"',
 *     name: 'ООО "КОНЬ-ОГОНЬ"',
 *     email: null,
 *     address: {
 *       АдресРФ: [{
 *         Индекс: '141006', НаимРегион: 'МОСКОВСКАЯ', НаимГород: 'МЫТИЩИ', НаимУлица: 'ШАРАПОВСКИЙ', Дом: 'ДОМ 1Б',
 *       }],
 *     },
 *     ogrn: '1145029012457',
 *     ogrn_date: '2014-10-27',
 *     okved: {
 *       OKVED: [
 *         { Код: '52.29', Наименование: 'Деятельность вспомогательная прочая, связанная с перевозками', Основной: 1 },
 *         { Код: '45.31', Наименование: 'Торговля оптовая автомобильными деталями, узлами и принадлежностями', Основной: 0 },
 *         { Код: '45.32', Наименование: 'Торговля розничная автомобильными деталями, узлами и принадлежностями', Основной: 0 },
 *         { Код: '46.31', Наименование: 'Торговля оптовая фруктами и овощами', Основной: 0 },
 *         { Код: '46.31.11', Наименование: 'Торговля оптовая свежим картофелем', Основной: 0 },
 *         { Код: '46.32', Наименование: 'Торговля оптовая мясом и мясными продуктами', Основной: 0 },
 *         { Код: '46.32.3', Наименование: 'Торговля оптовая консервами из мяса и мяса птицы', Основной: 0 },
 *         { Код: '46.33', Наименование: 'Торговля оптовая молочными продуктами, яйцами и пищевыми маслами и жирами', Основной: 0 },
 *         { Код: '46.34', Наименование: 'Торговля оптовая напитками', Основной: 0 },
 *         { Код: '46.36', Наименование: 'Торговля оптовая сахаром, шоколадом и сахаристыми кондитерскими изделиями', Основной: 0 },
 *         { Код: '46.37', Наименование: 'Торговля оптовая кофе, чаем, какао и пряностями', Основной: 0 },
 *         { Код: '46.38', Наименование: 'Торговля оптовая прочими пищевыми продуктами, включая рыбу, ракообразных и моллюсков', Основной: 0 },
 *         { Код: '46.39', Наименование: 'Торговля оптовая неспециализированная пищевыми продуктами, напитками и табачными изделиями', Основной: 0 },
 *         { Код: '46.41', Наименование: 'Торговля оптовая текстильными изделиями', Основной: 0 },
 *         { Код: '46.41.2', Наименование: 'Торговля оптовая галантерейными изделиями', Основной: 0 },
 *         { Код: '46.42', Наименование: 'Торговля оптовая одеждой и обувью', Основной: 0 },
 *         { Код: '46.43', Наименование: 'Торговля оптовая бытовыми электротоварами', Основной: 0 },
 *         { Код: '46.44', Наименование: 'Торговля оптовая изделиями из керамики и стекла и чистящими средствами', Основной: 0 },
 *         { Код: '46.45', Наименование: 'Торговля оптовая парфюмерными и косметическими товарами', Основной: 0 },
 *         { Код: '46.49', Наименование: 'Торговля оптовая прочими бытовыми товарами', Основной: 0 },
 *         { Код: '46.90', Наименование: 'Торговля оптовая неспециализированная', Основной: 0 },
 *         { Код: '47.11', Наименование: 'Торговля розничная преимущественно пищевыми продуктами, включая напитки, и табачными изделиями в неспециализированных магазинах', Основной: 0 },
 *         { Код: '47.19', Наименование: 'Торговля розничная прочая в неспециализированных магазинах', Основной: 0 },
 *         { Код: '47.21', Наименование: 'Торговля розничная фруктами и овощами в специализированных магазинах', Основной: 0 },
 *         { Код: '47.22', Наименование: 'Торговля розничная мясом и мясными продуктами в специализированных магазинах', Основной: 0 },
 *         { Код: '47.23', Наименование: 'Торговля розничная рыбой, ракообразными и моллюсками в специализированных магазинах', Основной: 0 },
 *         { Код: '47.24', Наименование: 'Торговля розничная хлебом и хлебобулочными изделиями и кондитерскими изделиями в специализированных магазинах', Основной: 0 },
 *         { Код: '47.25', Наименование: 'Торговля розничная напитками в специализированных магазинах', Основной: 0 },
 *         { Код: '47.43', Наименование: 'Торговля розничная аудио- и видеотехникой в специализированных магазинах', Основной: 0 },
 *         { Код: '47.51', Наименование: 'Торговля розничная текстильными изделиями в специализированных магазинах', Основной: 0 },
 *         { Код: '47.52', Наименование: 'Торговля розничная скобяными изделиями, лакокрасочными материалами и стеклом в специализированных магазинах', Основной: 0 },
 *         { Код: '47.54', Наименование: 'Торговля розничная бытовыми электротоварами в специализированных магазинах', Основной: 0 },
 *         { Код: '47.59', Наименование: 'Торговля розничная мебелью, осветительными приборами и прочими бытовыми изделиями в специализированных магазинах', Основной: 0 },
 *         { Код: '47.61', Наименование: 'Торговля розничная книгами в специализированных магазинах', Основной: 0 },
 *         { Код: '47.7', Наименование: 'Торговля розничная прочими товарами в специализированных магазинах', Основной: 0 },
 *         { Код: '47.71', Наименование: 'Торговля розничная одеждой в специализированных магазинах', Основной: 0 },
 *         { Код: '47.72', Наименование: 'Торговля розничная обувью и изделиями из кожи в специализированных магазинах', Основной: 0 },
 *         { Код: '47.79', Наименование: 'Торговля розничная бывшими в употреблении товарами в магазинах', Основной: 0 },
 *         { Код: '47.9', Наименование: 'Торговля розничная вне магазинов, палаток, рынков', Основной: 0 },
 *         { Код: '47.99', Наименование: 'Торговля розничная прочая вне магазинов, палаток, рынков', Основной: 0 },
 *         { Код: '49.4', Наименование: 'Деятельность автомобильного грузового транспорта и услуги по перевозкам', Основной: 0 },
 *         { Код: '82.99', Наименование: 'Деятельность по предоставлению прочих вспомогательных услуг для бизнеса, не включенная в другие группировки', Основной: 0 },
 *         { Код: '93.29', Наименование: 'Деятельность зрелищно-развлекательная прочая', Основной: 0 },
 *         { Код: '93.29.9', Наименование: 'Деятельность зрелищно-развлекательная прочая, не включенная в другие группировки', Основной: 0 },
 *       ],
 *     },
 *     state_code: null,
 *     state_name: null,
 *     state_except_date: null,
 *     state_except_num: null,
 *     state_except_pub_date: null,
 *     state_except_mag_num: null,
 *     date_of_termination: null,
 *     code_of_termination: null,
 *     name_of_termination: null,
 *     ordinality: '382',
 *     validfrom: '2019-09-30',
 *     load_date: '2019-10-09 05:13:13.473734+03',
 *     filename: 'EGRUL_2019-09-30_398636.XML',
 *     session_id: '1910090000001468',
 *   },
 *   success: true,
 *   error: 'error message' | null,
 *   errorCode: 'ErrorCode' | null,
 * }
 */
router.get(
  '/info',
  validator.query(OrganizationControllerCommon.validators.getInfoFromCloud),
  asyncErrorHandler(OrganizationControllerCommon.getInfoFromCloud),
);

/**
 * @api {post} /api/organization/update Обновление информации
 * @apiVersion 1.0.0
 * @apiName UpdateUser
 * @apiGroup User
 * @apiParam (Body) {UUID} id UUID организации

 * @apiParam (Body) {String{2..}} [name] Название организации
 * @apiParam (Body) {String{10..12}} [inn] ИНН
 * @apiParam (Body) {String{9}} [kpp] КПП
 * @apiParam (Body) {String{1..255}} [externalId] Id интегратора
 *
 * @apiParam (Body) {Object} [digitalSignature] Срок действия ЭЦП
 * @apiParam (Body) {String} digitalSignature.from Начало срока действия ЭЦП в формате 'YYYY-MM-DD'
 * @apiParam (Body) {String} digitalSignature.to Окончание срока действия ЭЦП в формате 'YYYY-MM-DD'
 *
 * @apiParam (Body) {Object} [legalAddress] Юридический адрес
 * @apiParam (Body) {UUID} [legalAddress.region] Регион
 * @apiParam (Body) {String{2..}} [legalAddress.city] Город
 * @apiParam (Body) {UUID} [legalAddress.cityId] Город
 * @apiParam (Body) {String{2..}} [legalAddress.street] Улица
 * @apiParam (Body) {UUID} [legalAddress.fiasId] UUID фиас
 * @apiParam (Body) {String} [legalAddress.house] Номер дома
 * @apiParam (Body) {String} [legalAddress.apartment] Номер помещения
 * @apiParam (Body) {String} [legalAddress.postalCode] Индекс
 *
 * @apiParam (Body) {Object} [realAddress] Фактический адрес
 * @apiParam (Body) {UUID} [realAddress.region] Регион
 * @apiParam (Body) {String{2..}} [realAddress.city] Город
 * @apiParam (Body) {UUID} [realAddress.cityId] Город
 * @apiParam (Body) {String{2..}} [realAddress.street] Улица
 * @apiParam (Body) {String} [realAddress.house] Номер дома
 * @apiParam (Body) {String} [realAddress.apartment] Номер помещения
 * @apiParam (Body) {String} [realAddress.postalCode] Индекс
 *
 * @apiParam (Body) {Object} [bankDetail] Банковские реквизиты
 * @apiParam (Body) {String{2..100}} [bankDetail.name] Название банка
 * @apiParam (Body) {String{20}} [bankDetail.bankAccount] Расчетный счет
 * @apiParam (Body) {String{20}} [bankDetail.correspondentAccount] Кор. счет
 * @apiParam (Body) {String{9}} [bankDetail.rcbic] БИК
 * @apiParam (Body) {UUID} [bankDetail.taxType] Система налогообложения
 * @apiParam (Body) {UUID} [electronicDocumentCirculationId] Оператор электронного документа оборота
 * @apiParam (Body) {String{..1024}} [electronicDocumentCirculationDescription] Если оператора нет, нужно указать его в описании
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      data: {
 *        id: '1b09d415-5d13-4895-a091-cab8c97fbb37',
 *        name: 'ЗАО Лапландия',
 *        inn: '5551285234',
 *        kpp: '810664644',
 *        externalId: null,
 *        sbbidId: null,
 *        clientId: 'e27e006f-db69-483e-8a2b-5cb4a458785d',
 *        real_address_id: '33aa1b46-2ea9-4322-b20b-31aed5c998b4',
 *        legal_address_id: 'efa65c27-e30d-41fc-ad8c-bc6310b24adb',
 *        electronic_document_circulation_id: 'e6514477-fcb0-42a4-a411-1d956cca7bd0',
 *        electronicDocumentCirculationId: 'e6514477-fcb0-42a4-a411-1d956cca7bd0',
 *        updated_by: '29776f43-5c4c-48ba-a164-c9447588d0f5',
 *        legalAddress: {
 *          id: 'efa65c27-e30d-41fc-ad8c-bc6310b24adb',
 *          city: 'Иматра',
 *          street: 'Лахтинская пл.',
 *          house: '33',
 *          apartment: '98',
 *          postalCode: '143814',
 *          fiasId: '6cc78970-6242-408d-b349-b414fe507af6',
 *          cityId: null,
 *          updated_by: '29776f43-5c4c-48ba-a164-c9447588d0f5',
 *          updatedAt: '2021-03-26T10:43:11.006Z',
 *          region: {
 *            id: '4680dce7-ac13-410c-8b7d-a467845fe7ed',
 *            name: 'Республика Удмуртия',
 *            externalId: null,
 *            alias: null,
 *          },
 *        },
 *        realAddress: {
 *          id: '19265160-8358-4e45-8b5c-0688c7a1711d',
 *          city: 'Ижевск',
 *          street: 'улица Ленина',
 *          house: '33',
 *          apartment: '98',
 *          postalCode: '143814',
 *          fiasId: '6cc78970-6242-408d-b349-b414fe507af6',
 *          externalId: null,
 *          region: {
 *            id: '4680dce7-ac13-410c-8b7d-a467845fe7ed',
 *            name: 'Республика Удмуртия',
 *            externalId: null,
 *            alias: null,
 *          },
 *        },
 *        electronicDocumentCirculation: {
 *          id: 'e6514477-fcb0-42a4-a411-1d956cca7bd0',
 *          name: 'E-COM',
 *          alias: 'E_COM',
 *        },
 *        electronicDocumentCirculationDescription: {},
 *        taxType: {
 *          id: 'ad24d66f-eda3-4844-a1de-1c6d055bb4b8',
 *          name: 'Общая система налогообложения (ОСНО)',
 *          alias: 'OSNO',
 *          externalId: null,
 *        },
 *        digitalSignature: {
 *          from: null,
 *          to: null,
 *        },
 *        bank: {
 *          id: '4ee36fe6-512c-4fc0-b3dd-4f4997f0f577',
 *          name: 'Сбер',
 *          correspondentAccount: '09876543185987654321',
 *          rcbic: '147257369',
 *          externalId: null,
 *          bankAccount: '96342178985566547890',
 *        },
 *      },
 *      success: true,
 *      error: null,
 *      errorCode: null,
 *    }
 */
router.post(
  '/update',
  validator.body(OrganizationController.validators.updateOrganization),
  asyncErrorHandler(OrganizationController.updateOrganization),
);

export default router;
