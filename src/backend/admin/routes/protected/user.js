import { Router } from 'express';

import UserController from '../../controllers/UserController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';
import validator from '../../../common/libs/requestValidator';

const router = new Router();

/**
 * @api {get} /api/user/:id/reset-password Сброс пароля пользователю
 * @apiVersion 1.0.0
 * @apiName ResetPassword
 * @apiParam (Params) {String} id Идентификатор пользователя
 * @apiGroup User
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *   {
 *     data: null,
 *     success: true,
 *     error: null,
 *     errorCode: null,
 *   }
 *
 */
router.post(
  '/:id/reset-password',
  validator.params(UserController.validators.resetPassword),
  asyncErrorHandler(UserController.resetPassword),
);

/**
 * @api {get} /api/user/me/settings Получить пользовательские настройки
 * @apiVersion 1.0.0
 * @apiName GetUserSettings
 * @apiGroup User
 * @apiDescription Сервер возвращает информацию о пользователе.
 * @apiParamExample Request-Example:
 *    http://host/api/user/me/settings
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      data: {
 *          firstName: 'Вася',
 *          lastName: 'Пупкин',
 *          patronymicName: 'Сергеевич',
 *          email: 'xxyyzz@yandex.ru',
 *          phone: '79119998877',
 *          birthday: '12.02.1857',
 *        },
 *      success: true,
 *      error: null,
 *      errorCode: null,
 *    }
 */

router.get(
  '/me/settings',
  asyncErrorHandler(UserController.getInfo),
);

/**
 * @api {get} /api/user/get-user-groups Получить список ролей
 * @apiVersion 1.0.0
 * @apiName GetUserGroups
 * @apiGroup User
 * @apiParamExample Request-Example:
 *    http://host/api/user/get-user-groups
 *
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: '45f54fd5-28af-40ec-8c4c-a516cbbeb2dd',
 *          name: 'Админ',
 *          alias: 'ADMIN',
 *        },
 *        {
 *          id: '07fd8ab6-9b79-45b0-a727-ac8b68fe25b7',
 *          name: 'Диспетчер',
 *          alias: 'DISPATCHER',
 *        },
 *        {
 *          id: '463cae5d-b6e6-4349-8849-22a0dce46e88',
 *          name: 'Менеджер по продажам',
 *          alias: 'SALES_MANAGER',
 *        },
 *      ],
 *      success: true,
 *      error: null,
 *      errorCode: null,
 *    }
 */
router.get(
  '/get-user-groups',
  asyncErrorHandler(UserController.getUserGroups),
);

/**
 * @api {get} /api/user/get-users Получить список пользователей в зависимости от роли
 * @apiVersion 1.0.0
 * @apiName GetUsers
 * @apiGroup User
 * @apiParam (Query) {UUID} [userGroupId] ID роли
 * @apiParam (Query) {String} [type='any'] `any` - все пользователи, `user` - пользователи, `admin` - админы (сейчас 'user' работает как 'any')
 * @apiParam (Query) {number} [limit=10] Кол-во записей
 * @apiParam (Query) {number} [offset=0] Сдвиг
 * @apiParam (Query) {String} [sortByDate=desc] Сортировка по дате. `asc` или `desc`
 * @apiDescription В запросе должен быть параметр, или role, или type. Так реализована для потому, что у администраторов есть несколько ролей
 * @apiParamExample Request-Example role:
 *    'http://host/api/user/get-users?userGroupId=463cae5d-b6e6-4349-8849-22a0dce46e88'
 * @apiParamExample Request-Example type:
 *    'http://host/api/user/get-users?type=admin'
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      data: {
 *        count: 453,
 *        rows: [{
 *          id: '87d5c223-8a1a-48b5-b34e-b525d71c5e38',
 *          firstName: 'Вася-Менеджер',
 *          lastName: 'Менеджер',
 *          patronymicName: 'Менеджерович',
 *          email: 'manager@admin.com',
 *          phone: '79223335541',
 *          createdAt: '2021-03-24T07:25:19.848Z',
            deletedAt: '2021-03-25T20:04:31.000Z' | null
 *        }]
 *      },
 *      success: true,
 *      error: null,
 *      errorCode: null,
 *    }
 */
router.get(
  '/get-users',
  validator.query(UserController.validators.getUsers),
  asyncErrorHandler(UserController.getUsers),
);

/**
 * @api {post} /api/user/update Обновление информации
 * @apiVersion 1.0.0
 * @apiName UpdateUser
 * @apiGroup User
 * @apiParam (Body) {UUID} id UUID пользователя
 * @apiParam (Body) {String} [firstName] Имя
 * @apiParam (Body) {String} [lastName] Фамилия
 * @apiParam (Body) {String} [patronymicName] Отчество
 * @apiParam (Body) {String} [email] Почта
 * @apiParam (Body) {Number} [phone] Номер телефона
 * @apiParam (Body) {String} [birthday] День рождения в формате 'YYYY-MM-DD'
 * @apiParam (Body) {Boolean} [delete] Если нужно удалить пользователя `true`, если нужно восстановить `false`
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      data: '5369b19d-b8fb-4839-b72d-cd97003abf16',
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null
 *    }
 */
router.post(
  '/update',
  validator.body(UserController.validators.updateUser),
  asyncErrorHandler(UserController.updateUser),
);

export default router;
