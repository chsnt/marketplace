import { Router } from 'express';
import validator from '../../../common/libs/requestValidator';

import OrderController from '../../controllers/OrderController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';
import FileStorage from '../../../common/libs/multerFileStorage';

const router = new Router();

const DOCUMENTS_PATH = 'order';

const fileStorageInstance = new FileStorage({ storagePath: DOCUMENTS_PATH, isSecure: false });

const ORDER_OFFER_DOCUMENTS_PATH = 'order/offer-documents';

const offerDocumentFileStorageInstance = new FileStorage({ storagePath: ORDER_OFFER_DOCUMENTS_PATH, isSecure: false });

/**
 * @api {get} /api/order/get-list Список заказов
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup Order
 * @apiParam (Query) {Number} [number] Номер заказа
 * @apiParam (Query) {String} [dateFrom] Дата создания заказа 'От'
 * @apiParam (Query) {String} [dateTo] Дата создания заказа 'До'
 * @apiParam (Query) {UUID} [clientId] Идентификатор клиента
 * @apiParam (Query) {String} [paymentType] Тип оплаты ('CARD', 'INVOICE', null - не оплачен)
 * @apiParam (Query) {UUID} [orderStatusId] ID статуса заказа
 * @apiParam (Query) {String} [paymentDateFrom] Дата оплаты 'От'
 * @apiParam (Query) {String} [paymentDateTo] Дата оплаты 'До'
 * @apiParam (Query) {UUID} [responsibleId] Идентификатор ответственного
 * @apiParam (Query) {String=all, individual, organization} [client='all'] Фильтрация по клиенту
 * @apiParam (Query) {number} [limit=10] Кол-во записей
 * @apiParam (Query) {number} [offset=0] Сдвиг
 * @apiParam (Query) {Array=["createdDate","DESC"]} [sort=["createdDate","DESC"]] Первым элементом идет название сортировки, вторым направление сортировки<br/>
 * Значение передается в формате JSON<br/><br/>
 *                    <b>number</b> - сортировка по номеру заказа<br/>
 *                    <b>createdDate</b> - сортировка по дате создания заказа<br/>
 *                    <b>paymentDate</b> - сортировка по дате оплаты<br/>
 *                    <b>client</b> - сортировка по имени клиента, по умолчанию сортировка для физика. Для юрика в параметрах нужно передать client='organization'<br/>
 *                    <b>responsible</b> - сортировка по ответственному<br/>
 *                    <b>amount</b> - сортировка по сумме заказа<br/>
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: {
 *        count: 1
 *        rows: [{
 *          id: '4d7ef473-c334-479a-8d6f-d0ee64dd7af5',
 *          number: 1,
 *          createdAt: '2020-10-28T14:41:59.848Z',
 *          client: 'ООО Рога Гопника',
 *          cost: 100 | null,
 *          paymentType: 'CARD'
 *          paymentDate: '2020-11-10T00:00:00.000Z',
 *          isPaid: false | true, //оплачен или нет
 *          typePayment: 'invoice' | 'card' //выбранный способ оплаты
 *        }]
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-list',
  validator.query(OrderController.validators.getList),
  asyncErrorHandler(OrderController.getList),
);

/**
 * @api {get} /api/order/:id/get-info Объект заказа
 * @apiVersion 1.0.0
 * @apiName GetInfo
 * @apiGroup Order
 * @apiParam (Params) {UUID} id Идентификатор заказа
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: {
 *        id: '004333e1-e684-4ddd-bc25-0dffbbf39229',
 *        number: 68,
 *        description: 'Комментарий к заказу',
 *        client: {
 *          id: '7e08e648-583c-40ad-bf2d-b810da53bf70',
 *          name: 'ООО \'СБЕРБАНК-СЕРВИС\''
 *        },
 *        user: {
 *            id: 'd500c3db-ad4e-4c8a-a57c-eac3331e13e0',
 *            fullName: 'Пупкин Иван Васильевич'
 *        },
 *        services: [
 *          {
 *            name: 'Получение электронной цифровой подписи без носителя',
 *            count: 1,
 *            cost: 666,
 *            serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *            serviceAddress: {
 *              id: '29ecb4f9-e89d-460a-95c2-bbbb84eb0688',
 *              street: 'Уральская',
 *              house: '17',
 *              apartment: null,
 *              postalCode: null,
 *              fiasId: null,
 *              city: {
 *                id: '88903722-56be-46e3-8b32-94737856812f',
 *                name: 'Барнаул',
 *                utcOffset: 0,
 *              },
 *              region: {
 *                id: 'd5ec8435-8910-479c-ba60-c3919fce1ac9',
 *                name: 'Алтайский край',
 *              },
 *            },
 *            device: {
 *              id: '28640cd0-f013-4c18-95e5-7159d518b70d',
 *              type: 'ККТ',
 *              alias: 'KKT',
 *              manufacturer: 'ШТРИХ',
 *              model: 'ЭЛВЕС-ФР-Ф',
 *              parameters: [
 *                {
 *                  value: 'Asus',
 *                  name: 'Производитель',
 *                  alias: 'MANUFACTURER',
 *                }
 *              ],
 *            },
 *          }
 *        ],
 *        tariffs: [
 *          {
 *            name: 'Удаленная помощь+ на год',
 *            count: 1,
 *            cost: 100500,
 *            serviceAddress: {
 *              id: '29ecb4f9-e89d-460a-95c2-bbbb84eb0688',
 *              street: 'Уральская',
 *              house: '17',
 *              apartment: null,
 *              postalCode: null,
 *              fiasId: null,
 *              city: {
 *                id: '88903722-56be-46e3-8b32-94737856812f',
 *                name: 'Барнаул',
 *                utcOffset: 0,
 *              },
 *              region: {
 *                id: 'd5ec8435-8910-479c-ba60-c3919fce1ac9',
 *                name: 'Алтайский край',
 *              },
 *            },
 *            device: {
 *              id: '28640cd0-f013-4c18-95e5-7159d518b70d',
 *              type: 'ККТ',
 *              alias: 'KKT',
 *              manufacturer: 'ШТРИХ',
 *              model: 'ЭЛВЕС-ФР-Ф',
 *              parameters: [
 *                {
 *                  value: 'Asus',
 *                  name: 'Производитель',
 *                  alias: 'MANUFACTURER',
 *                }
 *              ],
 *            }
 *          }
 *        ],
 *        responsible: 'Иванов Иван Иванович',
 *        paymentDate: '2020-12-24T12:46:28.726Z',
 *        documents: [
 *          {
 *            id: 'd47a02d6-662c-4a97-b4dc-97506a08f25b',
 *            name: 'Публичная оферта на приобретение услуг абонентского сервисного обслуживания.docx.docx'
 *          }
 *        ],
 *        totalCost: 100500,
 *        typePayment: 'invoice' | 'card' //выбранный способ оплаты
 *      },
 *      success: true,
 *      error: null,
 *      errorCode: null,
 *    }
 */
router.get(
  '/:id/get-info',
  validator.params(OrderController.validators.getInfo),
  asyncErrorHandler(OrderController.getInfo),
);

/**
 * @api {post} /api/order/add-document Загрузить документ к заказу
 * @apiVersion 1.0.0
 * @apiName AddDocument
 * @apiGroup Order
 * @apiParam (FormData) {String} orderId id заказа
 * @apiParam (FormData) {String} typeId id типа документа
 * @apiParam (FormData) {Object} document файл документа
 * @apiDescription Импорт устройств и их производителей из CRM
 * @apiSuccessExample Пример ответа:
 * HTTP/1.1 200 OK
 * {
 *   data: null,
 *   success: true,
 *   error: null,
 *   errorCode: null
 * }
 *
 */

router.post(
  '/add-document',
  fileStorageInstance.upload.single('document'),
  validator.body(OrderController.validators.addDocumentToOrder),
  asyncErrorHandler(OrderController.addDocumentToOrder),
);

/**
 * @api {get} /api/order/get-document-types Список типов документов
 * @apiVersion 1.0.0
 * @apiName GetDocumentTypes
 * @apiGroup Order
 * @apiParam (Query) {number} [limit=10] Кол-во записей
 * @apiParam (Query) {number} [offset=0] Сдвиг
 * @apiParam (Query) {String} [sortByDate=desc] Сортировка по дате. `asc` или `desc`
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: {
 *        count: 1,
 *        rows: [
 *        {
 *          id: '4d7ef473-c334-479a-8d6f-d0ee64dd7af5',
 *          name: 'Договор-оферта',
 *          alias: 'OFFER'
 *        }
 *      ]
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-document-types',
  validator.query(OrderController.validators.getDocumentTypes),
  asyncErrorHandler(OrderController.getDocumentTypes),
);

/**
 * @api {post} /api/order/create Создать заказ
 * @apiVersion 1.0.0
 * @apiName CreateOrder
 * @apiGroup Order
 * @apiParam (Body) {UUID} clientId id клиента для которого создается заказ
 * @apiParam (Body) {String} paymentType alias типа оплаты ('CARD', 'INVOICE')
 * @apiParam (Body) {Object[]} [tariffs] Тарифы
 * @apiParam (Body) {UUID} tariffs.id ID тарифа
 * @apiParam (Body) {UUID} [tariffs.serviceAddressId] ID устройства
 * @apiParam (Body) {Object[]} [services] Услуги
 * @apiParam (Body) {UUID} services.id ID услуги
 * @apiParam (Body) {String{0..1024}} [description] Комментарий к заказу
 * @apiParam (Body) {UUID} [services.serviceAddressId] ID услуги (требуется если у услуги статус адреса обслуживания "При заказе")
 * @apiParamExample {json}
 * {
 *   clientId: '3380c985-665c-4011-a674-72b187c531a1',
 *   paymentType: 'INVOICE',
 *   tariffs: [{
 *     id: '81e580c8-6d3d-4723-82de-f3775d435877',
 *     serviceAddressId: 'bc14c5c1-0c09-427d-8f79-82f5c74483a9',
 *   }],
 *   services: [{
 *     id: '63e02fdf-343a-449f-bb20-1d00c8addfca',
 *     serviceAddressId: 'bc14c5c1-0c09-427d-8f79-82f5c74483a9',
 *   }],
 * }
 *
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: null,
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.post(
  '/create',
  validator.body(OrderController.validators.createOrder),
  asyncErrorHandler(OrderController.createOrder),
);

/**
 * @api {post} /api/order/update Редактирование заказа
 * @apiVersion 1.0.0
 * @apiName UpdateOrder
 * @apiGroup Order
 * @apiParam (Body) {UUID} id  Идентификатор заказа
 * @apiParam (Body) {Date} paymentDate Дата оплаты в формате 'YYYY-MM-DD HH:mm:ss'
 * @apiParam (Body) {String{0..1024}} [description] Комментарий к заказу
 * @apiParam (Body) {Number{0..100}} discount Скидка в процентах в виде положительного целого числа от 1 до 100. Если стоит 0, то скидка удаляется
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: null,
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.post(
  '/update',
  validator.body(OrderController.validators.updateOrder),
  asyncErrorHandler(OrderController.updateOrder),
);

/**
 * @api {post} /api/order/add-offer-document Загрузить документ договора оферты
 * @apiVersion 1.0.0
 * @apiName AddOfferDocument
 * @apiGroup Order
 * @apiParam (FormData) {Object} document файл документа
 * @apiDescription Импорт устройств и их производителей из CRM
 * @apiSuccessExample Пример ответа:
 * HTTP/1.1 200 OK
 * {
 *   data: null,
 *   success: true,
 *   error: null,
 *   errorCode: null
 * }
 */

router.post(
  '/add-offer-document',
  offerDocumentFileStorageInstance.upload.single('document'),
  asyncErrorHandler(OrderController.addOfferDocument),
);

/**
 * @api {get} /api/order/document/:documentId/get-document Скачать документ
 * @apiVersion 1.0.0
 * @apiName GetDocument
 * @apiGroup Order
 * @apiParam (Params) {UUID} documentId ID документа
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: null,
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null
 *    }
 */
router.get(
  '/document/:documentId/get-document',
  validator.params(OrderController.validators.getDocument),
  asyncErrorHandler(OrderController.getDocument),
);

/**
 * @api {get} /api/order/:orderId/status Получить статус заказа
 * @apiVersion 1.0.0
 * @apiName GetStatus
 * @apiGroup Order
 * @apiParam (Params) {UUID} orderId Идентификатор заказа
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: "7d7fcc3f-0234-435f-9985-54120ef244dd",
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null
 *    }
 */
router.get(
  '/:orderId/status',
  validator.params(OrderController.validators.getStatus),
  asyncErrorHandler(OrderController.getStatus),
);

/**
 * @api {get} /api/order/get-status-list Список статусов заказа
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup Order
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: { },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-status-list',
  asyncErrorHandler(OrderController.getOrderStatusList),
);

export default router;
