import { Router } from 'express';
import validator from '../../../common/libs/requestValidator';

import ClientController from '../../controllers/ClientController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';

const router = new Router();

/**
 * @api {get} /api/client/get-all Список клиентов 'Все'
 * @apiVersion 1.0.0
 * @apiName GetAll
 * @apiGroup Client
 * @apiParam (Query) {String} [number] Поиск по номеру клиента
 * @apiParam (Query) {String} [name] Поиск по наименованию организации или ФИО физика
 * @apiParam (Query) {String} [firstName] Поиск по имени физика, если client='individual'
 * @apiParam (Query) {String} [lastName] Поиск по фамилии физика, если client='individual'
 * @apiParam (Query) {String} [fullName] Поиск по фио физика, если client='individual'
 * @apiParam (Query) {String} [inn] Поиск по номеру клиента, если client='organization'
 * @apiParam (Query) {String} [representative] Поиск фио представителя организации , если client='organization'
 * @apiParam (Query) {String} [phone] Поиск по номеру телефона, если client='individual' | 'organization'
 * @apiParam (Query) {String} [email] Поиск email, если client='individual' | 'organization'
 * @apiParam (Query) {String} [dateFrom] Дата создания 'От'
 * @apiParam (Query) {String} [dateTo] Дата создания 'До'
 * @apiParam (Query) {UUID} [responsibleId] Идентификатор ответственного
 * @apiParam (Query) {number} [limit=10] Кол-во записей
 * @apiParam (Query) {number} [offset=0] Сдвиг
 * @apiParam (Query) {String=all, individual, organization} [client='all'] Фильтрация по клиенту<br/>
 *                    <b>all</b> - Все<br/>
 *                    <b>individual</b> - Физические лица<br/>
 *                    <b>organization</b> - Юридический лица<br/>
 *
 * @apiParam (Query) {Array=["data","DESC"]} [sort=["data","DESC"]] Первым элементом идет название сортировки, вторым направление сортировки<br/>
 * Значение передается в формате JSON<br/><br/>
 *                    <b>date</b> - дате создания<br/>
 *                    <b>number</b> - номер клиента<br/>
 *                    <b>name</b> - название клиента<br/>
 *                    <b>organization</b> - название организации<br/>
 *                    <b>inn</b> - инн организации<br/>
 *                    <b>representative</b> - ФИО представителя организации<br/>
 *                    <b>phone</b> - номер телефона, по умолчанию сортировка для физика. Для юрика в параметрах нужно передать client='organization' <br/>
 *                    <b>email</b> - почта, по умолчанию сортировка для физика. Для юрика в параметрах нужно передать client='organization'<br/>
 *                    <b>firstName</b> - имя физика<br/>
 *                    <b>lastName</b> - фамилия физика<br/>
 *                    <b>responsible</b> - ответственный<br/>
 *
 * @apiParamExample {json} Пример запроса:
 * {
 *   name: 'Рога и ко',
 *   limit: 15,
 *   offset: 7,
 *   client: 'organization',
 *   sort: ["representative","ASC"],
 * }
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: {
 *        count: 6,
 *        rows: [
 *          {
 *            id: '474368e4-0010-49f7-a3a0-7ce7442590ce',
 *            number: 126,
 *            name: 'Физик Физиков',
 *            type: 'individual',
 *            createdAt: '2021-02-26T10:06:28.575Z',
 *            profile: 'ФЛ',
 *            organization: null,
 *            individual: {
 *              id: 'd500c3db-ad4e-4c8a-a57c-eac3331e13e0',
 *              email: 'fizik@yandex.ru',
 *              phone: '79003331188',
 *              fullName: 'Физик Физиков Физикович',
 *              lastName: 'Физиков',
 *              firstName: 'Физик',
 *              patronymicName: 'Физикович',
 *            },
 *            responsible: 'Сайт',
 *          },
 *          {
 *            id: 'e27e006f-db69-483e-8a2b-5cb4a458785d',
 *            number: 13,
 *            name: 'ЗАО Лапландия',
 *            type: 'organization',
 *            createdAt: '2021-02-01T09:18:54.817Z',
 *            profile: 'ЮЛ',
 *            organization: {
 *              id: '1b09d415-5d13-4895-a091-cab8c97fbb37',
 *              inn: '5551285234',
 *              name: 'ЗАО Лапландия',
 *              user: {
 *                id: 'bb7e1347-b47f-471a-92c9-8e18e013806d',
 *                email: 'lapty@mail.ru',
 *                phone: '79995553311',
 *                fullName: 'Вилле Хаапасало Константинович',
 *                lastName: 'Хаапасало',
 *                firstName: 'Вилле',
 *                patronymicName: 'Константинович',
 *                isMaintainer: true,
 *              },
 *            },
 *            individual: null,
 *            responsible: 'Админ Админ Админ',
 *          },
 *          {
 *            id: 'e5705b40-8aae-495e-be62-f462b89fd2a5',
 *            number: 1,
 *            name: 'ИП Джелацкий',
 *            type: 'organization',
 *            createdAt: '2021-01-28T21:48:52.472Z',
 *            profile: 'ЮЛ',
 *            organization: {
 *              id: '6f621d33-cde9-4a11-a243-3f5ac1d51605',
 *              inn: '9871285231',
 *              name: 'ИП Джелацкий',
 *              user: {
 *                id: '29adffae-7e1e-425e-a585-cdefd7c865fe',
 *                email: 'djl@mail.ru',
 *                phone: '79995552233',
 *                fullName: 'Багумил Джелацкий Львович',
 *                lastName: 'Джелацкий',
 *                firstName: 'Багумил',
 *                isMaintainer: true,
 *                patronymicName: 'Львович',
 *              },
 *            },
 *            individual: null,
 *            responsible: 'Сайт',
 *          },
 *          {
 *            id: '4482ce9c-5a64-4605-93a9-196be1f7d2d5',
 *            number: 9,
 *            name: 'ИП Пупкович',
 *            type: 'organization',
 *            createdAt: '2021-01-28T21:48:52.472Z',
 *            profile: 'ЮЛ',
 *            organization: {
 *              id: '6fb4f5a4-5b47-40c5-ad2a-12657d8e56f8',
 *              inn: '7736663045',
 *              name: 'ИП Пупкович',
 *              user: {
 *                id: '1d1a74d7-dbca-4535-a3e6-4243ad171dd6',
 *                email: 'pupok@mail.ru',
 *                phone: '79990005500',
 *                fullName: 'ИваньКо Пупкович Иванович',
 *                lastName: 'Пупкович',
 *                firstName: 'ИваньКо',
 *                patronymicName: 'Иванович',
 *                isMaintainer: true,
 *              },
 *            },
 *            individual: null,
 *            responsible: 'Сайт',
 *          },
 *        ],
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-all',
  validator.query(ClientController.validators.getAll),
  asyncErrorHandler(ClientController.getAll),
);

/**
 * @api {get} /api/client/get-organizations Список клиентов 'ЮЛ'
 * @apiVersion 1.0.0
 * @apiName GetEntities
 * @apiGroup Client
 * @apiParam (Query) {String} [name] Поиск по наименованию клиента
 * @apiParam (Query) {String} [number] Поиск по номеру клиента
 * @apiParam (Query) {String{10..12}} [inn] ИНН
 * @apiParam (Query) {String} [email] Email контактного лица
 * @apiParam (Query) {UUID} [taxTypeId] Идентификатор системы налогообложения
 * @apiParam (Query) {String} [representative] Поиск по ФИО представителя
 * @apiParam (Query) {String} [phone] Телефон контактного лица
 * @apiParam (Query) {String} [dateFrom] Дата создания 'От'
 * @apiParam (Query) {String} [dateTo] Дата создания 'До'
 * @apiParam (Query) {UUID} [responsibleId] Идентификатор ответственного
 * @apiParam (Query) {number} [limit=10] Кол-во записей
 * @apiParam (Query) {number} [offset=0] Сдвиг
 * @apiParam (Query) {String} [sortByDate=desc] Сортировка по дате. `asc` или `desc`
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: {
 *        count: 9,
 *        rows: [
 *        {
 *          id: '338a1099-eb46-4e6f-a53c-e9b48fcd5db4',
 *          number: 7,
 *          createdAt: '2021-01-20T23:19:22.837Z',
 *          name: 'ООО "СБЕРБАНК-СЕРВИС"',
 *          inn: '7736663049',
 *          representative: 'Иванов Иван Иванович',
 *          email: 'test@mail.ru',
 *          phone: '79159441115',
 *          responsible: 'Тест Тест Тестович'
 *        }
 *      ]
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-organizations',
  validator.query(ClientController.validators.getOrganizations),
  asyncErrorHandler(ClientController.getOrganizations),
);

/**
 * @api {get} /api/client/get-individuals Список клиентов 'ФЛ'
 * @apiVersion 1.0.0
 * @apiName GetIndividuals
 * @apiGroup Client
 * @apiParam (Query) {String} [number] Поиск по номеру клиента
 * @apiParam (Query) {String} [firstName] Поиск по фамилии клиента
 * @apiParam (Query) {String} [lastName] Поиск по фамилии клиента
 * @apiParam (Query) {String} [email] Email клиента
 * @apiParam (Query) {String} [phone] Телефон контактного лица
 * @apiParam (Query) {String} [dateFrom] Дата создания 'От'
 * @apiParam (Query) {String} [dateTo] Дата создания 'До'
 * @apiParam (Query) {UUID} [responsibleId] Идентификатор ответственного
 * @apiParam (Query) {number} [limit=10] Кол-во записей
 * @apiParam (Query) {number} [offset=0] Сдвиг
 * @apiParam (Query) {String} [sortByDate=desc] Сортировка по дате. `asc` или `desc`
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: '14fa4266-db2d-4bbd-90a7-0ba970acbf42',
 *          name: 'ООО Рога Гопника',
 *          inn: '123123123',
 *          createdAt: '2020-10-28T12:45:47.498Z',
 *          taxType: '12312',
 *          representative: 'Иванов Иван',
 *          email: 'foo@yandex.ru',
 *          phone: '79101239780',
 *          createdBy: null
 *        }
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-individuals',
  validator.query(ClientController.validators.getIndividuals),
  asyncErrorHandler(ClientController.getIndividuals),
);

/**
 * @api {get} /api/client/:id/get-info Объект клиента
 * @apiVersion 1.0.0
 * @apiName GetInfo
 * @apiGroup Client
 * @apiParam (Params) {UUID} id Идентификатор клиента
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *   {
 *     data: {
 *       client: {
 *         id: '4482ce9c-5a64-4605-93a9-196be1f7d2d5',
 *         number: 9,
 *         user: {
 *           fullName: 'Пупкович ИваньКо Иванович',
 *           id: '1d1a74d7-dbca-4535-a3e6-4243ad171dd6',
 *           firstName: 'ИваньКо',
 *           patronymicName: 'Иванович',
 *           lastName: 'Пупкович',
 *           email: 'ivan@mail.ru',
 *           phone: '79607550011',
 *           isMaintainer: true,
 *         },
 *         organization: {
 *           id: '6fb4f5a4-5b47-40c5-ad2a-12657d8e56f8',
 *           name: 'ИП Пупкович',
 *           inn: '7736663045',
 *           kpp: '999818233',
 *           realAddressId: 'c6377909-7753-421d-971c-1dc549dfe401',
 *           legalAddressId: '4f16d6b2-49cc-47fc-a81a-5c99fac09ee5',
 *           users: [
 *             {
 *               fullName: 'Пупкович ИваньКо Иванович',
 *               id: '1d1a74d7-dbca-4535-a3e6-4243ad171dd6',
 *               firstName: 'ИваньКо',
 *               patronymicName: 'Иванович',
 *               lastName: 'Пупкович',
 *               email: 'ivan@mail.ru',
 *               phone: '79607550011',
 *               isMaintainer: true,
 *             },
 *           ],
 *           legalAddress: {
 *             id: '19265160-8358-4e45-8b5c-0688c7a1711d',
 *             city: 'Ижевск',
 *             street: 'улица Ленина',
 *             house: '6',
 *             apartment: '1',
 *             postalCode: '287647',
 *             region: {
 *               id: '4680dce7-ac13-410c-8b7d-a467845fe7ed',
 *               name: 'Республика Удмуртия',
 *             },
 *           },
 *           realAddress: {
 *             id: '19265160-8358-4e45-8b5c-0688c7a1711d',
 *             city: 'Ижевск',
 *             street: 'улица Ленина',
 *             house: '13',
 *             apartment: '13',
 *             postalCode: '141415',
 *             region: {
 *               id: '4680dce7-ac13-410c-8b7d-a467845fe7ed',
 *               name: 'Республика Удмуртия',
 *             },
 *           },
 *           electronicDocumentCirculation: {
 *             id: '30ffaaa1-1c4f-42a5-b9de-87fa2426dc1f',
 *             name: 'СКБ Контур (Контур.Диадок)',
 *             alias: 'SKB_KONTUR_KONTUR_DIADOK',
 *           },
 *           electronicDocumentCirculationDescription: {
 *             description: 'это мой ЭДО, которого нет у вас в списке'
 *           },
 *           taxType: {
 *             id: 'ad24d66f-eda3-4844-a1de-1c6d055bb4b8',
 *             name: 'Общая система налогообложения (ОСНО)',
 *             alias: 'OSNO',
 *           },
 *           digitalSignature: {
 *             from: '12.08.1257',
 *             to: '12.08.2157'
 *           },
 *           bank: {
 *             id: 'ac17285f-f48c-4eac-b25c-b86fc2efd31e',
 *             name: 'ВВВТТТ',
 *             rcbic: '044525225',
 *             correspondentAccount: '30101810400000000225',
 *             bankAccount: '40702810500036265802',
 *           },
 *         },
 *       },
 *       serviceAddresses: [
 *         {
 *           id: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *           name: 'ККТ',
 *           alias: 'KKT',
 *           clientDevices: [
 *             {
 *               id: 'd82bca88-9359-40ec-b7c0-baaf405ab9d6',
 *               serviceAddress: {
 *                 id: '4ca45504-2b86-42e9-90f8-39c29a5a63d9',
 *                 city: null,
 *                 street: 'улица Ленина',
 *                 house: '1',
 *                 apartment: '123',
 *                 postalCode: '555888',
 *                 region: {
 *                   id: '4680dce7-ac13-410c-8b7d-a467845fe7ed',
 *                   name: 'Республика Удмуртия',
 *                 },
 *                 city: {
 *                   id: 'a02cd33a-b7de-41c6-b191-b41168054bc1',
 *                   name: 'Зеленоградск',
 *                 },
 *               },
 *               manufacturer: 'Эвотор',
 *               model: 'Эвотор 7.2',
 *               serialNumber: 'zzz888yyy000123',
 *             },
 *             {
 *               id: '353d0ab3-2195-4e41-a554-f1444233f1ed',
 *               serviceAddress: {
 *                 id: '05c21fa9-bc52-4359-a47f-7b226f4a49d1',
 *                 city: null,
 *                 street: 'Ленина',
 *                 house: '17',
 *                 apartment: '17',
 *                 postalCode: null,
 *                 region: {
 *                   id: 'd5ec8435-8910-479c-ba60-c3919fce1ac9',
 *                   name: 'Алтайский край',
 *                 },
 *                 city: {
 *                   id: '88903722-56be-46e3-8b32-94737856812f',
 *                   name: 'Барнаул',
 *                 },
 *               },
 *               manufacturer: 'Эвотор',
 *               model: 'Эвотор 7.3',
 *             },
 *             {
 *               id: '28640cd0-f013-4c18-95e5-7159d518b70d',
 *               serviceAddress: {
 *                 id: '29ecb4f9-e89d-460a-95c2-bbbb84eb0688',
 *                 city: null,
 *                 street: 'Уральская',
 *                 house: '17',
 *                 apartment: null,
 *                 postalCode: null,
 *                 region: {
 *                   id: 'd5ec8435-8910-479c-ba60-c3919fce1ac9',
 *                   name: 'Алтайский край',
 *                 },
 *                 city: {
 *                   id: '88903722-56be-46e3-8b32-94737856812f',
 *                   name: 'Барнаул',
 *                 },
 *               },
 *               manufacturer: 'ШТРИХ',
 *               model: 'ЭЛВЕС-ФР-Ф',
 *             },
 *           ],
 *         },
 *         {
 *           id: '14eeadde-dbcb-44ac-9d72-e1cc11617c98',
 *           name: 'Поддержка АРМ',
 *           alias: 'ARM',
 *           clientDevices: [
 *             {
 *               id: 'e60bc9ea-577c-401d-85c1-7ed964d5fe51',
 *               serviceAddress: {
 *                 id: '7ebdec1f-edd6-4c9c-943d-1c70d8db5ffa',
 *                 city: null,
 *                 street: 'улица Ленина',
 *                 house: '1',
 *                 apartment: '123',
 *                 postalCode: '555888',
 *                 region: {
 *                   id: '4680dce7-ac13-410c-8b7d-a467845fe7ed',
 *                   name: 'Республика Удмуртия',
 *                 },
 *                 city: {
 *                   id: 'fbfc5bc7-6a43-4b07-b2b7-17960dc81bc9',
 *                   name: 'Саров',
 *                 },
 *               },
 *               manufacturer: 'Asus',
 *               model: 'Zver',
 *               operationSystem: 'Windows10',
 *             },
 *           ],
 *         },
 *       ],
 *       orders: [
 *         {
 *           id: '7f1a951c-87ee-4512-957f-7e4297fca899',
 *           number: 1126,
 *           orderDate: '2021-02-25T18:30:56.464Z',
 *           orderStatus: 'Оплачен',
 *           items: {
 *             tariffs: [],
 *             services: [
 *               {
 *                 id: 'f7e7ce01-32f0-4fec-805d-c999349c0ff9',
 *                 name: 'Код активации для продления ОФД на 12 месяцев',
 *                 cost: 3000,
 *                 count: 1,
 *               },
 *               {
 *                 id: '923a5fab-1c6d-466d-9df3-068d2b2f58b2',
 *                 name: 'Код активации для продления ОФД на 15 месяцев',
 *                 cost: 7800,
 *                 count: 1,
 *               },
 *             ],
 *           },
 *           documents: [
 *             {
 *               id: 'c078fc0b-d121-4ee2-ada8-867ed75dcccc',
 *               name: 'Публичная оферта на приобретение услуг абонентского сервисного обслуживания.pdf',
 *             },
 *           ],
 *           totalCost: 10800,
 *         },
 *         {
 *           id: '1af4b1fd-949f-4c9a-8527-3b5a766755de',
 *           number: 1178,
 *           orderDate: '2021-03-15T08:48:15.958Z',
 *           orderStatus: 'Не оплачен',
 *           items: {
 *             tariffs: [
 *               {
 *                 id: '51baf95e-4009-43e3-b30e-e0253d070374',
 *                 name: 'Удаленная помощь+ на полгода',
 *                 cost: 7700,
 *                 count: 1,
 *               },
 *             ],
 *             services: [],
 *           },
 *           documents: [
 *             {
 *               id: 'c078fc0b-d121-4ee2-ada8-867ed75dcccc',
 *               name: 'Публичная оферта на приобретение услуг абонентского сервисного обслуживания.pdf',
 *             },
 *           ],
 *           totalCost: 7700,
 *         },
 *       ],
 *       active: [
 *          {
 *            tariffs: [
 *              {
 *                serviceAddress: {
 *                  id: '29ecb4f9-e89d-460a-95c2-bbbb84eb0688',
 *                  street: 'Уральская',
 *                  house: '17',
 *                  apartment: null,
 *                  postalCode: null,
 *                  fiasId: null,
 *                  city: {
 *                    id: '88903722-56be-46e3-8b32-94737856812f',
 *                    name: 'Барнаул',
 *                    utcOffset: 0,
 *                  },
 *                  region: {
 *                    id: 'd5ec8435-8910-479c-ba60-c3919fce1ac9',
 *                    name: 'Алтайский край',
 *                  },
 *                },
 *                device: {
 *                  id: '28640cd0-f013-4c18-95e5-7159d518b70d',
 *                  type: 'ККТ',
 *                  alias: 'KKT',
 *                  manufacturer: 'ШТРИХ',
 *                  model: 'ЭЛВЕС-ФР-Ф',
 *                  parameters: [],
 *                },
 *                tariff: {
 *                  id: '9c1c86ad-7a0d-4a8a-a2f1-8ee859bc9e68',
 *                  clientTariffId: '42fd6ba6-778e-4785-96eb-0abc934879c4',
 *                  name: 'Удаленная помощь+ на год',
 *                  alias: 'udalennaya-pomoshch-plyus-12',
 *                  label: null,
 *                  icon: null,
 *                  expirationAt: '2022-03-22T00:00:00.000Z',
 *                  services: [
 *                    {
 *                      id: 'beb0c0b4-f8b8-4494-afeb-ddab7a0f6110',
 *                      value: null, //всего
 *                      progress: 0, //в процессе выполнения (есть ЗНО)
 *                      used: 153, //использовано
 *                      service: {
 *                        id: '85db8d1a-d35d-43c3-91b1-7290be621af6',
 *                        name: 'Неограниченные консультации службы технической поддержки',
 *                        alias: 'konsultaciya-tekhnicheskoj-podderzhki',
 *                        label: null,
 *                        icon: null,
 *                        serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *                        deviceTypeId: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *                        limit: null,
 *                        limitMeasure: 'Количество работ в месяц',
 *                      },
 *                    },
 *                    {
 *                      id: 'f517d269-0dd3-489a-9d99-1a0dc182b480',
 *                      value: 5, //всего
 *                      progress: 2, //в процессе выполнения (есть ЗНО)
 *                      used: 2, //использовано
 *                      service: {
 *                        id: '64da2bbc-5519-4b34-baad-3930c48f441f',
 *                        name: 'Регистрация онлайн-кассы',
 *                        alias: 'registratsiya-onlajn-kassy',
 *                        label: null,
 *                        icon: 'iconPossTerminalSuccess',
 *                        serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *                        deviceTypeId: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *                        limit: 5,
 *                        limitMeasure: 'Количество работ в месяц',
 *                      },
 *                    },
 *                  ],
 *                },
 *              },
 *            ],
 *            services: [
 *              {
 *                clientServiceId: 'f2531d1d-16dd-4897-bbc0-b5833b80ecf9',
 *                completed: null,
 *                id: '1c8dad6d-9ca1-4bfb-8102-74bc2d763a13',
 *                name: 'Получение электронной цифровой подписи без носителя',
 *                alias: 'ehcp-standart-markirovka-egais-god',
 *                label: null,
 *                icon: null,
 *                serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *                deviceTypeId: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *                serviceAddress: {
 *                  id: '29ecb4f9-e89d-460a-95c2-bbbb84eb0688',
 *                  street: 'Уральская',
 *                  house: '17',
 *                  apartment: null,
 *                  postalCode: null,
 *                  fiasId: null,
 *                  city: {
 *                    id: '88903722-56be-46e3-8b32-94737856812f',
 *                    name: 'Барнаул',
 *                    utcOffset: 0,
 *                  },
 *                  region: {
 *                    id: 'd5ec8435-8910-479c-ba60-c3919fce1ac9',
 *                    name: 'Алтайский край',
 *                  },
 *                },
 *                device: {
 *                  id: '28640cd0-f013-4c18-95e5-7159d518b70d',
 *                  type: 'ККТ',
 *                  alias: 'KKT',
 *                  manufacturer: 'ШТРИХ',
 *                  model: 'ЭЛВЕС-ФР-Ф',
 *                  parameters: [],
 *                },
 *              },
 *            ],
 *          }
 *       ],
 *       archive: {
 *         tariffs: [
 *           {
 *              serviceAddress: {
 *                id: '4ca45504-2b86-42e9-90f8-39c29a5a63d9',
 *                street: 'улица Ленина',
 *                house: '1',
 *                apartment: '123',
 *                postalCode: '555888',
 *                fiasId: null,
 *                city: {
 *                  id: 'a02cd33a-b7de-41c6-b191-b41168054bc1',
 *                  name: 'Зеленоградск',
 *                  utcOffset: 0,
 *                },
 *                region: {
 *                  id: '4680dce7-ac13-410c-8b7d-a467845fe7ed',
 *                  name: 'Республика Удмуртия',
 *                },
 *              },
 *              device: {
 *                id: 'd82bca88-9359-40ec-b7c0-baaf405ab9d6',
 *                type: 'ККТ',
 *                alias: 'KKT',
 *                manufacturer: 'Эвотор',
 *                model: 'Эвотор 7.2',
 *                parameters: [
 *                  {
 *                    value: '12',
 *                    name: 'Гарантия',
 *                    alias: 'GARANTIYAM',
 *                  },
 *                  {
 *                    value: 'Да',
 *                    name: 'Работа от аккумулятора',
 *                    alias: 'RABOTA-OT-AKKUMULYATORA',
 *                  },
 *                  {
 *                    value: 'Нет',
 *                    name: 'Сенсорный дисплей',
 *                    alias: 'SENSORNYJ-DISPLEJ',
 *                  },
 *                  {
 *                    value: 'zzz888yyy000123',
 *                    name: 'Серийный номер',
 *                    alias: 'SERIAL_NUMBER',
 *                  },
 *                ],
 *              },
 *              tariff: {
 *                id: '2786d693-0b0d-4844-8c8f-f4a30298cf34',
 *                clientTariffId: '90a4c719-142f-4263-8ce4-c3c9f0fa1a3f',
 *                name: 'Все включено на полгода',
 *                alias: 'vse-vklyucheno-6',
 *                label: null,
 *                icon: null,
 *                expirationAt: '2019-09-03T00:00:00.000Z',
 *                services: [
 *                  {
 *                    id: 'c4e7237e-a801-46cd-b069-466463997f94',
 *                    value: null,
 *                    progress: 0,
 *                    used: 358,
 *                    service: {
 *                      id: '85db8d1a-d35d-43c3-91b1-7290be621af6',
 *                      name: 'Неограниченные консультации службы технической поддержки',
 *                      alias: 'konsultaciya-tekhnicheskoj-podderzhki',
 *                      label: null,
 *                      icon: null,
 *                      serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *                      deviceTypeId: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *                      limit: null,
 *                      limitMeasure: 'Количество работ в месяц',
 *                    },
 *                  },
 *                  {
 *                    id: 'b129fbf6-1c22-4806-ba93-1cd7b8ddcd05',
 *                    value: 5,
 *                    progress: 1,
 *                    used: 2,
 *                    service: {
 *                      id: '38dc53a1-dbb8-4037-826e-97deb43a6dd5',
 *                      name: 'Контроль сроков действия фискального накопителя (ФН) и договора ОФД',
 *                      alias: 'kontrol-srokov-dejstviya-fn-i-dogovora-ofd',
 *                      label: null,
 *                      icon: null,
 *                      serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *                      deviceTypeId: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *                      limit: 5,
 *                      limitMeasure: 'Количество работ в месяц',
 *                    },
 *                  },
 *                ],
 *              },
 *            }
 *         ],
 *         services: [
 *          {
 *             clientServiceId: 'b7789e4b-9006-4b60-9d55-3e7a46d00022',
 *             completed: '2021-03-22T08:48:00.697Z',
 *             id: '923a5fab-1c6d-466d-9df3-068d2b2f58b2',
 *             name: 'Код активации для продления ОФД на 15 месяцев',
 *             alias: 'kod-aktivatsii-ofd-15-mes',
 *             label: null,
 *             icon: 'iconOfd',
 *             serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *             deviceTypeId: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *             serviceAddress: {
 *               id: '7ebdec1f-edd6-4c9c-943d-1c70d8db5ffa',
 *               street: 'улица Ленина',
 *               house: '1',
 *               apartment: '123',
 *               postalCode: '555888',
 *               fiasId: null,
 *               city: {
 *                 id: 'fbfc5bc7-6a43-4b07-b2b7-17960dc81bc9',
 *                 name: 'Саров',
 *                 utcOffset: 0,
 *               },
 *               region: {
 *                 id: '4680dce7-ac13-410c-8b7d-a467845fe7ed',
 *                 name: 'Республика Удмуртия',
 *               },
 *             },
 *             device: {
 *               id: 'e60bc9ea-577c-401d-85c1-7ed964d5fe51',
 *               type: 'Поддержка АРМ',
 *               alias: 'ARM',
 *               model: 'ПК',
 *               parameters: [
 *                 {
 *                   value: 'Asus',
 *                   name: 'Производитель',
 *                   alias: 'MANUFACTURER',
 *                 },
 *                 {
 *                   value: 'Zver',
 *                   name: 'Модель',
 *                   alias: 'MODEL',
 *                 },
 *                 {
 *                   value: 'Windows10',
 *                   name: 'Операционная система',
 *                   alias: 'OPERATION_SYSTEM',
 *                 },
 *               ],
 *             },
 *           }
 *         ],
 *       },
 *     },
 *     success: true,
 *     error: null,
 *     errorCode: null,
 *   }
 *
 */
router.get(
  '/:id/get-info',
  validator.params(ClientController.validators.getInfo),
  asyncErrorHandler(ClientController.getInfo),
);

/**
 * @api {post} /api/client/register/organization Регистрация ЮЛ или ИП
 * @apiVersion 1.0.0
 * @apiName RegisterOrganization
 * @apiGroup Client
 * @apiParam (Body) {UUID} [leadId] Идентификатор лида с шага "Контактные данные"
 * @apiParam (Body) {Object} user Представитель клиента
 * @apiParam (Body) {String} user.firstName Имя представителя
 * @apiParam (Body) {String} user.lastName Фамилия представителя
 * @apiParam (Body) {String} [user.patronymicName] Отчество представителя
 * @apiParam (Body) {String} user.email Email
 * @apiParam (Body) {String} user.phone Телефон
 * @apiParam (Body) {String} [user.birthday] День рождения в формате 'DD.MM.YYYY'
 *
 * @apiParam (Body) {Object} organization Данные организации
 * @apiParam (Body) {String{2..}} organization.name Наименование
 * @apiParam (Body) {String{10..12}} organization.inn ИНН
 * @apiParam (Body) {String{9}} organization.kpp КПП
 * @apiParam (Body) {UUID} organization.electronicDocumentCirculationId Идентификатор оператора ЭДО.
 * @apiParam (Body) {String{..1024}} [organization.electronicDocumentCirculationDescription] Если выбрано "Моего оператора нет в списке", нужно указать его тут.
 *
 * @apiParam (Body) {Boolean} organization.isSameAddress Флаг одинаковых юридического и фактического адресов
 *
 * @apiParam (Body) {Object} organization.legalAddress Юридический адрес
 * @apiParam (Body) {UUID} organization.legalAddress.region Субъект РФ
 * @apiParam (Body) {String} [organization.legalAddress.city] Город строкой (должно быть только что-то одно)
 * @apiParam (Body) {UUID} [organization.legalAddress.cityId] Город из списка (должно быть только что-то одно)
 * @apiParam (Body) {String} organization.legalAddress.street Улица
 * @apiParam (Body) {String} organization.legalAddress.house Дом
 * @apiParam (Body) {String} organization.legalAddress.apartment Квартира/офис
 * @apiParam (Body) {String} [organization.legalAddress.postalCode] Почтовый индекс
 *
 * @apiParam (Body) {Object} [organization.realAddress] Фактический адрес
 * @apiParam (Body) {UUID} [organization.realAddress.region] Субъект РФ
 * @apiParam (Body) {String} [organization.realAddress.city] Город строкой (должно быть только что-то одно)
 * @apiParam (Body) {UUID} [organization.realAddress.cityId] Город из списка (должно быть только что-то одно)
 * @apiParam (Body) {String} organization.realAddress.street Улица
 * @apiParam (Body) {String} organization.realAddress.house Дом
 * @apiParam (Body) {String} [organization.realAddress.apartment] Квартира/офис
 * @apiParam (Body) {String} [organization.realAddress.postalCode] Почтовый индекс
 *
 * @apiParam (Body) {Object} organization.bankDetail Банковские реквизиты.
 * @apiParam (Body) {String{2..100}} organization.bankDetail.name Название банка.
 * @apiParam (Body) {String{20}} organization.bankDetail.bankAccount Расчетный счет.
 * @apiParam (Body) {String{20}} organization.bankDetail.correspondentAccount Кор. счет.
 * @apiParam (Body) {String{9}} organization.bankDetail.rcbic БИК.
 * @apiParam (Body) {UUID} bankDetail.taxTypeId Идентификатор системы налогообложения
 * @apiSuccessExample Пример ответа:
 *  HTTP/1.1 200 OK
 *    {
 *      data: 'd758d71c-77d8-4941-b262-e612ea934e17'
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null
 *    }
 */
router.post(
  '/register/organization',
  validator.body(ClientController.validators.registerOrganization),
  asyncErrorHandler(ClientController.registerClient),
);

/**
 * @api {post} /api/client/register/individual Регистрация ФЛ
 * @apiVersion 1.0.0
 * @apiName RegisterIndividual
 * @apiGroup Client
 * @apiParam (Body) {UUID} leadId Идентификатор лида с шага "Контактные данные"
 * @apiParam (Body) {Object} user Клиент
 * @apiParam (Body) {String} user.firstName Имя клиента
 * @apiParam (Body) {String} [user.patronymicName] Отчество клиента
 * @apiParam (Body) {String} user.lastName Фамилия клиента
 * @apiParam (Body) {String} user.phone Телефон клиента
 * @apiParam (Body) {String} user.email Email клиента
 * @apiSuccessExample Пример ответа:
 *  HTTP/1.1 200 OK
 *    {
 *      data: 'd758d71c-77d8-4941-b262-e612ea934e17'
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null
 *    }
 */
router.post(
  '/register/individual',
  validator.body(ClientController.validators.registerIndividual),
  asyncErrorHandler(ClientController.registerClient),
);


/**
 * @api {get} /api/client/allowed-services Доступные услуги клиента
 * @apiVersion 1.0.0
 * @apiName GetAllowedServices
 * @apiGroup Client
 * @apiParam (Query) {UUID} clientId Идентификатор клиента
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *   {
 *     data: [
 *       {
 *         serviceAddressType: 'inOrder',
 *         services: [
 *           {
 *             id: '85db8d1a-d35d-43c3-91b1-7290be621af6',
 *             name: 'Неограниченные консультации службы технической поддержки',
 *             alias: 'konsultaciya-tekhnicheskoj-podderzhki',
 *             deviceType: 'kkt',
 *             serviceAddressType: 'inOrder',
 *             value: null,
 *           },
 *           {
 *             id: '64da2bbc-5519-4b34-baad-3930c48f441f',
 *             name: 'Регистрация онлайн-кассы',
 *             alias: 'registratsiya-onlajn-kassy',
 *             deviceType: 'kkt',
 *             serviceAddressType: 'inOrder',
 *             value: 5,
 *           },
 *         ],
 *         serviceAddress: {
 *           id: '29ecb4f9-e89d-460a-95c2-bbbb84eb0688',
 *           street: 'Уральская',
 *           house: '17',
 *           apartment: null,
 *           postalCode: null,
 *           fiasId: null,
 *           city: {
 *             id: '88903722-56be-46e3-8b32-94737856812f',
 *             name: 'Барнаул',
 *             utcOffset: 0,
 *           },
 *           region: {
 *             id: 'd5ec8435-8910-479c-ba60-c3919fce1ac9',
 *             name: 'Алтайский край',
 *           },
 *         },
 *       },
 *       {
 *         services: [
 *           {
 *             id: '8d9d7dea-8e88-4139-9097-442286fcc1d7',
 *             name: 'Снятие с учета онлайн-кассы',
 *             alias: 'snyatie-onlajn-kassy-s-ucheta',
 *             deviceType: 'kkt',
 *             serviceAddressType: 'inOrder',
 *             value: 8,
 *           },
 *           {
 *             id: '38dc53a1-dbb8-4037-826e-97deb43a6dd5',
 *             name: 'Контроль сроков действия фискального накопителя (ФН) и договора ОФД',
 *             alias: 'kontrol-srokov-dejstviya-fn-i-dogovora-ofd',
 *             deviceType: 'kkt',
 *             serviceAddressType: 'inOrder',
 *             value: 1,
 *           },
 *         ],
 *         serviceAddressType: 'inOrder',
 *         serviceAddress: {
 *           id: '66ecb6f6-e66d-660a-95c2-bbbb84eb0666',
 *           street: 'Красная',
 *           house: '25',
 *           apartment: null,
 *           postalCode: null,
 *           fiasId: null,
 *           city: {
 *             id: '88903722-56be-46e3-8b32-94737856812f',
 *             name: 'Москва',
 *             utcOffset: 0,
 *           },
 *           region: {
 *             id: 'd5ec8435-8910-479c-ba60-c3919fce1ac9',
 *             name: 'Москва',
 *           },
 *         },
 *       },
 *       {
 *         services: [
 *           {
 *             id: '923a5fab-1c6d-466d-9df3-068d2b2f58b2',
 *             name: 'Код активации для продления ОФД на 15 месяцев',
 *             alias: 'kod-aktivatsii-ofd-15-mes',
 *             deviceType: 'kkt',
 *             serviceAddressType: 'notRequested',
 *             value: 2,
 *           },
 *         ],
 *         serviceAddressType: 'notRequested',
 *         serviceAddress: null,
 *       },
 *       {
 *         services: [
 *           {
 *             id: '6571bc04-2ddb-49df-a4b9-c8e2e206fbf9',
 *             name: 'Техосмотр ноутбука или компьютера',
 *             alias: 'diagnostika-pk',
 *             deviceType: 'arm',
 *             serviceAddressType: 'inTicket',
 *             value: 6,
 *           },
 *         ],
 *         serviceAddressType: 'inTicket',
 *         serviceAddress: null,
 *       },
 *     ],
 *     success: true,
 *     error: null,
 *     errorCode: null,
 *   }
 *
 */
router.get(
  '/allowed-services',
  validator.query(ClientController.validators.getClientAllowedServices),
  asyncErrorHandler(ClientController.getClientAllowedServices),
);

export default router;
