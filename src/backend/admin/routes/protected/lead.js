import { Router } from 'express';
import validator from '../../../common/libs/requestValidator';

import LeadController from '../../controllers/LeadController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';

const router = new Router();

/**
 * @api {get} /api/lead/get-list Список лидов
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup Lead
 * @apiParam (Query) {String} [representative] Поиск фио лида
 * @apiParam (Query) {Number} [number] Номер лида
 * @apiParam (Query) {UUID} [leadNeedId] Идентификатор потребности лида
 * @apiParam (Query) {UUID} [clientId] Идентификатор клиента (допустимое значение: null)
 * @apiParam (Query) {String} [phone] Контактный телефон лида
 * @apiParam (Query) {String} [email] Почта лида
 * @apiParam (Query) {UUID} [cityId] Идентификатор города обслуживания (допустимое значение: null)
 * @apiParam (Query) {UUID} [deviceManufacturerId] Идентификатор производителя оборудования (допустимое значение: null)
 * @apiParam (Query) {UUID} [deviceModelId] Идентификатор модели оборудования (допустимое значение: null)
 * @apiParam (Query) {UUID} [leadStatusId] Идентификатор статуса лида
 * @apiParam (Query) {UUID} [leadConsultationResultId] Идентификатор результата консультации
 * @apiParam (Query) {UUID} [responsibleId] Идентификатор ответственного
 * @apiParam (Query) {String} [dateFrom] Дата создания 'От'
 * @apiParam (Query) {String} [dateTo] Дата создания 'До'
 * @apiParam (Query) {String} [nextCallDateFrom] Дата следующего звонка 'От' (YYYY-MM-DD HH:mm:ss)
 * @apiParam (Query) {String} [nextCallDateTo] Дата следующего звонка 'До' (YYYY-MM-DD HH:mm:ss)
 * @apiParam (Query) {Number} [limit=10] Кол-во записей
 * @apiParam (Query) {Number} [offset=0] Сдвиг
 * @apiParam (Query) {String=all, individual, organization, empty} [client='all'] Фильтрация по клиенту<br/>
 *                    <b>all</b> - Все<br/>
 *                    <b>individual</b> - Физические лица<br/>
 *                    <b>organization</b> - Юридический лица<br/>
 *                    <b>empty</b> - Без профиля<br/>
 * @apiParam (Query) {Array=["data","DESC"]} [sort=["data","DESC"]] Первым элементом идет название сортировки, вторым направление сортировки<br/>
 * Значение передается в формате JSON<br/><br/>
 *                    <b>representative</b> - фио лида<br/>
 *                    <b>date</b> - дате создания<br/>
 *                    <b>number</b> - номеру лида<br/>
 *                    <b>phone</b> - номеру телефона<br/>
 *                    <b>email</b> - почте<br/>
 *                    <b>city</b> - городу<br/>
 *                    <b>nextCallDate</b> - дате связи<br/>
 *                    <b>responsible</b> - ответственному<br/>
 *                    <b>client</b> - имени клиента. Сортировка если в фильтре client='individual' | 'organization'<br/>
 *
 * @apiParamExample {json} Пример запроса:
 * {
 *   representative: 'иван',
 *   limit: 15,
 *   offset: 15,
 *   client: 'organization',
 *   sort: ["representative","ASC"],
 * }
 *
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: {
 *        count: 45334545,
 *        rows: [
 *        {
 *          profile: 'ФЛ',
 *          fullName: 'ФИО представителя',
 *          id: 'cde6f193-cb5f-4fbc-baf6-3cf079ce564c',
 *          number: 2,
 *          phone: '79101239780',
 *          createdAt: '2020-11-02T08:09:46.854Z',
 *          leadNeed: 'Потребность клиента',
 *          client: 'Наименование клиента' | null,
 *          city: 'Краснодар',
 *          deviceManufacturer: 'Наименование производителя' | null,
 *          deviceModel: 'Модель устройства' | null,
 *          leadStatus: 'Статус лида',
 *          leadConsultationResult: 'Результат консультации' | null,
 *          responsible: 'Иванов Иван Иванович' | null,
 *          nextCallDate: '2020-11-02T08:09:46.854Z' | null
 *        }
 *      ]
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-list',
  validator.query(LeadController.validators.getList),
  asyncErrorHandler(LeadController.getList),
);

/**
 * @api {get} /api/lead/:id/get-info Карточка лида
 * @apiVersion 1.0.0
 * @apiName GetInfo
 * @apiGroup Lead
 * @apiParam (Params) {UUID} id Идентификатор лида
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: {
 *        id: '97094fd4-a0b9-4ba8-a1f5-8746a8d03990',
 *        number: 1,
 *        phone: '79101239780',
 *        createdAt: '2020-11-02T11:14:53.819Z',
 *        firstName: 'Имя',
 *        patronymicName: 'Отчество',
 *        lastName: 'Фамилия',
 *        comment: 'Комментарий',
 *        form: 'Панель администратора',
 *        operationSystem: 'Windows 7 Ultimate' | null,
 *        nextCallDate: '2020-11-02T08:09:46.854Z' | null,
 *        leadNeed: {
 *          id: '25ad376e-75a8-4e81-8900-1304975a6bbb',
 *          name: 'Покупка услуги / тарифного плана'
 *        },
 *        deviceType: {
 *           id: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *           name: 'ККТ',
 *           alias: 'KKT'
 *         },
 *        organization': {
 *          id: '14fa4266-db2d-4bbd-90a7-0ba970acbf42',
 *          name: 'ООО Рога Гопника'
 *        } | null,
 *        city: {
 *          id: '14fa4266-db2d-4bbd-90a7-0ba970acbf42',
 *          name: 'Москва',
 *          utcOffset: 3
 *        } | null,
 *        deviceManufacturer: {
 *          id: '14fa4266-db2d-4bbd-90a7-0ba970acbf42',
 *          name: 'Эвотор'
 *        } | null,
 *        deviceModel: {
 *          id: '14fa4266-db2d-4bbd-90a7-0ba970acbf42',
 *          name: 'Касса 10''
 *        } | null,
 *        leadStatus: {
 *          id: '726af6b4-8c4d-49e2-ab20-c7b7f65dd06b',
 *          name: 'Новый',
 *          alias: 'NEW'
 *        },
 *        leadConsultationResult: {
 *          id: '420d8b50-3372-452b-917e-480d7f0f46d0',
 *          name: 'Хочет купить',
 *          alias: 'WANTS_TO_BY'
 *        } | null,
 *        leadSource: {
 *          "id": "206d5522-bef4-4ae3-8837-459793c06406",
 *          "name": "source",
 *          "alias": "SOURCE"
 *        } | null,
 *        responsible: {
 *          id: '420d8b50-3372-452b-917e-480d7f0f46d0',
 *          fullName: 'Иванов Иван Иванович'
 *        } | null,
 *        tariffs: [
 *          {
 *            id: '7db9aa02-7f43-4462-a677-0c1973ef462e',
 *            name: 'Tariff 1'
 *          }
 *        ],
 *        services: [
 *          {
 *            id: '7db9aa02-7f43-4462-a677-0c1973ef462e',
 *            name: 'Service 1'
 *          }
 *        ],
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/:id/get-info',
  validator.params(LeadController.validators.getInfo),
  asyncErrorHandler(LeadController.getInfo),
);

/**
 * @api {get} /api/lead/get-statuses Справочник статусов лида
 * @apiVersion 1.0.0
 * @apiName GetStatuses
 * @apiGroup Lead
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: '97094fd4-a0b9-4ba8-a1f5-8746a8d03990',
 *          name: 'Новый',
 *          alias: 'NEW'
 *        }
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-statuses',
  asyncErrorHandler(LeadController.getStatuses),
);

/**
 * @api {get} /api/lead/get-sources Справочник источников лида
 * @apiVersion 1.0.0
 * @apiName GetSources
 * @apiGroup Lead
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: '97094fd4-a0b9-4ba8-a1f5-8746a8d03990',
 *          name: 'Эвотор',
 *          alias: 'EVOTOR'
 *        }
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-sources',
  asyncErrorHandler(LeadController.getSources),
);

/**
 * @api {get} /api/lead/get-consultation-results Справочник результатов консультации лида
 * @apiVersion 1.0.0
 * @apiName GetConsultationResults
 * @apiGroup Lead
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: '97094fd4-a0b9-4ba8-a1f5-8746a8d03990',
 *          name: 'Хочет купить',
 *          alias: 'WANTS_TO_BY',
 *        }
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-consultation-results',
  asyncErrorHandler(LeadController.getConsultationResults),
);

/**
 * @api {get} /api/lead/get-needs Справочник потребностей лида
 * @apiVersion 1.0.0
 * @apiName GetNeeds
 * @apiGroup Lead
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: '97094fd4-a0b9-4ba8-a1f5-8746a8d03990',
 *          name: 'Потребность',
 *          alias: 'NEED',
 *        }
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-needs',
  asyncErrorHandler(LeadController.getNeeds),
);

/**
 * @api {post} /api/lead/create Создание лида
 * @apiVersion 1.0.0
 * @apiName Create
 * @apiParam (Body) {Boolean} isOrganization Профиль лида
 * @apiParam (Body) {String} firstName Имя лида
 * @apiParam (Body) {String} [patronymicName] Отчество лида
 * @apiParam (Body) {String} [lastName] Фамилия лида
 * @apiParam (Body) {String} phone Контактный телефон
 * @apiParam (Body) {UUID} deviceTypeId Идентификатор продукта
 * @apiParam (Body) {UUID} [clientId] Идентификатор клиента
 * @apiParam (Body) {UUID} [cityId] Идентификатор города
 * @apiParam (Body) {UUID} leadNeedId Идентификатор потребности лида
 * @apiParam (Body) {UUID} [deviceManufacturerId] Идентификатор производителя оборудования
 * @apiParam (Body) {UUID} [deviceModelId] Идентификатор модели оборудования
 * @apiParam (Body) {String} [comment] Комментарий
 * @apiParam (Body) {UUID} leadConsultationResultId Идентификатор результата консультации
 * @apiParam (Body) {String} [nextCallDate] Дата и время следующего звонка лиду (YYYY-MM-DD HH:mm:ss)
 * @apiParam (Body) {UUID[]} [tariffs] Тарифы, которые были интересны лиду
 * @apiParam (Body) {UUID[]} [services] Разовые услуги, которые были интересны лиду
 * @apiParam (Body) {String} [operationSystem] Операционная система (только для ARM)
 * @apiParam (Body) {UUID} [leadSourceId] Источник лида
 * @apiGroup Lead
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: '97094fd4-a0b9-4ba8-a1f5-8746a8d03990',
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.post(
  '/create',
  validator.body(LeadController.validators.createLead),
  asyncErrorHandler(LeadController.createLead),
);

/**
 * @api {post} /api/lead/update Обновление лида
 * @apiVersion 1.0.0
 * @apiName Update
 * @apiParam (Body) {UUID} id Идентификатор обновляемой записи
 * @apiParam (Body) {Boolean} isOrganization Профиль лида
 * @apiParam (Body) {String} firstName Имя лида
 * @apiParam (Body) {String} [patronymicName] Отчество лида
 * @apiParam (Body) {String} [lastName] Фамилия лида
 * @apiParam (Body) {String} phone Контактный телефон
 * @apiParam (Body) {UUID} deviceTypeId Идентификатор продукта
 * @apiParam (Body) {UUID} [clientId] Идентификатор клиента
 * @apiParam (Body) {UUID} [cityId] Идентификатор города
 * @apiParam (Body) {UUID} leadNeedId Идентификатор потребности лида
 * @apiParam (Body) {UUID} [deviceManufacturerId] Идентификатор производителя оборудования
 * @apiParam (Body) {UUID} [deviceModelId] Идентификатор модели оборудования
 * @apiParam (Body) {String} [comment] Комментарий
 * @apiParam (Body) {UUID} consultationResultId Идентификатор результата консультации
 * @apiParam (Body) {String} [nextCallDate] Дата и время следующего звонка лиду (YYYY-MM-DD HH:mm:ss)
 * @apiParam (Body) {UUID[]} [tariffs] Тарифы, которые были интересны лиду
 * @apiParam (Body) {UUID[]} [services] Разовые услуги, которые были интересны лиду
 * @apiParam (Body) {String} [operationSystem] Операционная система (только для ARM)
 * @apiParam (Body) {UUID} [leadSourceId] Источник лида
 * @apiGroup Lead
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: '97094fd4-a0b9-4ba8-a1f5-8746a8d03990',
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.post(
  '/update',
  validator.body(LeadController.validators.updateLead),
  asyncErrorHandler(LeadController.updateLead),
);

export default router;
