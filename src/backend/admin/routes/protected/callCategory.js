import { Router } from 'express';

import CallCategoryController from '../../controllers/CallCategoryController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';

const router = new Router();

/**
 * @api {get} /api/call-category/get-list Список категорий звонков
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup CallCategory
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: '45a863fb-8e42-4dd5-992c-f668d812a26f'
 *          name: 'Продажа',
 *          alias: 'SALE'
 *        },
 *        {
 *          id: '6a0e7e03-86d7-41c1-98e4-abbcc02bf226'
 *          name: 'Консультация',
 *          alias: 'CONSULTATION'
 *        },
 *        {
 *          id: '2cd1543f-a4e3-4d78-aaef-e79e2e9f2ef9'
 *          name: 'Другое',
 *          alias: 'OTHER'
 *        }
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-list',
  asyncErrorHandler(CallCategoryController.getList),
);

export default router;
