import { Router } from 'express';

import validator from '../../../common/libs/requestValidator';
import DeviceController from '../../controllers/DeviceController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';

const router = new Router();

/**
 * @api {post} /api/device/create-client-device Создание оборудования клиента
 * @apiVersion 1.0.0
 * @apiName CreateClientDevice
 * @apiGroup Device
 * @apiParam (Body) {String} model UUID модели устройства
 * @apiParam (Body) {String} clientId UUID клиента
 * @apiParam (Body) {Array} parameters Коллекция параметров
 * @apiParam (Body) {String[]} parameters.id UUID параметра
 * @apiParam (Body) {String[]} parameters.value Значение параметра
 * @apiParam (Body) {Object} address Адрес обслуживания устройства
 * @apiParam (Body) {UUID} address.region Субъект РФ
 * @apiParam (Body) {UUID} address.cityId Город из списка обслуживаемых городов
 * @apiParam (Body) {String} address.street Улица
 * @apiParam (Body) {String} address.house Дом
 * @apiParam (Body) {String} address.apartment Квартира/офис
 * @apiParam (Body) {String} [address.postalCode] Почтовый индекс
 * @apiParamExample {json} Пример запроса:
 * {
 *   model: 'bc14c5c1-0c09-427d-8f79-82f5c74483a9',
 *   clientId: 'bc14c5c1-0c09-427d-8f79-82f5c74483a9',
 *   parameters: [
 *     {
 *       id: '31cb14f6-a421-4b58-85f2-90f643a50790',
 *       value: '888555000123',
 *     },
 *     {
 *       id: 'd0dae0b0-0dd4-4c9b-8f70-8eb519a0cda1',
 *       value: '7.5 В, встроенный аккумулятор 2600 мАч',
 *     },
 *     {
 *       id: 'd2707b66-b9e8-4cf4-8600-d9db716dd32e',
 *       value: 'ARM 9 Zatara 180 МГц',
 *     },
 *   ],
 *   address: {
 *     region: '59637637-4bbc-49b8-91cc-58c2cb43e51e',
 *     cityId: '21918605-05f3-42e9-bde4-eaa44a5c8eb1',
 *     street: 'Невский пр.',
 *     house: '1',
 *     apartment: '123',
 *     postalCode: '000555',
 *   }
 * }
 *
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: {
 *        manufacturer: 'VeriFone',
 *        model: 'Vx520',
 *        address: {
 *          region: 'Санкт-Петербург и Ленинградская область',
 *          city: 'Санкт-Петербург',
 *          street: 'Невский пр.',
 *          house: '1',
 *          apartment: '123',
 *          postalCode: '000555',
 *        },
 *        parameters: [
 *          {
 *            id: '31cb14f6-a421-4b58-85f2-90f643a50790',
 *            key: 'Серийный номер',
 *            value: '888555000123',
 *          },
 *          {
 *            id: 'd0dae0b0-0dd4-4c9b-8f70-8eb519a0cda1',
 *            key: 'Питание',
 *            value: '7.5 В, встроенный аккумулятор 2600 мАч',
 *          },
 *          {
 *            id: 'd2707b66-b9e8-4cf4-8600-d9db716dd32e',
 *            key: 'Процессор',
 *            value: 'ARM 9 Zatara 180 МГц',
 *          },
 *        ],
 *      },
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.post(
  '/create-client-device',
  validator.body(DeviceController.validators.createClientDevice),
  asyncErrorHandler(DeviceController.createClientDevice),
);

/**
 * @api {get} /api/device/get-list Получение списка оборудования клиентов
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup Device
 * @apiParam (Query) {String} [deviceModelName] Поиск по наименованию модели устройства
 * @apiParam (Query) {String} [clientName] Поиск по наименованию клиента
 * @apiParam (Query) {String} [parameterValue] Поиск по значению параметра устройства (например, серийник)
 * @apiParam (Query) {String} [deviceTypeId] UUID тип устройства
 * @apiParam (Query) {String} [deviceModelId] UUID модель устройства
 * @apiParam (Query) {String} [deviceManufacturerId] UUID производитель устройства устройства
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: 'a63b5868-2389-4f82-bd49-117ccbf17545',
 *          client: 'ИП Васильевич',
 *          deviceType: 'Кассы',
 *          deviceModel: {
 *            id: '19265160-8358-4e45-8b5c-0688c7a1711d',
 *            name: 'Эвотор 7.2'
 *          },
 *          deviceManufacturer: 'Эвотор',
 *          address: {
 *            street: 'улица Ленина',
 *            house: '1',
 *            apartment: '123',
 *            postalCode: '555888',
 *            city: {
 *              id: '19265160-8358-4e45-8b5c-0688c7a1711d',
 *              name: 'Ижевск',
 *            },
 *            region: {
 *              id: '4680dce7-ac13-410c-8b7d-a467845fe7ed',
 *              name: 'Республика Удмуртия',
 *            },
 *          },
 *          parameters: [
 *            {
 *              id: 'd194b253-227f-4759-8781-b505785aa689',
 *              name: 'Серийный номер',
 *              alias: 'SERIAL_NUMBER',
 *              value: 'zzz888yyy000123',
 *            },
 *          ],
 *        },
 *      ]
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-list',
  validator.query(DeviceController.validators.getList),
  asyncErrorHandler(DeviceController.getList),
);

/**
 * @api {get} /api/device/get-device-manufacturers Получение списка производителей оборудования
 * @apiVersion 1.0.0
 * @apiName GetDeviceManufacturers
 * @apiGroup Device
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: "6e586c9e-35f9-4dc4-bdf0-a4f946fd2773",
 *          name: "Эвотор",
 *        }
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-device-manufacturers',
  asyncErrorHandler(DeviceController.getDeviceManufacturers),
);

/**
 * @api {get} /api/device/get-device-models Получение списка оборудования
 * @apiVersion 1.0.0
 * @apiName GetDeviceModels
 * @apiParam (Query) {String} deviceManufacturerId Производитель устройства
 * @apiGroup Device
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: "6e586c9e-35f9-4dc4-bdf0-a4f946fd2773",
 *          name: "Касса 5",
 *        }
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-device-models',
  validator.query(DeviceController.validators.getDeviceModels),
  asyncErrorHandler(DeviceController.getDeviceModels),
);

/**
 * @api {get} /api/device/:id/get-device-services Получение списка услуг привязанных к оборудованию
 * @apiVersion 1.0.0
 * @apiName GetDeviceServices
 * @apiGroup Device
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *         {
 *           id: "7db9aa02-7f43-4462-a677-0c1973ef462e",
 *           name: "Название услуги",
 *         }
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/:id/get-device-services',
  validator.params(DeviceController.validators.getDeviceServices),
  asyncErrorHandler(DeviceController.getDeviceServices),
);

/**
 * @api {get} /api/device/:id/get-parameters Список параметров
 * @apiVersion 1.0.0
 * @apiName DeviceParameters
 * @apiGroup Device
 * @apiParam (Params) {UUID} id ID модели для которой необходимо получить доступные параметры.
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: '31cb14f6-a421-4b58-85f2-90f643a50790',
 *          name: 'Серийный номер',
 *          alias: 'SERIAL_NUMBER'
 *          isRequired: true,
 *          type: 'STRING'
 *        },
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/:id/get-parameters',
  validator.params(DeviceController.validators.getParameterList),
  asyncErrorHandler(DeviceController.getParameterList),
);

/**
 * @api {get} /api/device/get-device-types Получить список типов оборудования
 * @apiVersion 1.0.0
 * @apiName GetDeviceTypes
 * @apiGroup Device
 * @apiSuccessExample {json} Пример ответа:
 *  HTTP/1.1 200 OK
 *  {
 *    data: [
 *      {
 *        id: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *        name: 'ККТ',
 *        alias: 'ККТ',
 *      },
 *      {
 *        id: '14eeadde-dbcb-44ac-9d72-e1cc11617c98',
 *        name: 'Поддержка АРМ',
 *        alias: 'ARM',
 *      }
 *    ],
 *    success: true,
 *    error: 'error message' | null,
 *    errorCode: 'ErrorCode' | null
 *  }
 */
router.get(
  '/get-device-types',
  asyncErrorHandler(DeviceController.getDeviceTypes),
);

export default router;
