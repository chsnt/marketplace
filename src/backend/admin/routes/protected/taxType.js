import { Router } from 'express';

import TaxTypeController from '../../../common/controllers/TaxTypeController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';

const router = new Router();

/**
 * @api {get} /api/tax-type/get-list Список видов налогообложения
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup TaxType
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: '45a863fb-8e42-4dd5-992c-f668d812a26f'
 *          name: 'Упрощенка',
 *          alias: 'SIMPLE'
 *        }
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-list',
  asyncErrorHandler(TaxTypeController.getList),
);

export default router;
