import { Router } from 'express';
import validator from '../../../common/libs/requestValidator';

import TariffController from '../../controllers/TariffController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';

const router = new Router();

/**
 * @api {get} /api/tariff/get-list Список тарифов доступных к заказу
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup Tariff
 * @apiParam (Query) {String} search Поиск
 * @apiParam (Query) {UUID} [deviceTypeId] Идентификатор типа оборудования.
 * @apiDescription В данном списке отображаются номенклатура доступная к приобретению
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: '513587b2-8969-4fdf-8c2c-9390b8085fe8',
 *          name: 'tariff name',
 *          cost: 100500 | null,
 *          costMonth: 20100,
 *          label: 'tariff label',
 *          icon: 'icon name',
 *          expirationMonths: 5,
 *          alias: 'asdas',
 *          deviceType: {
 *            id: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *            name: 'ККТ',
 *            alias: 'KKT'
 *          } | null,
 *        }
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-list',
  validator.query(TariffController.validators.getList),
  asyncErrorHandler(TariffController.getList),
);

/**
 * @api {get} /api/tariff/get-all-list Список всех тарифов
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup Tariff
 * @apiParam (Query) {String} search Поиск
 * @apiDescription В данном списке отображается вся номенклатура
 *                  Если у свойства `cost` значение null, значит тариф недоступен к приобретению.
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: "513587b2-8969-4fdf-8c2c-9390b8085fe8",
 *          name: "tariff name",
 *          cost: 100500 | null,
 *          costMonth: 20100,
 *          label: "tariff label",
 *          icon: "icon name",
 *          expirationMonths: 5,
 *          alias: "asdas"
 *        }
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-all-list',
  validator.query(TariffController.validators.getAllList),
  asyncErrorHandler(TariffController.getAllList),
);

/**
 * @api {get} /api/tariff/get-completed-list Список завершенных тарифов
 * @apiVersion 1.0.0
 * @apiName GetCompletedList
 * @apiGroup Tariff
 * @apiParam (Query) {UUID} clientId Идентификатор клиента
 * @apiSuccess (SuccessResponse) {Object[]} data Массив тарифов
 * @apiSuccess (SuccessResponse) {Object} serviceAddress
 * @apiSuccess (SuccessResponse) {Object} device
 * @apiSuccess (SuccessResponse) {Object} tariff
 * @apiSuccess (SuccessResponse) {Object[]} tariff.services
 * @apiSuccess (SuccessResponse) {UUID} tariff.services.id ID Услуги в рамках тарифа
 * @apiSuccess (SuccessResponse) {Number|null} tariff.services.value Доступное кол-во, если null то кол-во услуг не ограниченно
 * @apiSuccess (SuccessResponse) {Number} tariff.services.progress Кол-во в стадии выполнения
 * @apiSuccess (SuccessResponse) {Number} tariff.services.used Кол-во использованных услуг
 * @apiSuccess (SuccessResponse) {Object} tariff.services.service
 * @apiSuccess (SuccessResponse) {UUID} tariff.services.service.id ID услуги
 * @apiSuccess (SuccessResponse) {String} tariff.services.service.name Название услуги
 * @apiSuccess (SuccessResponse) {String} tariff.services.service.alias URL
 * @apiSuccess (SuccessResponse) {String} tariff.services.service.label
 * @apiSuccess (SuccessResponse) {String} tariff.services.service.icon Иконка
 * @apiSuccess (SuccessResponse) {UUID=inOrder, inTicket, notRequested} tariff.services.service.serviceAddressType типа адреса обслуживания ("При заказе", "При создании ЗНО", "Не запрашивается")
 * @apiSuccess (SuccessResponse) {UUID} tariff.services.service.deviceTypeId ID типа оборудования для этой услуги
 * @apiSuccess (SuccessResponse) {Number|null} tariff.services.service.limit  Доступное кол-во, если null то кол-во услуг не ограниченно
 * @apiSuccess (SuccessResponse) {String} tariff.services.service.limitMeasure В чем измеряется кол-во услуг
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          serviceAddress: {
 *            id: '4ca45504-2b86-42e9-90f8-39c29a5a63d9',
 *            street: 'улица Ленина',
 *            house: '1',
 *            apartment: '123',
 *            postalCode: '555888',
 *            fiasId: null,
 *            city: {
 *              id: 'a02cd33a-b7de-41c6-b191-b41168054bc1',
 *              name: 'Зеленоградск',
 *              utcOffset: 0,
 *            },
 *            region: {
 *              id: '4680dce7-ac13-410c-8b7d-a467845fe7ed',
 *              name: 'Республика Удмуртия',
 *            },
 *          },
 *          device: {
 *            id: 'd82bca88-9359-40ec-b7c0-baaf405ab9d6',
 *            type: 'ККТ',
 *            manufacturer: 'Эвотор',
 *            model: 'Эвотор 7.2',
 *            parameters: [
 *              {
 *                value: '12',
 *                name: 'Гарантия',
 *                alias: 'GARANTIYAM',
 *              },
 *              {
 *                value: 'Да',
 *                name: 'Работа от аккумулятора',
 *                alias: 'RABOTA-OT-AKKUMULYATORA',
 *              },
 *              {
 *                value: 'Нет',
 *                name: 'Сенсорный дисплей',
 *                alias: 'SENSORNYJ-DISPLEJ',
 *              },
 *              {
 *                value: 'zzz888yyy000123',
 *                name: 'Серийный номер',
 *                alias: 'SERIAL_NUMBER',
 *              },
 *            ],
 *          },
 *          tariff: {
 *            id: '2786d693-0b0d-4844-8c8f-f4a30298cf34',
 *            clientTariffId: '90a4c719-142f-4263-8ce4-c3c9f0fa1a3f',
 *            name: 'Все включено на полгода',
 *            alias: 'vse-vklyucheno-6',
 *            label: null,
 *            icon: null,
 *            expirationAt: '2019-09-03T00:00:00.000Z',
 *            services: [
 *              {
 *                id: 'c4e7237e-a801-46cd-b069-466463997f94',
 *                value: null,
 *                progress: 0,
 *                used: 0,
 *                service: {
 *                  id: '85db8d1a-d35d-43c3-91b1-7290be621af6',
 *                  name: 'Неограниченные консультации службы технической поддержки',
 *                  alias: 'konsultaciya-tekhnicheskoj-podderzhki',
 *                  label: null,
 *                  icon: null,
 *                  serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *                  deviceTypeId: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *                  limit: null,
 *                  limitMeasure: 'Количество работ в месяц',
 *                },
 *              },
 *              {
 *                id: '77203ce9-d74b-49e3-835d-d1cc338207d7',
 *                value: 1,
 *                progress: 0,
 *                used: 0,
 *                service: {
 *                  id: '3e1bf921-0236-4202-bf21-3bc031e2b0ff',
 *                  name: 'Услуги гарантийного ремонта контрольно-кассовой техники (ККТ)',
 *                  alias: 'garantijnyj-remont-kkt',
 *                  label: null,
 *                  icon: null,
 *                  serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *                  deviceTypeId: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *                  limit: 1,
 *                  limitMeasure: 'Количество работ в месяц',
 *                },
 *              },
 *              {
 *                id: '25441611-f138-4142-8774-bfa1ac0a7224',
 *                value: 0,
 *                progress: 1,
 *                used: 0,
 *                service: {
 *                  id: 'a4e9ffe6-4190-4c2e-9496-54844829cc0f',
 *                  name: 'Экстренный выезд инженера',
 *                  alias: 'ehkstrennyj-vyezd-inzhenera',
 *                  label: null,
 *                  icon: 'iconEngineer',
 *                  serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *                  deviceTypeId: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *                  limit: 1,
 *                  limitMeasure: 'Количество работ в месяц',
 *                },
 *              },
 *              {
 *                id: '4039c9a5-45e2-4a8a-89a2-f0aeff94f63e',
 *                value: 0,
 *                progress: 1,
 *                used: 0,
 *                service: {
 *                  id: 'd7ad6c6a-6cb7-4bdc-b4fe-9b5a5ba887be',
 *                  name: 'Договор с ОФД на срок действия договора',
 *                  alias: 'dogovor-s-ofd-na-srok-dejstviya-dogovora',
 *                  label: null,
 *                  icon: null,
 *                  serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *                  deviceTypeId: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *                  limit: 1,
 *                  limitMeasure: 'Количество работ в месяц',
 *                },
 *              },
 *              {
 *                id: '12eca732-4157-4c39-8e0a-fdbd1a4b6a75',
 *                value: 2,
 *                progress: 0,
 *                used: 2,
 *                service: {
 *                  id: 'a32142d4-96e1-40aa-bf1a-68c201dbbbda',
 *                  name: 'Замена фискального накопителя',
 *                  alias: 'zamena-fiskalnogo-nakopitelya',
 *                  label: null,
 *                  icon: 'iconFiscalDriveRe',
 *                  serviceAddressType: 'inOrder',  // доступны inOrder, inTicket, notRequested
 *                  deviceTypeId: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
 *                  limit: 2,
 *                  limitMeasure: 'Количество работ в месяц',
 *                },
 *              },
 *            ],
 *          },
 *        },
 *      ],
 *      success: true,
 *      error: 'error message' | null,
 *      errorCode: 'ErrorCode' | null,
 *    }
 */
router.get(
  '/get-completed-list',
  validator.query(TariffController.validators.getCompletedList),
  asyncErrorHandler(TariffController.getCompletedList),
);

/**
 * @api {get} /api/tariff/:id/get-info Данные о тарифе вместе с услугами
 * @apiVersion 1.0.0
 * @apiName GetInfo
 * @apiGroup Tariff
 * @apiParam (Params) {UUID} id Идентификатор тарифа
 * @apiParamExample Пример запроса:
 *    http://host/api/tariff/779b0a45-590e-41ca-9f1f-e4562f97bfc5/get-info
 * @apiSuccessExample {json} Пример ответа:
 *  HTTP/1.1 200 OK
 * {
 *      'data': {
 *        id: '779b0a45-590e-41ca-9f1f-e4562f97bfc5',
 *        name: 'Удаленная помощь на полгода',
 *        cost: [
 *          {
 *            'value': 0,
 *            'startAt': '2020-10-20T15:07:55.698Z',
 *            'expirationAt': null
 *          }
 *        ],
 *        expirationMonths: 6,
 *        description: 'large description'
 *        alias: 'udalennaya-pomoshch-6',
 *        services: [
 *          {
 *            id: '85db8d1a-d35d-43c3-91b1-7290be621af6',
 *            name: 'Консультация технической поддержки',
 *            forTariffOnly: true,
 *            alias: 'konsultaciya-tekhnicheskoj-podderzhki',
 *            description: 'Описание услуги'
 *          },
 *          {
 *            id: '64da2bbc-5519-4b34-baad-3930c48f441f',
 *            name: 'Регистрация ККТ в ФНС',
 *            forTariffOnly: false,
 *            alias: 'registraciya-kkt-v-fns',
 *            description: 'Описание услуги'
 *          },
 *          {
 *            id: '40203727-0ac5-4e14-9240-fff0b9b556dc',
 *            name: 'Удаленное обновление ПО ККТ (без стоимости обновления)',
 *            forTariffOnly: false,
 *            alias: 'udalennoe-obnovlenie-po-kkt-bez-stoimosti-obnovleniya',
 *            description: 'Описание услуги'
 *          },
 *        ],
 *      },
 *      'success': true,
 *      'error': 'error message' | null,
 *      'errorCode': 'ErrorCode' | null
 * }
 */
router.get(
  '/:id/get-info',
  validator.params(TariffController.validators.getTariff),
  asyncErrorHandler(TariffController.getTariff),
);

/**
 * @api {post} /api/tariff/update Обновить данные тарифа
 * @apiVersion 1.0.0
 * @apiName Update
 * @apiGroup Tariff
 * @apiParam (Body) {UUID} id Идентификатор тарифа
 * @apiParam (Body) {UUID} [availableCatalogStatusId] ID статуса доступа
 * @apiParam (Body) {UUID} [externalId] ID в системе интегратора
 * @apiParam (Body) {String{1..255}} [name] Название тарифа
 * @apiParam (Body) {String{1..255}} [label] Лейбл тарифа
 * @apiParam (Body) {String{1..255}} [icon] Название файла иконки
 * @apiParam (Body) {String{1..255}} [alias] URL тарифа
 * @apiParam (Body) {String{1..255}} [description] Описание
 * @apiParam (Body) {Number} [expirationMonths] Кол-во месяцев в течении которых действует тариф с момента покупки
 * @apiParam (Body) {Array} [services] Список услуг. Если услуги нет, то она удаляется из списка
 * @apiParam (Body) {UUID} [services.serviceId] ID услуги, которую нужно добавить в тариф
 * @apiParam (Body) {Number} [services.limit] Кол-во услуг в рамках тарифа
 * @apiParam (Body) {UUID} [services.limitMeasureId='91aa207f-ad87-4332-a35f-063ed9cb7bc4'] В чем считается кол-во услуг (по умолчанию "Количество работ в месяц")
 * @apiSuccessExample {json} Пример ответа:
 *  HTTP/1.1 200 OK
 * {
 *      "data": null,
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 * }
 */
router.post(
  '/update',
  validator.body(TariffController.validators.updateTariff),
  asyncErrorHandler(TariffController.updateTariff),
);

export default router;
