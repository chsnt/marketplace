import { Router } from 'express';

import MenuController from '../../controllers/MenuController';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';

const router = new Router();

/**
 * @api {get} /api/menu/get-list Справочник меню
 * @apiVersion 1.0.0
 * @apiName GetList
 * @apiGroup Menu
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      data: [
 *        {
 *          id: 'example',
 *          name: 'Пример описания пункта меню',
 *          actionPermissions: {
 *            create: false,
 *            getList: true,
 *            getInfo: false,
 *            update: false,
 *          },
 *          pageComponent: 'default',
 *          isCustomPage: false,
 *          defaultIcon: 'icon',
 *          activeIcon: 'iconActive',
 *          children: [],
 *        },
 *      ],
 *      success: true,
 *      error: null,
 *      errorCode: null,
 *    }
 */
router.get('/get-list', asyncErrorHandler(MenuController.getList));

export default router;
