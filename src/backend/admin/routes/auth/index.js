import { Router } from 'express';
import asyncErrorHandler from '../../../common/libs/asyncErrorHandler';

import AuthController from '../../controllers/AuthController';
import rateLimiter from '../../../common/libs/rateLimiter';
import { MAX_ATTEMPTS } from '../../../client/constants/env';

const router = new Router();

/**
 * @api {post} /api/auth/login Аутентификация пользователя
 * @apiVersion 1.0.0
 * @apiName Login
 * @apiGroup Auth
 * @apiParam (Body) {Object} request
 * @apiParam (Body) {String} request.emailOrPhone Email или телефон пользователя
 * @apiParam (Body) {String} request.password Пароль
 * @apiParam (Body) {Boolean} request.shouldSave Запомнить пароль на неделю в противном случае на день
 * @apiParamExample {json} Body
 *    {
 *      "emailOrPhone": "foo@yandex.ru",
 *      "password": "myaw3somep@ssworD",
 *    }
 * @apiSuccess (Success 200) {Object} request
 * @apiSuccess (Success 200) {UUID} request.id Идентификатор созданного пользователя
 * @apiSuccess (Success 200) {String} request.email Email
 * @apiSuccess (Success 200) {String} request.phone Телефон
 * @apiSuccess (Success 200) {String} request.isLegal Флаг, обозначающий, что пользователь - юр. лицо
 * @apiSuccessExample {json} Пример ответа:
 *  HTTP/1.1 200 OK
 *    {
 *      "data": {
 *        "id": "7b5fcf18-88ee-4820-bc4b-8a1e36d7294a",
 *        "email": "foo@yandex.ru",
 *        "phone": "79250347824",
 *        "isLegal": true
 *      },
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 *    }
 */
router.post('/login',
  rateLimiter({ max: MAX_ATTEMPTS.LOGIN, skipSuccessfulRequests: true }),
  asyncErrorHandler(AuthController.localLogin),
  (req, res) => res.json(req.user.adminProfile));

/**
 * @api {get} /api/auth/logout Выйти из системы
 * @apiVersion 1.0.0
 * @apiName Logout
 * @apiGroup Auth
 * @apiSuccessExample {json} Пример ответа:
 *   HTTP/1.1 200 OK
 *    {
 *      "data": null
 *      "success": true,
 *      "error": "error message" | null,
 *      "errorCode": "ErrorCode" | null
 *    }
 */
router.get('/logout', (req, res) => {
  req.logout();
  req.session = null;
  res.json();
});

export default router;
