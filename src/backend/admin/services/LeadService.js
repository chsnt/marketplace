import Sequelize, { Op } from 'sequelize';
import ERRORS from '../constants/errors';
import {
  LeadConsultationResultModel,
  LeadModel,
  LeadNeedModel,
  LeadSourceModel,
  LeadStatusModel,
  LeadTariffRelationModel,
  LeadServiceRelationModel,
} from '../../common/database/models/Lead';
import APIError from '../../common/utils/APIError';
import LeadServiceCommon from '../../common/services/LeadService';
import { dateRangeForQuery } from '../../common/utils';
import db from '../../common/database';
import { GET_LIST_DEFAULT_LIMIT, GET_LIST_DEFAULT_SORT } from '../../common/constants';
import is from '../../common/libs/is';

export default class LeadService extends LeadServiceCommon {
  static async getList({
    number,
    leadNeedId,
    clientId,
    phone,
    email,
    cityId,
    deviceManufacturerId,
    deviceModelId,
    leadStatusId,
    leadConsultationResultId,
    responsibleId,
    dateFrom,
    dateTo,
    representative,
    nextCallDateFrom,
    nextCallDateTo,
    client = 'all',
    offset = 0,
    limit = GET_LIST_DEFAULT_LIMIT,
    sort,
    deviceType,
    responsible,
    clientName,
  } = {}) {
    const { sort: sortBy = 'date', direction = GET_LIST_DEFAULT_SORT } = sort || {};

    const concatOrganizationUserFullName = Sequelize.fn(
      'concat',
      Sequelize.col(`${LeadModel.name}.first_name`),
      Sequelize.col(`${LeadModel.name}.patronymic_name`),
      Sequelize.col(`${LeadModel.name}.last_name`),
    );

    const concatIndividualFullName = Sequelize.fn(
      'concat',
      Sequelize.col('client.users.first_name'),
      Sequelize.col('client.users.patronymic_name'),
      Sequelize.col('client.users.last_name'),
    );

    const searchAttribute = Sequelize.where(
      concatOrganizationUserFullName,
      { [Sequelize.Op.iLike]: `%${representative}%` },
    );

    const allowListClient = new Map([
      ['all', {}],
      ['empty', { clientId: null }],
      ['individual', {
        [Sequelize.Op.and]: {
          clientId: { [Sequelize.Op.not]: null },
          isOrganization: { [Sequelize.Op.not]: true },
        },
      }],
      ['organization', {
        [Sequelize.Op.and]: {
          clientId: { [Sequelize.Op.not]: null },
          isOrganization: true,
        },
      }],
    ]);

    const orderBy = new Map([
      ['representative', [concatOrganizationUserFullName]],
      ['date', ['createdAt']],
      ['number', ['number']],
      ['phone', ['phone']],
      ['email', ['email']],
      ['city', ['city', 'name']],
      ['nextCallDate', ['nextCallDate']],
      ['responsible', [Sequelize.fn(
        'concat',
        Sequelize.col('updatedBy.first_name'),
        Sequelize.col('updatedBy.patronymic_name'),
        Sequelize.col('updatedBy.last_name'),
      )]],
      ['client',
        (is.client.type.organization(client)) ? ['client', 'organization', 'name']
          : [concatIndividualFullName],
      ],
    ]);

    const result = await LeadModel.findAndCountAll({
      offset,
      limit,
      subQuery: false,
      where: {
        ...allowListClient.get(client),
        ...(number && { number }),
        ...(leadNeedId && { leadNeedId }),
        ...(clientId && { clientId }),
        ...(phone && { phone }),
        ...(email && { email }),
        ...(cityId !== undefined && { cityId }),
        ...(deviceManufacturerId !== undefined && { deviceManufacturerId }),
        ...(deviceModelId !== undefined && { deviceModelId }),
        ...(leadStatusId && { leadStatusId }),
        ...(leadConsultationResultId && { leadConsultationResultId }),
        ...(responsibleId && { '$updatedBy.id$': responsibleId }),
        ...dateRangeForQuery({ dateFrom, dateTo }),
        ...(representative && { $and: searchAttribute }),
        ...dateRangeForQuery({
          dateFrom: nextCallDateFrom,
          dateTo: nextCallDateTo,
          column: 'nextCallDate',
          isFullDay: false,
        }),
        ...(responsible && {
          updated_by: responsible,
        }),
        ...(is.client.type.organization(client) && clientName && {
          '$client.organization.name$': { [Op.iLike]: `%${clientName}%` },
        }),
        ...(is.client.type.individual(client) && clientName && {
          '$client.users': Sequelize.where(
            concatIndividualFullName,
            { [Op.iLike]: `%${clientName}%` },
          ),
        }),
      },
      order: [[...orderBy.get(sortBy), direction]],
      include: [
        {
          association: 'leadNeed',
          attributes: ['id', 'name'],
        },
        {
          association: 'client',
          attributes: ['id'],
          include: [
            {
              association: 'organization',
              attributes: ['id', 'name'],
            },
            {
              association: 'users',
              attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'fullName'],
            },
          ],
        },
        {
          association: 'deviceType',
          attributes: ['id', 'name'],
          ...(deviceType && {
            where: {
              id: deviceType,

            },
          }),
        },
        {
          association: 'city',
          attributes: ['id', 'name'],
        },
        {
          association: 'deviceManufacturer',
          attributes: ['id', 'name'],
        },
        {
          association: 'deviceModel',
          attributes: ['id', 'name'],
        },
        {
          association: 'leadStatus',
          attributes: ['id', 'name'],
        },
        {
          association: 'leadConsultationResult',
          attributes: ['id', 'name'],
        },
        {
          association: 'updatedBy',
          attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'fullName'],
        },
      ],
      attributes: [
        'id', 'number', 'phone', 'createdAt', 'firstName', 'patronymicName', 'lastName', 'fullName',
        'nextCallDate', 'isOrganization', 'email',
      ],
    });

    return {
      ...result,
      rows: result.rows.map((leadInstance) => {
        const {
          leadNeed, city, deviceManufacturer, deviceType: deviceTypeData,
          deviceModel, leadStatus, leadConsultationResult, updatedBy, ...lead
        } = leadInstance.toJSON();

        return {
          ...lead,
          firstName: undefined,
          patronymicName: undefined,
          lastName: undefined,
          deviceType: deviceTypeData?.name || null,
          leadNeed: leadNeed?.name || null,
          client: leadInstance.client?.name || null,
          city: city?.name || null,
          deviceManufacturer: deviceManufacturer?.name || null,
          deviceModel: deviceModel?.name || null,
          leadStatus: leadStatus?.name || null,
          leadConsultationResult: leadConsultationResult?.name || null,
          responsible: updatedBy?.fullName || null,
        };
      }),
    };
  }

  static async getInfo(id) {
    const leadInstance = await LeadModel.findByPk(id, {
      include: [
        {
          association: 'leadNeed',
          attributes: ['id', 'name'],
        },
        {
          association: 'client',
          attributes: ['id', 'number'],
          include: [
            {
              association: 'organization',
              attributes: ['id', 'name', 'inn'],
              include: [{
                association: 'users',
                attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'fullName'],
              }],
            },
            {
              association: 'users',
              attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'fullName'],
            },
          ],
        },
        {
          association: 'city',
          attributes: ['id', 'name', 'utcOffset'],
        },
        {
          association: 'deviceManufacturer',
          attributes: ['id', 'name'],
        },
        {
          association: 'deviceModel',
          attributes: ['id', 'name'],
        },
        {
          association: 'deviceType',
          attributes: ['id', 'name', 'alias'],
        },
        {
          association: 'leadStatus',
          attributes: ['id', 'name', 'alias'],
        },
        {
          association: 'leadSource',
          attributes: ['id', 'name', 'alias'],
        },
        {
          association: 'leadConsultationResult',
          attributes: ['id', 'name', 'alias'],
        },
        {
          association: 'updatedBy',
          attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'fullName'],
        },
        {
          association: 'tariffs',
          through: { attributes: [] },
          attributes: ['id', 'name'],
        },
        {
          association: 'services',
          through: { attributes: [] },
          attributes: ['id', 'name'],
        },
      ],
      attributes: [
        'id', 'number', 'phone', 'email', 'createdAt', 'firstName', 'patronymicName', 'lastName',
        'comment', 'formId', 'nextCallDate', 'isOrganization', 'operationSystem',
      ],
    });

    if (!leadInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(id));

    const { updatedBy: responsible, client, ...data } = leadInstance.toJSON();

    return {
      ...data,
      client: client ? {
        ...client,
        users: undefined,
        organization: undefined,
        phone: client.maintainer?.phone || null,
        ...(client.organization ? {
          inn: client.organization.inn,
          name: leadInstance.client.name,
        } : {
          firstName: leadInstance.client.maintainer.firstName,
          patronymicName: leadInstance.client.maintainer.patronymicName,
          lastName: leadInstance.client.maintainer.lastName,
        }),
      } : null,
      form: leadInstance.form,
      formId: undefined,
      responsible,
    };
  }

  static getStatuses() {
    return LeadStatusModel.findAll({
      order: [['priority', 'asc']],
      attributes: ['id', 'name', 'alias'],
    });
  }

  static getSources() {
    return LeadSourceModel.findAll({
      order: [['order', 'asc']],
      attributes: ['id', 'name', 'alias'],
    });
  }

  static getConsultationResults() {
    return LeadConsultationResultModel.findAll({ attributes: ['id', 'name', 'alias'] });
  }

  static getNeeds() {
    return LeadNeedModel.findAll({ attributes: ['id', 'name', 'alias'] });
  }

  static async updateLead({
    id, tariffs = [], services = [], ...lead
  }) {
    const transaction = await db.sequelize.transaction();
    try {
      const leadInstance = await LeadModel.findByPk(id, {
        transaction,
        include: [
          {
            association: 'tariffs',
            attributes: ['id'],
            through: { attributes: ['id'] },
          },
          {
            association: 'services',
            attributes: ['id'],
            through: { attributes: ['id'] },
          },
        ],
      });

      if (!leadInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(id));

      /* productKey важно указать с заглавной буквы */
      const resolveDivergence = async ({
        currentRows = [], incomingIds = [], productKey, model,
      }) => {
        const incomingIdsSet = new Set(incomingIds);
        /* По-умолчанию - создаем все пришедшие записи */
        const idsToCreate = new Set(incomingIds);
        /* Формируем ключ получения записи связующей таблицы */
        const throughModelKey = `lead${productKey}Relation`;
        /* Формируем строки на удаление */
        const rowsToDelete = currentRows.reduce((acc, row) => {
          const rowId = row.id;
          /* Помечаем на удаление id записи связи лида и продукта */
          if (!incomingIdsSet.has(rowId)) {
            acc.push(row[throughModelKey].id);
            return acc;
          }
          /* Удаляем id из массива на создание */
          if (idsToCreate.has(rowId)) {
            idsToCreate.delete(rowId);
          }
          return acc;
        }, []);

        /* Если есть что создавать - создаем */
        if (idsToCreate.size) {
          await model.bulkCreate(idsToCreate.values((productId) => ({
            [`${productKey.toLowerCase()}Id`]: productId, leadId: leadInstance.id,
          })), { transaction });
        }
        /* Если есть что удалять - удаляем */
        if (rowsToDelete.length) {
          await model.destroy({ where: { id: rowsToDelete }, transaction });
        }
      };

      /* Вычисляем расхождения пришедших тарифов и услуг с существующими */
      await resolveDivergence({
        currentRows: leadInstance.tariffs,
        incomingIds: tariffs,
        model: LeadTariffRelationModel,
        productKey: 'Tariff',
      });

      await resolveDivergence({
        currentRows: leadInstance.services,
        incomingIds: services,
        model: LeadServiceRelationModel,
        productKey: 'Service',
      });

      leadInstance.set(lead);
      await leadInstance.save({ transaction });

      await transaction.commit();

      return leadInstance;
    } catch (error) {
      await transaction.rollback();
      if (!(error instanceof APIError)) throw new APIError(ERRORS.LEAD.UPDATE_ERROR, { originalError: error });
      throw error;
    }
  }
}
