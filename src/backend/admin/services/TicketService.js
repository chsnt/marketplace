import path from 'path';
import { Op } from 'sequelize';

import {
  TicketModel,
  TicketStatusModel,
  TicketDocumentModel,
  TicketClientServiceRelationModel,
  TicketClientTariffServiceRelationModel,
} from '../../common/database/models/Ticket';
import TicketServiceCommon from '../../common/services/TicketService';
import { GET_LIST_DEFAULT_LIMIT, GET_LIST_DEFAULT_SORT } from '../../common/constants';
import APIError from '../../common/utils/APIError';
import ERRORS from '../constants/errors';
import db from '../../common/database';
import FileService from '../../common/services/FileService';
import { dateRangeForQuery } from '../../common/utils';
import { ClientServiceModel } from '../../common/database/models/Client';
import ServiceService from '../../common/services/ServiceService';
import { UserModel } from '../../common/database/models/User';
import { getAddressAndDevice } from '../../common/utils/prepareResponse';
import AddressModel from '../../common/database/models/AddressModel';

export default class TicketService extends TicketServiceCommon {
  static async getList({
    offset = 0,
    limit = GET_LIST_DEFAULT_LIMIT,
    serial,
    statusId,
    clientId,
    services,
    responsibleId,
    clientName,
    serviceDeskId,
    dateFrom,
    dateTo,
    inn,
    sortByDate = GET_LIST_DEFAULT_SORT,
    nextCallDateFrom,
    nextCallDateTo,
    client = 'all',
    deviceTypeId,
  } = {}) {
    const searchAttribute = UserModel.getSearchAttribute({ value: clientName, preKey: 'users' });

    const ticketInstances = await TicketModel.findAndCountAll({
      limit,
      offset,
      subQuery: false,
      distinct: true,
      where: {
        ...(statusId && { ticketStatusId: statusId }),
        ...(serial && {
          number: serial.replace(/[^\d;]/g, ''),
        }),
        ...dateRangeForQuery({ dateFrom, dateTo }),
        ...(serviceDeskId && {
          serviceDeskId: { [Op.iLike]: `%${serviceDeskId}%` },
        }),
        ...(responsibleId !== undefined && { responsibleId }),
        ...dateRangeForQuery({
          dateFrom: nextCallDateFrom,
          dateTo: nextCallDateTo,
          column: 'nextCallDate',
          isFullDay: false,
        }),
        ...(deviceTypeId && {
          [Op.or]: [
            { '$clientServices.service.device_type_id$': deviceTypeId },
            { '$clientTariffServices.tariffServiceRelation.service.device_type_id$': deviceTypeId },
          ],
        }),
        ...(clientId && { clientId }),
      },
      attributes: ['id', 'number', 'serviceDeskId', 'createdAt', 'nextCallDate', 'laborCosts'],
      order: [['createdAt', sortByDate]],
      include: [
        {
          association: 'client',
          where: {
            ...(clientName && {
              [Op.or]: [
                { '$organization.name$': { [Op.iLike]: `%${clientName}%` } },
                { $and: searchAttribute },
              ],
            }),
            ...(inn && { inn }),
          },
          include: [
            {
              association: 'organization',
              ...((client === 'organization') ? { required: true } : { }),
              attributes: ['id', 'name', 'inn'],
            },
            {
              association: 'users',
              ...((client === 'individual') ? { required: true } : { }),
              attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'fullName'],
            },
          ],
        },
        {
          association: 'ticketStatus',
          attributes: ['id', 'name', 'alias'],
        },
        {
          association: 'clientServices',
          attributes: ['id', 'service_id', 'clientId'],
          include: [
            {
              association: 'service',
              attributes: ['id', 'name', 'alias', 'deviceTypeId'],
            },
          ],
        },
        {
          association: 'clientTariffServices',
          attributes: ['id', 'value'],
          include: [
            {
              association: 'clientTariff',
              attributes: ['clientId'],
            },
            {
              association: 'tariffServiceRelation',
              paranoid: false,
              attributes: ['id'],
              include: [
                {
                  association: 'service',
                  paranoid: false,
                  attributes: ['id', 'name', 'deviceTypeId'],
                },
              ],
            },
          ],
        },
        AddressModel.addressAndDevice({ paranoidAddress: true, separateClientDeviceParameters: false }),
        {
          association: 'responsible',
          attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'fullName'],
        },
      ],
    });

    return {
      ...ticketInstances,
      rows: ticketInstances.rows.map((ticketInstance) => {
        const {
          responsible,
          clientServices,
          clientTariffServices,
          ticketStatus,
          number,
          client: clientData,
          serviceAddress,
          ...ticket
        } = ticketInstance.toJSON();

        const servicesInTicket = ServiceService.getServicesForTicket({
          clientServices, clientTariffServices,
        });

        /* Фильтруем ЗНО по услугам здесь, потому что в рамках запроса это сделать сложнее */
        if (services && services?.length > 0) {
          const successCount = servicesInTicket.reduce((acc, { id }) => {
            if (services.includes(id)) return acc + 1;
            return acc;
          }, 0);
          if (successCount < services.length) return null;
        }

        const {
          serviceAddress: preparedServiceAddress = null,
          device = null,
        } = getAddressAndDevice(serviceAddress) || {};

        const alias = this.getTicketNumberPrefix(device?.alias);

        const { responsibleResponseId = null, responsibleFullName = null } = responsible || {};

        return {
          ...ticket,
          serial: `${alias}${number}`,
          client: {
            id: clientData.id || null,
            name: clientData.name || null,
          },
          inn: clientData.organization?.inn || null,
          services: servicesInTicket,
          status: ticketStatus.name,
          responsible: {
            id: responsibleResponseId,
            fullName: responsibleFullName,
          },
          serviceAddress: preparedServiceAddress,
          device,
        };
      }).filter(Boolean),
    };
  }

  static async getInfo(ticketId) {
    const ticketInstance = await TicketModel.findByPk(ticketId, {
      attributes: [
        'id', 'number', 'solution', 'description',
        'comment', 'serviceDeskId', 'createdAt', 'nextCallDate', 'laborCosts',
      ],
      include: [
        {
          association: 'ticketStatus',
          attributes: ['id', 'name', 'alias'],
        },
        {
          association: 'responsible',
          attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'fullName'],
        },
        AddressModel.addressAndDevice({ separateClientDeviceParameters: true }),
        {
          association: 'clientServices',
          attributes: ['id'],
          include: [
            {
              association: 'service',
              attributes: ['id', 'name'],
            },
          ],
        },
        {
          association: 'clientTariffServices',
          attributes: ['id'],
          include: [
            {
              association: 'tariffServiceRelation',
              paranoid: false,
              attributes: ['id'],
              include: [
                {
                  association: 'service',
                  paranoid: false,
                  attributes: ['id', 'name'],
                },
              ],
            },
          ],
        },
        {
          association: 'ticketDocuments',
          attributes: ['id', 'name'],
        },
        {
          association: 'ticketCloseDocuments',
          attributes: ['id', 'name'],
        },
        {
          association: 'client',
          include: [
            {
              association: 'organization',
              attributes: ['id', 'name', 'kpp', 'inn'],
              include: [
                {
                  association: 'users',
                  attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'email', 'phone', 'isMaintainer'],
                  separate: true,
                },
              ],
            },
            {
              association: 'users',
              attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'email', 'phone'],
            },
          ],
        },
      ],
    });

    if (!ticketInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(ticketId));

    const {
      responsible,
      clientServices,
      clientTariffServices,
      ticketDocuments,
      ticketCLoseDocuments,
      number,
      serviceAddress,
      client,
      ...ticket
    } = ticketInstance.toJSON();

    /* Формируем массив услуг, входящих в ЗНО */
    const services = ServiceService.getServicesForTicket({
      clientServices,
      clientTariffServices,
    });

    const contactPerson = client.maintainer?.toJSON();

    const {
      serviceAddress: preparedServiceAddress = null,
      device = null,
    } = getAddressAndDevice(serviceAddress) || {};

    const alias = this.getTicketNumberPrefix(device?.alias);

    const { responsibleId = null, responsibleFullName = null } = responsible || {};

    return {
      ...ticket,
      serial: `${alias}${number}`,
      number,
      responsible: {
        id: responsibleId,
        fullName: responsibleFullName,
      },
      client: {
        id: client.id || null,
        name: client.name || null,
        organizations: {
          inn: client.organizations.inn || null,
        },
      },
      contactPerson: {
        ...contactPerson,
        fullName: undefined,
      },
      services,
      serviceAddress: preparedServiceAddress,
      device,
      documents: [
        ...(ticketDocuments || []),
        ...(ticketCLoseDocuments || []),
      ],
    };
  }

  static getStatuses() {
    return TicketStatusModel.findAll({
      attributes: ['id', 'name', 'alias'],
      order: [['order', 'ASC']],
    });
  }

  static async removeTicketServiceRelations({
    servicesToDelete,
    tariffServicesToDelete,
    transaction,
  }) {
    await TicketClientServiceRelationModel.destroy({ where: { id: servicesToDelete }, transaction });
    await TicketClientTariffServiceRelationModel.destroy({ where: { id: tariffServicesToDelete }, transaction });
  }

  static async updateTicket({
    ticketId: id,
    docNames: ticketAttachments,
    statusId: ticketStatusId,
    services = [],
    ...data
  }) {
    const transaction = await db.sequelize.transaction();

    try {
      /* Проверяем услуги на уникальность */
      this.checkServicesUnique(services);

      const ticketInstance = await TicketModel.findByPk(id, {
        attributes: ['id', 'clientId', 'serviceAddressId', 'number', 'ticketStatusId', 'createdAt'],
        include: [
          {
            association: 'ticketStatus',
            attributes: ['id', 'name'],
          },
          {
            association: 'clientServices',
            include: {
              association: 'service',
              attributes: { exclude: ['service_id'] },
            },
          },
          {
            association: 'clientTariffServices',
            include: {
              association: 'tariffServiceRelation',
              include: {
                association: 'service',
                attributes: { exclude: ['service_id'] },
              },
            },
          },
        ],
        transaction,
      });

      if (!ticketInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(id));

      /* Обновляем поля ЗНО */
      ticketInstance.set({
        ...data,
        ticketStatusId,
        ...(ticketStatusId === TicketStatusModel.STATUSES.COMPLETED
          ? { resolvedAt: new Date() }
          : { resolvedAt: null }),
      });

      /* Обновление списка услуг в ЗНО */
      if (services.length) {
        /* Формируем массив услуг для создания */
        const servicesToCreate = [...services];
        /* Формируем списки удаления услуг из ЗНО */
        /* Услуги тарифа */
        const tariffServicesToDelete = ticketInstance.clientTariffServices.reduce(
          (acc, clientTariffService) => {
            const serviceId = clientTariffService.tariffServiceRelation.service.id;
            /* Если услуга была и не прилетела с фронта - удаляем ее из ЗНО */
            if (!services.includes(serviceId)) {
              acc.push(clientTariffService.ticketClientTariffServiceRelation.id);
              return acc;
            }
            /* Если такая услуга уже есть, то удаляем из списка на создание */
            if (servicesToCreate.includes(serviceId)) {
              servicesToCreate.splice(servicesToCreate.indexOf(serviceId), 1);
            }
            return acc;
          }, [],
        );
        /* Разовые услуги */
        const servicesToDelete = ticketInstance.clientServices.reduce((acc, clientService) => {
          const serviceId = clientService.service.id;
          /* Если услуга была и не прилетела с фронта - удаляем ее из ЗНО */
          if (!services.includes(serviceId)) {
            acc.push(clientService.ticketClientServiceRelation.id);
            return acc;
          }
          /* Если такая услуга уже есть, то удаляем из списка на создание */
          if (servicesToCreate.includes(serviceId)) {
            servicesToCreate.splice(servicesToCreate.indexOf(serviceId), 1);
          }
          return acc;
        }, []);

        /* Не позволяем обновлять услуги в ЗНО при определенных ее статусах */
        // TODO: Сейчас можно перевести из любого статуса в любой и это потенциальная дыра, пока нет карты перехода между статусами
        if (
          (servicesToDelete.length || tariffServicesToDelete.length || servicesToCreate.length)
          && !TicketStatusModel
            .ALLOW_UPDATE_SERVICES_IN_TICKET_STATUSES.includes(ticketInstance.previous('ticketStatusId'))
        ) throw new APIError(ERRORS.TICKET.CANT_UPDATE_SERVICES);

        /* Если есть что создавать */
        if (servicesToCreate.length) {
          /* Получаем только те услуги, которые нужно создать */
          const { tariffServiceRelationsMap, oneTimeServicesMap } = await this.getServicesForCreating({
            clientId: ticketInstance.clientId,
            services: servicesToCreate,
            transaction,
          });

          /* Проверим есть ли адрес у ЗНО и нужно ли его создать */
          const shouldTicketHasAddress = this.checkShouldTicketHasAddress([
            ...Object.values(tariffServiceRelationsMap).map((item) => item.tariffServiceRelation.service),
            ...Object.values(oneTimeServicesMap).map((item) => item.service),
          ]);

          if (shouldTicketHasAddress && !ticketInstance.serviceAddressId) {
            throw new APIError(ERRORS.TICKET.TICKET_MUST_HAS_SERVICE_ADDRESS);
          }

          /* Выполняем запрос на создание услуг из ЗНО */
          await this.createTicketServiceRelations({
            ticketId: ticketInstance.id,
            oneTimeServices: Object.keys(oneTimeServicesMap),
            tariffServiceRelations: Object.keys(tariffServiceRelationsMap),
            transaction,
          });
        }

        /* Выполняем запрос на удаление услуг из ЗНО */
        if (servicesToDelete.length || tariffServicesToDelete.length) {
          await this.removeTicketServiceRelations({
            servicesToDelete,
            tariffServicesToDelete,
            transaction,
          });
        }
      }
      /* Обновляем ЗНО */
      await ticketInstance.save({ transaction });

      /* После создания и удаления записей услуг обновляем экземпляр ЗНО */
      await ticketInstance.reload({ transaction });

      /* Процедура закрытия ЗНО */
      if (TicketStatusModel.STATUSES.COMPLETED === ticketStatusId) {
        if (ticketInstance.clientServices.length) {
          /* Помечаем услуги ЗНО как завершенные */
          await ClientServiceModel.update({ completed: new Date() }, {
            where: { id: ticketInstance.clientServices.map((instance) => instance.id) },
            transaction,
          });
        }

        if (ticketInstance.clientTariffServices.length) {
          /* Декрементируем каждую использованную услугу тарифа клиента */
          for (const clientTariffService of ticketInstance.clientTariffServices) {
            clientTariffService.value -= 1;
            await clientTariffService.save({ transaction });
          }
        }
      }

      /* Добавляем вложения к ЗНО */
      if (ticketAttachments && ticketAttachments.length > 0) {
        const attachments = await TicketDocumentModel.bulkCreate(ticketAttachments.map(({ name }) => ({
          ticketId: ticketInstance.id,
          name,
        })));

        attachments.forEach((fileAttachment) => {
          FileService.renameFile({
            dir: path.join(FileService.DEFAULT_FILE_PATH, 'ticket'),
            oldName: ticketAttachments.find((doc) => doc.name === fileAttachment.name).fsName,
            newName: fileAttachment.id,
          });
        });
      }

      await transaction.commit();

      return ticketInstance;
    } catch (error) {
      await transaction.rollback();
      if (!(error instanceof APIError)) {
        throw new APIError(ERRORS.TICKET.UPDATE_TICKET, { originalError: error });
      }
      throw error;
    }
  }
}
