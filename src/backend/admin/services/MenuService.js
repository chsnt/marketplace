import httpContext from 'express-http-context';
import RoleService from '../../common/services/RoleService';
import { UserModel } from '../../common/database/models/User';
import is from '../../common/libs/is';

const getFlattenItems = (items = []) => items.reduce((result, item) => {
  result.push(item);
  const rest = getFlattenItems(item.children);
  result.push(...rest);
  return result;
}, []).filter(({ children }) => !children);

const MENU_ACTIONS = {
  MAIN: 'main',
  GET_LIST: 'getList',
  CREATE: 'create',
  GET_INFO: 'getInfo',
  UPDATE: 'update',
};

export default class MenuService {
  static items = [
    {
      id: 'lead',
      name: 'Лиды',
      defaultIcon: 'icon',
      activeIcon: 'iconActive',
      children: [
        {
          id: 'lead-create',
          name: 'Создать лид',
          action: MENU_ACTIONS.CREATE,
        },
        {
          id: 'lead-list',
          name: 'Список Лидов',
          action: MENU_ACTIONS.GET_LIST,
        },
      ],
      permissions: () => MenuService.checkAllActions(Object.values(MENU_ACTIONS), {
        [MENU_ACTIONS.GET_LIST]: {
          groups: [
            RoleService.USER_GROUPS.OPERATOR,
            RoleService.USER_GROUPS.SALES_MANAGER,
            RoleService.USER_GROUPS.TECHNICAL_SUPPORT,
          ],
        },
        [MENU_ACTIONS.CREATE]: {
          groups: [
            RoleService.USER_GROUPS.OPERATOR,
            RoleService.USER_GROUPS.SALES_MANAGER,
          ],
        },
        [MENU_ACTIONS.GET_INFO]: {
          groups: [
            RoleService.USER_GROUPS.TECHNICAL_SUPPORT,
            RoleService.USER_GROUPS.SALES_MANAGER,
          ],
        },
        [MENU_ACTIONS.UPDATE]: {
          groups: [
            RoleService.USER_GROUPS.TECHNICAL_SUPPORT,
            RoleService.USER_GROUPS.SALES_MANAGER,
          ],
        },
      }),
    },
    {
      id: 'client',
      name: 'Клиенты',
      defaultIcon: 'icon',
      activeIcon: 'iconActive',
      children: [
        {
          id: 'client-create',
          name: 'Создать клиента',
          action: MENU_ACTIONS.CREATE,
        },
        {
          id: 'client-list',
          name: 'Список Клиентов',
          action: MENU_ACTIONS.GET_LIST,
        },
      ],
      permissions: () => MenuService.checkAllActions(Object.values(MENU_ACTIONS), {
        [MENU_ACTIONS.GET_LIST]: {
          groups: [
            RoleService.USER_GROUPS.OPERATOR,
            RoleService.USER_GROUPS.SALES_MANAGER,
            RoleService.USER_GROUPS.TECHNICAL_SUPPORT,
            RoleService.USER_GROUPS.PROJECT_COMMITTEE,
          ],
        },
        [MENU_ACTIONS.CREATE]: {
          groups: [
            RoleService.USER_GROUPS.SALES_MANAGER,
          ],
        },
        [MENU_ACTIONS.GET_INFO]: {
          groups: [
            RoleService.USER_GROUPS.OPERATOR,
            RoleService.USER_GROUPS.SALES_MANAGER,
            RoleService.USER_GROUPS.TECHNICAL_SUPPORT,
            RoleService.USER_GROUPS.PROJECT_COMMITTEE,
          ],
        },
        [MENU_ACTIONS.UPDATE]: {
          groups: [
            RoleService.USER_GROUPS.SALES_MANAGER,
          ],
        },
      }),
    },
    {
      id: 'order',
      name: 'Заказы',
      defaultIcon: 'icon',
      activeIcon: 'iconActive',
      children: [
        {
          id: 'order-create',
          name: 'Создать заказ',
          action: MENU_ACTIONS.CREATE,
        },
        {
          id: 'order-list',
          name: 'Список Заказов',
          action: MENU_ACTIONS.GET_LIST,
        },
      ],
      permissions: () => MenuService.checkAllActions(Object.values(MENU_ACTIONS), {
        [MENU_ACTIONS.GET_LIST]: {
          groups: [
            RoleService.USER_GROUPS.SALES_MANAGER,
            RoleService.USER_GROUPS.PROJECT_COMMITTEE,
          ],
        },
        [MENU_ACTIONS.CREATE]: {
          groups: [
            RoleService.USER_GROUPS.SALES_MANAGER,
          ],
        },
        [MENU_ACTIONS.GET_INFO]: {
          groups: [
            RoleService.USER_GROUPS.SALES_MANAGER,
            RoleService.USER_GROUPS.PROJECT_COMMITTEE,
          ],
        },
      }),
    },
    {
      id: 'ticket',
      name: 'ЗНО',
      defaultIcon: 'icon',
      activeIcon: 'iconActive',
      children: [
        {
          id: 'ticket-create',
          name: 'Создать ЗНО',
          action: MENU_ACTIONS.CREATE,
        },
        {
          id: 'ticket-list',
          name: 'Список ЗНО',
          action: MENU_ACTIONS.GET_LIST,
        },
      ],
      permissions: () => MenuService.checkAllActions(Object.values(MENU_ACTIONS), {
        [MENU_ACTIONS.GET_LIST]: {
          groups: [
            RoleService.USER_GROUPS.SALES_MANAGER,
            RoleService.USER_GROUPS.OPERATOR,
            RoleService.USER_GROUPS.TECHNICAL_SUPPORT,
          ],
        },
        [MENU_ACTIONS.CREATE]: {
          groups: [
            RoleService.USER_GROUPS.SALES_MANAGER,
            RoleService.USER_GROUPS.OPERATOR,
            RoleService.USER_GROUPS.TECHNICAL_SUPPORT,
          ],
        },
        [MENU_ACTIONS.GET_INFO]: {
          groups: [
            RoleService.USER_GROUPS.SALES_MANAGER,
            RoleService.USER_GROUPS.OPERATOR,
            RoleService.USER_GROUPS.TECHNICAL_SUPPORT,
          ],
        },
        [MENU_ACTIONS.UPDATE]: {
          groups: [
            RoleService.USER_GROUPS.SALES_MANAGER,
            RoleService.USER_GROUPS.OPERATOR,
            RoleService.USER_GROUPS.TECHNICAL_SUPPORT,
          ],
        },
      }),
    },
    {
      id: 'support-request',
      name: 'Прочее',
      defaultIcon: 'icon',
      activeIcon: 'iconActive',
      children: [
        {
          id: 'support-request-create',
          name: 'Создать запрос “Прочее”',
          action: MENU_ACTIONS.CREATE,
        },
        {
          id: 'support-request-list',
          name: 'Список “Прочее”',
          action: MENU_ACTIONS.GET_LIST,
        },
      ],
      permissions: () => MenuService.checkAllActions(Object.values(MENU_ACTIONS), {
        [MENU_ACTIONS.GET_LIST]: {
          groups: [
            RoleService.USER_GROUPS.SALES_MANAGER,
            RoleService.USER_GROUPS.OPERATOR,
            RoleService.USER_GROUPS.PROJECT_COMMITTEE,
          ],
        },
        [MENU_ACTIONS.CREATE]: {
          groups: [
            RoleService.USER_GROUPS.SALES_MANAGER,
            RoleService.USER_GROUPS.OPERATOR,
          ],
        },
        [MENU_ACTIONS.GET_INFO]: {
          groups: [
            RoleService.USER_GROUPS.SALES_MANAGER,
            RoleService.USER_GROUPS.OPERATOR,
            RoleService.USER_GROUPS.PROJECT_COMMITTEE,
          ],
        },
        [MENU_ACTIONS.UPDATE]: {
          groups: [
            RoleService.USER_GROUPS.PROJECT_COMMITTEE,
          ],
        },
      }),
    },
    {
      id: 'discount',
      name: 'Скидки',
      defaultIcon: 'icon',
      activeIcon: 'iconActive',
      children: [
        {
          id: 'discount-create',
          name: 'Создать скидку',
          action: MENU_ACTIONS.CREATE,
        },
        {
          id: 'discount-list',
          name: 'Список скидок',
          action: MENU_ACTIONS.GET_LIST,
        },
      ],
      permissions: () => MenuService.checkAllActions(Object.values(MENU_ACTIONS), {
        [MENU_ACTIONS.GET_LIST]: {
          groups: [
            RoleService.USER_GROUPS.SALES_MANAGER,
            RoleService.USER_GROUPS.OPERATOR,
            RoleService.USER_GROUPS.PROJECT_COMMITTEE,
          ],
        },
        [MENU_ACTIONS.CREATE]: {
          groups: [
            RoleService.USER_GROUPS.SALES_MANAGER,
            RoleService.USER_GROUPS.OPERATOR,
          ],
        },
        [MENU_ACTIONS.GET_INFO]: {
          groups: [
            RoleService.USER_GROUPS.SALES_MANAGER,
            RoleService.USER_GROUPS.OPERATOR,
            RoleService.USER_GROUPS.PROJECT_COMMITTEE,
          ],
        },
        [MENU_ACTIONS.UPDATE]: {
          groups: [
            RoleService.USER_GROUPS.SALES_MANAGER,
            RoleService.USER_GROUPS.OPERATOR,
          ],
        },
      }),
    },
  ]

  static getList = async (items = MenuService.items) => {
    const result = [];
    await Promise.all(items.map(async ({ permissions, children, ...item }, index) => {
      /* Если не описаны правила доступа к меню - оно доступно всем */
      if (!permissions) {
        result.push(item);
        return;
      }
      /* Получаем права по типам пунктов меню */
      const actionPermissions = await permissions();

      /* Если нет ни одного доступного типа пункта меню - пункт меню недоступен  */
      if (Object.values(actionPermissions).every((hasPermissions) => hasPermissions === false)) return;

      let childItems;
      if (children) {
        childItems = await MenuService.getChildren(children, actionPermissions);
      }
      result.push({
        ...item, index, children: childItems, actionPermissions,
      });
    }));
    return result.sort((a, b) => a.index - b.index).map(({ index, ...item }) => item);
  };

  static getChildren(items, actionPermissions) {
    return items.map(({ children, action, ...item }) => {
      if (!action || !actionPermissions[action]) return;
      return {
        ...item,
        children: children ? MenuService.getChildren(children, actionPermissions) : undefined,
      };
    }).filter(Boolean);
  }

  static getMenu = (menu) => getFlattenItems(MenuService.items).find(({ id }) => id === menu);

  static async checkAllActions(actions = [], permissionRules = {}) {
    /* Получаем id пользователя из контекста */
    const currentUserId = httpContext.get('user');
    /* Получаем актуальные группы пользователя */
    const userInstance = await UserModel.findByPk(currentUserId, { include: ['groups'] });
    if (!userInstance || userInstance?.groups?.length === 0) {
      return actions.reduce((acc, actionKey) => {
        acc[actionKey] = false;
        return acc;
      }, {});
    }
    /* Администратору можно все */
    if (userInstance.groups.find(({ id }) => is.user.group.admin(id))) {
      return actions.reduce((acc, actionKey) => {
        acc[actionKey] = true;
        return acc;
      }, {});
    }

    /* Формируем объект уровней доступа по каждому возможному действию с сущностью */
    return actions.reduce((acc, actionKey) => {
      if (!permissionRules[actionKey]) {
        acc[actionKey] = false;
        return acc;
      }
      const { groups = [], checkAll = false } = permissionRules[actionKey];
      acc[actionKey] = RoleService.hasUserAccess({
        groups,
        userGroups: userInstance.groups,
        checkAll,
      });
      return acc;
    }, {});
  }
}
