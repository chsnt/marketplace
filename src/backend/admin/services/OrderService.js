import moment from 'moment';
import axios from 'axios';
import Sequelize, { Op } from 'sequelize';
import {
  OrderModel, OrderStatusModel, PaymentModel,
} from '../../common/database/models/Order';
import APIError from '../../common/utils/APIError';
import ERRORS from '../constants/errors';
import PaymentService from '../../client/services/PaymentService';
import OrderServiceCommon from '../../common/services/OrderService';
import {
  TariffServiceRelationModel, ServiceModel, TariffModel,
} from '../../common/database/models/Tariff';
import {
  ClientServiceModel,
  ClientTariffModel,
  ClientTariffServiceModel,
} from '../../common/database/models/Client';
import db from '../../common/database';
import { dateRangeForQuery } from '../../common/utils';
import HelperService from '../../common/services/HelperService';
import Discount from '../../common/utils/Discount';
import is from '../../common/libs/is';
import { SECURE_PAYMENTS_API } from '../../common/constants/env';
import { GET_LIST_DEFAULT_LIMIT, GET_LIST_DEFAULT_SORT, TYPE_PAYMENT } from '../../common/constants';
import { getAddressAndDevice, getTotalCostFromCard } from '../../common/utils/prepareResponse';
import AddressModel from '../../common/database/models/AddressModel';
import ServiceAddressAccountingTypeModel from '../../common/database/models/Tariff/ServiceAddressAccountingTypeModel';
import PaymentTypeModel from '../../common/database/models/Order/PaymentTypeModel';

export default class OrderService extends OrderServiceCommon {
  static async getList({
    number,
    dateFrom,
    dateTo,
    clientId,
    paymentType,
    paymentDateFrom,
    paymentDateTo,
    responsibleId,
    orderStatusId,
    client = 'all',
    sort,
    offset = 0,
    limit = GET_LIST_DEFAULT_LIMIT,
  } = {}) {
    const { sort: sortBy = 'createdDate', direction = GET_LIST_DEFAULT_SORT } = sort || {};

    const orderBy = new Map([
      ['number', ['number']],
      ['createdDate', ['created_at']],
      ['paymentDate', ['payments', 'payment_date']],
      ['client', (is.client.type.organization(client))
        ? ['client', 'organization', 'name']
        : [Sequelize.fn(
          'concat',
          Sequelize.col('client.users.first_name'),
          Sequelize.col('client.users.patronymic_name'),
          Sequelize.col('client.users.last_name'),
        )],
      ],
      ['responsible', [Sequelize.fn(
        'concat',
        Sequelize.col('updatedBy.first_name'),
        Sequelize.col('updatedBy.patronymic_name'),
        Sequelize.col('updatedBy.last_name'),
      )]],
      ['amount', ['payments', 'total_cost']],
    ]);

    const orders = await OrderModel.findAndCountAll({
      offset,
      limit,
      subQuery: false,
      where: {
        ...(number && { number }),
        ...(orderStatusId && { orderStatusId }),
        ...(paymentType
          ? {
            '$payments.payment_type_id$': PaymentTypeModel.TYPES[paymentType].id,
          }
          : { '$payments.payment_date$': null }
        ),
        ...dateRangeForQuery({ dateFrom, dateTo, column: 'orderDate' }),
        ...(clientId && { clientId }),
        ...dateRangeForQuery({
          dateFrom: paymentDateFrom,
          dateTo: paymentDateTo,
          column: '$payments.payment_date$',
        }),
        ...(responsibleId && { '$updatedBy.id$': responsibleId }),
      },
      attributes: ['id', 'number', 'orderDate', 'orderStatusId', 'createdAt'],
      include: [
        {
          association: 'client',
          attributes: ['id'],
          required: true,
          include: [
            {
              association: 'organization',
              ...(is.client.type.organization(client) && { required: true }),
              attributes: ['id', 'name'],
            },
            {
              association: 'users',
              ...(is.client.type.individual(client) && { required: true }),
              attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'fullName'],
            },
          ],
        },
        {
          required: false,
          association: 'payments',
          attributes: ['id', 'totalCost', 'paymentTypeId', 'paymentDate', 'number', 'paymentOrderId'],

          where: {
            paymentDate: { [Op.not]: null },
          },
        },
        {
          association: 'updatedBy',
          attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'fullName'],
        },
      ],
      ...(sortBy && { order: [[...orderBy.get(sortBy), direction]] }),
    });

    return {
      ...orders,
      rows: orders.rows.map((orderInstance) => {
        const {
          orderDate: createdAt, payments, updatedBy, ...order
        } = orderInstance.toJSON();

        const payment = PaymentModel.getCurrentPayment(payments);

        const preparedPaidByCard = PaymentService.getPaymentMethod(payment);

        const isInvoicePayment = is.payments.types.id.invoice(payment?.paymentTypeId);

        const typePayment = (isInvoicePayment) ? TYPE_PAYMENT.INVOICE : TYPE_PAYMENT.CARD;

        const paymentDate = payment?.paymentDate || null;

        return {
          ...order,
          createdAt,
          client: orderInstance.client.name,
          cost: payment?.totalCost || null,
          paymentNumber: (preparedPaidByCard) ? payment?.number : null,
          paidByCard: preparedPaidByCard,
          isPaid: !!payment,
          paymentDate,
          responsible: updatedBy?.fullName || null,
          typePayment: paymentDate ? typePayment : null,
        };
      }),
    };
  }

  static async getInfo(orderId) {
    const orderInstance = await OrderModel.findByPk(orderId, {
      attributes: ['id', 'number', 'description', 'orderStatusId'],
      include: [
        {
          association: 'client',
          attributes: ['id'],
          include: [
            {
              association: 'organization',
              attributes: ['id', 'name'],
              include: 'users',
            },
            {
              association: 'users',
              attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'fullName'],
            },
          ],
        },
        {
          association: 'clientServices',
          separate: true,
          attributes: ['id', 'cost'],
          include: [
            {
              association: 'service',
              attributes: ['id', 'name', 'serviceAddressAccountingTypeId'],
            },
            AddressModel.addressAndDevice(),
          ],
        },
        {
          association: 'clientTariffs',
          separate: true,
          attributes: ['id', 'cost'],
          include: [
            {
              association: 'tariff',
              attributes: ['id', 'name'],
              include: [
                {
                  association: 'services',
                  through: { attributes: [] },
                  attributes: ['id', 'serviceAddressAccountingTypeId'],
                },
              ],
            },
            AddressModel.addressAndDevice(),
          ],
        },
        {
          association: 'updatedBy',
          attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'fullName'],
        },
        {
          association: 'payments',
          attributes: ['id', 'paymentDate', 'number', 'paymentTypeId', 'paymentOrderId'],
        },
        {
          association: 'orderDocuments',
          attributes: ['id', 'name'],
        },
        {
          association: 'orderOfferDocument',
          attributes: ['id', 'name'],
        },
      ],
    });

    if (!orderInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(orderId));

    const {
      clientServices,
      clientTariffs,
      orderOfferDocument,
      client: { id: clientId },
      payments,
      updatedBy,
      orderDocuments,
      ...order
    } = orderInstance.toJSON();

    const payment = PaymentModel.getCurrentPayment(payments);

    const isInvoicePayment = is.payments.types.id.invoice(payment?.paymentTypeId);

    let totalCost = 0;

    const contactPerson = orderInstance.client.organization?.users.find((user) => user.isMaintainer)
      || orderInstance.client.users[0];

    const typePayment = (isInvoicePayment) ? TYPE_PAYMENT.INVOICE : TYPE_PAYMENT.CARD;

    const paymentDate = payment?.paymentDate || null;

    return {
      ...order,
      client: {
        id: clientId,
        name: orderInstance.client.name,
      },
      user: contactPerson ? {
        id: contactPerson.id,
        fullName: contactPerson.fullName,
      } : null,
      services: Object.values(clientServices.reduce((
        acc,
        { cost, service: { id, name, serviceAddressAccountingTypeId }, serviceAddress },
        index,
      ) => {
        /* Для услуг с учетом адреса "В заказе" специально модифицируем ключ */
        const key = is.service.serviceAddressAccountingType.inOrder(serviceAddressAccountingTypeId)
          ? `${id}-${index}`
          : id;

        if (!acc[key]) {
          const {
            serviceAddress: preparedServiceAddress = null,
            device = null,
          } = getAddressAndDevice(serviceAddress) || {};

          acc[id] = {
            name,
            count: 1,
            cost,
            serviceAddress: preparedServiceAddress,
            device,
            serviceAddressType: ServiceAddressAccountingTypeModel.NAMES[serviceAddressAccountingTypeId],
          };
        } else {
          acc[id].count += 1;
        }


        totalCost += cost;
        return acc;
      }, {})),
      tariffs: Object.values(clientTariffs.reduce((
        acc,
        { cost, tariff: { id, name, services }, serviceAddress },
        index,
      ) => {
        /* Проверяем услуги, входящие в тариф, чтобы определить тип учета адреса */
        const key = TariffModel.mustTariffHasServiceAddress(services)
          ? `${id}-${index}`
          : id;

        if (!acc[key]) {
          const {
            serviceAddress: preparedServiceAddress = null,
            device = null,
          } = getAddressAndDevice(serviceAddress) || {};

          acc[key] = {
            name,
            count: 1,
            cost,
            serviceAddress: preparedServiceAddress,
            device,
          };
        } else {
          acc[key] += 1;
        }

        totalCost += cost;
        return acc;
      }, {})),
      responsible: (updatedBy && (contactPerson.id !== updatedBy.id)) ? updatedBy.fullName : 'Сайт',
      paymentDate,
      paymentNumber: is.payments.types.id.card(payment?.paymentTypeId) ? payment?.number : null,
      documents: [...(orderOfferDocument ? [orderOfferDocument] : []), ...orderDocuments],
      totalCost,
      typePayment: paymentDate ? typePayment : null,
    };
  }

  static async updateOrder({
    id, paymentDate, discount, transaction, description,
  }) {
    const orderInstance = await OrderModel.findByPk(id, {
      include: [
        {
          association: 'payments',
          attributes: ['id', 'paymentDate', 'createdAt'],
          separate: true,
          limit: 1,
          order: [['createdAt', 'DESC']],
        },
        {
          association: 'client',
          attributes: ['id'],
          include: [
            {
              association: 'organization',
              attributes: ['id', 'name'],
            },
          ],
        },
        {
          association: 'coupon',
          attributes: ['code'],
          include: {
            association: 'discountTemplate',
            include: ['discountTariffServiceRelations'],
          },
        },
      ],
    });

    if (!orderInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(id));

    const localTransaction = transaction || await db.sequelize.transaction();

    const { id: orderId, coupon, discount: currentDiscount } = orderInstance.toJSON();

    try {
      if (!paymentDate && (discount || discount === 0)) {
        await orderInstance.update({ discount }, { transaction: localTransaction });
        if (!transaction) await localTransaction.commit();
        return orderInstance;
      }

      const [clientTariffs, clientServices] = await Promise.all([
        ClientTariffModel.findAll({
          where: { orderId },
          include: [{
            association: 'tariff',
            attributes: ['id', 'expirationMonths'],
            include: [
              TariffModel.includeCost(true),
              {
                association: 'discountService',
                required: false,
                where: {
                  expirationAt: {
                    [Op.or]: [
                      { [Op.gte]: new Date() },
                      { [Op.is]: null },
                    ],
                  },
                },
                include: {
                  association: 'discountTemplate',
                  include: 'discountTariffServiceRelations',
                },
              }],
          }],
          transaction: localTransaction,
        }),
        ClientServiceModel.findAll({
          where: { orderId },
          include: [{
            association: 'service',
            attributes: ['id'],
            include: [
              ServiceModel.includeCost(true),
              {
                association: 'discountService',
                required: false,
                where: {
                  expirationAt: {
                    [Op.or]: [
                      { [Op.gte]: new Date() },
                      { [Op.is]: null },
                    ],
                  },
                },
                include: {
                  association: 'discountTemplate',
                  include: 'discountTariffServiceRelations',
                },
              },
            ],
          }],
          transaction: localTransaction,
        }),
      ]);

      const discountObj = new Discount({
        coupon, clientTariffs, clientServices, discount: currentDiscount,
      });

      const { cost } = getTotalCostFromCard({ discountObj, clientTariffs, clientServices });

      const paymentInstance = orderInstance.payments[0];
      paymentInstance.set({
        paymentDate,
        cost,
        totalCost: cost,
      });

      /* Обновляем статус заказа на "Оплачен" */
      orderInstance.update(
        {
          orderStatusId: OrderStatusModel.STATUSES.PAID,
          description,
        },
        {
          transaction: localTransaction,
        },
      );

      await paymentInstance.save({ transaction: localTransaction });

      /* Добавляем время жизни к тарифy */
      await Promise.all(clientTariffs.map((orderTariff) => {
        const expirationAt = moment()
          .add(orderTariff.tariff.expirationMonths, 'M')
          .add(1, 'day')
          .startOf('day');

        return orderTariff.update({ expirationAt }, { transaction: localTransaction });
      }));

      if (!transaction) await localTransaction.commit();
      return orderInstance;
    } catch (err) {
      if (!transaction) await localTransaction.rollback();
      throw err;
    }
  }

  static async createOrder({
    clientId, tariffs = [], services = [], transaction, description, paymentType,
  }) {
    const localTransaction = transaction || await db.sequelize.transaction();

    try {
      const offerDocumentAndCount = await this.getLastOfferDocumentAndCount({ transaction: localTransaction });

      /* Выполняем проверку доступности услуг для оборудования организации */
      await this.checkAllowedProducts({
        services,
        tariffs,
        shouldAvailablePayment: false,
        transaction: localTransaction,
      });

      const orderInstance = await OrderModel.create({
        clientId,
        orderOfferDocumentId: offerDocumentAndCount.document?.id || null,
        orderStatusId: OrderStatusModel.STATUSES.NOT_PAID,
        clientServices: services.map(({ id: serviceId, serviceAddressId }) => ({
          serviceId,
          serviceAddressId,
        })),
        orderDate: new Date(),
        description,
        include: ['clientTariffs', 'clientServices'],
      }, { transaction: localTransaction });

      const servicesWithNameMap = HelperService.createMapFromArray(await ServiceModel.findAll({
        where: {
          id: services.map(({ id }) => id),
        },
        attributes: ['id', 'name', 'externalId', 'serviceAddressAccountingTypeId'],
        include: [
          ServiceModel.includeCost(true),
          {
            association: 'discountService',
            required: false,
            where: {
              expirationAt: {
                [Op.or]: [
                  { [Op.gte]: new Date() },
                  { [Op.is]: null },
                ],
              },
            },
            include: {
              association: 'discountTemplate',
              include: 'discountTariffServiceRelations',
            },
          },
        ],
      }));

      const tariffsWithNameMap = HelperService.createMapFromArray(await TariffModel.findAll({
        where: {
          id: tariffs.map(({ id }) => id),
        },
        attributes: ['id', 'name', 'externalId'],
        include: [
          {
            association: 'services',
            through: { attributes: [] },
            attributes: ['id', 'serviceAddressAccountingTypeId'],
          },
          {
            ...TariffModel.includeCost(true),
            separate: true,
          },
          {
            association: 'discountService',
            required: false,
            where: {
              expirationAt: {
                [Op.or]: [
                  { [Op.gte]: new Date() },
                  { [Op.is]: null },
                ],
              },
            },
            include: {
              association: 'discountTemplate',
              include: 'discountTariffServiceRelations',
            },
          },
        ],
      }));

      const discountObj = new Discount({
        clientTariffs: Object.values(tariffsWithNameMap).map((tariff) => ({ tariff })),
        clientServices: Object.values(servicesWithNameMap).map((service) => ({ service })),
      });

      const servicesWithCost = services.map(({ id: serviceId, serviceAddressId }) => {
        const serviceWithName = servicesWithNameMap[serviceId];
        /* Если адрес обслуживания должен быть указан при заказе и его нет - выбрасываем ошибку */
        if (
          is.service.serviceAddressAccountingType.inOrder(serviceWithName.serviceAddressAccountingTypeId)
          && !serviceAddressId
        ) {
          throw new APIError(ERRORS.ORDER.PRODUCT_MUST_HAS_SERVICE_ADDRESS({ name: serviceWithName.name }));
        }
        return {
          serviceId,
          clientId,
          serviceAddressId,
          orderId: orderInstance.id,
          cost: discountObj.getDiscountCost(
            { currentCost: ServiceModel.getCurrentCost(serviceWithName.costs), id: serviceId },
          ),
          name: serviceWithName.name,
          externalId: serviceWithName.externalId,
        };
      });

      const tariffsWithCost = tariffs.map(({ id: tariffId, serviceAddressId }) => {
        const tariffWithName = tariffsWithNameMap[tariffId];
        if (
          TariffModel.mustTariffHasServiceAddress(tariffWithName.services)
          && !serviceAddressId
        ) {
          throw new APIError(ERRORS.ORDER.PRODUCT_MUST_HAS_SERVICE_ADDRESS({ name: tariffWithName.name }));
        }
        return {
          tariffId,
          clientId,
          serviceAddressId,
          orderId: orderInstance.id,
          cost: discountObj.getDiscountCost(
            { currentCost: TariffModel.getCurrentCost(tariffWithName.costs), id: tariffId },
          ),
          name: tariffWithName.name,
          externalId: tariffWithName.externalId,
        };
      });

      const clientTariffInstances = await ClientTariffModel.bulkCreate(
        tariffsWithCost,
        { transaction: localTransaction },
      );

      await ClientServiceModel.bulkCreate(
        servicesWithCost,
        { transaction: localTransaction },
      );

      if (tariffs && tariffs.length > 0) {
        /* Получаем все связи с услугами, входящие в добавленные тарифы  */
        const allTariffServiceRelations = await TariffServiceRelationModel.findAll({
          where: { tariffId: tariffs.map(({ id }) => id) },
          attributes: ['id', 'tariffId', 'limit'],
          transaction: localTransaction,
        });

        /* Формируем записи услуг, входящих в тариф, для организации */
        const records = clientTariffInstances.reduce((acc, clientTariff) => {
          allTariffServiceRelations.forEach((tariffServiceRelation) => {
            if (clientTariff.tariffId === tariffServiceRelation.tariffId) {
              acc.push({
                clientTariffId: clientTariff.id,
                tariffServiceRelationId: tariffServiceRelation.id,
                value: tariffServiceRelation.limit,
              });
            }
          });
          return acc;
        }, []);

        await ClientTariffServiceModel.bulkCreate(records, { transaction: localTransaction });
      }
      const cost = tariffsWithCost.concat(servicesWithCost).reduce((acc, current) => (acc + current.cost), 0);
      await PaymentModel.create({
        orderId: orderInstance.id,
        cost,
        totalCost: cost,
        paymentTypeId: PaymentTypeModel.TYPES[paymentType].id,
      }, { transaction: localTransaction });

      if (!transaction) await localTransaction.commit();

      return {
        orderInstance,
        tariffsWithCost,
        servicesWithCost,
        offer: {
          id: offerDocumentAndCount.document?.id || null,
          createdAt: offerDocumentAndCount.document?.createdAt || null,
          count: offerDocumentAndCount.count,
        },
      };
    } catch (err) {
      if (!transaction) await localTransaction.rollback();
      if (!(err instanceof APIError)) throw new APIError(ERRORS.ORDER.CREATE_ORDER, { originalError: err });
      throw err;
    }
  }

  static async requestStatus({ order, paymentOrderId }) {
    const url = `${SECURE_PAYMENTS_API.URL}/${SECURE_PAYMENTS_API.GET_ORDER_STATUS}`;
    const result = await axios.get(
      `${url}
      ?userName=${SECURE_PAYMENTS_API.USER}
      &password=${SECURE_PAYMENTS_API.PASSWORD}
      &orderId=${paymentOrderId}`.replace(/[\s+\r\n]/g, ''),
    );
    const unpaidFinalStatusesSet = new Set([3, 6]);
    if (unpaidFinalStatusesSet.has(result?.data?.orderStatus)) {
      order.set({ orderStatusId: OrderStatusModel.STATUSES.CART });
      await order.save();
    }
    if (result?.data?.actionCode === 0) {
      return OrderStatusModel.STATUSES.PAID;
    }
    return order.orderStatusId;
  }

  static async getPayment(id) {
    const paymentInstance = (await PaymentModel.findAll({
      attributes: ['paymentOrderId', 'createdAt'],
      include: {
        association: 'order',
        where: { id },
        attributes: ['id', 'orderStatusId'],
      },
      limit: 1,
      order: [['createdAt', 'DESC']],
    }))[0];

    if (!paymentInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(id));
    return paymentInstance;
  }
}
