import { UserModel } from '../../common/database/models/User';
import APIError from '../../common/utils/APIError';
import UserServiceCommon from '../../common/services/UserService';
import { generateNewPassword, getPasswordHash } from '../../common/utils/auth';
import NotifierService from '../../common/services/NotifierService';
import { PASSWORD_MESSAGE_TEMPLATE } from '../../common/constants';
import ERRORS from '../constants/errors';
import AuthService from '../../common/services/AuthService';


export default class UserService extends UserServiceCommon {
  static async resetPasswordByAdmin(userId) {
    const userInstance = await UserModel.findByPk(userId, { attributes: ['id', 'phone', 'password'] });

    if (!userInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(userId));

    /* Генерим новый пароль с проверкой старых паролей */
    const newPassword = await UserService.genNewPassword(userId);

    await UserService.resetPassword(userInstance.id, newPassword);

    const passwordForSms = generateNewPassword();

    await AuthService.setTemporaryPassword(userInstance.id, getPasswordHash(passwordForSms));

    /* Отправляем СМС с паролем */
    await NotifierService.sendSMS({
      phone: userInstance.phone,
      text: `${PASSWORD_MESSAGE_TEMPLATE} ${passwordForSms}`,
    });

    return null;
  }

  static getInfo(id) {
    return UserModel.findByPk(id, {
      attributes: [
        'firstName',
        'lastName',
        'patronymicName',
        'email',
        'phone',
        'birthday',
      ],
    });
  }
}
