import httpContext from 'express-http-context';
import SupportRequestServiceCommon from '../../common/services/SupportRequestService';
import {
  SupportRequestModel,
  SupportRequestStatusModel,
} from '../../common/database/models/SupportRequest';
import APIError from '../../common/utils/APIError';
import ERRORS from '../constants/errors';
import { dateRangeForQuery } from '../../common/utils';
import { GET_LIST_DEFAULT_LIMIT, GET_LIST_DEFAULT_SORT } from '../../common/constants';

export default class SupportRequestService extends SupportRequestServiceCommon {
  static async getList({
    number,
    supportRequestStatusId,
    supportRequestTopicId,
    clientId,
    inn,
    responsibleId,
    dateFrom,
    dateTo,
    sortByDate = GET_LIST_DEFAULT_SORT,
    offset = 0,
    limit = GET_LIST_DEFAULT_LIMIT,
  } = {}) {
    const result = await SupportRequestModel.findAndCountAll({
      offset,
      limit,
      where: {
        ...(number && { number }),
        ...(supportRequestStatusId && { supportRequestStatusId }),
        ...(supportRequestTopicId && { supportRequestTopicId }),
        ...(clientId && { clientId }),
        ...(inn && { '$client.organization.inn$': inn }),
        ...(responsibleId && { responsibleId }),
        ...dateRangeForQuery({ dateFrom, dateTo }),
      },
      attributes: ['id', 'number', 'text', 'createdAt', 'firstName', 'patronymicName', 'lastName'],
      order: [['createdAt', sortByDate]],
      include: [
        {
          association: 'client',
          attributes: ['id'],
          include: [
            {
              association: 'organization',
              attributes: ['id', 'name', 'inn'],
            },
            {
              association: 'users',
              attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'fullName'],
            },
          ],
        },
        {
          association: 'supportRequestTopic',
          attributes: ['id', 'name'],
        },
        {
          association: 'supportRequestStatus',
          attributes: ['id', 'name'],
        },
        {
          association: 'responsible',
          attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'fullName'],
        },
      ],
    });

    return {
      ...result,
      rows: result.rows.map((requestInstance) => {
        const {
          responsible, supportRequestTopic, supportRequestStatus,
          firstName, patronymicName, lastName, ...supportRequest
        } = requestInstance.toJSON();

        return {
          ...supportRequest,
          client: requestInstance.client?.name || null,
          topic: supportRequestTopic.name,
          status: supportRequestStatus.name,
          representative: `${lastName ? `${lastName} ` : ''}${firstName}${patronymicName ? ` ${patronymicName}` : ''}`,
          responsible: responsible?.fullName || null,
        };
      }),
    };
  }

  static createRequest(data) {
    const responsibleId = httpContext.get('user');
    return super.createRequest({ ...data, responsibleId });
  }

  static async getStatuses() {
    return SupportRequestStatusModel.findAll({
      attributes: ['id', 'name', 'alias'],
    });
  }

  static async getInfo(supportRequestId) {
    const supportRequestInstance = await SupportRequestModel.findByPk(supportRequestId, {
      attributes: [
        'id', 'text', 'firstName', 'patronymicName', 'lastName',
        'phone', 'email', 'comment', 'text', 'solution', 'number',
      ],
      include: [
        {
          association: 'client',
          attributes: ['id'],
          include: [
            {
              association: 'organization',
              attributes: ['id', 'name'],
            },
            {
              association: 'users',
              attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'fullName'],
            },
          ],
        },
        {
          association: 'supportRequestTopic',
          attributes: ['id', 'name', 'alias'],
        },
        {
          association: 'supportRequestAttachments',
          attributes: ['id', 'name'],
        },
        {
          association: 'supportRequestStatus',
          attributes: ['id', 'name', 'alias'],
        },
        {
          association: 'responsible',
          attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'fullName'],
        },
      ],
    });

    if (!supportRequestInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(supportRequestId));

    const {
      supportRequestTopic: topic,
      supportRequestAttachments: attachments,
      responsible,
      ...supportRequest
    } = supportRequestInstance.toJSON();

    return {
      ...supportRequest,
      client: {
        id: supportRequestInstance.client?.id,
        name: supportRequestInstance.client?.name,
      },
      topic,
      attachments,
      responsible: responsible ? {
        id: responsible.id,
        fullName: responsible.fullName,
      } : null,
    };
  }

  static async updateRequest({ id, ...supportRequest }) {
    const supportRequestInstance = await SupportRequestModel.findByPk(id);

    if (!supportRequestInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(id));

    await supportRequestInstance.update({
      ...supportRequest,
      ...(
        supportRequest?.supportRequestStatusId === SupportRequestStatusModel.STATUSES.SOLVED
          ? { resolvedAt: new Date() }
          : { resolvedAt: null }
      ),
    });

    const supportRequestStatus = await SupportRequestStatusModel.findByPk(
      supportRequest?.supportRequestStatusId,
      { attributes: ['name'] },
    );

    const supportRequestUpdated = supportRequestInstance.toJSON();

    return {
      ...supportRequestUpdated,
      statusName: supportRequestStatus?.name,
    };
  }
}
