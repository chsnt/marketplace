import { Op } from 'sequelize';
import moment from 'moment';
import { CostModel } from '../../common/database/models/Tariff';
import { DATE_FORMAT } from '../../common/constants';
import db from '../../common/database';
import logger from '../../common/services/LoggerService';
import APIError from '../../common/utils/APIError';
import ERRORS from '../constants/errors';

export default class CostService {
  static async addCost({
    id,
    cost,
    startAt,
    expirationAt,
  }) {
    const transaction = await db.sequelize.transaction();

    const startDate = new Date(startAt);

    try {
      const costs = await CostModel.findAll({
        where: {
          tariffOrServiceId: id,
          expirationAt: {
            [Op.or]: [{
              [Op.gt]: startDate,
            }, {
              [Op.is]: null,
            }],
          },
        },
        transaction,
      });

      const dateStop = moment(startDate, DATE_FORMAT.KEBAB_FULL).add(1, 's');

      await Promise.all(costs.map((costInstance) => costInstance.update({
        expirationAt: dateStop.format(DATE_FORMAT.KEBAB_FULL),
      }, { transaction })));

      await CostModel.create({
        tariffOrServiceId: id,
        value: cost,
        startAt: startDate,
        expirationAt,
      }, { transaction });

      await transaction.commit();
      return null;
    } catch (error) {
      await transaction.rollback();
      logger.error(error);
      throw new APIError(ERRORS.COST.CREATE_COST);
    }
  }
}
