import { Op } from 'sequelize';
import ERRORS from '../../common/constants/errors';

import { ClientModel, VClientModel } from '../../common/database/models/Client';
import { UserModel } from '../../common/database/models/User';
import ClientServiceCommon from '../../common/services/ClientService';
import { dateRangeForQuery } from '../../common/utils';
import APIError from '../../common/utils/APIError';
import { GET_LIST_DEFAULT_LIMIT, GET_LIST_DEFAULT_SORT } from '../../common/constants';
import { organization as prepareOrganization } from '../../common/utils/prepareResponse';
import TariffService from '../../common/services/TariffService';
import ServiceService from '../../common/services/ServiceService';
import is from '../../common/libs/is';

const USER_ATTRIBUTES = ['id', 'firstName', 'patronymicName', 'lastName', 'email', 'phone', 'isMaintainer', 'fullName'];
const CLIENT_ATTRIBUTES = ['id', 'number', 'createdAt'];
const ADDRESS_ATTRIBUTES = ['id', 'city', 'street', 'house', 'apartment', 'postalCode'];

export default class ClientService extends ClientServiceCommon {
  static async getAll({
    number,
    name,
    dateFrom,
    dateTo,
    responsibleId,
    inn,
    representative,
    phone,
    email,
    firstName,
    lastName,
    fullName,
    client = 'all',
    sort,
    offset = 0,
    limit = GET_LIST_DEFAULT_LIMIT,
  } = {}) {
    const { sort: sortBy = 'date', direction = GET_LIST_DEFAULT_SORT } = sort || {};

    const orderBy = new Map([
      ['date', ['createdAt']],
      ['number', ['number']],
      ['name', ['name']],
      ['organization', ['organization.name']],
      ['inn', ['organization.inn']],
      ['representative', ['organization.user.fullName']],
      ['responsible', ['responsible.fullName']],
      ['phone', [(is.client.type.organization(client)) ? 'organization.user.phone' : 'individual.phone']],
      ['email', [(is.client.type.organization(client)) ? 'organization.user.email' : 'individual.email']],
      ['firstName', ['user.firstName']],
      ['lastName', ['user.lastName']],
    ]);

    const clients = await VClientModel.findAndCountAll({
      offset,
      limit,
      subQuery: false,
      where: {
        ...(number && { number }),
        ...(name && { name: { [Op.iLike]: `%${name}%` } }),
        ...(is.client.type.organization(client) && {
          individual: { [Op.is]: null },
        }),
        ...(is.client.type.individual(client) && {
          organization: { [Op.is]: null },
        }),
        ...(is.client.type.organization(client) && inn && {
          'organization.inn': { [Op.iLike]: `%${inn}%` },
        }),
        ...(is.client.type.organization(client) && representative && {
          'organization.user.fullName': { [Op.iLike]: `%${representative}%` },
        }),
        ...(is.client.type.organization(client) && phone && {
          'organization.user.phone': { [Op.iLike]: `%${phone}%` },
        }),
        ...(is.client.type.organization(client) && email && {
          'organization.user.email': { [Op.iLike]: `%${email}%` },
        }),
        ...(is.client.type.individual(client) && firstName && {
          'individual.firstName': { [Op.iLike]: `%${firstName}%` },
        }),
        ...(is.client.type.individual(client) && lastName && {
          'individual.lastName': { [Op.iLike]: `%${lastName}%` },
        }),
        ...(is.client.type.individual(client) && fullName && {
          'individual.fullName': { [Op.iLike]: `%${fullName}%` },
        }),
        ...(is.client.type.individual(client) && phone && {
          'individual.phone': { [Op.iLike]: `%${phone}%` },
        }),
        ...(is.client.type.individual(client) && email && {
          'individual.email': { [Op.iLike]: `%${email}%` },
        }),
        ...(responsibleId && { 'responsible.id': responsibleId }),
        ...dateRangeForQuery({ dateFrom, dateTo }),
      },
      ...(sortBy && { order: [[...orderBy.get(sortBy), direction]] }),
    });

    return {
      ...clients,
      rows: clients.rows.map((clientInstance) => {
        const {
          organization, individual, responsible, ...clientData
        } = clientInstance.toJSON();

        return {
          ...clientData,
          profile: (is.client.type.organization(clientData.type)) ? 'ЮЛ' : 'ФЛ',
          organization,
          individual,
          responsible: responsible
            ? `${responsible?.lastName} ${responsible?.firstName} ${responsible?.patronymicName}`
            : 'Сайт',
        };
      }),
    };
  }

  static async getOrganizations({
    number,
    name,
    inn,
    email,
    representative,
    phone,
    dateFrom,
    dateTo,
    responsibleId,
    sortByDate = GET_LIST_DEFAULT_SORT,
    offset = 0,
    limit = GET_LIST_DEFAULT_LIMIT,
  } = {}) {
    const searchAttribute = UserModel.getSearchAttribute({ value: representative, preKey: 'organization.users' });
    const clients = await ClientModel.findAndCountAll({
      offset,
      limit,
      subQuery: false,
      where: {
        ...(number && { number }),
        ...(name && { '$organization.name$': { [Op.iLike]: `%${name}%` } }),
        ...(inn && { '$organization.inn$': inn }),
        ...(representative && { $and: searchAttribute }),
        ...(email && { '$organization.users.email$': { [Op.iLike]: `%${email}%` } }),
        ...(phone && { '$organization.users.phone$': phone }),
        ...(responsibleId && { '$updatedBy.id$': responsibleId }),
        ...dateRangeForQuery({ dateFrom, dateTo }),
      },
      attributes: ['id', 'number', 'createdAt'],
      include: [
        {
          association: 'organization',
          attributes: ['id', 'name', 'inn'],
          required: true,
          include: [
            {
              association: 'users',
              attributes: USER_ATTRIBUTES,
            },
          ],
        },
        {
          association: 'updatedBy',
          attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'fullName'],
        },
      ],
      order: [['createdAt', sortByDate]],
    });

    return {
      ...clients,
      rows: clients.rows.map((clientInstance) => {
        const {
          organization: {
            ...organization
          },
          updatedBy,
          ...client
        } = clientInstance.toJSON();

        const user = clientInstance.maintainer;

        return {
          ...client,
          organization: { ...organization, users: undefined },
          representative: user?.fullName || null,
          email: user?.email || null,
          phone: user?.phone || null,
          responsible: updatedBy?.fullName || null,
        };
      }),
    };
  }

  static async getIndividuals({
    number,
    firstName,
    lastName,
    phone,
    email,
    dateFrom,
    dateTo,
    responsibleId,
    sortByDate = GET_LIST_DEFAULT_SORT,
    offset = 0,
    limit = GET_LIST_DEFAULT_LIMIT,
  } = {}) {
    const clients = await ClientModel.findAndCountAll({
      offset,
      limit,
      subQuery: false,
      where: {
        ...(number && { number }),
        ...(firstName && { '$users.first_name$': { [Op.iLike]: `%${firstName}%` } }),
        ...(lastName && { '$users.last_name$': { [Op.iLike]: `%${lastName}%` } }),
        ...(responsibleId && { '$updatedBy.id$': responsibleId }),
        ...(email && { '$users.email$': { [Op.iLike]: `%${email}%` } }),
        ...(phone && { '$users.phone$': phone }),
        ...dateRangeForQuery({ dateFrom, dateTo }),
      },
      attributes: CLIENT_ATTRIBUTES,
      include: [
        {
          association: 'users',
          attributes: ['id', 'firstName', 'lastName', 'phone', 'email', 'isMaintainer'],
          where: { organizationId: null },
          required: true,
        },
        {
          association: 'updatedBy',
          attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'fullName'],
        },
      ],
      order: [['createdAt', sortByDate]],
    });

    return {
      ...clients,
      rows: clients.rows.map((clientInstance) => {
        const { updatedBy, ...client } = clientInstance.toJSON();

        const user = clientInstance.maintainer.toJSON();

        return {
          ...user,
          ...client,
          users: undefined,
          responsible: updatedBy?.fullName || null,
        };
      }),
    };
  }

  static async getInfo(id) {
    const clientInstance = await ClientModel.findByPk(id, {
      include: [
        {
          association: 'organization',
          attributes: ['id', 'name', 'inn', 'kpp', 'realAddressId', 'legalAddressId'],
          include: [
            {
              association: 'users',
              attributes: USER_ATTRIBUTES,
            },
            {
              association: 'electronicDocumentCirculation',
              attributes: ['id', 'name', 'alias'],
            },
            {
              association: 'electronicDocumentCirculationDescription',
              attributes: ['id', 'description'],
            },
            {
              association: 'bankDetail',
              attributes: ['id', 'bankAccount'],
              include: [
                {
                  association: 'taxType',
                  attributes: ['id', 'name', 'alias'],
                },
                {
                  association: 'bank',
                  attributes: ['id', 'name', 'rcbic', 'correspondentAccount'],
                },
              ],
            },
            {
              association: 'realAddress',
              attributes: ADDRESS_ATTRIBUTES,
              include: [{
                association: 'federationSubject',
                attributes: ['id', 'name'],
              }, {
                association: 'cityRelationship',
                attributes: ['id', 'name'],
              }],
            },
            {
              association: 'legalAddress',
              attributes: ADDRESS_ATTRIBUTES,
              include: [{
                association: 'federationSubject',
                attributes: ['id', 'name'],
              }, {
                association: 'cityRelationship',
                attributes: ['id', 'name'],
              }],
            },
          ],
        },
        {
          association: 'users',
          attributes: USER_ATTRIBUTES,
        },
      ],
      attributes: ['id', 'number'],
    });

    if (!clientInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(id));

    const clientId = clientInstance.id;
    const [
      serviceAddresses, orders,
      archivedTariffs, archivedServices,
      clientTariffs, clientServices,
    ] = await Promise.all([
      ClientService.getServiceAddressesByDeviceType(clientId),
      ClientService.getClientOrders(clientId),
      TariffService.getClientTariffs({ clientId, used: true }),
      ServiceService.getClientServices({ clientId, used: true }),
      TariffService.getClientTariffs({ clientId }),
      ServiceService.getClientServices({ clientId }),
    ]);

    const { organization, ...client } = clientInstance.toJSON();

    return {
      client: {
        ...client,
        users: undefined,
        user: clientInstance.maintainer,
        organization: organization ? prepareOrganization(organization) : undefined,
      },
      serviceAddresses,
      orders,
      active: {
        tariffs: clientTariffs,
        services: clientServices,
      },
      archive: {
        tariffs: archivedTariffs,
        services: archivedServices,
      },
    };
  }
}
