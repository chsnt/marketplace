import { UniqueConstraintError } from 'sequelize';
import IntegratorModel from '../../common/database/models/IntegratorModel';
import APIError from '../../common/utils/APIError';
import ERRORS from '../../common/constants/errors';
import db from '../../common/database';


export default class IntegratorService {
  static async createIntegrator({ token, name }) {
    try {
      const integratorInstance = await IntegratorModel.create({
        token, name,
      });
      return integratorInstance.toJSON();
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        throw new APIError(ERRORS.INTEGRATION.CREATE_DUPLICATE_INTEGRATOR);
      }

      throw new APIError(ERRORS.INTEGRATION.CREATE_INTEGRATOR, { originalError: error });
    }
  }

  static async updateIntegrator({ id, token, name }) {
    const transaction = await db.sequelize.transaction();
    try {
      const integratorInstance = await IntegratorModel.findByPk(id, {
        transaction,
      });

      if (!integratorInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(id));

      await integratorInstance.update({
        ...(token && { token }),
        ...(name && { name }),
      }, { transaction });

      const integrator = integratorInstance.toJSON();
      await transaction.commit();
      return integrator;
    } catch (error) {
      await transaction.rollback();
      if (error instanceof UniqueConstraintError) {
        throw new APIError(ERRORS.INTEGRATION.UPDATE_DUPLICATE_INTEGRATOR);
      }

      throw new APIError(ERRORS.INTEGRATION.UPDATE_INTEGRATOR, { originalError: error });
    }
  }

  static async getInfo({ id }) {
    const integratorInstance = await IntegratorModel.findByPk(id);
    const { id: integratorId, token, name } = integratorInstance.toJSON();
    return { id: integratorId, token, name };
  }

  static async getList() {
    const integratorInstances = await IntegratorModel.findAll();

    return integratorInstances.map((integrator) => {
      const { id, token, name } = integrator.toJSON();
      return { id, token, name };
    });
  }
}
