import { CallCategoryModel } from '../../common/database/models/Admin';

export default class CallCategoryService {
  static getList() {
    return CallCategoryModel.findAll({
      attributes: ['id', 'name', 'alias'],
    });
  }
}
