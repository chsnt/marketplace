import { Strategy as LocalStrategy } from 'passport-local';
import { OAuth2Strategy } from 'passport-oauth';
import https from 'https';
import fs from 'fs';
import bcrypt from 'bcrypt';
import axios from 'axios';
import path from 'path';
import { URL } from 'url';
import jwt from 'jsonwebtoken';
import { Op } from 'sequelize';

import RoleService from '../services/RoleService';
import { UserModel } from '../database/models/User';
import ActiveDirectoryService from '../services/ActiveDirectoryService';
import { SBBID } from '../constants/env';

import ERRORS from '../constants/errors';
import AuthService from '../services/AuthService';
import UserService from '../services/UserService';
import db from '../database';
import logger from '../services/LoggerService';
import APIError from './APIError';
import ClientService from '../services/ClientService';
import { TaxTypeModel } from '../database/models/Client/Organization';
import { ElectronicDocumentCirculationModel } from '../database/models/Edo';

import { generateNewPassword } from './auth';

class OAuth2 extends OAuth2Strategy {
   userProfile = async (accessToken, done) => {
     const DEFAULT_FILE_PATH = process.cwd();
     try {
       const instance = axios.create({
         httpsAgent: new https.Agent({
           cert: fs.readFileSync(path.join(DEFAULT_FILE_PATH, SBBID.CERTIFICATE_PATH, SBBID.CERTIFICATE)),
           key: fs.readFileSync(path.join(DEFAULT_FILE_PATH, SBBID.CERTIFICATE_PATH, SBBID.CERTIFICATE_KEY)),
           rejectUnauthorized: false,
         }),
         headers: { Authorization: `Bearer ${accessToken}` },
         baseURL: SBBID.BASE_URL,
       });

       return done(null, await instance.get(SBBID.RESOURCE_USER_INFO));
     } catch (error) {
       return done(error);
     }
   }

  authorizationParams = (options) => ({
    nonce: options.nonce,
  })
}

export function setup({ passportInstance, isAdmin = false }) {
  passportInstance.use(new LocalStrategy(
    { usernameField: 'emailOrPhone', passwordField: 'password' },
    async (emailOrPhone, password, done) => {
      if (!emailOrPhone || !password) return done(null, false, ERRORS.AUTH.USER_NOT_FOUND.error);

      try {
        const user = await UserModel.findByEmailOrPhoneOrLogin(emailOrPhone, {
          include: [{
            association: 'groups',
            attributes: ['id'],
          }],
        });

        if (!user) return done(null, false, ERRORS.AUTH.USER_NOT_FOUND.error);

        const hasUserAccessToAdmin = RoleService.hasUserAccess({
          groups: RoleService.ACCESS_TO_ADMIN_PANEL,
          userGroups: user.groups,
        });

        const temporaryPassword = await AuthService.getTemporaryPassword(user.id);
        const isTemporaryPassword = temporaryPassword ? bcrypt.compareSync(String(password), temporaryPassword) : false;

        if (isTemporaryPassword) {
          await AuthService.deleteTemporaryPassword(user.id);
          await AuthService.setTemporaryAuthentication(user.id);
          return done(null, {
            ...user.toJSON(),
            clientId: user.clientId || user?.organization?.clientId,
            isTemporaryPassword,
          });
        }

        await AuthService.deleteTemporaryAuthentication(user.id);
        await AuthService.deleteTemporaryPassword(user.id);

        if (isAdmin) {
          /* Проверка прав для админки */
          if (!hasUserAccessToAdmin) return done(null, false, ERRORS.AUTH.USER_IS_NOT_ADMIN.error);

          if (process.env.NODE_ENV === 'development') return done(null, user.toJSON());
          const activeDirectoryService = new ActiveDirectoryService();
          await activeDirectoryService.checkAuth(emailOrPhone, password);
          return done(null, user.toJSON());
        }

        if (hasUserAccessToAdmin) return done(null, false, ERRORS.AUTH.USER_NOT_FOUND.error);

        const authenticated = await user.authenticate(password);
        if (!authenticated) return done(null, false, ERRORS.AUTH.USER_NOT_FOUND.error);

        return done(null, {
          ...user.toJSON(),
          clientId: user.clientId || user.organization?.clientId,
        });
      } catch (err) {
        return done(err);
      }
    },
  ));

  const authorizationURL = new URL(SBBID.RESOURCE_AUTHORIZATION_URL, SBBID.AUTH_BASE_URL).href;
  const tokenURL = new URL(SBBID.RESOURCE_TOKEN_URL, SBBID.AUTH_BASE_URL).href;

  passportInstance.use('provider', new OAuth2({
    authorizationURL,
    tokenURL,
    clientID: SBBID.CLIENT_ID,
    clientSecret: SBBID.CLIENT_SECRET,
    callbackURL: SBBID.CALLBACK_URL,
    state: true,
    scope: ['openid', 'inn', 'email', 'phone_number', 'HashOrgId', 'orgKpp', 'orgFullName', 'OrgName', 'sid2', 'sub',
      'name', 'orgLawFormShort', 'orgOkpo', 'orgActualAddress', 'orgJuridicalAddress', 'accounts'],
    passReqToCallback: true,
  },
  async (req, accessToken, refreshToken, params, profile, done) => {
    const transaction = await db.sequelize.transaction();

    try {
      const profileData = jwt.decode(profile.data);

      const user = await UserModel.findOne({
        where: {
          [Op.or]: [
            { '$organization.sbbid_id$': profileData.HashOrgId },
            { '$organization.inn$': profileData.inn },
          ],
        },
        include: 'organization',
        transaction,
      });

      if (user) {
        if (user.organization.sbbidId) {
          done(null, user.toJSON());
          await transaction.commit();
          return;
        }

        await user.update({
          sbbidId: profileData.sub,
        }, { transaction });

        await user.organization.update({
          sbbidId: profileData.HashOrgId,
        }, { transaction });

        done(null, user.toJSON());
        await transaction.commit();
        return;
      }

      const [
        firstName = 'Ваше Имя',
        lastName = 'Ваша Фамилия',
        patronymicName = '',
      ] = profileData.name.split(' ') || [];

      const { organization } = await ClientService.registerClient({
        user: {
          lastName,
          firstName,
          patronymicName,
          email: profileData.email,
          phone: profileData.phone_number,
          sbbidId: profileData.sub,
          password: generateNewPassword(),
        },
        organization: {
          sbbidId: profileData.HashOrgId,
          kpp: profileData.orgKpp,
          inn: profileData.inn,
          name: profileData.OrgName,
          legalAddress: UserService.separateAddress(profileData.orgJuridicalAddress),
          realAddress: UserService.separateAddress(profileData.orgActualAddress || profileData.orgJuridicalAddress),
          electronicDocumentCirculationId: ElectronicDocumentCirculationModel.STATUSES.EDO_IS_MISSING,
          bankDetail: {
            taxTypeId: TaxTypeModel.TYPES.OSNO,
          },
        },
        transaction,
      });

      done(null, {
        newUser: true,
        ...organization.users[0].toJSON(),
      });
      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      logger.error(error);
      return done(error);
    }
  }));


  passportInstance.serializeUser((user, done) => {
    done(null, user.id);
  });

  passportInstance.deserializeUser(async (id, done) => {
    try {
      const userInstance = await UserModel.findByPk(id, {
        attributes: { exclude: ['password'] },
        include: [{
          association: 'organization',
          attributes: ['id', 'clientId'],
        }],
      });

      if (!userInstance) throw new APIError(ERRORS.AUTH.USER_NOT_FOUND);

      const isTemporaryPassword = await AuthService.getTemporaryAuthentication(id);

      done(null, {
        ...userInstance.toJSON(),
        clientId: userInstance.clientId || userInstance.organization?.clientId,
        isTemporaryPassword: isTemporaryPassword === 'true',
      });
    } catch (err) {
      done(err);
    }
  });
}
