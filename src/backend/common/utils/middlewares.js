import httpContext from 'express-http-context';

import { MulterError } from 'multer';
import RequestLogModel from '../database/models/RequestLogModel';
import IntegratorModel from '../database/models/IntegratorModel';
import logger from '../services/LoggerService';
import APIError from './APIError';
import ERRORS from '../constants/errors';

const authError = (error, res, req, type) => {
  if (type === 'authOpenID') {
    return res.redirect(new URL(req.session.referer).pathname);
  }

  return res.status(ERRORS.COMMON.SERVER_ERROR.statusCode).json(
    null, false, error.message, ERRORS.COMMON.SERVER_ERROR.errorCode,
  );
};

const login = ({
  req, res, next, user, isTemporaryPassword, type,
}) => req.login(user, (loginError) => {
  if (loginError) {
    return res.status(ERRORS.AUTH.LOGIN_ERROR.statusCode).json(
      null, false, loginError.message, ERRORS.AUTH.LOGIN_ERROR.errorCode,
    );
  }

  const oneDay = 96 * 90 * 10000;
  const maxAge = req.body?.shouldSave ? 7 * oneDay : process.env.SESSION_EXPIRE || oneDay;

  if (isTemporaryPassword) user.profile.isTemporaryPassword = isTemporaryPassword;

  req.session.cookie.originalMaxAge = maxAge;

  if (type === 'authOpenID') {
    return res.redirect(new URL(req.session.referer).pathname);
  }

  return next();
});

/* Middleware, необходимое для логирования тела ответа (response body) */
export const memoResponseBody = (req, res, next) => {
  const boundWrite = res.write.bind(res);
  const boundEnd = res.end.bind(res);

  const chunks = [];

  res.write = (chunk, ...rest) => {
    chunks.push(chunk);
    boundWrite(chunk, ...rest);
  };

  res.end = (chunk, ...rest) => {
    if (chunk) chunks.push(chunk);
    if (!['string', 'undefined'].includes(typeof chunk)) res.locals.body = Buffer.concat(chunks).toString();
    boundEnd(chunk, ...rest);
  };

  next();
};

/* Middleware для логирования запросов. Создает запись в БД по каждому запросу,
также выводит verbose-лог с основной информацией */
export const logRequest = async (req, res, next) => {
  res.on('finish', async () => {
    const body = JSON.stringify(req.body);

    const result = {
      userId: req.user?.id,
      url: req.path,
      method: req.method,
      body,
      headers: JSON.stringify(req.headers),
      params: JSON.stringify(req.params),
      query: JSON.stringify(req.query),
      time: res.get('X-Response-Time'),
      statusCode: res.statusCode,
      responseBody: res.locals.body,
    };
    try {
      const logRecord = await RequestLogModel.create(result);
      // eslint-disable-next-line max-len
      logger.verbose(`[HTTP-${logRecord.method}] | ${logRecord.url} | STATUS: ${logRecord.statusCode} | TIME: ${logRecord.time}ms`);
    } catch (error) {
      /* Т.к. у нас теперь в каждом запросе потенциально опасная операция (запись в бд),
      обрабатываем ошибку, логируем ее, но не мешаем запросу выполняться дальше */
      logger.error(error);
    }
  });
  next();
};

/* Middleware проверки авторизации пользоватекля */
export const isAuthenticated = (req, res, next) => {
  if ((!req.isAuthenticated || !req.isAuthenticated())
    || (req.user.isTemporaryPassword && !(req.url === '/user/me/settings' && req.method === 'POST'))) {
    return res.status(ERRORS.COMMON.UNAUTHORIZED.statusCode).json(
      null, false, ERRORS.COMMON.UNAUTHORIZED.error, ERRORS.COMMON.UNAUTHORIZED.errorCode,
    );
  }

  if (req.user.isTemporaryPassword) req.body.isTemporaryPassword = req.user?.isTemporaryPassword;

  req.account = req.user.account;
  /* Записываем в контекст id пользователя */
  httpContext.set('user', req.user.id);
  return next();
};

export const isIntegrator = async (req, res, next) => {
  const account = await IntegratorModel.findOne({ where: { token: req.headers.apikey } });
  if (!account) {
    return res.status(ERRORS.COMMON.UNAUTHORIZED.statusCode).json(
      null, false, ERRORS.COMMON.UNAUTHORIZED.error, ERRORS.COMMON.UNAUTHORIZED.errorCode,
    );
  }
  return next();
};

export const errorHandler = (err, req, res, next) => {
  logger.error(err);

  if (err instanceof MulterError) {
    if (err.code === 'LIMIT_FILE_SIZE') {
      return res.status(ERRORS.FILE.FILE_TOO_LARGE.statusCode)
        .json(null, false, ERRORS.FILE.FILE_TOO_LARGE.error, ERRORS.FILE.FILE_TOO_LARGE.errorCode);
    }
    return res.status(ERRORS.FILE.FILE_UPLOAD_ERROR.statusCode)
      .json(null, false, ERRORS.FILE.FILE_UPLOAD_ERROR.error, ERRORS.FILE.FILE_UPLOAD_ERROR.errorCode);
  }

  if (err instanceof APIError) {
    return res.status(err.statusCode).json(err.data, false, err.error, err.errorCode);
  }

  if (err?.error?.isJoi) {
    return res.status(ERRORS.COMMON.VALIDATION_ERROR().statusCode).json(
      null, false, err.error.toString(), ERRORS.COMMON.VALIDATION_ERROR().errorCode,
    );
  }

  if (Number.isInteger(err?.statusCode)) { // обработка невалидного json
    return res.status(err.statusCode).json(err.message, false, err.error, err.errorCode);
  }

  return res.status(ERRORS.COMMON.SERVER_ERROR.statusCode).json(
    null, false, ERRORS.COMMON.SERVER_ERROR.error, ERRORS.COMMON.SERVER_ERROR.errorCode,
  );
};

export const authMiddleware = ({
  req, res, next, emailOrPhone, password, isTemporaryPassword,
}) => (err, user, info) => {
  if (!emailOrPhone || !password) {
    return res.status(ERRORS.AUTH.NOT_FULL_CREDENTIALS.statusCode).json(
      null, false, ERRORS.AUTH.NOT_FULL_CREDENTIALS.error, ERRORS.AUTH.NOT_FULL_CREDENTIALS.errorCode,
    );
  }

  if (err) return authError(err, res);

  if (!user) {
    return res.status(ERRORS.AUTH.USER_NOT_FOUND.statusCode).json(
      null, false, info, ERRORS.AUTH.USER_NOT_FOUND.errorCode,
    );
  }

  return login({
    req, res, next, user, isTemporaryPassword,
  });
};


export const authOpenIDMiddleware = ({ req, res }) => (err, user) => {
  const type = 'authOpenID';
  if (err) return authError(err, res, req, type);

  return login({
    req, res, user, type,
  });
};
