import Joi from '@hapi/joi';
import joiDate from '@hapi/joi-date';
import ERRORS from '../constants/errors';
import { DATE_FORMAT } from '../constants';

import { ElectronicDocumentCirculationModel } from '../database/models/Edo';
import HelperService from '../services/HelperService';

const JoiExt = Joi.extend(joiDate);

const isEmptyString = (value) => (typeof value === 'string' && !/\S/.test(value));
const isEmpty = (value) => value === undefined || value === null || value === '' || isEmptyString(value);

export function isPhone(value, { mobileOnly = true }) {
  const regularExpressions = {
    mobilePhonesOnly: /^(\+?7|8)?9\d{9}$/i,
    allRussianPhones: /^(\+?7|8)?(3|4|8|9)\d{9}$/i,
  };

  return !isEmpty(value) && (mobileOnly
    ? regularExpressions.mobilePhonesOnly
    : regularExpressions.allRussianPhones
  ).test(value.replace(/\D/g, ''));
}

export const passwordValidation = Joi.string().min(8);
export const confirmPasswordValidation = Joi
  .string().when('password', { is: Joi.exist(), then: Joi.required().valid(Joi.ref('password')) });

export const checkPhone = ({ mobileOnly = true } = {}) => Joi.custom((value) => {
  const strPhone = value.toString();
  const result = isPhone(strPhone, { mobileOnly });
  if (!result) {
    throw Error();
  }
  return HelperService.normalizePhone(strPhone);
}).message(ERRORS.USER.PHONE_IS_INVALID.error);

export const baseDateValidation = JoiExt.date().format(DATE_FORMAT.KEBAB_FULL).raw();
export const dateValidationWithTimeZone = JoiExt.date().format(DATE_FORMAT.KEBAB_FULL_WITH_TIME_ZONE).raw();
export const dateOnly = JoiExt.date().format(DATE_FORMAT.KEBAB_SHORT).raw();
export const dateDotNormal = JoiExt.date().format(DATE_FORMAT.DOT_NORMAL).raw();

export const checkId = Joi.string().uuid();

/* Функция, возвращающая схему валидации для query параметров, где значением может быть null */
export const queryWithNull = (schema) => Joi.any().custom((value) => {
  if (value === 'null') return null;

  return schema.validate(value).value;
});

export const checkRequiredId = checkId.required();

export const checkAddress = {
  region: checkId,
  federationSubjectId: checkId,
  fiasId: checkId,
  city: Joi.string(),
  cityId: checkId,
  street: Joi.string(),
  house: Joi.string(),
  apartment: Joi.string(),
  postalCode: Joi.string().allow(null, ''),
};

export const limitOffset = {
  offset: Joi.number(),
  limit: Joi.number(),
  sortByDate: Joi.string().valid('ASC', 'DESC', '').default('DESC'),
};

const whenNotFias = (field) => field.when('fiasId', { not: Joi.exist(), then: Joi.required() });

const subjectRequired = ['federationSubjectId', 'street'];

const addressForRegister = Joi.object(checkAddress)
  .fork(['house'], (field) => field.required())
  .fork(subjectRequired, whenNotFias)
  .min(1)
  .xor('cityId', 'city', 'fiasId')
  .rename('region', 'federationSubjectId');

export const bankDetail = {
  name: Joi.string().min(2).max(100),
  bankAccount: Joi.string().length(20),
  correspondentAccount: Joi.string().length(20),
  rcbic: Joi.string().length(9),
  taxTypeId: checkId,
};

export const innValidator = Joi.string().min(10).max(12);

export const organization = {
  isSameAddress: Joi.boolean(),
  name: Joi.string().min(2),
  inn: innValidator,
  kpp: Joi.string().length(9),
  electronicDocumentCirculationId: checkId,
  electronicDocumentCirculationDescription: Joi.string().when(
    'electronicDocumentCirculationId',
    {
      is: Joi.exist().valid(ElectronicDocumentCirculationModel.STATUSES.NOT_IN_LIST),
      then: Joi.required(),
    },
  ),
  legalAddress: addressForRegister.required(),
  realAddress: addressForRegister.when('isSameAddress', { is: Joi.valid(false), then: Joi.required() }),
  bankDetail: Joi.object(bankDetail).min(1).and(
    ...Object.keys(bankDetail).filter((field) => ![
      'name',
      'bankAccount',
      'correspondentAccount',
      'rcbic',
    ].includes(field)),
  ).required(),
  digitalSignature: Joi.object({
    from: dateOnly,
    to: dateOnly.greater(Joi.ref('from')),
  }),
};

export const baseRegister = {
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  patronymicName: Joi.string().allow(null, ''),
  email: Joi.string().email(),
  birthday: dateOnly,
  phone: checkPhone().required(),
  organization: Joi.object(organization),
};

export const clientDevice = {
  parameters: Joi.array().items(Joi.object({
    id: checkRequiredId,
    value: Joi.any().required(),
  })).unique('id'),
};

export const search = Joi.string().allow('');

export const getList = Joi.object({
  search,
});

export const createClientDevice = {
  model: checkRequiredId,
  clientId: checkId,
  parameters: Joi.array().items(Joi.object({
    id: checkRequiredId,
    value: Joi.any().required(),
  })).required().unique('id'),
  address: Joi.object(checkAddress)
    .fork(['house'], (field) => field.required())
    .fork([...subjectRequired, 'cityId'], whenNotFias)
    .min(1)
    .required()
    .rename('region', 'federationSubjectId'),
};


export const checkTariffsOrServices = Joi.array().items(checkRequiredId);

export const passwordAndToken = {
  password: passwordValidation.required(),
  confirmPassword: confirmPasswordValidation,
  registrationToken: checkRequiredId,
};

export const userRegistration = {
  email: Joi.string().email().required(),
  phone: checkPhone({ mobileOnly: true }).required(),
  firstName: Joi.string().required(),
  patronymicName: Joi.string().allow(null, ''),
  lastName: Joi.string().required(),
};

export const createOrderFromCart = {
  invoice: Joi.boolean().default(false),
  description: Joi.string().allow('', null),
  order: Joi.array().items(Joi.object({
    deviceId: checkId,
    tariff: checkId,
    services: checkTariffsOrServices.unique(),

  }).xor('deviceId', 'newDevice').or('tariff', 'services')),
};

const validateSort = (...validKeys) => Joi.string().custom((value, helpers) => {
  const [sort, direction] = JSON.parse(value);

  const schema = Joi.object({
    sort: Joi.string().valid(...validKeys),
    direction: Joi.string().valid('DESC', 'ASC'),
  });

  const result = {
    sort,
    direction,
  };

  const resultValidation = schema.validate(result);

  if (resultValidation.error) {
    return helpers.error(resultValidation.error);
  }

  return result;
});

export const clientSort = validateSort('date', 'number', 'organization', 'inn', 'representative',
  'phone', 'email', 'firstName', 'lastName', 'name', 'responsible');

export const leadSort = validateSort('representative', 'number', 'date', 'phone',
  'email', 'city', 'nextCallDate', 'responsible', 'client');

export const orderSort = validateSort('number', 'createdDate', 'paymentDate', 'client',
  'responsible', 'amount');

export const discountTemplateSort = validateSort('date');
export const couponSort = validateSort('date');
export const discountServiceSort = validateSort('date');
