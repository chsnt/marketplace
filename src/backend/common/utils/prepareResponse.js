import NomenclatureModel from '../database/models/Abstract/NomenclatureModel';

export const organization = (data) => {
  const {
    legalAddress,
    realAddress,
    digitalSignatureFrom,
    digitalSignatureTo,
    electronicDocumentCirculation,
    electronicDocumentCirculationDescription,
    bankDetail: {
      taxType,
      bankAccount,
      bank,
    },
    ...organizationData
  } = data;

  const prepareAddress = (rawAddress) => {
    const {
      federationSubject,
      cityRelationship,
      ...address
    } = rawAddress;

    return {
      ...address,
      ...(cityRelationship?.name && {
        id: cityRelationship?.id,
        city: cityRelationship?.name,
        externalId: cityRelationship?.externalId,
        alias: cityRelationship?.alias,
      }),
      region: {
        id: federationSubject?.id,
        name: federationSubject?.name,
        externalId: federationSubject?.externalId,
        alias: federationSubject?.alias,
      },
    };
  };

  const cleanData = (rawData) => ({
    ...rawData,
    createdAt: undefined,
    updatedAt: undefined,
    deletedAt: undefined,
    created_by: undefined,
    updated_by: undefined,
  });

  return {
    ...organizationData,
    legalAddress: prepareAddress(legalAddress || {}),
    realAddress: prepareAddress(realAddress || {}),
    electronicDocumentCirculation: {
      ...cleanData(electronicDocumentCirculation),
      externalId: undefined,
    },
    electronicDocumentCirculationDescription: {
      ...cleanData(electronicDocumentCirculationDescription),
      organization_id: undefined,
      electronicDocumentCirculationId: undefined,
      organizationId: undefined,
    },
    taxType: cleanData(taxType),
    digitalSignature: {
      from: digitalSignatureFrom,
      to: digitalSignatureTo,
    },
    bank: cleanData({
      ...bank,
      bankAccount,
    }),
  };
};

/**
 * getTotalCostFromOrder
 * @param {Array} clientTariffs купленные тарифы
 * @param {Array} clientServices купленные тарифы
 * @returns {number} сумма стоимости тарифов и услуг
 */

export const getTotalCostFromOrder = ({ clientTariffs, clientServices }) => {
  const getCost = (clientNomenclature) => clientNomenclature?.reduce((acc, data) => acc + data.cost, 0);
  return parseFloat((getCost(clientTariffs) + getCost(clientServices)).toFixed(2));
};

/**
 * @typedef {Object} cost
 * @property {Number} cost.cost Общая стоимость без скидки
 * @property {Number} cost.totalCost Общая стоимость с учетом скидок
 */

/**
 * @param {Object.<Discount>} discountObj Экземпляр доступных скидок
 * @param {Array} clientTariffs тарифы в корзине
 * @param {Array} clientServices услуги в корзине
 * @description Возвращает цены с учетом сумм скидок и без них
 * @return {{cost}}
 */
export const getTotalCostFromCard = ({ discountObj, clientTariffs, clientServices }) => {
  const getCost = (clientNomenclature) => clientNomenclature?.reduce(
    (acc, data) => {
      const currentCost = NomenclatureModel.getCurrentCost(data.service?.costs || data.tariff?.costs);
      const discountCost = discountObj.getDiscountCost({ currentCost, id: data.service?.id || data.tariff?.id });
      acc.cost += currentCost;
      acc.totalCost += discountCost || currentCost;
      return acc;
    },
    { cost: 0, totalCost: 0 }
  );

  const tariffCosts = getCost(clientTariffs);
  const servicesCosts = getCost(clientServices);

  return {
    cost: parseFloat((tariffCosts.cost + servicesCosts.cost).toFixed(2)),
    totalCost: parseFloat((tariffCosts.totalCost + servicesCosts.totalCost).toFixed(2)),
  };
};

export const userToString = ({ lastName, firstName, patronymicName }) => `${lastName} ${firstName} ${patronymicName}`;

export const addressToString = ({
  postalCode, subject, city, street, house, apartment,
}) => {
  const index = postalCode ? `${postalCode}, ` : '';
  const room = apartment ? `, помещение ${apartment}` : '';
  return `${index}${subject}, г. ${city}, ул. ${street}, д. ${house}${room}`;
};

export const getAddressAndDevice = (serviceAddress) => {
  if (!serviceAddress) return;

  const {
    cityRelationship, federationSubject, clientDevice, ...address
  } = serviceAddress;

  const {
    id,
    deviceModel: { name: model, deviceType: { name: type, alias }, deviceManufacturer },
    clientDeviceParameters,
  } = clientDevice;

  const { name: manufacturer } = deviceManufacturer || {};

  return {
    serviceAddress: {
      ...address,
      city: cityRelationship,
      region: federationSubject,
      cityRelationship: undefined,
      federationSubject: undefined,
    },
    device: {
      id,
      type,
      alias,
      manufacturer,
      model,
      parameters: clientDeviceParameters.map(({
        value,
        deviceParameter,
      }) => ({ value, ...deviceParameter })),
    },
  };
};
