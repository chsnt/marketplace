import bcrypt from 'bcrypt';
import randomize from 'randomatic';
import { MIN_PASSWORD_LENGTH, GENERATED_PASSWORD_PATTERN } from '../constants/env';

export const getPasswordHash = (password, salt) => bcrypt.hashSync(password, salt || bcrypt.genSaltSync());

export const generateNewPassword = ({ pattern, length } = {}) => randomize(
  pattern || GENERATED_PASSWORD_PATTERN,
  length || MIN_PASSWORD_LENGTH,
  {
    exclude: ['0', 'o', 'O', 'i', 'I', 'l', 'L', '-', '{', '}', '[', ']', ',', '.', '\''],
  },
);
