import httpStatus from 'http-status';

import logger from '../services/LoggerService';

export default class APIError extends Error {
  constructor({ error, errorCode, statusCode = httpStatus.BAD_REQUEST }, { originalError, errorData = null } = {}) {
    super(error);
    this.errorCode = errorCode;
    this.error = error;
    this.statusCode = statusCode;
    this.originalError = originalError;
    this.data = errorData;

    if (originalError) logger.error(originalError, errorData);

    Error.captureStackTrace(this, APIError);
    /* Создаем запись в system_logs */
  }
}
