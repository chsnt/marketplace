import is from '../libs/is';

export default class Discount {
  #discountFixMap = new Map();

  #discountPercentList = new Set();

  #discountPercent = 0;

  #coupon;

  #discount;

  #discountByCard;

  #addToDiscountMap = (clientNomenclatures) => {
    clientNomenclatures.forEach((clientNomenclature) => {
      const nomenclature = clientNomenclature.tariff || clientNomenclature.service;
      /* к тарифу или услуге привязывается только услуга у которой может быть новая фиксированная цена */
      const discountTemplate = nomenclature.discountService?.discountTemplate;

      if (!discountTemplate) return;

      if (is.discount.type.fixedPrice(discountTemplate.discountTypeId)) {
        discountTemplate.discountTariffServiceRelations.forEach((discountNomenclature) => this.#discountFixMap.set(
          discountNomenclature.tariffId || discountNomenclature.serviceId,
          discountNomenclature.value,
        ));
      } else {
        this.#discountPercentList.add(discountTemplate.value);
      }
    });
  };

  constructor({
    coupon, clientTariffs, clientServices, discount = 0, discountByCard = 0,
  }) {
    this.#coupon = coupon;
    this.#discount = discount;
    this.#discountByCard = discountByCard;

    this.#addToDiscountMap(clientTariffs);
    this.#addToDiscountMap(clientServices);

    if (coupon) {
      if (is.discount.type.fixedPrice(coupon.discountTemplate.discountTypeId)) {
        coupon.discountTemplate.discountTariffServiceRelations.forEach(
          (nomenclature) => this.#discountFixMap.set(
            nomenclature.tariffId || nomenclature.serviceId, nomenclature.value,
          ),
        );
      } else {
        this.#discountPercentList.add(coupon.discountTemplate.value);
      }
    }

    const discountPercent = this.#discountPercentList.size ? Math.max(...this.#discountPercentList) : 0;

    this.#discountPercent = (discountPercent > 100) ? 100 : discountPercent;
  }

  getDiscountCost({ currentCost, id, isInvoicePayment }) {
    if (!currentCost) return currentCost;

    let newCost = currentCost;

    if (this.#discountFixMap.has(id)) {
      newCost = parseFloat(this.#discountFixMap.get(id).toFixed(2));
    }

    this.#discountByCard = (isInvoicePayment) ? 0 : this.#discountByCard;

    const commonPercent = this.#discount + this.#discountPercent + this.#discountByCard;

    const commonPercentDiscount = (commonPercent > 100) ? 100 : commonPercent;

    if (commonPercentDiscount && commonPercentDiscount) {
      newCost = parseFloat((newCost - ((newCost / 100) * commonPercentDiscount)).toFixed(2));
    }

    return newCost;
  }
}
