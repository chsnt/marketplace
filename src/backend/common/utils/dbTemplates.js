
export default (DataTypes) => {
  const TABLES = {
    USERS: 'users',
  };

  const foreign = ({
    onDelete = 'SET NULL', onUpdate = 'CASCADE', table, allowNull = true, defaultValue,
  }) => ({
    type: DataTypes.UUID,
    references: {
      model: table,
      key: 'id',
    },
    defaultValue,
    onDelete,
    onUpdate,
    allowNull,
  });

  const autoIncrement = ({ allowNull = true, unique = false }) => ({
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull,
    unique,
  });

  return {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      allowNull: false,
      defaultValue: DataTypes.literal ? DataTypes.literal('gen_random_uuid()') : DataTypes.UUIDV4,
    },
    number: autoIncrement({ allowNull: false, unique: true }),
    timestamps: {
      created_at: {
        type: 'TIMESTAMP',
        allowNull: false,
        defaultValue: DataTypes.literal ? DataTypes.literal('CURRENT_TIMESTAMP') : DataTypes.NOW,
      },
      updated_at: {
        type: 'TIMESTAMP',
        allowNull: false,
        defaultValue: DataTypes.literal ? DataTypes.literal('CURRENT_TIMESTAMP') : DataTypes.NOW,
      },
      deleted_at: { type: 'TIMESTAMP' },
    },
    foreign,
    autoIncrement,
    by: {
      created_by: {
        ...foreign({ table: TABLES.USERS }),
      },
      updated_by: {
        ...foreign({ table: TABLES.USERS }),
      },
    },
  };
};
