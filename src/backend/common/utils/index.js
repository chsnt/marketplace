import { Op } from 'sequelize';
import moment from 'moment';

export const isEqual = (value1, value2) => {
  /* Если оба значения falsy, возвращаем, что они равны */
  if (!value1 && !value2) return true;

  /* Если одно из значений (или оба) - числа, сравниваем их строковые значения, это нужно потому что
  секвелайз часто хранит в инстансе строковое значение, даже если поле типа number, при этом при получении
  инстанса из БД это поле является числом */
  if ([typeof value1, typeof value2].includes('number')) return String(value1) === String(value2);

  /* если это даты, сравниваем их как даты */
  if ([new Date(value1).getTime(), new Date(value2).getTime()].every((value) => !Number.isNaN(value))) {
    return new Date(value1).getTime() === new Date(value2).getTime();
  }

  return JSON.stringify(value1) === JSON.stringify(value2);
};

// Начинает строку с заглавной буквы
export const uppercaseFirst = (str) => {
  if (!str) return str;

  return `${str[0].toUpperCase()}${str.slice(1)}`;
};

export const dateRangeForQuery = ({
  dateFrom, dateTo, column = 'createdAt', isFullDay = true,
}) => ((dateFrom || dateTo) && {
  [column]: {
    [Op.and]: [
      ...(dateFrom ? [{
        [Op.gte]: isFullDay ? moment(dateFrom).startOf('day').toISOString() : moment(dateFrom).toISOString(),
      }] : []),
      ...(dateTo ? [{
        [Op.lte]: isFullDay ? moment(dateTo).endOf('day').toISOString() : moment(dateTo).toISOString(),
      }] : []),
    ],
  },
});
