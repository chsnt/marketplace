export default {
  up: async (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const payments = await queryInterface.sequelize.query(
      'SELECT id, cost FROM payments WHERE payments.cost = div(payments.total_cost::numeric, 100);',
      { type: DataTypes.QueryTypes.SELECT, transaction },
    );

    if (!payments?.length) return;

    return Promise.all(payments.map((payment) => queryInterface.bulkUpdate(
      'payments',
      {
        total_cost: payment.cost,
      },
      { id: payment.id },
      { transaction },
    )));
  }),
  down: async () => {},
};
