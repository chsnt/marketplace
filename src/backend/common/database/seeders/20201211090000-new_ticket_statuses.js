import { Op } from 'sequelize';

const TICKET_STATUSES_TABLE = 'ticket_statuses';

const TICKET_STATUSES = [
  {
    id: 'f12d1350-8375-4f5e-8833-a1c0037cbd88',
    name: 'Новая',
    alias: 'NEW',
  },
  {
    id: 'c02e88f5-6873-4d80-9bf7-2d10f3b86507',
    name: 'В работе',
    alias: 'IN_PROGRESS',
  },
  {
    id: '28a69c44-5c6b-4bd1-9b00-993d0434a5f5',
    name: 'Уточнение информации',
    alias: 'UPDATE_INFORMATION',
  },
  {
    id: 'ca1eeef7-b084-4546-955a-45da4f333679',
    name: 'Выезд инженера',
    alias: 'ENGINEER_VISIT',
  },
  {
    id: 'b539214b-89e3-4cc9-9f73-0f7c080e1afe',
    name: 'Решен',
    alias: 'COMPLETED',
  },
];

export default {
  up: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    /* Удаляем старые статусы */
    await queryInterface.bulkDelete(TICKET_STATUSES_TABLE, { name: { [Op.not]: null } }, { transaction });

    return queryInterface.bulkInsert(TICKET_STATUSES_TABLE, TICKET_STATUSES, { transaction });
  }),

  down: (queryInterface) => queryInterface.bulkDelete(TICKET_STATUSES_TABLE, {
    id: TICKET_STATUSES.map(({ id }) => id),
  }),
};
