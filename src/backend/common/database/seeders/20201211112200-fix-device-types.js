const TABLE = 'device_types';

const VALUE = {
  ID: 'b57e113d-cb78-4313-a039-24e80d53a223',
  ALIAS: 'DEFAULT',
};

export default {
  up: (queryInterface) => queryInterface.bulkUpdate(
    TABLE, {
      alias: VALUE.ALIAS,
    },
    { id: VALUE.ID },
  ),

  down: async () => {},
};
