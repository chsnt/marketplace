const TABLE = 'device_types';
const now = new Date();

export default {
  up: (queryInterface) => queryInterface.bulkInsert(TABLE, [
    {
      id: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
      name: 'Кассы',
      alias: 'CASHIER',
      created_at: now,
      updated_at: now,
    },
    {
      id: '14eeadde-dbcb-44ac-9d72-e1cc11617c98',
      name: 'Компьютеры',
      alias: 'COMPUTERS',
      created_at: now,
      updated_at: now,
    },
  ]),
  down: async () => {}, // do nothing
};
