const TABLE = 'available_payment_methods';
const NOMENCLATURE_TABLES = [
  'services',
  'tariffs',
];

const DATA = [
  {
    id: '86536609-fb7d-4831-846a-d32fd581978a',
    name: 'Все способы',
    alias: 'ALL',
  }, {
    id: '8871a1bc-c482-42ef-b5b3-3e66f5c73771',
    name: 'Банковская карта',
    alias: 'CARD',
  }, {
    id: '87cb562f-d5c7-4650-bcec-5429cbacc2cb',
    name: 'Счет на оплату',
    alias: 'INVOICE',
  },
];

export default {
  up: (queryInterface) => queryInterface.sequelize.transaction(
    async (transaction) => {
      await queryInterface.bulkInsert(
        TABLE,
        DATA,
        { transaction },
      );

      await Promise.all(NOMENCLATURE_TABLES.map((table) => queryInterface.bulkUpdate(
        table,
        { available_payment_method_id: '86536609-fb7d-4831-846a-d32fd581978a' },
        { },
        { transaction },
      )));
    },
  ),
  down: (queryInterface) => queryInterface.sequelize.transaction(
    async (transaction) => {
      await Promise.all(NOMENCLATURE_TABLES.map((table) => queryInterface.bulkUpdate(
        table,
        { available_payment_method_id: null },
        {},
        { transaction },
      )));

      await Promise.all(DATA.map((item) => queryInterface.bulkDelete(
        TABLE,
        { id: item.id },
        { transaction },
      )));
    },
  ),
};
