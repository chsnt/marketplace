/* Чтоб не плодить файлы с сидерами - создадим типы документов и города в одном файле */

const ORDER_DOCUMENT_TYPES_TABLE = 'order_document_types';
const CITIES_TABLE = 'cities';

const ORDER_DOCUMENT_TYPES = [
  {
    id: 'd045b659-7914-4bf6-a100-9523e64b3428',
    name: 'Договор-оферта',
    alias: 'OFFER',
  },
  {
    id: '5bc13be2-a0b5-4cf1-8be4-bdc385442830',
    name: 'Акт',
    alias: 'ACT',
  },
  {
    id: '0d019524-ef4e-4742-902c-1f7aea6eb554',
    name: 'УПД',
    alias: 'UTD',
  },
  {
    id: 'ee19de7a-41f5-428e-aa6b-1611683e706f',
    name: 'Счёт на оплату',
    alias: 'INVOICE',
  },
];

const CITIES = [
  {
    id: 'e359bacc-06fe-4826-b828-de4aae3a62b6',
    name: 'Москва',
    is_testing_participant: true,
  },
  {
    id: '244e0486-4f5c-4a49-9468-da541fe2a209',
    name: 'Краснодар',
    is_testing_participant: true,
  },
  {
    id: 'ef820116-ea20-4d4d-9c2e-c462099a1f77',
    name: 'Калининград',
    is_testing_participant: false,
  },
  {
    id: '4b665516-7cf3-42de-b2e6-11ae4167d8b1',
    name: 'Нижний Новгород',
    is_testing_participant: true,
  },
  {
    id: 'ae02ef18-6937-42d8-83a8-6832b7568cd6',
    name: 'Самара',
    is_testing_participant: false,
  },
  {
    id: '7f6c6e7f-a62f-4c4f-94cd-da2938d5c0fc',
    name: 'Сочи',
    is_testing_participant: true,
  },
  {
    id: '53da2ca9-4876-4585-8a43-2d24721a5b81',
    name: 'Челябинск',
    is_testing_participant: false,
  },
];

export default {
  up: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.bulkInsert(ORDER_DOCUMENT_TYPES_TABLE, ORDER_DOCUMENT_TYPES, { transaction }),
    queryInterface.bulkInsert(CITIES_TABLE, CITIES, { transaction }),
  ])),

  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.bulkDelete(ORDER_DOCUMENT_TYPES_TABLE, {
      id: ORDER_DOCUMENT_TYPES.map(({ id }) => id),
    }, { transaction }),
    queryInterface.bulkDelete(CITIES_TABLE, {
      id: CITIES.map(({ id }) => id),
    }, { transaction }),
  ])),


};
