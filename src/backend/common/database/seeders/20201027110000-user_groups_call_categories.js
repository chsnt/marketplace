const DATA = [{
  tableName: 'user_groups',
  data: [
    {
      id: '45f54fd5-28af-40ec-8c4c-a516cbbeb2dd',
      name: 'Админ',
      alias: 'ADMIN',
    },
    {
      id: '07fd8ab6-9b79-45b0-a727-ac8b68fe25b7',
      name: 'Диспетчер',
      alias: 'DISPATCHER',
    },
    {
      id: '463cae5d-b6e6-4349-8849-22a0dce46e88',
      name: 'Менеджер по продажам',
      alias: 'SALES_MANAGER',
    },
  ],
},
{
  tableName: 'call_categories',
  data: [
    {
      id: '4d2aabd2-c99e-4969-808e-b0175ff08d8e',
      name: 'Консультация',
      alias: 'CONSULTATION',
    },
    {
      id: '81978ce7-9555-4ac7-b9ba-4434fafeb20c',
      name: 'Продажа',
      alias: 'SALE',
    },
    {
      id: '565a99e7-cb51-46ac-b472-2ea9ece8b245',
      name: 'Другое',
      alias: 'OTHER',
    },
  ],
},
];

export default {
  up: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all(
    DATA.map(({ tableName, data }) => queryInterface.bulkInsert(tableName, data, { transaction })),
  )),
  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all(
    DATA.map(({ tableName, data }) => queryInterface.bulkDelete(
      tableName,
      { id: data.map(({ id }) => id) },
      { transaction },
    )),
  )),
};
