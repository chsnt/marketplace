export default {
  up: async (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const tickets = await queryInterface.sequelize.query(`
        SELECT "TicketModel"."id",
               "clientServices"."client_id"                     AS "clientServicesClientId",
               "clientTariffServices->clientTariff"."client_id" AS "clientTariffServicesClientId"
        FROM "tickets" AS "TicketModel"
                 LEFT OUTER JOIN ( "ticket_client_service_relations" AS "clientServices->ticketClientServiceRelation" 
                     INNER JOIN "client_services" AS "clientServices" ON
                    "clientServices"."id" = "clientServices->ticketClientServiceRelation"."client_service_id" AND
                    ("clientServices->ticketClientServiceRelation"."deleted_at" IS NULL))
                                 ON "TicketModel"."id" = "clientServices->ticketClientServiceRelation"."ticket_id" AND
                                    ("clientServices"."deleted_at" IS NULL)
                 LEFT OUTER JOIN "services" AS "clientServices->service"
                                 ON "clientServices"."service_id" = "clientServices->service"."id" AND
                                    ("clientServices->service"."deleted_at" IS NULL)
                 LEFT OUTER JOIN ( "ticket_client_tariff_service_relations" AS 
                     "clientTariffServices->ticketClientTariffServiceRelation" INNER JOIN "client_tariff_services" AS
                         "clientTariffServices" ON
                    "clientTariffServices"."id" =
                    "clientTariffServices->ticketClientTariffServiceRelation"."client_tariff_service_id" AND
                    ("clientTariffServices->ticketClientTariffServiceRelation"."deleted_at" IS NULL))
                                 ON "TicketModel"."id" =
                                    "clientTariffServices->ticketClientTariffServiceRelation"."ticket_id" AND
                                    ("clientTariffServices"."deleted_at" IS NULL)
                 LEFT OUTER JOIN "client_tariffs" AS "clientTariffServices->clientTariff"
                                 ON "clientTariffServices"."client_tariff_id" =
                                    "clientTariffServices->clientTariff"."id" AND
                                    ("clientTariffServices->clientTariff"."deleted_at" IS NULL);
    `, { type: DataTypes.QueryTypes.SELECT, transaction });

    if (!tickets?.length) return;

    return Promise.all(tickets.map((ticket) => queryInterface.bulkUpdate(
      'tickets',
      {
        client_id: ticket.clientServicesClientId || ticket.clientTariffServicesClientId,
      },
      { id: ticket.id },
      { transaction },
    )));
  }),
  down: async () => {},
};
