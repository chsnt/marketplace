const TABLE = 'discount_types';

const DATA = [{
  id: 'c764b9a8-45ec-44de-bba7-b68c580d478b',
  name: 'Скидка',
  alias: 'DISCOUNT',
}, {
  id: '9d879343-6d34-45e7-9467-1dcd13c23dd0',
  name: 'Фиксированная цена',
  alias: 'FIXED_PRICE',
}];

export default {
  up: (queryInterface) => queryInterface.bulkInsert(TABLE, DATA),
  down: (queryInterface) => queryInterface.sequelize.transaction(
    (transaction) => Promise.all(DATA.map((item) => queryInterface.bulkDelete(
      TABLE,
      { id: item.id },
      { transaction },
    ))),
  ),
};
