const DATA = {
  tableName: 'support_request_statuses',
  values: [
    {
      id: '708ae989-0f82-4371-9e3c-33463eb960e4',
      name: 'Новый',
      alias: 'NEW',
    },
    {
      id: 'e8d914c9-76a0-4689-9b23-5daf3f6d3a9b',
      name: 'В работе',
      alias: 'IN_PROGRESS',
    },
    {
      id: '8b2fe441-05b2-448d-aefa-811e20cbbaa2',
      name: 'Уточнение информации',
      alias: 'UPDATE_INFORMATION',
    },
    {
      id: '5cccbe60-118d-4d1f-8d2d-42822ccc08c9',
      name: 'Решено',
      alias: 'SOLVED',
    },
  ],
};
export default {
  up: (queryInterface) => queryInterface.bulkInsert(DATA.tableName, DATA.values),
  down: (queryInterface) => queryInterface.bulkDelete(DATA.tableName, { id: DATA.values.map(({ id }) => id) }),
};
