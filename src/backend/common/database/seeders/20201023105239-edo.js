
const ELECTRONIC_DOCUMENT_CIRCULATIONS_TABLE = 'electronic_document_circulations';

const now = new Date();

const OPERATORS = [
  {
    id: 'cfb4588a-bdf1-4d43-9d83-6f2b9bd6d5a0',
    name: 'Корус консалтинг СНГ (Сфера Курьер)',
    alias: 'KORUS_KONSALTING_SNG_SFERA_KURER',
  },
  {
    id: '30ffaaa1-1c4f-42a5-b9de-87fa2426dc1f',
    name: 'СКБ Контур (Контур.Диадок)',
    alias: 'SKB_KONTUR_KONTUR_DIADOK',
  },
  {
    id: '9575b473-bb84-471b-a20e-090e56f402da',
    name: 'TaxCom',
    alias: 'TAX_COM',
  },
  {
    id: 'e6514477-fcb0-42a4-a411-1d956cca7bd0',
    name: 'E-COM',
    alias: 'E_COM',
  },
  {
    id: 'a8a42118-24e5-4ce8-9b97-bc17b4c2f45b',
    name: 'Калуга.Астрал (Калуга Онлайн)',
    alias: 'KALUGA_ASTRAL_KALUGA-ONLAJN',
  },
];

export default {
  up: (queryInterface) => queryInterface.bulkInsert(
    ELECTRONIC_DOCUMENT_CIRCULATIONS_TABLE,
    OPERATORS.map((operator) => ({
      ...operator,
      created_at: now,
      updated_at: now,
    })),
  ),
  down: (queryInterface) => queryInterface.bulkDelete(ELECTRONIC_DOCUMENT_CIRCULATIONS_TABLE, {
    id: OPERATORS.map(({ id }) => id),
  }),
};
