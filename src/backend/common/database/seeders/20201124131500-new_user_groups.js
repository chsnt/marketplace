const TABLE = 'user_groups';

const ALIASES_TO_DELETE = ['DISPATCHER'];

const ADMIN_ALIAS = 'ADMIN';

const NEW_USER_GROUPS = [
  {
    id: 'e0645386-7de3-43e6-9720-06a21ab83868',
    name: 'Оператор',
    alias: 'OPERATOR',
  },
  {
    id: 'd33ea5be-7a87-4f41-acac-9d4958e5d243',
    name: 'Техническая поддержка',
    alias: 'TECHNICAL_SUPPORT',
  },
  {
    id: '8fa7a6ee-ad01-451b-8485-22f7e16a0166',
    name: 'Проектный комитет',
    alias: 'PROJECT_COMMITTEE',
  },
];

export default {
  up: (queryInterface) => queryInterface.sequelize.transaction(
    async (transaction) => Promise.all([
      queryInterface.bulkDelete(TABLE, { alias: ALIASES_TO_DELETE }, { transaction }),
      queryInterface.bulkInsert(TABLE, NEW_USER_GROUPS, { transaction }),
      queryInterface.bulkUpdate(TABLE, {
        alias: 'ADMINISTRATOR',
        name: 'Администратор',
      }, { alias: ADMIN_ALIAS }, { transaction }),
    ]),
  ),

  down: async () => {},
};
