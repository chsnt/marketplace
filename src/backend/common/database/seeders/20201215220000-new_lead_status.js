const TABLE = 'lead_statuses';

const LEAD_STATUS = {
  id: 'c6017f97-dc9c-4ca2-8eae-4e9affc8f11c',
  name: 'Ожидаем получение документов',
  alias: 'AWAITING_DOCUMENTS',
  priority: 4,
};

const OLD_MAX_PRIORITY = 5;

export default {
  up: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    for (let i = OLD_MAX_PRIORITY; i >= LEAD_STATUS.priority; i -= 1) {
      queryInterface.bulkUpdate(TABLE, { priority: i + 1 }, { priority: i }, { transaction });
    }
    return queryInterface.bulkInsert(TABLE, [LEAD_STATUS], { transaction });
  }),

  down: async () => {},
};
