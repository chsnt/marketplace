/*
Договор-оферта - генерится сайтом в момент заказа
Счет на оплату - есть не во всех заказах. Приходит после запроса из CRM (1С)
УПД (статус 2) - приходит из CRM (1С) после получения оплаты
УПД (статус 1) - приходит не документ, а информация о способе отправки оригинала (ЭДО / Почта)
*/


const ORDER_DOCUMENT_TYPES_TABLE = 'order_document_types';
const LIMIT_MEASURES_TABLE = 'limit_measures';

const ORDER_DOCUMENT_TYPES_UP = [
  {
    id: '0d019524-ef4e-4742-902c-1f7aea6eb554',
    name: 'УПД (статус 1)',
    alias: 'UTD_STATUS_ONE',
  },
  {
    id: '5bc13be2-a0b5-4cf1-8be4-bdc385442830',
    name: 'УПД (статус 2)',
    alias: 'UTD_STATUS_TWO',
  },
];

const ORDER_DOCUMENT_TYPES_DOWN = [
  {
    id: '0d019524-ef4e-4742-902c-1f7aea6eb554',
    name: 'УПД',
    alias: 'UTD',
  },
  {
    id: '5bc13be2-a0b5-4cf1-8be4-bdc385442830',
    name: 'Акт',
    alias: 'ACT',
  },
];

const LIMIT_MEASURES = [
  {
    id: '895f6745-f9eb-4d73-ba8c-4a545ffa0a7b',
    name: 'Срок оказания услуги',
    alias: 'PERIOD_OF_SERVICE',
  },
  {
    id: '91aa207f-ad87-4332-a35f-063ed9cb7bc4',
    name: 'Количество работ в месяц',
    alias: 'COUNT_PER_MONTH',
  },
];


export default {
  up: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    ...ORDER_DOCUMENT_TYPES_UP.map(
      (documentType) => queryInterface.bulkUpdate(ORDER_DOCUMENT_TYPES_TABLE,
        { name: documentType.name, alias: documentType.alias },
        {
          id: documentType.id,
        }, { transaction }),
    ),
    queryInterface.bulkInsert(LIMIT_MEASURES_TABLE, LIMIT_MEASURES, { transaction }),
  ])),
  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    ...ORDER_DOCUMENT_TYPES_DOWN.map(
      (documentType) => queryInterface.bulkUpdate(ORDER_DOCUMENT_TYPES_TABLE,
        { name: documentType.name, alias: documentType.alias },
        {
          id: documentType.id,
        }, { transaction }),
    ),
    queryInterface.bulkDelete(LIMIT_MEASURES_TABLE, {
      id: LIMIT_MEASURES.map(({ id }) => id),
    }, { transaction }),
  ])),
};
