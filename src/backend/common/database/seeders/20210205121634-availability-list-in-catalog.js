const TABLE = 'available_catalog_statuses';
const NOMENCLATURE_TABLES = [
  'services',
  'tariffs',
];

const DATA = [{
  id: 'd4c9ac21-279e-48c4-8064-2349dfa50001',
  name: 'Везде',
  alias: 'ALL',
}, {
  id: 'b6a19d76-9906-4b9f-8493-9ec1aeb1fb43',
  name: 'На сайте',
  alias: 'SITE',
}, {
  id: 'b7140b0f-1df1-48f5-a955-7598eb20c63c',
  name: 'В админке',
  alias: 'ADMIN',
}];

export default {
  up: (queryInterface) => queryInterface.sequelize.transaction(
    async (transaction) => {
      await queryInterface.bulkInsert(
        TABLE,
        DATA,
        { transaction },
      );

      await Promise.all(NOMENCLATURE_TABLES.map((table) => queryInterface.bulkUpdate(
        table,
        { available_catalog_status_id: 'd4c9ac21-279e-48c4-8064-2349dfa50001' },
        { },
        { transaction },
      )));
    },
  ),
  down: (queryInterface) => queryInterface.sequelize.transaction(
    async (transaction) => {
      await Promise.all(NOMENCLATURE_TABLES.map((table) => queryInterface.bulkUpdate(
        table,
        { available_catalog_status_id: null },
        {},
        { transaction },
      )));

      await Promise.all(DATA.map((item) => queryInterface.bulkDelete(
        TABLE,
        { id: item.id },
        { transaction },
      )));
    },
  ),
};
