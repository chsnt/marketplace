const DATA = [
  {
    table: 'support_request_topics',
    rows: [
      {
        id: '2426d252-40ca-4f01-8d79-93c6cda61ac3',
        name: 'Работа с сайтом',
        alias: 'WORKING_WITH_SITE',
      },
      {
        id: '009f274c-2e09-49af-86d4-fe90d3d59bc1',
        name: 'Гарантия и возврат',
        alias: 'GUARANTEE_AND_REFUND',
      },
      {
        id: '1845e775-4721-4b26-af19-d3a699df388c',
        name: 'Документы и правовые вопросы',
        alias: 'DOCUMENTS_AND_LEGAL',
      },
      {
        id: 'ac9555dd-e258-41e8-886a-245aaf0af44e',
        name: 'Прочее',
        alias: 'ETC',
      },
    ],
  },
  {
    table: 'lead_statuses',
    rows: [
      {
        id: 'e26776e5-a6f3-4e1c-913b-9f7b9cf0413a',
        name: 'Готов к продаже',
        alias: 'READY_FOR_SALES',
        priority: 1,
      },
    ],
  },
  {
    table: 'tax_types',
    rows: [
      {
        id: 'ad24d66f-eda3-4844-a1de-1c6d055bb4b8',
        name: 'Общая система налогообложения (ОСНО)',
        alias: 'OSNO',
      },
      {
        id: '9f10486d-5f51-4771-922f-c562ed333b36',
        name: 'Упрощенная (включая ЕНВД, ЕСХН, ПСН и НПД)',
        alias: 'USN',
      },
    ],
  },
];

export default {
  up: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all(
    DATA.map(({ table, rows }) => queryInterface.bulkInsert(
      table,
      rows,
      { transaction, updateOnDuplicate: ['name', 'alias'], upsertKeys: ['id'] },
    )),
  )),

  down: async () => {},
};
