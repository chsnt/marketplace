const DATA = [
  {
    tableName: 'lead_needs',
    data: [
      {
        id: '9bb09a44-7a9e-43a2-a1e3-68161f98f98d',
        name: 'Бесплатная консультация',
        alias: 'FREE_CONSULTATION',
      },
      {
        id: '25ad376e-75a8-4e81-8900-1304975a6bbb',
        name: 'Покупка услуги / тарифного плана',
        alias: 'PURCHASE_SERVICE_OR_TARIFF',
      },
    ],
  },
  {
    tableName: 'lead_statuses',
    data: [
      {
        id: '726af6b4-8c4d-49e2-ab20-c7b7f65dd06b',
        name: 'Новый',
        alias: 'NEW',
      },
      {
        id: '6f04750d-47de-4b7e-aab2-de8a0e245688',
        name: 'Уточнение потребности',
        alias: 'NEED_CLARIFICATION',
      },
      {
        id: '7ab6e605-9176-4cc3-841f-9618cd4856b1',
        name: 'Потребность удовлетворена',
        alias: 'NEED_SATISFIED',
      },
      {
        id: '32505a52-b031-48ae-a45f-e50df187dd1e',
        name: 'Потребность неудовлетворена',
        alias: 'NEED_NOT_SATISFIED',
      },
    ],
  },
  {
    tableName: 'lead_consultation_results',
    data: [
      {
        id: 'e85c1792-773c-4ff1-a194-504f688b912d',
        name: 'Проблема решена',
        alias: 'PROBLEM_SOLVED',
      },
      {
        id: 'dca9c318-59c5-428c-afa4-a9755607cde5',
        name: 'Проблема не решена',
        alias: 'PROBLEM_NOT_SOLVED',
      },
      {
        id: '420d8b50-3372-452b-917e-480d7f0f46d0',
        name: 'Хочет купить',
        alias: 'WANTS_TO_BY',
      },
    ],
  },
];

export default {
  up: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all(
    DATA.map(({ tableName, data }) => queryInterface.bulkInsert(tableName, data, { transaction })),
  )),
  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all(
    DATA.map(({ tableName, data }) => queryInterface.bulkDelete(
      tableName,
      { id: data.map(({ id }) => id) },
      { transaction },
    )),
  )),
};
