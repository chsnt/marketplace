const ORDER_STATUSES_TABLE = 'order_statuses';
const now = new Date();

const ORDER_STATUSES = [
  {
    id: '7d7fcc3f-0234-435f-9985-54120ef244dd',
    name: 'Корзина',
    alias: 'CART',
    created_at: now,
    updated_at: now,
  },
  {
    id: '800cd96b-cb52-4613-a6c2-b3711c39654b',
    name: 'Оплачен',
    alias: 'PAID',
    created_at: now,
    updated_at: now,
  },
  {
    id: 'de786ca8-d642-4696-bef5-bab2437b9fd4',
    name: 'Не оплачен',
    alias: 'NOT_PAID',
    created_at: now,
    updated_at: now,
  },
];

export default {
  up: (queryInterface) => queryInterface.bulkInsert(ORDER_STATUSES_TABLE, ORDER_STATUSES),

  down: (queryInterface) => queryInterface.bulkDelete(ORDER_STATUSES_TABLE, {
    id: ORDER_STATUSES.map(({ id }) => id),
  }),
};
