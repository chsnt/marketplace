import { DeviceTypeModel } from '../models/Device';

const TABLES = {
  DEVICE_MODELS: 'device_models',
  DEVICE_PARAMETERS: 'device_parameters',
  FIELD_TYPES: 'field_types',
};

const deviceModelId = 'deb43204-0448-4cd9-8d1a-4b8681ff630c';

const FIELD_TYPES = {
  STRING: {
    id: 'cd435b53-cc70-469d-aac1-f0425753ef06',
    name: 'Текст',
    alias: 'STRING',
  },
  DATE: {
    id: '3b75c937-987f-4953-811b-73bbe311fa3f',
    name: 'Дата',
    alias: 'DATE',
  },
};

export default {
  up: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    await Promise.all([
      queryInterface.bulkInsert(TABLES.DEVICE_MODELS, [{
        id: deviceModelId,
        name: 'ПК',
        device_type_id: DeviceTypeModel.TYPES.ARM,
      }], { transaction }),
      queryInterface.bulkInsert(
        TABLES.FIELD_TYPES,
        Object.values(FIELD_TYPES),
        {
          transaction, updateOnDuplicate: ['name', 'alias'], upsertKeys: ['alias'],
        },
      ),
    ]);

    await queryInterface.bulkInsert(TABLES.DEVICE_PARAMETERS, [
      {
        device_model_id: deviceModelId,
        name: 'Производитель',
        alias: 'MANUFACTURER',
        field_type_id: FIELD_TYPES.STRING.id,
        is_required: false,
      },
      {
        device_model_id: deviceModelId,
        name: 'Модель',
        alias: 'MODEL',
        field_type_id: FIELD_TYPES.STRING.id,
        is_required: false,
      },
      {
        device_model_id: deviceModelId,
        name: 'Операционная система',
        alias: 'OPERATION_SYSTEM',
        field_type_id: FIELD_TYPES.STRING.id,
        is_required: true,
      },
    ], { transaction });
  }),
  down: async () => {}, // do nothing
};
