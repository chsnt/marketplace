export default {
  up: async (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const users = await queryInterface.sequelize.query(`
  SELECT users.id AS id
  FROM organizations
    INNER JOIN users ON organizations.id = users.organization_id AND users.login IS NULL
  WHERE (SELECT count(*) FROM users WHERE organizations.id = users.organization_id  AND users.login IS NULL) =
    (SELECT count(*) FROM users WHERE organizations.id = users.organization_id  AND users.login IS NULL 
                                  AND users.is_maintainer = false)
  LIMIT 1;
`, { type: DataTypes.QueryTypes.SELECT, transaction });

    if (!users?.length) return;

    return Promise.all(users.map((user) => queryInterface.bulkUpdate(
      'users',
      {
        is_maintainer: true,
      },
      { id: user.id },
      { transaction },
    )));
  }),
  down: async () => {},
};
