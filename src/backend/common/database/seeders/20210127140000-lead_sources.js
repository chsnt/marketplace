const TABLES = {
  DEVICE_TYPES: 'device_types',
  LEAD_SOURCES: 'lead_sources',
  LEAD_SOURCE_DEVICE_TYPE_RELATIONS: 'lead_source_device_type_relations',
};

const DATA = [
  {
    tableName: TABLES.LEAD_SOURCES,
    values: [
      {
        id: '2b9e9860-5c33-48c2-ba73-b094d21f21d5',
        name: 'Яндекс / Google',
        alias: 'YANDEX-GOOGLE',
        order: 1,
      },
      {
        id: 'fe750016-1bad-47dd-aef9-b47e7be231ab',
        name: 'Facebook / Instagram',
        alias: 'FACEBOOK-INSTAGRAM',
        order: 2,
      },
      {
        id: 'd283ff85-aa81-4ec4-80ec-85e1e586832a',
        name: 'Avito / Юла.Ру',
        alias: 'AVITO-ULA',
        order: 3,
      },
      {
        id: '1ba13e9f-dc25-4ee9-a72b-fa354c491124',
        name: 'Эвотор',
        alias: 'EVOTOR',
        order: 4,
      },
      {
        id: 'c1bf127b-43de-4708-b2af-b81fd11ec652',
        name: 'Порекомендоавали',
        alias: 'RECOMMENDATION',
        order: 5,
      },
      {
        id: '0107fc83-10c6-4e99-99e5-b22d62e39ead',
        name: 'Email рассылка',
        alias: 'EMAIL',
        order: 6,
      },
      {
        id: '6631551d-da28-48c3-8d76-bf7d7264483b',
        name: 'Прочее (указать в комментариях)',
        alias: 'OTHER',
        order: 7,
      },
    ],
  },
  {
    tableName: TABLES.LEAD_SOURCE_DEVICE_TYPE_RELATIONS,
    values: [{
      id: '2da00ae8-e492-4646-b0b3-95903a9de878',
      lead_source_id: '1ba13e9f-dc25-4ee9-a72b-fa354c491124',
      device_type_id: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
    }],
  },
];

const DEVICE_TYPE_RENAME = {
  CASHIER: {
    name: 'ККТ',
    alias: 'KKT',
  },
  COMPUTERS: {
    name: 'Поддержка АРМ',
    alias: 'ARM',
  },
};

export default {
  up: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    for (const { tableName, values } of DATA) {
      await queryInterface.bulkInsert(tableName, values, { transaction });
    }
    return Promise.all(Object.keys(DEVICE_TYPE_RENAME).map((oldAlias) => queryInterface.bulkUpdate(
      TABLES.DEVICE_TYPES,
      DEVICE_TYPE_RENAME[oldAlias],
      { alias: oldAlias },
      { transaction },
    )));
  }),
  down: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    for (const { tableName, values } of DATA) {
      await queryInterface.bulkDelete(tableName, { id: values.map(({ id }) => id) }, { transaction });
    }
  }),
};
