/* eslint-disable object-curly-newline */
/* eslint-disable max-len */
import { DeviceTypeModel } from '../models/Device';

const TABLES = {
  DEVICE_MANUFACTURERS: 'device_manufacturers',
  DEVICE_MODELS: 'device_models',
};

const MANUFACTURERS = {
  ATOL: 'ab30bf7c-c5f0-4f89-a25b-5400ef26aca3',
  AKSI: 'b439cbf6-75da-446e-ae32-0091ac7c7d21',
  DREAM: '08091c1c-4d38-4dff-a263-f23f0898f332',
  HATCH: '5e895ea9-f0da-416f-8e4e-df72976df98c',
  EVOTOR: '8f1efab2-d916-4290-8c52-88748e2ab1b5',
};

const DEVICE_MANUFACTURERS = [
  {
    id: MANUFACTURERS.ATOL,
    name: 'Атол',
  },
  {
    id: MANUFACTURERS.HATCH,
    name: 'ШТРИХ',
  },
  {
    id: MANUFACTURERS.AKSI,
    name: 'АКСИ',
  },
  {
    id: MANUFACTURERS.DREAM,
    name: 'ДримКасса',
  },
  {
    id: MANUFACTURERS.EVOTOR,
    name: 'Эвотор',
  },
];

/*
  Добавляем модели оборудования сразу с id чтоб потом было проще переносить из одной бд в другую
  Указываем уже существующие данные, потому что под них нет сидеров
*/
const DEVICE_MODELS = [
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: 'e06d062f-54dc-4b37-baaf-9aeed9b20d0f', device_manufacturer_id: MANUFACTURERS.EVOTOR, name: 'Эвотор 7.2' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '93226545-999e-4dba-b9a0-2293592a7e0c', device_manufacturer_id: MANUFACTURERS.EVOTOR, name: 'Эвотор 7.3' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '8f84e331-445c-4d33-81b3-16c492d43fdf', device_manufacturer_id: MANUFACTURERS.EVOTOR, name: 'Эвотор 10' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '1447f3dc-6260-465b-81c8-b3575468a7e4', device_manufacturer_id: MANUFACTURERS.EVOTOR, name: 'Эвотор 5' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '7f6ddf5b-08e1-4713-9729-7d5d2e25fd68', device_manufacturer_id: MANUFACTURERS.EVOTOR, name: 'Эвотор 5i' },

  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '1f994bff-27f5-48a1-9af3-8b830710daa4', device_manufacturer_id: MANUFACTURERS.ATOL, name: 'АТОЛ SIGMA 7' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: 'a7329cd3-aa99-4999-b1be-442dc5f6c298', device_manufacturer_id: MANUFACTURERS.ATOL, name: 'АТОЛ 91Ф ' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '512e4ab1-a243-49fd-8a45-5155a3e4bc41', device_manufacturer_id: MANUFACTURERS.ATOL, name: 'АТОЛ 91Ф Лайт' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '9141bb95-b970-4c78-8d80-bf40c0c164f6', device_manufacturer_id: MANUFACTURERS.ATOL, name: 'АТОЛ 92Ф ' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '03a81ea6-103c-46ce-8077-75d273e3ff53', device_manufacturer_id: MANUFACTURERS.ATOL, name: 'АТОЛ Fprint-22ПТК' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '1b72e417-d999-4c28-a5ea-015d116d572f', device_manufacturer_id: MANUFACTURERS.ATOL, name: 'АТОЛ 30Ф+' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '1bc1f5c9-ae9e-4b83-b9c2-39c59dfea315', device_manufacturer_id: MANUFACTURERS.ATOL, name: 'АТОЛ 15Ф' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '8c3c9b10-ae45-4225-85ce-a682c1cb5871', device_manufacturer_id: MANUFACTURERS.ATOL, name: 'АТОЛ 77Ф' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '7d12a443-0495-4edc-87c0-c267207d2257', device_manufacturer_id: MANUFACTURERS.ATOL, name: 'АТОЛ 11Ф' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '54efd8ca-3ace-425c-980c-627030e535f2', device_manufacturer_id: MANUFACTURERS.ATOL, name: 'АТОЛ 50Ф' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '95c12cf5-6f29-4be5-9b3f-27468a096bb4', device_manufacturer_id: MANUFACTURERS.ATOL, name: 'АТОЛ 20Ф' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '2c249c5d-0ce5-4c36-8ac7-f058c3c55b0f', device_manufacturer_id: MANUFACTURERS.ATOL, name: 'АТОЛ 25Ф' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '46419835-e6e3-4933-90fa-ccee7e6cad2e', device_manufacturer_id: MANUFACTURERS.ATOL, name: 'АТОЛ 27Ф' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: 'dbd0195b-98cb-4607-875d-3681b48c0cf7', device_manufacturer_id: MANUFACTURERS.ATOL, name: 'АТОЛ 55Ф' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: 'a83cbd9d-0530-4d96-99da-022b75c3b146', device_manufacturer_id: MANUFACTURERS.ATOL, name: 'АТОЛ 30Ф' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '9ca609e9-aa41-4b2d-8279-cc418abab0db', device_manufacturer_id: MANUFACTURERS.ATOL, name: 'АТОЛ 1Ф' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '17d630a7-c321-4319-9197-2800ee1c0a3d', device_manufacturer_id: MANUFACTURERS.ATOL, name: 'АТОЛ SIGMA 10' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '8f387677-cec7-4c80-b606-4b8dc88cbc10', device_manufacturer_id: MANUFACTURERS.ATOL, name: 'АТОЛ SIGMA 8' },

  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '67ece6c3-ac5c-4f6a-9f28-979dff22a705', device_manufacturer_id: MANUFACTURERS.HATCH, name: 'ШТРИХ-КАРТ-Ф' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: 'f490cc28-b2a3-429c-8cb7-0a7cb77808b3', device_manufacturer_id: MANUFACTURERS.HATCH, name: 'ШТРИХ-СМАРТПОС-Ф МИНИ' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '4be39277-7a58-4a5c-adb1-685fcb315ac4', device_manufacturer_id: MANUFACTURERS.HATCH, name: 'ЭЛВЕС-ФР-Ф' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: 'e4d2edf6-85b1-4f35-8020-d77f9b0c0a40', device_manufacturer_id: MANUFACTURERS.HATCH, name: 'ЭЛВЕС-МФ' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '0b2da49c-9b6d-49b1-9056-2ef0aabcb785', device_manufacturer_id: MANUFACTURERS.HATCH, name: 'ШТРИХ-MPAY-Ф' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '762e1f38-ef83-4f32-98c9-537bb131fcc2', device_manufacturer_id: MANUFACTURERS.HATCH, name: 'ШТРИХ-МИНИ-02Ф' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: 'a50970d1-2f5c-47a6-93d1-b8b864fa44d7', device_manufacturer_id: MANUFACTURERS.HATCH, name: 'ШТРИХ-М-01Ф' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: 'd828b273-7ddf-4646-92ca-b1871869ce38', device_manufacturer_id: MANUFACTURERS.HATCH, name: 'ШТРИХ-СИТИ-Ф' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: 'e8ee06ff-f1aa-4359-8a61-ace3918149cb', device_manufacturer_id: MANUFACTURERS.HATCH, name: 'ШТРИХ-ЛАЙТ-01Ф' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '16667b78-35a7-41aa-9bfd-f6a65c7abc00', device_manufacturer_id: MANUFACTURERS.HATCH, name: 'ШТРИХ-ON-LINE' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: 'f52fadca-b132-4cd5-a717-1b171f572db7', device_manufacturer_id: MANUFACTURERS.HATCH, name: 'ШТРИХ-НАНО-Ф' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '99b6e360-a454-4689-a33c-2cde539accc0', device_manufacturer_id: MANUFACTURERS.HATCH, name: 'ШТРИХ-СМАРТПОС-Ф   ' },

  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '52338fba-df27-4dfb-985e-fdfda6c14985', device_manufacturer_id: MANUFACTURERS.AKSI, name: 'АКСИ 5Ф' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: 'd5de2d86-7049-4b82-8711-09ea38315e47', device_manufacturer_id: MANUFACTURERS.AKSI, name: 'АКСИ 7Ф' },

  { device_type_id: DeviceTypeModel.TYPES.KKT, id: 'ecaa6daf-cd25-469f-a677-44f6bbfe436e', device_manufacturer_id: MANUFACTURERS.DREAM, name: 'Вики Микро' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '861013d7-a935-4056-8c12-2977f8035992', device_manufacturer_id: MANUFACTURERS.DREAM, name: 'Вики Мини' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '1882ef4f-95ef-4f0e-8312-10235c6b4b76', device_manufacturer_id: MANUFACTURERS.DREAM, name: 'Вики Принт 80 Плюс' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: 'a50efbc3-1434-4e37-984f-5a9d0190abe0', device_manufacturer_id: MANUFACTURERS.DREAM, name: 'Вики Принт 57 Плюс' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '59237d31-af90-4fb3-baf8-64c845c355ec', device_manufacturer_id: MANUFACTURERS.DREAM, name: 'Вики Принт 57' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: 'e1848d08-79aa-4658-a73a-c18695f65a3f', device_manufacturer_id: MANUFACTURERS.DREAM, name: 'Вики Классик' },
  { device_type_id: DeviceTypeModel.TYPES.KKT, id: '4108d763-571c-4705-a932-7b77790c6b46', device_manufacturer_id: MANUFACTURERS.DREAM, name: 'Вики Тауэр' },
];

export default {
  up: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.bulkInsert(TABLES.DEVICE_MANUFACTURERS, DEVICE_MANUFACTURERS, {
      transaction,
      updateOnDuplicate: ['name'],
      upsertKeys: ['id'],
    });

    return queryInterface.bulkInsert(TABLES.DEVICE_MODELS, DEVICE_MODELS, {
      transaction,
      updateOnDuplicate: ['name'],
      upsertKeys: ['id'],
    });
  }),

  down: async () => {},
};
