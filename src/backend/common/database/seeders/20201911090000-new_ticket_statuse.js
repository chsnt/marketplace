const TICKET_STATUSES_TABLE = 'ticket_statuses';

const NEW_TICKET_STATUSES = {
  id: 'b45f44b5-31c6-430b-a781-508032447940',
  name: 'Отмененy',
  alias: 'CANCELLEDD',
};

const OLD_ENGINEER_VISIT = {
  id: 'ca1eeef7-b084-4546-955a-45da4f333679',
  name: 'Выезд инженера',
  alias: 'ENGINEER_VISIT',
};

export default {
  up: (queryInterface) => queryInterface.sequelize.transaction(
    async (transaction) => {
      await queryInterface.bulkDelete(TICKET_STATUSES_TABLE, { id: OLD_ENGINEER_VISIT.id }, { transaction });
      await queryInterface.bulkInsert(TICKET_STATUSES_TABLE, [NEW_TICKET_STATUSES], { transaction });
    },
  ),

  down: (queryInterface) => queryInterface.sequelize.transaction(
    async (transaction) => {
      await queryInterface.bulkDelete(TICKET_STATUSES_TABLE, { id: NEW_TICKET_STATUSES.id }, { transaction });
      await queryInterface.bulkInsert(TICKET_STATUSES_TABLE, [OLD_ENGINEER_VISIT], { transaction });
    },
  ),
};
