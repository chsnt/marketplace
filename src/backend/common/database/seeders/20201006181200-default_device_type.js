const TABLE = 'device_types';

const ID = 'b57e113d-cb78-4313-a039-24e80d53a223';

export default {
  up: (queryInterface) => queryInterface.bulkInsert(TABLE, [
    {
      id: ID,
      name: 'Не распределенные',
      alias: 'DEAFAULT',
    },
  ]),
  down: (queryInterface) => queryInterface.bulkDelete(TABLE, { id: ID }),
};
