const TICKET_STATUSES_TABLE = 'ticket_statuses';
const now = new Date();

const TICKET_STATUSES = [
  {
    id: 'f12d1350-8375-4f5e-8833-a1c0037cbd88',
    name: 'Принята',
    alias: 'ACCEPTED',
    created_at: now,
    updated_at: now,
  },
  {
    id: 'c02e88f5-6873-4d80-9bf7-2d10f3b86507',
    name: 'В работе',
    alias: 'IN_PROGRESS',
    created_at: now,
    updated_at: now,
  },
  {
    id: '28a69c44-5c6b-4bd1-9b00-993d0434a5f5',
    name: 'Выполнена',
    alias: 'COMPLETED',
    created_at: now,
    updated_at: now,
  },
];

export default {
  up: (queryInterface) => queryInterface.bulkInsert(TICKET_STATUSES_TABLE, TICKET_STATUSES),

  down: (queryInterface) => queryInterface.bulkDelete(TICKET_STATUSES_TABLE, {
    id: TICKET_STATUSES.map(({ id }) => id),
  }),
};
