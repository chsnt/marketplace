import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class OrderOfferDocument extends DefaultModel {
    static associate({ order }) {
      this.hasMany(order);
    }
  }
  OrderOfferDocument.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
    alias: { type: DataTypes.STRING },
  }, {
    sequelize,
    modelName: 'orderOfferDocument',
  });
  return OrderOfferDocument;
};
