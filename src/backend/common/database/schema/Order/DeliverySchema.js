import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class Delivery extends DefaultModel {}
  Delivery.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false, unique: true },
    alias: { type: DataTypes.STRING, unique: true },
    cost: { type: DataTypes.FLOAT },
    data: { type: DataTypes.JSONB },
    externalId: { type: DataTypes.STRING, unique: true },
  }, {
    sequelize,
    modelName: 'delivery',
  });
  return Delivery;
};
