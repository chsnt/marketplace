import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class Order extends DefaultModel {
    static associate({
      client, orderStatus, clientTariff, clientService, payment, orderDocument, orderOfferDocument, coupon, delivery,
    }) {
      this.belongsTo(client, { foreignKey: 'client_id' });
      this.belongsTo(orderStatus);
      this.belongsTo(orderOfferDocument);
      this.belongsTo(coupon, { foreignKey: 'coupon_id' });
      this.belongsTo(delivery, { foreignKey: 'delivery_id' });
      this.hasMany(clientTariff);
      this.hasMany(clientService);
      this.hasMany(orderDocument);
      this.hasMany(payment);
    }
  }
  Order.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    number: { type: DataTypes.INTEGER, allowNull: false, autoIncrement: true },
    orderDate: { type: 'TIMESTAMP' },
    couponId: { type: DataTypes.UUID },
    discount: { type: DataTypes.INTEGER },
    deliveryId: { type: DataTypes.UUID },
    description: { type: DataTypes.STRING(1024) },
  }, {
    sequelize,
    modelName: 'order',
  });
  return Order;
};
