import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class OrderDocument extends DefaultModel {
    static associate({ order, orderDocumentType }) {
      this.belongsTo(order);
      this.belongsTo(orderDocumentType);
    }
  }
  OrderDocument.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
    externalId: { type: DataTypes.STRING },
    date: { type: DataTypes.DATEONLY },
  }, {
    sequelize,
    modelName: 'orderDocument',
  });
  return OrderDocument;
};
