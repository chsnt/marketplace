import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class Payment extends DefaultModel {
    static associate({ order, discountByCard, paymentType }) {
      this.belongsTo(order, { foreignKey: 'order_id' });
      this.belongsTo(discountByCard, { foreignKey: 'discount_by_card_id' });
      this.belongsTo(paymentType, { foreignKey: 'payment_type_id' });
    }
  }
  Payment.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    promoCode: { type: DataTypes.STRING },
    cost: { type: DataTypes.FLOAT, allowNull: false },
    totalCost: { type: DataTypes.FLOAT, allowNull: false },
    data: { type: DataTypes.JSONB },
    paymentTypeId: DataTypes.UUID,
    paymentOrderId: { type: DataTypes.STRING },
    paymentDate: { type: 'TIMESTAMP' },
    orderId: DataTypes.UUID,
    discountByCardId: DataTypes.UUID,
    number: { type: DataTypes.INTEGER, autoIncrement: true },
  }, {
    sequelize,
    modelName: 'payment',
  });
  return Payment;
};
