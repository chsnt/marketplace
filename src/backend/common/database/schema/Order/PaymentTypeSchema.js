import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class PaymentType extends DefaultModel {}

  PaymentType.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
    alias: { type: DataTypes.STRING },
  }, {
    sequelize,
    modelName: 'paymentType',
  });
  return PaymentType;
};
