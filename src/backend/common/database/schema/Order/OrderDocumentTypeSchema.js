import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class OrderDocumentType extends DefaultModel {}

  OrderDocumentType.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
    alias: { type: DataTypes.STRING },
  }, {
    sequelize,
    modelName: 'orderDocumentType',
  });
  return OrderDocumentType;
};
