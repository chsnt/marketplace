import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class TariffServiceRelation extends DefaultModel {
    static associate({
      tariff, service, limitMeasure, clientTariffService,
    }) {
      this.belongsTo(tariff);
      this.belongsTo(service);
      this.belongsTo(limitMeasure);
      this.hasMany(clientTariffService);
    }
  }
  TariffServiceRelation.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    limit: { type: DataTypes.FLOAT, allowNull: false },
  }, {
    sequelize,
    modelName: 'tariffServiceRelation',
  });
  return TariffServiceRelation;
};
