import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class LimitMeasure extends DefaultModel {}

  LimitMeasure.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
    alias: { type: DataTypes.STRING, allowNull: false },
  }, {
    sequelize,
    modelName: 'limitMeasure',
  });
  return LimitMeasure;
};
