import NomenclatureModel from '../../models/Abstract/NomenclatureModel';

export default (sequelize, DataTypes) => {
  class Service extends NomenclatureModel {
    static associate({
      deviceModelServiceRelation, tariffServiceRelation, leadServiceRelation,
      deviceManufacturer, deviceManufacturerServiceRelation, lead, cost,
      availableCatalogStatus, deviceType, discountService, serviceAddressAccountingType,
      availablePaymentMethod,
    }) {
      this.hasMany(deviceModelServiceRelation);
      this.hasMany(deviceManufacturerServiceRelation);
      this.hasMany(tariffServiceRelation);
      this.belongsToMany(deviceManufacturer, {
        as: 'deviceManufacturers',
        through: deviceManufacturerServiceRelation,
      });
      this.belongsToMany(lead, {
        as: 'leads',
        through: leadServiceRelation,
      });
      this.belongsTo(serviceAddressAccountingType, { foreignKey: 'service_address_accounting_type_id' });
      this.hasMany(cost, {
        foreignKey: 'tariff_or_service_id',
        onDelete: 'cascade',
        hooks: true,
      });
      this.belongsTo(availableCatalogStatus, { foreignKey: 'available_catalog_status_id' });
      this.belongsTo(deviceType, { foreignKey: 'device_type_id' });
      this.belongsTo(discountService, { foreignKey: 'discount_service_id' });
      this.belongsTo(availablePaymentMethod, { foreignKey: 'available_payment_method_id' });
    }
  }
  Service.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
    forTariffOnly: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
    label: { type: DataTypes.STRING },
    icon: { type: DataTypes.STRING },
    externalId: { type: DataTypes.STRING, unique: true },
    alias: { type: DataTypes.STRING, allowNull: false, unique: true },
    nameInTariff: { type: DataTypes.STRING },
    availableCatalogStatusId: { type: DataTypes.UUID },
    deviceTypeId: { type: DataTypes.UUID },
    discountServiceId: { type: DataTypes.UUID },
    serviceAddressAccountingTypeId: { type: DataTypes.UUID },
    description: { type: DataTypes.STRING(1024) },
    availablePaymentMethodId: { type: DataTypes.UUID },
  }, {
    sequelize,
    modelName: 'service',
  });
  return Service;
};
