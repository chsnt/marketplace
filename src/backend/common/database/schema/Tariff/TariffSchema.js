import NomenclatureModel from '../../models/Abstract/NomenclatureModel';

export default (sequelize, DataTypes) => {
  class Tariff extends NomenclatureModel {
    static associate({
      deviceModelTariffRelation, tariffServiceRelation, leadTariffRelation,
      service, deviceManufacturer, deviceManufacturerTariffRelation, lead,
      cost, availableCatalogStatus, deviceType, discountService, availablePaymentMethod,
    }) {
      this.hasMany(deviceModelTariffRelation);
      this.hasMany(deviceManufacturerTariffRelation);
      this.hasMany(tariffServiceRelation);
      this.belongsToMany(service, {
        as: 'services',
        through: tariffServiceRelation,
      });
      this.belongsToMany(deviceManufacturer, {
        as: 'deviceManufacturers',
        through: deviceManufacturerTariffRelation,
      });
      this.belongsToMany(lead, {
        as: 'leads',
        through: leadTariffRelation,
      });
      this.hasMany(cost, {
        foreignKey: 'tariff_or_service_id',
        onDelete: 'cascade',
        hooks: true,
      });
      this.belongsTo(availableCatalogStatus, { foreignKey: 'available_catalog_status_id' });
      this.belongsTo(deviceType, { foreignKey: 'device_type_id' });
      this.belongsTo(discountService, { foreignKey: 'discount_service_id' });
      this.belongsTo(availablePaymentMethod, { foreignKey: 'available_payment_method_id' });
    }
  }
  Tariff.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
    label: { type: DataTypes.STRING },
    icon: { type: DataTypes.STRING },
    externalId: { type: DataTypes.STRING, unique: true },
    alias: { type: DataTypes.STRING, allowNull: false, unique: true },
    expirationMonths: { type: DataTypes.SMALLINT, allowNull: false, defaultValue: 0 },
    description: { type: DataTypes.STRING },
    availableCatalogStatusId: { type: DataTypes.UUID },
    deviceTypeId: { type: DataTypes.UUID },
    discountServiceId: { type: DataTypes.UUID },
    availablePaymentMethodId: { type: DataTypes.UUID },
  }, {
    sequelize,
    modelName: 'tariff',
  });
  return Tariff;
};
