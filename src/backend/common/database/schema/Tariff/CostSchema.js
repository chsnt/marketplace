import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class Cost extends DefaultModel {}

  Cost.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    tariffOrServiceId: { type: DataTypes.UUID },
    goodCatalogId: { type: DataTypes.UUID },
    value: { type: DataTypes.FLOAT, allowNull: false },
    startAt: { type: 'TIMESTAMP', allowNull: false },
    expirationAt: { type: 'TIMESTAMP' },
  }, {
    sequelize,
    modelName: 'cost',
  });
  return Cost;
};
