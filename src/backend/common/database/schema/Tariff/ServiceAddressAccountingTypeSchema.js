import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class ServiceAddressAccountingType extends DefaultModel {}

  ServiceAddressAccountingType.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
    alias: { type: DataTypes.STRING, allowNull: false },
    order: { type: DataTypes.INTEGER, autoIncrement: true },
  }, {
    sequelize,
    modelName: 'serviceAddressAccountingType',
  });
  return ServiceAddressAccountingType;
};
