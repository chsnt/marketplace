import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class UserGroup extends DefaultModel {}

  UserGroup.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
  }, {
    sequelize,
    paranoid: false,
    updatedAt: false,
    modelName: 'userGroupRelation',
  });
  return UserGroup;
};
