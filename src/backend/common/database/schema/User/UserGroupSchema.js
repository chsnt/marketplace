import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class UserGroup extends DefaultModel {
    static associate({ user, userGroupRelation }) {
      this.belongsToMany(user, { through: userGroupRelation, as: 'users' });
    }
  }

  UserGroup.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
    alias: { type: DataTypes.STRING, allowNull: false },
  }, {
    sequelize,
    modelName: 'userGroup',
  });
  return UserGroup;
};
