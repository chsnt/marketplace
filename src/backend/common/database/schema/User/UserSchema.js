import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class User extends DefaultModel {
    static associate({
      organization, oldPasswordHash, client, userGroup, userGroupRelation,
    }) {
      this.belongsTo(organization);
      this.belongsTo(client);
      this.hasMany(oldPasswordHash);
      this.hasMany(userGroupRelation);
      this.belongsToMany(userGroup, { through: userGroupRelation, as: 'groups' });
    }
  }

  User.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    firstName: { type: DataTypes.STRING, allowNull: false },
    lastName: { type: DataTypes.STRING, allowNull: false },
    patronymicName: { type: DataTypes.STRING },
    email: { type: DataTypes.CITEXT, validate: { isEmail: true }, unique: true },
    phone: { type: DataTypes.STRING, unique: true },
    password: { type: DataTypes.STRING },
    birthday: { type: DataTypes.DATE },
    externalId: { type: DataTypes.STRING, unique: true },
    sbbidId: { type: DataTypes.STRING, unique: true },
    isLegal: { type: DataTypes.VIRTUAL, get() { return !!this.organizationId; } },
    isEmailConfirmed: { type: DataTypes.BOOLEAN, defaultValue: false },
    isMaintainer: { type: DataTypes.BOOLEAN, default: false, allowNull: true },

    profile: {
      type: DataTypes.VIRTUAL,
      get() {
        return {
          id: this.id,
          email: this.email,
          phone: this.phone,
          isLegal: this.isLegal,
          isEmailConfirmed: this.isEmailConfirmed,
        };
      },
    },
    adminProfile: {
      type: DataTypes.VIRTUAL,
      get() {
        return {
          id: this.id, email: this.email, phone: this.phone,
        };
      },
    },
    fullName: {
      type: DataTypes.VIRTUAL,
      get() {
        return `${this.lastName} ${this.firstName}${this.patronymicName ? ` ${this.patronymicName}` : ''}`;
      },
    },
  }, {
    modelName: 'user',
    sequelize,
  });

  return User;
};
