import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class SupportRequest extends DefaultModel {
    static associate({
      client, supportRequestTopic, supportRequestAttachment,
      supportRequestStatus, user, supportRequestReview,
    }) {
      this.belongsTo(client, { foreignKey: 'client_id' });
      this.belongsTo(supportRequestTopic, { foreignKey: 'support_request_topic_id' });
      this.belongsTo(supportRequestStatus, { foreignKey: 'support_request_status_id' });
      this.belongsTo(user, { as: 'responsible', foreignKey: 'responsible_id' });
      this.belongsTo(supportRequestReview, { foreignKey: 'support_request_review_id' });
      this.hasMany(supportRequestAttachment);
    }
  }
  SupportRequest.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    text: { type: DataTypes.STRING(1024), allowNull: false },
    number: { type: DataTypes.INTEGER, autoIncrement: true },
    firstName: { type: DataTypes.STRING, allowNull: false },
    lastName: { type: DataTypes.STRING },
    patronymicName: { type: DataTypes.STRING },
    phone: { type: DataTypes.STRING },
    email: { type: DataTypes.STRING },
    solution: { type: DataTypes.STRING },
    comment: { type: DataTypes.STRING(1024) },
    responsibleId: { type: DataTypes.UUID },
    supportRequestReviewId: { type: DataTypes.UUID },
    supportRequestTopicId: { type: DataTypes.UUID },
    supportRequestStatusId: { type: DataTypes.UUID },
    clientId: { type: DataTypes.UUID },
    resolvedAt: { type: 'TIMESTAMP' },
  }, {
    sequelize,
    modelName: 'supportRequest',
  });
  return SupportRequest;
};
