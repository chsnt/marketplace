import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class SupportRequestAttachment extends DefaultModel {
    static associate({ supportRequest }) {
      this.belongsTo(supportRequest);
    }
  }
  SupportRequestAttachment.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
  }, {
    sequelize,
    modelName: 'supportRequestAttachment',
  });
  return SupportRequestAttachment;
};
