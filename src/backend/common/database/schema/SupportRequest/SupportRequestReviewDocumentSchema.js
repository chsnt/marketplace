import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class SupportRequestReviewDocument extends DefaultModel {
    static associate({ supportRequestReview }) {
      this.belongsTo(supportRequestReview);
    }
  }
  SupportRequestReviewDocument.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
  }, {
    sequelize,
    modelName: 'supportRequestReviewDocument',
  });
  return SupportRequestReviewDocument;
};
