import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class SupportRequestReview extends DefaultModel {
    static associate({ supportRequest, supportRequestReviewDocument }) {
      this.hasOne(supportRequest);
      this.hasMany(supportRequestReviewDocument);
    }
  }

  SupportRequestReview.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    text: { type: DataTypes.STRING },
    mark: { type: DataTypes.SMALLINT, allowNull: false },
  }, {
    sequelize,
    modelName: 'supportRequestReview',
  });

  return SupportRequestReview;
};
