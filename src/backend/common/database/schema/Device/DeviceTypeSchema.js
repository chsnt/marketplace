import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class DeviceType extends DefaultModel {
    static associate({
      deviceModel, deviceManufacturer,
    }) {
      this.hasMany(deviceModel);
      this.belongsToMany(deviceManufacturer, {
        as: 'deviceManufacturers',
        through: deviceModel,
      });
    }
  }
  DeviceType.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
    alias: { type: DataTypes.STRING, allowNull: false },
    externalId: { type: DataTypes.STRING, unique: true },
    isSealedByUs: { type: DataTypes.BOOLEAN },
    isUsedByClient: { type: DataTypes.BOOLEAN },
    params: { type: DataTypes.JSONB },
    availableDeliveryTypes: { type: DataTypes.ARRAY(DataTypes.UUID) },
  }, {
    sequelize,
    modelName: 'deviceType',
  });
  return DeviceType;
};
