import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class DeviceManufacturerTariffRelation extends DefaultModel {
    static associate({ deviceManufacturer, service }) {
      this.belongsTo(deviceManufacturer);
      this.belongsTo(service);
    }
  }
  DeviceManufacturerTariffRelation.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
  }, {
    sequelize,
    modelName: 'deviceManufacturerServiceRelation',
  });
  return DeviceManufacturerTariffRelation;
};
