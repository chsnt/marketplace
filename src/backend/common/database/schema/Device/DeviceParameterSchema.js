import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class DeviceParameter extends DefaultModel {
    static associate({ deviceModel, fieldType, clientDeviceParameter }) {
      this.belongsTo(deviceModel);
      this.belongsTo(fieldType);
      this.hasMany(clientDeviceParameter);
    }
  }
  DeviceParameter.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
    alias: { type: DataTypes.STRING, allowNull: false },
    isRequired: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
  }, {
    sequelize,
    paranoid: false,
    modelName: 'deviceParameter',
  });
  return DeviceParameter;
};
