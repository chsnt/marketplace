import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class DeviceManufacturer extends DefaultModel {
    static associate({
      deviceModel, service, tariff, deviceManufacturerTariffRelation, deviceManufacturerServiceRelation,
    }) {
      this.hasMany(deviceModel);
      this.belongsToMany(service, {
        as: 'services',
        through: deviceManufacturerServiceRelation,
      });
      this.belongsToMany(tariff, {
        as: 'tariffs',
        through: deviceManufacturerTariffRelation,
      });
    }
  }
  DeviceManufacturer.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
    externalId: { type: DataTypes.STRING, unique: true },
  }, {
    sequelize,
    modelName: 'deviceManufacturer',
  });
  return DeviceManufacturer;
};
