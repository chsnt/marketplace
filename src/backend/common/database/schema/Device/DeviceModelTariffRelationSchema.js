import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class DeviceModelTariffRelation extends DefaultModel {
    static associate({ deviceModel, tariff }) {
      this.belongsTo(deviceModel);
      this.belongsTo(tariff);
    }
  }
  DeviceModelTariffRelation.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
  }, {
    sequelize,
    modelName: 'deviceModelTariffRelation',
  });
  return DeviceModelTariffRelation;
};
