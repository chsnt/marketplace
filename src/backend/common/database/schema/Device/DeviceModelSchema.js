import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class Device extends DefaultModel {
    static associate({
      deviceManufacturer, deviceType, clientDevice, deviceParameter,
    }) {
      this.belongsTo(deviceManufacturer);
      this.belongsTo(deviceType);
      this.hasMany(clientDevice);
      this.hasMany(deviceParameter);
    }
  }
  Device.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
    externalId: { type: DataTypes.STRING, unique: true },
  }, {
    sequelize,
    modelName: 'deviceModel',
  });
  return Device;
};
