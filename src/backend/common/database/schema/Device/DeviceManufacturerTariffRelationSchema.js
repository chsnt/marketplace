import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class DeviceManufacturerTariffRelation extends DefaultModel {
    static associate({ deviceManufacturer, tariff }) {
      this.belongsTo(deviceManufacturer);
      this.belongsTo(tariff);
    }
  }
  DeviceManufacturerTariffRelation.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
  }, {
    sequelize,
    modelName: 'deviceManufacturerTariffRelation',
  });
  return DeviceManufacturerTariffRelation;
};
