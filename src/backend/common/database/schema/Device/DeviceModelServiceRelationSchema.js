import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class DeviceModelServiceRelation extends DefaultModel {
    static associate({ deviceModel, service }) {
      this.belongsTo(deviceModel);
      this.belongsTo(service);
    }
  }
  DeviceModelServiceRelation.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
  }, {
    sequelize,
    modelName: 'deviceModelServiceRelation',
  });
  return DeviceModelServiceRelation;
};
