import DefaultModel from '../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class SystemLog extends DefaultModel {
    static associate({ user }) {
      this.belongsTo(user);
    }
  }
  SystemLog.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    event: { type: DataTypes.STRING(128), allowNull: false },
    errorMessage: { type: DataTypes.TEXT },
    errorStack: { type: DataTypes.TEXT },
    data: { type: DataTypes.TEXT, allowNull: false },
  }, {
    modelName: 'systemLog',
    sequelize,
    paranoid: false,
    updatedAt: false,
  });

  return SystemLog;
};
