import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class ElectronicDocumentCirculation extends DefaultModel {
    static associate({
      electronicDocumentCirculationDescription, organization,
    }) {
      this.hasMany(electronicDocumentCirculationDescription);
      this.hasMany(organization);
    }
  }

  ElectronicDocumentCirculation.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
    alias: { type: DataTypes.STRING, allowNull: false },
    externalId: { type: DataTypes.STRING, unique: true },
  }, {
    sequelize,
    modelName: 'electronicDocumentCirculation',
  });
  return ElectronicDocumentCirculation;
};
