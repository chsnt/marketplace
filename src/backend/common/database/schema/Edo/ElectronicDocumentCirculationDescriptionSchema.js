import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class ElectronicDocumentCirculationDescription extends DefaultModel {
    static associate({
      electronicDocumentCirculation, organization,
    }) {
      this.belongsTo(electronicDocumentCirculation);
      this.belongsTo(organization);
    }
  }

  ElectronicDocumentCirculationDescription.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    description: { type: DataTypes.STRING },
  }, {
    sequelize,
    modelName: 'electronicDocumentCirculationDescription',
  });
  return ElectronicDocumentCirculationDescription;
};
