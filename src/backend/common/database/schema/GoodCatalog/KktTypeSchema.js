import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class KktType extends DefaultModel {}
  KktType.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false, unique: true },
    alias: { type: DataTypes.STRING, unique: true },
    externalId: { type: DataTypes.STRING, unique: true },
  }, {
    sequelize,
    modelName: 'kktType',
  });
  return KktType;
};
