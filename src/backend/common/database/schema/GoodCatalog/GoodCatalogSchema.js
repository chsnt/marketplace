import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class GoodCatalog extends DefaultModel {}
  GoodCatalog.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false, unique: true },
    fullName: { type: DataTypes.STRING, unique: true },
    description: { type: DataTypes.STRING, allowNull: false, unique: true },
    numberRegistryFns: { type: DataTypes.STRING, unique: true },
    limit: { type: DataTypes.INTEGER, allowNull: false },
    deviceTypeId: { type: DataTypes.UUID },
    availableCatalogStatusId: { type: DataTypes.UUID },
    externalId: { type: DataTypes.STRING, unique: true },
    data: { type: DataTypes.JSONB },
  }, {
    sequelize,
    modelName: 'goodCatalog',
  });
  return GoodCatalog;
};
