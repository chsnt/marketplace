import DefaultModel from '../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class RequestLog extends DefaultModel {
    static associate({ user }) {
      this.belongsTo(user);
    }
  }
  RequestLog.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    url: DataTypes.TEXT,
    headers: DataTypes.TEXT,
    method: { type: DataTypes.STRING, allowNuLL: false },
    body: DataTypes.TEXT,
    params: DataTypes.TEXT,
    query: DataTypes.TEXT,
    statusCode: DataTypes.INTEGER,
    responseBody: DataTypes.TEXT,
    time: DataTypes.FLOAT,
  }, {
    modelName: 'requestLog',
    sequelize,
    paranoid: false,
    updatedAt: false,
  });

  return RequestLog;
};
