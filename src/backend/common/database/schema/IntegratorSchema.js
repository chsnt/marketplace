import DefaultModel from '../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class Integrator extends DefaultModel {}

  Integrator.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    token: { type: DataTypes.UUID, allowNull: false, unique: true },
    name: { type: DataTypes.STRING, allowNull: false, unique: true },
  }, {
    modelName: 'integrator',
    sequelize,
  });

  return Integrator;
};
