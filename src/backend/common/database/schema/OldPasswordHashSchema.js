import { getPasswordHash } from '../../utils/auth';
import DefaultModel from '../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class OldPasswordHash extends DefaultModel {
    static associate({ user }) {
      this.belongsTo(user);
    }
  }
  OldPasswordHash.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      set(value) {
        this.setDataValue('password', getPasswordHash(value));
      },
    },
  }, {
    modelName: 'oldPasswordHash',
    sequelize,
    paranoid: false,
    updatedAt: false,
  });

  return OldPasswordHash;
};
