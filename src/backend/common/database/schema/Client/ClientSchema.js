import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class Client extends DefaultModel {
    static associate({
      user, clientService, clientDevice, notification, order, supportRequest, lead, organization, clientTariff,
    }) {
      this.hasMany(user);
      this.hasMany(notification);
      this.hasMany(clientService);
      this.hasMany(clientTariff);
      this.hasMany(clientDevice);
      this.hasMany(lead);
      this.hasMany(order);
      this.hasMany(supportRequest);
      this.hasOne(organization);
    }
  }

  Client.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    number: { type: DataTypes.INTEGER, autoIncrement: true },
    maintainer: {
      type: DataTypes.VIRTUAL,
      get() {
        const users = this.organization?.users || this.users;
        return users?.find(({ isMaintainer }) => isMaintainer) || users[0];
      },
    },
    name: {
      type: DataTypes.VIRTUAL,
      get() {
        return this.organization?.name || this.maintainer?.fullName || '<Неизвестно>';
      },
    },

  }, {
    sequelize,
    modelName: 'client',
  });
  return Client;
};
