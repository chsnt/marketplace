import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class Notification extends DefaultModel {
    static associate({ client }) {
      this.belongsTo(client);
    }
  }
  Notification.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    title: { type: DataTypes.STRING, allowNull: false },
    message: { type: DataTypes.STRING(1024) },
    payload: { type: DataTypes.TEXT },
    isRead: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
  }, {
    sequelize,
    modelName: 'notification',
  });
  return Notification;
};
