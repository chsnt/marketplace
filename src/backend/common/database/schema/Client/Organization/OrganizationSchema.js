import DefaultModel from '../../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class Organization extends DefaultModel {
    static associate({
      address, user, bankDetail, client, electronicDocumentCirculationDescription,
      electronicDocumentCirculation,
    }) {
      this.belongsTo(address, { as: 'realAddress', foreignKey: 'real_address_id' });
      this.belongsTo(address, { as: 'legalAddress', foreignKey: 'legal_address_id' });
      this.belongsTo(electronicDocumentCirculation, { foreignKey: 'electronic_document_circulation_id' });
      this.belongsTo(client);
      this.hasMany(user);
      this.hasOne(bankDetail);
      this.hasOne(electronicDocumentCirculationDescription, {
        as: 'electronicDocumentCirculationDescription', foreignKey: 'organization_id',
      });
    }
  }
  Organization.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
    inn: { type: DataTypes.STRING },
    kpp: { type: DataTypes.STRING },
    realAddressId: { type: DataTypes.UUID },
    legalAddressId: { type: DataTypes.UUID },
    externalId: { type: DataTypes.STRING, unique: true },
    digitalSignatureFrom: { type: DataTypes.DATE },
    digitalSignatureTo: { type: DataTypes.DATE },
    sbbidId: { type: DataTypes.STRING, unique: true },
  }, {
    sequelize,
    modelName: 'organization',
  });
  return Organization;
};
