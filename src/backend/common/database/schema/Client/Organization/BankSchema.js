import DefaultModel from '../../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class Bank extends DefaultModel {}

  Bank.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING },
    correspondentAccount: { type: DataTypes.STRING(20) },
    rcbic: { type: DataTypes.STRING(9) },
    externalId: { type: DataTypes.STRING, unique: true },
  }, {
    sequelize,
    modelName: 'bank',
  });
  return Bank;
};
