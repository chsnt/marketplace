import DefaultModel from '../../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class BankDetail extends DefaultModel {
    static associate({
      taxType,
      organization,
      bank,
    }) {
      this.belongsTo(organization);
      this.belongsTo(taxType);
      this.belongsTo(bank);
    }
  }
  BankDetail.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    bankAccount: { type: DataTypes.STRING(20) },
  }, {
    sequelize,
    modelName: 'bankDetail',
  });
  return BankDetail;
};
