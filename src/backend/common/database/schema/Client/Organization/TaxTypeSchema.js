import DefaultModel from '../../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class TaxType extends DefaultModel {}

  TaxType.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
    alias: { type: DataTypes.STRING, allowNull: false },
    externalId: { type: DataTypes.STRING, unique: true },
  }, {
    sequelize,
    modelName: 'taxType',
  });
  return TaxType;
};
