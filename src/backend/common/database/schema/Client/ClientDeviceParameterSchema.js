import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class ClientDeviceParameter extends DefaultModel {
    static associate({ clientDevice, deviceParameter }) {
      this.belongsTo(clientDevice, { foreignKey: 'client_device_id' });
      this.belongsTo(deviceParameter, { foreignKey: 'device_parameter_id' });
    }
  }
  ClientDeviceParameter.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    value: { type: DataTypes.STRING, allowNull: false },
  }, {
    sequelize,
    modelName: 'clientDeviceParameter',
  });
  return ClientDeviceParameter;
};
