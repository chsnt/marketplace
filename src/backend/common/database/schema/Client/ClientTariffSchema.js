import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class ClientTariff extends DefaultModel {
    static associate({
      clientTariffService, tariff, order, client, address,
    }) {
      this.belongsTo(order);
      this.belongsTo(tariff, { foreignKey: 'tariff_id' });
      this.belongsTo(client, { foreignKey: 'client_id' });
      this.belongsTo(address, { as: 'serviceAddress', foreignKey: 'service_address_id' });
      this.hasMany(clientTariffService);
    }
  }
  ClientTariff.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    cost: { type: DataTypes.FLOAT, allowNull: false },
    expirationAt: { type: DataTypes.DATE, allowNull: false },
    tariffId: { type: DataTypes.UUID },
    clientId: { type: DataTypes.UUID },
    serviceAddressId: { type: DataTypes.UUID },
  }, {
    sequelize,
    modelName: 'clientTariff',
  });
  return ClientTariff;
};
