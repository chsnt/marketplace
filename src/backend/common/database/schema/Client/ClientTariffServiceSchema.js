import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class ClientTariffService extends DefaultModel {
    static associate({
      clientTariff, tariffServiceRelation, ticket, ticketClientTariffServiceRelation,
    }) {
      this.belongsTo(clientTariff, { foreignKey: 'client_tariff_id' });
      this.belongsTo(tariffServiceRelation, { foreignKey: 'tariff_service_relation_id' });
      this.belongsToMany(ticket, {
        as: 'tickets',
        through: ticketClientTariffServiceRelation,
      });
    }
  }
  ClientTariffService.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    value: { type: DataTypes.FLOAT, allowNull: false },
  }, {
    sequelize,
    modelName: 'clientTariffService',
  });
  return ClientTariffService;
};
