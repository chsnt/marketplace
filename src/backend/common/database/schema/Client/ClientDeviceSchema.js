import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class ClientDevice extends DefaultModel {
    static associate({
      address, deviceModel, client, clientDeviceParameter,
    }) {
      this.belongsTo(address, { as: 'serviceAddress', foreignKey: 'service_address_id' });
      this.belongsTo(deviceModel, { foreignKey: 'device_model_id' });
      this.belongsTo(client, { foreignKey: 'client_id' });
      this.hasMany(clientDeviceParameter);
    }
  }
  ClientDevice.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    serviceAddressId: { type: DataTypes.UUID },
    clientId: { type: DataTypes.UUID },
    serialNumber: {
      type: DataTypes.VIRTUAL,
      get() {
        return this.clientDeviceParameters?.find(
          (clientDeviceParameter) => clientDeviceParameter.deviceParameter?.alias === 'SERIAL_NUMBER',
        )?.value;
      },
    },
    armParameters: {
      type: DataTypes.VIRTUAL,
      get() {
        return this.clientDeviceParameters?.reduce((acc, clientDeviceParameter) => {
          let key = null;
          switch (clientDeviceParameter.deviceParameter.alias) {
            case 'OPERATION_SYSTEM': key = 'operationSystem'; break;
            case 'MANUFACTURER': key = 'manufacturer'; break;
            case 'MODEL': key = 'model'; break;
            default: break;
          }
          if (key) acc[key] = clientDeviceParameter.value;
          return acc;
        }, { operationSystem: null, manufacturer: null, model: null });
      },
    },
  }, {
    sequelize,
    modelName: 'clientDevice',
  });
  return ClientDevice;
};
