import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class ClientProduct extends DefaultModel {}
  ClientProduct.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    goodCatalogId: { type: DataTypes.UUID },
    orderId: { type: DataTypes.UUID },
    count: { type: DataTypes.INTEGER, allowNull: false },
  }, {
    sequelize,
    modelName: 'clientProduct',
  });
  return ClientProduct;
};
