import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class ClientService extends DefaultModel {
    static associate({
      service, client, ticket, order, address, ticketClientServiceRelation,
    }) {
      this.belongsTo(address, { as: 'serviceAddress', foreignKey: 'service_address_id' });
      this.belongsTo(service, { foreignKey: 'service_id' });
      this.belongsTo(client);
      this.belongsTo(order);
      this.belongsToMany(ticket, {
        as: 'tickets',
        through: ticketClientServiceRelation,
      });
      this.hasMany(ticketClientServiceRelation);
    }
  }
  ClientService.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    cost: { type: DataTypes.FLOAT, allowNull: false },
    serviceId: { type: DataTypes.UUID },
    serviceAddressId: { type: DataTypes.UUID },
    completed: { type: 'TIMESTAMP' },
  }, {
    sequelize,
    modelName: 'clientService',
  });
  return ClientService;
};
