import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class VClient extends DefaultModel {}
  VClient.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    number: { type: DataTypes.INTEGER },
    name: { type: DataTypes.STRING },
    type: { type: DataTypes.STRING },
    organization: { type: DataTypes.JSONB },
    individual: { type: DataTypes.JSONB },
    responsible: { type: DataTypes.JSONB },
    createdAt: { type: 'TIMESTAMP' },
  }, {
    sequelize,
    modelName: 'vClient',
    timestamps: false,
    responsibleBy: false,
  });

  return VClient;
};
