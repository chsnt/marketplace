import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class TicketClientServiceRelation extends DefaultModel {
    static associate({
      ticket, clientService,
    }) {
      this.belongsTo(clientService);
      this.belongsTo(ticket);
    }
  }
  TicketClientServiceRelation.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    clientServiceId: { type: DataTypes.UUID, allowNull: false },
    ticketId: { type: DataTypes.UUID, allowNull: false },
  }, {
    sequelize,
    modelName: 'ticketClientServiceRelation',
  });
  return TicketClientServiceRelation;
};
