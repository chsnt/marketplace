import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class TicketReview extends DefaultModel {
    static associate({ ticket, ticketReviewDocument }) {
      this.hasOne(ticket);
      this.hasMany(ticketReviewDocument);
    }
  }
  TicketReview.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    text: { type: DataTypes.STRING, allowNull: false, unique: true },
    mark: { type: DataTypes.INTEGER },
  }, {
    sequelize,
    modelName: 'ticketReview',
  });
  return TicketReview;
};
