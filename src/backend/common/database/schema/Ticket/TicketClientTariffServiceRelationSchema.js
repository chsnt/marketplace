import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class TicketClientTariffServiceRelation extends DefaultModel {
    static associate({
      ticket, clientTariffService,
    }) {
      this.belongsTo(clientTariffService);
      this.belongsTo(ticket);
    }
  }
  TicketClientTariffServiceRelation.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    clientTariffServiceId: { type: DataTypes.UUID, allowNull: false },
    ticketId: { type: DataTypes.UUID, allowNull: false },
  }, {
    sequelize,
    modelName: 'ticketClientTariffServiceRelation',
  });
  return TicketClientTariffServiceRelation;
};
