import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class Ticket extends DefaultModel {
    static associate({
      ticketStatus,
      ticketReview,
      ticketDocument,
      ticketCloseDocument,
      clientService,
      clientTariffService,
      ticketClientServiceRelation,
      ticketClientTariffServiceRelation,
      address,
      user,
      client,
    }) {
      this.belongsTo(ticketStatus, { foreignKey: 'ticket_status_id' });
      this.belongsTo(client, { foreignKey: 'client_id' });
      this.belongsTo(ticketReview, { foreignKey: 'ticket_review_id' });
      this.belongsToMany(clientService, {
        as: 'clientServices',
        through: ticketClientServiceRelation,
      });
      this.belongsToMany(clientTariffService, {
        as: 'clientTariffServices',
        through: ticketClientTariffServiceRelation,
      });
      this.belongsTo(user, { as: 'responsible', foreignKey: 'responsible_id' });
      this.hasMany(ticketDocument);
      this.hasMany(ticketCloseDocument);
      this.belongsTo(address, { as: 'serviceAddress', foreignKey: 'service_address_id' });
    }
  }
  Ticket.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    number: { type: DataTypes.INTEGER, allowNull: false, autoIncrement: true },
    solution: { type: DataTypes.TEXT },
    comment: { type: DataTypes.TEXT },
    description: { type: DataTypes.TEXT },
    serviceDeskId: { type: DataTypes.STRING },
    responsibleId: { type: DataTypes.UUID },
    ticketStatusId: { type: DataTypes.UUID },
    ticketReviewId: { type: DataTypes.UUID },
    resolvedAt: { type: 'TIMESTAMP' },
    nextCallDate: { type: 'TIMESTAMP' },
    serviceAddressId: { type: DataTypes.UUID },
    clientId: { type: DataTypes.UUID },
    laborCosts: { type: DataTypes.INTEGER },
  }, {
    sequelize,
    modelName: 'ticket',
  });
  return Ticket;
};
