import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class TicketReviewDocument extends DefaultModel {
    static associate({ ticketReview }) {
      this.belongsTo(ticketReview);
    }
  }
  TicketReviewDocument.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
  }, {
    sequelize,
    modelName: 'ticketReviewDocument',
  });
  return TicketReviewDocument;
};
