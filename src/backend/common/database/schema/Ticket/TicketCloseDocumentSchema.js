import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class TicketCloseDocument extends DefaultModel {
    static associate({ ticket }) {
      this.belongsTo(ticket);
    }
  }
  TicketCloseDocument.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
  }, {
    sequelize,
    modelName: 'ticketCloseDocument',
  });
  return TicketCloseDocument;
};
