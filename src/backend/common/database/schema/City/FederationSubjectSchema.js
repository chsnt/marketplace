import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class FederationSubject extends DefaultModel {
    static associate({ city, address }) {
      this.hasMany(city);
      this.hasMany(address);
    }
  }

  FederationSubject.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false, unique: true },
    externalId: { type: DataTypes.STRING, unique: true },
    alias: { type: DataTypes.STRING, unique: true },
  }, {
    modelName: 'federationSubject',
    sequelize,
  });

  return FederationSubject;
};
