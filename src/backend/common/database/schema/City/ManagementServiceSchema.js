import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class ManagementService extends DefaultModel {
    static associate({ city }) {
      this.hasMany(city);
    }
  }

  ManagementService.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false, unique: true },
    externalId: { type: DataTypes.STRING, unique: true },
    alias: { type: DataTypes.STRING, unique: true },
    code: { type: DataTypes.STRING, unique: true },
  }, {
    modelName: 'managementService',
    sequelize,
  });

  return ManagementService;
};
