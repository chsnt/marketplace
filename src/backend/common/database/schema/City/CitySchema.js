import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class City extends DefaultModel {
    static associate({ federationSubject, managementService, address }) {
      this.belongsTo(federationSubject);
      this.belongsTo(managementService);
      this.hasMany(address);
    }
  }

  City.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
    externalId: { type: DataTypes.STRING, unique: true },
    isTestingParticipant: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
    utcOffset: { type: DataTypes.INTEGER, defaultValue: 0, allowNull: false },
  }, {
    modelName: 'city',
    sequelize,
  });

  return City;
};
