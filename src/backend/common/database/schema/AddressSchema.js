import DefaultModel from '../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class Address extends DefaultModel {
    static associate({ city, federationSubject, clientDevice }) {
      this.belongsTo(city, { as: 'cityRelationship', foreignKey: 'city_id' });
      this.belongsTo(federationSubject);
      this.hasOne(clientDevice, { foreignKey: 'service_address_id' });
    }
  }

  Address.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    city: { type: DataTypes.STRING },
    street: { type: DataTypes.STRING },
    house: { type: DataTypes.STRING },
    apartment: { type: DataTypes.STRING },
    postalCode: { type: DataTypes.STRING },
    fiasId: { type: DataTypes.UUID },
  }, {
    sequelize,
    modelName: 'address',
  });
  return Address;
};
