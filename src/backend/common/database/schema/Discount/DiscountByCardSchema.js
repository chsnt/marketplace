import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class DiscountByCard extends DefaultModel {}

  DiscountByCard.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    value: { type: DataTypes.INTEGER, allowNull: false },
    expirationAt: { type: 'TIMESTAMP' },
  }, {
    modelName: 'discountByCard',
    sequelize,
  });

  return DiscountByCard;
};
