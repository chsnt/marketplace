import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class DiscountTemplate extends DefaultModel {
    static associate({
      coupon, discountTariffServiceRelation, discountType, discountService, tariff, service,
    }) {
      this.belongsTo(discountType, { foreignKey: 'discount_type_id' });
      this.hasMany(discountTariffServiceRelation);
      this.hasMany(coupon);
      this.hasMany(discountService);
      this.belongsToMany(tariff, {
        as: 'tariffs',
        through: discountTariffServiceRelation,
      });
      this.belongsToMany(service, {
        as: 'services',
        through: discountTariffServiceRelation,
      });
    }
  }

  DiscountTemplate.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    description: { type: DataTypes.STRING, allowNull: false },
    timeToLive: { type: DataTypes.SMALLINT },
    limit: { type: DataTypes.SMALLINT },
    discountTypeId: { type: DataTypes.UUID, allowNull: false },
    value: { type: DataTypes.INTEGER, allowNull: false, defaultValue: 0 },
  }, {
    modelName: 'discountTemplate',
    sequelize,
  });

  return DiscountTemplate;
};
