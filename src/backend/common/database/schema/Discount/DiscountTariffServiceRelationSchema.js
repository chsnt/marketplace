import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class DiscountTariffServiceRelation extends DefaultModel {
    static associate({ discountTemplate, tariff, service }) {
      this.belongsTo(discountTemplate, { foreignKey: 'discount_template_id' });
      this.belongsTo(tariff, { foreignKey: 'tariff_id' });
      this.belongsTo(service, { foreignKey: 'service_id' });
    }
  }

  DiscountTariffServiceRelation.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    discountTemplateId: { type: DataTypes.UUID, allowNull: false },
    tariffId: { type: DataTypes.UUID },
    serviceId: { type: DataTypes.UUID },
    value: { type: DataTypes.INTEGER },
  }, {
    modelName: 'discountTariffServiceRelation',
    sequelize,
  });

  return DiscountTariffServiceRelation;
};
