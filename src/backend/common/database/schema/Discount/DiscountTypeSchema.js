import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class DiscountType extends DefaultModel {
    static associate({ discountTemplate }) {
      this.hasMany(discountTemplate);
    }
  }

  DiscountType.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false, unique: true },
    alias: { type: DataTypes.STRING, allowNull: false, unique: true },
  }, {
    modelName: 'discountType',
    sequelize,
  });

  return DiscountType;
};
