import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class Coupon extends DefaultModel {
    static associate({ discountTemplate, order }) {
      this.belongsTo(discountTemplate, { foreignKey: 'discount_template_id' });
      this.hasMany(order);
    }
  }

  Coupon.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    expirationAt: { type: 'TIMESTAMP' },
    code: { type: DataTypes.STRING, allowNull: false, unique: true },
    discountTemplateId: { type: DataTypes.UUID },
  }, {
    modelName: 'coupon',
    sequelize,
  });

  return Coupon;
};
