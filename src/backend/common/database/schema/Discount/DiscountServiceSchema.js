import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class DiscountService extends DefaultModel {
    static associate({ discountTemplate, tariff, service }) {
      this.belongsTo(discountTemplate, { foreignKey: 'discount_template_id' });
      this.hasMany(tariff);
      this.hasMany(service);
    }
  }

  DiscountService.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    discountTemplateId: { type: DataTypes.UUID, allowNull: false },
    expirationAt: { type: 'TIMESTAMP' },
  }, {
    modelName: 'discountService',
    sequelize,
  });

  return DiscountService;
};
