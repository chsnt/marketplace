import DefaultModel from '../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class IntegratorLog extends DefaultModel {}

  IntegratorLog.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    status: { type: DataTypes.SMALLINT },
    resource: { type: DataTypes.STRING },
    method: { type: DataTypes.STRING },
    request: { type: DataTypes.JSONB },
    response: { type: DataTypes.JSONB },
  }, {
    sequelize,
    modelName: 'integratorLog',
  });

  return IntegratorLog;
};
