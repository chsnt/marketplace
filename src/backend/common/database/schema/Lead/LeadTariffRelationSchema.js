import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class LeadTariffRelation extends DefaultModel {
    static associate({ lead, tariff }) {
      this.belongsTo(lead);
      this.belongsTo(tariff);
    }
  }

  LeadTariffRelation.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
  }, {
    sequelize,
    modelName: 'leadTariffRelation',
  });
  return LeadTariffRelation;
};
