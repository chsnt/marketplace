import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class LeadServiceRelation extends DefaultModel {
    static associate({ lead, service }) {
      this.belongsTo(lead);
      this.belongsTo(service);
    }
  }

  LeadServiceRelation.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
  }, {
    sequelize,
    modelName: 'leadServiceRelation',
  });
  return LeadServiceRelation;
};
