import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class LeadStatus extends DefaultModel {}

  LeadStatus.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false },
    alias: { type: DataTypes.STRING, allowNull: false },
    priority: { type: DataTypes.SMALLINT },
  }, {
    sequelize,
    modelName: 'leadStatus',
  });
  return LeadStatus;
};
