import DefaultModel from '../../models/Abstract/DefaultModel';
import dbTemplates from '../../../utils/dbTemplates';

export default (sequelize, DataTypes) => {
  class Lead extends DefaultModel {
    static associate({
      client, city, deviceManufacturer, deviceModel,
      deviceType, leadNeed, leadStatus, leadConsultationResult, leadSource,
      service, tariff, leadServiceRelation, leadTariffRelation,
    }) {
      this.belongsTo(client, { foreignKey: 'client_id' });
      this.belongsTo(city, { foreignKey: 'city_id' });
      this.belongsTo(deviceType, { foreignKey: 'device_type_id' });
      this.belongsTo(deviceManufacturer, { foreignKey: 'device_manufacturer_id' });
      this.belongsTo(deviceModel, { foreignKey: 'device_model_id' });
      this.belongsTo(leadNeed, { foreignKey: 'lead_need_id' });
      this.belongsTo(leadStatus, { foreignKey: 'lead_status_id' });
      this.belongsTo(leadSource, { foreignKey: 'lead_source_id' });
      this.belongsTo(leadConsultationResult, { foreignKey: 'lead_consultation_result_id' });
      this.belongsToMany(service, {
        as: 'services',
        through: leadServiceRelation,
      });
      this.belongsToMany(tariff, {
        as: 'tariffs',
        through: leadTariffRelation,
      });
    }
  }

  Lead.init({
    id: dbTemplates(DataTypes).id,
    number: dbTemplates(DataTypes).number,
    firstName: { type: DataTypes.STRING, allowNull: false },
    patronymicName: { type: DataTypes.STRING },
    lastName: { type: DataTypes.STRING },
    phone: { type: DataTypes.STRING, allowNull: false },
    email: { type: DataTypes.STRING },
    comment: { type: DataTypes.TEXT },
    leadNeedId: { type: DataTypes.UUID, allowNull: false },
    leadStatusId: { type: DataTypes.UUID, allowNull: false },
    deviceManufacturerId: { type: DataTypes.UUID },
    deviceModelId: { type: DataTypes.UUID },
    cityId: { type: DataTypes.UUID },
    leadConsultationResultId: { type: DataTypes.UUID },
    clientId: { type: DataTypes.UUID },
    deviceTypeId: { type: DataTypes.UUID },
    leadSourceId: { type: DataTypes.UUID },
    fullName: {
      type: DataTypes.VIRTUAL,
      get() {
        // eslint-disable-next-line max-len
        return `${this.lastName ? `${this.lastName} ` : ''}${this.firstName}${this.patronymicName ? ` ${this.patronymicName}` : ''}`;
      },
    },
    nextCallDate: { type: 'TIMESTAMP' },
    formId: DataTypes.STRING,
    operationSystem: DataTypes.STRING,
    isOrganization: DataTypes.BOOLEAN,
  }, {
    modelName: 'lead',
    sequelize,
  });

  return Lead;
};
