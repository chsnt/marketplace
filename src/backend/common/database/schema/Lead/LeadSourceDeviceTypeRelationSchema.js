import DefaultModel from '../../models/Abstract/DefaultModel';

export default (sequelize, DataTypes) => {
  class LeadSourceDeviceTypeRelation extends DefaultModel {
    static associate({ leadSource, deviceType }) {
      this.belongsTo(leadSource);
      this.belongsTo(deviceType);
    }
  }

  LeadSourceDeviceTypeRelation.init({
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
  }, {
    sequelize,
    modelName: 'leadSourceDeviceTypeRelation',
  });
  return LeadSourceDeviceTypeRelation;
};
