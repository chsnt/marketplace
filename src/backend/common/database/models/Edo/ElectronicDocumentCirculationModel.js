import db from '../../index';

export default class ElectronicDocumentCirculationModel
  extends db.models.electronicDocumentCirculation {
  static STATUSES = {
    NOT_IN_LIST: 'abe383da-dd10-4944-8ca7-96b5c55b7c18',
    EDO_IS_MISSING: '79f09207-352e-4ae0-86dc-bd77ee0086b3',
  }

  static checkEDOExists(id) {
    return id !== this.STATUSES.EDO_IS_MISSING;
  }
}
