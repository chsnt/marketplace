import db from '../../index';

export default class ElectronicDocumentCirculationDescriptionModel
  extends db.models.electronicDocumentCirculationDescription {}
