import bcrypt from 'bcrypt';

import { Sequelize, UniqueConstraintError } from 'sequelize';
import db from '../..';
import HelperService from '../../../services/HelperService';
import { getPasswordHash } from '../../../utils/auth';

import APIError from '../../../utils/APIError';
import ERRORS from '../../../constants/errors';

export default class UserModel extends db.models.user {
  /* Overrides */
  async save(options) {
    if (this.isNewRecord && !options.withoutPassword) this.hashPassword();
    return super.save(options);
  }

  async update(values, options) {
    return super.update({
      ...values,
      ...(values.password && { password: getPasswordHash(values.password) }),
    }, options);
  }

  /* Custom instance methods */
  hashPassword() {
    if (!this.isNewRecord) return;
    this.set({ password: getPasswordHash(this.password) });
  }

  authenticate(password) {
    // сделать проверку в редисе
    return bcrypt.compareSync(password, this.password);
  }

  static async setPassword(userId, password, transaction) {
    this.update({ password: getPasswordHash(this.password) }, {
      where: { id: userId },
      transaction,
    });
  }

  /* Custom model methods */
  static async findByEmailOrPhoneOrLogin(emailOrPhone, options) {
    return UserModel.findOne({
      where: {
        [db.Sequelize.Op.or]: [
          { email: emailOrPhone },
          { login: emailOrPhone },
          { phone: HelperService.normalizePhone(emailOrPhone) },
        ],
      },
      ...options,
    });
  }

  static uniqueConstraintError = (constraintKey) => {
    const baseErrorObject = ERRORS.ENTITY.UNIQUE_CONSTRAINT();
    let { error } = baseErrorObject;
    switch (constraintKey) {
      case 'users_email_key':
        error = 'Пользователь с таким email уже существует.';
        break;
      case 'users_phone_key':
        error = 'Пользователь с таким номером телефона уже существует.';
        break;
      default:
        break;
    }
    return {
      ...baseErrorObject, error,
    };
  }

  static getSearchAttribute({ value, preKey }) {
    const preKeyString = preKey ? `${preKey}.` : '';
    return Sequelize.where(
      Sequelize.fn(
        'concat',
        Sequelize.col(`${preKeyString}first_name`),
        Sequelize.col(`${preKeyString}patronymic_name`),
        Sequelize.col(`${preKeyString}last_name`),
      ),
      { [Sequelize.Op.iLike]: `%${value}%` },
    );
  }

  static async registerUser({ user, transaction }) {
    const localTransaction = transaction || await db.sequelize.transaction();
    try {
      const userInstance = await UserModel.create(user, {
        transaction: localTransaction,
        include: 'userGroupRelations',
        returning: true,
      });

      /* Проставляем пароль в таблицу старых паролей */
      await userInstance.createOldPasswordHash({ password: user.password }, { transaction: localTransaction });
      if (!transaction) await localTransaction.commit();
      return userInstance;
    } catch (error) {
      if (!transaction) await localTransaction.rollback();
      if (error instanceof UniqueConstraintError) {
        const errorObject = UserModel.uniqueConstraintError(error?.original?.constraint);
        throw new APIError(errorObject);
      }
      if (!(error instanceof APIError)) {
        throw new APIError(
          ERRORS.USER.REGISTRATION_ERROR, { originalError: error },
        );
      }
      throw error;
    }
  }
}
