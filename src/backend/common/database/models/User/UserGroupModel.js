import db from '../..';

export default class UserGroupModel extends db.models.userGroup {
  static GROUPS = {
    ADMIN: '45f54fd5-28af-40ec-8c4c-a516cbbeb2dd',
    SALES_MANAGER: '463cae5d-b6e6-4349-8849-22a0dce46e88',
    TECHNICAL_SUPPORT: 'd33ea5be-7a87-4f41-acac-9d4958e5d243',
    PROJECT_COMMITTEE: '8fa7a6ee-ad01-451b-8485-22f7e16a0166',
    OPERATOR: 'e0645386-7de3-43e6-9720-06a21ab83868',
  }

  static ACCESS_TO_ADMIN_PANEL = [
    this.GROUPS.ADMIN,
    this.GROUPS.SALES_MANAGER,
    this.GROUPS.OPERATOR,
    this.GROUPS.PROJECT_COMMITTEE,
    this.GROUPS.TECHNICAL_SUPPORT,
  ]
}
