import db from '../..';

export default class PaymentModel extends db.models.payment {
  static getCurrentPayment = (payments = []) => payments.find((payment) => payment.paymentDate);
}
