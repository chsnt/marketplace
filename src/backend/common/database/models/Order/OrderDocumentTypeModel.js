import db from '../..';

export default class OrderDocumentTypeModel extends db.models.orderDocumentType {
  static TYPES ={
    OFFER: 'd045b659-7914-4bf6-a100-9523e64b3428',
    INVOICE: 'ee19de7a-41f5-428e-aa6b-1611683e706f',
    UTD_STATUS_ONE: '0d019524-ef4e-4742-902c-1f7aea6eb554',
    UTD_STATUS_TWO: '5bc13be2-a0b5-4cf1-8be4-bdc385442830',
  }
}
