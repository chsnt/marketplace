import db from '../..';

export default class OrderStatusModel extends db.models.orderStatus {
  static STATUSES = {
    CART: '7d7fcc3f-0234-435f-9985-54120ef244dd',
    PAID: '800cd96b-cb52-4613-a6c2-b3711c39654b',
    NOT_PAID: 'de786ca8-d642-4696-bef5-bab2437b9fd4',
  }
}
