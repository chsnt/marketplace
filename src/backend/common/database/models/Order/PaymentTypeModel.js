import db from '../..';

export default class PaymentTypeModel extends db.models.paymentType {
  static TYPES ={
    CARD: { id: '9ae39995-8757-43b9-8943-325b0a49dc30', alias: 'CARD', name: 'По карте' },
    INVOICE: { id: '8e076648-735b-42f2-8cd8-99dd8de36986', alias: 'INVOICE', name: 'По счету' },
  }
}
