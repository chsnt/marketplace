import db from '../..';

export default class ServiceAddressAccountingTypeModel extends db.models.serviceAddressAccountingType {
  static TYPES = {
    IN_ORDER: '84db3034-1068-47f7-a8f3-1fba5790d861',
    IN_TICKET: '161f4f19-9f4f-4af9-8a2e-cad5e76e7449',
    NOT_REQUESTED: '36aae030-ef17-462f-b613-c9c3cbb1e90c',
  };

  static NAMES = {
    '84db3034-1068-47f7-a8f3-1fba5790d861': 'inOrder',
    '161f4f19-9f4f-4af9-8a2e-cad5e76e7449': 'inTicket',
    '36aae030-ef17-462f-b613-c9c3cbb1e90c': 'notRequested',
  }
}
