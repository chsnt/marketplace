import db from '../..';

export default class AvailableCatalogStatusModel extends db.models.availableCatalogStatus {
  static STATUSES = {
    ALL: 'd4c9ac21-279e-48c4-8064-2349dfa50001',
    SITE: 'b6a19d76-9906-4b9f-8493-9ec1aeb1fb43',
    ADMIN: 'b7140b0f-1df1-48f5-a955-7598eb20c63c',
  }
}
