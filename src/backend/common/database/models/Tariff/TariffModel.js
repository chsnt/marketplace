import ServiceAddressAccountingTypeModel from './ServiceAddressAccountingTypeModel';
import db from '../..';

export default class TariffModel extends db.models.tariff {
  static async mustTariffHasServiceAddress(services) {
    const types = new Set(services.map(({ serviceAddressAccountingTypeId }) => serviceAddressAccountingTypeId));

    return types.has(ServiceAddressAccountingTypeModel.TYPES.IN_ORDER);
  }
}
