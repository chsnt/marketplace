import db from '../..';

export default class LimitMeasureModel extends db.models.limitMeasure {
  static STATUSES = {
    PERIOD_OF_SERVICE: '895f6745-f9eb-4d73-ba8c-4a545ffa0a7b',
    COUNT_PER_MONTH: '91aa207f-ad87-4332-a35f-063ed9cb7bc4',
  }
}
