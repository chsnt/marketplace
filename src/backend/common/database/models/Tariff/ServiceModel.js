import db from '../..';

export default class ServiceModel extends db.models.service {
  static SERVICES = {
    ENGINEER_VISIT: '20c5bd1a-ea92-48b2-8f13-6c83e6aaf4b5',
    EMERGENCY_ENGINEER_VISIT: 'a4e9ffe6-4190-4c2e-9496-54844829cc0f',
  };
}
