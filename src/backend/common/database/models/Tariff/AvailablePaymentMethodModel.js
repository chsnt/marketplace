import db from '../..';

export default class AvailablePaymentMethodModel extends db.models.availablePaymentMethod {
  static STATUSES = {
    ALL: '86536609-fb7d-4831-846a-d32fd581978a',
    CARD: '8871a1bc-c482-42ef-b5b3-3e66f5c73771',
    INVOICE: '87cb562f-d5c7-4650-bcec-5429cbacc2cb',
  }
}
