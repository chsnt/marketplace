import db from '../../..';
import ERRORS from '../../../../constants/errors';
import APIError from '../../../../utils/APIError';

export default class OrganizationModel extends db.models.organization {
  /**
   * @async
   * @param {String} inn
   * @param {String} [kpp]
   * @return {Object.<OrganizationModel>}
   * @description - Для ЮЛ должна быть уникальна связка: ИНН + КПП. КПП допускается у других организаций.КПП могут повторяться для разных ЮЛ с разными ИНН. А к одному ИНН могут быть привязаны несколько разных КПП.
   *              - Для ИП должны быть уникальным только: ИНН
   */
  static findByRequisites({
    inn = '', kpp = '',
  }) {
    return OrganizationModel.findOne({
      where: {
        inn,
        ...(kpp && { kpp }),
      },
    });
  }

  static async registerOrganization({
    organization: {
      legalAddress, realAddress, clientId,
      isSameAddress = false,
      electronicDocumentCirculationId,
      electronicDocumentCirculationDescription,
      bankDetail,
      ...organization
    },
    transaction,
  }) {
    const localTransaction = transaction || await db.sequelize.transaction();
    try {
      /* Проверка на уникальность */
      const existedOrganizationInstance = await this.findByRequisites(organization);

      if (existedOrganizationInstance) throw new APIError(ERRORS.ORGANIZATION.ORGANIZATION_EXISTS);

      const organizationInstance = await this.create({
        ...organization,
        bankDetail: {
          ...bankDetail,
          bank: {
            ...bankDetail,
          },
        },
        clientId,
        legalAddress,
        ...(isSameAddress ? { realAddress: legalAddress } : { realAddress }),
        electronicDocumentCirculationId,
        ...(electronicDocumentCirculationDescription && {
          electronicDocumentCirculationDescription: {
            electronicDocumentCirculationId,
            description: electronicDocumentCirculationDescription,
          },
        }),
      }, {
        transaction: localTransaction,
        returning: true,
        include: [
          'legalAddress', 'realAddress', 'electronicDocumentCirculationDescription',
          { association: 'bankDetail', include: ['bank'] },
        ],
      });

      if (!transaction) await localTransaction.commit();

      return organizationInstance;
    } catch (error) {
      if (!transaction) await localTransaction.rollback();
      if (!(error instanceof APIError)) {
        throw new APIError(
          ERRORS.ORGANIZATION.REGISTRATION_ERROR, { originalError: error },
        );
      }
      throw error;
    }
  }
}
