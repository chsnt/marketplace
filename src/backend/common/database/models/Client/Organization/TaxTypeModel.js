import db from '../../..';

export default class TaxTypeModel extends db.models.taxType {
  static TYPES = {
    OSNO: 'ad24d66f-eda3-4844-a1de-1c6d055bb4b8',
    USN: '9f10486d-5f51-4771-922f-c562ed333b36',
  }
}
