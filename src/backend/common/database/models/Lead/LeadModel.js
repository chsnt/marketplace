import db from '../..';

export default class LeadModel extends db.models.lead {
  static LEAD_FORMS = {
    CALLBACK_MAIN: 'Главная',
    CALLBACK_ASSISTANCE: 'Тариф "Удаленная помощь"',
    CALLBACK_ASSISTANCEPLUS: 'Тариф "Удаленная помощь+"',
    CALLBACK_ALLINCLUSIVE: 'Тариф "Все включено"',
    CALLBACK_ZAMENAFN: 'Замена ФН',
    CALLBACK_REGKASSA: 'Регистрация онлайн-кассы',
    CALLBACK_PEREGKASSA: 'Перерегистрация онлайн-кассы',
    CALLBACK_SNYATIEKASSA: 'Снятие онлайн-кассы с учета',
    CALLBACK_ETSPKASSA: 'Получение ЭЦП',
    CALLBACK_ZAKLOFD: 'Заключение договора с офд',
    CALLBACK_OBNOVLPOKASSA: 'Обновление ПО',
    ADMIN: 'Панель администратора',
    REGISTER: 'Регистрация в момент покупки',
    UNDEFINED_FORM: 'Неизвестная форма создания лида',
  }

  get form() {
    if (!this.formId) return LeadModel.LEAD_FORMS.UNDEFINED_FORM;

    /* Некоторые id формы содержат порядковые номера на странице
    Для определения формы используем includes */
    const formKey = Object.keys(LeadModel.LEAD_FORMS).find((key) => this.formId.includes(key));

    return LeadModel.LEAD_FORMS[formKey] || LeadModel.LEAD_FORMS.UNDEFINED_FORM;
  }
}
