import db from '../..';

export default class LeadStatusModel extends db.models.leadStatus {
  static STATUSES = {
    NEW: '726af6b4-8c4d-49e2-ab20-c7b7f65dd06b',
    READY_FOR_SALES: 'e26776e5-a6f3-4e1c-913b-9f7b9cf0413a',
    NEED_CLARIFICATION: '6f04750d-47de-4b7e-aab2-de8a0e245688',
    NEED_SATISFIED: '7ab6e605-9176-4cc3-841f-9618cd4856b1',
    NEED_NOT_SATISFIED: '32505a52-b031-48ae-a45f-e50df187dd1e',
    AWAITING_DOCUMENTS: 'c6017f97-dc9c-4ca2-8eae-4e9affc8f11c',
  };
}
