import db from '../..';

export default class LeadNeedModel extends db.models.leadNeed {
  static NEEDS = {
    FREE_CONSULTATION: '9bb09a44-7a9e-43a2-a1e3-68161f98f98d',
    PURCHASE_SERVICE_OR_TARIFF: '25ad376e-75a8-4e81-8900-1304975a6bbb',
  }
}
