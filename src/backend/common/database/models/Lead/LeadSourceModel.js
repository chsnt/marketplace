import db from '../..';

export default class LeadSourceModel extends db.models.leadSource {
  static SOURCES = {
    'YANDEX-GOOGLE': '2b9e9860-5c33-48c2-ba73-b094d21f21d5',
    'FACEBOOK-INSTAGRAM': 'fe750016-1bad-47dd-aef9-b47e7be231ab',
    'AVITO-ULA': 'd283ff85-aa81-4ec4-80ec-85e1e586832a',
    EVOTOR: '1ba13e9f-dc25-4ee9-a72b-fa354c491124',
    RECOMMENDATION: 'c1bf127b-43de-4708-b2af-b81fd11ec652',
    EMAIL: '0107fc83-10c6-4e99-99e5-b22d62e39ead',
    OTHER: '6631551d-da28-48c3-8d76-bf7d7264483b',
  }
}
