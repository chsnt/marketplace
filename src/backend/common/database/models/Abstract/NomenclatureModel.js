import { Op } from 'sequelize';
import DefaultModel from './DefaultModel';

export default class NomenclatureModel extends DefaultModel {
  static includeCost = (forCurrentTime) => {
    const timeIsNow = new Date();

    return {
      association: 'costs',
      attributes: ['value', 'startAt', 'expirationAt'],
      required: true,
      ...(forCurrentTime && {
        where: {
          [Op.and]: {
            startAt: {
              [Op.lte]: timeIsNow,
            },
            expirationAt: {
              [Op.or]: [{
                [Op.gt]: timeIsNow,
              }, {
                [Op.is]: null,
              }],
            },
          },
        },
      }),
    };
  }

  static getCurrentCost = (costs = []) => ((costs[0]?.value === undefined) ? null : costs[0]?.value);
}
