import Sequelize, { Op } from 'sequelize';
import httpContext from 'express-http-context';

import asyncPool from '../../../libs/asyncPools';


import db from '../../index';

export default class extends Sequelize.Sequelize.Model {
  /*
    Метод апсерта данных с предварительным удалением неиспользуемых записей
    Потребуется для импорта справочников
   */
  static async bulkUpsertRemoveObsolete(records, { returnOnlyInstances, ...options } = {}) {
    const transaction = options?.transaction || await db.sequelize.transaction();
    try {
      /* Колонка для проверки наличия записи в таблице */
      const columnToCheck = options?.columnToCheck || 'externalId';

      /* Удаляем записи, которых нет во входящих записях */
      await this.destroy({
        where: {
          [columnToCheck]: { [Op.notIn]: records.map((row) => row[columnToCheck]) },
        },
        transaction,
      });

      /* Выполняем upsert по входящим строкам и возвращаем результат */
      const result = await asyncPool(records, (record) => this.upsert(record, {
        ...options,
        transaction,
        returning: options?.returning || !!returnOnlyInstances,
      }));

      if (!options?.transaction) await transaction.commit();

      /*
        По-умолчанию upsert возвращает ответ вида boolean | [instance, boolean]
        Для удобства этот флаг позволяет вернуть сразу инстенсы записей, без флага
      */
      if (!returnOnlyInstances) return result;

      return result.map(([instance]) => instance);
    } catch (error) {
      if (!options?.transaction) await transaction.rollback();
      throw error;
    }
  }

  static async bulkCreate(records, options = {}) {
    const user = httpContext.get('user');
    /* проставим автора записи */
    const result = super.bulkCreate(records.map((item) => ({
      ...item,
      created_by: user,
      updated_by: user,
    })), options);

    return result;
  }

  /* Методы инстенса */
  async save(options = {}) {
    // before save

    // На каждый вызов сейва принудительно обновляем поле updated_at
    if (Object.keys(this.dataValues).includes('updated_at')) this.changed('updated_at', true);

    this.setCreatedByUpdatedBy();

    const result = await super.save(options);

    return result;
  }

  setCreatedByUpdatedBy() {
    const user = httpContext.get('user');
    if (!user) return;
    this.set({ updated_by: user, ...(this.isNewRecord && { created_by: user }) });
  }
}
