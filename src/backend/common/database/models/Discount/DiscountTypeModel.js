import db from '../..';

export default class DiscountTypeModel extends db.models.discountType {
  static TYPES = {
    DISCOUNT: 'c764b9a8-45ec-44de-bba7-b68c580d478b',
    FIXED_PRICE: '9d879343-6d34-45e7-9467-1dcd13c23dd0',
  }
}
