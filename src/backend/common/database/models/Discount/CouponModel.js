import db from '../..';
import ERRORS from '../../../constants/errors';

export default class CouponModel extends db.models.coupon {
  static uniqueConstraintError = (constraintKey) => {
    const baseErrorObject = ERRORS.ENTITY.UNIQUE_CONSTRAINT();
    const descriptionErrors = {
      coupons_cod_key: {
        error: 'Купон с таким названием уже существует',
        errorCode: `CouponCode${baseErrorObject.errorCode}`,
      },
    };
    return descriptionErrors[constraintKey];
  }
}
