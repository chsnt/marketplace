import bcrypt from 'bcrypt';
import db from '..';

import { USED_PASSWORDS_LIMIT } from '../../constants/env';


export default class OldPasswordHashModel extends db.models.oldPasswordHash {
  /**
   * @async
   * @param {String} userId UUID пользователя
   * @param {String} password проверяемый пароль
   * @returns {Promise<boolean>}
   * @description Если пароля нет в последних {USED_PASSWORDS_LIMIT} записях пользователя, его можно использовать
   */
  static async isPasswordAlreadyUsed({ userId, password, transaction }) {
    const hashInstances = await OldPasswordHashModel.findAll({
      where: { userId },
      order: [['created_at', 'desc']],
      limit: USED_PASSWORDS_LIMIT,
      attributes: ['password'],
      transaction,
    });

    return hashInstances.reduce((isUsed, instance) => {
      if (isUsed) return isUsed;
      return instance.comparePasswords(password);
    }, false);
  }

  comparePasswords(password) {
    return bcrypt.compareSync(password, this.password);
  }
}
