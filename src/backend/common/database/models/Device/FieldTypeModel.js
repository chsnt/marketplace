import db from '../..';

export default class FieldTypeModel extends db.models.fieldType {
  static TYPES = {
    STRING: 'cd435b53-cc70-469d-aac1-f0425753ef06',
    DATE: '3b75c937-987f-4953-811b-73bbe311fa3f',
  }
}
