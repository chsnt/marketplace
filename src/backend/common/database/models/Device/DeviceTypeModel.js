import db from '../..';

export default class DeviceTypeModel extends db.models.deviceType {
  static TYPES = {
    KKT: 'e94e55e1-28f5-48cf-881c-779ddf5d31ce',
    ARM: '14eeadde-dbcb-44ac-9d72-e1cc11617c98',
    DEFAULT: 'b57e113d-cb78-4313-a039-24e80d53a223',
  }

  static NAMES = {
    'e94e55e1-28f5-48cf-881c-779ddf5d31ce': 'kkt',
    '14eeadde-dbcb-44ac-9d72-e1cc11617c98': 'arm',
    'b57e113d-cb78-4313-a039-24e80d53a223': 'default',
  }
}
