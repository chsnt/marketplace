import db from '..';

export default class AddressModel extends db.models.address {
  /**
   *
   * @param {Boolean} [paranoidAddress]
   * @param {Boolean} [separateClientDeviceParameters]
   * @return {{include: [{association: string, attributes: [string, string, string]}, {association: string, attributes: [string, string]}, {include: [{include: {association: string, attributes: [string, string]}, separate: boolean, association: string, attributes: [string]}, {include: [{association: string, attributes: [string, string]}, {association: string, attributes: [string]}], association: string, attributes: [string]}], association: string, attributes: [string]}], association: string, attributes: string[], paranoid: boolean}}
   */
  static addressAndDevice = ({ paranoidAddress = true, separateClientDeviceParameters = false } = {}) => ({
    association: 'serviceAddress',
    attributes: ['id', 'street', 'house', 'apartment', 'postalCode', 'fiasId'],
    paranoid: paranoidAddress,
    include: [
      {
        association: 'cityRelationship',
        attributes: ['id', 'name', 'utcOffset'],
      },
      {
        association: 'federationSubject',
        attributes: ['id', 'name'],
      },
      {
        association: 'clientDevice',
        paranoid: false,
        attributes: ['id'],
        include: [
          {
            association: 'clientDeviceParameters',
            separate: separateClientDeviceParameters,
            paranoid: false,
            attributes: ['value'],
            include: {
              association: 'deviceParameter',
              attributes: ['name', 'alias'],
            },
          },
          {
            association: 'deviceModel',
            attributes: ['name'],
            include: [
              {
                association: 'deviceType',
                attributes: ['name', 'alias'],
              },
              {
                association: 'deviceManufacturer',
                attributes: ['name'],
              },
            ],
          },
        ],
      },
    ],
  });
}
