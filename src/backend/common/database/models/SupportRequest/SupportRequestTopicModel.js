import db from '../..';

export default class SupportRequestTopicsModel extends db.models.supportRequestTopic {
  static TOPICS = {
    WORKING_WITH_SITE: '2426d252-40ca-4f01-8d79-93c6cda61ac3',
    GUARANTEE_AND_REFUND: '009f274c-2e09-49af-86d4-fe90d3d59bc1',
    DOCUMENTS_AND_LEGAL: '1845e775-4721-4b26-af19-d3a699df388c',
    ETC: 'ac9555dd-e258-41e8-886a-245aaf0af44e',
  }
}
