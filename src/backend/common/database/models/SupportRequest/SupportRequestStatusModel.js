import db from '../..';

export default class SupportRequestStatusModel extends db.models.supportRequestStatus {
  static STATUSES = {
    NEW: '708ae989-0f82-4371-9e3c-33463eb960e4',
    IN_PROGRESS: 'e8d914c9-76a0-4689-9b23-5daf3f6d3a9b',
    UPDATE_INFORMATION: '8b2fe441-05b2-448d-aefa-811e20cbbaa2',
    SOLVED: '5cccbe60-118d-4d1f-8d2d-42822ccc08c9',
  }
}
