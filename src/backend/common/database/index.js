import path from 'path';
import Sequelize from 'sequelize';
import Umzug from 'umzug';

import FileService from '../services/FileService';
import logger from '../services/LoggerService'; // eslint-disable-line import/no-cycle

const models = {};

const sequelize = new Sequelize(process.env.DATABASE_URL, {
  seederStorage: 'sequelize',
  dialect: 'postgres',
  define: { underscored: true, paranoid: true },
  logging: false,
  minifyAliases: true,
});

FileService.getFiles(path.resolve(__dirname, './schema')).forEach((file) => {
  // eslint-disable-next-line
  const modelFactory = require(file);
  const model = modelFactory.default(sequelize, Sequelize.DataTypes);
  models[model.name] = model;
});

export const IGNORE_CBUB_ASSOCIATION_TABLES = [
  'sequelize_meta',
  'sequelize_data',
  'system_logs',
  'request_logs',
  'user_group_relations',
  'v_clients',
];

Object.keys(models).forEach((modelName) => {
  const currentModel = models[modelName];
  if (currentModel.associate) {
    currentModel.associate(models);
  }

  /* Если у таблицы есть поля created_by и updated_by */
  if (!IGNORE_CBUB_ASSOCIATION_TABLES.includes(currentModel.tableName)) {
    currentModel.belongsTo(models.user, { as: 'createdBy', foreignKey: 'created_by' });
    currentModel.belongsTo(models.user, { as: 'updatedBy', foreignKey: 'updated_by' });
  }
});

const umzugConfig = {
  storage: 'sequelize',
  storageOptions: { sequelize },
  migrations: {
    params: [sequelize.getQueryInterface(), sequelize.constructor],
    path: path.resolve(__dirname, './migrations'),
    traverseDirectories: true,
  },
};

const umzug = {
  migrations: new Umzug(umzugConfig),
  seeders: new Umzug({
    ...umzugConfig,
    storageOptions: { sequelize, tableName: 'sequelize_data', modelName: 'SequelizeData' },
    migrations: {
      ...umzugConfig.migrations,
      path: path.resolve(__dirname, './seeders'),
    },
  }),
};

umzug.migrations.on('migrating', (name) => logger.info(`Migrating ${name}`));
umzug.seeders.on('migrating', (name) => logger.info(`Seeding ${name}`));

const databaseConnect = async (shouldMigrate = true) => {
  await sequelize.authenticate();
  logger.info('Database connected');

  if (!shouldMigrate) return;

  for (const type of ['migrations', 'seeders']) {
    const result = await umzug[type].up();
    if (result.length) {
      logger.info(`UP-ed ${type}:${result.map((item) => `\n${item.file}`)}`);
    } else {
      logger.info(`Pending ${type} not found`);
    }
  }
};

export default {
  models, sequelize, Sequelize, databaseConnect, umzug,
};
