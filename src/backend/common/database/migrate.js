import argumentsParser from 'minimist';
import db from '.';
import logger from '../services/LoggerService';
import { uppercaseFirst } from '../utils/index';

const argv = argumentsParser(process.argv.slice(2));
const method = argv._[0] || 'up';
const type = argv.type || 'migrations';
const params = {};
if (typeof argv.from !== 'undefined') params.from = argv.from;
if (typeof argv.to !== 'undefined') params.to = argv.to;

logger.info(`${type === 'migrations' ? 'Migrating' : 'Seeding'} "${process.env.NODE_ENV}" environment...`);

db.umzug[type][method](params)
  .then((migrations) => {
    if (migrations && migrations.length === 0) logger.info(`${uppercaseFirst(type)} not found`);
    process.exit();
  })
  .catch((err) => {
    logger.error(err);
    logger.error(err.sql);
    process.exit(1);
  });
