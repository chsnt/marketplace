import { Op } from 'sequelize';
import dbTemplates from '../../utils/dbTemplates';
import { OrderStatusModel } from '../models/Order';

const TABLES = {
  PAYMENT_TYPES: 'payment_types',
  PAYMENTS: 'payments',
  ORDERS: 'orders',
};

const PAYMENT_TYPES = {
  CARD: { id: '9ae39995-8757-43b9-8943-325b0a49dc30', alias: 'CARD', name: 'По карте' },
  INVOICE: { id: '8e076648-735b-42f2-8cd8-99dd8de36986', alias: 'INVOICE', name: 'По счету' },
};


module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const templates = dbTemplates(DataTypes);
    await queryInterface.createTable(TABLES.PAYMENT_TYPES, {
      id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
      name: { type: DataTypes.STRING, allowNull: false, unique: true },
      alias: { type: DataTypes.STRING, unique: true },
    }, { transaction });
    await queryInterface.bulkInsert(TABLES.PAYMENT_TYPES, Object.values(PAYMENT_TYPES), { transaction });
    await queryInterface.removeColumn(TABLES.PAYMENTS, 'paid_by_card', { transaction });
    await queryInterface.addColumn(
      TABLES.PAYMENTS,
      'payment_type_id',
      templates.foreign({ table: TABLES.PAYMENT_TYPES }),
      { transaction },
    );
    await Promise.all([
      queryInterface.bulkUpdate(
        TABLES.PAYMENTS,
        { payment_type_id: PAYMENT_TYPES.CARD.id },
        {
          payment_order_id: {
            [Op.not]: null,
          },
        },
        { transaction },
      ),
      queryInterface.bulkUpdate(
        TABLES.PAYMENTS,
        { payment_type_id: PAYMENT_TYPES.INVOICE.id },
        { payment_order_id: null },
        { transaction },
      ),
    ]);
    const ordersWithoutPayments = await queryInterface.sequelize.query(
      `SELECT orders.id as id, SUM(COALESCE(client_services.cost,0) + COALESCE(client_tariffs.cost,0)) as "cost"
        FROM orders
          LEFT JOIN client_services ON orders.id = client_services.order_id
          LEFT JOIN client_tariffs ON orders.id = client_tariffs.order_id
            WHERE orders.order_status_id = '${OrderStatusModel.STATUSES.NOT_PAID}'
            AND (SELECT count(*) FROM payments WHERE payments.order_id = orders.id) = 0
            GROUP BY orders.id`,
      { type: DataTypes.QueryTypes.SELECT, transaction },
    );
    if (!ordersWithoutPayments.length) return;
    await queryInterface.bulkInsert(TABLES.PAYMENTS,
      ordersWithoutPayments.map(({ id, cost }) => ({
        order_id: id,
        payment_type_id: PAYMENT_TYPES.INVOICE.id,
        cost,
        total_cost: cost,
      })),
      { transaction });
  }),
  down: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.removeColumn(TABLES.PAYMENTS, 'payment_type_id', { transaction });
    await queryInterface.addColumn(
      TABLES.PAYMENTS,
      'paid_by_card',
      {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      { transaction },
    );
    await queryInterface.dropTable(TABLES.PAYMENT_TYPES, { transaction });
  }),
};
