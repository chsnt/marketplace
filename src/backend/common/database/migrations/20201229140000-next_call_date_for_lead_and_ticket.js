const TABLES = ['leads', 'tickets'];

const COLUMN = 'next_call_date';

module.exports = {
  up: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all(
    TABLES.map((TABLE) => queryInterface.addColumn(TABLE, COLUMN, { type: 'TIMESTAMP' }, { transaction })),
  )),


  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all(
    TABLES.map((TABLE) => queryInterface.removeColumn(TABLE, COLUMN, { transaction })),
  )),
};
