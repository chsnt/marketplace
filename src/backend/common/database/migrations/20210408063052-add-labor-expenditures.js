const TICKET_TABLE = 'tickets';
const LABOR_COSTS_COLUMN = 'labor_costs';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.addColumn(
    TICKET_TABLE,
    LABOR_COSTS_COLUMN,
    { type: DataTypes.INTEGER },
  ),
  down: (queryInterface) => queryInterface.removeColumn(TICKET_TABLE, LABOR_COSTS_COLUMN),
};
