import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  ORDERS: 'orders',
  ORDER_OFFER_DOCUMENTS: 'order_offer_documents',
};

const OFFER_ID_COLUMN = 'order_offer_document_id';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const templates = dbTemplates(DataTypes);

    await queryInterface.createTable(TABLES.ORDER_OFFER_DOCUMENTS,
      {
        id: templates.id,
        name: { type: DataTypes.STRING, allowNull: false },
        alias: { type: DataTypes.STRING },
        ...templates.timestamps,
        ...templates.by,
      }, { transaction });

    await queryInterface.addColumn(
      TABLES.ORDERS,
      OFFER_ID_COLUMN,
      templates.foreign({ table: TABLES.ORDER_OFFER_DOCUMENTS }),
      { transaction },
    );
  }),
  down: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.removeColumn(
      TABLES.ORDERS,
      OFFER_ID_COLUMN,
      { transaction },
    );

    await queryInterface.dropTable(TABLES.ORDER_OFFER_DOCUMENTS, { transaction });
  }),
};
