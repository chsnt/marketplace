const TABLES = {
  SUPPORT_REQUESTS: 'support_requests',
  USER_GROUP_RELATIONS: 'user_group_relations',
  USERS: 'users',
  USER_GROUPS: 'user_groups',
};

const COLUMNS = {
  SOLUTION: 'solution',
  USER_ID: 'user_id',
  USER_GROUP_ID: 'user_group_id',
};

const RECREATE_CONSTRAINTS = [
  {
    columnName: COLUMNS.USER_GROUP_ID,
    referenceTable: TABLES.USER_GROUPS,
  },
  {
    columnName: COLUMNS.USER_ID,
    referenceTable: TABLES.USERS,
  }];

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await Promise.all([
      queryInterface.addColumn(TABLES.SUPPORT_REQUESTS, COLUMNS.SOLUTION, { type: DataTypes.STRING }, { transaction }),
      ...RECREATE_CONSTRAINTS.map(({ columnName }) => queryInterface.removeConstraint(
        TABLES.USER_GROUP_RELATIONS,
        `${TABLES.USER_GROUP_RELATIONS}_${columnName}_fkey`,
        { transaction },
      )),
    ]);

    return Promise.all(
      RECREATE_CONSTRAINTS.map(({ columnName, referenceTable }) => queryInterface
        .addConstraint(TABLES.USER_GROUP_RELATIONS, {
          fields: [columnName],
          type: 'foreign key',
          name: `${TABLES.USER_GROUP_RELATIONS}_${columnName}_fkey`,
          references: {
            table: referenceTable,
            field: 'id',
          },
          onDelete: 'cascade',
          onUpdate: 'cascade',
          transaction,
        })),
    );
  }),

  down: (queryInterface) => queryInterface.removeColumn(TABLES.SUPPORT_REQUESTS, COLUMNS.SOLUTION),
};
