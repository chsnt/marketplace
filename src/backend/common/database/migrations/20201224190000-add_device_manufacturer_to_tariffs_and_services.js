import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  DEVICE_MANUFACTURERS: 'device_manufacturers',
  TARIFFS: 'tariffs',
  SERVICES: 'services',
  DEVICE_MANUFACTURER_TARIFF_RELATIONS: 'device_manufacturer_tariff_relations',
  DEVICE_MANUFACTURER_SERVICE_RELATIONS: 'device_manufacturer_service_relations',
};

const COLUMNS = {
  TARIFF_ID: 'tariff_id',
  SERVICE_ID: 'service_id',
  DEVICE_MANUFACTURER_ID: 'device_manufacturer_id',
};

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.createTable(
      TABLES.DEVICE_MANUFACTURER_SERVICE_RELATIONS,
      {
        id: dbTemplates(DataTypes).id,
        [COLUMNS.SERVICE_ID]: dbTemplates(DataTypes).foreign({
          table: TABLES.SERVICES,
          allowNull: false,
          onDelete: 'CASCADE',
        }),
        [COLUMNS.DEVICE_MANUFACTURER_ID]: dbTemplates(DataTypes).foreign({
          table: TABLES.DEVICE_MANUFACTURERS,
          allowNull: false,
          onDelete: 'CASCADE',
        }),
        ...dbTemplates(DataTypes).timestamps,
        ...dbTemplates(DataTypes).by,
      },
      { transaction },
    ),
    queryInterface.createTable(
      TABLES.DEVICE_MANUFACTURER_TARIFF_RELATIONS,
      {
        id: dbTemplates(DataTypes).id,
        [COLUMNS.TARIFF_ID]: dbTemplates(DataTypes).foreign({
          table: TABLES.TARIFFS,
          allowNull: false,
          onDelete: 'CASCADE',
        }),
        [COLUMNS.DEVICE_MANUFACTURER_ID]: dbTemplates(DataTypes).foreign({
          table: TABLES.DEVICE_MANUFACTURERS,
          allowNull: false,
          onDelete: 'CASCADE',
        }),
        ...dbTemplates(DataTypes).timestamps,
        ...dbTemplates(DataTypes).by,
      },
      { transaction },
    ),
  ])),


  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.dropTable(TABLES.DEVICE_MANUFACTURER_SERVICE_RELATIONS, { transaction }),
    queryInterface.dropTable(TABLES.DEVICE_MANUFACTURER_TARIFF_RELATIONS, { transaction }),
  ])),
};
