const TARIFFS_TABLE = 'tariffs';
const DESCRIPTION_COLUMN = 'description';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.addColumn(
    TARIFFS_TABLE, DESCRIPTION_COLUMN, { type: DataTypes.STRING },
  ),

  down: (queryInterface) => queryInterface.removeColumn(TARIFFS_TABLE, DESCRIPTION_COLUMN),
};
