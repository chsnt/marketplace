import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  TICKETS: 'tickets',
  CLIENTS: 'clients',
};

const COLUMN_CLIENT_ID = 'client_id';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.addColumn(
    TABLES.TICKETS,
    COLUMN_CLIENT_ID,
    dbTemplates(DataTypes).foreign({ table: TABLES.CLIENTS, onDelete: 'CASCADE' }),
  ),

  down: (queryInterface) => queryInterface.removeColumn(TABLES.CLIENT_TARIFFS, COLUMN_CLIENT_ID),
};
