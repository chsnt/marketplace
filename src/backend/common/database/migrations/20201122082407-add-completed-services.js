const TABLES = {
  ORGANIZATION_SERVICES: 'organization_services',
};

const COLUMNS = {
  COMPLETED: 'completed',
};

module.exports = {
  up: (queryInterface) => queryInterface.addColumn(
    TABLES.ORGANIZATION_SERVICES,
    COLUMNS.COMPLETED,
    { type: 'TIMESTAMP' },
  ),

  down: (queryInterface) => queryInterface.removeColumn(
    TABLES.ORGANIZATION_SERVICES,
    COLUMNS.COMPLETED,
  ),
};
