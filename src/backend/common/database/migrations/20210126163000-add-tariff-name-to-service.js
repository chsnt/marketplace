const TABLE = 'services';
const COLUMN = 'name_in_tariff';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.addColumn(TABLE, COLUMN, DataTypes.STRING),

  down: (queryInterface) => queryInterface.removeColumn(TABLE, COLUMN),
};
