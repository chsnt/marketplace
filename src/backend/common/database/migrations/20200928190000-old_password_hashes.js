import templates from '../../utils/dbTemplates';

const TABLES = {
  OLD_PASSWORD_HASHES: 'old_password_hashes',
  USERS: 'users',
};

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.createTable(TABLES.OLD_PASSWORD_HASHES, {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    password: { type: DataTypes.STRING, allowNull: false },
    user_id: templates(DataTypes).foreign({ table: TABLES.USERS, onDelete: 'CASCADE', allowNull: false }),
    created_at: templates(DataTypes).timestamps.created_at,
  }),
  down: (queryInterface) => queryInterface.dropTable(TABLES.OLD_PASSWORD_HASHES),
};
