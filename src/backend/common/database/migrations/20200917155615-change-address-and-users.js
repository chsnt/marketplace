import templates from '../../utils/dbTemplates';

const ORGANIZATIONS_TABLE = 'organizations';
const ADDRESSES_TABLE = 'addresses';
const USER_TABLE = 'users';
const OLD_SERVICE_ADDRESS_COLUMN = 'service_address';
const NEW_SERVICE_ADDRESS_COLUMN = 'service_address_id';
const BIRTHDAY_COLUMN = 'birthday';


export default {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.removeColumn(ORGANIZATIONS_TABLE, OLD_SERVICE_ADDRESS_COLUMN, { transaction }),

    queryInterface.addColumn(
      ORGANIZATIONS_TABLE,
      NEW_SERVICE_ADDRESS_COLUMN,
      templates(DataTypes).foreign({ table: ADDRESSES_TABLE }),
      { transaction },
    ),

    queryInterface.addColumn(USER_TABLE, BIRTHDAY_COLUMN, { type: 'TIMESTAMP' }),
  ])),
  down: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.removeColumn(ORGANIZATIONS_TABLE, NEW_SERVICE_ADDRESS_COLUMN, { transaction }),

    queryInterface.addColumn(
      ORGANIZATIONS_TABLE,
      OLD_SERVICE_ADDRESS_COLUMN,
      templates(DataTypes).foreign({ table: ADDRESSES_TABLE }),
      { transaction },
    ),

    queryInterface.removeColumn(USER_TABLE, BIRTHDAY_COLUMN),
  ])),
};
