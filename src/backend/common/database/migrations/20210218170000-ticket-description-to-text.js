const TABLE = 'tickets';
const COLUMNS = ['comment', 'description', 'solution'];

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all(
    COLUMNS.map((COLUMN) => queryInterface.changeColumn(TABLE, COLUMN, { type: DataTypes.TEXT }, { transaction })),
  )),

  down: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all(
    COLUMNS.map((COLUMN) => queryInterface.changeColumn(TABLE, COLUMN, { type: DataTypes.STRING }, { transaction })),
  )),
};
