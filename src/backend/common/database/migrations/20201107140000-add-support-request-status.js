import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  SUPPORT_REQUEST_STATUSES: 'support_request_statuses',
  SUPPORT_REQUESTS: 'support_requests',
};

const COLUMN = 'support_request_status_id';

const NUMBER_COLUMN = 'number';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const templates = dbTemplates(DataTypes);
    await Promise.all([
      queryInterface.createTable(TABLES.SUPPORT_REQUEST_STATUSES, {
        id: templates.id,
        name: { type: DataTypes.STRING, unique: true, allowNull: false },
        alias: { type: DataTypes.STRING, unique: true, allowNull: false },
        ...templates.timestamps,
      }, { transaction }),
      queryInterface.addColumn(
        TABLES.SUPPORT_REQUESTS,
        NUMBER_COLUMN,
        { type: DataTypes.INTEGER, autoIncrement: true },
        { transaction },
      ),
    ]);

    return queryInterface.addColumn(
      TABLES.SUPPORT_REQUESTS,
      COLUMN,
      templates.foreign({ table: TABLES.SUPPORT_REQUEST_STATUSES, allowNull: false }),
      { transaction },
    );
  }),

  down: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.removeColumn(
      TABLES.SUPPORT_REQUESTS,
      COLUMN,
      { transaction },
    );
    return queryInterface.dropTable(TABLES.SUPPORT_REQUEST_STATUSES, { transaction });
  }),
};
