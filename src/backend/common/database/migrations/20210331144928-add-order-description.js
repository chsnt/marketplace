const ORDERS_TABLE = 'orders';
const DESCRIPTION_COLUMN = 'description';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.addColumn(
    ORDERS_TABLE,
    DESCRIPTION_COLUMN,
    { type: DataTypes.STRING(1024) },
  ),
  down: (queryInterface) => queryInterface.removeColumn(ORDERS_TABLE, DESCRIPTION_COLUMN),
};
