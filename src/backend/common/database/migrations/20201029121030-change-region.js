const ADDRESSES_TABLE = 'addresses';

const COLUMNS = ['region', 'area'];

module.exports = {
  up: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all(
    COLUMNS.map((column) => queryInterface.removeColumn(ADDRESSES_TABLE, column, { transaction })),
  )),

  down: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all(
    COLUMNS.map((column) => queryInterface.addColumn(
      ADDRESSES_TABLE,
      column,
      { type: DataTypes.STRING },
      { transaction },
    )),
  )),
};
