import dbTemplates from '../../utils/dbTemplates';

const TABLE = 'integrator_logs';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => {
    const templates = dbTemplates(DataTypes);
    return queryInterface.createTable(TABLE, {
      id: templates.id,
      resource: { type: DataTypes.STRING },
      method: { type: DataTypes.STRING },
      status: { type: DataTypes.SMALLINT },
      request: { type: DataTypes.JSONB },
      response: { type: DataTypes.JSONB },
      ...templates.timestamps,
      ...templates.by,
    }, { transaction });
  }),

  down: (queryInterface) => queryInterface.dropTable(TABLE),
};
