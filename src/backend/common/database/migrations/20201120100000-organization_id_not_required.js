const TABLE = 'support_requests';

const COLUMNS = {
  ORGANIZATION_ID: 'organization_id',
  COMMENT: 'comment',
};

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.sequelize
      .query(`ALTER TABLE ${TABLE} ALTER COLUMN ${COLUMNS.ORGANIZATION_ID} DROP NOT NULL`, { transaction }),
    queryInterface.addColumn(TABLE, COLUMNS.COMMENT, { type: DataTypes.STRING(1024) }, { transaction }),
  ])),


  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.sequelize
      .query(`ALTER TABLE ${TABLE} ALTER COLUMN ${COLUMNS.ORGANIZATION_ID} SET NOT NULL`, { transaction }),
    queryInterface.removeColumn(TABLE, COLUMNS.COMMENT, { transaction }),
  ])),
};
