const TABLES = ['federation_subjects', 'management_services'];

module.exports = {
  up: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all(
    TABLES.map((table) => queryInterface.renameColumn(
      table,
      'externalId',
      'external_id',
      { transaction },
    )),
  )),
  down: async () => {}, // do nothing
};
