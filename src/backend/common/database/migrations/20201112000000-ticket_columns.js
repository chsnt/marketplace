const TABLE = 'tickets';

const COLUMNS = [
  'id_sd',
  'comment',
  'description',
];

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all(COLUMNS
    .map((column) => queryInterface.addColumn(TABLE, column, { type: DataTypes.STRING }, { transaction })))),

  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all(COLUMNS
    .map((column) => queryInterface.removeColumn(TABLE, column, { transaction })))),
};
