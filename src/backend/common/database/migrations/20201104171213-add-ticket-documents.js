import templates from '../../utils/dbTemplates';

const TICKET_DOCUMENTS_TABLE = 'ticket_documents';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => queryInterface.createTable(
    TICKET_DOCUMENTS_TABLE,
    {
      id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
      ticket_id: templates(DataTypes).foreign({ table: 'tickets', onDelete: 'CASCADE', allowNull: false }),
      name: { type: DataTypes.STRING, allowNull: false },
      ...templates(DataTypes).timestamps,
    }, { transaction },
  )),

  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => queryInterface.dropTable(
    TICKET_DOCUMENTS_TABLE,
    { transaction },
  )),
};
