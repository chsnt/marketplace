import dbTemplates from '../../utils/dbTemplates';

const ID_COLUMN = 'id';
const TIMESTAMP_COLUMNS = ['created_at', 'updated_at'];
const IGNORE_TABLES = ['sequelize_meta', 'sequelize_data'];

export default {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.sequelize.query('CREATE EXTENSION IF NOT EXISTS pgcrypto;', { transaction });
    const tables = await queryInterface.showAllTables();
    for (const table of tables) {
      if (!IGNORE_TABLES.includes(table)) {
        const schema = await queryInterface.describeTable(table, { transaction });
        await queryInterface.changeColumn(
          table,
          ID_COLUMN,
          dbTemplates(DataTypes).id,
          { transaction },
        );

        await Promise.all(TIMESTAMP_COLUMNS.map((column) => {
          if (!Object.keys(schema).includes(column)) return;
          return queryInterface.changeColumn(table, column, dbTemplates(DataTypes).timestamps[column], { transaction });
        }));
      }
    }
  }),
  down: async () => {}, // do nothing
};
