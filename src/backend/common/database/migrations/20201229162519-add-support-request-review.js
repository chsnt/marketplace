import templates from '../../utils/dbTemplates';

const TABLES = {
  SUPPORT_REQUESTS: 'support_requests',
  SUPPORT_REQUEST_REVIEWS: 'support_request_reviews',
  SUPPORT_REQUEST_REVIEW_DOCUMENTS: 'support_request_review_documents',
};

const COLUMNS = {
  SUPPORT_REQUEST_REVIEW_ID: 'support_request_review_id',
  TEXT: 'text',
  MARK: 'mark',
};

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.createTable(
      TABLES.SUPPORT_REQUEST_REVIEWS,
      {
        id: templates(DataTypes).id,
        text: { type: DataTypes.STRING },
        mark: { type: DataTypes.SMALLINT, allowNull: false },
        ...templates(DataTypes).timestamps,
        ...templates(DataTypes).by,
      },
      { transaction },
    );

    await queryInterface.createTable(
      TABLES.SUPPORT_REQUEST_REVIEW_DOCUMENTS,
      {
        id: templates(DataTypes).id,
        name: { type: DataTypes.STRING, allowNull: false },
        [COLUMNS.SUPPORT_REQUEST_REVIEW_ID]: templates(DataTypes).foreign({
          table: TABLES.SUPPORT_REQUEST_REVIEWS,
          onDelete: 'CASCADE',
          allowNull: false,
        }),
        ...templates(DataTypes).timestamps,
        ...templates(DataTypes).by,
      },
      { transaction },
    );

    await queryInterface.addColumn(
      TABLES.SUPPORT_REQUESTS,
      COLUMNS.SUPPORT_REQUEST_REVIEW_ID,
      templates(DataTypes).foreign({
        table: TABLES.SUPPORT_REQUEST_REVIEWS,
        onDelete: 'CASCADE',
      }),
      { transaction },
    );
  }),
  down: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.removeColumn(
      TABLES.SUPPORT_REQUESTS,
      COLUMNS.SUPPORT_REQUEST_REVIEW_ID,
      { transaction },
    );
    await queryInterface.dropTable(TABLES.SUPPORT_REQUEST_REVIEW_DOCUMENTS, { transaction });
    await queryInterface.dropTable(TABLES.SUPPORT_REQUEST_REVIEWS, { transaction });
  }),
};
