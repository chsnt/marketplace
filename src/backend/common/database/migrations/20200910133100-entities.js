/* eslint-disable max-len */
import templates from '../../utils/dbTemplates';

const ORGANIZATIONS_TABLE = 'organizations';
const ADDRESSES_TABLE = 'addresses';
const SERVICE_ADDRESS_COLUMN = 'service_address';

const INDEPENDENT_TABLES = (DataTypes) => {
  const dictionaryTemplate = {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING, allowNull: false, unique: true },
    alias: { type: DataTypes.STRING, allowNull: false, unique: true },
    ...templates(DataTypes).timestamps,
  };
  return {
    device_types: dictionaryTemplate,
    tax_types: dictionaryTemplate,
    ownership_types: dictionaryTemplate,
    order_statuses: dictionaryTemplate,
    field_types: dictionaryTemplate,
    limit_measures: dictionaryTemplate,
    ticket_statuses: dictionaryTemplate,
    support_request_topics: dictionaryTemplate,
    device_manufacturers: {
      id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
      name: { type: DataTypes.STRING, allowNull: false, unique: true },
      ...templates(DataTypes).timestamps,
    },
    ticket_reviews: {
      id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
      avatar: { type: DataTypes.STRING },
      text: { type: DataTypes.STRING, allowNull: false, unique: true },
      mark: { type: DataTypes.INTEGER },
      ...templates(DataTypes).timestamps,
    },
    order_element_types: {
      ...dictionaryTemplate,
      table: { type: DataTypes.STRING, allowNull: false },
    },
  };
};

const TABLES_FIRST_ORDER = (DataTypes) => ({
  bank_details: {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    organization_id: templates(DataTypes).foreign({ table: ORGANIZATIONS_TABLE, onDelete: 'CASCADE', allowNull: false }),
    bank: { type: DataTypes.STRING },
    bank_account: { type: DataTypes.STRING(20) },
    correspondent_account: { type: DataTypes.STRING(20) },
    rcbic: { type: DataTypes.STRING(9) },
    tax_type_id: templates(DataTypes).foreign({ table: 'tax_types', onDelete: 'CASCADE', allowNull: false }),
    ownership_type_id: templates(DataTypes).foreign({ table: 'ownership_types', onDelete: 'CASCADE', allowNull: false }),
    ...templates(DataTypes).timestamps,
  },
  notifications: {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    organization_id: templates(DataTypes).foreign({ table: ORGANIZATIONS_TABLE, onDelete: 'CASCADE', allowNull: false }),
    title: { type: DataTypes.STRING, allowNull: false },
    message: { type: DataTypes.STRING(1024) },
    payload: { type: DataTypes.TEXT },
    ...templates(DataTypes).timestamps,
  },
  orders: {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    organization_id: templates(DataTypes).foreign({ table: ORGANIZATIONS_TABLE, onDelete: 'CASCADE', allowNull: false }),
    order_status_id: templates(DataTypes).foreign({ table: 'order_statuses', onDelete: 'CASCADE', allowNull: false }),
    number: { type: DataTypes.INTEGER, allowNull: false, autoIncrement: true },
    ...templates(DataTypes).timestamps,
  },
  support_requests: {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    support_request_topic_id: templates(DataTypes).foreign({ table: 'support_requests', onDelete: 'CASCADE', allowNull: false }),
    organization_id: templates(DataTypes).foreign({ table: ORGANIZATIONS_TABLE, onDelete: 'CASCADE', allowNull: false }),
    text: { type: DataTypes.STRING(1024), allowNull: false },
    ...templates(DataTypes).timestamps,
  },
  devices: {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    device_manufacturer_id: templates(DataTypes).foreign({ table: 'device_manufacturers', onDelete: 'CASCADE', allowNull: false }),
    device_type_id: templates(DataTypes).foreign({ table: 'device_types', onDelete: 'CASCADE', allowNull: false }),
    name: { type: DataTypes.STRING, allowNull: false },
    ...templates(DataTypes).timestamps,
  },
  tariffs: {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    device_type_id: templates(DataTypes).foreign({ table: 'device_types', onDelete: 'CASCADE', allowNull: false }),
    name: { type: DataTypes.STRING, allowNull: false },
    cost: { type: DataTypes.FLOAT, allowNull: false },
    ...templates(DataTypes).timestamps,
  },
  services: {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    device_type_id: templates(DataTypes).foreign({ table: 'device_types', onDelete: 'CASCADE', allowNull: false }),
    name: { type: DataTypes.STRING, allowNull: false },
    cost: { type: DataTypes.FLOAT, allowNull: false },
    for_tariff_only: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
    ...templates(DataTypes).timestamps,
  },
});

const TABLES_SECOND_ORDER = (DataTypes) => ({
  organization_devices: {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    device_id: templates(DataTypes).foreign({ table: 'devices', onDelete: 'CASCADE', allowNull: false }),
    organization_id: templates(DataTypes).foreign({ table: 'organizations', onDelete: 'CASCADE', allowNull: false }),
    ...templates(DataTypes).timestamps,
  },
  device_parameters: {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    device_id: templates(DataTypes).foreign({ table: 'devices', onDelete: 'CASCADE', allowNull: false }),
    field_type_id: templates(DataTypes).foreign({ table: 'field_types', onDelete: 'CASCADE', allowNull: false }),
    name: { type: DataTypes.STRING, allowNull: false },
    alias: { type: DataTypes.STRING, allowNull: false },
    is_required: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
  },
  tariff_service_relations: {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    tariff_id: templates(DataTypes).foreign({ table: 'tariffs', onDelete: 'CASCADE', allowNull: false }),
    service_id: templates(DataTypes).foreign({ table: 'services', onDelete: 'CASCADE', allowNull: false }),
    limit: { type: DataTypes.FLOAT, allowNull: false },
    limit_measure_id: templates(DataTypes).foreign({ table: 'limit_measures', onDelete: 'CASCADE', allowNull: false }),
    ...templates(DataTypes).timestamps,
  },
  organization_services: {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    organization_id: templates(DataTypes).foreign({ table: 'organizations', onDelete: 'CASCADE', allowNull: false }),
    service_id: templates(DataTypes).foreign({ table: 'services', onDelete: 'CASCADE', allowNull: false }),
    cost: { type: DataTypes.FLOAT, allowNull: false },
    expiration_at: { type: DataTypes.DATE, allowNull: false },
    ...templates(DataTypes).timestamps,
  },
  support_request_attachments: {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    support_request_id: templates(DataTypes).foreign({ table: 'support_requests', onDelete: 'CASCADE', allowNull: false }),
    name: { type: DataTypes.STRING, allowNull: false },
    ...templates(DataTypes).timestamps,
  },
  order_documents: {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    order_id: templates(DataTypes).foreign({ table: 'orders', onDelete: 'CASCADE', allowNull: false }),
    name: { type: DataTypes.STRING, allowNull: false },
    ...templates(DataTypes).timestamps,
  },
  order_elements: {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    order_id: templates(DataTypes).foreign({ table: 'orders', onDelete: 'CASCADE', allowNull: false }),
    order_element_type_id: templates(DataTypes).foreign({ table: 'order_element_types', onDelete: 'CASCADE', allowNull: false }),
    value: { type: DataTypes.UUID, allowNull: false },
    cost: { type: DataTypes.FLOAT, allowNull: false },
    ...templates(DataTypes).timestamps,
  },
  payments: {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    order_id: templates(DataTypes).foreign({ table: 'orders', onDelete: 'CASCADE', allowNull: false }),
    promo_code: { type: DataTypes.STRING, allowNull: false },
    document_name: { type: DataTypes.STRING, allowNull: false },
    cost: { type: DataTypes.FLOAT, allowNull: false },
    total_cost: { type: DataTypes.FLOAT, allowNull: false },
    ...templates(DataTypes).timestamps,
  },
});

const TABLES_THIRD_ORDER = (DataTypes) => ({
  tickets: {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    organization_id: templates(DataTypes).foreign({ table: ORGANIZATIONS_TABLE, onDelete: 'CASCADE', allowNull: false }),
    ticket_status_id: templates(DataTypes).foreign({ table: 'ticket_statuses', onDelete: 'CASCADE', allowNull: false }),
    device_type_id: templates(DataTypes).foreign({ table: 'device_types', onDelete: 'CASCADE', allowNull: false }),
    support_request_id: templates(DataTypes).foreign({ table: 'device_types', onDelete: 'CASCADE' }),
    ticket_review_id: templates(DataTypes).foreign({ table: 'ticket_reviews', onDelete: 'CASCADE' }),
    number: { type: DataTypes.INTEGER, allowNull: false, autoIncrement: true },
    solution: { type: DataTypes.STRING },
    ...templates(DataTypes).timestamps,
  },
  organization_device_parameters: {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    organization_device_id: templates(DataTypes).foreign({ table: 'organization_devices', onDelete: 'CASCADE', allowNull: false }),
    device_parameter_id: templates(DataTypes).foreign({ table: 'device_parameters', onDelete: 'CASCADE', allowNull: false }),
    value: { type: DataTypes.STRING, allowNull: false },
    ...templates(DataTypes).timestamps,
  },
  organization_tariffs: {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    organization_device_id: templates(DataTypes).foreign({ table: 'organization_devices', onDelete: 'CASCADE', allowNull: false }),
    tariff_id: templates(DataTypes).foreign({ table: 'tariffs', onDelete: 'CASCADE', allowNull: false }),
    cost: { type: DataTypes.FLOAT, allowNull: false },
    expiration_at: { type: DataTypes.DATE, allowNull: false },
    ...templates(DataTypes).timestamps,
  },
});

const TABLES_FOURTH_ORDER = (DataTypes) => ({
  ticket_close_documents: {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    ticket_id: templates(DataTypes).foreign({ table: 'tickets', onDelete: 'CASCADE', allowNull: false }),
    name: { type: DataTypes.STRING, allowNull: false },
    ...templates(DataTypes).timestamps,
  },
  organization_tariff_balances: {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    organization_tariff_id: templates(DataTypes).foreign({ table: 'organization_tariffs', onDelete: 'CASCADE', allowNull: false }),
    tariff_service_relation_id: templates(DataTypes).foreign({ table: 'tariff_service_relations', onDelete: 'CASCADE', allowNull: false }),
    value: { type: DataTypes.FLOAT, allowNull: false },
    ...templates(DataTypes).timestamps,
  },
});

export default {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    /* Создадим основные справочники */
    await Promise.all(Object.entries(INDEPENDENT_TABLES(DataTypes)).map(([table, schema]) => queryInterface.createTable(
      table,
      schema,
      { transaction },
    )));
    /* Создаем таблицы, которые ссылаются на существующие таблицы */
    await Promise.all([
      queryInterface.addColumn(
        ORGANIZATIONS_TABLE,
        SERVICE_ADDRESS_COLUMN,
        templates(DataTypes).foreign({ table: ADDRESSES_TABLE }),
        { transaction },
      ),
      ...Object.entries(TABLES_FIRST_ORDER(DataTypes)).map(([table, schema]) => queryInterface.createTable(
        table,
        schema,
        { transaction },
      )),
    ]);
    /* Создаем таблицы, которые ссылаются на существующие таблицы */
    await Promise.all(
      Object.entries(TABLES_SECOND_ORDER(DataTypes)).map(([table, schema]) => queryInterface.createTable(
        table,
        schema,
        { transaction },
      )),
    );
    /* Создаем таблицы, которые ссылаются на существующие таблицы */
    await Promise.all(
      Object.entries(TABLES_THIRD_ORDER(DataTypes)).map(([table, schema]) => queryInterface.createTable(
        table,
        schema,
        { transaction },
      )),
    );
    return Promise.all(
      Object.entries(TABLES_FOURTH_ORDER(DataTypes)).map(([table, schema]) => queryInterface.createTable(
        table,
        schema,
        { transaction },
      )),
    );
  }),
  down: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await Promise.all(
      Object.keys(TABLES_FOURTH_ORDER(DataTypes)).map((table) => queryInterface.dropTable(
        table,
        { transaction },
      )),
    );
    await Promise.all(
      Object.keys(TABLES_THIRD_ORDER(DataTypes)).map((table) => queryInterface.dropTable(
        table,
        { transaction },
      )),
    );
    await Promise.all(
      Object.keys(TABLES_SECOND_ORDER(DataTypes)).map((table) => queryInterface.dropTable(
        table,
        { transaction },
      )),
    );
    /* Удалим таблицы, которые ссылаются на существующие таблицы */
    await Promise.all([
      queryInterface.removeColumn(
        ORGANIZATIONS_TABLE,
        SERVICE_ADDRESS_COLUMN,
        { transaction },
      ),
      ...Object.keys(TABLES_FIRST_ORDER(DataTypes)).map((table) => queryInterface.dropTable(
        table,
        { transaction },
      )),
    ]);
    /* Удаляем основные справочники */
    return Promise.all(Object.keys(INDEPENDENT_TABLES(DataTypes)).map((table) => queryInterface.dropTable(
      table,
      { transaction },
    )));
  }),
};
