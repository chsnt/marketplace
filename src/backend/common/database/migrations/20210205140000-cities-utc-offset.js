const TABLE = 'cities';
const COLUMN = 'utc_offset';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.addColumn(TABLE, COLUMN, {
    type: DataTypes.INTEGER, defaultValue: 0, allowNull: false,
  }),

  down: (queryInterface) => queryInterface.removeColumn(TABLE, COLUMN),
};
