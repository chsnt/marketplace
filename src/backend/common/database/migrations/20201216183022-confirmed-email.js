const TABLE = 'users';

const COLUMN = 'is_email_confirmed';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => queryInterface.addColumn(
    TABLE,
    COLUMN,
    { type: DataTypes.BOOLEAN, defaultValue: false },
    { transaction },
  )),


  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => queryInterface.removeColumn(
    TABLE,
    COLUMN,
    { transaction },
  )),
};
