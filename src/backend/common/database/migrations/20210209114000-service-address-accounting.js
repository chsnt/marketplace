import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  SERVICES: 'services',
  SERVICE_ADDRESS_ACCOUNTING_TYPES: 'service_address_accounting_types',
  ADDRESSES: 'addresses',
  TICKETS: 'tickets',
  CLIENT_DEVICES: 'client_devices',
};

const COLUMNS = {
  SERVICE_ADDRESS_ACCOUNTING_TYPE_ID: 'service_address_accounting_type_id',
  SERVICE_ADDRESS_ID: 'service_address_id',
};

const DATA = [
  {
    id: '84db3034-1068-47f7-a8f3-1fba5790d861', name: 'При заказе', alias: 'IN_ORDER', order: 1,
  },
  {
    id: '161f4f19-9f4f-4af9-8a2e-cad5e76e7449', name: 'При создании ЗНО', alias: 'IN_TICKET', order: 2,
  },
  {
    id: '36aae030-ef17-462f-b613-c9c3cbb1e90c', name: 'Не запрашивается', alias: 'NOT_REQUESTED', order: 3,
  },
];

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const templates = dbTemplates(DataTypes);
    await queryInterface.createTable(
      TABLES.SERVICE_ADDRESS_ACCOUNTING_TYPES,
      {
        id: templates.id,
        name: { type: DataTypes.STRING, allowNull: false, unique: true },
        alias: { type: DataTypes.STRING, allowNull: false, unique: true },
        order: templates.autoIncrement({ allowNull: false }),
        ...templates.by,
        ...templates.timestamps,
      },
      { transaction },
    );

    await Promise.all([
      queryInterface.addColumn(
        TABLES.TICKETS,
        COLUMNS.SERVICE_ADDRESS_ID,
        templates.foreign({ table: TABLES.ADDRESSES }),
        { transaction },
      ),
      queryInterface.sequelize.query(
        `ALTER TABLE ${TABLES.CLIENT_DEVICES} ALTER COLUMN ${COLUMNS.SERVICE_ADDRESS_ID} DROP NOT NULL`,
        { transaction },
      ),
    ]);

    /* Создадим записи, чтоб проставить значения по-умолчанию и повесить not null на колонку */
    await queryInterface.bulkInsert(TABLES.SERVICE_ADDRESS_ACCOUNTING_TYPES, DATA, { transaction });

    await queryInterface.addColumn(TABLES.SERVICES, COLUMNS.SERVICE_ADDRESS_ACCOUNTING_TYPE_ID,
      templates.foreign({ table: TABLES.SERVICE_ADDRESS_ACCOUNTING_TYPES, onDelete: 'CASCADE' }),
      { transaction });

    /* Проставим значение по-умолчанию */
    await queryInterface.bulkUpdate(
      TABLES.SERVICES,
      { [COLUMNS.SERVICE_ADDRESS_ACCOUNTING_TYPE_ID]: DATA[0].id },
      {}, { transaction },
    );

    /* Запрещаем писать null в колонку */
    return queryInterface.sequelize.query(
      `ALTER TABLE ${TABLES.SERVICES} ALTER COLUMN ${COLUMNS.SERVICE_ADDRESS_ACCOUNTING_TYPE_ID} SET NOT NULL`,
      { transaction },
    );
  }),

  down: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.removeColumn(TABLES.SERVICES, COLUMNS.SERVICE_ADDRESS_ACCOUNTING_TYPE_ID, { transaction });
    await Promise.all([
      queryInterface.removeColumn(
        TABLES.TICKETS,
        COLUMNS.SERVICE_ADDRESS_ID,
        { transaction },
      ),
      queryInterface.sequelize.query(
        `ALTER TABLE ${TABLES.CLIENT_DEVICES} ALTER COLUMN ${COLUMNS.SERVICE_ADDRESS_ID} SET NOT NULL`,
        { transaction },
      ),
    ]);
    return queryInterface.dropTable(TABLES.SERVICE_ADDRESS_ACCOUNTING_TYPES, { transaction });
  }),
};
