import templates from '../../utils/dbTemplates';

const TABLES = {
  ORDERS: 'orders',
  ORDER_ELEMENT_TYPES: 'order_element_types',
  ORDER_ELEMENTS: 'order_elements',
  ORGANIZATION_SERVICES: 'organization_services',
  ORGANIZATION_TARIFFS: 'organization_tariffs',
  DEVICE_TYPES: 'device_types',
  DEVICE_MODEL_SERVICE_RELATIONS: 'device_model_service_relations',
  DEVICE_MODEL_TARIFF_RELATIONS: 'device_model_tariff_relations',
  ORGANIZATION_DEVICES: 'organization_devices',
  ORGANIZATIONS: 'organizations',
  ADDRESSES: 'addresses',
  DEVICE_PARAMETERS: 'device_parameters',
  TARIFFS: 'tariffs',
  SERVICES: 'services',
};

const ORDER_ID_COLUMN = 'order_id';
const DEVICE_TYPE_ID_COLUMN = 'device_type_id';
const ORGANIZATION_DEVICE_ID_COLUMN = 'organization_device_id';
const SERVICE_ADDRESS_ID_COLUMN = 'service_address_id';

const CASCADE = 'CASCADE';

const removeTables = [TABLES.ORDER_ELEMENTS, TABLES.ORDER_ELEMENT_TYPES];

const organizationTables = [TABLES.ORGANIZATION_SERVICES, TABLES.ORGANIZATION_TARIFFS];

const removeDeviceType = [TABLES.TARIFFS, TABLES.SERVICES];

module.exports = {
  up: async (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    /* удаление таблиц order_elements и order_element_types */
    await Promise.all(removeTables.map((table) => queryInterface.dropTable(table, { transaction })));

    /* добавление колонок order_id в organization_tariffs и organization_services */
    await Promise.all([
      ...organizationTables.map((table) => queryInterface.addColumn(
        table,
        ORDER_ID_COLUMN,
        templates(DataTypes).foreign({ onDelete: CASCADE, table: TABLES.ORDERS }),
        { transaction },
      )),
      queryInterface.addColumn(
        TABLES.ORGANIZATION_SERVICES,
        ORGANIZATION_DEVICE_ID_COLUMN,
        templates(DataTypes).foreign({ onDelete: CASCADE, table: 'organization_devices' }),
        { transaction },
      ),
    ]);

    /* Поля expiration_at, cost,  в таблице organization_services и поля organization_device_id, expiration_at, cost в таблице organization_tariffs при формировании заказа могут быть пустыми.
     Поля cost и organization_device_id заполняются при оформлении заказа.
     Поля expiration_at при его оплате.
     */
    const fields = {
      organization_device_id: { type: DataTypes.UUID },
      cost: { type: DataTypes.FLOAT },
      expiration_at: { type: DataTypes.DATE },
    };

    await Promise.all(organizationTables.map((table) => Promise.all(Object.entries(fields).map(
      ([field, value]) => queryInterface.changeColumn(
        table,
        field,
        value,
        { transaction },
      ),
    ))));

    /* удаление device_type_id из таблиц tariffs и services */
    await Promise.all(removeDeviceType.map((table) => queryInterface.removeColumn(
      table,
      DEVICE_TYPE_ID_COLUMN,
      { transaction },
    )));

    /* создание таблиц  device_model_tariff_relations и device_model_service_relations вместе со связями */
    const deviceModelId = templates(DataTypes).foreign({
      table: 'device_models',
      onDelete: CASCADE,
      allowNull: false,
    });

    await Promise.all([
      queryInterface.createTable(
        TABLES.DEVICE_MODEL_TARIFF_RELATIONS,
        {
          id: templates(DataTypes).id,
          tariff_id: templates(DataTypes).foreign({
            table: 'tariffs',
            onDelete: CASCADE,
            allowNull: false,
          }),
          device_model_id: deviceModelId,
          ...templates(DataTypes).timestamps,
        },
        { transaction },
      ),
      queryInterface.createTable(
        TABLES.DEVICE_MODEL_SERVICE_RELATIONS,
        {
          id: templates(DataTypes).id,
          service_id: templates(DataTypes).foreign({
            table: 'services',
            onDelete: CASCADE,
            allowNull: false,
          }),
          device_model_id: deviceModelId,
          ...templates(DataTypes).timestamps,
        },
        { transaction },
      ),
    ]);

    /* привязка адреса обслуживания к устройству */
    await Promise.all([
      queryInterface.removeColumn(
        TABLES.ORGANIZATIONS,
        SERVICE_ADDRESS_ID_COLUMN,
        { transaction },
      ),
      queryInterface.addColumn(
        TABLES.ORGANIZATION_DEVICES,
        SERVICE_ADDRESS_ID_COLUMN,
        templates(DataTypes).foreign({ onDelete: CASCADE, table: TABLES.ADDRESSES }),
        { transaction },
      ),
    ]);

    /* исправление таблицы device_parameters */
    await Promise.all(Object.entries(templates(DataTypes).timestamps).map(([column, value]) => queryInterface.addColumn(
      TABLES.DEVICE_PARAMETERS,
      column,
      value,
      { transaction },
    )));
  }),
  down: async (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    /* возвращение адреса обслуживания в организацию */

    await Promise.all([
      queryInterface.removeColumn(
        TABLES.ORGANIZATION_DEVICES,
        SERVICE_ADDRESS_ID_COLUMN,
        { transaction },
      ),
      queryInterface.addColumn(
        TABLES.ORGANIZATIONS,
        SERVICE_ADDRESS_ID_COLUMN,
        templates(DataTypes).foreign({ onDelete: CASCADE, table: TABLES.ADDRESSES }),
        { transaction },
      ),
    ]);

    /* удаление таблиц  device_model_tariff_relations и device_model_service_relations вместе со связями */
    await Promise.all([
      queryInterface.dropTable(TABLES.DEVICE_MODEL_TARIFF_RELATIONS, { transaction }),
      queryInterface.dropTable(TABLES.DEVICE_MODEL_SERVICE_RELATIONS, { transaction }),
    ]);

    /* восстановление device_type_id в таблицах tariffs и services */
    await Promise.all(removeDeviceType.map(
      (table) => queryInterface.addColumn(
        table,
        DEVICE_TYPE_ID_COLUMN,
        templates(DataTypes).foreign({ onDelete: CASCADE, table: TABLES.DEVICE_TYPES }),
        { transaction },
      ),
    ));

    /* восстановление таблиц order_elements и order_element_types */
    await queryInterface.createTable(
      TABLES.ORDER_ELEMENT_TYPES,
      {
        id: templates(DataTypes).id,
        name: { type: DataTypes.STRING, allowNull: false, unique: true },
        alias: { type: DataTypes.STRING, allowNull: false, unique: true },
        model: { type: DataTypes.STRING, allowNull: false },
        ...templates(DataTypes).timestamps,
      },
      { transaction },
    );

    await queryInterface.createTable(
      TABLES.ORDER_ELEMENTS,
      {
        id: templates(DataTypes).id,
        order_id: templates(DataTypes).foreign({ table: TABLES.ORDERS, onDelete: CASCADE, allowNull: false }),
        order_element_type_id: templates(DataTypes).foreign({
          table: TABLES.ORDER_ELEMENT_TYPES,
          onDelete: CASCADE,
          allowNull: false,
        }),
        value: { type: DataTypes.UUID, allowNull: false },
        cost: { type: DataTypes.FLOAT },
        ...templates(DataTypes).timestamps,
      },
      { transaction },
    );

    /* удаление колонок order_id из organization_tariffs и organization_services */
    const fields = {
      organization_device_id: { type: DataTypes.UUID, allowNull: false },
      cost: { type: DataTypes.FLOAT, allowNull: false },
      expiration_at: { type: DataTypes.DATE, allowNull: false },
    };

    await Promise.all(organizationTables.map((table) => Promise.all(Object.entries(fields).map(
      ([field, value]) => queryInterface.changeColumn(
        table,
        field,
        value,
        { transaction },
      ),
    ))));

    await Promise.all([
      ...organizationTables.map((table) => queryInterface.removeColumn(
        table,
        ORDER_ID_COLUMN,
        { transaction },
      )),
      queryInterface.removeColumn(
        TABLES.ORGANIZATION_SERVICES,
        ORGANIZATION_DEVICE_ID_COLUMN,
        { transaction },
      ),
    ]);
  }),
};
