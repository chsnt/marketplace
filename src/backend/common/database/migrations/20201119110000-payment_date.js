const TABLE = 'payments';

const COLUMN = 'payment_date';

module.exports = {
  up: (queryInterface) => queryInterface.addColumn(TABLE, COLUMN, { type: 'TIMESTAMP' }),

  down: (queryInterface) => queryInterface.removeColumn(TABLE, COLUMN),
};
