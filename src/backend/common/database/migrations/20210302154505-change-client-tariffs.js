import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  CLIENT_TARIFFS: 'client_tariffs',
  CLIENTS: 'clients',
};

const COLUMN_CLIENT_ID = 'client_id';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.addColumn(
    TABLES.CLIENT_TARIFFS,
    COLUMN_CLIENT_ID,
    dbTemplates(DataTypes).foreign({ table: TABLES.CLIENTS }),
  ),

  down: (queryInterface, DataTypes) => queryInterface.removeColumn(TABLES.CLIENT_TARIFFS, COLUMN_CLIENT_ID),
};
