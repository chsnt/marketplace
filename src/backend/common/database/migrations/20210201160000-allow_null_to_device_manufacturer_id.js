/* eslint-disable max-len */
const TABLES = {
  DEVICE_MODELS: 'device_models',
  LEADS: 'leads',
};
const COLUMNS = {
  DEVICE_MANUFACTURER_ID: 'device_manufacturer_id',
  IS_ENTITY: 'is_entity',
  IS_ORGANIZATION: 'is_organization',
};

module.exports = {
  up: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.sequelize
      .query(`ALTER TABLE ${TABLES.DEVICE_MODELS} ALTER COLUMN ${COLUMNS.DEVICE_MANUFACTURER_ID} DROP NOT NULL`, { transaction }),
    queryInterface.renameColumn(TABLES.LEADS, COLUMNS.IS_ENTITY, COLUMNS.IS_ORGANIZATION, { transaction }),
  ])),


  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.sequelize
      .query(`ALTER TABLE ${TABLES.DEVICE_MODELS} ALTER COLUMN ${COLUMNS.DEVICE_MANUFACTURER_ID} SET NOT NULL`, { transaction }),
    queryInterface.renameColumn(TABLES.LEADS, COLUMNS.IS_ORGANIZATION, COLUMNS.IS_ENTITY, { transaction }),
  ])),
};
