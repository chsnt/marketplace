const TABLE = 'payments';

const COLUMNS = {
  DATA: 'data',
  DOCUMENT_NAME: 'document_name',
  PAID_BY_CARD: 'paid_by_card',
  PAYMENT_ORDER_ID: 'payment_order_id',
  PROMO_CODE: 'promo_code',
};

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.addColumn(TABLE, COLUMNS.DATA, { type: DataTypes.JSONB }, { transaction }),
    queryInterface.addColumn(TABLE, COLUMNS.PAID_BY_CARD, {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    }, { transaction }),
    queryInterface.addColumn(TABLE, COLUMNS.PAYMENT_ORDER_ID, { type: DataTypes.UUID }, { transaction }),
    queryInterface.removeColumn(TABLE, COLUMNS.DOCUMENT_NAME, { transaction }),
    queryInterface.changeColumn(TABLE, COLUMNS.PROMO_CODE, { type: DataTypes.STRING }, { transaction }),
  ])),
  down: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.removeColumn(TABLE, COLUMNS.DATA, { transaction }),
    queryInterface.removeColumn(TABLE, COLUMNS.PAID_BY_CARD, { transaction }),
    queryInterface.removeColumn(TABLE, COLUMNS.PAYMENT_ORDER_ID, { transaction }),
    queryInterface.addColumn(TABLE, COLUMNS.DOCUMENT_NAME, { type: DataTypes.STRING }, { transaction }),
    queryInterface.changeColumn(TABLE, COLUMNS.PROMO_CODE, {
      type: DataTypes.STRING,
      allowNull: false,
    }, { transaction }),
  ])),
};
