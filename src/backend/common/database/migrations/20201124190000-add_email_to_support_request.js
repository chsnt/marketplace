import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  SUPPORT_REQUESTS: 'support_requests',
  USERS: 'users',
};

const COLUMNS = {
  EMAIL: 'email',
  PHONE: 'phone',
  RESPONSIBLE: 'responsible_id',
};

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => {
    const templates = dbTemplates(DataTypes);
    return Promise.all([
      queryInterface.addColumn(TABLES.SUPPORT_REQUESTS, COLUMNS.EMAIL, { type: DataTypes.STRING }, { transaction }),
      queryInterface.sequelize.query(
        `ALTER TABLE ${TABLES.SUPPORT_REQUESTS} ALTER COLUMN ${COLUMNS.PHONE} DROP NOT NULL`,
        { transaction },
      ),
      queryInterface.addColumn(
        TABLES.SUPPORT_REQUESTS,
        COLUMNS.RESPONSIBLE,
        templates.foreign({ table: TABLES.USERS }),
        { transaction },
      ),
    ]);
  }),

  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.removeColumn(TABLES.SUPPORT_REQUESTS, COLUMNS.EMAIL, { transaction }),
    queryInterface.sequelize.query(
      `ALTER TABLE ${TABLES.SUPPORT_REQUESTS} ALTER COLUMN ${COLUMNS.PHONE} SET NOT NULL`,
      { transaction },
    ),
    queryInterface.removeColumn(TABLES.SUPPORT_REQUESTS, COLUMNS.RESPONSIBLE, { transaction }),
  ])),
};
