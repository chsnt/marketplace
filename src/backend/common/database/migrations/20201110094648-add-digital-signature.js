const ORGANIZATIONS_TABLE = 'organizations';
const COLUMNS = [
  'digital_signature_from', 'digital_signature_to',
];
module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all(COLUMNS.map(
    (column) => queryInterface.addColumn(ORGANIZATIONS_TABLE,
      column,
      { type: 'TIMESTAMP' },
      { transaction }),
  ))),

  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all(COLUMNS.map(
    (column) => queryInterface.removeColumn(ORGANIZATIONS_TABLE,
      column,
      { transaction }),
  ))),
};
