const TABLE = 'leads';

const COLUMN = 'email';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.addColumn(TABLE, COLUMN, { type: DataTypes.STRING }),

  down: (queryInterface) => queryInterface.removeColumn(TABLE, COLUMN),
};
