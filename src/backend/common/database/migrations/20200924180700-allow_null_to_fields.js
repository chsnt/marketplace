const TABLE = 'order_elements';
const COLUMN = 'cost';

export default {
  up: (queryInterface, DataTypes) => queryInterface.changeColumn(
    TABLE, COLUMN, { type: DataTypes.FLOAT },
  ),
  down: async (queryInterface, DataTypes) => queryInterface.changeColumn(
    TABLE, COLUMN, { type: DataTypes.FLOAT, allowNull: false },
  ),
};
