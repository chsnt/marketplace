/* eslint-disable no-console */
import { v4 as uuid } from 'uuid';
import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  CLIENTS: 'clients',
  ORGANIZATIONS: 'organizations',
  ORDERS: 'orders',
  SUPPORT_REQUESTS: 'support_requests',
  TICKETS: 'tickets',
  LEADS: 'leads',
  NOTIFICATIONS: 'notifications',
  USERS: 'users',

  OWNERSHIP_TYPES: 'ownership_types',
  BANK_DETAILS: 'bank_details',

  ORGANIZATION_SERVICES: 'organization_services',
  CLIENT_SERVICES: 'client_services',

  ORGANIZATION_TARIFFS: 'organization_tariffs',
  CLIENT_TARIFFS: 'client_tariffs',

  ORGANIZATION_TARIFF_SERVICES: 'organization_tariff_services',
  CLIENT_TARIFF_SERVICES: 'client_tariff_services',

  TICKET_ORGANIZATION_TARIFF_SERVICE_RELATIONS: 'ticket_organization_tariff_service_relations',
  TICKET_CLIENT_TARIFF_SERVICE_RELATIONS: 'ticket_client_tariff_service_relations',

  TICKET_ORGANIZATION_SERVICE_RELATIONS: 'ticket_organization_service_relations',
  TICKET_CLIENT_SERVICE_RELATIONS: 'ticket_client_service_relations',

  ORGANIZATION_DEVICES: 'organization_devices',
  CLIENT_DEVICES: 'client_devices',

  ORGANIZATION_DEVICE_PARAMETERS: 'organization_device_parameters',
  CLIENT_DEVICE_PARAMETERS: 'client_device_parameters',
};

const COLUMNS = {
  ORGANIZATION_ID: 'organization_id',
  CLIENT_ID: 'client_id',
  OGRN: 'ogrn',
  OGRNIP: 'ogrnip',
  OWNERSHIP_TYPE_ID: 'ownership_type_id',
  IS_MAINTAINER: 'is_maintainer',
};

const TABLES_TO_RENAME_TO_V2 = [
  TABLES.ORGANIZATION_SERVICES,
  TABLES.ORGANIZATION_TARIFFS,
  TABLES.ORGANIZATION_TARIFF_SERVICES,
  TABLES.TICKET_ORGANIZATION_TARIFF_SERVICE_RELATIONS,
  TABLES.TICKET_ORGANIZATION_SERVICE_RELATIONS,
  TABLES.ORGANIZATION_DEVICES,
  TABLES.ORGANIZATION_DEVICE_PARAMETERS,
];

const TABLES_TO_RENAME_TO_V1 = [
  TABLES.CLIENT_SERVICES,
  TABLES.CLIENT_TARIFFS,
  TABLES.CLIENT_TARIFF_SERVICES,
  TABLES.TICKET_CLIENT_TARIFF_SERVICE_RELATIONS,
  TABLES.TICKET_CLIENT_SERVICE_RELATIONS,
  TABLES.CLIENT_DEVICES,
  TABLES.CLIENT_DEVICE_PARAMETERS,
];

const ORGANIZATION_TO_CLIENT_TABLES = [
  TABLES.ORDERS,
  TABLES.SUPPORT_REQUESTS,
  TABLES.CLIENT_SERVICES,
  TABLES.CLIENT_TARIFFS,
  TABLES.CLIENT_TARIFF_SERVICES,
  TABLES.TICKET_CLIENT_TARIFF_SERVICE_RELATIONS,
  TABLES.TICKET_CLIENT_SERVICE_RELATIONS,
  TABLES.TICKETS,
  TABLES.CLIENT_DEVICES,
  TABLES.CLIENT_DEVICE_PARAMETERS,
  TABLES.LEADS,
  TABLES.NOTIFICATIONS,
];

const CLIENT_TO_ORGANIZATION_TABLES = [
  TABLES.ORDERS,
  TABLES.SUPPORT_REQUESTS,
  TABLES.ORGANIZATION_SERVICES,
  TABLES.ORGANIZATION_TARIFFS,
  TABLES.ORGANIZATION_TARIFF_SERVICES,
  TABLES.TICKET_ORGANIZATION_TARIFF_SERVICE_RELATIONS,
  TABLES.TICKET_ORGANIZATION_SERVICE_RELATIONS,
  TABLES.TICKETS,
  TABLES.ORGANIZATION_DEVICES,
  TABLES.ORGANIZATION_DEVICE_PARAMETERS,
  TABLES.LEADS,
  TABLES.NOTIFICATIONS,
];

const ORGANIZATION = 'organization';
const CLIENT = 'client';

const renameOrganizationToClient = (text = '') => text.replace(/organization/g, CLIENT);

const renameClientToOrganization = (text = '') => text.replace(/client/g, ORGANIZATION);

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const templates = dbTemplates(DataTypes);
    /* Создаем таблицу "клиенты" */
    await queryInterface.createTable(TABLES.CLIENTS, {
      id: templates.id,
      number: templates.number,
      // organization_id: templates.foreign({ table: TABLES.ORGANIZATIONS, onDelete: 'CASCADE' }),
      ...templates.timestamps,
      ...templates.by,
    }, { transaction });

    await Promise.all([
      queryInterface.addColumn(
        TABLES.ORGANIZATIONS,
        COLUMNS.CLIENT_ID,
        templates.foreign({ table: TABLES.CLIENTS }),
        { transaction },
      ),
      queryInterface.addColumn(
        TABLES.USERS,
        COLUMNS.CLIENT_ID,
        templates.foreign({ table: TABLES.CLIENTS }),
        { transaction },
      ),
      queryInterface.addColumn(
        TABLES.USERS,
        COLUMNS.IS_MAINTAINER,
        { type: DataTypes.BOOLEAN, defaultValue: false, allowNull: false },
        { transaction },
      ),
    ]);

    /* Получим всех существующие организации и создадим под каждого из них нового клиента */
    const organizations = await queryInterface.rawSelect(TABLES.ORGANIZATIONS,
      { raw: false, plain: false, transaction }, ['id']);
    const organizationClientMap = {};

    const clients = organizations.map(({ id }) => {
      const clientId = uuid();
      organizationClientMap[id] = clientId;
      return {
        id: clientId,
      };
    });

    if (clients.length) {
      await queryInterface.bulkInsert(TABLES.CLIENTS, clients, { transaction });
    }

    await Promise.all(organizations.map(({ id }) => queryInterface.bulkUpdate(
      TABLES.ORGANIZATIONS,
      { client_id: organizationClientMap[id] },
      { id },
      { transaction },
    )));

    const ticketColumns = await queryInterface.describeTable(TABLES.TICKETS, { transaction });
    if (Object.keys(ticketColumns).includes(COLUMNS.ORGANIZATION_ID)) {
      /* Удалим избыточную колонку ибо в таблице тикетов уже есть колонка с оборудованием клиента */
      await queryInterface.removeColumn(TABLES.TICKETS, COLUMNS.ORGANIZATION_ID, { transaction });
    }

    /* Переименуем таблицы с organization_ на client_ */
    await Promise.all(TABLES_TO_RENAME_TO_V2.map((tableName) => queryInterface.renameTable(
      tableName,
      renameOrganizationToClient(tableName),
      { transaction },
    )));

    /* Получим fk для нужных таблиц */
    const foreignKeysForAllTables = await queryInterface.getForeignKeysForTables(
      ORGANIZATION_TO_CLIENT_TABLES, { transaction },
    );

    const TABLES_TO_RECREATE_CLIENT_LINK = [];

    /* Переименуем fk ключи и колонки с organization_ на client_ */
    for (const tableName of ORGANIZATION_TO_CLIENT_TABLES) {
      /* Переименовываем колонки */
      const tableColumns = await queryInterface.describeTable(tableName, { transaction });
      for (const columnName of Object.keys(tableColumns)) {
        /* Исключаем из переименования колонку organization_id */
        if (columnName.includes(ORGANIZATION) && columnName !== COLUMNS.ORGANIZATION_ID) {
          await queryInterface.sequelize.query(`ALTER TABLE ${tableName}
            RENAME COLUMN ${columnName} TO ${renameOrganizationToClient(columnName)};`,
          { transaction });
        }
        if (columnName === COLUMNS.ORGANIZATION_ID) {
          TABLES_TO_RECREATE_CLIENT_LINK.push({
            tableName,
            allowNull: tableColumns[columnName].allowNull,
          });
        }
      }
      /* Переименовываем fk */
      const foreignKeys = foreignKeysForAllTables[tableName];
      for (const constraintName of foreignKeys) {
        if (constraintName.includes(ORGANIZATION)) {
          await queryInterface.sequelize.query(`ALTER TABLE ${tableName}
            RENAME CONSTRAINT ${constraintName} TO ${renameOrganizationToClient(constraintName)};`,
          { transaction });
        }
      }
    }

    /* Создаем колонку client_id рядом с organization_id, проставляем значения из карты и удаляем organization_id */
    await Promise.all(TABLES_TO_RECREATE_CLIENT_LINK.map(async ({ tableName, allowNull }) => {
      /* Добавляем client_id */
      await queryInterface.addColumn(
        tableName,
        COLUMNS.CLIENT_ID,
        templates.foreign({ table: TABLES.CLIENTS, onDelete: 'CASCADE' }),
        { transaction },
      );

      /* Получаем все строки из таблицы */
      const rows = await queryInterface.rawSelect(tableName,
        { raw: false, plain: false, transaction }, ['id']);

      /* Проставляем client_id */
      await Promise.all(rows.map(({ id, organization_id: organizationId }) => {
        if (!organizationId) return;
        return queryInterface.bulkUpdate(
          tableName,
          { [COLUMNS.CLIENT_ID]: organizationClientMap[organizationId] },
          { id },
          { transaction },
        );
      }));

      /* Устанавливаем в колонку правило allowNull */
      if (!allowNull) {
        await queryInterface.sequelize.query(
          `ALTER TABLE ${tableName} ALTER COLUMN ${COLUMNS.CLIENT_ID} SET NOT NULL`,
          { transaction },
        );
      }

      /* Удаляем колонку organization_id */
      await queryInterface.removeColumn(tableName, COLUMNS.ORGANIZATION_ID, { transaction });
    }));

    /* Удаляем ненужные колонки и справочник типов собственности */
    await Promise.all([
      queryInterface.removeColumn(TABLES.ORGANIZATIONS, COLUMNS.OGRN, { transaction }),
      queryInterface.removeColumn(TABLES.ORGANIZATIONS, COLUMNS.OGRNIP, { transaction }),
      queryInterface.removeColumn(TABLES.BANK_DETAILS, COLUMNS.OWNERSHIP_TYPE_ID, { transaction }),
    ]);

    return queryInterface.dropTable(TABLES.OWNERSHIP_TYPES, { transaction });
  }),

  down: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const templates = dbTemplates(DataTypes);
    /* Создаем справочник ownership_types */
    await queryInterface.createTable(TABLES.OWNERSHIP_TYPES, {
      id: templates.id,
      name: { type: DataTypes.STRING, allowNull: false },
      alias: { type: DataTypes.STRING, allowNull: false },
      ...templates.timestamps,
      ...templates.by,
    }, { transaction });
    /* Создаем колонки ogrn, ogrnip, ownership_type_id  */
    await Promise.all([
      queryInterface.addColumn(TABLES.ORGANIZATIONS, COLUMNS.OGRN, DataTypes.STRING, { transaction }),
      queryInterface.addColumn(TABLES.ORGANIZATIONS, COLUMNS.OGRNIP, DataTypes.STRING, { transaction }),
      queryInterface.addColumn(
        TABLES.BANK_DETAILS,
        COLUMNS.OWNERSHIP_TYPE_ID,
        templates.foreign({ table: TABLES.OWNERSHIP_TYPES }),
        { transaction },
      ),
    ]);

    /* Получим всех существующих клиентов и сформируем карту с организацией */
    const organizations = await queryInterface.rawSelect(TABLES.ORGANIZATIONS,
      { raw: false, plain: false, transaction }, ['id']);
    const clientOrganizationMap = organizations.reduce((acc, { id, client_id: clientId }) => {
      acc[clientId] = id;
      return acc;
    }, {});

    /* Переименуем таблицы с client_ на organization_ */
    await Promise.all(TABLES_TO_RENAME_TO_V1.map((tableName) => queryInterface.renameTable(
      tableName,
      renameClientToOrganization(tableName),
      { transaction },
    )));

    /* Получим fk для нужных таблиц */
    const foreignKeysForAllTables = await queryInterface.getForeignKeysForTables(
      CLIENT_TO_ORGANIZATION_TABLES, { transaction },
    );

    const TABLES_TO_RECREATE_CLIENT_LINK = [];

    /* Переименуем fk ключи и колонки с client_ на organization_ */
    for (const tableName of CLIENT_TO_ORGANIZATION_TABLES) {
      /* Переименовываем колонки */
      const tableColumns = await queryInterface.describeTable(tableName, { transaction });
      for (const columnName of Object.keys(tableColumns)) {
        /* Исключаем из переименования колонку organization_id */
        if (columnName.includes(CLIENT) && columnName !== COLUMNS.CLIENT_ID) {
          await queryInterface.sequelize.query(`ALTER TABLE ${tableName}
            RENAME COLUMN ${columnName} TO ${renameClientToOrganization(columnName)};`,
          { transaction });
        }
        if (columnName === COLUMNS.CLIENT_ID) {
          TABLES_TO_RECREATE_CLIENT_LINK.push({
            tableName,
            allowNull: tableColumns[columnName].allowNull,
          });
        }
      }
      /* Переименовываем fk */
      const foreignKeys = foreignKeysForAllTables[tableName];
      for (const constraintName of foreignKeys) {
        if (constraintName.includes(CLIENT)) {
          await queryInterface.sequelize.query(`ALTER TABLE ${tableName}
            RENAME CONSTRAINT ${constraintName} TO ${renameClientToOrganization(constraintName)};`,
          { transaction });
        }
      }
    }

    /* Создаем колонку client_id рядом с organization_id, проставляем значения из карты и удаляем organization_id */
    await Promise.all(TABLES_TO_RECREATE_CLIENT_LINK.map(async ({ tableName, allowNull }) => {
      /* Добавляем client_id */
      await queryInterface.addColumn(
        tableName,
        COLUMNS.ORGANIZATION_ID,
        templates.foreign({ table: TABLES.ORGANIZATIONS, onDelete: 'CASCADE' }),
        { transaction },
      );

      /* Получаем все строки из таблицы */
      const rows = await queryInterface.rawSelect(tableName,
        { raw: false, plain: false, transaction }, ['id']);

      /* Проставляем client_id */
      await Promise.all(rows.map(({ id, client_id: clientId }) => {
        if (!clientId) return;
        return queryInterface.bulkUpdate(
          tableName,
          { [COLUMNS.ORGANIZATION_ID]: clientOrganizationMap[clientId] },
          { id },
          { transaction },
        );
      }));

      /* Устанавливаем в колонку правило allowNull */
      if (!allowNull) {
        await queryInterface.sequelize.query(
          `ALTER TABLE ${tableName} ALTER COLUMN ${COLUMNS.ORGANIZATION_ID} SET NOT NULL`,
          { transaction },
        );
      }

      /* Удаляем колонку client_id */
      await queryInterface.removeColumn(tableName, COLUMNS.CLIENT_ID, { transaction });
    }));

    await Promise.all([
      queryInterface.removeColumn(
        TABLES.ORGANIZATIONS,
        COLUMNS.CLIENT_ID,
        { transaction },
      ),
      queryInterface.removeColumn(
        TABLES.USERS,
        COLUMNS.CLIENT_ID,
        { transaction },
      ),
      queryInterface.removeColumn(
        TABLES.USERS,
        COLUMNS.IS_MAINTAINER,
        { transaction },
      ),
    ]);

    return queryInterface.dropTable(TABLES.CLIENTS, { transaction });
  }),
};
