/* Миграция "Вне очереди" чтоб выпилить дефолтные поля со значениями id */
import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  LEAD_STATUESES: 'lead_statuses',
  LEAD_CONSULTATION_RESULTS: 'lead_consultation_results',
  LEAD_NEEDS: 'lead_needs',
  LEADS: 'leads',
  ORGANIZATIONS: 'organizations',
  CITIES: 'cities',
  DEVICE_MANUFACTURERS: 'device_manufacturers',
  DEVICE_MODELS: 'device_models',
};

const META_TABLES = [
  TABLES.LEAD_STATUESES,
  TABLES.LEAD_CONSULTATION_RESULTS,
  TABLES.LEAD_NEEDS,
];


module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const templates = dbTemplates(DataTypes);
    await Promise.all(META_TABLES.map((tableName) => queryInterface.createTable(
      tableName,
      {
        id: templates.id,
        name: { type: DataTypes.STRING, allowNull: false },
        alias: { type: DataTypes.STRING, allowNull: false },
        ...templates.timestamps,
      },
      { transaction },
    )));

    return queryInterface.createTable(TABLES.LEADS, {
      id: templates.id,
      number: templates.number,
      organization_id: templates.foreign({ table: TABLES.ORGANIZATIONS }),
      first_name: { type: DataTypes.STRING, alloNull: false },
      patronymic_name: { type: DataTypes.STRING },
      last_name: { type: DataTypes.STRING },
      phone: { type: DataTypes.STRING, allowNull: false },
      city_id: templates.foreign({ table: TABLES.CITIES }),
      device_manufacturer_id: templates.foreign({ table: TABLES.DEVICE_MANUFACTURERS }),
      device_model_id: templates.foreign({ table: TABLES.DEVICE_MODELS }),
      lead_need_id: templates.foreign({ table: TABLES.LEAD_NEEDS, allowNull: false }),
      lead_status_id: templates.foreign({ table: TABLES.LEAD_STATUESES, allowNull: false }),
      lead_consultation_result_id: templates.foreign({ table: TABLES.LEAD_CONSULTATION_RESULTS }),
      comment: { type: DataTypes.TEXT },
      ...templates.timestamps,
    }, { transaction });
  }),

  down: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.dropTable(TABLES.LEADS, { transaction });

    return Promise.all(META_TABLES.map((tableName) => queryInterface.dropTable(tableName, { transaction })));
  }),
};
