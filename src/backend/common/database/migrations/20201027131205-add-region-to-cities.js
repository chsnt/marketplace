import templates from '../../utils/dbTemplates';

const CITIES_TABLE = 'cities';

const tables = {
  federation_subjects: 'federation_subject_id',
  management_services: 'management_service_id',
};

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await Promise.all(Object.keys(tables).map((table) => queryInterface.createTable(
      table,
      {
        id: templates(DataTypes).id,
        name: { type: DataTypes.STRING, allowNull: false, unique: true },
        externalId: { type: DataTypes.STRING, unique: true },
        alias: { type: DataTypes.STRING, unique: true },
        ...(table === 'management_services' && { code: { type: DataTypes.STRING, unique: true } }),
        ...templates(DataTypes).timestamps,
      }, { transaction },
    )));

    await Promise.all(Object.entries(tables).map(([table, column]) => queryInterface.addColumn(
      CITIES_TABLE,
      column,
      templates(DataTypes).foreign({ table }),
      { transaction },
    )));
  }),
  down: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    await Promise.all(Object.entries(tables).map(([, column]) => queryInterface.removeColumn(
      CITIES_TABLE,
      column,
      { transaction },
    )));

    await Promise.all(Object.keys(tables).map((table) => queryInterface.dropTable(table,
      { transaction })));
  }),
};
