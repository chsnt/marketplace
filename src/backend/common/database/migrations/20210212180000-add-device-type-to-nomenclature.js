import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  SERVICES: 'services',
  TARIFFS: 'tariffs',
  DEVICE_TYPES: 'device_types',
};

const COLUMNS = {
  DEVICE_TYPE_ID: 'device_type_id',
};

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const templates = dbTemplates(DataTypes);

    return Promise.all([
      queryInterface.addColumn(
        TABLES.SERVICES, COLUMNS.DEVICE_TYPE_ID,
        templates.foreign({ table: TABLES.DEVICE_TYPES }),
        { transaction },
      ),
      queryInterface.addColumn(
        TABLES.TARIFFS, COLUMNS.DEVICE_TYPE_ID,
        templates.foreign({ table: TABLES.DEVICE_TYPES }),
        { transaction },
      ),
    ]);
  }),
  down: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => Promise.all([
    queryInterface.removeColumn(TABLES.SERVICES, COLUMNS.DEVICE_TYPE_ID, { transaction }),
    queryInterface.removeColumn(TABLES.TARIFFS, COLUMNS.DEVICE_TYPE_ID, { transaction }),
  ])),
};
