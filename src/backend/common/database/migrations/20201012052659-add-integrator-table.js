import templates from '../../utils/dbTemplates';

const INTEGRATIONS_TABLE = 'integrators';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.createTable(
    INTEGRATIONS_TABLE,
    {
      id: templates(DataTypes).id,
      token: { type: DataTypes.UUID, allowNull: false, unique: true },
      name: { type: DataTypes.STRING, allowNull: false, unique: true },
      ...templates(DataTypes).timestamps,
    },
  ),
  down: (queryInterface) => queryInterface.dropTable(INTEGRATIONS_TABLE),
};
