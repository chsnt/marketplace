import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  COUPONS: {
    NAME: 'coupons',
    COLUMNS: {
      DISCOUNT_TEMPLATE_ID: 'discount_template_id',
      EXPIRATION_AT: 'expiration_at',
      CODE: 'code',
    },
  },
  DISCOUNT_TEMPLATES: {
    NAME: 'discount_templates',
    COLUMNS: {
      DESCRIPTION: 'description',
      EXPIRATION_DAY: 'expiration_day',
      LIMIT: 'limit',
      DISCOUNT_TYPE_ID: 'discount_type_id',
      VALUE: 'value',
    },
  },
  DISCOUNT_TARIFF_SERVICE_RELATIONS: {
    NAME: 'discount_tariff_service_relations',
    COLUMNS: {
      TARIFF_ID: 'tariff_id',
      SERVICE_ID: 'service_id',
      DISCOUNT_TEMPLATE_ID: 'discount_template_id',
      VALUE: 'value',
    },
  },

  DISCOUNT_SERVICES: {
    NAME: 'discount_services',
    COLUMNS: {
      DISCOUNT_TEMPLATE_ID: 'discount_template_id',
      EXPIRATION_AT: 'expiration_at',
    },
  },

  DISCOUNT_TYPES: {
    NAME: 'discount_types',
    COLUMNS: {
      NAME: 'name',
      ALIAS: 'alias',
    },
  },
  ORDERS: {
    NAME: 'orders',
    COLUMNS: {
      COUPONS_ID: 'coupon_id',
      DISCOUNT: 'discount',
    },
  },
  SERVICES: {
    NAME: 'services',
    COLUMNS: {
      DISCOUNT_SERVICE_ID: 'discount_service_id',
    },
  },
  TARIFFS: {
    NAME: 'tariffs',
    COLUMNS: {
      DISCOUNT_SERVICE_ID: 'discount_service_id',
    },
  },
  DISCOUNT_BY_CARDS: {
    NAME: 'discount_by_cards',
    COLUMNS: {
      VALUE: 'value',
      EXPIRATION_AT: 'expiration_at',
    },
  },
  PAYMENTS: {
    NAME: 'payments',
    COLUMNS: {
      DISCOUNT_BY_CARD_ID: 'discount_by_card_id',
    },
  },
};

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const templates = dbTemplates(DataTypes);
    const createTemplates = {
      id: templates.id,
      ...templates.timestamps,
      ...templates.by,
    };

    await queryInterface.createTable(TABLES.DISCOUNT_TYPES.NAME, {
      ...createTemplates,
      [TABLES.DISCOUNT_TYPES.COLUMNS.NAME]: { type: DataTypes.STRING, allowNull: false, unique: true },
      [TABLES.DISCOUNT_TYPES.COLUMNS.ALIAS]: { type: DataTypes.STRING, allowNull: false, unique: true },
    }, { transaction });


    await queryInterface.createTable(TABLES.DISCOUNT_TEMPLATES.NAME, {
      ...createTemplates,
      [TABLES.DISCOUNT_TEMPLATES.COLUMNS.DESCRIPTION]: { type: DataTypes.STRING },
      [TABLES.DISCOUNT_TEMPLATES.COLUMNS.EXPIRATION_DAY]: {
        type: DataTypes.REAL,
        allowNull: false,
      },
      [TABLES.DISCOUNT_TEMPLATES.COLUMNS.LIMIT]: {
        type: DataTypes.REAL,
        allowNull: false,
      },
      [TABLES.DISCOUNT_TEMPLATES.COLUMNS.DISCOUNT_TYPE_ID]: templates.foreign({
        table: TABLES.DISCOUNT_TYPES.NAME,
        allowNull: false,
      }),
      [TABLES.DISCOUNT_TEMPLATES.COLUMNS.VALUE]: { type: DataTypes.INTEGER, allowNull: false, defaultValue: 0 },
    }, { transaction });


    await Promise.all([
      queryInterface.createTable(TABLES.COUPONS.NAME, {
        ...createTemplates,
        [TABLES.COUPONS.COLUMNS.DISCOUNT_TEMPLATE_ID]: templates.foreign({
          table: TABLES.DISCOUNT_TEMPLATES.NAME,
          allowNull: false,
          onDelete: 'CASCADE',
        }),

        [TABLES.COUPONS.COLUMNS.EXPIRATION_AT]: { type: 'TIMESTAMP' },
        [TABLES.COUPONS.COLUMNS.CODE]: { type: DataTypes.STRING, allowNull: false, unique: true },
      }, { transaction }),

      queryInterface.createTable(TABLES.DISCOUNT_TARIFF_SERVICE_RELATIONS.NAME, {
        ...createTemplates,
        [TABLES.DISCOUNT_TARIFF_SERVICE_RELATIONS.COLUMNS.TARIFF_ID]: templates.foreign({
          table: TABLES.TARIFFS.NAME,
        }),
        [TABLES.DISCOUNT_TARIFF_SERVICE_RELATIONS.COLUMNS.SERVICE_ID]: templates.foreign({
          table: TABLES.SERVICES.NAME,
        }),
        [TABLES.DISCOUNT_TARIFF_SERVICE_RELATIONS.COLUMNS.DISCOUNT_TEMPLATE_ID]: templates.foreign({
          table: TABLES.DISCOUNT_TEMPLATES.NAME,
          allowNull: false,
        }),
        [TABLES.DISCOUNT_TARIFF_SERVICE_RELATIONS.COLUMNS.VALUE]: { type: DataTypes.INTEGER },
      }, { transaction }),


      queryInterface.createTable(TABLES.DISCOUNT_SERVICES.NAME, {
        ...createTemplates,

        [TABLES.DISCOUNT_SERVICES.COLUMNS.DISCOUNT_TEMPLATE_ID]: templates.foreign({
          table: TABLES.DISCOUNT_TEMPLATES.NAME,
          allowNull: false,
        }),
        [TABLES.DISCOUNT_SERVICES.COLUMNS.EXPIRATION_AT]: { type: 'TIMESTAMP' },
      }, { transaction }),

      queryInterface.createTable(TABLES.DISCOUNT_BY_CARDS.NAME, {
        ...createTemplates,
        [TABLES.DISCOUNT_BY_CARDS.COLUMNS.EXPIRATION_AT]: { type: 'TIMESTAMP' },
        [TABLES.DISCOUNT_BY_CARDS.COLUMNS.VALUE]: { type: DataTypes.INTEGER, allowNull: false },
      }, { transaction }),
    ]);

    await Promise.all([
      queryInterface.addColumn(
        TABLES.ORDERS.NAME,
        TABLES.ORDERS.COLUMNS.COUPONS_ID,
        templates.foreign({ table: TABLES.COUPONS.NAME }),
        { transaction },
      ),
      queryInterface.addColumn(
        TABLES.ORDERS.NAME,
        TABLES.ORDERS.COLUMNS.DISCOUNT,
        { type: DataTypes.INTEGER },
        { transaction },
      ),

      queryInterface.addColumn(
        TABLES.SERVICES.NAME,
        TABLES.SERVICES.COLUMNS.DISCOUNT_SERVICE_ID,
        templates.foreign({ table: TABLES.DISCOUNT_SERVICES.NAME }),
        { transaction },
      ),

      queryInterface.addColumn(
        TABLES.TARIFFS.NAME,
        TABLES.TARIFFS.COLUMNS.DISCOUNT_SERVICE_ID,
        templates.foreign({ table: TABLES.DISCOUNT_SERVICES.NAME }),
        { transaction },
      ),

      queryInterface.addColumn(
        TABLES.PAYMENTS.NAME,
        TABLES.PAYMENTS.COLUMNS.DISCOUNT_BY_CARD_ID,
        templates.foreign({ table: TABLES.DISCOUNT_BY_CARDS.NAME }),
        { transaction },
      ),
    ]);
  }),
  down: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    await Promise.all([
      queryInterface.removeColumn(TABLES.ORDERS.NAME, TABLES.ORDERS.COLUMNS.COUPONS_ID, { transaction }),
      queryInterface.removeColumn(TABLES.ORDERS.NAME, TABLES.ORDERS.COLUMNS.DISCOUNT, { transaction }),
      queryInterface.removeColumn(TABLES.PAYMENTS.NAME, TABLES.PAYMENTS.COLUMNS.DISCOUNT_BY_CARD_ID, { transaction }),
    ]);

    await Promise.all([
      queryInterface.dropTable(TABLES.COUPONS.NAME, { transaction }),
      queryInterface.dropTable(TABLES.DISCOUNT_TARIFF_SERVICE_RELATIONS.NAME, { transaction }),
      queryInterface.dropTable(TABLES.DISCOUNT_BY_CARDS.NAME, { transaction }),
    ]);

    await queryInterface.dropTable(TABLES.DISCOUNT_TEMPLATES.NAME, { transaction });
    await queryInterface.dropTable(TABLES.DISCOUNT_TYPES.NAME, { transaction });
  }),
};
