const TABLE = 'orders';
const COLUMN = 'order_date';

module.exports = {
  up: (queryInterface) => queryInterface.addColumn(TABLE, COLUMN, 'TIMESTAMP'),

  down: (queryInterface) => queryInterface.removeColumn(TABLE, COLUMN),
};
