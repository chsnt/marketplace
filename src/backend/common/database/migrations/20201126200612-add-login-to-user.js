const USERS_TABLE = 'users';

const COLUMNS = {
  PHONE: 'phone',
  PASSWORD: 'password',
  LOGIN: 'login',
};

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.sequelize
      .query(`ALTER TABLE ${USERS_TABLE} ALTER COLUMN ${COLUMNS.PHONE} DROP NOT NULL`, { transaction }),
    queryInterface.sequelize
      .query(`ALTER TABLE ${USERS_TABLE} ALTER COLUMN ${COLUMNS.PASSWORD} DROP NOT NULL`, { transaction }),
    queryInterface.addColumn(USERS_TABLE, COLUMNS.LOGIN, { type: DataTypes.STRING }, { transaction }),
  ])),

  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.sequelize
      .query(`ALTER TABLE ${USERS_TABLE} ALTER COLUMN ${COLUMNS.PHONE} SET NOT NULL`, { transaction }),
    queryInterface.sequelize
      .query(`ALTER TABLE ${USERS_TABLE} ALTER COLUMN ${COLUMNS.PASSWORD} SET NOT NULL`, { transaction }),
    queryInterface.removeColumn(USERS_TABLE, COLUMNS.LOGIN, { transaction }),
  ])),
};
