import templates from '../../utils/dbTemplates';

const now = new Date();

const ELECTRONIC_DOCUMENT_CIRCULATIONS_TABLE = 'electronic_document_circulations';
const ELECTRONIC_DOCUMENT_CIRCULATION_DESCRIPTIONS_TABLE = 'electronic_document_circulation_descriptions';
const BANK_DETAILS_TABLE = 'bank_details';

const ELECTRONIC_DOCUMENT_CIRCULATION_ID_COLUMN = 'electronic_document_circulation_id';


module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.createTable(
      ELECTRONIC_DOCUMENT_CIRCULATIONS_TABLE,
      {
        id: templates(DataTypes).id,
        name: { type: DataTypes.STRING, allowNull: false },
        alias: { type: DataTypes.STRING, allowNull: false },
        external_id: { type: DataTypes.STRING, unique: true },
        ...templates(DataTypes).timestamps,
      },
      { transaction },
    );

    await queryInterface.createTable(
      ELECTRONIC_DOCUMENT_CIRCULATION_DESCRIPTIONS_TABLE,
      {
        id: templates(DataTypes).id,
        electronic_document_circulation_id: templates(DataTypes).foreign({
          table: ELECTRONIC_DOCUMENT_CIRCULATIONS_TABLE,
          onDelete: 'CASCADE',
          allowNull: false,
        }),
        bank_detail_id: templates(DataTypes).foreign({
          table: BANK_DETAILS_TABLE,
          onDelete: 'CASCADE',
          allowNull: false,
        }),

        description: { type: DataTypes.STRING, allowNull: false },
        ...templates(DataTypes).timestamps,
      },
      { transaction },
    );

    /* заполнение происходит для того, что бы можно было проставить данные для уже созданных записей в таблице bank_details */
    await queryInterface.bulkInsert(
      ELECTRONIC_DOCUMENT_CIRCULATIONS_TABLE,
      [
        {
          id: '79f09207-352e-4ae0-86dc-bd77ee0086b3',
          name: 'Нет ЭДО',
          alias: 'EDO_IS_MISSING',
        },
        {
          id: 'abe383da-dd10-4944-8ca7-96b5c55b7c18',
          name: 'Моего оператора нет в списке',
          alias: 'NOT_IN_LIST',
        },
      ].map((operator) => ({
        ...operator,
        created_at: now,
        updated_at: now,
      })),
      { transaction },
    );

    await queryInterface.addColumn(
      BANK_DETAILS_TABLE,
      ELECTRONIC_DOCUMENT_CIRCULATION_ID_COLUMN,
      templates(DataTypes).foreign({
        allowNull: false,
        table: ELECTRONIC_DOCUMENT_CIRCULATIONS_TABLE,
        defaultValue: '79f09207-352e-4ae0-86dc-bd77ee0086b3',
      }),
      { transaction },
    );
  }),
  down: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.removeColumn(BANK_DETAILS_TABLE, ELECTRONIC_DOCUMENT_CIRCULATION_ID_COLUMN, { transaction });
    await queryInterface.dropTable(ELECTRONIC_DOCUMENT_CIRCULATION_DESCRIPTIONS_TABLE, { transaction });
    await queryInterface.dropTable(ELECTRONIC_DOCUMENT_CIRCULATIONS_TABLE, { transaction });
  }),
};
