import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  COSTS: 'costs',
  SERVICES: 'services',
  TARIFFS: 'tariffs',
};

const COLUMNS = {
  COST: 'cost',
};

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const templates = dbTemplates(DataTypes);

    await queryInterface.createTable(TABLES.COSTS, {
      id: templates.id,
      tariff_or_service_id: { type: DataTypes.UUID, allowNull: false },
      value: { type: DataTypes.FLOAT, allowNull: false },
      start_at: { type: 'TIMESTAMP', allowNull: false },
      expiration_at: { type: 'TIMESTAMP' },
      ...templates.timestamps,
      ...templates.by,
    }, { transaction });

    const serviceCosts = await queryInterface.rawSelect(TABLES.SERVICES,
      { raw: false, plain: false, transaction }, ['id', 'cost']);

    const tariffCosts = await queryInterface.rawSelect(TABLES.TARIFFS,
      { raw: false, plain: false, transaction }, ['id', 'cost']);

    if (serviceCosts.length > 0 || tariffCosts.length > 0) {
      await queryInterface.bulkInsert(TABLES.COSTS, [...serviceCosts, ...tariffCosts].map(
        // eslint-disable-next-line camelcase
        ({ id: tariff_or_service_id, cost: value, created_at: start_at }) => ({
          tariff_or_service_id, value, start_at,
        }),
      ), { transaction });
    }


    await Promise.all([
      queryInterface.removeColumn(TABLES.SERVICES, COLUMNS.COST, { transaction }),
      queryInterface.removeColumn(TABLES.TARIFFS, COLUMNS.COST, { transaction }),
    ]);
  }),
  down: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await Promise.all([
      queryInterface.dropTable(TABLES.COSTS, { transaction }),
      queryInterface.addColumn(TABLES.SERVICES,
        COLUMNS.COST,
        { type: DataTypes.FLOAT, allowNull: false, defaultValue: 0 },
        { transaction }),
      queryInterface.addColumn(TABLES.TARIFFS,
        COLUMNS.COST,
        { type: DataTypes.FLOAT, allowNull: false, defaultValue: 0 },
        { transaction }),
    ]);
  }),
};
