const SERVICE_TABLE = 'services';
const DESCRIPTION_COLUMN = 'description';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.addColumn(
    SERVICE_TABLE,
    DESCRIPTION_COLUMN,
    { type: DataTypes.STRING(1024) },
  ),
  down: (queryInterface) => queryInterface.removeColumn(SERVICE_TABLE, DESCRIPTION_COLUMN),
};
