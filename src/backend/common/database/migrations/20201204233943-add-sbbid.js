const TABLES = ['users', 'organizations'];

const SBBID_ID_COLUMN = 'sbbid_id';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all(
    TABLES.map((table) => queryInterface.addColumn(
      table, SBBID_ID_COLUMN, { type: DataTypes.STRING, unique: true }, { transaction },
    )),
  )),
  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all(
    TABLES.map((table) => queryInterface.removeColumn(
      table, SBBID_ID_COLUMN, { transaction },
    )),
  )),
};
