export default {
  up: (queryInterface, DataTypes) => queryInterface.changeColumn('request_logs', 'url', { type: DataTypes.TEXT }),
  down: async () => {}, // do nothing
};
