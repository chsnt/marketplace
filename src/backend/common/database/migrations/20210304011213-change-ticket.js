const TABLE = 'tickets';
const COLUMN = 'client_device_id';

module.exports = {
  up: (queryInterface) => queryInterface.sequelize.query(
    `ALTER TABLE ${TABLE} ALTER COLUMN ${COLUMN} DROP NOT NULL;`,
  ),
  down: (queryInterface) => queryInterface.sequelize.query(
    `ALTER TABLE ${TABLE} ALTER COLUMN ${COLUMN} SET NOT NULL;`,
  ),
};
