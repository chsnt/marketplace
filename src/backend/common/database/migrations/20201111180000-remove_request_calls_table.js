import templates from '../../utils/dbTemplates';

const TABLE = 'request_phone_calls';

module.exports = {
  up: (queryInterface) => queryInterface.dropTable(TABLE),
  down: (queryInterface, DataTypes) => queryInterface.createTable(TABLE, {
    id: templates(DataTypes).id,
    name: { type: DataTypes.STRING },
    number: { type: DataTypes.STRING },
    email: { type: DataTypes.STRING },
    ...templates(DataTypes).timestamps,
  }),
};
