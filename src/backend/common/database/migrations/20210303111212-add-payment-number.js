const TABLE = 'payments';
const COLUMN = 'number';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.addColumn(
    TABLE, COLUMN,
    { type: DataTypes.INTEGER, autoIncrement: true },
  ),
  down: (queryInterface) => queryInterface.removeColumn(TABLE, COLUMN),
};
