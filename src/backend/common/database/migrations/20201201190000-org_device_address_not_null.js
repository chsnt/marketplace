const TABLE = 'organization_devices';

const COLUMN = 'service_address_id';

module.exports = {
  up: (queryInterface) => queryInterface.sequelize
    .query(`ALTER TABLE ${TABLE} ALTER COLUMN ${COLUMN} SET NOT NULL`),

  down: (queryInterface) => queryInterface.sequelize
    .query(`ALTER TABLE ${TABLE} ALTER COLUMN ${COLUMN} DROP NOT NULL`),
};
