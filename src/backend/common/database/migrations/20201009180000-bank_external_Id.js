import dbTemplates from '../../utils/dbTemplates';

const TABLE = 'banks';
const EXTERNAL_ID_COLUMN = 'external_id';
const ID_COLUMN = 'id';
const ORDER_DOCUMENTS_TABLE = 'order_documents';
const DATE_COLUMN = 'date';

const ORDER_DOCUMENT_TYPE_TABLE = 'order_document_types';
const TIMESTAMP_COLUMNS = ['created_at', 'updated_at', 'deleted_at'];

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.addColumn(TABLE, EXTERNAL_ID_COLUMN, { type: DataTypes.STRING, unique: true }, { transaction }),
    queryInterface.changeColumn(TABLE, ID_COLUMN, dbTemplates(DataTypes).id, { transaction }),
    queryInterface.addColumn(ORDER_DOCUMENTS_TABLE, DATE_COLUMN, { type: DataTypes.DATEONLY }, { transaction }),
    ...TIMESTAMP_COLUMNS.map((COLUMN) => queryInterface
      .addColumn(ORDER_DOCUMENT_TYPE_TABLE, COLUMN, dbTemplates(DataTypes).timestamps[COLUMN], { transaction })),
  ])),
  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.removeColumn(TABLE, EXTERNAL_ID_COLUMN, { transaction }),
    queryInterface.removeColumn(ORDER_DOCUMENTS_TABLE, DATE_COLUMN, { transaction }),
  ])),
};
