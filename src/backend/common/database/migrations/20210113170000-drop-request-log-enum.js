const TYPE = 'enum_request_logs_method';
const REQUEST_LOGS = 'request_logs';
const METHOD = 'method';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.changeColumn(REQUEST_LOGS, METHOD, {
      type: DataTypes.STRING,
      allowNuLL: false,
    }, { transaction });

    return queryInterface.sequelize.query(`DROP TYPE ${TYPE};`, { transaction });
  }),

  down: async () => {},
};
