import templates from '../../utils/dbTemplates';

const TABLES = {
  DEVICES: 'devices',
  DEVICE_MODELS: 'device_models',
  DEVICE_TYPES: 'device_types',
  DEVICE_MANUFACTURERS: 'device_manufacturers',
  ORGANIZATION_DEVICES: 'organization_devices',
  DEVICE_PARAMETERS: 'device_parameters',
  USERS: 'users',
  ORGANIZATIONS: 'organizations',
  BANKS: 'banks',
  BANK_DETAILS: 'bank_details',
  ORDER_ELEMENT_TYPES: 'order_element_types',
  ORDER_ELEMENTS: 'order_elements',
  ORDERS: 'orders',
};

const COLUMNS = {
  EXTERNAL_ID: 'external_id',
  DEVICE_ID: 'device_id',
  DEVICE_MODEL_ID: 'device_model_id',
  CORRESPONDENT_ACCOUNT: 'correspondent_account',
  RCBIC: 'rcbic',
  EDO: 'edo',
  ID: 'id',
  BANK: 'bank',
  BANK_ID: 'bank_id',
};

const FKEYS = {
  ORGANIZATION_DEVICES_DEVICE_ID: 'organization_devices_device_id_fkey',
  DEVICE_PARAMETERS_DEVICE_ID: 'device_parameters_device_id_fkey',
  ORGANIZATION_DEVICES_DEVICE_MODEL_ID: 'organization_devices_device_model_id_fkey',
  DEVICE_PARAMETERS_DEVICE_MODEL_ID: 'device_parameters_device_model_id_fkey',
  BANK_DETAILS_BANK_ID: 'bank_details_bank_id_fkey',
};

const FOREIGN_KEY = 'FOREIGN KEY';
const CASCADE = 'CASCADE';

const commonAttr = {
  type: FOREIGN_KEY,
  onDelete: CASCADE,
  onUpdate: CASCADE,
};

const tablesWithExternalId = [TABLES.DEVICE_TYPES, TABLES.DEVICE_MANUFACTURERS, TABLES.USERS, TABLES.ORGANIZATIONS];
const bankColumns = [COLUMNS.CORRESPONDENT_ACCOUNT, COLUMNS.RCBIC, COLUMNS.BANK];

module.exports = {
  up: async (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    /* добавление колонки  external_id */
    await Promise.all([...tablesWithExternalId, TABLES.DEVICES].map((table) => queryInterface.addColumn(
      table,
      COLUMNS.EXTERNAL_ID,
      { type: DataTypes.STRING, unique: true },
      { transaction },
    )));

    const attributesOrganizationDevices = {
      ...commonAttr,
      fields: [COLUMNS.DEVICE_MODEL_ID],
      name: FKEYS.ORGANIZATION_DEVICES_DEVICE_MODEL_ID,
      references: {
        table: TABLES.DEVICE_MODELS,
        field: COLUMNS.ID,
      },
      transaction,
    };

    const attributesDeviceParameters = {
      ...commonAttr,
      fields: [COLUMNS.DEVICE_MODEL_ID],
      name: FKEYS.DEVICE_PARAMETERS_DEVICE_MODEL_ID,
      references: {
        table: TABLES.DEVICE_MODELS,
        field: COLUMNS.ID,
      },
      transaction,
    };

    /* отвязывание ключей */
    await Promise.all([
      queryInterface.removeConstraint(
        TABLES.ORGANIZATION_DEVICES,
        FKEYS.ORGANIZATION_DEVICES_DEVICE_ID,
        { transaction },
      ),
      queryInterface.removeConstraint(
        TABLES.DEVICE_PARAMETERS,
        FKEYS.DEVICE_PARAMETERS_DEVICE_ID,
        { transaction },
      ),
    ]);

    /* переименование таблицы */
    await queryInterface.renameTable(TABLES.DEVICES, TABLES.DEVICE_MODELS, { transaction });

    /* переименование полей */
    await Promise.all([
      queryInterface.renameColumn(
        TABLES.DEVICE_PARAMETERS,
        COLUMNS.DEVICE_ID,
        COLUMNS.DEVICE_MODEL_ID,
        { transaction },
      ),
      queryInterface.renameColumn(
        TABLES.ORGANIZATION_DEVICES,
        COLUMNS.DEVICE_ID,
        COLUMNS.DEVICE_MODEL_ID,
        { transaction },
      ),
    ]);

    /* связывание ключей */
    await Promise.all([
      queryInterface.addConstraint(TABLES.ORGANIZATION_DEVICES, attributesOrganizationDevices),
      queryInterface.addConstraint(TABLES.DEVICE_PARAMETERS, attributesDeviceParameters),
    ]);

    /* добавление колонки  edo в таблицу organizations */
    await queryInterface.addColumn(
      TABLES.ORGANIZATIONS,
      COLUMNS.EDO,
      {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      { transaction },
    );

    /* удаление колонок correspondent_account и rcbic из таблицы  bank_details */
    await Promise.all(bankColumns.map((column) => queryInterface.removeColumn(
      TABLES.BANK_DETAILS,
      column,
      { transaction },
    )));

    /* создать таблицу banks */
    await queryInterface.createTable(
      TABLES.BANKS,
      {
        id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
        name: { type: DataTypes.STRING, allowNull: false, unique: true },
        rcbic: { type: DataTypes.STRING(9), allowNull: false },
        correspondent_account: { type: DataTypes.STRING(20), allowNull: false },
        ...templates(DataTypes).timestamps,
      },
      { transaction },
    );

    await queryInterface.addColumn(
      TABLES.BANK_DETAILS,
      COLUMNS.BANK_ID,
      {
        type: DataTypes.UUID,
      },
      { transaction },
    );

    const attributesBankId = {
      ...commonAttr,
      fields: [COLUMNS.BANK_ID],
      name: FKEYS.BANK_DETAILS_BANK_ID,
      references: {
        table: TABLES.BANKS,
        field: COLUMNS.ID,
      },
      transaction,
    };

    await queryInterface.addConstraint(TABLES.BANK_DETAILS, attributesBankId);
  }),

  down: async (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    /* удаление колонки external_id */
    await Promise.all([...tablesWithExternalId, TABLES.DEVICE_MODELS].map((table) => queryInterface.removeColumn(
      table,
      COLUMNS.EXTERNAL_ID,
      { transaction },
    )));

    /* перенос колонок correspondent_account и rcbic из таблицы  bank_details в таблицу banks */
    await queryInterface.removeColumn(TABLES.BANK_DETAILS, COLUMNS.BANK_ID, { transaction });

    await queryInterface.dropTable(TABLES.BANKS, { transaction });

    await Promise.all(bankColumns.map((column) => queryInterface.addColumn(
      TABLES.BANK_DETAILS,
      column,
      {
        type: DataTypes.STRING(
          // eslint-disable-next-line no-nested-ternary
          (column === COLUMNS.RCBIC) ? 9
            : (column === COLUMNS.CORRESPONDENT_ACCOUNT) ? 20
              : 255,
        ),
      },
      { transaction },
    )));

    /* удаление колонки  edo в таблицу organizations */
    await queryInterface.removeColumn(TABLES.ORGANIZATIONS, COLUMNS.EDO, { transaction });


    /* переименование таблицы device_models в devices */
    /* отвязка ключей */
    await Promise.all([
      queryInterface.removeConstraint(
        TABLES.ORGANIZATION_DEVICES,
        FKEYS.ORGANIZATION_DEVICES_DEVICE_MODEL_ID,
        { transaction },
      ),
      queryInterface.removeConstraint(
        TABLES.DEVICE_PARAMETERS,
        FKEYS.DEVICE_PARAMETERS_DEVICE_MODEL_ID,
        { transaction },
      ),
    ]);

    /* переименование таблицы */
    await queryInterface.renameTable(TABLES.DEVICE_MODELS, TABLES.DEVICES, { transaction });

    /* переименование полей */
    await Promise.all([
      queryInterface.renameColumn(
        TABLES.DEVICE_PARAMETERS,
        COLUMNS.DEVICE_MODEL_ID,
        COLUMNS.DEVICE_ID,
        { transaction },
      ),
      queryInterface.renameColumn(
        TABLES.ORGANIZATION_DEVICES,
        COLUMNS.DEVICE_MODEL_ID,
        COLUMNS.DEVICE_ID,
        { transaction },
      ),
    ]);

    const attributesOrganizationDevices = {
      ...commonAttr,
      fields: [COLUMNS.DEVICE_ID],
      name: FKEYS.ORGANIZATION_DEVICES_DEVICE_ID,
      references: {
        table: TABLES.DEVICES,
        field: COLUMNS.ID,
      },
      transaction,
    };

    const attributesDeviceParameters = {
      ...commonAttr,
      fields: [COLUMNS.DEVICE_ID],
      name: FKEYS.DEVICE_PARAMETERS_DEVICE_ID,
      references: {
        table: TABLES.DEVICES,
        field: COLUMNS.ID,
      },
      transaction,
    };

    /* связывание ключей */
    await Promise.all([
      queryInterface.addConstraint(TABLES.ORGANIZATION_DEVICES, attributesOrganizationDevices),
      queryInterface.addConstraint(TABLES.DEVICE_PARAMETERS, attributesDeviceParameters),
    ]);
  }),
};
