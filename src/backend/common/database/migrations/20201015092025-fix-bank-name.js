module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.removeConstraint(
    'banks',
    'banks_name_key',
  ),
  down: async () => {}, // do nothing
};
