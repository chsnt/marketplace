import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  AVAILABLE_PAYMENTS: 'available_payment_methods',
  SERVICES: 'services',
  TARIFFS: 'tariffs',
};

const COLUMNS = {
  AVAILABLE_PAYMENT_ID: 'available_payment_method_id',
};
module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const templates = dbTemplates(DataTypes);

    await queryInterface.createTable(TABLES.AVAILABLE_PAYMENTS, {
      id: templates.id,
      name: { type: DataTypes.STRING, allowNull: false },
      alias: { type: DataTypes.STRING, allowNull: false },
      ...templates.timestamps,
      ...templates.by,
    }, { transaction });

    await Promise.all([
      queryInterface.addColumn(
        TABLES.SERVICES,
        COLUMNS.AVAILABLE_PAYMENT_ID,
        dbTemplates(DataTypes).foreign({ table: TABLES.AVAILABLE_PAYMENTS }),
        { transaction },
      ),

      queryInterface.addColumn(
        TABLES.TARIFFS,
        COLUMNS.AVAILABLE_PAYMENT_ID,
        dbTemplates(DataTypes).foreign({ table: TABLES.AVAILABLE_PAYMENTS }),
        { transaction },
      ),
    ]);
  }),

  down: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    await Promise.all([
      queryInterface.removeColumn(TABLES.SERVICES, COLUMNS.AVAILABLE_PAYMENT_ID, { transaction }),
      queryInterface.removeColumn(TABLES.TARIFFS, COLUMNS.AVAILABLE_PAYMENT_ID, { transaction }),
    ]);

    await queryInterface.dropTable(TABLES.AVAILABLE_PAYMENTS, { transaction });
  }),
};
