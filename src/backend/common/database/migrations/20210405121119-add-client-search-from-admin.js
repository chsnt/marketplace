module.exports = {
  up: async (queryInterface) => queryInterface.sequelize.query(`
  CREATE VIEW v_clients AS
  SELECT "ClientModel"."id",
    "ClientModel"."number",
    "ClientModel"."created_at" AS "created_at",
    
    (CASE WHEN organization.id NOTNULL THEN jsonb_build_object(
      'id', "organization"."id",
      'name', "organization"."name",
      'inn', "organization"."inn",
      'user', jsonb_build_object(
        'id',  "organization->users"."id",
        'firstName', "organization->users"."first_name",
        'lastName', "organization->users"."last_name",
        'patronymicName', "organization->users"."patronymic_name",
        'fullName',  concat_ws(' ', "organization->users"."first_name", 
            "organization->users"."last_name", "organization->users"."patronymic_name"),
        'email', "organization->users"."email",
        'phone', "organization->users"."phone",
        'isMaintainer', "organization->users"."is_maintainer"
      )
    ) END) AS organization,
    
    (CASE WHEN organization.id NOTNULL THEN  "organization"."name" ELSE concat_ws(' ', "user"."first_name", 
      "user"."last_name") END) AS name,
      
    (CASE WHEN organization.id ISNULL THEN
      jsonb_build_object(
        'id', "user"."id",
        'firstName', "user"."first_name",
        'lastName', "user"."last_name",
        'patronymicName', "user"."patronymic_name",
        'fullName',  concat_ws(' ', "user"."first_name", "user"."last_name", "user"."patronymic_name"),
        'email', "user"."email",
        'phone', "user"."phone"
      ) END ) AS individual,

    (CASE WHEN "updatedBy"."id" NOTNULL THEN
      jsonb_build_object(
      'id', "updatedBy"."id",
      'firstName', "updatedBy"."first_name",
      'patronymicName', "updatedBy"."patronymic_name",
      'fullName',  concat_ws(' ', "updatedBy"."first_name", "updatedBy"."last_name", "updatedBy"."patronymic_name"),
      'lastName', "updatedBy"."last_name"
    ) END ) AS responsible,
    
    (CASE WHEN organization.id NOTNULL THEN  'organization' ELSE 'individual' END) AS type

  FROM "clients" AS "ClientModel"
    LEFT OUTER JOIN "organizations" AS "organization"
      ON "ClientModel"."id" = "organization"."client_id" AND ("organization"."deleted_at" IS NULL)
    LEFT OUTER JOIN "users" AS "organization->users"
      ON "organization"."id" = "organization->users"."organization_id" AND ("organization->users"."deleted_at" IS NULL)
      AND "organization->users"."is_maintainer" IS true
    LEFT OUTER JOIN "users" AS "user"
      ON "ClientModel"."id" = "user"."client_id" AND ("user"."deleted_at" IS NULL)
    LEFT OUTER JOIN "users" AS "updatedBy"
      ON "ClientModel"."updated_by" = "updatedBy"."id" AND ("updatedBy"."deleted_at" IS NULL)
  WHERE "ClientModel"."deleted_at" IS NULL
`),

  down: async (queryInterface) => queryInterface.sequelize.query('DROP VIEW IF EXISTS v_clients;'),
};
