import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  LEADS: 'leads',
  DEVICE_TYPES: 'device_types',
  LEAD_TARIFF_RELATIONS: 'lead_tariff_relations',
  LEAD_SERVICE_RELATIONS: 'lead_service_relations',
  LEAD_SOURCES: 'lead_sources',
  TARIFFS: 'tariffs',
  SERVICES: 'services',
  LEAD_SOURCE_DEVICE_TYPE_RELATIONS: 'lead_source_device_type_relations',
};

const COLUMNS = {
  DEVICE_TYPE_ID: 'device_type_id',
  OPERATION_SYSTEM: 'operation_system',
  LEAD_SOURCE_ID: 'lead_source_id',
  NAME: 'name',
  ALIAS: 'alias',
  IS_ENTITY: 'is_entity',
};

const UNIQUE_INDEXES = {
  LEAD_TARIFF_UNIQUE: 'lead_id_tariff_id_unique',
  LEAD_SERVICE_UNIQUE: 'lead_id_service_id_unique',
  LEAD_SOURCE_DEVICE_TYPE_UNIQUE: 'lead_source_id_device_type_id_unique',
};

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const templates = dbTemplates(DataTypes);

    await Promise.all([
      /* Источники лида */
      queryInterface.createTable(TABLES.LEAD_SOURCES, {
        id: templates.id,
        name: { type: DataTypes.STRING, unique: true, allowNull: false },
        alias: { type: DataTypes.STRING, unique: true, allowNull: false },
        order: { type: DataTypes.INTEGER, autoIncrement: true, allowNull: false },
        ...templates.timestamps,
        ...templates.by,
      }, { transaction }),

      /* Связь лида и тарифа */
      queryInterface.createTable(TABLES.LEAD_TARIFF_RELATIONS, {
        id: templates.id,
        tariff_id: templates.foreign({
          table: TABLES.TARIFFS,
          allowNull: false,
          unique: UNIQUE_INDEXES.LEAD_TARIFF_UNIQUE,
        }),
        lead_id: templates.foreign({
          table: TABLES.LEADS,
          allowNull: false,
          unique: UNIQUE_INDEXES.LEAD_TARIFF_UNIQUE,
        }),
        ...templates.timestamps,
        ...templates.by,
      }, { transaction }),

      /* Связь лида и услуги */
      queryInterface.createTable(TABLES.LEAD_SERVICE_RELATIONS, {
        id: templates.id,
        service_id: templates.foreign({
          table: TABLES.SERVICES,
          allowNull: false,
          unique: UNIQUE_INDEXES.LEAD_SERVICE_UNIQUE,
        }),
        lead_id: templates.foreign({
          table: TABLES.LEADS,
          allowNull: false,
          unique: UNIQUE_INDEXES.LEAD_SERVICE_UNIQUE,
        }),
        ...templates.timestamps,
        ...templates.by,
      }, { transaction }),
    ]);

    await queryInterface.createTable(TABLES.LEAD_SOURCE_DEVICE_TYPE_RELATIONS,
      {
        id: templates.id,
        lead_source_id: templates.foreign({
          table: TABLES.LEAD_SOURCES,
          allowNull: false,
          unique: UNIQUE_INDEXES.LEAD_SOURCE_DEVICE_TYPE_UNIQUE,
        }),
        device_type_id: templates.foreign({
          table: TABLES.DEVICE_TYPES,
          allowNull: false,
          unique: UNIQUE_INDEXES.LEAD_SOURCE_DEVICE_TYPE_UNIQUE,
        }),
        ...templates.timestamps,
        ...templates.by,
      }, { transaction });

    return Promise.all([
      queryInterface.addColumn(TABLES.LEADS, COLUMNS.OPERATION_SYSTEM, DataTypes.STRING, { transaction }),
      queryInterface.addColumn(TABLES.LEADS, COLUMNS.DEVICE_TYPE_ID,
        templates.foreign({ table: TABLES.DEVICE_TYPES }), { transaction }),
      queryInterface.addColumn(TABLES.LEADS, COLUMNS.LEAD_SOURCE_ID,
        templates.foreign({ table: TABLES.LEAD_SOURCES }), { transaction }),
      queryInterface.addColumn(TABLES.LEADS, COLUMNS.IS_ENTITY, DataTypes.BOOLEAN, { transaction }),
    ]);
  }),

  down: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    await Promise.all([
      queryInterface.removeColumn(TABLES.LEADS, COLUMNS.OPERATION_SYSTEM, { transaction }),
      queryInterface.removeColumn(TABLES.LEADS, COLUMNS.DEVICE_TYPE_ID, { transaction }),
      queryInterface.removeColumn(TABLES.LEADS, COLUMNS.LEAD_SOURCE_ID, { transaction }),
      queryInterface.removeColumn(TABLES.LEADS, COLUMNS.IS_ENTITY, { transaction }),
    ]);

    await queryInterface.dropTable(TABLES.LEAD_SOURCE_DEVICE_TYPE_RELATIONS, { transaction });

    return Promise.all([
      queryInterface.dropTable(TABLES.LEAD_SOURCES, { transaction }),
      queryInterface.dropTable(TABLES.LEAD_SERVICE_RELATIONS, { transaction }),
      queryInterface.dropTable(TABLES.LEAD_TARIFF_RELATIONS, { transaction }),
    ]);
  }),
};
