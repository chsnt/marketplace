const TABLES = ['tariffs', 'services'];
const COLUMNS = ['label', 'icon'];

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all(
    TABLES.map((table) => COLUMNS.map((column) => queryInterface.addColumn(
      table, column, { type: DataTypes.STRING }, { transaction },
    ))).flat(),
  )),
  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all(
    TABLES.map((table) => COLUMNS.map((column) => queryInterface.removeColumn(
      table, column, { transaction },
    ))).flat(),
  )),
};
