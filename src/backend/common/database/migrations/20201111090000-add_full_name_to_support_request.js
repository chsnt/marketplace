const TABLE = 'support_requests';
const COLUMNS = [
  'first_name',
  'patronymic_name',
  'last_name',
  'phone',
];

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all(
    COLUMNS.map((column) => queryInterface.addColumn(
      TABLE,
      column,
      { type: DataTypes.STRING, ...(['first_name', 'phone'].includes(column) && { allowNull: false }) },
      { transaction },
    )),
  )),
  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all(
    COLUMNS.map((column) => queryInterface.removeColumn(
      TABLE,
      column,
      { transaction },
    )),
  )),
};
