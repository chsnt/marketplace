import templates from '../../utils/dbTemplates';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.createTable('request_phone_calls', {
    id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
    name: { type: DataTypes.STRING },
    number: { type: DataTypes.STRING },
    email: { type: DataTypes.STRING },
    ...templates(DataTypes).timestamps,
  }),
  down: (queryInterface) => queryInterface.dropTable('request_phone_calls'),
};
