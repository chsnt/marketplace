import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  TARIFF_SERVICE_RELATIONS: 'tariff_service_relations',
  LIMIT_MEASURES: 'limit_measures',
  CITIES: 'cities',
};

const TABLES_WITH_ALIASES = ['tax_types', 'ownership_types', 'device_types'];
const TABLES_FOR_EXTERNAL_ID = ['tariffs', 'services', 'tax_types', 'ownership_types'];
const EXTERNAL_ID_COLUMN = 'external_id';
const ALIAS_COLUMN = 'alias';

const LIMIT_MEASURE_COLUMN = 'limit_measure_id';

const UNIQUE_CONSTRAINT = 'tariff_service_relations_unique_key';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    /* Удаляем constraint */
    await Promise.all([
      ...TABLES_WITH_ALIASES.map((table) => queryInterface
        .removeConstraint(table, `${table}_${ALIAS_COLUMN}_key`, { transaction })),
      /* Проще пересоздать колонку, чем управлять констрейнтом */
      queryInterface.removeColumn(TABLES.TARIFF_SERVICE_RELATIONS, LIMIT_MEASURE_COLUMN, { transaction }),
    ]);
    /* Потом меняем тип полей */
    return Promise.all([
      /* Чтоб не плодить файлы миграций, здесь же создадим таблицу для хранения городов */
      queryInterface.createTable(TABLES.CITIES, {
        id: dbTemplates(DataTypes).id,
        name: { type: DataTypes.STRING, allowNull: false },
        external_id: { type: DataTypes.STRING, unique: true },
        is_testing_participant: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
        ...dbTemplates(DataTypes).timestamps,
      }, { transaction }),
      ...TABLES_FOR_EXTERNAL_ID.map((table) => queryInterface.addColumn(
        table, EXTERNAL_ID_COLUMN, { type: DataTypes.STRING, unique: true }, { transaction },
      )),
      ...TABLES_WITH_ALIASES.map((table) => queryInterface.changeColumn(
        table, ALIAS_COLUMN, { type: DataTypes.STRING, unique: false }, { transaction },
      )),
      queryInterface.addColumn(
        TABLES.TARIFF_SERVICE_RELATIONS, LIMIT_MEASURE_COLUMN, dbTemplates(DataTypes).foreign({
          table: TABLES.LIMIT_MEASURES,
        }),
        { transaction },
      ),
      queryInterface.addConstraint(TABLES.TARIFF_SERVICE_RELATIONS, {
        type: 'unique',
        fields: ['tariff_id', 'service_id'],
        name: UNIQUE_CONSTRAINT,
        where: { deleted_at: null },
        transaction,
      }),
    ]);
  }),
  down: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    ...TABLES_WITH_ALIASES.map((table) => queryInterface.changeColumn(
      table, ALIAS_COLUMN, { type: DataTypes.STRING, unique: true, allowNull: false }, { transaction },
    )),
    ...TABLES_FOR_EXTERNAL_ID.map((table) => queryInterface.removeColumn(
      table, EXTERNAL_ID_COLUMN, { transaction },
    )),
    queryInterface.changeColumn(
      TABLES.TARIFF_SERVICE_RELATIONS, LIMIT_MEASURE_COLUMN, dbTemplates(DataTypes).foreign({
        table: TABLES.LIMIT_MEASURES,
        allowNull: false,
      }),
      { transaction },
    ),
    queryInterface.dropTable(TABLES.CITIES, { transaction }),
    queryInterface.removeConstraint(TABLES.TARIFF_SERVICE_RELATIONS, UNIQUE_CONSTRAINT, { transaction }),
  ])),
};
