const TABLE = 'leads';

const COLUMN = 'form_id';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.bulkDelete('sequelize_meta',
      { name: '20201712180000-add_form_id_to_lead.js' },
      { transaction });

    const tableDefinition = await queryInterface.describeTable(TABLE);

    if (tableDefinition[COLUMN]) return;

    await queryInterface.addColumn(
      TABLE,
      COLUMN,
      { type: DataTypes.STRING },
      { transaction },
    );
  }),

  down: (queryInterface) => queryInterface.removeColumn(TABLE, COLUMN),
};
