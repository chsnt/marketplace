import templates from '../../utils/dbTemplates';

const TICKETS_TABLE = 'tickets';
const TICKET_REVIEWS_TABLE = 'ticket_reviews';
const TICKET_REVIEW_DOCUMENTS_TABLE = 'ticket_review_documents';
const ORGANIZATION_DEVICES_TABLE = 'organization_devices';
const ORGANIZATION_DEVICE_ID_COLUMN = 'organization_device_id';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.createTable(
      TICKET_REVIEW_DOCUMENTS_TABLE,
      {
        id: templates(DataTypes).id,
        ticket_review_id: templates(DataTypes).foreign({
          table: TICKET_REVIEWS_TABLE,
          onDelete: 'CASCADE',
          allowNull: false,
        }),
        name: { type: DataTypes.STRING, allowNull: false },
        ...templates(DataTypes).timestamps,
      },
      { transaction },
    );

    await queryInterface.removeColumn(
      TICKET_REVIEWS_TABLE,
      'avatar',
      { transaction },
    );

    await queryInterface.removeConstraint(
      TICKETS_TABLE,
      'tickets_organization_device_fkey',
      { transaction },
    );

    await queryInterface.renameColumn(
      TICKETS_TABLE,
      'organization_device',
      'organization_device_id',
      { transaction },
    );

    await queryInterface.addConstraint(
      TICKETS_TABLE,
      {
        type: 'FOREIGN KEY',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
        fields: [ORGANIZATION_DEVICE_ID_COLUMN],
        name: 'tickets_organization_device_id_fkey',
        references: {
          table: ORGANIZATION_DEVICES_TABLE,
          field: 'id',
        },
        transaction,
      },
    );
  }),
  down: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.dropTable(TICKET_REVIEW_DOCUMENTS_TABLE, { transaction });
    await queryInterface.addColumn(
      TICKET_REVIEWS_TABLE,
      'avatar',
      {
        type: DataTypes.STRING,
      },
      { transaction },
    );
  }),
};
