const TABLES = ['support_requests', 'tickets'];

const COLUMN = 'resolved_at';

module.exports = {
  up: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all(TABLES.map(
    (TABLE) => queryInterface.addColumn(TABLE, COLUMN, { type: 'TIMESTAMP' }, { transaction }),
  ))),


  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all(TABLES.map(
    (TABLE) => queryInterface.removeColumn(TABLE, COLUMN, { transaction }),
  ))),
};
