import dbTemplates from '../../utils/dbTemplates';

const TICKETS_TABLE = 'tickets';
const ORGANIZATION_DEVICES_TABLE = 'organization_devices';

const ORGANIZATION_DEVICE_COLUMN = 'organization_device';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.addColumn(
    TICKETS_TABLE,
    ORGANIZATION_DEVICE_COLUMN,
    dbTemplates(DataTypes).foreign({ allowNull: false, table: ORGANIZATION_DEVICES_TABLE }),
  ),
  down: (queryInterface) => queryInterface.removeColumn(TICKETS_TABLE, ORGANIZATION_DEVICE_COLUMN),
};
