import dbTemplates from '../../utils/dbTemplates';

const ADMIN_USERS_TABLE = 'admin_users';
const ADMIN_USER_GROUPS = 'admin_user_groups';
const CALL_CATEGORIES_TABLE = 'call_categories';

export default {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const templates = dbTemplates(DataTypes);
    const metaTableParameters = {
      id: templates.id,
      name: { type: DataTypes.STRING, allowNull: false, unqiue: true },
      alias: { type: DataTypes.STRING, allowNull: false, unqiue: true },
      ...templates.timestamps,
    };

    await Promise.all([
      queryInterface.createTable(ADMIN_USER_GROUPS, metaTableParameters, { transaction }),
      queryInterface.createTable(CALL_CATEGORIES_TABLE, metaTableParameters, { transaction }),
    ]);

    return queryInterface.createTable(ADMIN_USERS_TABLE, {
      id: templates.id,
      first_name: { type: DataTypes.STRING, allowNull: false },
      patronymic_name: { type: DataTypes.STRING },
      last_name: { type: DataTypes.STRING, allowNull: false },
      email: { type: DataTypes.STRING, allowNull: false, unique: true },
      phone: { type: DataTypes.STRING, allowNull: false, unique: true },
      password: { type: DataTypes.STRING, allowNull: false },
      admin_user_group_id: templates.foreign({ table: ADMIN_USER_GROUPS, allowNull: false }),
      ...templates.timestamps,
    }, { transaction });
  }),
  down: (queryInterface, dataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.dropTable(ADMIN_USERS_TABLE, { transaction });
    return Promise.all([
      queryInterface.dropTable(ADMIN_USER_GROUPS, { transaction }),
      queryInterface.dropTable(CALL_CATEGORIES_TABLE, { transaction }),
    ]);
  }),
};
