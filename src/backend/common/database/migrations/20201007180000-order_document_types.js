import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  ORDER_DOCUMENT_TYPES: 'order_document_types',
  ORDER_DOCUMENTS: 'order_documents',
};

const COLUMN_FOR_TYPE = 'order_document_type_id';

const EXTERNAL_ID = 'external_id';


module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.createTable(TABLES.ORDER_DOCUMENT_TYPES, {
      id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
      name: { type: DataTypes.STRING, allowNull: false, unique: true },
      alias: { type: DataTypes.STRING, unique: true },
    }, { transaction });

    return Promise.all([
      queryInterface.addColumn(
        TABLES.ORDER_DOCUMENTS,
        COLUMN_FOR_TYPE,
        dbTemplates(DataTypes).foreign({ allowNull: false, table: TABLES.ORDER_DOCUMENT_TYPES }),
        { transaction },
      ),
      queryInterface.addColumn(
        TABLES.ORDER_DOCUMENTS, EXTERNAL_ID, { type: DataTypes.STRING }, { transaction },
      ),
    ]);
  }),
  down: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    await Promise.all([
      queryInterface.removeColumn(TABLES.ORDER_DOCUMENTS, COLUMN_FOR_TYPE, { transaction }),
      queryInterface.removeColumn(TABLES.ORDER_DOCUMENTS, EXTERNAL_ID, { transaction }),
    ]);
    return queryInterface.dropTable(TABLES.ORDER_DOCUMENT_TYPES, { transaction });
  }),
};
