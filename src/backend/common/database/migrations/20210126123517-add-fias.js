const TABLE = 'addresses';
const COLUMN = 'fias_id';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.addColumn(TABLE, COLUMN, { type: DataTypes.UUID }),
  down: (queryInterface) => queryInterface.removeColumn(TABLE, COLUMN),
};
