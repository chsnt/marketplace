import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  TICKETS: 'tickets',
  TICKET_ORGANIZATION_SERVICE_RELATIONS: 'ticket_organization_service_relations',
  TICKET_ORGANIZATION_TARIFF_SERVICE_RELATIONS: 'ticket_organization_tariff_service_relations',
  ORGANIZATION_SERVICES: 'organization_services',
  ORGANIZATION_TARIFF_SERVICES: 'organization_tariff_services',
  TICKET_STATUSES: 'ticket_statuses',
};

const COLUMNS = {
  ORGANIZATION_SERVICE_ID: 'organization_service_id',
  ORGANIZATION_TARIFF_SERVICE_ID: 'organization_tariff_service_id',
  ORDER: 'order',
  TICKET_ID: 'ticket_id',
};

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.removeColumn(TABLES.TICKETS, COLUMNS.ORGANIZATION_SERVICE_ID, { transaction }),
    queryInterface.removeColumn(TABLES.TICKETS, COLUMNS.ORGANIZATION_TARIFF_SERVICE_ID, { transaction }),
    queryInterface.createTable(TABLES.TICKET_ORGANIZATION_SERVICE_RELATIONS, {
      id: dbTemplates(DataTypes).id,
      [COLUMNS.TICKET_ID]: dbTemplates(DataTypes).foreign({
        table: TABLES.TICKETS,
        onDelete: 'CASCADE',
        allowNull: false,
      }),
      [COLUMNS.ORGANIZATION_SERVICE_ID]: dbTemplates(DataTypes).foreign({
        table: TABLES.ORGANIZATION_SERVICES,
        onDelete: 'CASCADE',
        allowNull: false,
      }),
      ...dbTemplates(DataTypes).timestamps,
      ...dbTemplates(DataTypes).by,
    }, { transaction }),
    queryInterface.createTable(TABLES.TICKET_ORGANIZATION_TARIFF_SERVICE_RELATIONS, {
      id: dbTemplates(DataTypes).id,
      [COLUMNS.TICKET_ID]: dbTemplates(DataTypes).foreign({
        table: TABLES.TICKETS,
        onDelete: 'CASCADE',
        allowNull: false,
      }),
      [COLUMNS.ORGANIZATION_TARIFF_SERVICE_ID]: dbTemplates(DataTypes).foreign({
        table: TABLES.ORGANIZATION_TARIFF_SERVICES,
        onDelete: 'CASCADE',
        allowNull: false,
      }),
      ...dbTemplates(DataTypes).timestamps,
      ...dbTemplates(DataTypes).by,
    }, { transaction }),
    /* Добавляем поле order для статусов заявки */
    queryInterface.addColumn(TABLES.TICKET_STATUSES, COLUMNS.ORDER, { type: DataTypes.INTEGER }, { transaction }),
  ])),

  down: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => Promise.all([
    queryInterface.addColumn(
      TABLES.TICKETS,
      COLUMNS.ORGANIZATION_SERVICE_ID,
      dbTemplates(DataTypes).foreign({ table: TABLES.ORGANIZATION_SERVICES, onDelete: 'CASCADE' }),
      { transaction },
    ),
    queryInterface.addColumn(
      TABLES.TICKETS,
      COLUMNS.ORGANIZATION_TARIFF_SERVICE_ID,
      dbTemplates(DataTypes).foreign({ table: TABLES.ORGANIZATION_TARIFF_SERVICES, onDelete: 'CASCADE' }),
      { transaction },
    ),
    queryInterface.dropTable(TABLES.TICKET_ORGANIZATION_SERVICE_RELATIONS, { transaction }),
    queryInterface.dropTable(TABLES.TICKET_ORGANIZATION_TARIFF_SERVICE_RELATIONS, { transaction }),
    queryInterface.removeColumn(TABLES.TICKET_STATUSES, COLUMNS.ORDER, { transaction }),
  ])),
};
