const TABLE = 'discount_templates';
const COLUMNS = {
  EXPIRATION_DAY: 'expiration_day',
  TIME_TO_LIVE: 'time_to_live',
  LIMIT: 'limit',
};

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.sequelize.query(
      `ALTER TABLE ${TABLE} ALTER COLUMN "${COLUMNS.EXPIRATION_DAY}" DROP NOT NULL;`,
      { transaction },
    );
    await queryInterface.sequelize.query(
      `ALTER TABLE ${TABLE} ALTER COLUMN "${COLUMNS.LIMIT}" DROP NOT NULL;`,
      { transaction },
    );

    await Promise.all([COLUMNS.EXPIRATION_DAY, COLUMNS.LIMIT].map((column) => queryInterface.bulkUpdate(
      TABLE,
      { [column]: null },
      { [column]: Infinity },
      { transaction },
    )));

    await queryInterface.renameColumn(TABLE, COLUMNS.EXPIRATION_DAY, COLUMNS.TIME_TO_LIVE, { transaction });
    await queryInterface.changeColumn(TABLE, COLUMNS.TIME_TO_LIVE, { type: DataTypes.SMALLINT },
      { transaction });
    await queryInterface.changeColumn(TABLE, COLUMNS.LIMIT, { type: DataTypes.SMALLINT },
      { transaction });
  }),
  down: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.changeColumn(TABLE, COLUMNS.TIME_TO_LIVE, { type: DataTypes.REAL },
      { transaction });
    await queryInterface.changeColumn(TABLE, COLUMNS.LIMIT, { type: DataTypes.REAL },
      { transaction });

    await Promise.all([COLUMNS.TIME_TO_LIVE, COLUMNS.LIMIT].map((column) => queryInterface.bulkUpdate(
      TABLE,
      { [column]: Infinity },
      { [column]: null },
      { transaction },
    )));

    await queryInterface.renameColumn(TABLE, COLUMNS.TIME_TO_LIVE, COLUMNS.EXPIRATION_DAY, { transaction });

    await queryInterface.sequelize.query(
      `ALTER TABLE ${TABLE} ALTER COLUMN "${COLUMNS.EXPIRATION_DAY}" SET NOT NULL;`,
      { transaction },
    );

    await queryInterface.sequelize.query(
      `ALTER TABLE ${TABLE} ALTER COLUMN "${COLUMNS.LIMIT}" SET NOT NULL;`,
      { transaction },
    );
  }),
};
