import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  ADMIN_USERS: 'admin_users',
  ADMIN_USER_GROUPS: 'admin_user_groups',
  USERS: 'users',
  USER_GROUPS: 'user_groups',
};

const COLUMNS = {
  CREATED_BY: 'created_by',
  UPDATED_BY: 'updated_by',
  USER_GROUP_ID: 'user_group_id',
};

const IGNORE_TABLES = [
  'sequelize_meta',
  'sequelize_data',
  'system_logs',
  'request_logs',
];

const FK_USER_COLUMNS = [COLUMNS.CREATED_BY, COLUMNS.UPDATED_BY];

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const templates = dbTemplates(DataTypes);

    await queryInterface.dropTable(TABLES.ADMIN_USERS, { transaction });

    await queryInterface.renameTable(TABLES.ADMIN_USER_GROUPS, TABLES.USER_GROUPS, { transaction });

    await queryInterface.addColumn(
      TABLES.USERS,
      COLUMNS.USER_GROUP_ID,
      templates.foreign({ table: TABLES.USER_GROUPS }),
      { transaction },
    );

    const tables = await queryInterface.showAllTables({ transaction });

    return Promise.all(
      tables
        .filter((tableName) => !IGNORE_TABLES.includes(tableName))
        .map((tableName) => FK_USER_COLUMNS.map((columnName) => queryInterface.addColumn(
          tableName,
          columnName,
          templates.foreign({ table: TABLES.USERS }),
          { transaction },
        ))).flat(),
    );
  }),

  down: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const templates = dbTemplates(DataTypes);
    const tables = await queryInterface.showAllTables({ transaction });
    await Promise.all(
      tables
        .filter((tableName) => !IGNORE_TABLES.includes(tableName))
        .map((tableName) => FK_USER_COLUMNS.map((columnName) => queryInterface.removeColumn(
          tableName,
          columnName,
          { transaction },
        ))).flat(),
    );
    await queryInterface.removeColumn(
      TABLES.USERS,
      COLUMNS.USER_GROUP_ID,
      { transaction },
    );
    await queryInterface.renameTable(TABLES.USER_GROUPS, TABLES.ADMIN_USER_GROUPS, { transaction });
    return queryInterface.createTable(TABLES.ADMIN_USERS, {
      id: templates.id,
      first_name: { type: DataTypes.STRING, allowNull: false },
      patronymic_name: { type: DataTypes.STRING },
      last_name: { type: DataTypes.STRING, allowNull: false },
      email: { type: DataTypes.STRING, allowNull: false, unique: true },
      phone: { type: DataTypes.STRING, allowNull: false, unique: true },
      password: { type: DataTypes.STRING, allowNull: false },
      admin_user_group_id: templates.foreign({ table: TABLES.ADMIN_USER_GROUPS, allowNull: false }),
      ...templates.timestamps,
    }, { transaction });
  }),
};
