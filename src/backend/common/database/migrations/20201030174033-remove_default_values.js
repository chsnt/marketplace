/* Миграция "Вне очереди" чтоб выпилить дефолтные поля со значениями id */
const TABLE = 'addresses';
const COLUMN = 'federation_subject_id';

module.exports = {
  up: (queryInterface) => queryInterface.sequelize.query(
    `ALTER TABLE ${TABLE} ALTER COLUMN ${COLUMN} SET DEFAULT NULL;`,
  ),

  down: (queryInterface) => queryInterface.sequelize.query(
    `ALTER TABLE ${TABLE} ALTER COLUMN ${COLUMN} SET DEFAULT '4250f0fc-e565-43f4-847a-519eda055817';`,
  ),
};
