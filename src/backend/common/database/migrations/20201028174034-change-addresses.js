import templates from '../../utils/dbTemplates';

const TABLES = {
  ADDRESSES: 'addresses',
  CITIES: 'cities',
  FEDERATION_SUBJECTS: 'federation_subjects',
};

const COLUMNS = {
  FEDERATION_SUBJECT_ID: 'federation_subject_id',
  CITY_ID: 'city_id',
};

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface
      .addColumn(
        TABLES.ADDRESSES,
        COLUMNS.CITY_ID,
        templates(DataTypes).foreign({ table: TABLES.CITIES }),
        { transaction },
      ),

    queryInterface
      .addColumn(
        TABLES.ADDRESSES,
        COLUMNS.FEDERATION_SUBJECT_ID,
        templates(DataTypes).foreign({
          table: TABLES.FEDERATION_SUBJECTS,
          allowNull: false,
          defaultValue: '4250f0fc-e565-43f4-847a-519eda055817', // Регион "Москва город". Т.к. нет прода и на тесте мало данных.
        }),
        { transaction },
      ),
  ])),

  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.removeColumn(TABLES.ADDRESSES, COLUMNS.FEDERATION_SUBJECT_ID, { transaction }),
    queryInterface.removeColumn(
      TABLES.ADDRESSES,
      COLUMNS.CITY_ID,
      { transaction },
    ),
  ])),
};
