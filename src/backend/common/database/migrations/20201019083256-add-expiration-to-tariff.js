const TARIFFS_TABLE = 'tariffs';
const SERVICES_TABLE = 'services';
const ORGANIZATION_SERVICES_TABLE = 'organization_services';


const EXPIRATION_COLUMN = 'expiration_months';
const EXPIRATION_AT_COLUMN = 'expiration_at';
const ALIAS_COLUMN = 'alias';

const ALIASES = [TARIFFS_TABLE, SERVICES_TABLE];

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await Promise.all([
      queryInterface.addColumn(
        TARIFFS_TABLE,
        EXPIRATION_COLUMN,
        { type: DataTypes.SMALLINT, allowNull: false, defaultValue: 0 },
        { transaction },
      ),


      Promise.all(ALIASES.map((table) => queryInterface.addColumn(
        table,
        ALIAS_COLUMN,
        {
          type: DataTypes.STRING, allowNull: false, unique: true,
        },
        { transaction },
      ))),

      queryInterface.removeColumn(ORGANIZATION_SERVICES_TABLE, EXPIRATION_AT_COLUMN, { transaction }),
    ]);
  }),
  down: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    await Promise.all([
      queryInterface.removeColumn(TARIFFS_TABLE, EXPIRATION_COLUMN, { transaction }),
      Promise.all(ALIASES.map((table) => queryInterface.removeColumn(table, ALIAS_COLUMN, { transaction }))),
    ]);
  }),
};
