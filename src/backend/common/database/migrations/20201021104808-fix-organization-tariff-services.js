const ORGANIZATION_TARIFFS_TABLE = 'organization_tariffs';
const ORGANIZATION_TARIFF_SERVICES_TABLE = 'organization_tariff_services';
const TARIFF_SERVICE_RELATIONS_TABLE = 'tariff_service_relations';

const ORGANIZATION_TARIFF_ID_COLUMN = 'organization_tariff_id';
const TARIFF_SERVICE_RELATION_ID_COLUMN = 'tariff_service_relation_id';

module.exports = {
  up: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    await Promise.all([
      queryInterface.removeConstraint(
        ORGANIZATION_TARIFF_SERVICES_TABLE,
        'organization_tariff_balances_organization_tariff_id_fkey',
        { transaction },
      ),
      queryInterface.removeConstraint(
        ORGANIZATION_TARIFF_SERVICES_TABLE,
        'organization_tariff_balances_tariff_service_relation_id_fkey',
        { transaction },
      ),
    ]);

    await Promise.all([
      queryInterface.addConstraint(
        ORGANIZATION_TARIFF_SERVICES_TABLE,
        {
          type: 'FOREIGN KEY',
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
          fields: [ORGANIZATION_TARIFF_ID_COLUMN],
          name: 'organization_tariff_services_organization_tariff_id_fkey',
          references: {
            table: ORGANIZATION_TARIFFS_TABLE,
            field: 'id',
          },
          transaction,
        },
      ),

      queryInterface.addConstraint(
        ORGANIZATION_TARIFF_SERVICES_TABLE,
        {
          type: 'FOREIGN KEY',
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
          fields: [TARIFF_SERVICE_RELATION_ID_COLUMN],
          name: 'organization_tariff_services_tariff_service_relation_id_fkey',
          references: {
            table: TARIFF_SERVICE_RELATIONS_TABLE,
            field: 'id',
          },
          transaction,
        },
      ),
    ]);
  }),

  down: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    await Promise.all([
      queryInterface.removeConstraint(
        ORGANIZATION_TARIFF_SERVICES_TABLE,
        'organization_tariff_services_organization_tariff_id_fkey',
        { transaction },
      ),
      queryInterface.removeConstraint(
        ORGANIZATION_TARIFF_SERVICES_TABLE,
        'organization_tariff_services_tariff_service_relation_id_fkey',
        { transaction },
      ),
    ]);

    await Promise.all([
      queryInterface.addConstraint(
        ORGANIZATION_TARIFF_SERVICES_TABLE,
        {
          type: 'FOREIGN KEY',
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
          fields: [ORGANIZATION_TARIFF_ID_COLUMN],
          name: 'organization_tariff_balances_organization_tariff_id_fkey',
          references: {
            table: ORGANIZATION_TARIFFS_TABLE,
            field: 'id',
          },
          transaction,
        },
      ),

      queryInterface.addConstraint(
        ORGANIZATION_TARIFF_SERVICES_TABLE,
        {
          type: 'FOREIGN KEY',
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
          fields: [TARIFF_SERVICE_RELATION_ID_COLUMN],
          name: 'organization_tariff_balances_tariff_service_relation_id_fkey',
          references: {
            table: TARIFF_SERVICE_RELATIONS_TABLE,
            field: 'id',
          },
          transaction,
        },
      ),
    ]);
  }),
};
