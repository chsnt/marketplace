import dbTemplates from '../../utils/dbTemplates';

const newTables = ['vats', 'measurement_units', 'kkt_types', 'pc_types', 'operating_systems'];

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const deviceTypeColumns = [{
      name: 'is_sealed_by_us',
      attribute: { type: DataTypes.BOOLEAN },
    }, {
      name: 'is_used_by_client',
      attribute: { type: DataTypes.BOOLEAN },
    }, {
      name: 'params',
      attribute: { type: DataTypes.JSONB },
    }, {
      name: 'available_delivery_types',
      attribute: { type: DataTypes.ARRAY(DataTypes.UUID) },
    }];

    const templates = dbTemplates(DataTypes);
    const createTemplates = {
      ...templates.timestamps,
      ...templates.by,
    };

    await Promise.all([
      ...newTables.map((table) => queryInterface.createTable(table, {
        id: templates.id,
        name: { type: DataTypes.STRING, allowNull: false, unique: true },
        alias: { type: DataTypes.STRING, unique: true },
        external_id: { type: DataTypes.STRING, unique: true },
        ...createTemplates,
      },
      { transaction })),

      queryInterface.createTable('deliveries', {
        id: templates.id,
        name: { type: DataTypes.STRING, allowNull: false, unique: true },
        alias: { type: DataTypes.STRING, unique: true },
        cost: { type: DataTypes.FLOAT },
        data: { type: DataTypes.JSONB },
        external_id: { type: DataTypes.STRING, unique: true },
        ...createTemplates,
      },
      { transaction }),

      queryInterface.createTable('good_catalogs', {
        id: templates.id,
        name: { type: DataTypes.STRING, allowNull: false, unique: true },
        full_name: { type: DataTypes.STRING, unique: true },
        description: { type: DataTypes.STRING, allowNull: false, unique: true },
        number_registry_fns: { type: DataTypes.STRING, unique: true },
        limit: { type: DataTypes.INTEGER, allowNull: false },
        device_type_id: templates.foreign({
          table: 'device_types',
          allowNull: false,
        }),
        available_catalog_status_id: templates.foreign({
          table: 'available_catalog_statuses',
          allowNull: false,
        }),
        external_id: { type: DataTypes.STRING, unique: true },
        data: { type: DataTypes.JSONB },
        ...createTemplates,
      },
      { transaction }),
    ]);

    await queryInterface.createTable('client_products', {
      id: templates.id,
      good_catalog_id: templates.foreign({
        table: 'good_catalogs',
        allowNull: false,
      }),
      order_id: templates.foreign({
        table: 'orders',
        onDelete: 'CASCADE',
        allowNull: false,
      }),
      count: { type: DataTypes.INTEGER, allowNull: false },
      ...createTemplates,
    },
    { transaction });

    await queryInterface.addColumn('orders',
      'delivery_id',
      templates.foreign({ table: 'client_products' }),
      { transaction });

    await queryInterface.addColumn('costs',
      'good_catalog_id',
      templates.foreign({ table: 'good_catalogs', onDelete: 'CASCADE' }),
      { transaction });

    await Promise.all(deviceTypeColumns.map((column) => queryInterface.addColumn('device_types',
      column.name,
      column.attribute,
      { transaction })));
  }),
  down: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    const deviceTypeColumns = ['is_sealed_by_us', 'is_used_by_client', 'params', 'available_delivery_types'];
    await queryInterface.removeColumn('orders', 'delivery_id', { transaction });
    await queryInterface.removeColumn('costs', 'good_catalog_id', { transaction });

    await Promise.all([
      ...deviceTypeColumns.map((column) => queryInterface.removeColumn('device_types', column, { transaction })),
      ...newTables.map((table) => queryInterface.dropTable(table, { transaction })),
      queryInterface.dropTable('deliveries', { transaction }),
      queryInterface.dropTable('client_products', { transaction }),
    ]);

    await queryInterface.dropTable('good_catalogs', { transaction });
  }),
};
