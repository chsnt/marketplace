import templates from '../../utils/dbTemplates';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.createTable('addresses', {
      id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
      region: { type: DataTypes.STRING },
      area: { type: DataTypes.STRING },
      city: { type: DataTypes.STRING },
      street: { type: DataTypes.STRING, allowNull: false },
      house: { type: DataTypes.STRING, allowNull: false },
      apartment: { type: DataTypes.STRING },
      postal_code: { type: DataTypes.STRING },
      ...templates(DataTypes).timestamps,
    }, { transaction });
    await queryInterface.createTable('organizations', {
      id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
      name: { type: DataTypes.STRING, allowNull: false },
      inn: { type: DataTypes.STRING },
      kpp: { type: DataTypes.STRING },
      ogrn: { type: DataTypes.STRING },
      ogrnip: { type: DataTypes.STRING },
      real_address_id: templates(DataTypes).foreign({ table: 'addresses' }),
      legal_address_id: templates(DataTypes).foreign({ table: 'addresses' }),
      ...templates(DataTypes).timestamps,
    }, { transaction });
    await queryInterface.createTable('users', {
      id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
      first_name: { type: DataTypes.STRING, allowNull: false },
      last_name: { type: DataTypes.STRING, allowNull: false },
      patronymic_name: { type: DataTypes.STRING },
      email: { type: DataTypes.STRING, unique: true },
      phone: { type: DataTypes.STRING, allowNull: false, unique: true },
      password: { type: DataTypes.STRING, allowNull: false },
      organization_id: templates(DataTypes).foreign({ table: 'organizations' }),
      ...templates(DataTypes).timestamps,
    }, { transaction });
    await queryInterface.createTable('request_logs', {
      id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
      url: DataTypes.STRING,
      headers: DataTypes.TEXT,
      method: DataTypes.ENUM('GET', 'POST', 'DELETE', 'PUT', 'PATCH'),
      body: DataTypes.TEXT,
      params: DataTypes.TEXT,
      query: DataTypes.TEXT,
      status_code: DataTypes.INTEGER,
      response_body: DataTypes.TEXT,
      time: DataTypes.FLOAT,
      user_id: templates(DataTypes).foreign({ table: 'users' }),
      created_at: templates(DataTypes).timestamps.created_at,
    }, { transaction });
    await queryInterface.createTable('system_logs', {
      id: { type: DataTypes.UUID, primaryKey: true, defaultValue: DataTypes.UUIDV4 },
      event: { type: DataTypes.STRING(128), allowNull: false },
      error_message: { type: DataTypes.STRING(512) },
      error_stack: { type: DataTypes.STRING(1024) },
      data: { type: DataTypes.TEXT, allowNull: false },
      user_id: templates(DataTypes).foreign({ table: 'users' }),
      created_at: templates(DataTypes).timestamps.created_at,
    }, { transaction });
  }),
  down: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.dropTable('request_logs', { transaction });
    await queryInterface.dropTable('system_logs', { transaction });
    await queryInterface.dropTable('users', { transaction });
    await queryInterface.dropTable('organizations', { transaction });
    await queryInterface.dropTable('addresses', { transaction });
  }),
};
