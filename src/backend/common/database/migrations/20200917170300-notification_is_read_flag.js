import templates from '../../utils/dbTemplates';

const NOTIFICATIONS_TABLE = 'notifications';
const IS_READ_COLUMN = 'is_read';
const PAYLOAD_COLUMN = 'payload';

const ORDER_ELEMENT_TYPES_TABLE = 'order_element_types';
const TABLE_COLUMN = 'table';
const MODEL_COLUMN = 'model';

const ORDERS_TABLE = 'orders';
const ORDER_STATUS_COLUMN = 'order_status_id';
const FKEY = 'orders_order_status_id_fkey';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    /* Исправляем косяк миграции создания сущностей и пересоздаем fk */
    await queryInterface.removeConstraint(ORDERS_TABLE, FKEY, { transaction });
    return Promise.all([
      queryInterface
        .addColumn(
          NOTIFICATIONS_TABLE,
          IS_READ_COLUMN,
          { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
          { transaction },
        ),
      /* Удобнее хранить имя модели, чем имя таблицы. Переименуем колонку */
      queryInterface.renameColumn(ORDER_ELEMENT_TYPES_TABLE, TABLE_COLUMN, MODEL_COLUMN, { transaction }),
      queryInterface.changeColumn(
        ORDERS_TABLE,
        ORDER_STATUS_COLUMN,
        templates(DataTypes).foreign({ table: 'order_statuses', onDelete: 'CASCADE', allowNull: false }),
        { transaction },
      ),
      queryInterface.sequelize.query(
        `ALTER TABLE ${NOTIFICATIONS_TABLE} ALTER COLUMN ${PAYLOAD_COLUMN} TYPE JSONB USING ${PAYLOAD_COLUMN}::JSONB;`,
        { transaction },
      ),
    ]);
  }),

  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.removeColumn(NOTIFICATIONS_TABLE, IS_READ_COLUMN),
    queryInterface.renameColumn(ORDER_ELEMENT_TYPES_TABLE, MODEL_COLUMN, TABLE_COLUMN, { transaction }),
  ])),
};
