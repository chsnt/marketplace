import templates from '../../utils/dbTemplates';

const TABLES = {
  TICKETS: 'tickets',
  USERS: 'users',
};

const COLUMNS = {
  RESPONSIBLE_ID: 'responsible_id',
  ID_SD: 'id_sd',
  SERVICE_DESK_ID: 'service_desk_id',
};


module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.addColumn(
      TABLES.TICKETS,
      COLUMNS.RESPONSIBLE_ID,
      templates(DataTypes).foreign({ table: TABLES.USERS }),
      { transaction },
    ),

    queryInterface.renameColumn(
      TABLES.TICKETS,
      COLUMNS.ID_SD,
      COLUMNS.SERVICE_DESK_ID,
      { transaction },
    ),
  ])),

  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.renameColumn(
      TABLES.TICKETS,
      COLUMNS.SERVICE_DESK_ID,
      COLUMNS.ID_SD,
      { transaction },
    ),

    queryInterface.removeColumn(TABLES.TICKETS, COLUMNS.RESPONSIBLE_ID, { transaction }),
  ])),
};
