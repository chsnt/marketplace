const TABLES = {
  LEAD_STATUSES: 'lead_statuses',
};

const COLUMNS = {
  PRIORITY: 'priority',
};

const data = [
  { id: '726af6b4-8c4d-49e2-ab20-c7b7f65dd06b', priority: 0 },
  { id: 'e26776e5-a6f3-4e1c-913b-9f7b9cf0413a', priority: 1 },
  { id: '6f04750d-47de-4b7e-aab2-de8a0e245688', priority: 2 },
  { id: '7ab6e605-9176-4cc3-841f-9618cd4856b1', priority: 3 },
  { id: '32505a52-b031-48ae-a45f-e50df187dd1e', priority: 4, name: 'Потребность не удовлетворена' },
];

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.addColumn(
      TABLES.LEAD_STATUSES,
      COLUMNS.PRIORITY,
      { type: DataTypes.SMALLINT },
      { transaction },
    );

    await Promise.all(data.map((status) => {
      const { id, ...value } = status;
      return queryInterface.bulkUpdate(
        TABLES.LEAD_STATUSES,
        value,
        { id },
        { transaction },
      );
    }));
  }),

  down: (queryInterface) => queryInterface.removeColumn(
    TABLES.LEAD_STATUSES,
    COLUMNS.PRIORITY,
  ),
};
