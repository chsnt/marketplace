import templates from '../../utils/dbTemplates';

const TABLES = {
  CLIENT_TARIFFS: 'client_tariffs',
  CLIENT_SERVICES: 'client_services',
  ADDRESSES: 'addresses',
  CLIENT_DEVICES: 'client_devices',
};

const COLUMNS = {
  CLIENT_DEVICE_ID: 'client_device_id',
  SERVICE_ADDRESS_ID: 'service_address_id',
};

const clientTariffOrServiceUpdate = async ({
  table, clientData, queryInterface, transaction,
}) => Promise.all(
  clientData.map((data) => queryInterface.bulkUpdate(
    table,
    data,
    { id: data.id },
    { transaction },
  )),
);

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const clientTariffs = await queryInterface.sequelize.query(`
        SELECT "ClientTariffModel"."id",
               "clientDevice->serviceAddress"."id" AS "service_address_id"
        FROM "client_tariffs" AS "ClientTariffModel"
                 LEFT OUTER JOIN "client_devices" AS "clientDevice"
                                 ON "ClientTariffModel"."client_device_id" = "clientDevice"."id"
                 LEFT OUTER JOIN "addresses" AS "clientDevice->serviceAddress"
                                 ON "clientDevice"."service_address_id" = "clientDevice->serviceAddress"."id"
    `, { type: DataTypes.QueryTypes.SELECT, transaction });

    const clientServices = await queryInterface.sequelize.query(`
        SELECT "ClientServiceModel"."id",
               "clientDevice->serviceAddress"."id" AS "service_address_id"
        FROM "client_services" AS "ClientServiceModel"
                 LEFT OUTER JOIN "client_devices" AS "clientDevice"
                                 ON "ClientServiceModel"."client_device_id" = "clientDevice"."id" 
                 LEFT OUTER JOIN "addresses" AS "clientDevice->serviceAddress"
                                 ON "clientDevice"."service_address_id" = "clientDevice->serviceAddress"."id"
                            `, { type: DataTypes.QueryTypes.SELECT, transaction });

    await Promise.all([
      queryInterface.removeColumn(TABLES.CLIENT_TARIFFS, COLUMNS.CLIENT_DEVICE_ID, { transaction }),
      queryInterface.removeColumn(TABLES.CLIENT_SERVICES, COLUMNS.CLIENT_DEVICE_ID, { transaction }),

      queryInterface.addColumn(
        TABLES.CLIENT_TARIFFS,
        COLUMNS.SERVICE_ADDRESS_ID,
        templates(DataTypes).foreign({ table: TABLES.ADDRESSES }),
        { transaction },
      ),

      queryInterface.addColumn(
        TABLES.CLIENT_SERVICES,
        COLUMNS.SERVICE_ADDRESS_ID,
        templates(DataTypes).foreign({ table: TABLES.ADDRESSES }),
        { transaction },
      ),
    ]);


    if (clientTariffs.length) {
      await clientTariffOrServiceUpdate({
        table: TABLES.CLIENT_TARIFFS,
        clientData: clientTariffs,
        queryInterface,
        transaction,
      });
    }

    if (clientServices.length) {
      await clientTariffOrServiceUpdate({
        table: TABLES.CLIENT_SERVICES,
        clientData: clientServices,
        queryInterface,
        transaction,
      });
    }
  }),

  down: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const clientTariffs = await queryInterface.sequelize.query(`
        SELECT "ClientTariffModel"."id",
               "serviceAddress->clientDevice"."id" AS "client_device_id"
        FROM "client_tariffs" AS "ClientTariffModel"
                 LEFT OUTER JOIN "addresses" AS "serviceAddress"
                                 ON "ClientTariffModel"."service_address_id" = "serviceAddress"."id"
                 LEFT OUTER JOIN "client_devices" AS "serviceAddress->clientDevice"
                                 ON "serviceAddress"."id" = "serviceAddress->clientDevice"."service_address_id"
    `, { type: DataTypes.QueryTypes.SELECT, transaction });

    const clientServices = await queryInterface.sequelize.query(`
        SELECT "ClientServiceModel"."id",
               "serviceAddress->clientDevice"."id" AS "client_device_id"
        FROM "client_services" AS "ClientServiceModel"
                 LEFT OUTER JOIN "addresses" AS "serviceAddress"
                                 ON "ClientServiceModel"."service_address_id" = "serviceAddress"."id"
                 LEFT OUTER JOIN "client_devices" AS "serviceAddress->clientDevice"
                                 ON "serviceAddress"."id" = "serviceAddress->clientDevice"."service_address_id"
                            `, { type: DataTypes.QueryTypes.SELECT, transaction });

    await Promise.all([
      queryInterface.removeColumn(TABLES.CLIENT_TARIFFS, COLUMNS.SERVICE_ADDRESS_ID, { transaction }),
      queryInterface.removeColumn(TABLES.CLIENT_SERVICES, COLUMNS.SERVICE_ADDRESS_ID, { transaction }),

      queryInterface.addColumn(
        TABLES.CLIENT_TARIFFS,
        COLUMNS.CLIENT_DEVICE_ID,
        templates(DataTypes).foreign({ table: TABLES.CLIENT_DEVICES }),
        { transaction },
      ),

      queryInterface.addColumn(
        TABLES.CLIENT_SERVICES,
        COLUMNS.CLIENT_DEVICE_ID,
        templates(DataTypes).foreign({ table: TABLES.CLIENT_DEVICES }),
        { transaction },
      ),
    ]);

    if (clientTariffs.length) {
      await clientTariffOrServiceUpdate({
        table: TABLES.CLIENT_TARIFFS,
        clientData: clientTariffs,
        queryInterface,
        transaction,
      });
    }

    if (clientServices.length) {
      await clientTariffOrServiceUpdate({
        table: TABLES.CLIENT_SERVICES,
        clientData: clientServices,
        queryInterface,
        transaction,
      });
    }
  }),
};
