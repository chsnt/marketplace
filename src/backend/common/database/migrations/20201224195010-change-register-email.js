const TABLE = 'users';
const COLUMN = 'email';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.sequelize.query('CREATE EXTENSION IF NOT EXISTS citext;', { transaction });
    await queryInterface.changeColumn(TABLE, COLUMN, DataTypes.CITEXT, { transaction });
  }),

  down: (queryInterface, DataTypes) => queryInterface.changeColumn(TABLE, COLUMN, DataTypes.STRING),
};
