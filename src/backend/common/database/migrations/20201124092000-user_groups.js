import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  USER_GROUPS: 'user_groups',
  USERS: 'users',
  RELATIONS: 'user_group_relations',
};

const COLUMNS = {
  USER_GROUP_ID: 'user_group_id',
};

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const templates = dbTemplates(DataTypes);
    return Promise.all([
      queryInterface.removeColumn(TABLES.USERS, COLUMNS.USER_GROUP_ID, { transaction }),
      queryInterface.createTable(TABLES.RELATIONS, {
        id: templates.id,
        user_id: templates.foreign({ table: TABLES.USERS, allowNull: false }),
        user_group_id: templates.foreign({ table: TABLES.USER_GROUPS, allowNull: false }),
        created_at: templates.timestamps.created_at,
      }, { transaction }),
    ]);
  }),

  down: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const templates = dbTemplates(DataTypes);
    return Promise.all([
      queryInterface.addColumn(
        TABLES.USERS,
        COLUMNS.USER_GROUP_ID,
        templates.foreign({ table: TABLES.USER_GROUPS }),
        { transaction },
      ),
      queryInterface.dropTable(TABLES.RELATIONS, { transaction }),
    ]);
  }),
};
