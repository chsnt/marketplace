import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  EDO_DESCRIPTIONS: 'electronic_document_circulation_descriptions',
  EDO: 'electronic_document_circulations',
  ORGANIZATIONS: 'organizations',
  BANK_DETAILS: 'bank_details',
};

const COLUMNS = {
  BANK_DETAILS_ID: 'bank_detail_id',
  ORGANIZATION_ID: 'organization_id',
  EDO_ID: 'electronic_document_circulation_id',
};

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const templates = dbTemplates(DataTypes);
    /* Получаем записи из bank_details с organization_id */
    const bankDetails = await queryInterface.rawSelect(TABLES.BANK_DETAILS,
      { raw: false, plain: false, transaction }, ['id']);

    /* Формируем карту организация -> ЭДО и bankDetails -> organization */
    const { organizationEdoMap, bankDetailEdoMap } = bankDetails.reduce((acc, {
      id: bankDetailsId,
      organization_id: organizationId,
      electronic_document_circulation_id: electronicDocumentCirculationId,
    }) => {
      acc.organizationEdoMap[organizationId] = electronicDocumentCirculationId;
      acc.bankDetailEdoMap[bankDetailsId] = organizationId;
      return acc;
    }, { organizationEdoMap: {}, bankDetailEdoMap: {} });

    /* Создаем колонку EDO_ID в организации */
    await Promise.all([
      queryInterface.addColumn(
        TABLES.ORGANIZATIONS, COLUMNS.EDO_ID,
        templates.foreign({ table: TABLES.EDO }), { transaction },
      ),
      queryInterface.addColumn(
        TABLES.EDO_DESCRIPTIONS, COLUMNS.ORGANIZATION_ID,
        templates.foreign({ table: TABLES.ORGANIZATIONS }), { transaction },
      ),
    ]);

    /* Получаем все существующие организации */
    const organizations = await queryInterface.rawSelect(TABLES.ORGANIZATIONS,
      { raw: false, plain: false, transaction }, ['id']);

    /* Для каждой организации проставляем ЭДО */
    await Promise.all(organizations.map(({ id }) => queryInterface.bulkUpdate(
      TABLES.ORGANIZATIONS,
      { [COLUMNS.EDO_ID]: organizationEdoMap[id] || null },
      { id },
      { transaction },
    )));

    /* Проставляем ссылку на организацию в ЭДО, которых нет у нас в системе в виде справочных данных */
    await Promise.all(Object.keys(bankDetailEdoMap).map((bankDetailId) => queryInterface.bulkUpdate(
      TABLES.EDO_DESCRIPTIONS,
      { [COLUMNS.ORGANIZATION_ID]: bankDetailEdoMap[bankDetailId] || null },
      { [COLUMNS.BANK_DETAILS_ID]: bankDetailId },
      { transaction },
    )));

    return Promise.all([
      queryInterface.removeColumn(TABLES.BANK_DETAILS, COLUMNS.EDO_ID, { transaction }),
      queryInterface.removeColumn(TABLES.EDO_DESCRIPTIONS, COLUMNS.BANK_DETAILS_ID, { transaction }),
    ]);
  }),

  down: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const templates = dbTemplates(DataTypes);

    /* Создаем удаленные колонки */
    await Promise.all([
      queryInterface.addColumn(
        TABLES.BANK_DETAILS, COLUMNS.EDO_ID,
        templates.foreign({ table: TABLES.EDO }), { transaction },
      ),
      queryInterface.addColumn(
        TABLES.EDO_DESCRIPTIONS, COLUMNS.BANK_DETAILS_ID,
        templates.foreign({ table: TABLES.BANK_DETAILS }), { transaction },
      ),
    ]);

    /* Получаем все существующие организации */
    const organizations = await queryInterface.rawSelect(TABLES.ORGANIZATIONS,
      { raw: false, plain: false, transaction }, ['id']);

    const bankDetails = await queryInterface.rawSelect(TABLES.BANK_DETAILS,
      { raw: false, plain: false, transaction }, ['id']);

    const organizationEdoMap = organizations.reduce((acc, {
      id,
      electronic_document_circulation_id: electronicDocumentCirculationId,
    }) => {
      acc[id] = electronicDocumentCirculationId;
      return acc;
    }, {});

    const organizationBankDetailMap = bankDetails.reduce((acc, {
      id: bankDetailId,
      organization_id: organizationId,
    }) => {
      acc[organizationId] = bankDetailId;
      return acc;
    }, {});

    /* Проставляем ЭДО в банковские реквизиты */
    await Promise.all(bankDetails.map(({ id, organization_id: organizationId }) => queryInterface.bulkUpdate(
      TABLES.BANK_DETAILS,
      { [COLUMNS.EDO_ID]: organizationEdoMap[organizationId] || null },
      { id },
      { transaction },
    )));

    await Promise.all(Object.keys(organizationBankDetailMap).map((organizationId) => queryInterface.bulkUpdate(
      TABLES.EDO_DESCRIPTIONS,
      { [COLUMNS.BANK_DETAILS_ID]: organizationBankDetailMap[organizationId] || null },
      { [COLUMNS.ORGANIZATION_ID]: organizationId },
      { transaction },
    )));

    return Promise.all([
      queryInterface.removeColumn(TABLES.ORGANIZATIONS, COLUMNS.EDO_ID, { transaction }),
      queryInterface.removeColumn(TABLES.EDO_DESCRIPTIONS, COLUMNS.ORGANIZATION_ID, { transaction }),
    ]);
  }),
};
