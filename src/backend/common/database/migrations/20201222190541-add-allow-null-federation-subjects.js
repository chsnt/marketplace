const TABLES = {
  banks: ['name', 'rcbic', 'correspondent_account'],
  addresses: ['federation_subject_id', 'street', 'house'],
};

module.exports = {
  up: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all(
    Object.entries(TABLES).map(
      ([table, columns]) => Promise.all(columns.map((column) => queryInterface.sequelize.query(
        `ALTER TABLE ${table} ALTER COLUMN ${column} DROP NOT NULL;`, { transaction },
      ))),
    ),
  )),

  down: (queryInterface) => queryInterface.sequelize.transaction((transaction) => Promise.all(
    Object.entries(TABLES).map(
      ([table, columns]) => Promise.all(columns.map((column) => queryInterface.sequelize.query(
        `ALTER TABLE ${table} ALTER COLUMN ${column} SET NOT NULL;`, { transaction },
      ))),
    ),
  )),
};
