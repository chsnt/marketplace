import templates from '../../utils/dbTemplates';

const SUPPORT_REQUESTS_TABLE = 'support_requests';
const TICKETS_TABLE = 'tickets';
const ORGANIZATION_TARIFF_BALANCES_TABLE = 'organization_tariff_balances';
const ORGANIZATION_TARIFF_SERVICES_TABLE = 'organization_tariff_services';
const ORGANIZATION_SERVICES_TABLE = 'organization_services';
const SUPPORT_REQUEST_TOPICS_TABLE = 'support_request_topics';
const DEVICE_TYPES_TABLE = 'device_types';
const SUPPORT_REQUEST_TABLE = 'support_request';

const ORGANIZATION_SERVICE_ID_COLUMN = 'organization_service_id';
const ORGANIZATION_TARIFF_SERVICE_ID_COLUMN = 'organization_tariff_service_id';
const DEVICE_TYPE_ID_COLUMN = 'device_type_id';
const SUPPORT_REQUEST_TOPIC_ID_COLUMN = 'support_request_topic_id';
const SUPPORT_REQUEST_ID_COLUMN = 'support_request_id';

const TICKETS_SUPPORT_REQUEST_ID_FKEY = 'tickets_support_request_id_fkey';
const SUPPORT_REQUESTS_SUPPORT_REQUEST_TOPIC_ID_FKEY = 'support_requests_support_request_topic_id_fkey';

const FOREIGN_KEY = 'FOREIGN KEY';
const CASCADE = 'CASCADE';

const ID = 'id';

const commonAttr = {
  type: FOREIGN_KEY,
  onDelete: CASCADE,
  onUpdate: CASCADE,
};

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const attributes = {
      ...commonAttr,
      fields: [SUPPORT_REQUEST_TOPIC_ID_COLUMN],
      name: SUPPORT_REQUESTS_SUPPORT_REQUEST_TOPIC_ID_FKEY,
      references: {
        table: SUPPORT_REQUEST_TOPICS_TABLE,
        field: ID,
      },
      transaction,
    };

    await Promise.all([
      queryInterface.removeConstraint(
        TICKETS_TABLE,
        TICKETS_SUPPORT_REQUEST_ID_FKEY,
        { transaction },
      ),
      queryInterface.removeConstraint(
        SUPPORT_REQUESTS_TABLE,
        SUPPORT_REQUESTS_SUPPORT_REQUEST_TOPIC_ID_FKEY,
        { transaction },
      ),
      queryInterface.renameTable(
        ORGANIZATION_TARIFF_BALANCES_TABLE,
        ORGANIZATION_TARIFF_SERVICES_TABLE,
        { transaction },
      ),
      queryInterface.removeColumn(TICKETS_TABLE, DEVICE_TYPE_ID_COLUMN, { transaction }),
    ]);

    await Promise.all([
      queryInterface.addConstraint(SUPPORT_REQUESTS_TABLE, attributes),
      queryInterface.addColumn(
        TICKETS_TABLE,
        ORGANIZATION_SERVICE_ID_COLUMN,
        templates(DataTypes).foreign({ table: ORGANIZATION_SERVICES_TABLE }),
        { transaction },
      ),
      queryInterface.addColumn(
        TICKETS_TABLE,
        ORGANIZATION_TARIFF_SERVICE_ID_COLUMN,
        templates(DataTypes).foreign({ table: ORGANIZATION_TARIFF_SERVICES_TABLE }), { transaction },
      ),
    ]);
  }),
  down: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    /* ключ support_requests_support_request_topic_id_fkey для таблицы support_requests откатывать не нужно, т.к. он ошибочно ссылался на свою же таблицу */

    const attributes = {
      ...commonAttr,
      fields: [SUPPORT_REQUEST_ID_COLUMN],
      name: TICKETS_SUPPORT_REQUEST_ID_FKEY,
      references: {
        table: SUPPORT_REQUEST_TABLE,
        field: ID,
      },
      transaction,
    };

    await Promise.all([
      queryInterface.removeColumn(TICKETS_TABLE, ORGANIZATION_TARIFF_SERVICE_ID_COLUMN, { transaction }),
      queryInterface.removeColumn(TICKETS_TABLE, ORGANIZATION_SERVICE_ID_COLUMN, { transaction }),
    ]);

    await Promise.all([
      queryInterface.addColumn(
        TICKETS_TABLE, DEVICE_TYPE_ID_COLUMN,
        templates(DataTypes).foreign({ table: DEVICE_TYPES_TABLE, onDelete: CASCADE, allowNull: false }),
        { transaction },
      ),
      queryInterface.renameTable(
        ORGANIZATION_TARIFF_SERVICES_TABLE,
        ORGANIZATION_TARIFF_BALANCES_TABLE,
        { transaction },
      ),
      queryInterface.addConstraint(TICKETS_TABLE, attributes),
    ]);
  }),
};
