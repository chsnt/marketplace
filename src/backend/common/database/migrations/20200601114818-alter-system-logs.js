export default {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction((transaction) => Promise.all([
    queryInterface.changeColumn('system_logs', 'error_stack', { type: DataTypes.TEXT }, { transaction }),
    queryInterface.changeColumn('system_logs', 'error_message', { type: DataTypes.TEXT }, { transaction }),
  ])),
  down: async () => {}, // do nothing
};
