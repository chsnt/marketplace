import dbTemplates from '../../utils/dbTemplates';

const TABLES = {
  AVAILABLE_STATUSES: 'available_catalog_statuses',
  SERVICES: 'services',
  TARIFFS: 'tariffs',
};

const COLUMNS = {
  AVAILABLE_STATUS_ID: 'available_catalog_status_id',
};

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    const templates = dbTemplates(DataTypes);

    await queryInterface.createTable(TABLES.AVAILABLE_STATUSES, {
      id: templates.id,
      name: { type: DataTypes.STRING, allowNull: false },
      alias: { type: DataTypes.STRING, allowNull: false },
      ...templates.timestamps,
      ...templates.by,
    }, { transaction });

    await Promise.all([
      queryInterface.addColumn(
        TABLES.SERVICES,
        COLUMNS.AVAILABLE_STATUS_ID,
        dbTemplates(DataTypes).foreign({ table: TABLES.AVAILABLE_STATUSES }),
        { transaction },
      ),

      queryInterface.addColumn(
        TABLES.TARIFFS,
        COLUMNS.AVAILABLE_STATUS_ID,
        dbTemplates(DataTypes).foreign({ table: TABLES.AVAILABLE_STATUSES }),
        { transaction },
      ),
    ]);
  }),

  down: (queryInterface) => queryInterface.sequelize.transaction(async (transaction) => {
    await Promise.all([
      queryInterface.removeColumn(TABLES.SERVICES, COLUMNS.AVAILABLE_STATUSES, { transaction }),
      queryInterface.removeColumn(TABLES.TARIFFS, COLUMNS.AVAILABLE_STATUSES, { transaction }),
    ]);

    await queryInterface.dropTable(TABLES.AVAILABLE_STATUSES, { transaction });
  }),
};
