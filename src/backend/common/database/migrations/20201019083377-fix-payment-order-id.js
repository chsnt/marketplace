const PAYMENTS_TABLE = 'payments';

const PAYMENT_ORDER_ID_COLUMN = 'payment_order_id';

module.exports = {
  up: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(
    async (transaction) => queryInterface.changeColumn(PAYMENTS_TABLE, PAYMENT_ORDER_ID_COLUMN, {
      type: DataTypes.STRING,
    }, { transaction }),
  ),
  down: (queryInterface, DataTypes) => queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.removeColumn(PAYMENTS_TABLE, PAYMENT_ORDER_ID_COLUMN, { transaction });

    await queryInterface.addColumn(PAYMENTS_TABLE,
      PAYMENT_ORDER_ID_COLUMN,
      { type: DataTypes.UUID },
      { transaction });
  }),
};
