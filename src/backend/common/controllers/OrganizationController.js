import Joi from '@hapi/joi';
import OrganizationService from '../services/OrganizationService';

export default class {
  static get validators() {
    return {
      getInfoFromCloud: Joi.object({
        inn: Joi.string(),
        ogrn: Joi.string(),
      }).xor('inn', 'ogrn'),
    };
  }

  static async getInfoFromCloud(req, res) {
    const { inn, ogrn } = req.query;
    const result = await OrganizationService.getInfoFromCloud(`${inn ? `inn=${inn}` : `ogrn=${ogrn}`}`);
    return res.json(result);
  }
}
