import Joi from '@hapi/joi';
import CityService from '../services/CityService';

export default class CityController {
  static get validators() {
    return {
      getList: Joi.object({
        region: Joi.string().allow(''),
        city: Joi.string().allow(''),
        onlyTestingParticipant: Joi.boolean(),
      })
        .rename('onlyTestingParticipants', 'onlyTestingParticipant'),
    };
  }

  static async getList(req, res) {
    const cities = await CityService.getList(req.query);
    return res.json(cities);
  }
}
