import fs from 'fs';
import path from 'path';
import DeviceService from '../services/DeviceService';
import OrganizationService from '../services/OrganizationService';
import IntegrationExportService from '../services/IntegrationExportService';
import OrderService from '../services/OrderService';
import { OrderDocumentTypeModel } from '../database/models/Order';
import MailClient from '../libs/MailClient';
import UserService from '../services/UserService';
import FileService from '../services/FileService';
import logger from '../services/LoggerService';

export default class IntegrationController {
  /**
   *
   * @param {Number} orderNumber Порядковый номер заказа
   * @param {String} orderId Номер заказа
   * @param {String} clientId UUID клиента
   * @param {Boolean} isFirstOrder Если первый заказ
   * @param {String} [user] UUID пользователя. Обязательно, если это первый заказ
   * @param {Object} offer Договор оферты
   * @param {String} offer.id UUID договора оферты
   * @param {Object.<Date>} offer.createdAt Дата создание документа
   * @param {Number} offer.count Порядковый номер
   * @param {String.<'YYYY.MM.DD.HH.mm.ss'>} createdAt Дата создания заказа
   * @param {Array.<Object>} services Разовые услуги
   * @param {String} services.externalId ID услуги интегратора
   * @param {String} services.cost Цена
   * @param {String} services.clientDeviceId Оборудование для которого приобреталась услуга
   * @param {Array} tariffs Тарифы
   * @param {String} tariffs.externalId ID тарифа интегратора
   * @param {String} tariffs.cost Цена
   * @param {String} tariffs.clientDeviceId Оборудование для которого приобретался тариф
   * @returns {Promise<void>}
   */
  static async createOrder({
    orderNumber, orderId, clientId, isFirstOrder,
    userId, offer, createdAt,
    services, tariffs,
  }) {
    const purchases = [...tariffs, ...services].reduce((acc, current) => acc.concat({
      GoodsId: current.externalId,
      Total: current.cost,
      Quantity: 1,
      clientDeviceId: current.clientDeviceId,
    }), []);

    const purchasesWithManagementService = await Promise.all(purchases.map(async (purchase) => {
      const device = await DeviceService.getClientDevice(purchase.clientDeviceId);
      purchase.deviceTypeId = device.deviceTypeId;
      purchase.TerBankID = device.address.managementService.externalId;
      return purchase;
    }));

    const goods = [...purchasesWithManagementService.reduce((map, current) => {
      const key = `${current.GoodsId}_${current.TerBankID}`;
      const existing = map.get(key) || {};
      map.set(key, {
        ...current, ...existing, Quantity: (existing.Quantity || 0) + 1, Total: (existing.Total || 0) + current.Total,
      });
      return map;
    }, new Map()).values()];

    const organization = await OrganizationService.getInfo({ clientId });

    if (isFirstOrder) {
      await IntegrationExportService.createContragent({ userId, organizationId: organization.id });
      await IntegrationExportService.createOrUpdateBankAccount(organization);
    }

    await IntegrationExportService.createContract({
      organizationId: organization.id,
      bankAccount: organization.bank.bankAccount,
      management: goods[0],
      offer,
      createdAt,
    });

    const files = await IntegrationExportService.createOrder({
      orderNumber,
      organizationId: organization.id,
      offer,
      createdAt,
      goods,
    }) || [];

    const addedFiles = await Promise.all(files.map((file) => OrderService.addDocumentToOrder({
      typeId: OrderDocumentTypeModel.TYPES.INVOICE,
      orderId,
      file: {
        name: file,
        fsName: file,
      },
    })));

    const attachments = addedFiles.reduce((result, current) => {
      const { extension } = FileService.getFilenameWithExtension(current.document?.name);

      result[current.document?.name] = fs.readFileSync(
        path.join(FileService.DEFAULT_FILE_PATH, 'order',
          `${current.document?.id}.${extension}`), 'base64',
      );

      return result;
    }, {});

    try {
      const user = await UserService.getClientMaintainer({ clientId });

      await MailClient.order({
        attachmentsBinary: attachments,
        name: 'invoice',
        recipients: [{
          name: `${user?.lastName || ''} ${user?.firstName || ''} ${user?.patronymicName || ''}`,
          email: user?.email,
        }],
        variables: { orderNumber },
      });
    } catch (error) {
      logger.error(error);
    }
  }

  static createOrUpdateBankAccount(bankDetails) {
    return IntegrationExportService.createOrUpdateBankAccount(bankDetails);
  }
}
