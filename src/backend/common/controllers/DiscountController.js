import Joi from '@hapi/joi';
import {
  checkRequiredId,
  discountTemplateSort,
  limitOffset,
  couponSort,
  checkId,
  discountServiceSort,
  dateValidationWithTimeZone,
} from '../utils/validation';
import DiscountService from '../services/DiscountService';
import { DiscountTypeModel } from '../database/models/Discount';

export default class DiscountController {
  static get validators() {
    const tariffOrServiceRelations = Joi.array().items(Joi.object({
      id: checkRequiredId,
      value: Joi.number(),
    }));

    return {
      createTemplate: Joi.object({
        discountTypeId: checkRequiredId,
        value: Joi.number().when('discountTypeId', {
          is: Joi.exist().valid(DiscountTypeModel.TYPES.DISCOUNT),
          then: Joi.required(),
          otherwise: Joi.forbidden(),
        }),
        description: Joi.string().required(),
        timeToLive: Joi.number(),
        limit: Joi.number(),
        serviceRelations: tariffOrServiceRelations,
        tariffRelations: tariffOrServiceRelations,
      }),
      getTemplateList: Joi.object({
        ...limitOffset,
        sort: discountTemplateSort,
      }),
      createCoupon: Joi.object({
        discountTemplateId: checkRequiredId,
        code: Joi.string(),
      }),
      getCouponList: Joi.object({
        ...limitOffset,
        sort: couponSort,
      }),
      getCoupon: Joi.object({
        id: checkRequiredId,
      }),
      deleteCoupon: Joi.object({
        id: checkRequiredId,
      }),
      createDiscountService: Joi.object({
        discountTemplateId: checkRequiredId,
        services: Joi.array().items(checkId).min(1).required(),
      }),
      getDiscountServiceList: Joi.object({
        ...limitOffset,
        sort: discountServiceSort,
      }),
      getDiscountService: Joi.object({
        id: checkRequiredId,
      }),
      deleteDiscountService: Joi.object({
        id: checkRequiredId,
      }),
      createDiscountByCard: Joi.object({
        value: Joi.number().required(),
        expirationAt: dateValidationWithTimeZone,
      }),
    };
  }

  static async createTemplate(req, res) {
    const result = await DiscountService.createTemplate(req.body);
    return res.json(result.id);
  }

  static async getTemplateList(req, res) {
    const result = await DiscountService.getTemplateList(req.query);
    return res.json(result);
  }

  static async getDiscountTypeList(req, res) {
    const result = await DiscountService.getDiscountTypeList();
    return res.json(result);
  }

  static async createCoupon(req, res) {
    const template = await DiscountService.getTemplate(req.body.discountTemplateId);
    const result = await DiscountService.createCoupon({
      ...req.body,
      timeToLive: template.timeToLive,
    });
    return res.json(result.id);
  }

  static async getCouponList(req, res) {
    const result = await DiscountService.getCouponList(req.query);
    return res.json(result);
  }

  static async getCoupon(req, res) {
    const result = await DiscountService.getCoupon(req.query.id);
    return res.json(result);
  }

  static async deleteCoupon(req, res) {
    await DiscountService.deleteCoupon(req.body.id);
    return res.json(null);
  }

  static async createDiscountService(req, res) {
    const template = await DiscountService.getTemplate(req.body.discountTemplateId);
    const result = await DiscountService.createDiscountService({
      ...req.body,
      expirationDay: template.expirationDay,
    });
    return res.json(result.id);
  }

  static async getDiscountServiceList(req, res) {
    const result = await DiscountService.getDiscountServiceList(req.query);
    return res.json(result);
  }

  static async getDiscountService(req, res) {
    const result = await DiscountService.getDiscountService(req.query.id);
    return res.json(result);
  }

  static async deleteDiscountService(req, res) {
    await DiscountService.deleteDiscountService(req.body.id);
    return res.json(null);
  }

  static async createDiscountByCard(req, res) {
    const result = await DiscountService.createDiscountByCard(req.body);
    return res.json(result.id);
  }

  static async getCurrentDiscountByCard(req, res) {
    const result = await DiscountService.getCurrentDiscountByCard();
    return res.json(result);
  }

  static async deleteDiscountByCard(req, res) {
    await DiscountService.deleteDiscountByCard();
    return res.json(null);
  }
}
