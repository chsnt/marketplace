import Joi from '@hapi/joi';
import DeviceService from '../services/DeviceService';
import { checkRequiredId } from '../utils/validation';

export default class DeviceController {
  static get validators() {
    return {
      getDeviceServices: Joi.object({
        id: checkRequiredId,
      }),
    };
  }

  static async getDeviceServices(req, res) {
    const result = await DeviceService.getAllowedServices({ clientDeviceId: req.params.id });

    return res.json(result);
  }

  static async getDeviceTypes(req, res) {
    const result = await DeviceService.getDeviceTypes();

    return res.json(result);
  }
}
