import TariffService from '../services/TariffService';
import ServiceService from '../services/ServiceService';
import DeviceTypeModel from '../database/models/Device/DeviceTypeModel';

export default class ClientController {
  /**
   *
   * @param {String} clientId UUID клиента
   * @return {Promise<Array<Object>>} Список доступных услуг разбитых на адреса обслуживания и, при их отсутствии, на типы адресов услуг
   */
  static async getClientAllowedServices(clientId) {
    const clientTariffServices = await TariffService.getClientTariffs({ clientId });
    const clientServices = await ServiceService.getClientServices({ clientId });
    const services = new Map();

    const prepareService = ({
      id, name, alias, deviceTypeId, serviceAddressType, serviceAddress, value,
    }) => {
      if (value !== undefined && value <= 0) return;

      const currentAccountingId = serviceAddress?.id || serviceAddressType;
      const currentAccountingType = services.get(currentAccountingId);
      const allowedService = {
        id,
        name,
        alias,
        deviceType: DeviceTypeModel.NAMES[deviceTypeId],
        serviceAddressType,
      };

      if (!currentAccountingType) {
        return services.set(currentAccountingId, {
          services: new Map([[id, {
            ...allowedService,
            value: value || 1,
          }]]),
          serviceAddressType,
          serviceAddress,
        });
      }

      const currentService = currentAccountingType.services.get(id);

      currentAccountingType.services.set(id, {
        ...allowedService,
        value: (value || 1) + (currentService?.value || 0),
      });
    };

    clientTariffServices.forEach(({ tariff, serviceAddress }) => {
      tariff.services.forEach(({
        service: {
          id, name, alias, deviceTypeId, serviceAddressType,
        }, value,
      }) => prepareService({
        id, name, alias, deviceTypeId, serviceAddressType, serviceAddress, value,
      }));
    });

    clientServices.forEach(prepareService);

    services.forEach((value, key) => {
      services.set(key, {
        ...value,
        services: Array.from(value.services.values()),
      });
    });

    return Array.from(services.values());
  }
}
