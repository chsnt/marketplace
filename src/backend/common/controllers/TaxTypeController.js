import TaxTypeService from '../services/TaxTypeService';

export default class TaxTypeController {
  static async getList(req, res) {
    const taxTypes = await TaxTypeService.getList();

    return res.json(taxTypes);
  }
}
