import Joi from '@hapi/joi';
import moment from 'moment';
import { checkRequiredId } from '../utils/validation';
import TicketService from '../services/TicketService';
import ServiceService from '../services/ServiceService';
import TariffService from '../services/TariffService';
import APIError from '../utils/APIError';
import ERRORS from '../constants/errors';
import DeviceService from '../services/DeviceService';
import MailClient from '../libs/MailClient';
import { DATE_FORMAT } from '../constants';
import ClientService from '../services/ClientService';
import logger from '../services/LoggerService';
import is from '../libs/is';
import { PaymentModel } from '../database/models/Order';

export default class TicketController {
  static get validators() {
    return {
      getDocument: Joi.object({
        documentId: checkRequiredId,
      }),
    };
  }

  static async getDocument(req, res) {
    const { pathToFile, filename } = await TicketService.getDocument(req.params.documentId);
    return res.download(pathToFile, filename);
  }

  static async createTicket({
    user, docNames, services, serviceAddressId, device, ...ticket
  }) {
    const clientId = ClientService.getClientId(user);
    const deviceData = await DeviceService.createClientDevice({
      device,
      clientId,
    });

    const newServiceAddressId = deviceData?.serviceAddress.id;

    const deviceTypeId = await ServiceService.checkSameDeviceType(services);

    TicketService.checkServicesUnique(services);

    const clientServicesMap = new Map();

    const clientServiceAddressTypes = new Set();

    const availableClientTariffServices = await TariffService.getAvailableService({ services, clientId });

    const availableClientServices = await ServiceService.getAvailableClientServices({ services, clientId });

    availableClientTariffServices.forEach((clientTariffService) => {
      const currentService = clientServicesMap.get(clientTariffService.service.id);
      if (clientTariffService.value <= 0) return;

      clientServiceAddressTypes.add(clientTariffService.service.serviceAddressAccountingTypeId);

      /* Услуга должна быть из тарифа, который закончиться раньше */
      const isBeforeCurrentService = moment(clientTariffService.expirationAt)
        .isBefore(currentService?.tariffServiceRelation?.expirationAt);

      clientServicesMap.set(clientTariffService.service.id, {
        ...(currentService && currentService),
        tariffServiceRelation: (!currentService?.tariffServiceRelation || isBeforeCurrentService)
          ? {
            serviceId: clientTariffService.id,
            expirationAt: clientTariffService.expirationAt,
          }
          : currentService.tariffServiceRelation,
      });
    });

    availableClientServices.forEach((availableClientService) => {
      const currentService = clientServicesMap.get(availableClientService.serviceId);

      /* если услуга есть в тарифе, разовую услугу не используем  */
      if (currentService?.tariffServiceRelation) return;

      const { paymentDate } = PaymentModel.getCurrentPayment(availableClientService.order.payments);

      clientServiceAddressTypes.add(availableClientService.service.serviceAddressAccountingTypeId);

      /* Услуга должна быть первой */
      const isBeforeCurrentService = moment(paymentDate)
        .isBefore(currentService?.oneTimeServices?.paymentDate);

      clientServicesMap.set(availableClientService.serviceId, {
        ...(currentService && currentService),
        oneTimeServices: (!currentService?.oneTimeServices || isBeforeCurrentService)
          ? {
            serviceId: availableClientService.id,
            paymentDate,
          }
          : currentService.oneTimeServices,
      });
    });

    if (services.length !== clientServicesMap.size) {
      throw new APIError(ERRORS.TICKET.SERVICE_NOT_PURCHASED);
    }

    const shouldTicketHasAddress = [...clientServiceAddressTypes]
      .some((serviceAddressAccountingTypeId) => is.service.serviceAddressAccountingType.inTicket(
        serviceAddressAccountingTypeId,
      ));

    if (shouldTicketHasAddress && !serviceAddressId) {
      throw new APIError(ERRORS.TICKET.TICKET_MUST_HAS_SERVICE_ADDRESS);
    }

    const { tariffServiceRelations, oneTimeServices } = services.reduce((acc, serviceId) => {
      const currentService = clientServicesMap.get(serviceId);

      if (currentService.tariffServiceRelation) {
        acc.tariffServiceRelations.push(currentService.tariffServiceRelation.serviceId);
        return acc;
      }
      acc.oneTimeServices.push(currentService.oneTimeServices.serviceId);

      return acc;
    }, { tariffServiceRelations: [], oneTimeServices: [] });

    const result = await TicketService.createTicket({
      clientId,
      services,
      docNames,
      serviceAddressId: serviceAddressId || newServiceAddressId,
      deviceTypeId,
      tariffServiceRelations,
      oneTimeServices,
      ...ticket,
    });

    try {
      await MailClient.ticket({
        name: 'created',
        recipients: [{
          name: `${user?.lastName || ''} ${user?.firstName || ''} ${user?.patronymicName || ''}`,
          email: user?.email,
        }],
        variables: {
          serial: result.serial,
          createdAt: moment(result.createdAt).format(DATE_FORMAT.KEBAB_NORMAL),
        },
      });
    } catch (error) {
      logger.error(error);
    }

    return result;
  }
}
