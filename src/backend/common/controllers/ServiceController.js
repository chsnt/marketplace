import Joi from '@hapi/joi';
import ServiceService from '../services/ServiceService';
import { checkRequiredId, checkId } from '../utils/validation';

export default class ServiceController {
  static get validators() {
    return {
      getList: Joi.object({
        deviceTypeId: checkId,
        search: Joi.string().allow(''),
      }),
      getAllList: Joi.object({
        search: Joi.string().allow(''),
      }),
      getCompletedList: Joi.object({
        clientId: checkRequiredId,
      }),
      getAllowedServicesForDevice: Joi.object({
        deviceId: checkRequiredId,
      }),
      getService: Joi.object({
        id: checkRequiredId,
      }),
      updateService: Joi.object({
        id: checkRequiredId,
        availableCatalogStatusId: checkId,
        availablePaymentMethodId: checkId,
        externalId: checkId,
        name: Joi.string().min(1).max(255),
        label: Joi.string().min(1).max(255),
        icon: Joi.string().min(1).max(255),
        alias: Joi.string().min(1).max(255),
        nameInTariff: Joi.string().min(1).max(255),
        forTariffOnly: Joi.boolean(),
        description: Joi.string().max(1024).allow(null, ''),
      }).min(2),
    };
  }

  static async getList(req, res) {
    const result = await ServiceService.getList({ ...req.query, forTariffOnly: false });
    res.json(result);
  }

  static async getCompletedList(req, res) {
    const result = await ServiceService.getClientServices({
      clientId: req.query.clientId,
      used: true,
    });
    res.json(result);
  }

  static async updateService(req, res) {
    const result = await ServiceService.updateService(req.body);
    res.json(result);
  }

  static async getAvailablePaymentMethods(req, res) {
    const result = await ServiceService.getAvailablePaymentMethods();
    res.json(result);
  }
}
