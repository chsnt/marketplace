
import Joi from '@hapi/joi';

import {
  checkPhone,
  checkAddress,
  organization,
  dateOnly,
  confirmPasswordValidation,
  passwordValidation,
  bankDetail,
  checkId,
} from '../utils/validation';
import UserService from '../services/UserService';
import { VERIFICATION_MAIL } from '../constants';

export default class UserController {
  static get validators() {
    const forUsual = Joi.object({
      isTemporaryPassword: Joi.boolean().default(false), // Подставляется в isAuthenticated, если пользователь со временным паролем.
      user: Joi.object({
        id: checkId,
        firstName: Joi.string(),
        lastName: Joi.string(),
        patronymicName: Joi.string().allow(null, ''),
        email: Joi.string().email(),
        birthday: dateOnly,
        phone: checkPhone(),
        oldPassword: Joi.string(),
        password: passwordValidation,
        confirmPassword: confirmPasswordValidation,
      }).and('oldPassword', 'password', 'confirmPassword'),
      organization: Joi.object({
        ...organization,
        legalAddress: Joi.object(checkAddress).xor('cityId', 'city', 'fiasId').rename('region', 'federationSubjectId'),
        realAddress: Joi.object(checkAddress).xor('cityId', 'city', 'fiasId').rename('region', 'federationSubjectId'),
        bankDetail: Joi.object(bankDetail),
      }),
    });

    const forTemporaryPassword = Joi.object({
      isTemporaryPassword: Joi.boolean().default(false), // Подставляется в isAuthenticated, если пользователь со временным паролем.
      user: Joi.object({
        password: passwordValidation,
        confirmPassword: confirmPasswordValidation,
      }).and('password', 'confirmPassword'),
    });

    return {
      id: Joi.object({
        id: checkId,
      }),

      updateUser: Joi.object({
        isTemporaryPassword: Joi.boolean().default(false), // Подставляется в isAuthenticated, если пользователь со временным паролем.
      }).when('.isTemporaryPassword', {
        is: Joi.exist().invalid(false),
        then: forTemporaryPassword,
        otherwise: forUsual,
      }),

      confirmationEmail: Joi.object({
        codeMail: Joi.string().required().min(VERIFICATION_MAIL.LENGTH_CODE).max(VERIFICATION_MAIL.LENGTH_CODE),
      }),
    };
  }

  static async getInfo(req, res) {
    const data = await UserService.getInfo(req.user.id);
    return res.json(data);
  }
}
