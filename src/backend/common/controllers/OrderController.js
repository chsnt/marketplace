import Joi from '@hapi/joi';
import moment from 'moment';
import OrderService from '../services/OrderService';
import { checkRequiredId } from '../utils/validation';
import UserService from '../../client/services/UserService';
import MailService from '../services/MailService';
import MailClient from '../libs/MailClient';
import { INTEGRATOR, SENDPULSE } from '../constants/env';
import logger from '../services/LoggerService';
import { ServiceModel, TariffModel } from '../database/models/Tariff';
import RabbitClient from '../libs/RabbitClient';

export default class OrderController {
  static get validators() {
    return {
      getDocument: Joi.object({ documentId: checkRequiredId }),
    };
  }

  static async getDocument(req, res) {
    const { pathToFile, filename } = await OrderService.getDocument(req.params.documentId);
    return res.download(pathToFile, filename);
  }

  static async getLastOffer(req, res) {
    const offerDocumentAndCount = await OrderService.getLastOfferDocumentAndCount();
    const { pathToFile } = await OrderService.getDocument(offerDocumentAndCount.document?.id);
    return res.sendFile(pathToFile);
  }

  static async completeOrder(paymentOrderId) {
    const payment = await OrderService.completeOrder(paymentOrderId);

    const {
      id: orderId,
      clientId,
      number: orderNumber,
      clientTariffs,
      clientServices,
      createdAt,
      orderOfferDocument,
    } = payment.order;

    const user = await UserService.getClientMaintainer({ clientId });

    try {
      const mailData = await MailService.prepareInvoiceForUser({
        clientId,
        clientTariffs,
        clientServices,
        createdAt,
        paymentDate: payment.paymentDate,
      });

      const template = MailService.createInvoiceTemplate(mailData.goods);

      await MailClient.order({
        name: 'issuedAndPaid',
        recipients: [{
          email: SENDPULSE.MAIL_RECIPIENT,
        }, {
          name: `${user.lastName} ${user.firstName} ${user.patronymicName}`,
          email: user.email,
        }],
        variables: {
          orderNumber,
          totalCost: mailData.totalCost,
          data: template,
        },
      });
    } catch (error) {
      logger.error(error);
    }


    if (INTEGRATOR.IS_SENDING_ENABLED) {
      const isFirstOrder = await OrderService.checkFirstOrder({ clientId, orderId });

      const services = clientServices.map((service) => ({
        externalId: service.service.externalId,
        cost: ServiceModel.getCurrentCost(service.costs),
        clientDeviceId: service.clientDeviceId,
      }));
      const tariffs = clientTariffs.map((tariff) => ({
        externalId: tariff.tariff.externalId,
        cost: TariffModel.getCurrentCost(tariff.costs),
        clientDeviceId: tariff.clientDeviceId,
      }));

      try {
        const rabbitClient = new RabbitClient();
        await rabbitClient.sendToQueue({
          queue: rabbitClient.queues.integrationCreateOrder,
          message: {
            orderId,
            orderNumber: payment.number,
            clientId,
            isFirstOrder,
            userId: user?.id,
            offer: orderOfferDocument,
            createdAt: moment(createdAt).format('YYYY.MM.DD.HH.mm.ss'),
            tariffs,
            services,
          },
        });
      } catch (error) {
        logger.error(error);
      }
    }
  }

  static async getOrderStatusList(req, res) {
    const result = await OrderService.getOrderStatusList();

    return res.json(result);
  }
}
