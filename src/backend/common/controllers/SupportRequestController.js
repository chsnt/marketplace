import Joi from '@hapi/joi';
import moment from 'moment';
import SupportRequestService from '../services/SupportRequestService';
import { SupportRequestTopicModel } from '../database/models/SupportRequest';

import { checkRequiredId } from '../utils/validation';
import MailClient from '../libs/MailClient';
import { DATE_FORMAT } from '../constants';
import logger from '../services/LoggerService';

export default class SupportRequestController {
  static get validators() {
    return {
      createRequest: Joi.object({
        topic: checkRequiredId,
        description: Joi.string().max(1024).required(),
      }),
      getDocument: Joi.object({
        documentId: checkRequiredId,
      }),
      createReview: Joi.object({
        supportRequestId: checkRequiredId,
        mark: Joi.number().max(10).required(),
        text: Joi.string().max(255).required(),
      }),
    };
  }

  static async getTopics(req, res) {
    const result = await SupportRequestService.getTopics();

    return res.json(result);
  }

  static async createRequest(req, res) {
    const docNames = req.files.map((file) => ({ fsName: file.filename, name: file.originalname }));

    /* Получаем необходимые атрибуты текущего пользователя и пробрасываем их в запрос */
    const {
      firstName, patronymicName, lastName, phone, email,
    } = req.user;

    const result = await SupportRequestService.createRequest({
      clientId: req.user.clientId,
      ...req.body,
      firstName,
      patronymicName,
      lastName,
      phone,
      docNames,
      email,
    });

    if (result.supportRequestTopicId === SupportRequestTopicModel.TOPICS.ETC) {
      try {
        await MailClient.supportRequest({
          name: 'created',
          recipients: [{
            name: `${result.lastName || ''} ${result.firstName || ''} ${result.patronymicName || ''}`,
            email: result.email,
          }],
          variables: {
            number: result.number,
            createdAt: moment(result.createdAt).format(DATE_FORMAT.KEBAB_NORMAL),
            topic: result.topicName,
          },
        });
      } catch (error) {
        logger.error(error);
      }
    }

    return res.json(result.id);
  }

  static async getDocument(req, res) {
    const { pathToFile, filename } = await SupportRequestService.getDocument(req.params.documentId);
    return res.download(pathToFile, filename);
  }
}
