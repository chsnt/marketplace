import Joi from '@hapi/joi';
import TariffService from '../services/TariffService';
import {
  checkId, checkRequiredId,
} from '../utils/validation';
import LimitMeasureModel from '../database/models/Tariff/LimitMeasureModel';

export default class TariffController {
  static get validators() {
    return {
      getList: Joi.object({
        search: Joi.string().allow(''),
        deviceTypeId: checkId,
      }),
      getAllList: Joi.object({
        search: Joi.string(),
      }),
      getCompletedList: Joi.object({
        clientId: checkRequiredId,
      }),
      getTariff: Joi.object({
        id: checkRequiredId,
      }),
      updateTariff: Joi.object({
        id: checkRequiredId,
        availableCatalogStatusId: checkId,
        externalId: checkId,
        name: Joi.string().min(1).max(255),
        label: Joi.string().min(1).max(255),
        icon: Joi.string().min(1).max(255),
        alias: Joi.string().min(1).max(255),
        description: Joi.string().min(1).max(255),
        expirationMonths: Joi.number(),
        services: Joi.array().items(Joi.object({
          serviceId: checkRequiredId,
          limit: Joi.number().required().allow(null),
          limitMeasureId: checkId.default(LimitMeasureModel.STATUSES.COUNT_PER_MONTH),
        })).min(1),
      }).min(2),
    };
  }

  static async getList(req, res) {
    const result = await TariffService.getList(req.query);
    res.json(result);
  }

  static async getTariff(req, res) {
    const result = await TariffService.getTariff({
      ...req.params,
      isCostRequired: true,
    });
    res.json(result);
  }

  static async getCompletedList(req, res) {
    const result = await TariffService.getClientTariffs({ clientId: req.query.clientId, used: true });
    res.json(result);
  }

  static async updateTariff(req, res) {
    const result = await TariffService.updateTariff(req.body);
    res.json(result);
  }
}
