import EdoService from '../../client/services/EdoService';

export default class EdoController {
  static async getList(req, res) {
    const operators = await EdoService.getList();

    return res.json(operators);
  }
}
