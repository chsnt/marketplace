import APIError from '../utils/APIError';
import RedisService from './RedisService';
import { TEMPORARY_PASSWORD_EXPIRE, VERIFICATION_MAIL } from '../constants/index';
import { RESET_PASS_TIMEOUT, SERVICE_DOMAIN } from '../constants/env';
import MailClient from '../libs/MailClient';
import ERRORS from '../constants/errors';
import { generateNewPassword } from '../utils/auth';
import NotifierService from './NotifierService';

const redis = new RedisService();

const PASSWORD_COMPLEXITY_RULES = [
  {
    regexp: '[a-z]',
    error: 'Должна присутствовать хотя бы одна строчная буква',
  },
  {
    regexp: '[A-Z]',
    error: 'Должна присутствовать хотя бы одна прописная буква',
  },
  {
    regexp: '[0-9]',
    error: 'Должна присутствовать хотя бы одна цифра',
  },
  {
    regexp: '[!@#$%^&*]',
    error: 'Должен присутствовать хотя бы один специальный символ из списка: !@#$%^&*',
  },
];
const MAX_COMPLEXITY_RULES_ERRORS = 1;

export default class AuthService {
  static loginUser = async (req, user) => new Promise((resolve, reject) => {
    req.login(user, (error) => {
      if (error) reject(error);
      resolve();
    });
  })

  static passwordComplexityCheck({ email, phone, password }) {
    if (email === password || phone === password) {
      throw new APIError(ERRORS.AUTH.LOGIN_AND_PASSWORD_SHOULD_NOT_BE_SAME);
    }

    let complexityErrorCounter = 0;

    const errors = PASSWORD_COMPLEXITY_RULES.reduce((acc, { regexp, error }) => {
      if (password.match(regexp)) return acc;
      complexityErrorCounter += 1;
      return acc.concat(`${error}\n`);
    }, '');

    if (complexityErrorCounter > MAX_COMPLEXITY_RULES_ERRORS) {
      throw new APIError(ERRORS.COMMON.VALIDATION_ERROR(errors));
    }
  }

  /**
   *
   * @param {String} id ID пользователя
   * @param {String} password Временный пароль
   * @param {Number} [expire] время жизни
   * @description время жизни по умолчанию не задается,
   *              т.к. для пароли новых пользователей, созданных через адменку, не должны быть ограничены по времени
   * @returns {Promise<void>}
   */
  static async setTemporaryPassword(id, password, expire) {
    await redis.setQuery(`${id}:new-pass`, password, expire);
    await redis.setQuery(`${id}:prev-reset-pass-attempt`, new Date().getTime(), RESET_PASS_TIMEOUT);
  }

  static async getTemporaryPassword(id) {
    return redis.getQuery(`${id}:new-pass`);
  }

  static async deleteTemporaryPassword(id) {
    await redis.delQuery(`${id}:new-pass`);
  }

  static async setTemporaryAuthentication(id) {
    await redis.setQuery(`${id}:temporary-auth`, 'true', TEMPORARY_PASSWORD_EXPIRE);
  }

  static getTemporaryAuthentication(id) {
    return redis.getQuery(`${id}:temporary-auth`);
  }

  static async deleteTemporaryAuthentication(id) {
    await redis.delQuery(`${id}:temporary-auth`);
  }

  static async getMailCode({ email }) {
    const code = generateNewPassword({ pattern: VERIFICATION_MAIL.PATTERN, length: VERIFICATION_MAIL.LENGTH_CODE });
    /* Сохраняем код в редис на 24 часа */
    await redis.setQuery(`${code}:email`, email, VERIFICATION_MAIL.EXPIRES_IN);

    return code;
  }

  static sendSMSWithPassword({ phone, password }) {
    return NotifierService.sendSMS({
      phone,
      text: `Доступ к услугам: https://itusluga.ru/lk/ Временный пароль: ${password}`,
    });
  }

  static async sendEmailConfirmationUrl({ email }) {
    const code = await this.getMailCode({ email });
    const urlForCheckMail = new URL('/api/user/me/confirmation-email', SERVICE_DOMAIN);

    urlForCheckMail.searchParams.set('codeMail', code);

    await MailClient.registration({
      name: 'checkMail',
      recipients: [{
        email,
      }],
      variables: {
        url: urlForCheckMail.href,
      },
    });
  }
}
