import moment from 'moment';
import { UniqueConstraintError } from 'sequelize';
import {
  DiscountTemplateModel, DiscountTypeModel, CouponModel,
  DiscountTariffServiceRelationModel, DiscountServiceModel, DiscountByCardModel,
} from '../database/models/Discount';
import { DATE_FORMAT, GET_LIST_DEFAULT_LIMIT, GET_LIST_DEFAULT_SORT } from '../constants';
import APIError from '../utils/APIError';
import ERRORS from '../constants/errors';
import db from '../database';
import { ServiceModel, TariffModel } from '../database/models/Tariff';
import is from '../libs/is';

export default class DiscountService {
  static getExpirationAt(expirationDay) {
    return moment()
      .add(expirationDay, 'day')
      .startOf('day');
  }

  static getDiscountNomenclature(discountTariffServiceRelations) {
    return discountTariffServiceRelations.reduce((acc, current) => {
      if (current.tariff) {
        acc.discountTariffs.push({
          ...current.tariff,
          withoutDiscountCost: TariffModel.getCurrentCost(current.tariff.costs),
          cost: current.value,
          costs: undefined,
        });
      }

      if (current.service) {
        acc.discountServices.push({
          ...current.service,
          withoutDiscountCost: TariffModel.getCurrentCost(current.service.costs),
          cost: current.value,
          costs: undefined,
        });
      }

      return acc;
    }, { discountTariffs: [], discountServices: [] });
  }

  /**
   *
   * @param {String} description Описание
   * @param {Number} [timeToLive] Кол-во дней действия. Если значение отсутствует, то считается безлимитным по времени
   * @param {Number} [limit] Кол-во раз использования для одного клиента. Если значение отсутствует, то считается безлимитным по кол-во раз использования
   * @param {String} discountTypeId UUID типа использования шаблона (скидка или фиксированная цена)
   * @param {Number} [value] значение скидки для шаблона с типом "скидка"
   * @param {Array.<Object>} [serviceRelations] массив с объектами услуги для которых будет применена фиксированная цена. Данная опция доступна в случае выбора шаблона с фиксированной ценой
   * @param {String} serviceRelations.id Id услуги у которой будет другая, фиксированная цена
   * @param {Number} serviceRelations.value Значение новой цены
   * @param {Array.<Object>} [tariffRelations] массив с объектами услуги для которых будет применена фиксированная цена. Данная опция доступна в случае выбора шаблона с фиксированной ценой
   * @param {String} tariffRelations.id Id услуги у которой будет другая, фиксированная цена
   * @param {Number} tariffRelations.value Значение новой цены
   * @return {Promise<Object>} инстанс шаблона скидок
   */
  static async createTemplate({
    description,
    timeToLive,
    limit,
    discountTypeId,
    value,
    serviceRelations,
    tariffRelations,
  }) {
    const transaction = await db.sequelize.transaction();

    try {
      const discountTemplateInstance = await DiscountTemplateModel.create({
        description,
        timeToLive,
        limit,
        discountTypeId,
        value,
      }, { transaction });

      if (serviceRelations) {
        await Promise.all(serviceRelations.map(
          (serviceRelation) => DiscountTariffServiceRelationModel.create({
            serviceId: serviceRelation.id,
            discountTemplateId: discountTemplateInstance.id,
            value: serviceRelation.value,
          }, { transaction }),
        ));
      }

      if (tariffRelations) {
        await Promise.all(tariffRelations.map(
          (tariffRelation) => DiscountTariffServiceRelationModel.create({
            tariffId: tariffRelation.id,
            discountTemplateId: discountTemplateInstance.id,
            value: tariffRelation.value,
          }, { transaction }),
        ));
      }

      await transaction.commit();
      return discountTemplateInstance;
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }

  static async getTemplateList({
    sort,
    offset = 0,
    limit = GET_LIST_DEFAULT_LIMIT,
  }) {
    const { sort: sortBy = 'date', direction = GET_LIST_DEFAULT_SORT } = sort || {};

    const orderBy = { date: 'createdAt' };

    const discountTemplateList = await DiscountTemplateModel.findAndCountAll({
      offset,
      limit,
      attributes: ['id', 'description', 'timeToLive', 'limit', 'value'],
      include: {
        association: 'discountType',
        attributes: ['id', 'name', 'alias'],
      },
      order: [[orderBy[sortBy], direction]],
    });

    return discountTemplateList;
  }

  static async getDiscountTypeList() {
    const discountTypeList = await DiscountTypeModel.findAll({
      attributes: ['id', 'name', 'alias'],
    });

    return discountTypeList;
  }

  static async getTemplate(templateId) {
    const discountTemplateInstance = await DiscountTemplateModel.findByPk(templateId);

    if (!discountTemplateInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(templateId));

    return discountTemplateInstance;
  }


  static async createCoupon({
    code,
    timeToLive,
    discountTemplateId,
  }) {
    const expirationAt = this.getExpirationAt(timeToLive);

    try {
      const couponInstance = await CouponModel.create({
        code,
        expirationAt: (!timeToLive) ? null : expirationAt,
        discountTemplateId,
      });
      return couponInstance;
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        const errorObject = CouponModel.uniqueConstraintError(error?.original?.constraint);
        throw new APIError(errorObject);
      }
      throw error;
    }
  }

  static async getCouponList({
    sort,
    offset = 0,
    limit = GET_LIST_DEFAULT_LIMIT,
  }) {
    const { sort: sortBy = 'date', direction = GET_LIST_DEFAULT_SORT } = sort || {};

    const orderBy = { date: 'createdAt' };

    const couponList = await CouponModel.findAndCountAll({
      offset,
      limit,
      attributes: ['id', 'code', 'expirationAt', 'createdAt'],
      include: {
        association: 'discountTemplate',
        attributes: ['id', 'description', 'timeToLive', 'limit', 'value'],
        include: {
          association: 'discountType',
          attributes: ['id', 'name', 'alias'],
        },
      },
      order: [[orderBy[sortBy], direction]],
    });

    return {
      ...couponList,
      rows: couponList.rows.map((couponInstance) => {
        const {
          id, code, expirationAt, createdAt,
          discountTemplate: { value, discountType, limit: usageLimit },
        } = couponInstance.toJSON();

        return {
          id,
          value,
          code,
          limit: usageLimit,
          expirationAt,
          createdAt,
          discountType,
        };
      }),
    };
  }

  static async getCoupon(id) {
    const couponInstance = await CouponModel.findByPk(id, {
      attributes: ['id', 'code', 'expirationAt', 'createdAt'],
      include: {
        association: 'discountTemplate',
        attributes: ['id', 'description', 'timeToLive', 'limit', 'value'],
        include: [
          {
            association: 'discountType',
            attributes: ['id', 'name', 'alias'],
          },
          {
            association: 'discountTariffServiceRelations',
            attributes: ['value'],
            include: [
              {
                association: 'tariff',
                attributes: ['id', 'name'],
                include: 'costs',
              },
              {
                association: 'service',
                attributes: ['id', 'name'],
                include: 'costs',
              },
            ],
          },
        ],
      },
    });

    if (!couponInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(id));

    const {
      code, expirationAt, createdAt,
      discountTemplate: {
        value, discountType, discountTariffServiceRelations, limit,
      },
    } = couponInstance.toJSON();

    const { discountTariffs, discountServices } = this.getDiscountNomenclature(discountTariffServiceRelations);

    return {
      value,
      code,
      limit,
      expirationAt,
      createdAt,
      discountType,
      discounts: {
        services: discountServices,
        tariffs: discountTariffs,
      },
    };
  }

  static deleteCoupon(id) {
    return CouponModel.destroy({
      where: { id },
    });
  }


  static async createDiscountService({
    discountTemplateId,
    services,
    expirationDay,
  }) {
    const expirationAt = this.getExpirationAt(expirationDay);

    const transaction = await db.sequelize.transaction();

    try {
      const discountServicesInstance = await DiscountServiceModel.create({
        expirationAt: (Number.POSITIVE_INFINITY === expirationDay) ? null : expirationAt,
        discountTemplateId,
      }, { transaction });

      const serviceInstances = await ServiceModel.findAll({
        where: {
          id: services,
        },
        attributes: { exclude: ['service_id'] },
        transaction,
      });

      if (!serviceInstances.length) throw new APIError(ERRORS.SERVICE.DISCOUNT_SERVICE_NOT_FOUND);

      await Promise.all(serviceInstances.map((serviceInstance) => serviceInstance.update({
        discountServiceId: discountServicesInstance.id,
      }, { transaction })));

      await transaction.commit();
      return discountServicesInstance;
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }

  static async getDiscountServiceList({
    sort,
    offset = 0,
    limit = GET_LIST_DEFAULT_LIMIT,
  }) {
    const { sort: sortBy = 'date', direction = GET_LIST_DEFAULT_SORT } = sort || {};

    const orderBy = { date: ['createdAt'] };

    const discountServiceList = await DiscountServiceModel.findAndCountAll({
      offset,
      limit,
      attributes: ['id', 'expirationAt', 'createdAt'],
      include: [
        {
          association: 'discountTemplate',
          attributes: ['limit', 'value'],
          include: {
            association: 'discountType',
            attributes: ['id', 'name', 'alias'],
          },
        },
        {
          association: 'services',
          attributes: ['id', 'name'],
        },
      ],
      order: [[...orderBy[sortBy], direction]],
    });

    return {
      ...discountServiceList,
      rows: discountServiceList.rows.map((discountServiceInstance) => {
        const {
          id, expirationAt, createdAt,
          discountTemplate: { value, discountType, limit: usageLimit },
          services,
        } = discountServiceInstance.toJSON();

        return {
          id,
          value,
          limit: usageLimit,
          expirationAt,
          createdAt,
          discountType,
          services,
        };
      }),
    };
  }

  static async getDiscountService(id) {
    const discountServiceInstance = await DiscountServiceModel.findByPk(id, {
      attributes: ['expirationAt'],
      include: [
        {
          association: 'services',
          attributes: ['id', 'name'],
        },
        {
          association: 'discountTemplate',
          attributes: ['description', 'limit', 'value'],
          include: [
            {
              association: 'discountType',
              attributes: ['id', 'name', 'alias'],
            },
            {
              association: 'discountTariffServiceRelations',
              attributes: ['value'],
              include: [
                {
                  association: 'tariff',
                  attributes: ['id', 'name'],
                  include: 'costs',
                },
                {
                  association: 'service',
                  attributes: ['id', 'name'],
                  include: 'costs',
                },
              ],
            },
          ],
        },
      ],
    });

    if (!discountServiceInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(id));

    const {
      expirationAt, services,
      discountTemplate: {
        discountType, discountTariffServiceRelations, ...discountTemplate
      },
    } = discountServiceInstance.toJSON();

    const { discountTariffs, discountServices } = this.getDiscountNomenclature(discountTariffServiceRelations);

    return {
      expirationAt,
      services,
      discountTemplate: {
        ...discountTemplate,
        value: is.discount.type.fixedPrice(discountType.id) ? discountTemplate.value : null,
      },
      discountType,
      discounts: {
        services: discountServices,
        tariffs: discountTariffs,
      },
    };
  }

  static deleteDiscountService(id) {
    return DiscountServiceModel.destroy({
      where: { id },
    });
  }

  static async createDiscountByCard({ value, expirationAt }) {
    const transaction = await db.sequelize.transaction();
    try {
      const lastDiscountByCard = await DiscountByCardModel.findOne({
        order: [['createdAt', 'DESC']],
      }, { transaction });

      if (lastDiscountByCard) {
        const dateStop = moment().add(1, 's');
        await lastDiscountByCard.update({
          expirationAt: dateStop.format(DATE_FORMAT.KEBAB_FULL),
        }, { transaction });
      }

      const newDiscount = await DiscountByCardModel.create({
        value,
        expirationAt,
      }, { transaction });

      await transaction.commit();
      return newDiscount;
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }

  static getCurrentDiscountByCard() {
    return DiscountByCardModel.findOne({
      attributes: ['value', 'expirationAt'],
      order: [['createdAt', 'DESC']],
    });
  }

  static async deleteDiscountByCard() {
    const transaction = await db.sequelize.transaction();

    try {
      const lastDiscountByCard = await DiscountByCardModel.findOne({
        order: [['createdAt', 'DESC']],
      }, { transaction });

      if (lastDiscountByCard) {
        const dateStop = moment().add(1, 's');
        await lastDiscountByCard.update({
          expirationAt: dateStop.format(DATE_FORMAT.KEBAB_FULL),
        }, { transaction });
      }
      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }
}
