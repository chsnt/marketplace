import { Op } from 'sequelize';
import { TariffModel, AvailableCatalogStatusModel, TariffServiceRelationModel } from '../database/models/Tariff';
import { ClientTariffModel } from '../database/models/Client';
import { OrderStatusModel } from '../database/models/Order';
import { tariffServicesFilter } from '../libs/tariffServicesFilter';
import APIError from '../utils/APIError';
import ERRORS from '../constants/errors';
import db from '../database';
import { getAddressAndDevice } from '../utils/prepareResponse';
import AddressModel from '../database/models/AddressModel';

export default class TariffService {
  /**
   * @param {String} [search] Поиск
   * @param {Boolean} [withServices] Если требуется тарифы с включенными в них сервисами
   * @param {Boolean} [isCostRequired] Если требуется тарифы с действующими ценами
   * @param {String} [availableCatalogStatusId] UUID доступа для интерфейса (сайт или админка). Если id не указан выводятся все тарифы
   * @param {String} [deviceTypeId] UUID типа оборудования
   * @return {Promise<*>}
   */
  static async getList({
    search,
    withServices = true,
    isCostRequired = true,
    availableCatalogStatusId,
    deviceTypeId,
  } = {}) {
    const include = [{
      ...TariffModel.includeCost(true),
      required: isCostRequired,
    },
    {
      association: 'deviceType',
      attributes: ['id', 'name', 'alias'],
    }];

    if (withServices) {
      include.push({
        association: 'tariffServiceRelations',
        attributes: ['id'],
        include: [
          {
            required: true,
            association: 'service',
            attributes: ['id', 'name', 'nameInTariff', 'alias'],
          },
        ],
      });
    }

    const result = await TariffModel.findAll({
      attributes: ['id', 'name', 'label', 'icon', 'expirationMonths', 'alias', 'description'],
      where: {
        ...(search && { name: { [Op.iLike]: `%${search}%` } }),
        ...(deviceTypeId && { deviceTypeId }),
        ...(availableCatalogStatusId && {
          availableCatalogStatusId: [
            AvailableCatalogStatusModel.STATUSES.ALL,
            availableCatalogStatusId,
          ],
        }),
      },
      include,
    });

    return result.map((tariffInstance) => {
      const { tariffServiceRelations, ...tariff } = tariffInstance.toJSON();
      const cost = TariffModel.getCurrentCost(tariff.costs);
      return {
        ...tariff,
        cost,
        costMonth: Math.ceil(cost / tariff.expirationMonths),
        costs: undefined,
        services: withServices ? tariffServiceRelations.map(({
          service: { name, nameInTariff, ...service },
        }) => ({ ...service, name: nameInTariff || name })) : undefined,
      };
    });
  }

  /**
   *
   * @param {String} id
   * @param {Boolean} isCostRequired если нужны тарифы с действующей ценой.
   *                  При значении true тарифы, у которых закончился период цены, не будут отображаться.
   *                  При значении false отображается весь список тарифов не зависимо от наличия цены.
   * @return {Promise<{services}>}
   */
  static async getTariff({ id, isCostRequired }) {
    const tariffInstance = await TariffModel.findByPk(id, {
      attributes: ['id', 'name', 'expirationMonths', 'alias', 'description'],
      include: [
        {
          association: 'services',
          attributes: ['id', 'name', 'nameInTariff', 'forTariffOnly', 'alias', 'description'],
          through: { attributes: [] },
        },
        {
          ...TariffModel.includeCost(),
          required: isCostRequired,
        },
      ],
    });
    if (!tariffInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(id));

    const { services, ...tariff } = tariffInstance.toJSON();
    const cost = TariffModel.getCurrentCost(tariff.costs);
    return {
      ...tariff,
      ...(isCostRequired && {
        cost,
        costMonth: Math.ceil(cost / tariff.expirationMonths),
        costs: undefined,
      }),
      services: services.map(({ name, nameInTariff, ...service }) => ({
        ...service,
        name: nameInTariff || name,
      })),
    };
  }

  static async getClientTariffs({ clientId, used = false }) {
    const clientTariffs = await ClientTariffModel.findAll({
      where: {
        clientId,
        expirationAt: {
          [used ? Op.lt : Op.gte]: new Date(),
        },
        '$order.order_status_id$': OrderStatusModel.STATUSES.PAID,
      },
      attributes: ['id', 'tariffId', 'expirationAt'],
      include: [
        {
          association: 'tariff',
          attributes: ['id', 'name', 'alias', 'label', 'icon'],
        },
        {
          association: 'clientTariffServices',
          attributes: ['id', 'value'],
          include: [
            {
              association: 'tariffServiceRelation',
              paranoid: false,
              attributes: ['id', 'limit'],
              include: [
                {
                  association: 'limitMeasure',
                  attributes: ['name'],
                },
                {
                  association: 'service',
                  paranoid: false,
                  attributes: [
                    'id', 'name', 'alias', 'label', 'icon',
                    'serviceAddressAccountingTypeId', 'deviceTypeId',
                  ],
                },
              ],
            },
            {
              association: 'tickets',
              attributes: ['id'],
              include: {
                association: 'ticketStatus',
                attributes: ['id'],
              },
            },
          ],
        },
        {
          association: 'order',
          attributes: [],
        },
        AddressModel.addressAndDevice(),
      ],
    });


    return clientTariffs.map((clientTariff) => {
      const {
        id: clientTariffId, tariffId, expirationAt, tariff, clientTariffServices, serviceAddress,
      } = clientTariff.toJSON();

      const {
        serviceAddress: preparedServiceAddress = null,
        device = null,
      } = getAddressAndDevice(serviceAddress) || {};

      return {
        serviceAddress: preparedServiceAddress,
        device,
        tariff: {
          id: tariffId,
          clientTariffId,
          ...tariff,
          expirationAt,
          services: tariffServicesFilter({ clientTariffServices }),
        },
      };
    });
  }

  static async updateTariff({
    id, availableCatalogStatusId, services = [], ...serviceData
  }) {
    const transaction = await db.sequelize.transaction();
    try {
      const tariffInstance = await TariffModel.findByPk(id, {
        attributes: { exclude: ['tariff_id'] },
        include: [
          {
            association: 'services',
            attributes: { exclude: ['service_id'] },
          },
          'availableCatalogStatus',
          'tariffServiceRelations',
        ],
        transaction,
      });

      if (!tariffInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(id));

      await tariffInstance.update({
        ...serviceData,
        availableCatalogStatusId,
      },
      { transaction });

      const servicesMap = new Set();

      await TariffServiceRelationModel.bulkCreate(services?.map(
        (service) => {
          servicesMap.add(service.serviceId);
          const limit = (service.limit === null) ? Infinity : service.limit;
          return {
            tariffId: tariffInstance.id,
            deletedAt: null,
            ...service,
            limit,
          };
        }
      ), {
        fields: ['tariffId', 'serviceId', 'limit', 'limitMeasureId'],
        updateOnDuplicate: ['limit', 'limitMeasureId', 'deletedAt'],
        transaction,
      });

      const servicesToDelete = tariffInstance.tariffServiceRelations.reduce((acc, current) => {
        if (!servicesMap.has(current.serviceId)) acc.push(current.serviceId);
        return acc;
      }, []);

      await TariffServiceRelationModel.destroy({
        where: { serviceId: servicesToDelete },
        transaction,
      });

      await transaction.commit();
      return null;
    } catch (error) {
      await transaction.rollback();
      throw new APIError(ERRORS.TARIFF.UPDATE_ERROR, { originalError: error });
    }
  }

  /**
   *
   * @param clientId
   * @param services
   * @return {Promise<void>}
   * @description возвращает свободные сервисы для ЗНО
   */
  static async getAvailableService({
    clientId, services,
  }) {
    const clientTariffs = await ClientTariffModel.findAll({
      where: { clientId },
      include: [
        {
          association: 'order',
          attributes: ['orderStatusId'],
          where: { orderStatusId: OrderStatusModel.STATUSES.PAID },
        },
        {
          association: 'clientTariffServices',
          attributes: ['id', 'value'],
          include: [
            {
              association: 'tariffServiceRelation',
              paranoid: false,
              attributes: ['id', 'limit', 'serviceId'],
              where: { serviceId: services },
              include: [
                {
                  association: 'limitMeasure',
                  attributes: ['name'],
                },
                {
                  association: 'service',
                  paranoid: false,
                  attributes: ['id', 'serviceAddressAccountingTypeId'],
                },
              ],
            },
            {
              association: 'tickets',
              attributes: ['id'],
              include: {
                association: 'ticketStatus',
                attributes: ['id'],
              },
            },
          ],
        },
      ],
    });

    return clientTariffs.map((clientTariff) => {
      const { clientTariffServices, expirationAt } = clientTariff.toJSON();
      return tariffServicesFilter({ clientTariffServices, expirationAt });
    }).flat();
  }
}
