/* Сервис для взаимодействия с Active Directory */

import { promisify } from 'util';

import ActiveDirectory from 'activedirectory';

import { ACTIVE_DIRECTORY } from '../constants/env';
import ERRORS from '../constants/errors';

import APIError from '../utils/APIError';
import logger from './LoggerService';

export default class ActiveDirectoryService {
  constructor() {
    const client = new ActiveDirectory(ACTIVE_DIRECTORY);
    /* Промисифицируем методы клиента, чтобы не утонуть в callback hell */
    this.authenticate = promisify(client.authenticate).bind(client);
  }

  async checkAuth(login, password) {
    try {
      await this.authenticate(`${login}@${ACTIVE_DIRECTORY.domain}`, password);
    } catch (error) {
      logger.error({ ...ERRORS.AUTH.USER_NOT_FOUND, data: error });
      throw new APIError(ERRORS.AUTH.USER_NOT_FOUND);
    }
  }
}
