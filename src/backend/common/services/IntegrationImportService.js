import { CityModel } from '../database/models/City';
import {
  BankModel, TaxTypeModel,
} from '../database/models/Client/Organization';
import { DeviceTypeModel, DeviceManufacturerModel, DeviceModelModel } from '../database/models/Device';
import { TariffModel, ServiceModel, TariffServiceRelationModel } from '../database/models/Tariff';
import db from '../database';
import HelperService from './HelperService';
import OrderService from './OrderService';


// TODO: придумать, что делать с импортируемыми данными и алиасами

export default class IntegrationImportService {
  /* Импорт банков */
  static importBanks(banks) {
    return BankModel.bulkUpsertRemoveObsolete(HelperService.idToExternalIdConverter(banks));
  }

  /* Импорт видов налогообложения из CRM */
  static importTaxTypes(taxTypes) {
    return TaxTypeModel.bulkUpsertRemoveObsolete(HelperService.idToExternalIdConverter(taxTypes));
  }

  /* Импорт обслуживаемых городов из CRM */
  static importCities(cities) {
    return CityModel.bulkUpsertRemoveObsolete(HelperService.idToExternalIdConverter(cities));
  }

  /* Импорт номенклатуры из CRM */
  static async importNomenclature({ tariffs = [], services = [], tariffServiceRelations = [] }) {
    const transaction = await db.sequelize.transaction();
    try {
      let tariffInstances = [];
      let serviceInstances = [];
      /* 1. Создаем тарифы */
      if (tariffs.length) {
        tariffInstances = await TariffModel.bulkUpsertRemoveObsolete(
          HelperService.idToExternalIdConverter(tariffs),
          { returnOnlyInstances: true, transaction },
        );
      }


      /* 2. Создаем услуги */
      if (services.length) {
        serviceInstances = await ServiceModel.bulkUpsertRemoveObsolete(
          HelperService.idToExternalIdConverter(services),
          { returnOnlyInstances: true, transaction },
        );
      }


      /* 3. Генерим карты по externalId */
      if (tariffServiceRelations.length) {
        const tariffsMap = HelperService.createMapFromArray(tariffInstances, 'externalId');
        const servicesMap = HelperService.createMapFromArray(serviceInstances, 'externalId');

        /* 4. Создаем записи отношений тарифов и услуг */
        // TODO: Сделать удаление связей, которые более не актуальны
        for (const tariffServiceRelation of tariffServiceRelations) {
          const { tariffId, serviceId, ...rest } = tariffServiceRelation;

          await TariffServiceRelationModel.upsert({
            ...rest,
            tariffId: tariffsMap[tariffId].id,
            serviceId: servicesMap[serviceId].id,
          }, { transaction });
        }
      }


      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }

  static async importDevices({ deviceTypes = [], deviceManufacturers = [] }) {
    const transaction = await db.sequelize.transaction();
    try {
      /* Создаем типы устройств и создаем из них карту */
      const deviceTypeInstances = await DeviceTypeModel.bulkUpsertRemoveObsolete(
        HelperService.idToExternalIdConverter(deviceTypes),
        { returnOnlyInstances: true, transaction },
      );
      const deviceTypesMap = HelperService.createMapFromArray(deviceTypeInstances, 'externalId');

      const deviceModels = [];

      /* Создаем производителей и создаем из них карту */
      const deviceManufacturerInstances = await DeviceManufacturerModel.bulkUpsertRemoveObsolete(
        deviceManufacturers.map(({
          deviceModels: deviceModelsInManufacturer, id: externalId, ...deviceManufacturer
        }) => {
          /* Формируем массив моделей устройств с проставленным внешним id производителя */
          deviceModels.push(...deviceModelsInManufacturer.map((model) => ({
            ...model,
            deviceManufacturerId: externalId,
          })));

          return {
            externalId,
            ...deviceManufacturer,
          };
        }),
        { returnOnlyInstances: true, transaction },
      );
      const deviceManufacturersMap = HelperService.createMapFromArray(deviceManufacturerInstances, 'externalId');

      /* Создаем модели устройств */
      await DeviceModelModel.bulkUpsertRemoveObsolete(
        deviceModels.map(({
          id: externalId, deviceManufacturerId, deviceTypeId, ...deviceModel
        }) => ({
          ...deviceModel,
          externalId,
          deviceManufacturerId: deviceManufacturersMap[deviceManufacturerId].id,
          deviceTypeId: deviceTypesMap[deviceTypeId].id,
        })),
        { transaction },
      );

      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }

  static importDocument(parameters) {
    return OrderService.addDocumentToOrder(parameters);
  }
}
