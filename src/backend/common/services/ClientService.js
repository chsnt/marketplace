import { Op } from 'sequelize';
import {
  ClientDeviceModel, ClientModel,
} from '../database/models/Client';
import { UserModel } from '../database/models/User';
import db from '../database';
import APIError from '../utils/APIError';
import ERRORS from '../constants/errors';
import AuthService from './AuthService';
import { generateNewPassword, getPasswordHash } from '../utils/auth';
import MailClient from '../libs/MailClient';
import LeadService from './LeadService';
import { OrderModel, OrderStatusModel } from '../database/models/Order';
import is from '../libs/is';
import { OrganizationModel } from '../database/models/Client/Organization';
import { ServiceModel, TariffModel } from '../database/models/Tariff';
import NomenclatureModel from '../database/models/Abstract/NomenclatureModel';
import logger from './LoggerService';

const ADDRESS_ATTRIBUTES = ['id', 'city', 'street', 'house', 'apartment', 'postalCode'];

export default class ClientService {
  static getListWithSearch({
    clientId,
    clientName,
    inn,
    client,
  } = {}) {
    const searchAttribute = UserModel.getSearchAttribute({ value: clientName, preKey: 'users' });
    /* Получим клиентов в соответствии с фильтрами */
    return ClientModel.findAll({
      where: {
        ...(clientId && { id: clientId }),
        ...(clientName && {
          [Op.or]: [
            { '$organization.name$': { [Op.iLike]: `%${clientName}%` } },
            { $and: searchAttribute },
          ],
        }),
        ...(inn && { inn }),
      },
      include: [
        {
          association: 'organization',
          ...((client === 'organization') ? { required: true } : {}),
          attributes: ['id', 'name', 'inn'],
        },
        {
          association: 'users',
          ...((client === 'individual') ? { required: true } : {}),
          attributes: ['id', 'firstName', 'patronymicName', 'lastName', 'fullName'],
        },
      ],
    });
  }

  /**
   *
   * @param {Object} user
   * @param {String} user.firstName Имя
   * @param {String} user.lastName Фамилия
   * @param {String} [user.patronymicName] Отчество
   * @param {String} [user.email] Почта
   * @param {String} [user.phone] Телефон
   * @param {String} user.password Пароль
   * @param {Boolean} user.isMaintainer Флаг определяющий владельца организации или главного представителя клиента
   * @param {String} user.sbbidId ID из SBBID
   *
   * @param {Object} organization
   * @param {String} organization.sbbidId ID из SBBID
   * @param {String} organization.name Название организации
   * @param {String} organization.inn ИНН
   * @param {String} organization.kpp КПП
   * @param {String} organization.electronicDocumentCirculationId Идентификатор оператора ЭДО
   * @param {String} [organization.electronicDocumentCirculationDescription] Если выбрано "Моего оператора нет в списке", нужно указать его тут
   * @param {Boolean} organization.isSameAddress Флаг одинаковых юридического и фактического адресов
   * @param {Object} organization.legalAddress Юридический адрес (описание объекта находится в api)
   * @param {Object} [organization.realAddress] Юридический адрес (описание объекта находится в api)
   *
   * @param {Object} [organization.bankDetail] Юридический адрес (описание объекта находится в api)
   * @param {String} organization.bankDetail.name Название банка
   * @param {String} organization.bankDetail.bankAccount Расчетный счет
   * @param {String} organization.bankDetail.correspondentAccount Кор. счет
   * @param {String} organization.bankDetail.rcbic БИК
   * @param {String} organization.bankDetail.taxTypeId Идентификатор системы налогообложения
   *
   * @param {String} [leadId] UUID лида
   *
   * @return {Promise<*>}
   */
  static async registerClient({
    user, organization, leadId,
  }) {
    /* Если нет данных пользователя - запрещаем регистрацию */
    if (!user) {
      throw new APIError(ERRORS.CLIENT.NOT_FULL_DATA);
    }

    let clientInstance;
    let clientUser;
    let clientOrganization;
    const password = generateNewPassword();

    const transaction = await db.sequelize.transaction();

    try {
      /* Создаем клиента */
      clientInstance = await ClientModel.create({}, { transaction });
      /* Если есть данные организации и пользователя, то создаем ЮЛ / ИП */
      if (organization) {
        clientOrganization = await OrganizationModel.registerOrganization({
          organization: { ...organization, clientId: clientInstance.id },
          transaction,
        });
      }

      /* Если была создана организация - привязываем пользователя к ней */
      clientUser = await UserModel.registerUser({
        user: {
          ...user,
          password,
          isMaintainer: true,
          ...(clientOrganization ? { organizationId: clientOrganization.id } : { clientId: clientInstance.id }),
        },
        transaction,
      });

      /* "Закрываем" лид, если указан */
      if (leadId) {
        await LeadService.closeLeadAfterRegistration({ leadId, clientId: clientInstance.id, transaction });
      }

      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      if (!(error instanceof APIError)) throw new APIError(ERRORS.CLIENT.REGISTRATION_ERROR, { originalError: error });
      throw error;
    }

    /* Устанавливаем временные пароли и т.п. вне транзакции, т.к. процесс отправки не должен влиять на основную регистрацию */

    /* Установим текущий пароль как временный */
    await AuthService.setTemporaryPassword(clientUser.id, getPasswordHash(password));

    try {
      /* Отправляем на почту уведомление о регистрации */
      await MailClient.registration({
        name: 'templePassword',
        recipients: [{
          name: clientUser.fullName,
          email: clientUser.email,
        }],
        variables: {
          email: clientUser.email,
          password,
        },
      });

      /* Отправляем на почту ссылку на подтверждение email */
      await AuthService.sendEmailConfirmationUrl({ email: clientUser.email });
    } catch (error) {
      logger.error(error);
    }

    /* Отправляем пароль по смс */
    await AuthService.sendSMSWithPassword({ phone: clientUser.phone, password });

    return { client: clientInstance, clientUser };
  }

  static async getServiceAddressesByDeviceType(clientId) {
    const clientDeviceInstances = await ClientDeviceModel.findAll({
      where: { clientId },
      attributes: ['id'],
      include: [
        {
          association: 'deviceModel',
          attributes: ['id', 'name'],
          include: [
            {
              association: 'deviceManufacturer',
              attributes: ['id', 'name'],
            },
            {
              association: 'deviceType',
              attributes: ['id', 'name', 'alias'],
            },
          ],
        },
        {
          association: 'serviceAddress',
          attributes: ADDRESS_ATTRIBUTES,
          include: [{
            association: 'federationSubject',
            attributes: ['id', 'name'],
          }, {
            association: 'cityRelationship',
            attributes: ['id', 'name'],
          }],
        },
        {
          association: 'clientDeviceParameters',
          attributes: ['id', 'value'],
          include: [{ association: 'deviceParameter', attributes: ['name', 'alias'] }],
        },
      ],
    });

    const serviceAddressesByDeviceType = clientDeviceInstances.reduce((acc, clientDeviceInstance) => {
      const {
        deviceModel: { deviceType, deviceManufacturer, ...deviceModel },
        serviceAddress,
        ...clientDevice
      } = clientDeviceInstance.toJSON();

      const deviceTypeId = deviceType.id;

      if (!acc[deviceTypeId]) {
        acc[deviceTypeId] = { ...deviceType, clientDevices: [] };
      }

      const item = {
        ...clientDevice,
        serviceAddress: {
          ...serviceAddress,
          region: serviceAddress.federationSubject,
          federationSubject: undefined,
          city: serviceAddress.cityRelationship,
          cityRelationship: undefined,
        },
        clientDeviceParameters: undefined,
        manufacturer: deviceManufacturer?.name,
        model: deviceModel.name,
      };

      if (is.device.type.arm(deviceTypeId)) {
        Object.assign(item, clientDeviceInstance.armParameters);
      } else {
        item.serialNumber = clientDeviceInstance.serialNumber;
      }

      acc[deviceTypeId].clientDevices.push(item);
      return acc;
    }, {});

    return Object.values(serviceAddressesByDeviceType);
  }

  static async getClientOrders(clientId) {
    const orderInstances = await OrderModel.findAll({
      where: { clientId, orderStatusId: { [Op.ne]: OrderStatusModel.STATUSES.CART } },
      attributes: ['id', 'number', 'orderDate', 'description'],
      include: [
        {
          association: 'orderStatus',
          attributes: ['id', 'name'],
        },
        {
          association: 'clientTariffs',
          attributes: ['id'],
          include: [
            {
              association: 'tariff',
              attributes: ['id', 'name'],
              include: [{
                ...TariffModel.includeCost(true),
                separate: true,
              }],
            },
          ],
        },
        {
          association: 'clientServices',
          attributes: ['id'],
          include: [
            {
              association: 'service',
              attributes: ['id', 'name'],
              include: [{
                ...ServiceModel.includeCost(true),
                separate: true,
              }],
            },
          ],
        },
        {
          association: 'orderDocuments',
          attributes: ['id', 'name'],
        },
        {
          association: 'orderOfferDocument',
          attributes: ['id', 'name'],
        },
      ],
    });

    const reduceOrderItems = ({ rows, itemKey }) => {
      let totalCost = 0;
      const itemsMap = rows.reduce((acc, { [itemKey]: { id: itemId, name, costs } }) => {
        const cost = NomenclatureModel.getCurrentCost(costs);
        if (!acc[itemId]) {
          acc[itemId] = {
            id: itemId, name, cost, count: 1,
          };
        } else {
          acc[itemId].count += 1;
        }

        totalCost += cost;

        return acc;
      }, {});
      return { totalCost, rows: Object.values(itemsMap) };
    };


    return orderInstances.map((orderInstance) => {
      const {
        orderStatus, clientTariffs, clientServices, orderDocuments, orderOfferDocument, ...order
      } = orderInstance.toJSON();

      const { rows: tariffs, totalCost: tariffsCost } = reduceOrderItems({ rows: clientTariffs, itemKey: 'tariff' });
      const { rows: services, totalCost: servicesCost } = reduceOrderItems({
        rows: clientServices, itemKey: 'service',
      });

      return {
        ...order,
        orderStatus: orderStatus.name,
        items: { tariffs, services },
        documents: [
          ...(orderOfferDocument ? [orderOfferDocument] : []),
          ...orderDocuments,
        ],
        totalCost: tariffsCost + servicesCost,
      };
    });
  }

  static getClientId(user) {
    return user.clientId || user.organization.clientId;
  }

  /**
   *
   * @param {String} clientId UUID клиента
   * @return {String} тип клиента individual или organization
   */
  static async getClientType(clientId) {
    const clientInstance = await ClientModel.findByPk(clientId, {
      include: ['organization'],
    });

    return (clientInstance.organization) ? 'organization' : 'individual';
  }
}
