import RabbitClient from '../libs/RabbitClient';
import { IS_SMS_SENDING_ENABLED } from '../constants/env';
import { Logger } from './LoggerService';

const SMSLogger = new Logger('SMS_SENDER');

export default class {
  /* Сервис, отвечающий за отправку СМС, почты и тд */

  /**
   * @async
   * @param {Object} message
   * @param {String} message.phone Номер телефона.
   * @param {String} message.text Текст сообщения.
   * @returns {Promise<void>}
   */
  static async sendSMS(message) {
    if (!IS_SMS_SENDING_ENABLED) {
      return SMSLogger
        .warn(`Отправка СМС отключена. Проверьте переменные окружения сервера. Сообщение: ${JSON.stringify(message)}`);
    }
    const client = new RabbitClient();
    await client.sendToQueue({ queue: client.queues.sms, message: { sms: message } });
  }
}
