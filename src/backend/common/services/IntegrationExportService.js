import { OrganizationModel, TaxTypeModel } from '../database/models/Client/Organization';
import { OrderDocumentModel, OrderModel } from '../database/models/Order';
import { ElectronicDocumentCirculationModel } from '../database/models/Edo';
import { DeviceTypeModel } from '../database/models/Device';
import APIError from '../utils/APIError';
import COMMON_ERRORS from '../constants/errors';
import { Logger } from './LoggerService';
import { userToString, addressToString } from '../utils/prepareResponse';
import { INTEGRATOR } from '../constants/env';
import IntegratorClient from '../libs/IntegratorClient';

const logger = new Logger('INTEGRATION SERVICE');

const integratorClient = new IntegratorClient({
  baseURL: INTEGRATOR.BASE_URL,
  username: INTEGRATOR.USERNAME,
  password: INTEGRATOR.PASSWORD,
});

const OWNERSHIP_TYPES_BY_INN_LENGTH = {
  10: 1, // ЮЛ
  12: 2, // ИП
};

const TaxTypeStatus = new Map([[TaxTypeModel.TYPES.OSNO, 'Общая'], [TaxTypeModel.TYPES.USN, 'Упрощенная']]);
const DeviceTypes = new Map([
  [DeviceTypeModel.TYPES.KKT, '000000043'], // код в 1С для ККТ
  [DeviceTypeModel.TYPES.ARM, '000000037'], // код в 1С для АРМ
]);

export default class IntegrationExportService {
  /**
   *
   * @param {String} userId
   * @param {String} organizationId
   * @returns {Promise<void>}
   * @description Метод экспорта контрагента в 1С. Экспортируется при создании первого заказа.
   */
  static async createContragent({ userId, organizationId }) {
    /* Получаем записи с нуля, чтоб сделать метод более универсальным в применении */
    const ADDRESS_ATTRIBUTES = ['federationSubjectId', 'cityId', 'city', 'street', 'house', 'apartment', 'postalCode'];
    const ORGANIZATION_ATTRIBUTES = ['id', 'name', 'inn', 'kpp'];
    const USER_ATTRIBUTES = ['firstName', 'patronymicName', 'lastName', 'phone', 'email'];

    const organizationInstance = await OrganizationModel.findByPk(organizationId, {
      attributes: ORGANIZATION_ATTRIBUTES,
      include: [{
        association: 'realAddress',
        attributes: ADDRESS_ATTRIBUTES,
        include: [{
          association: 'federationSubject',
          attributes: ['name'],
        }, {
          association: 'cityRelationship',
          attributes: ['name'],
        }],
      },
      {
        association: 'legalAddress',
        attributes: ADDRESS_ATTRIBUTES,
        include: [{
          association: 'federationSubject',
          attributes: ['name'],
        }, {
          association: 'cityRelationship',
          attributes: ['name'],
        }],
      },
      {
        association: 'users',
        attributes: USER_ATTRIBUTES,
        where: { id: userId },
      },
      {
        association: 'bankDetail',
        attributes: ['id'],
        include: [{
          association: 'taxType',
          attributes: ['id'],
        }],
      },
      {
        association: 'electronicDocumentCirculation',
        attributes: ['id'],
      },
      ],
    });

    const {
      users: [user], bankDetail, realAddress, legalAddress, electronicDocumentCirculation, ...organization
    } = organizationInstance.toJSON();

    const data = {
      ...organization,
      LegalStatus: OWNERSHIP_TYPES_BY_INN_LENGTH[organization.inn.length],

      PhisicalAdress: addressToString({
        ...realAddress,
        subject: realAddress.federationSubject.name,
        city: realAddress.cityRelationship?.name || realAddress.city,
      }),
      LegalAdress: addressToString({
        ...legalAddress,
        subject: legalAddress.federationSubject.name,
        city: legalAddress.cityRelationship?.name || legalAddress.city,
      }),
      Email: user.email,
      Phone: user.phone,
      ContactPerson: userToString(user),
      TaxeAdvantage: TaxTypeStatus.get(bankDetail.taxType.id),
      EDW: ElectronicDocumentCirculationModel.checkEDOExists(electronicDocumentCirculation.id) ? 1 : 0,
      Fax: '',
    };

    await integratorClient.send({ resource: INTEGRATOR.RESOURCES.CREATE_PARTNER, data });
  }

  /**
   *
   * @param {Object} organization
   * @param {String} organization.id UUID организации
   * @param {Object} organization.bank Банковские данные
   * @param {String} organization.bank.bankAccount Номер банковского счета
   * @param {String} organization.bank.rcbic Банковский идентификационный код (бик)
   * @returns {Promise<void>}
   */
  static async createOrUpdateBankAccount(organization) {
    const data = {
      PartnerID: organization.id,
      AccountNumber: organization.bank.bankAccount,
      BIC: organization.bank.rcbic,
      BICBankForSettlements: '',
    };

    await integratorClient.send({ resource: INTEGRATOR.RESOURCES.CREATE_BANK_ACCOUNT, data });
  }

  /**
   *
   * @param {String} organizationId UUID организации
   * @param {String} bankAccount Номер банковского счета
   * @param {Object} management
   * @param {String} management.modelId UUID модели оборудования
   * @param {Object} management.managementService Сервисное управление
   * @param {String} management.managementService.id UUID сервисного управления
   * @param {String} management.managementService.name Название сервисного управления
   * @param {String} management.managementService.externalId ID интегратора
   * @param {String} management.managementService.code Код из 1С
   * @param {Object} offer Договор оферты
   * @param {String} offer.id UUID договора оферты
   * @param {Object.<Date>} offer.createdAt Дата создания документа
   * @param {Object.<Date>} createdAt Дата создания заказа
   * @param {Number} offer.count Порядковый номер
   * @returns {Promise<void>}
   */
  static async createContract({
    organizationId,
    bankAccount,
    management,
    offer,
    createdAt,
  }) {
    const data = {
      PartnerID: organizationId,
      ContractID: offer.id,
      ContractNumber: offer.count,
      ContractDate: createdAt,
      FinishDate: '',
      AccountNumber: bankAccount,
      ServiceID: DeviceTypes.get(management.deviceTypeId),
      TerritoryID: management.TerBankID,
    };
    await integratorClient.send({ resource: INTEGRATOR.RESOURCES.CREATE_CONTRACT, data });
  }

  /**
   *
   * @param {Number} orderNumber Порядковый номер заказа
   * @param {String} organizationId UUID организации
   * @param {Object} offer Договор оферты
   * @param {String} offer.id UUID договора оферты
   * @param {Object.<Date>} offer.createdAt Дата создания документа
   * @param {Number} offer.count Порядковый номер
   * @param {String.<'YYYY.MM.DD.HH.mm.ss'>} createdAt Дата создания заказа (запись из бд, требование интегратора)
   * @param {Array.<Object>} goods список заказов
   * @param {String} goods.goodsID ID тарифа или услуги в системе интегратора (externalId)
   * @param {Number} goods.quantity Кол-во
   * @param {Number} goods.quantity Общая стоимость
   * @param {String} goods.managementId Код сервисного управления к которому привязан город обслуживания оборудования в системе интегратора (externalId)
   * @returns {Promise<Array>} список имен документов
   */
  static async createOrder({
    orderNumber, organizationId, offer, createdAt, goods,
  }) {
    const data = {
      ID: orderNumber,
      PartnerID: organizationId,
      ContractID: offer.id,
      Number: '',
      Date: createdAt,
      PaidByCard: false,
      Goods: goods,
    };

    const result = await integratorClient.send({ resource: INTEGRATOR.RESOURCES.CREATE_ORDER, data });
    return result;
  }

  /* Метод экспорта договора */
  static async exportContract(contractId) {
    const contractInstance = await OrderDocumentModel.findByPk(contractId, {
      attributes: ['externalId', 'name', 'date'],
    });


    const result = contractInstance.toJSON();

    logger.verbose(result);
    // TODO: Тут нужно сделать отправку данных в CRM, не делаем потому что пока не знаем куда отправлять
    // TODO: и в идеале, если принимающая сторона в качестве ответа будет отдавать id записи, чтоб мы могли проставить externalId
  }

  /* Метод экспорта счета об оплате */
  static async exportInvoice(invoiceId) {
    const invoiceInstance = await OrderDocumentModel.findByPk(invoiceId, {
      attributes: ['id'],
      include: [{
        association: 'order',
        attributes: ['id'],
        include: [{
          association: 'client',
          attributes: ['id'],
          include: [{
            association: 'organization',
            attributes: ['externalId'],
            include: [{
              association: 'bankDetail',
              attributes: ['bankAccount'],
              include: [{
                association: 'bank',
                attributes: ['rcbic'],
              }],
            }],
          }],
        }],
      }],
    });

    const {
      order: { client: { organization: { externalId: contragentId, bankDetail } } },
    } = invoiceInstance.toJSON();

    const rcbic = bankDetail?.bank.rcbic || null;

    const result = {
      contragentId,
      bankAccount: bankDetail?.bankAccount || null,
      rcbic,
      rcbicBank: rcbic,
    };

    logger.verbose(result);
    // TODO: Тут нужно сделать отправку данных в CRM, не делаем потому что пока не знаем куда отправлять
    // TODO: и в идеале, если принимающая сторона в качестве ответа будет отдавать id записи, чтоб мы могли проставить externalId
  }

  /* Метод экспорта заказа */
  static async exportOrder(orderId) {
    const orderInstance = await OrderModel.findByPk(orderId, {
      attributes: ['id', 'number'],
      include: [
        {
          association: 'clientTariffs',
          attributes: ['tariffId'],
          include: [{
            association: 'tariff',
            attributes: ['cost'],
          }],
        },
        {
          association: 'clientServices',
          attributes: ['serviceId'],
          include: [{
            association: 'service',
            attributes: ['cost'],
          }],
        },
      ],
    });

    if (!orderInstance) throw new APIError(COMMON_ERRORS.COMMON.OBJECT_NOT_FOUND(orderId));

    const { clientTariffs, clientServices, ...order } = orderInstance.toJSON();
    let cost = 0;

    const result = {
      ...order,
      services: clientServices.map(({ serviceId, service: { cost: serviceCost } }) => {
        cost += serviceCost;
        return serviceId;
      }),
      tariffs: clientTariffs.map(({ tariffId, tariff: { cost: tariffCost } }) => {
        cost += tariffCost;
        return tariffId;
      }),
      cost,
    };

    logger.verbose(result);
    // TODO: Тут нужно сделать отправку данных в CRM, не делаем потому что пока не знаем куда отправлять
    // TODO: и в идеале, если принимающая сторона в качестве ответа будет отдавать id записи, чтоб мы могли проставить externalId
  }

  static checkChangedBankDetails({ oldBankDetails, newBankDetails }) {
    const dataS = new Set(['bankAccount', 'rcbic']);

    for (const datum of dataS) {
      if (oldBankDetails[datum] !== newBankDetails[datum]) {
        return true;
      }
    }
    return false;
  }
}
