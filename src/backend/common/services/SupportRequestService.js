import path from 'path';

import {
  SupportRequestModel,
  SupportRequestTopicModel,
  SupportRequestStatusModel,
  SupportRequestAttachmentModel,
} from '../database/models/SupportRequest';
import FileService from './FileService';
import db from '../database';
import DocumentService from './DocumentService';

export default class SupportRequestService {
  /**
   * createRequest
   * @param {Object} data
   * @param {String} data.clientId Id клиента
   * @param {String} data.topic Id темы обращения
   * @param {String} data.description Описание темы обращения
   * @param {Object[]} [data.docNames] Список названий файлов
   * @param {String} data.docNames.fsName Имя файла на диске
   * @param {String} data.docNames.name Оригинальное имя файла
   * @param {String} data.firstName Имя представителя клиента
   * @param {String} data.patronymicName Отчество представителя клиента
   * @param {String} data.lastName Фамилия представителя клиента
   * @param {String} data.phone Номер телефона представителя
   * @param {String} data.email Email представителя
   * @returns {Promise<void>}
   */
  static async createRequest({
    description: text,
    topic: supportRequestTopicId,
    docNames: supportRequestAttachments,
    ...supportRequestData
  }) {
    const transaction = await db.sequelize.transaction();
    try {
      const supportRequestInstance = await SupportRequestModel.create({
        ...supportRequestData,
        text,
        supportRequestTopicId,
        supportRequestStatusId: SupportRequestStatusModel.STATUSES.NEW,
      }, { transaction });

      const supportRequestTopic = await SupportRequestTopicModel.findByPk(
        supportRequestTopicId,
        { attributes: ['name'] },
      );

      if (supportRequestAttachments && supportRequestAttachments.length > 0) {
        await this.addAttachmentsToRequest({
          supportRequestAttachments,
          supportRequestId: supportRequestInstance.id,
          transaction,
        });
      }

      const supportRequest = supportRequestInstance.toJSON();

      await transaction.commit();

      return {
        ...supportRequest,
        topicName: supportRequestTopic?.name,
      };
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  }

  static async addAttachmentsToRequest({ supportRequestId, supportRequestAttachments, transaction }) {
    const localTransaction = transaction || await db.sequelize.transaction();

    try {
      const attachments = await SupportRequestAttachmentModel.bulkCreate(
        supportRequestAttachments.map(({ name }) => ({
          supportRequestId, name,
        })),
        { transaction: localTransaction },
      );

      attachments.forEach((fileAttachment) => {
        FileService.renameFile({
          dir: path.join(FileService.DEFAULT_FILE_PATH, 'support'),
          oldName: supportRequestAttachments.find((doc) => doc.name === fileAttachment.name).fsName,
          newName: fileAttachment.id,
        });
      });

      if (!transaction) await localTransaction.commit();
    } catch (err) {
      if (!transaction) await localTransaction.rollback();
      throw err;
    }
  }

  static getTopics() {
    return SupportRequestTopicModel.findAll({
      attributes: ['id', 'name', 'alias'],
    });
  }

  static getDocument(id) {
    return DocumentService.getDocument({ model: SupportRequestAttachmentModel, id, pathToDir: 'support' });
  }
}
