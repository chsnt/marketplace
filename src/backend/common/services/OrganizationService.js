import axios from 'axios';
import { UniqueConstraintError } from 'sequelize';
import { GET_ORGS_URL, GET_ORGS_USERNAME, GET_ORGS_PASSWORD } from '../../client/constants/env';
import logger from './LoggerService';
import { OrganizationModel } from '../database/models/Client/Organization';
import { UserModel } from '../database/models/User';
import APIError from '../utils/APIError';
import ERRORS from '../constants/errors';
import { organization as prepareOrganization } from '../utils/prepareResponse';
import db from '../database';
import is from '../libs/is';

const ADDRESS_ATTRIBUTES = ['city', 'street', 'house', 'apartment', 'postalCode'];
const USER_ATTRIBUTES = ['id', 'firstName', 'patronymicName', 'lastName', 'email', 'phone'];
const META_ALIAS_ATTRIBUTES = ['id', 'name', 'alias'];
const META_ATTRIBUTES = ['id', 'name'];
const CITY_RELATIONSHIP = 'cityRelationship';
const FEDERATION_SUBJECT = 'federationSubject';

const EXCLUDE_ATTRIBUTES = [
  'createdAt', 'updatedAt', 'deletedAt',
  'createdBy', 'updatedBy', 'created_by', 'updated_by',
];

export default class OrganizationService {
  static async getInfoFromCloud(query) {
    let result;
    try {
      const { data } = await axios.get(`${GET_ORGS_URL}?${query}`, {
        data: { username: GET_ORGS_USERNAME, password: GET_ORGS_PASSWORD },
      });
      result = data;
    } catch (error) {
      /* Этот кривейщий эндпоинт отдает 404, если не найден такой ИНН или ОГРН ((((( */
      if (error.response?.status !== 404) logger.error(error);
    }

    return result || [];
  }

  /**
   * @param {String} id UUID организации
   * @description запрос c ассоциациями
   * @returns {Promise}
   */
  static async getInfo(where = {}) {
    const instance = await OrganizationModel.findOne({
      where,
      attributes: ['id', 'name', 'inn', 'kpp', 'digitalSignatureFrom', 'digitalSignatureTo'],
      include: [
        {
          association: 'legalAddress',
          attributes: ADDRESS_ATTRIBUTES,
          include: [
            {
              association: CITY_RELATIONSHIP,
              attributes: META_ATTRIBUTES,
            },
            {
              association: FEDERATION_SUBJECT,
              attributes: META_ATTRIBUTES,
            },
          ],
        },
        {
          association: 'realAddress',
          attributes: ADDRESS_ATTRIBUTES,
          include: [
            {
              association: CITY_RELATIONSHIP,
              attributes: META_ATTRIBUTES,
            },
            {
              association: FEDERATION_SUBJECT,
              attributes: META_ATTRIBUTES,
            },
          ],
        },
        {
          association: 'bankDetail',
          attributes: ['id', 'bankAccount'],
          include: [
            {
              association: 'bank',
              attributes: ['id', 'name', 'rcbic', 'correspondentAccount'],
            },
            {
              association: 'taxType',
              attributes: META_ALIAS_ATTRIBUTES,
            },
          ],
        },
        {
          association: 'electronicDocumentCirculation',
          attributes: META_ALIAS_ATTRIBUTES,
        },
        {
          association: 'electronicDocumentCirculationDescription',
          attributes: ['id', 'description'],
        },
        {
          association: 'users',
          attributes: USER_ATTRIBUTES,
        },
      ],
    });

    if (!instance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(JSON.stringify(where)));

    const { users, ...organization } = instance.toJSON();

    return {
      users,
      userId: users.id,
      ...prepareOrganization(organization),
    };
  }

  /**
   * @param {String} id UUID организации
   * @param {Array} attributes атрибуты, которые нужно получить
   * @description запрос без ассоциаций
   * @returns {Promise}
   */
  static getShortInfo(id, attributes = []) {
    return OrganizationModel.findByPk(id, {
      attributes,
    });
  }

  /**
   * @typedef {Object} bankInfo
   * @property {String} name Название банка
   * @property {String} correspondentAccount Корреспондентский счёт банка
   * @property {String} rcbic Бик банка
   * @property {String} bankAccount Расчетный счет организации
   */

  /**
   * @param {string} [organizationId] UUID организации
   * @param {string} [userId] UUID пользователя
   * @returns {Promise<{bankInfo}>}
   */
  static async getBankInfo({ organizationId, userId }) {
    let organizationInstance = null;

    if (organizationId) {
      organizationInstance = await OrganizationModel.findByPk(organizationId, {
        include: {
          association: 'bankDetail',
          attributes: ['bankAccount'],
          include: {
            association: 'bank',
            attributes: ['name', 'rcbic', 'correspondentAccount'],
          },
        },
      });
    } else {
      const userInstance = await UserModel.findByPk(userId, {
        include: {
          association: 'organization',
          include: {
            association: 'bankDetail',
            attributes: ['bankAccount'],
            include: {
              association: 'bank',
              attributes: ['name', 'rcbic', 'correspondentAccount'],
            },
          },
        },
      });

      organizationInstance = userInstance.organization;
    }

    const organization = organizationInstance.toJSON();
    return {
      bankAccount: organization.bankDetail.bankAccount,
      ...organization.bankDetail.bank,
    };
  }

  static async checkUserMaintainer({ userId }) {
    const organizationInstance = await OrganizationModel.findOne({
      attributes: ['id'],
      include: {
        required: true,
        association: 'users',
        attributes: ['id'],
        where: {
          id: userId,
        },
      },
    });
    return !!organizationInstance;
  }

  static async updateInfo({
    id, users, maintainer,
    legalAddress, realAddress, bankDetail, digitalSignature,
    electronicDocumentCirculationId: edoId, electronicDocumentCirculationDescription: edoDescription,
    ...organizationData
  } = {}) {
    const ATR_ORGANIZATION_UPD = ['id', 'city', 'street', 'house', 'apartment', 'postalCode', 'fiasId'];

    const transaction = await db.sequelize.transaction();

    try {
      const organizationInstance = await OrganizationModel.findByPk(id, {
        attributes: { exclude: ['realAddressId', 'legalAddressId', 'organizationId', ...EXCLUDE_ATTRIBUTES] },
        include: [
          {
            association: 'legalAddress',
            attributes: ATR_ORGANIZATION_UPD,
            include: [
              CITY_RELATIONSHIP,
              FEDERATION_SUBJECT,
            ],
          }, {
            association: 'realAddress',
            attributes: ATR_ORGANIZATION_UPD,
            include: [
              CITY_RELATIONSHIP,
              FEDERATION_SUBJECT,
            ],
          },
          {
            association: 'electronicDocumentCirculation',
            attributes: {
              exclude: EXCLUDE_ATTRIBUTES,
            },
          },
          {
            association: 'electronicDocumentCirculationDescription',
            attributes: { exclude: EXCLUDE_ATTRIBUTES },
          },
          {
            association: 'bankDetail',
            attributes: ['id', 'bankAccount'],
            include: [
              {
                association: 'taxType',
                attributes: {
                  exclude: EXCLUDE_ATTRIBUTES,
                },
              },
              {
                association: 'bank',
                attributes: {
                  exclude: EXCLUDE_ATTRIBUTES,
                },
              },
            ],
          },
        ],
      });

      await organizationInstance.update({
        ...organizationData,
        electronicDocumentCirculationId: edoId,
        digitalSignatureFrom: digitalSignature?.from,
        digitalSignatureTo: digitalSignature?.to,
      }, { transaction });

      if (legalAddress) {
        await organizationInstance.legalAddress.update({
          ...legalAddress,
          ...(legalAddress.city && { cityId: null }),
        }, { transaction });
      }

      if (realAddress) {
        await organizationInstance.realAddress.update({
          ...realAddress,
          ...(realAddress.city && { cityId: null }),
        }, { transaction });
      }

      if (edoId) {
        if (edoDescription && is.electronicDocumentCirculation.notInList(edoId)) {
          await organizationInstance.createElectronicDocumentCirculationDescription({
            electronicDocumentCirculationId: edoId,
            description: edoDescription,
          }, { transaction });
        } else {
          await organizationInstance.electronicDocumentCirculationDescription?.destroy({ transaction });
        }
      }

      if (bankDetail) {
        await organizationInstance.bankDetail.update({
          ...(bankDetail.taxType && { taxTypeId: bankDetail.taxType }),
          ...(bankDetail.bankAccount && { bankAccount: bankDetail.bankAccount }),
        }, { transaction });

        await organizationInstance.bankDetail.bank?.update({
          name: bankDetail.name,
          correspondentAccount: bankDetail.correspondentAccount,
          rcbic: bankDetail.rcbic,
        }, { transaction });
      }

      const result = prepareOrganization(organizationInstance.toJSON());

      await transaction.commit();

      return result;
    } catch (error) {
      await transaction.rollback();
      if (error instanceof UniqueConstraintError) {
        const errorObject = UserModel.uniqueConstraintError(error?.original?.constraint);
        throw new APIError(errorObject, { originalError: error });
      }
      if (error instanceof APIError) {
        throw error;
      }
      throw new APIError(ERRORS.USER.UPDATE_USER, { originalError: error });
    }
  }
}
