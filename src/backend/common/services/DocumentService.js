import path from 'path';
import COMMON_ERRORS from '../constants/errors';
import APIError from '../utils/APIError';
import FileService from './FileService';

/**
  * @typedef {Object} fileData
  * @property {String} pathToFile Путь до файла
  * @property {String} filename Имя файла
  */

/**
   * getDocument
   * @static
   * @async
   * @param {<DBModel>} model Модель в которой нужно найти документ
   * @param {String} id ID документа
   * @param {Array} [attribute='name'] Атрибут модели с именем файла
   * @param {Boolean} [shouldThrowError=true] если нужно выдавать ошибку при отсутствии файла
   * @returns {Object} fileData
   */

export default class DocumentService {
  static async getDocument({
    model, id, pathToDir = '', attribute = 'name', shouldThrowError = true,
  }) {
    const document = await model.findByPk(id, {
      attributes: [attribute],
    });

    if (!document) {
      if (shouldThrowError) throw new APIError(COMMON_ERRORS.ENTITY.OBJECT_NOT_FOUND(id));
      return null;
    }

    const { name, extension } = FileService.getFilenameWithExtension(document[attribute]);

    // eslint-disable-next-line max-len
    const pathToFile = `${path.join(FileService.DEFAULT_FILE_PATH, pathToDir)}/${id}${extension ? `.${extension}` : ''}`;

    if (!FileService.pathExists(pathToFile)) throw new APIError(COMMON_ERRORS.FILE.FILE_NOT_FOUND(id));

    return {
      pathToFile,
      filename: `${name}.${extension}`,
    };
  }
}
