import path from 'path';
import { Op } from 'sequelize';
import db from '../database';
import {
  TicketCloseDocumentModel,
  TicketDocumentModel,
  TicketModel,
  TicketClientServiceRelationModel,
  TicketClientTariffServiceRelationModel,
  TicketReviewDocumentModel,
  TicketStatusModel,
} from '../database/models/Ticket';
import APIError from '../utils/APIError';
import DocumentService from './DocumentService';
import FileService from './FileService';
import HelperService from './HelperService';
import ServiceService from './ServiceService';
import is from '../libs/is';
import ERRORS from '../constants/errors';
import { DeviceTypeModel } from '../database/models/Device';

export default class TicketService {
  static TICKET_NAMES = {
    MAIN: '',
    KKT: 'KKT',
    ARM: 'АРМ',
  };

  static checkServicesUnique(services) {
    if (new Set(services).size < services.length) throw new APIError(ERRORS.TICKET.SERVICES_SHOULD_BE_UNIQUE);
  }

  static async getServicesForCreating({
    clientId,
    services,
    transaction,
  }) {
    if (services.length === 0) return [];

    /* Получим все купленные услуги и услуги в рамках тарифа для клиента */
    const [clientTariffServices, clientServices] = await ServiceService.getAllPurchasedServices({
      clientId,
      serviceAttributes: ['id', 'name', 'serviceAddressAccountingTypeId'],
      transaction,
    });

    /* Создаем карту услуг */
    const clientServicesMap = HelperService.createMapFromArray(clientServices, 'serviceId');
    /* Создаем руками карту услуг в тарифе */
    const clientTariffServicesMap = clientTariffServices.reduce((acc, clientTariffService) => ({
      ...acc,
      [clientTariffService.tariffServiceRelation.serviceId]: clientTariffService,
    }), {});

    /* Для каждой услуги проверяем относится она к тарифу или к разовым услугам */
    return services.reduce((acc, serviceId) => {
      /* Сначала проверяем услугу в тарифе (в приоритете списать услугу с тарифа) */
      if (clientTariffServicesMap[serviceId]) {
        acc.tariffServiceRelationsMap[clientTariffServicesMap[serviceId].id] = clientTariffServicesMap[serviceId];
        return acc;
      }
      /* Если нет услуги в тарифе - ищем в разовых услугах */
      if (clientServicesMap[serviceId]) {
        acc.oneTimeServicesMap[clientServicesMap[serviceId].id] = clientServicesMap[serviceId];
        return acc;
      }
      /* Если нигде не нашли - говорим что услуга не была куплена или была использована */
      throw new APIError(ERRORS.TICKET.SERVICE_NOT_PURCHASED);
    }, { tariffServiceRelationsMap: {}, oneTimeServicesMap: {} });
  }

  static async createTicketServiceRelations({
    oneTimeServices,
    ticketId,
    tariffServiceRelations,
    transaction,
  }) {
    await TicketClientServiceRelationModel.bulkCreate(
      oneTimeServices.map((clientServiceId) => ({
        clientServiceId,
        ticketId,
      })),
      { transaction },
    );
    await TicketClientTariffServiceRelationModel.bulkCreate(
      tariffServiceRelations.map(
        (clientTariffServiceId) => ({
          clientTariffServiceId,
          ticketId,
        }),
      ),
      { transaction },
    );
  }

  /**
   * create
   * @param {String} clientId Id клиента
   * @param {String} serviceAddressId Id адреса обслуживания
   * @param {String} [statusId] Id статуса
   * @param {String} [description] Описание заявки
   * @param {String} [solution] Описание решения
   * @param {String} [comment] Комментарий
   * @param {Object[]} [docNames] Список названий файлов
   * @param {String} docNames.fsName Имя файла на диске
   * @param {String} docNames.name Оригинальное имя файла
   * @param {Array} tariffServiceRelations Массив UUID услуг клиента из тарифа
   * @param {Array} oneTimeServices Массив UUID услуг клиента из разовых услуг
   * @returns {Promise<void>}
   */
  static async createTicket({
    supportRequestId,
    statusId,
    responsibleId,
    docNames,
    serviceAddressId,
    tariffServiceRelations,
    oneTimeServices,
    deviceTypeId,
    ...ticket
  } = {}) {
    const transaction = await db.sequelize.transaction();
    try {
      /* Создаем новый ЗНО */
      const ticketInstance = await TicketModel.create({
        ...ticket,
        ticketStatusId: statusId || TicketStatusModel.STATUSES.NEW,
        supportRequestId,
        responsibleId,
        serviceAddressId,
        ...(docNames && { ticketDocuments: docNames }),
      },
      {
        include: [
          {
            association: 'ticketDocuments',
            attributes: ['id', 'name'],
          },
        ],
        transaction,
      });

      /* Переименовываем документы в соответствии с id документа в бд */
      if (ticketInstance.ticketDocuments) {
        const docNamesMap = HelperService.createMapFromArray(docNames, 'name');
        ticketInstance.ticketDocuments.forEach((fileAttachment) => {
          FileService.renameFile({
            dir: path.join(FileService.DEFAULT_FILE_PATH, 'ticket'),
            oldName: docNamesMap[fileAttachment.name].fsName,
            newName: fileAttachment.id,
          });
        });
      }

      /* Создаем записи связей ЗНО и услуг клиента */
      await this.createTicketServiceRelations({
        ticketId: ticketInstance.id,
        oneTimeServices,
        tariffServiceRelations,
        transaction,
      });

      const alias = this.getTicketNumberPrefix(
        Object.keys(DeviceTypeModel.TYPES).find((key) => DeviceTypeModel.TYPES[key] === deviceTypeId),
      );

      await transaction.commit();
      return {
        ...ticketInstance.toJSON(),
        serial: `${alias}${ticketInstance.number}`,
      };
    } catch (error) {
      await transaction.rollback();
      if (!(error instanceof APIError)) {
        throw new APIError(ERRORS.TICKET.CREATE_TICKET, { originalError: error });
      }
      throw error;
    }
  }

  static checkShouldTicketHasAddress(services = []) {
    return services.some((service) => is.service.serviceAddressAccountingType.inTicket(
      service.serviceAddressAccountingTypeId,
    ));
  }

  static getTicketNumberPrefix(deviceTypeAlias) {
    return this.TICKET_NAMES[deviceTypeAlias] || this.TICKET_NAMES.MAIN;
  }

  static async getDocument(documentId) {
    const [document] = (await Promise.all([
      DocumentService.getDocument({
        model: TicketReviewDocumentModel,
        id: documentId,
        shouldThrowError: false,
        pathToDir: 'ticket',
      }),
      DocumentService.getDocument({
        model: TicketCloseDocumentModel,
        id: documentId,
        shouldThrowError: false,
        pathToDir: 'ticket',
      }),
      DocumentService.getDocument({
        model: TicketDocumentModel,
        id: documentId,
        shouldThrowError: false,
        pathToDir: 'ticket',
      }),
    ])).filter(Boolean);

    if (!document) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(documentId));

    return document;
  }


  static getActiveTickets({ clientId, services = [], transaction }) {
    return TicketModel.findAll({
      where: {
        ticketStatusId: {
          [Op.notIn]: [
            TicketStatusModel.STATUSES.COMPLETED,
            TicketStatusModel.STATUSES.CANCELLED,
          ],
        },
      },
      include: [
        {
          association: 'clientServices',
          where: {
            clientId,
            serviceId: services,
          },
        },
        {
          association: 'clientTariffServices',
          include: [
            {
              association: 'clientTariff',
              where: { clientId },
            },
            {
              association: 'tariffServiceRelation',
              paranoid: false,
              where: { serviceId: services },
            },
          ],
        },
      ],
      transaction,
    });
  }
}
