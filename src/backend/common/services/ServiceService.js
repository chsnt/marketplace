import {
  Op, fn, col, where, cast,
} from 'sequelize';
import { ServiceModel, AvailablePaymentMethodModel } from '../database/models/Tariff';
import {
  ClientServiceModel, ClientTariffServiceModel,
}
  from '../database/models/Client';
import { OrderStatusModel } from '../database/models/Order';
import { TicketStatusModel, TicketClientServiceRelationModel } from '../database/models/Ticket';
import APIError from '../utils/APIError';
import ERRORS from '../../admin/constants/errors';
import db from '../database';
import AvailableCatalogStatusModel from '../database/models/Tariff/AvailableCatalogStatusModel';
import { getAddressAndDevice } from '../utils/prepareResponse';
import AddressModel from '../database/models/AddressModel';
import ServiceAddressAccountingTypeModel from '../database/models/Tariff/ServiceAddressAccountingTypeModel';
import HelperService from './HelperService';
import DeviceTypeModel from '../database/models/Device/DeviceTypeModel';

export default class ServiceService {
  /**
   *
   * @param {String} search поиск
   * @param {Boolean} isCostRequired=true учитывать период действия цены
   * @param {Boolean} [forTariffOnly=false] учитывать доступные услуги только в рамках тарифа
   * @param {String} availableCatalogStatusId UUID доступа для интерфейса (сайт или админка). Если id не указан выводятся все услуги
   * @param {String} [deviceTypeId] Идентификатор типа оборудования
   * @return {Promise<*>}
   */
  static async getList({
    search,
    isCostRequired = true,
    forTariffOnly,
    availableCatalogStatusId,
    deviceTypeId,
  } = {}) {
    const services = await ServiceModel.findAll({
      where: {
        ...((typeof forTariffOnly === 'boolean') && { forTariffOnly }),
        ...(search && { name: { [Op.iLike]: `%${search}%` } }),
        ...(deviceTypeId && { deviceTypeId }),
        ...(availableCatalogStatusId && {
          availableCatalogStatusId: [
            AvailableCatalogStatusModel.STATUSES.ALL,
            availableCatalogStatusId,
          ],
        }),
      },
      attributes: ['id', 'name', 'label', 'icon', 'alias', 'description', 'serviceAddressAccountingTypeId'],
      include: [
        {
          ...ServiceModel.includeCost(true),
          required: isCostRequired,
        },
        {
          association: 'deviceType',
          attributes: ['id', 'name', 'alias'],
        },
      ],
    });

    return services.map((serviceInstance) => ({
      ...serviceInstance.toJSON(),
      cost: ServiceModel.getCurrentCost(serviceInstance.costs),
      costs: undefined,
      serviceAddressType: ServiceAddressAccountingTypeModel.NAMES[serviceInstance.serviceAddressAccountingTypeId],
    }));
  }

  static async getService({ id, isCostRequired }) {
    const serviceInstance = await ServiceModel.findByPk(id, {
      attributes: ['id', 'name', 'label', 'icon', 'alias', 'description', 'serviceAddressAccountingTypeId'],
      include: {
        ...ServiceModel.includeCost(),
        required: isCostRequired,
      },
    });

    if (!serviceInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(id));

    const { serviceAddressAccountingTypeId, ...service } = serviceInstance.toJSON();

    return {
      ...service,
      serviceAddressType: ServiceAddressAccountingTypeModel.NAMES[serviceAddressAccountingTypeId],
    };
  }

  /**
   *
   * @param {String} clientId UUID клиента
   * @param {Boolean} used Если нужно вернуть список использованных услуг
   * @return {Promise<Object>}
   * @description Возвращает, или использованные, или доступные для использования. Услуга не входит в список, если у неё есть активный тикет (ЗНО)
   */
  static async getClientServices({ clientId, used = false }) {
    const services = await ClientServiceModel.findAll({
      where: {
        clientId,
        '$order.order_status_id$': OrderStatusModel.STATUSES.PAID,
      },
      attributes: ['id'],
      include: [
        {
          association: 'service',
          attributes: ['id', 'name', 'alias', 'label', 'icon', 'serviceAddressAccountingTypeId', 'deviceTypeId'],
        },
        {
          association: 'order',
          attributes: [],
        },
        {
          association: 'tickets',
          attributes: ['id', 'ticketStatusId', 'updatedAt'],
          required: false,
          where: {
            ticketStatusId: {
              [Op.in]: [
                TicketStatusModel.STATUSES.NEW,
                TicketStatusModel.STATUSES.IN_PROGRESS,
                TicketStatusModel.STATUSES.UPDATE_INFORMATION,
                TicketStatusModel.STATUSES.COMPLETED,
              ],
            },
          },
        },
        AddressModel.addressAndDevice(),
      ],
    });

    return services.reduce((result, serviceItem) => {
      const {
        id: clientServiceId, service, tickets, serviceAddress,
      } = serviceItem.toJSON();

      const isCompleted = new Set(tickets.map((ticket) => ticket.ticketStatusId))
        .has(TicketStatusModel.STATUSES.COMPLETED);

      if ((!tickets.length && !used) || (used && isCompleted)) {
        const {
          serviceAddress: preparedServiceAddress = null,
          device = null,
        } = getAddressAndDevice(serviceAddress) || {};

        result.push({
          clientServiceId,
          completed: (isCompleted) ? tickets.pop().updatedAt : null,
          ...service,
          serviceAddressType: ServiceAddressAccountingTypeModel.NAMES[service.serviceAddressAccountingTypeId],
          deviceType: DeviceTypeModel.NAMES[service.deviceTypeId],
          serviceAddressAccountingTypeId: undefined,
          serviceAddress: preparedServiceAddress,
          device,
        });
      }

      return result;
    }, []);
  }

  static getAllPurchasedServices({ clientId, transaction, serviceAttributes = ['id', 'name'] } = {}) {
    return Promise.all([
      ClientTariffServiceModel.findAll({
        where: { value: { [Op.gt]: 0 } },
        include: [
          {
            association: 'tariffServiceRelation',
            paranoid: false,
            include: {
              association: 'service',
              paranoid: false,
              attributes: serviceAttributes,
            },
          },
          {
            association: 'clientTariff',
            where: {
              expirationAt: { [Op.gte]: new Date() },
              clientId,
            },
          },
        ],
        transaction,
      }),
      ClientServiceModel.findAll({
        where: { completed: null, clientId },
        include: [{
          association: 'service',
          attributes: serviceAttributes,
        }],
        transaction,
      }),
    ]);
  }

  static getServicesForTicket({
    clientServices = [],
    clientTariffServices = [],
  } = {}) {
    const servicesMap = {};

    clientServices.forEach((clientService) => {
      const { service } = clientService;
      servicesMap[service.id] = clientService.service;
    });

    clientTariffServices.forEach((clientTariffService) => {
      const { service } = clientTariffService.tariffServiceRelation;
      servicesMap[service.id] = service;
    });

    return Object.values(servicesMap).sort((a, b) => {
      if (a.name < b.name) { return -1; }
      if (a.name > b.name) { return 1; }
      return 0;
    });
  }

  static async updateService({ id, ...serviceData }) {
    const transaction = await db.sequelize.transaction();
    try {
      const serviceInstance = await ServiceModel.findByPk(id, { transaction });

      if (!serviceInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(id));

      await serviceInstance.update({
        ...serviceData,
      },
      { transaction });
      await transaction.commit();
      return null;
    } catch (error) {
      await transaction.rollback();
      throw new APIError(ERRORS.SERVICE.UPDATE_ERROR, { originalError: error });
    }
  }

  /**
   *
   * @param {Array} services UUID услуг
   * @return {String} UUID типа оборудования
   * @throws Если в списке есть услуги с разными типами устройств
   */
  static async checkSameDeviceType(services) {
    const serviceList = await ServiceModel.findAll({
      where: { id: services },
      attributes: ['deviceTypeId'],
    });

    if (new Set(serviceList.map(
      (serviceInstance) => serviceInstance.deviceTypeId,
    )).size !== 1) throw new APIError(ERRORS.SERVICE.DIFFERENT_TYPES_DEVICES);

    return serviceList[0].deviceTypeId;
  }

  static async getAvailableClientServices({ clientId, services }) {
    const skipClientServices = await TicketClientServiceRelationModel.findAll({
      attributes: ['clientServiceId', [cast(fn('COUNT', col('client_service_id')), 'integer'), 'count']],
      group: ['client_service_id'],
      having: where(fn('COUNT', col('client_service_id')), '>=', 2),
    });

    const cancelledClientServices = HelperService.createMapFromArray(await TicketClientServiceRelationModel.findAll({
      attributes: ['clientServiceId', [cast(fn('COUNT', col('client_service_id')), 'integer'), 'count']],
      include: {
        association: 'ticket',
        attributes: [],
        where: { ticketStatusId: TicketStatusModel.STATUSES.CANCELLED },
      },
      group: ['client_service_id'],
    }), 'clientServiceId');

    const preparedSkipClientServices = new Set();

    skipClientServices.forEach((skipClientService) => {
      const cancelled = cancelledClientServices[skipClientService.clientServiceId];

      if (!cancelled?.dataValues || skipClientService.dataValues.count <= cancelled.dataValues.count) return;

      preparedSkipClientServices.add(skipClientService.clientServiceId);
    });

    const clientServices = await ClientServiceModel.findAll({
      where: {
        id: {
          [Op.not]: [...preparedSkipClientServices],
        },
        clientId,
        serviceId: services,
        [Op.or]: [
          { $tickets$: null },
          {
            '$tickets.ticket_status_id$': TicketStatusModel.STATUSES.CANCELLED,
          },
        ],
      },
      attributes: ['id', 'serviceId'],
      include: [
        {
          association: 'ticketClientServiceRelations',
          attributes: ['clientServiceId'],
        },
        {
          association: 'order',
          attributes: ['orderStatusId'],
          where: { orderStatusId: OrderStatusModel.STATUSES.PAID },
          include: 'payments',
        },
        {
          association: 'tickets',
          attributes: ['ticketStatusId'],
        },
        {
          association: 'service',
          attributes: ['serviceAddressAccountingTypeId'],
        },
      ],
    });

    return clientServices;
  }

  static getAvailablePaymentMethods() {
    return AvailablePaymentMethodModel.findAll({
      attributes: ['id', 'name', 'alias'],
    });
  }
}
