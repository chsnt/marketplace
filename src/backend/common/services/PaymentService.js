import { OrderModel, PaymentModel } from '../database/models/Order';
import APIError from '../utils/APIError';
import ERRORS from '../../admin/constants/errors';

export default class PaymentService {
  static async getInfoByOrder(orderId) {
    const orderInstance = await OrderModel.findByPk(orderId, {
      attributes: ['id'],
      include: {
        association: 'payments',
        attributes: ['id', 'paymentDate'],
      },
    });

    if (!orderInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(orderId));

    const payment = PaymentModel.getCurrentPayment(orderInstance.payments);

    return payment.toJSON();
  }
}
