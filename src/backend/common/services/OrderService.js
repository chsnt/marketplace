import path from 'path';
import { Op } from 'sequelize';
import moment from 'moment';
import {
  OrderDocumentModel, OrderDocumentTypeModel, OrderModel, OrderStatusModel, OrderOfferDocumentModel, PaymentModel,
} from '../database/models/Order';
import APIError from '../utils/APIError';
import Discount from '../utils/Discount';
import FileService from './FileService';
import { ServiceModel, TariffModel, TariffServiceRelationModel } from '../database/models/Tariff';
import {
  ClientDeviceModel,
  ClientServiceModel,
  ClientTariffModel,
} from '../database/models/Client';
import db from '../database';
import ERRORS from '../../client/constants/errors';
import DocumentService from './DocumentService';
import HelperService from './HelperService';
import { GET_LIST_DEFAULT_LIMIT, GET_LIST_DEFAULT_SORT } from '../constants';
import is from '../libs/is';
import PaymentTypeModel from '../database/models/Order/PaymentTypeModel';

export default class OrderService {
  /**
   *
   * @param {String} [externalId] ID у интегратора
   * @param {String} typeId UUID типа документа
   * @param {String} orderId UUID заказа
   * @param {object} file
   * @param {object} file.name Оригинальное имя файла
   * @param {object} file.fsName Имя файла на диске
   * @returns {Promise<void>}
   */
  static async addDocumentToOrder({
    id: externalId, typeId, orderId, file,
  }) {
    const [orderInstance, orderDocumentTypeInstance] = await Promise.all([
      OrderModel.findByPk(orderId, {
        attributes: ['id', 'clientId', 'number', 'description'],
        include: {
          association: 'client',
          attributes: ['id'],
          include: [{
            association: 'organization',
            include: [
              {
                association: 'legalAddress',
                include: 'federationSubject',
              },
              {
                association: 'bankDetail',
                include: [{
                  association: 'taxType',
                }],
              },
              {
                association: 'electronicDocumentCirculation',
              },
            ],
          }],
        },
      }),
      OrderDocumentTypeModel.findByPk(typeId, { attributes: ['id'] }),
    ]);

    if (!orderInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(orderId));
    if (!orderDocumentTypeInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(typeId));

    const orderDocumentInstance = await OrderDocumentModel.create({
      orderId: orderInstance.id,
      orderDocumentTypeId: orderDocumentTypeInstance.id,
      name: file.name,
      externalId,
    });

    FileService.renameFile({
      dir: path.join(FileService.DEFAULT_FILE_PATH, 'order'),
      oldName: file.fsName,
      newName: orderDocumentInstance.id,
    });

    return {
      order: orderInstance.toJSON(),
      document: orderDocumentInstance.toJSON(),
    };
  }

  static async addOfferDocument(file) {
    const orderDocumentInstance = await OrderOfferDocumentModel.create({
      name: file.name,
      alias: FileService.removeTimestamp(file.fsName),
    });

    return FileService.renameFile({
      dir: path.join(FileService.DEFAULT_FILE_PATH, 'order/offer-documents'),
      oldName: file.fsName,
      newName: orderDocumentInstance.id,
    });
  }

  static getDocumentTypes({
    sortByDate = GET_LIST_DEFAULT_SORT,
    offset = 0,
    limit = GET_LIST_DEFAULT_LIMIT,
  }) {
    return OrderDocumentTypeModel.findAndCountAll({
      offset,
      limit,
      order: [['createdAt', sortByDate]],
      attributes: ['id', 'name', 'alias'],
    });
  }

  static async addToCart({ tariffs = [], services = [], clientId }) {
    const transaction = await db.sequelize.transaction();
    try {
      /* Находим или создаем корзину */
      const [cartInstance] = await OrderModel.findOrCreate({
        where: {
          clientId,
          orderStatusId: OrderStatusModel.STATUSES.CART,
        },
        transaction,
      });

      let cartTariffs = null;
      let cartServices = null;

      /* Создаем элементы */
      if (tariffs.length) {
        const tariffServiceRelation = await TariffServiceRelationModel.findAll({
          where: {
            tariffId: tariffs,
          },
          transaction,
        });

        cartTariffs = await ClientTariffModel.bulkCreate(tariffs.map((tariffId) => ({
          tariffId,
          clientId,
          orderId: cartInstance.id,
          clientTariffServices: tariffServiceRelation.map((service) => ({
            tariffServiceRelationId: service.id,
            value: service.limit,
          })),
        })), { include: ['clientTariffServices'], transaction });
      }

      if (services.length) {
        cartServices = await ClientServiceModel.bulkCreate(services.map((serviceId) => ({
          clientId,
          serviceId,
          orderId: cartInstance.id,
        })), { transaction });
      }

      await transaction.commit();
      return {
        orderId: cartInstance.id,
        tariffs: cartTariffs,
        services: cartServices,
      };
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }

  static async getCart(clientId) {
    /* Получим справочник тарифов и включенных в него услуг, и справочник разовых услуг */
    const [tariffsWithServices, services] = await Promise.all([
      TariffModel.findAll({
        attributes: ['id', 'name', 'label', 'icon', 'expirationMonths', 'alias', 'description'],
        include: [
          {
            association: 'services',
            through: { attributes: [] },
            attributes: ['id', 'name', 'nameInTariff', 'forTariffOnly', 'label', 'icon', 'alias'],
          },
          TariffModel.includeCost(true),
        ],
      }),
      ServiceModel.findAll({
        where: { forTariffOnly: false },
        attributes: ['id', 'name', 'label', 'icon', 'alias'],
        include: ServiceModel.includeCost(true),
      }),
    ]);

    /* Формируем карты */
    const tariffsWithServicesMap = HelperService.createMapFromArray(tariffsWithServices);
    const servicesMap = HelperService.createMapFromArray(services);

    const orderInstance = await OrderModel.findOne({
      where: {
        clientId,
        orderStatusId: OrderStatusModel.STATUSES.CART,
      },
      attributes: ['id', 'number', 'discount', 'description'],
      include: [
        {
          association: 'clientTariffs',
          attributes: ['id', 'tariffId'],
          include: {
            attributes: ['id'],
            association: 'tariff',
            include: {
              association: 'discountService',
              required: false,
              where: {
                expirationAt: {
                  [Op.or]: [
                    { [Op.gte]: new Date() },
                    { [Op.is]: null },
                  ],
                },
              },
              include: {
                association: 'discountTemplate',
                include: 'discountTariffServiceRelations',
              },
            },
          },
        },
        {
          association: 'clientServices',
          attributes: ['id', 'serviceId'],
          include: {
            attributes: ['id'],
            association: 'service',
            include: {
              association: 'discountService',
              required: false,
              where: {
                expirationAt: {
                  [Op.or]: [
                    { [Op.gte]: new Date() },
                    { [Op.is]: null },
                  ],
                },
              },
              include: {
                association: 'discountTemplate',
                include: 'discountTariffServiceRelations',
              },
            },
          },
        },
        {
          association: 'coupon',
          attributes: ['code'],
          include: {
            association: 'discountTemplate',
            include: 'discountTariffServiceRelations',
          },
        },
      ],
    });

    if (!orderInstance) return null;

    const {
      id: orderId, clientTariffs, clientServices, coupon, discount, ...order
    } = orderInstance.toJSON();

    let totalCost = 0;
    let totalWithoutDiscountCost = 0;

    const discountObj = new Discount({
      coupon, clientTariffs, clientServices, discount,
    });

    return {
      ...order,
      orderId,
      tariffs: clientTariffs.map(({ tariffId, ...clientTariff }) => {
        const tariff = tariffsWithServicesMap[tariffId]?.toJSON();
        const currentCost = TariffModel.getCurrentCost(tariff?.costs);
        const discountCost = discountObj.getDiscountCost({
          currentCost, id: tariff?.id,
        });
        totalCost += discountCost || 0;
        totalWithoutDiscountCost += currentCost || 0;
        return {
          ...clientTariff,
          tariff: {
            ...tariff,
            cost: discountCost,
            withoutDiscountCost: currentCost,
            costs: undefined,
            services: tariff.services.map(({
              name, nameInTariff, ...service
            }) => ({ ...service, name: nameInTariff || name })),
          },
        };
      }),
      services: clientServices.map(({ serviceId, ...clientService }) => {
        const service = servicesMap[serviceId]?.toJSON();
        const currentCost = ServiceModel.getCurrentCost(service?.costs);
        const discountCost = discountObj.getDiscountCost({
          currentCost, id: service?.id,
        });
        totalCost += discountCost || 0;
        totalWithoutDiscountCost += currentCost || 0;
        return {
          ...clientService,
          service: {
            ...service,
            cost: discountCost,
            withoutDiscountCost: currentCost,
            costs: undefined,
          },
        };
      }),
      totalCost: parseFloat(totalCost.toFixed(2)),
      totalWithoutDiscountCost,
      discountCod: coupon?.code,
      specialDiscount: discount,
    };
  }

  static async getLastOfferDocumentAndCount({ transaction } = {}) {
    const orderOfferDocumentInstances = await OrderOfferDocumentModel.findAll({
      attributes: ['id', 'createdAt'],
      order: [['createdAt', 'desc']],
      transaction,
    });

    return {
      document: orderOfferDocumentInstances[0],
      count: orderOfferDocumentInstances.length,
    };
  }

  static async getDocument(documentId) {
    const [document] = (await Promise.all([
      DocumentService.getDocument({
        model: OrderDocumentModel,
        id: documentId,
        shouldThrowError: false,
        pathToDir: 'order',
      }),
      DocumentService.getDocument({
        model: OrderOfferDocumentModel,
        id: documentId,
        shouldThrowError: false,
        pathToDir: 'order/offer-documents',
      }),
    ])).filter(Boolean);

    if (!document) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(documentId));

    return document;
  }

  static async checkAllowedProducts({
    services = [], tariffs = [], clientType, shouldAvailablePayment = false, transaction,
  }) {
    /* Получаем справочники услуг и тарифов с привязкой к производителям оборудования */
    /* Получаем список оборудования организации с производителем */
    const [tariffList, serviceList, clientDevices] = await Promise.all([
      TariffModel.findAll({
        where: { id: tariffs.map(({ id }) => id) },
        include: [{
          association: 'deviceManufacturers',
          attributes: ['id'],
        }],
        attributes: ['id', 'name', 'availablePaymentMethodId'],
        transaction,
      }),
      ServiceModel.findAll({
        where: { id: services.map(({ id }) => id) },
        include: [{
          association: 'deviceManufacturers',
          attributes: ['id'],
        }],
        attributes: ['id', 'name', 'availablePaymentMethodId'],
        transaction,
      }),
      ClientDeviceModel.findAll({
        where: {
          serviceAddressId: [
            ...services.map(({ serviceAddressId }) => serviceAddressId),
            ...tariffs.map(({ serviceAddressId }) => serviceAddressId),
          ].filter(Boolean),
        },
        include: [{
          association: 'deviceModel',
          attributes: ['id', 'deviceManufacturerId'],
        }],
        transaction,
      }),
    ]);

    const availableTestList = new Set();

    const checkAvailablePayment = (rows) => {
      rows.forEach(({ name, availablePaymentMethodId }) => {
        if (
          is.client.type.individual(clientType)
          && is.service.availablePaymentMethod.invoice(availablePaymentMethodId)
        ) {
          throw new APIError(ERRORS.ORDER.INDIVIDUAL_NOT_AVAILABLE_PAYMENT(name));
        }

        if (
          is.service.availablePaymentMethod.card(availablePaymentMethodId)
          || is.service.availablePaymentMethod.invoice(availablePaymentMethodId)
        ) {
          availableTestList.add(availablePaymentMethodId);
        }
      });
    };

    if (shouldAvailablePayment) {
      checkAvailablePayment(tariffList);
      checkAvailablePayment(serviceList);
      if (availableTestList.size > 1) throw new APIError(ERRORS.ORDER.CONFLICT_AVAILABLE_PAYMENT);
    }


    /* Функция формирования карт тарифа/услуга -> список id доступных производителей */
    const getInstanceToManufacturerMap = (rows) => rows.reduce((acc, instance) => ({
      ...acc,
      [instance.id]: instance.deviceManufacturers.map((manufacturerInstance) => manufacturerInstance.id),
    }), {});

    /* Формируем карты */
    const tariffsMap = getInstanceToManufacturerMap(tariffList);
    const servicesMap = getInstanceToManufacturerMap(serviceList);
    const clientDevicesMap = clientDevices.reduce((acc, instance) => ({
      ...acc,
      [instance.id]: instance.deviceModel.deviceManufacturerId,
    }), {});

    /* Проверяем доступность тарифов для оборудования организации */
    /* Если в тарифе или услуге нет производителя оборудования - продукт доступен всем производителям */
    const checkAllowed = ({ rows, map, isTariff = true }) => {
      rows.forEach(({ id, serviceAddressId }) => {
        const deviceManufacturers = map[id];
        if (deviceManufacturers?.length && !deviceManufacturers.includes(clientDevicesMap[serviceAddressId])) {
          throw new APIError(ERRORS.ORDER.PRODUCT_NOT_ALLOWED_FOR_DEVICE({ isTariff }));
        }
      });
    };
    checkAllowed({ rows: services, map: servicesMap, isTariff: false });
    checkAllowed({ rows: tariffs, map: tariffsMap });
  }

  /**
   * @param {String} clientId id клиента
   * @param {String} orderId id заказа
   * @param {Object.<sequelize.transaction>} [transaction] транзакция
   * @returns {Promise<boolean>}
   */
  static async checkFirstOrder({ clientId, transaction, orderId }) {
    const orderInstance = await OrderModel.findOne({
      attributes: ['id'],
      where: {
        clientId,
        [Op.and]: {
          ...(orderId && {
            id: {
              [Op.not]: orderId,
            },
          }),
          orderStatusId: {
            [Op.or]: [
              OrderStatusModel.STATUSES.PAID,
              OrderStatusModel.STATUSES.NOT_PAID,
            ],
          },
        },
      },
      transaction,
    });

    return !orderInstance;
  }

  /**
   *
   * @param {String} couponId UUID купона
   * @param {String} clientId UUID клиента
   */
  static async addCoupon({
    couponId, clientId,
  }) {
    const orderInstance = await OrderModel.findOne({
      where: {
        clientId,
        orderStatusId: OrderStatusModel.STATUSES.CART,
      },
    });

    if (!orderInstance) throw new APIError(ERRORS.ORDER.NOT_FOUND);

    await orderInstance.update({ couponId });
  }

  static async removeCoupon({ clientId }) {
    const transaction = await db.sequelize.transaction();
    try {
      const orderInstance = await OrderModel.findOne({
        where: {
          clientId,
          orderStatusId: OrderStatusModel.STATUSES.CART,
        },
      }, { transaction });

      if (!orderInstance) throw new APIError(ERRORS.ORDER.NOT_FOUND);
      await orderInstance.update({ couponId: null }, { transaction });
      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      if (error instanceof APIError) throw error;
      throw new APIError(ERRORS.DISCOUNT.COUPON.FAILED_TO_REMOVE, { originalError: error });
    }
  }

  static async resetOrderToCart(paymentOrderId) {
    const paymentInstance = await PaymentModel.findOne({
      where: { paymentOrderId },
      attributes: ['id'],
      include: [{
        association: 'order',
        attributes: ['id', 'orderStatusId'],
      }],
    });

    return paymentInstance.order.update({
      orderStatusId: OrderStatusModel.STATUSES.CART,
      orderDate: null,
    });
  }

  static async completeOrder(paymentOrderId) {
    const transaction = await db.sequelize.transaction();
    try {
      const paymentInstance = await PaymentModel.findOne({
        where: { paymentOrderId },
        attributes: ['id', 'paymentTypeId', 'paymentDate', 'number'],
        include: {
          association: 'order',
          attributes: ['id', 'number', 'orderStatusId', 'clientId', 'createdAt', 'orderDate'],
          include: [{
            association: 'clientServices',
            attributes: ['id', 'serviceAddressId'],
            include: {
              association: 'service',
              attributes: ['id', 'name'],
              include: [{
                ...ServiceModel.includeCost(true),
                separate: true,
              }],
            },
          },
          {
            association: 'clientTariffs',
            attributes: ['id', 'serviceAddressId', 'expirationAt'],
            include: {
              association: 'tariff',
              attributes: ['id', 'name', 'expirationMonths'],
              include: [{
                ...TariffModel.includeCost(true),
                separate: true,
              }],
            },
          },
          {
            association: 'orderOfferDocument',
            attributes: ['id', 'createdAt'],
          }],
        },
        transaction,
      });

      if (!paymentInstance) throw new APIError(ERRORS.ORDER.PAYMENT_NOT_FOUND);

      /* Добавляем время жизни к тарифy */
      await Promise.all(paymentInstance.order?.clientTariffs.map((orderTariff) => {
        const expirationAt = moment()
          .add(orderTariff.tariff.expirationMonths, 'M')
          .add(1, 'day')
          .startOf('day');

        return orderTariff.update({ expirationAt }, { transaction });
      }));

      const now = new Date();

      await Promise.all([
        paymentInstance.update({ paymentDate: now, paymentTypeId: PaymentTypeModel.TYPES.CARD.id }),
        paymentInstance.order.update({ orderStatusId: OrderStatusModel.STATUSES.PAID, orderDate: now }),
      ]);
      await transaction.commit();
      return paymentInstance.toJSON();
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }

  static getOrderStatusList() {
    return OrderStatusModel.findAll({
      attributes: ['id', 'name', 'alias'],
    });
  }
}
