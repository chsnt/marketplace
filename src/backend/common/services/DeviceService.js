import { Op, literal } from 'sequelize';
import {
  ClientDeviceParameterModel,
  ClientDeviceModel,
  ClientTariffModel,
  ClientServiceModel,
} from '../database/models/Client';
import {
  DeviceManufacturerModel, DeviceModelModel, DeviceParameterModel, DeviceTypeModel,
} from '../database/models/Device';
import APIError from '../utils/APIError';
import ERRORS from '../constants/errors';
import HelperService from './HelperService';

import db from '../database';
import asyncPool from '../libs/asyncPools';
import ServiceService from './ServiceService';
import { TicketModel, TicketStatusModel } from '../database/models/Ticket';
import ClientService from './ClientService';

export default class DeviceService {
  static async createClientDevice({ device, clientId }) {
    if (!device) return null;

    const transaction = await db.sequelize.transaction();
    try {
      const clientDevice = await ClientDeviceModel.create({
        deviceModelId: device.model,
        serviceAddress: device.address,
        clientId,
      }, {
        attributes: ['id'],
        include: 'serviceAddress',
        transaction,
      });

      await ClientDeviceParameterModel.bulkCreate(device.parameters.map((parameter) => ({
        deviceParameterId: parameter.id,
        clientDeviceId: clientDevice.id,
        value: parameter.value,
      })), { transaction });

      await transaction.commit();
      return clientDevice;
    } catch (error) {
      await transaction.rollback();
      if (!(error instanceof APIError)) throw new APIError(ERRORS.DEVICE.CREATE_DEVICE, { originalError: error });
      throw error;
    }
  }

  /**
   * @typedef {Object} device
   * @property {String} device.manufacturer Название производителя.
   * @property {String} device.model Модель устройства.
   * @property {Array.<Object>} device.parameters Список параметров.
   * @property {String} device.parameter.key Название параметра.
   * @property {String} device.parameter.value Данные параметра.
   * @property {Object} device.address Адрес обслуживания.
   * @property {String} device.address.region Субъект РФ
   * @property {String} device.address.city Город
   * @property {String} device.address.street Улица
   * @property {String} device.address.house Дом
   * @property {String} device.address.apartment Квартира/офис
   * @property {String} device.address.postalCode Почтовый индекс
   */

  /**
   * @async
   * @param {String} id UUID устройства организации
   * @returns {Promise<device>}
   */
  static async getClientDevice(id) {
    const clientDeviceInstance = await ClientDeviceModel.findByPk(id, {
      attributes: ['id', 'clientId'],
      include: [
        {
          association: 'serviceAddress',
          attributes: ['id', 'street', 'house', 'apartment', 'postalCode'],
          include: [
            {
              association: 'cityRelationship',
              attributes: ['name', 'utcOffset'],
              include: {
                association: 'managementService',
                attributes: ['id', 'name', 'externalId', 'code'],
              },
            },
            {
              association: 'federationSubject',
              attributes: ['name'],
            },
          ],
        },
        {
          association: 'deviceModel',
          attributes: ['id', 'name'],
          include: [{
            association: 'deviceManufacturer',
            attributes: ['name'],
          }, {
            association: 'deviceType',
            attributes: ['id'],
          }],
        },
        {
          association: 'clientDeviceParameters',
          attributes: ['id', 'value'],
          include: [{ association: 'deviceParameter', attributes: ['name', 'alias'] }],
        },
      ],
    });

    if (!clientDeviceInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(id));

    const {
      cityRelationship,
      federationSubject,
      ...address
    } = clientDeviceInstance.serviceAddress.toJSON();

    const { name: city, utcOffset, managementService } = cityRelationship || {};
    const { name: region } = federationSubject || {};

    return {
      id: clientDeviceInstance.id,
      clientId: clientDeviceInstance.clientId,
      manufacturer: clientDeviceInstance.deviceModel.deviceManufacturer?.name || null,
      model: clientDeviceInstance.deviceModel.name,
      modelId: clientDeviceInstance.deviceModel.id,
      deviceTypeId: clientDeviceInstance.deviceModel.deviceType.id,
      address: {
        ...address, city, utcOffset, region, managementService,
      },
      serialNumber: clientDeviceInstance.serialNumber,
      parameters: clientDeviceInstance.clientDeviceParameters.map(({
        id: clientDeviceParameterId, value, deviceParameter,
      }) => ({
        id: clientDeviceParameterId,
        value,
        ...deviceParameter,
      })),
    };
  }

  static async getDeviceParameters() {
    const rawResult = await DeviceManufacturerModel.findAll({
      attributes: ['id', 'name'],
      include: [
        {
          association: 'deviceModels',
          attributes: ['id', 'name'],
          include: [
            {
              association: 'deviceType',
              attributes: ['id', 'name'],
            }, {
              association: 'deviceParameters',
              attributes: ['id', 'name', 'isRequired'],
              include: [{
                association: 'fieldType',
                attributes: ['id', 'alias'],
              }],
            }],
        },
      ],
    });

    /* формирование структуры ответа */
    return rawResult.map((manufacturerWithParameters) => {
      const { id, name: manufacturer, deviceModels } = manufacturerWithParameters.toJSON();

      const models = deviceModels.map((model) => {
        const preparedParameters = model.deviceParameters.map((parameters) => {
          const { fieldType: { alias: type }, ...params } = parameters;
          return { ...params, type };
        });

        return {
          id: model.id,
          model: model.name,
          type: model.deviceType.name,
          parameters: preparedParameters,
        };
      });

      return {
        id,
        manufacturer,
        models,
      };
    });
  }

  static async getClientDevices({
    parameterValue,
    clientName,
    deviceModelName,
    clientId,
    deviceTypeId,
    deviceModelId,
    deviceManufacturerId,
    isFiasRequired,
  } = {}) {
    /* Получим модели оборудования в соответствии с фильтрами */
    const deviceModels = await DeviceModelModel.findAll({
      where: {
        ...(deviceModelId && { deviceModelId }),
        ...(deviceTypeId && { deviceTypeId }),
        ...(deviceManufacturerId && { deviceManufacturerId }),
        ...(deviceModelName && { name: { [Op.iLike]: `%${deviceModelName}%` } }),
      },
      attributes: ['id', 'name'],
      include: [
        {
          association: 'deviceType',
          attributes: ['name', 'alias'],
        },
        {
          association: 'deviceManufacturer',
          attributes: ['id', 'name'],
        },
      ],
    });

    const deviceModelsMap = HelperService.createMapFromArray(deviceModels);

    const deviceParametersMap = HelperService.createMapFromArray(await DeviceParameterModel.findAll({
      where: {
        ...(deviceModels.length && { deviceModelId: Object.keys(deviceModelsMap) }),
      },
    }));

    const clients = await ClientService.getListWithSearch({ clientId, clientName });

    const clientsMap = HelperService.createMapFromArray(clients);

    /* Получим оборудование клиентов по подготовленным фильтрам */
    const clientDevices = await ClientDeviceModel.findAll({
      where: {
        deviceModelId: Object.keys(deviceModelsMap),
        clientId: Object.keys(clientsMap),
      },
      attributes: ['id', 'createdAt', 'clientId', 'deviceModelId'],
      order: [['createdAt', 'desc']],
      include: [
        {
          association: 'serviceAddress',
          attributes: ['id', 'street', 'house', 'apartment', 'postalCode', 'fiasId'],
          include: [
            {
              association: 'cityRelationship',
              attributes: ['id', 'name', 'utcOffset'],
            },
            {
              association: 'federationSubject',
              attributes: ['id', 'name'],
            },
          ],
        },
        {
          association: 'clientDeviceParameters',
          attributes: ['id', 'value', 'deviceParameterId'],
          ...(parameterValue && { where: { value: { [Op.iLike]: `%${parameterValue}%` } } }),
        },
      ],
    });

    return clientDevices.map((device) => {
      const {
        id,
        serviceAddress: {
          cityRelationship,
          federationSubject,
          fiasId,
          ...address
        },
        clientDeviceParameters,
        deviceModelId: clientDeviceModelId,
        clientId: deviceClientId,
      } = device.toJSON();

      const { deviceType, deviceManufacturer, ...deviceModel } = deviceModelsMap[clientDeviceModelId].toJSON();
      const clientInstance = clientsMap[deviceClientId];

      const { name: manufacturer } = deviceManufacturer || {};

      return {
        id,
        deviceType: deviceType.name,
        deviceManufacturer: manufacturer,
        deviceModel,
        address: {
          ...address,
          city: cityRelationship,
          region: federationSubject,
          fiasId: isFiasRequired ? fiasId : undefined,
        },
        parameters: clientDeviceParameters.map(({ deviceParameterId, ...clientDeviceParameter }) => {
          const { name, alias } = deviceParametersMap[deviceParameterId];
          return {
            ...clientDeviceParameter, name, alias,
          };
        }),
        client: clientInstance.name,
      };
    });
  }

  static async getDeviceModelParameters(deviceModelId) {
    const result = await DeviceParameterModel.findAll({
      where: { deviceModelId },
      attributes: ['id', 'name', 'isRequired', 'alias'],
      include: [{
        association: 'fieldType',
        attributes: ['alias'],
      }],
    });

    return result.map((parameterInstance) => {
      const { fieldType, ...parameter } = parameterInstance.toJSON();
      return {
        ...parameter,
        type: fieldType.alias,
      };
    });
  }

  static getManufacturerList() {
    return DeviceManufacturerModel.findAll({
      attributes: ['id', 'name'],
    });
  }

  static async getTypeManufacturerModel() {
    const result = await DeviceTypeModel.findAll({
      attributes: ['id', 'name'],
      include: [{
        association: 'deviceManufacturers',
        attributes: ['id', 'name'],
        through: { attributes: [] },
        include: [{
          association: 'deviceModels',
          attributes: ['id', 'name'],
        }],
      }],
    });

    return result.map((type) => ({
      id: type.id,
      name: type.name,
      manufacturers: type.deviceManufacturers.map((manufacturer) => ({
        id: manufacturer.id,
        name: manufacturer.name,
        models: manufacturer.deviceModels,
      })),
    }));
  }

  static getDeviceModels(deviceManufacturerId) {
    return DeviceModelModel.findAll({
      where: { ...(deviceManufacturerId && { deviceManufacturerId }) },
      attributes: ['id', 'name'],
    });
  }

  static async deleteClientDevice(deviceId) {
    const tariffs = await ClientTariffModel.findAll({
      where: {
        clientDeviceId: deviceId,
        expirationAt: {
          [Op.and]: [{
            [Op.not]: null,
          }, {
            [Op.gt]: literal('NOW()'),
          }],
        },
      },
    });

    const services = await ClientServiceModel.findAll({
      where: {
        clientDeviceId: deviceId,
        completed: null,
      },
    });

    if (tariffs.length || services.length) {
      throw new APIError(ERRORS.DEVICE.DELETE_ORGANIZATION_DEVICE, {
        errorData: {
          tariffs: tariffs.map((tariff) => tariff.id),
          services: services.map((service) => service.id),
        },
      });
    }

    await ClientDeviceModel.destroy({
      where: {
        id: deviceId,
      },
    });
  }

  static async updateClientDevice({ id: deviceId, parameters, address }) {
    const transaction = await db.sequelize.transaction();

    try {
      const clientDeviceInstance = await ClientDeviceModel.findByPk(deviceId, {
        include: [
          {
            association: 'serviceAddress',
            include: ['cityRelationship', 'federationSubject'],
          }, {
            association: 'clientDeviceParameters',
            separate: true,
          }],
        transaction,
      });

      if (!clientDeviceInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(deviceId));

      /* Обновляем параметры */
      if (parameters) {
        const clientDeviceParametersMap = HelperService.createMapFromArray(
          clientDeviceInstance.clientDeviceParameters, 'deviceParameterId',
        );

        await asyncPool(parameters, (newParameter) => {
          const parameter = clientDeviceParametersMap[newParameter.id];
          if (!parameter) return null;
          return parameter.update(newParameter, { transaction });
        });
      }

      /* Обновляем адрес */
      if (address) {
        await clientDeviceInstance.serviceAddress.update(address, { transaction });
      }
      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }

  static getManagementServices(deviceId) {
    return ClientDeviceModel.findByPk(deviceId, {
      include: {
        association: 'serviceAddress',
        include: {
          association: 'cityRelationship',
          include: {
            association: 'managementService',
          },
        },
      },
    });
  }

  static getActiveTickets({
    excludeTickets,
    clientServices = [],
    clientTariffServices = [],
    transaction,
  }) {
    return TicketModel.findAll({
      where: {
        ticketStatusId: { [Op.notIn]: [TicketStatusModel.STATUSES.COMPLETED, TicketStatusModel.STATUSES.CANCELLED] },
        resolvedAt: null,
        ...(excludeTickets && { id: { [Op.notIn]: excludeTickets } }),
      },
      include: [
        {
          association: 'clientServices',
          required: false,
          where: { id: clientServices },
        },
        {
          association: 'clientTariffServices',
          required: false,
          where: { id: clientTariffServices },
        },
      ],
      transaction,
    });
  }


  static async getAllowedServices({ clientDeviceId, transaction }) {
    const deviceInstance = await ClientDeviceModel.findByPk(clientDeviceId, {
      attributes: ['id', 'clientId'],
      transaction,
    });

    if (!deviceInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(clientDeviceId));

    const [clientTariffServices, clientServices] = await ServiceService.getAllPurchasedServices({
      clientId: deviceInstance.clientId,
      transaction,
      serviceAttributes: ['id', 'name', 'serviceAddressAccountingTypeId'],
    });

    /* Получим незакрытые ЗНО по купленным разовым услугам клиента */
    const activeTickets = await DeviceService.getActiveTickets({
      clientDeviceId,
      clientServices: clientServices.map((instance) => instance.id),
      clientTariffServices: clientTariffServices.map((instance) => instance.id),
      transaction,
    });

    /* Отметаем разовые услуги из незакрытых ЗНО */
    const notAllowedClientServicesMap = HelperService.createMapFromArray(activeTickets
      .map((ticket) => ticket.clientServices
        .map((clientService) => clientService.id))
      .flat());

    /* Считаем, сколько раз была использована услуга тарифа в незакрытых ЗНО  */
    const usedClientTariffServices = activeTickets.reduce((acc, ticket) => {
      ticket.clientTariffServices.forEach((clientTariffService) => {
        if (!acc[clientTariffService.id]) {
          acc[clientTariffService.id] = 1;
          return;
        }
        acc[clientTariffService.id] += 1;
      });
      return acc;
    }, {});

    return ServiceService.getServicesForTicket({
      /* Для разовых услуг просто фильтруем по карте использованных услуг */
      clientServices: clientServices
        .filter((instance) => !notAllowedClientServicesMap[instance.id]),
      /* Для услуг из тарифа фильтруем в соответствии с количеством использований
      (количество незакрытых ЗНО не должно превышать остаток услуги на тарифе) */
      clientTariffServices: clientTariffServices
        .filter((instance) => instance.value > (usedClientTariffServices[instance.id] || 0)),
    });
  }

  static getDeviceTypes({ skipDefault = false } = {}) {
    return DeviceTypeModel.findAll({
      ...(skipDefault && {
        where: {
          id: {
            [Op.not]: DeviceTypeModel.TYPES.DEFAULT,
          },
        },
      }),
      attributes: ['id', 'name', 'alias'],
    });
  }
}
