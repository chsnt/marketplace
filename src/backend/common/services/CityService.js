import { Op } from 'sequelize';
import { FederationSubjectModel } from '../database/models/City';

export default class CityService {
  static async getList({ onlyTestingParticipant, region, city }) {
    return FederationSubjectModel.findAll({
      where: {
        ...(region && {
          name: {
            [Op.iLike]: `%${region}%`,
          },
        }),
        ...(city && {
          '$cities.name$': {
            [Op.iLike]: `%${city}%`,
          },
        }),
        ...(onlyTestingParticipant && {
          '$cities.is_testing_participant$': true,
        }),
      },
      attributes: ['id', 'name'],
      include: [{
        association: 'cities',
        attributes: ['id', 'name', 'isTestingParticipant', 'utcOffset'],
      }],
      order: [
        ['name', 'ASC'],
        ['cities', 'name', 'ASC'],
      ],
    });
  }
}
