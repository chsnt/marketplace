import { Op } from 'sequelize';
import { CouponModel } from '../database/models/Discount';
import APIError from '../utils/APIError';
import ERRORS from '../constants/errors';

export default class CouponService {
  /**
   * @param {String} code купон
   * @param {String} clientId UUID клиента
   * @return {Promise<{couponId, value, typeId}>} couponId - UUID купона, value - значение, typeId - тип
   *
   */
  static async getCoupon({ code, clientId }) {
    const couponInstance = await CouponModel.findOne({
      where: {
        code,
        expirationAt: {
          [Op.or]: [
            { [Op.gte]: new Date() },
            { [Op.is]: null },
          ],
        },
      },
      attributes: ['id', 'code'],
      include: [
        {
          association: 'discountTemplate',
          include: 'discountType',
        },
        {
          association: 'orders',
          required: false,
          where: { clientId },
        },
      ],
    });

    if (!couponInstance) throw new APIError(ERRORS.DISCOUNT.COUPON.NOT_FOUND);

    if (couponInstance.discountTemplate.limit <= couponInstance.orders.length) {
      throw new APIError(ERRORS.DISCOUNT.COUPON.USED);
    }

    return {
      couponId: couponInstance.id,
      value: couponInstance.discountTemplate.value,
      typeId: couponInstance.discountTemplate.discountType.id,
    };
  }
}
