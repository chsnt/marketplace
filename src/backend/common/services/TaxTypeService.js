import { TaxTypeModel } from '../database/models/Client/Organization';

export default class TaxTypeService {
  static getList() {
    return TaxTypeModel.findAll({
      attributes: ['id', 'name', 'alias'],
    });
  }
}
