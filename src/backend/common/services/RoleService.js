import httpContext from 'express-http-context';
import { UserModel, UserGroupModel } from '../database/models/User';
import is from '../libs/is';

export default class RoleService {
  static USER_GROUPS = UserGroupModel.GROUPS;

  static ACCESS_TO_ADMIN_PANEL = UserGroupModel.ACCESS_TO_ADMIN_PANEL;

  /* Метод проверяет уровень доступа пользователя */
  static async hasUserGroups(groups, checkAll = false) {
    /* Получаем id пользователя из контекста */
    const currentUserId = httpContext.get('user');
    /* Получаем актуальные группы пользователя */
    const userInstance = await UserModel.findByPk(currentUserId, { include: ['groups'] });
    if (!userInstance || userInstance?.groups?.length === 0) return false;
    /* Администратору можно все */
    if (userInstance.groups.find(({ id }) => is.user.group.admin(id))) return true;
    return this.hasUserAccess({ groups, userGroups: userInstance.groups, checkAll });
  }

  /* Метод сравнивает группы пользователя и группы доступа */
  static hasUserAccess({ groups = [], userGroups = [], checkAll = false }) {
    /* В зависимости от того, нужна ли нам только одна группа из, либо обязательно все,
    возвращаем результат */
    const method = checkAll ? 'every' : 'some';
    return groups[method]((group) => userGroups.map(({ id }) => id).includes(group));
  }
}
