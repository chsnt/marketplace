import {
  LeadModel, LeadNeedModel, LeadStatusModel, LeadTariffRelationModel, LeadServiceRelationModel,
} from '../database/models/Lead';
import db from '../database';
import APIError from '../utils/APIError';
import ERRORS from '../constants/errors';

export default class LeadService {
  static async createLead({
    leadStatusId, leadNeedId, formId, services, tariffs, ...leadData
  }) {
    const transaction = await db.sequelize.transaction();
    try {
      const leadInstance = await LeadModel.create({
        ...leadData,
        leadStatusId: leadStatusId || LeadStatusModel.STATUSES.NEW,
        leadNeedId: leadNeedId || LeadNeedModel.NEEDS.FREE_CONSULTATION,
        ...(formId && { formId: formId.toUpperCase() }),
      }, { transaction });

      const leadId = leadInstance.id;

      /* Создаем записи связей лида и интересующих тарифов и услуг */
      if (tariffs?.length) {
        await LeadTariffRelationModel.bulkCreate(
          tariffs.map((tariffId) => ({ tariffId, leadId })),
          { transaction },
        );
      }

      if (services?.length) {
        await LeadServiceRelationModel.bulkCreate(
          services.map((serviceId) => ({ serviceId, leadId })),
          { transaction },
        );
      }

      await transaction.commit();
      return leadInstance;
    } catch (error) {
      await transaction.rollback();
      if (!(error instanceof APIError)) {
        throw new APIError(
          ERRORS.LEAD.CREATE_ERROR,
          { originalError: error },
        );
      }
      throw error;
    }
  }

  static async closeLeadAfterRegistration({ leadId, clientId, transaction }) {
    const leadInstance = await LeadModel.findByPk(leadId, {
      attributes: ['id', 'leadStatusId', 'clientId'],
    });

    if (!leadInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(leadId));

    leadInstance.set({ clientId, leadStatusId: LeadStatusModel.STATUSES.NEED_SATISFIED });

    await leadInstance.save({ transaction });

    return leadInstance;
  }
}
