import { jsonToHTMLTable } from 'nested-json-to-table';
import moment from 'moment';
import OrganizationService from './OrganizationService';
import DeviceService from './DeviceService';
import { DATE_FORMAT } from '../constants';
import { addressToString } from '../utils/prepareResponse';
import { ServiceModel, TariffModel } from '../database/models/Tariff';


export default class MailService {
  /**
   * prepareInvoice
   * @async
   * @static
   * @param {Number} orderNumber номер заказа
   * @param {String} clientId UUID организации
   * @param {Object.<Date>} createdAt Дата создания
   * @param {Array} [clientServices] Приобретённые услуги
   * @param {Number} clientServices.cost Цена услуги
   * @param {String} [clientServices.service.name] Название услуги
   * @param {String} [clientServices.name] Название услуги
   * @param {String} clientServices.clientDeviceId Оборудование услуги
   * @param {Array} [clientTariffs] Приобретённые тарифы
   * @param {Number} clientTariffs.cost Цена тарифа
   * @param {String} [clientTariffs.tariff.name] Название тарифа
   * @param {String} [clientTariffs.name] Название тарифа
   * @param {String} clientTariffs.clientDeviceId Оборудование тарифа
   * @returns {Promise<{client: {inn: string, name: string}, tariff: Array | null, services: Array | null, totalCost: number}>}
   * @example return
   * {
   *   totalCost: 5444,
   *   client: {
   *     inn: '123123123',
   *     name: 'ИП Васильевич',
   *   },
   *   services: [
   *     {
   *       name: 'Установка онлайн-кассы в торгово-сервисном предприятии',
   *       price: 500,
   *       count: 2,
   *       cost: 1000,
   *       device: 'b2304eb7-812e-4073-8a1d-5224492bb226',
   *       management: [
   *         {
   *           name: 'Управление по сервису Волго-Вятского Банка',
   *           code: '000001175',
   *         },
   *         {
   *           name: 'Управление по сервису Сибирского банка',
   *           code: '000001203',
   *         },
   *       ],
   *     },
   *   ],
   *   tariff: [
   *     {
   *       name: 'Удаленная помощь на год',
   *       price: 4444,
   *       count: 1,
   *       cost: 4444,
   *       device: 'a63b5868-2389-4f82-bd49-117ccbf17545',
   *       management: [
   *         {
   *           name: 'Управление по сервису Поволжского Банка',
   *           code: '000001418',
   *         },
   *       ],
   *     },
   *   ],
   * }
   */
  static async prepareInvoice({
    orderNumber, organizationId, createdAt, clientServices, clientTariffs,
  }) {
    const { name, inn } = await OrganizationService.getShortInfo(organizationId, ['name', 'inn']);
    const devices = new Set();
    let totalCost = 0;

    const addToCollection = (servicesOrTariffs) => {
      servicesOrTariffs.forEach((serviceOrTariff) => {
        devices.add(serviceOrTariff.clientDeviceId);
      });
    };

    addToCollection(clientServices);
    addToCollection(clientTariffs);

    const managements = new Map();

    await Promise.all([...devices].map(async (deviceId) => {
      const {
        serviceAddress: { cityRelationship },
      } = await DeviceService.getManagementServices(deviceId);

      const { managementService: management } = cityRelationship || {};

      managements.set(deviceId, {
        name: management.name,
        code: management.code,
        deviceId,
      });
    }));

    const prepareResult = (servicesOrTariffs, type) => servicesOrTariffs.reduce((acc, current) => {
      const management = managements.get(current.clientDeviceId);
      const currentName = current.name || current[type]?.name;

      const newService = {
        name: currentName,
        price: current.cost,
        count: 1,
        cost: current.cost,
        device: current.clientDeviceId,
        management: [{
          name: management.name,
          code: management.code,
        }],
      };

      totalCost += current.cost;

      if (!acc.length) {
        acc.push(newService);
      } else {
        const existingService = acc.find((service) => service.name === currentName);
        if (existingService) {
          existingService.count += 1;
          existingService.cost += existingService.price;
          if (!existingService.management.some((mngt) => mngt.name === management.name)) {
            existingService.management.push({
              name: management.name,
              code: management.code,
            });
          }
        } else {
          acc.push(newService);
        }
      }
      return acc;
    }, []);

    const services = prepareResult(clientServices, 'service');
    const tariffs = prepareResult(clientTariffs, 'tariff');

    return {
      order: {
        totalCost,
        createdAt: moment(createdAt).format(DATE_FORMAT.KEBAB_NORMAL),
        number: orderNumber,
      },
      client: {
        inn,
        name,
      },
      services,
      tariffs,
    };
  }

  /**
   *
   * @param {Object.<Date>} [paymentDate] Дата оплаты
   * @param {Array} [clientServices] Приобретённые услуги
   * @param {Number} clientServices.cost Цена услуги
   * @param {String} [clientServices.service.name] Название услуги
   * @param {String} [clientServices.name] Название услуги
   * @param {String} clientServices.clientDeviceId Оборудование услуги
   * @param {Array} [clientTariffs] Приобретённые тарифы
   * @param {Number} clientTariffs.cost Цена тарифа
   * @param {String} [clientTariffs.tariff.name] Название тарифа
   * @param {String} [clientTariffs.name] Название тарифа
   * @param {String} clientTariffs.clientDeviceId Оборудование тарифа
   * @return {Promise<void>}
   * @example return
   * {
   *   totalCost: 5444,
   *   goods: [
   *     name: 'Установка онлайн-кассы в торгово-сервисном предприятии',
   *     device: 'Атол 110 (серийный номер)',
   *     address: '165884, Калужская обл, пос. Ярнок, д.17, под.181'
   *     cost: 1000,
   *     expirationAt: 22.11.2025 | undefined
   *   ]
   * }
   */
  static async prepareInvoiceForUser({
    clientTariffs,
    clientServices,
    paymentDate,
  }) {
    const devices = new Set();
    let totalCost = 0;

    const addToCollection = (servicesOrTariffs) => {
      servicesOrTariffs.forEach((serviceOrTariff) => {
        totalCost += serviceOrTariff.cost
          || TariffModel.getCurrentCost(serviceOrTariff.tariff?.costs)
          || ServiceModel.getCurrentCost(serviceOrTariff.service?.costs);
        if (serviceOrTariff.clientDeviceId) {
          devices.add(serviceOrTariff.clientDeviceId);
        }
      });
    };

    addToCollection(clientServices);
    addToCollection(clientTariffs);

    const preparedDevices = new Map();

    await Promise.all([...devices].map(async (deviceId) => {
      const { address, model, serialNumber } = await DeviceService.getClientDevice(deviceId);
      preparedDevices.set(deviceId, {
        address: addressToString({ ...address, subject: address.region }),
        modelAndSerial: `${model} (${serialNumber})`,
        deviceId,
      });
    }));

    const prepareResult = (servicesOrTariffs) => servicesOrTariffs.map((serviceOrTariff) => {
      const preparedDevice = preparedDevices.get(serviceOrTariff.clientDeviceId);

      let expirationAt = '';

      if (paymentDate) {
        if (serviceOrTariff.expirationAt) {
          expirationAt = moment(serviceOrTariff.expirationAt).format(DATE_FORMAT.DOT_SHORT);
        } else if (serviceOrTariff.service.id === ServiceModel.SERVICES.ENGINEER_VISIT) {
          expirationAt = `Использовать услугу можно с ${moment(paymentDate).format(DATE_FORMAT.DOT_SHORT)}. 
          Услуга оказывается в течение 2 рабочих дней с момента регистрации запроса`;
        } else if (serviceOrTariff.service.id === ServiceModel.SERVICES.EMERGENCY_ENGINEER_VISIT) {
          expirationAt = `Использовать услугу можно с ${moment(paymentDate).format(DATE_FORMAT.DOT_SHORT)}. 
          Услуга оказывается в течение 6 рабочих часов с момента регистрации запроса`;
        } else {
          expirationAt = `Использовать услугу можно с ${moment(paymentDate).format(DATE_FORMAT.DOT_SHORT)}.`;
        }
      }

      return {
        name: serviceOrTariff.name || serviceOrTariff.tariff?.name || serviceOrTariff.service?.name,
        cost: serviceOrTariff.cost || serviceOrTariff.tariff?.cost || serviceOrTariff.service?.cost,
        devices: preparedDevice?.modelAndSerial || null,
        address: preparedDevice?.address || null,
        expirationAt,
      };
    });

    return {
      totalCost,
      goods: [...prepareResult(clientTariffs), ...prepareResult(clientServices)],
    };
  }

  /**
   *
   * @param {Array.<Object>} servicesOrTariffs Массив разовых услуг и тарифов
   * @param {String} servicesOrTariffs.name Называние тарифа или услуги
   * @param {Number} servicesOrTariffs.cost Цена тарифа или услуги
   * @param {String} servicesOrTariffs.devices Модель с серийным номером
   * @param {String} servicesOrTariffs.address Адрес оборудования
   * @param {String} [servicesOrTariffs.expirationAt] Время до которого можно воспользоваться тарифом или услугой. Если такого ограничения нет, то с которого момента можно воспользоваться. Только для оплаченных товаров.
   * @returns {Array}
   * @description Создает таблицу на основе объекта.
   *      Названия колонок таблицы создается из имен свойств объектов.
   */
  static createInvoiceTemplate(servicesOrTariffs) {
    return jsonToHTMLTable(servicesOrTariffs.map((serviceOrTariff) => ({
      Наименование: serviceOrTariff.name,
      Оборудование: serviceOrTariff.devices,
      'Адрес оборудования': serviceOrTariff.address,
      ...(serviceOrTariff.expirationAt && { 'Сроки оказания': serviceOrTariff.expirationAt }),
      Стоимость: serviceOrTariff.cost,
    })));
  }
}
