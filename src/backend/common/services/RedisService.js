import { promisify } from 'util';

import redis from 'redis';

import { REDIS_URL } from '../constants/env';
import { Logger } from './LoggerService';

const logger = new Logger('REDIS-SERVICE');

const METHODS = ['get', 'set', 'del'];

export default class RedisService {
  #client = null;

  constructor() {
    this.#client = redis.createClient(REDIS_URL);
    this.#client.on('error', (error) => {
      logger.error(error.message);
    });
    /* Промисифицируем нужные нам методы, используя нативный util */
    METHODS.forEach((method) => {
      this[`${method}Async`] = promisify(this.#client[method]).bind(this.#client);
    });
  }

  getQuery(query) {
    return this.getAsync(query);
  }

  delQuery(query) {
    return this.delAsync(query);
  }

  setQuery(query, data, expiresIn) {
    const params = [query, data];
    if (expiresIn) {
      params.push('EX', expiresIn);
    }
    return this.setAsync(...params);
  }

  get client() {
    return this.#client;
  }
}
