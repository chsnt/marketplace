import moment from 'moment';
import { UniqueConstraintError, Sequelize, Op } from 'sequelize';

import db from '../database';

import { UserModel, UserGroupModel } from '../database/models/User';
import OldPasswordHashModel from '../database/models/OldPasswordHashModel';

import APIError from '../utils/APIError';
import ERRORS from '../constants/errors';
import { generateNewPassword, getPasswordHash } from '../utils/auth';
import { organization as prepareOrganization } from '../utils/prepareResponse';
import { DATE_FORMAT, GET_LIST_DEFAULT_LIMIT, GET_LIST_DEFAULT_SORT } from '../constants';
import is from '../libs/is';
import HelperService from './HelperService';


const ATR_ORGANIZATION = ['city', 'street', 'house', 'apartment', 'postalCode', 'fiasId'];
const ORGANIZATION = 'organization';
const LEGAL_ADDRESS = 'legalAddress';
const REAL_ADDRESS = 'realAddress';
const BANK_DETAIL = 'bankDetail';
const TAX_TYPE = 'taxType';
const BANK = 'bank';
const EDO_DESCRIPTION = 'electronicDocumentCirculationDescription';
const ELECTRONIC_DOCUMENT_CIRCULATION = 'electronicDocumentCirculation';
const CITY_RELATIONSHIP = 'cityRelationship';
const FEDERATION_SUBJECT = 'federationSubject';

const EXCLUDE_ATTRIBUTES = [
  'createdAt', 'updatedAt', 'deletedAt',
  'createdBy', 'updatedBy', 'created_by', 'updated_by',
];

const prepareReturnInfo = (data) => {
  const { organization, ...user } = data.toJSON();

  user.birthday = (user.birthday) ? moment(user.birthday).format(DATE_FORMAT.DOT_SHORT) : null;

  return {
    user: {
      id: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      patronymicName: user.patronymicName,
      email: user.email,
      phone: user.phone,
      birthday: user.birthday,
    },
    organization: organization ? prepareOrganization(organization) : undefined,
  };
};

export default class {
  /**
   * @param {string} address Адрес в виде строки '181664, Россия, г.Москва, г.Москва, ул. Ленина, д.4, кор/стр.1, оф.995'
   * @returns {{house: (string|string)}}
   * @description Если в строка не вписывается в шаблон, то возвращается объект с пустой улицей и домом.
   */
  static separateAddress = (address) => {
    const addressArray = address.split(',');
    const properties = new Map([
      /* деструктуризация не подойдет, т.к. важно соблюдать последовательность элементов, для сепарирования адреса */
      [7, ['postalCode', 'country', 'region', 'city', 'street', 'house', 'apartment']],
      [8, ['postalCode', 'country', 'region', 'city', 'street', 'house', 'kor', 'apartment']],
    ]);

    const template = properties.get(addressArray.length);

    const data = template?.reduce((result, current, index) => {
      result[current] = addressArray[index];
      return result;
    }, {});

    return {
      ...data,
      house: (addressArray.length === 8) ? `${data.house} ${data.kor}` : data.house,
    };
  };

  static async genNewPassword(userId) {
    const newPassword = generateNewPassword();
    if (await OldPasswordHashModel.isPasswordAlreadyUsed({ userId, password: newPassword })) {
      await this.genNewPassword(userId);
    }
    return newPassword;
  }

  static async getInfo(id) {
    const ATR_BANK_TYPE = ['id', 'name', 'alias'];
    const data = await UserModel.findByPk(id, {
      attributes: [
        'firstName',
        'lastName',
        'patronymicName',
        'email',
        'phone',
        'birthday',
      ],
      include: [
        {
          association: ORGANIZATION,
          attributes: {
            exclude: [
              ...EXCLUDE_ATTRIBUTES, 'realAddressId', 'legalAddressId', 'electronicDocumentCirculationId',
              'real_address_id', 'legal_address_id', 'electronic_document_circulation_id',
            ],
          },
          include: [
            {
              association: LEGAL_ADDRESS,
              attributes: ATR_ORGANIZATION,
              include: [
                {
                  association: CITY_RELATIONSHIP,
                  attributes: ['id', 'name'],
                },
                {
                  association: FEDERATION_SUBJECT,
                  attributes: ['id', 'name'],
                },
              ],
            },
            { association: ELECTRONIC_DOCUMENT_CIRCULATION, attributes: ['id', 'name', 'alias'] },
            { association: EDO_DESCRIPTION, attributes: ['description'] },
            {
              association: REAL_ADDRESS,
              attributes: ATR_ORGANIZATION,
              include: [
                {
                  association: CITY_RELATIONSHIP,
                  attributes: ['id', 'name'],
                },
                {
                  association: FEDERATION_SUBJECT,
                  attributes: ['id', 'name'],
                },
              ],
            },
            {
              association: BANK_DETAIL,
              attributes: ['bankAccount'],
              include: [
                { association: TAX_TYPE, attributes: ATR_BANK_TYPE },
                { association: BANK, attributes: ['id', 'name', 'correspondentAccount', 'rcbic'] },
              ],
            },
          ],
        },
      ],
    });


    return prepareReturnInfo(data);
  }

  static async updateInfo({
    user: { id, ...user },
    organization: organizationData,
    isTemporaryPassword,
  }) {
    const ATR_ORGANIZATION_UPD = ['id', ...ATR_ORGANIZATION];

    const transaction = await db.sequelize.transaction();

    try {
      if (user?.password && await OldPasswordHashModel.isPasswordAlreadyUsed({
        userId: id, password: user.password, transaction,
      })) {
        throw new APIError(ERRORS.USER.PASSWORD_REPEAT_ERROR);
      }

      const userInstance = await UserModel.findByPk(id, {
        attributes: { exclude: EXCLUDE_ATTRIBUTES },
        include: [
          {
            association: ORGANIZATION,
            attributes: { exclude: ['realAddressId', 'legalAddressId', 'organizationId', ...EXCLUDE_ATTRIBUTES] },
            include: [
              {
                association: LEGAL_ADDRESS,
                attributes: ATR_ORGANIZATION_UPD,
                include: [
                  CITY_RELATIONSHIP,
                  FEDERATION_SUBJECT,
                ],
              }, {
                association: REAL_ADDRESS,
                attributes: ATR_ORGANIZATION_UPD,
                include: [
                  CITY_RELATIONSHIP,
                  FEDERATION_SUBJECT,
                ],
              },
              {
                association: ELECTRONIC_DOCUMENT_CIRCULATION,
                attributes: {
                  exclude: EXCLUDE_ATTRIBUTES,
                },
              },
              { association: EDO_DESCRIPTION, attributes: { exclude: EXCLUDE_ATTRIBUTES } },
              {
                association: BANK_DETAIL,
                attributes: ['id', 'bankAccount'],
                include: [
                  {
                    association: TAX_TYPE,
                    attributes: {
                      exclude: EXCLUDE_ATTRIBUTES,
                    },
                  },
                  {
                    association: BANK,
                    attributes: {
                      exclude: EXCLUDE_ATTRIBUTES,
                    },
                  },
                ],
              },
            ],
          },
        ],
        transaction,
      });

      if (!userInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(id));


      if (user && Object.keys(user).length) {
        if (user.password && !isTemporaryPassword && !await userInstance.authenticate(user.oldPassword)) {
          throw new APIError(ERRORS.USER.OLD_PASSWORD);
        }

        if (user.password) {
          await OldPasswordHashModel.create({
            password: user.password,
            userId: userInstance.id,
          }, {
            transaction,
          });
        }

        await userInstance.update(user, { transaction });
      }

      if (organizationData) {
        const {
          bankDetail,
          legalAddress,
          realAddress,
          electronicDocumentCirculationId: edoId,
          electronicDocumentCirculationDescription: edoDescription,
          ...organization
        } = organizationData;
        await userInstance.organization.update({
          ...organization,
          electronicDocumentCirculationId: edoId,
          digitalSignatureFrom: organization?.digitalSignature?.from,
          digitalSignatureTo: organization?.digitalSignature?.to,
        }, { transaction });

        if (legalAddress) {
          await userInstance.organization.legalAddress.update({
            ...legalAddress,
            ...(legalAddress.city && { cityId: null }),
          }, { transaction });
        }

        if (realAddress) {
          await userInstance.organization.realAddress.update({
            ...realAddress,
            ...(realAddress.city && { cityId: null }),
          }, { transaction });
        }

        if (edoId) {
          if (edoDescription && is.electronicDocumentCirculation.notInList(edoId)) {
            await userInstance.organization.createElectronicDocumentCirculationDescription({
              electronicDocumentCirculationId: edoId,
              description: edoDescription,
            }, { transaction });
          } else {
            await userInstance.organization.electronicDocumentCirculationDescription?.destroy({ transaction });
          }
        }

        if (bankDetail) {
          await userInstance.organization.bankDetail.update({
            ...(bankDetail.taxType && { taxTypeId: bankDetail.taxType }),
            ...(bankDetail.bankAccount && { bankAccount: bankDetail.bankAccount }),
          }, { transaction });

          await userInstance.organization.bankDetail.bank?.update({
            name: bankDetail.name,
            correspondentAccount: bankDetail.correspondentAccount,
            rcbic: bankDetail.rcbic,
          }, { transaction });
        }
      }

      const result = prepareReturnInfo(userInstance);

      await transaction.commit();

      return result;
    } catch (error) {
      await transaction.rollback();
      if (error instanceof UniqueConstraintError) {
        const errorObject = UserModel.uniqueConstraintError(error?.original?.constraint);
        throw new APIError(errorObject, { originalError: error });
      }
      if (error instanceof APIError) {
        throw error;
      }
      throw new APIError(ERRORS.USER.UPDATE_USER, { originalError: error });
    }
  }

  /**
   * @async
   * @param {String} userId UUID пользователя
   * @param {String} newPassword новый пароль
   * @returns {Promise<void>}
   * @description записывает прежний пароль в OldPasswordHash и меняет у пользователя на новый
   */
  static async resetPassword(userId, newPassword) {
    const transaction = await db.sequelize.transaction();

    try {
      await OldPasswordHashModel.create({
        password: newPassword,
        userId,
      }, {
        transaction,
      });

      await UserModel.update(
        { password: getPasswordHash(newPassword) },
        { where: { id: userId }, transaction },
      );
      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }

  static async getByEmailOrPhone(emailOrPhone) {
    const user = await UserModel.findByEmailOrPhoneOrLogin(emailOrPhone);
    if (!user) {
      throw new APIError(ERRORS.AUTH.USER_NOT_FOUND);
    }
    return user.id;
  }

  /**
   *
   * @param id UUID пользователя
   * @return {Promise<UserModel> | Promise<UserModel | null>}
   */
  static getUserData({ emailOrPhone }) {
    return UserModel.findOne({
      where: {
        [db.Sequelize.Op.or]: [
          { email: emailOrPhone },
          { login: emailOrPhone },
          { phone: HelperService.normalizePhone(emailOrPhone) },
        ],
      },
      attributes: ['clientId'],
      include: [{
        association: 'organization',
        attributes: ['clientId'],
      }],
    });
  }

  static getUserGroups() {
    return UserGroupModel.findAll({
      attributes: ['id', 'name', 'alias'],
    });
  }

  /**
   *
   * @param {String} search Полнотекстовый поиск по ФИО
   * @param {String} userGroupId UUID роли
   * @param {String} type Тип роли (пока не используется)
   * @param {String} sortByDate Сортировка по дате. `asc` или `desc`
   * @param {Number} offset Сдвиг
   * @param {Number} limit Кол-во записей
   * @returns {Promise<UserModel[]> | []}
   */
  static async getUsers({
    search,
    userGroupId,
    type,
    sortByDate = GET_LIST_DEFAULT_SORT,
    offset = 0,
    limit = GET_LIST_DEFAULT_LIMIT,
  }) {
    const searchAttribute = Sequelize.where(
      Sequelize.fn(
        'concat',
        Sequelize.col('first_name'),
        Sequelize.col('patronymic_name'),
        Sequelize.col('last_name'),
      ),
      { [Sequelize.Op.iLike]: `%${search}%` },
    );

    const userType = userGroupId || type;

    const userGroupFilter = new Map([
      ['any', { }],
      ['admin', { '$groups.id$': UserGroupModel.ACCESS_TO_ADMIN_PANEL }],
      ['user', { '$groups.id$': { [Op.is]: null } }],
      [userGroupId, { '$groups.id$': userGroupId }],
    ]);

    const result = await UserModel.findAndCountAll({
      offset,
      limit,
      subQuery: false,
      paranoid: false,
      where: {
        ...(userType && userGroupFilter.get(userType)),
        ...(search && { $and: searchAttribute }),
      },
      include: {
        association: 'groups',
        attributes: ['id'],
      },
      attributes: [
        'id',
        'firstName',
        'lastName',
        'patronymicName',
        'email',
        'phone',
        'createdAt',
        'deletedAt',
      ],
      order: [['createdAt', sortByDate]],
    });

    return {
      ...result,
      rows: result.rows.map((userInstance) => ({
        ...userInstance.toJSON(),
        groups: undefined,
      })),
    };
  }

  static getClientMaintainer({ clientId, transaction, attributes }) {
    return UserModel.findOne({
      where: {
        [Op.or]: [
          {
            [Op.and]: {
              isMaintainer: true,
              clientId,
            },
          },
          {
            [Op.and]: {
              isMaintainer: true,
              '$organization.client_id$': clientId,
            },
          },
        ],
      },
      include: [{
        association: 'organization',
        attributes: ['id'],
      }],
      ...(attributes && { attributes }),
      transaction,
    });
  }

  /**
   *
   * @param id
   * @param firstName
   * @param lastName
   * @param patronymicName
   * @param email
   * @param phone
   * @param birthday
   * @return {Promise<void>}
   */
  static async updateData({
    id, delete: userDelete, ...user
  }) {
    const transaction = await db.sequelize.transaction();
    try {
      const userInstance = await UserModel.findByPk(id, {
        paranoid: false,
        attributes: { exclude: EXCLUDE_ATTRIBUTES },
        transaction,
      });

      if (!userInstance) throw new APIError(ERRORS.ENTITY.OBJECT_NOT_FOUND(id));

      if (userDelete) {
        await userInstance.destroy({ transaction });
      } else {
        await userInstance.update(user, { transaction });
        await userInstance.restore({ transaction });
      }

      await transaction.commit();

      return userInstance;
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        const errorObject = UserModel.uniqueConstraintError(error?.original?.constraint);
        throw new APIError(errorObject, { originalError: error });
      }
      if (error instanceof APIError) {
        throw error;
      }
      throw new APIError(ERRORS.USER.UPDATE_USER, { originalError: error });
    }
  }
}
