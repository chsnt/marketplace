export default class HelperService {
  static normalizePhone = (value) => (value ? value.replace(/\D/g, '').replace(/^(\+?7|8)?(9\d{9})$/i, '7$2') : value); // eslint-disable-line max-len

  /* Создание карты ключ-значение из массива без повторяющихся ключей */
  static createMapFromArray = (array = [], keyField = 'id') => array.reduce(
    (result, current) => {
      Object.assign(result, { [current[keyField]]: current });
      return result;
    },
    {},
  );

  /* Используется для конвертации входящих массивов данных при импорте */
  static idToExternalIdConverter(rows = []) {
    return rows.map(({ id: externalId, ...rest }) => ({ ...rest, externalId, alias: externalId }));
  }

  /* Метод проверки наличия дубликатов в массиве из примитивных типов данных */
  static hasDuplicates(rows = []) {
    return new Set(rows).size !== rows.length;
  }
}
