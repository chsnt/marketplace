import fs from 'fs';
import path from 'path';
import { SBBID } from '../constants/env';

export default class FileService {
  static createDirIfNotExists(dir) {
    if (!fs.existsSync(dir)) fs.mkdirSync(dir, { recursive: true });
  }

  /* Возможно, process.cwd() стоит поменять на какой нибудь хардкод или process.env.APP_DIRECTORY */
  static DEFAULT_FILE_PATH = path.join(process.cwd(), 'files');

  static DEFAULT_CERTIFICATE_PATH = path.join(process.cwd(), SBBID.CERTIFICATE_PATH);

  static renameFile({ dir, oldName, newName }) {
    const oldNamePath = path.join(dir, oldName);

    const { extension } = this.getFilenameWithExtension(oldNamePath);

    if (!fs.statSync(oldNamePath).isFile()) return;

    const newNamePath = path.join(dir, `${newName}${extension ? `.${extension}` : ''}`);

    fs.renameSync(oldNamePath, newNamePath, (err) => {
      if (err) throw err;
    });
  }

  static removeTimestamp(name) {
    return name.replace(/^-*(\w+)-/, '');
  }

  static removeExtension(name) {
    return name.replace(/\..+$/, '');
  }

  static getFilenameWithExtension = (filename) => {
    const extension = filename.split('.').pop();
    const hasExtension = extension !== filename;
    const name = hasExtension ? filename.slice(0, filename.length - (extension.length + 1)) : filename;
    return { name, extension: hasExtension ? extension : '' };
  };

  static getFiles = (dir) => {
    const result = [];
    const list = fs.readdirSync(dir);
    list.forEach((file) => {
      const filePath = `${dir}/${file}`;
      const stat = fs.statSync(filePath);
      if (stat && stat.isDirectory()) {
        result.push(...this.getFiles(filePath));
      } else {
        result.push(filePath);
      }
    });
    return result;
  };

  /**
   * @description Создание дефолтных директорий для файлов и TLS сертификатов
   */
  static initMainFileFolders() {
    this.createDirIfNotExists(this.DEFAULT_FILE_PATH);
    this.createDirIfNotExists(this.DEFAULT_CERTIFICATE_PATH);
  }

  static pathExists(entityPath) {
    return fs.existsSync(entityPath);
  }

  static removeFile(pathToFile) {
    return fs.unlinkSync(pathToFile);
  }
}
