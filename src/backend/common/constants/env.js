export const TITLE = process.env.TITLE || 'ИТ-Услуга.Ру';

export const RESET_PASS_TIMEOUT = parseInt(process.env.RESET_PASS_TIMEOUT || '1', 10) * 60;

export const ASYNC_POOL_CONCURRENCY = process.env.ASYNC_POOL_CONCURRENCY || 100;

export const SENDPULSE = {
  USER_ID: process.env.SENDPULSE_USER_ID || '',
  SECRET: process.env.SENDPULSE_SECRET || '',
  TOKEN_STORAGE: process.env.SENDPULSE_TOKEN_STORAGE || '/tmp/',
  SENDER_NAME: process.env.SENDPULSE_SENDER_NAME || 'itusluga.ru',
  SENDER_EMAIL: process.env.SENDPULSE_SENDER_EMAIL || 'noreply@itusluga.ru',
  MAIL_RECIPIENT: process.env.MAIL_RECIPIENT || '',
  MAIL_RECIPIENT_CLIENT_MANAGER: process.env.MAIL_RECIPIENT_CLIENT_MANAGER || '',
  IS_MAIL_SENDING_ENABLED: process.env.ENABLE_MAIL_SENDING === 'true',
};

export const REDIS_URL = process.env.REDIS_URL || 'redis://localhost:6379/0';

export const MIN_PASSWORD_LENGTH = parseInt(process.env.MIN_PASSWORD_LENGTH || '6', 10);

export const GENERATED_PASSWORD_PATTERN = process.env.GENERATED_PASSWORD_PATTERN || '0Aa!';

export const USED_PASSWORDS_LIMIT = process.env.USED_PASSWORDS_LIMIT || 4;

export const COOKIE_SECURE = process.env.COOKIE_SECURE ? process.env.COOKIE_SECURE === 'true' : true;
export const COOKIE_HTTP_ONLY = process.env.COOKIE_HTTP_ONLY ? process.env.COOKIE_SECURE === 'true' : true;

export const ANTIVIRUS_HOST = process.env.ANTIVIRUS_HOST || 'localhost';
export const ANTIVIRUS_PORT = process.env.ANTIVIRUS_PORT || '3310';

export const ACTIVE_DIRECTORY = {
  url: process.env.AD_URL || '',
  baseDN: process.env.AD_BASE_DN || '',
  username: process.env.AD_USERNAME || '',
  password: process.env.AD_PASSWORD || '',
  domain: process.env.AD_DOMAIN || '',
};

export const INTEGRATOR = {
  IS_SENDING_ENABLED: process.env.ENABLE_INTEGRATION_SENDING === 'true',
  BASE_URL: process.env.INTEGRATOR_BASE_URL || 'http://192.168.67.36/UT_Test/hs/It_services',
  USERNAME: process.env.INTEGRATOR_USERNAME || 'ws_user',
  PASSWORD: process.env.INTEGRATOR_PASSWORD || '123qwe',
  RESOURCES: {
    CREATE_PARTNER: process.env.INTEGRATOR_RESOURCE_CREATE_PARTNER || 'CreatePartner',
    CREATE_BANK_ACCOUNT: process.env.INTEGRATOR_RESOURCE_CREATE_BANK_ACCOUNT || 'CreateBankAccount',
    CREATE_CONTRACT: process.env.INTEGRATOR_RESOURCE_CREATE_CONTRACT || 'CreateContract',
    CREATE_ORDER: process.env.INTEGRATOR_RESOURCE_CREATE_ORDER || 'CreateOrder',
    GENERATED_ORDER: process.env.INTEGRATOR_RESOURCE_GENERATED_ORDER || 'GeneratedOrder',
  },
};

export const SBBID = {
  AUTH_BASE_URL: process.env.SBBID_AUTH_BASE_URL || 'https://edupir.testsbi.sberbank.ru:9443',
  BASE_URL: process.env.SBBID_BASE_URL || 'https://edupirfintech.sberbank.ru:9443',
  CERTIFICATE: process.env.SBBID_CERTIFICATE || 'certificate.pem',
  CERTIFICATE_KEY: process.env.SBBID_CERTIFICATE_KEY || 'private-key.pem',

  RESOURCE_USER_INFO: process.env.SBBID_RESOURCE_USER_INFO || '/ic/sso/api/v1/oauth/user-info',
  RESOURCE_CRYPTO: process.env.SBBID_RESOURCE_CRYPTO || '/fintech/api/v1/crypto',
  RESOURCE_AUTHORIZATION_URL: process.env.SBBID_RESOURCE_AUTHORIZATION_URL || '/ic/sso/api/v2/oauth/authorize',
  RESOURCE_TOKEN_URL: process.env.SBBID_RESOURCE_TOKEN_URL || '/ic/sso/api/v2/oauth/token',
  RESOURCE_CHANGE_SECRET: process.env.SBBID_RESOURCE_CHANGE_SECRET || '/v1/change-client-secret',

  CALLBACK_URL: process.env.SBBID_CALLBACK_URL || 'https://marketplace-test.marketplace-service.ru/api/auth/callback-sbbid/',

  CLIENT_ID: process.env.SBBID_CLIENT_ID || '111191',
  CLIENT_SECRET: process.env.SBBID_CLIENT_SECRET || 'Ok08LMlL',

  CERTIFICATE_PATH: process.env.SBBID_CERTIFICATE_PATH || 'certificate',
};

export const SERVICE_DOMAIN = process.env.SERVICE_DOMAIN || 'http://localhost';

export const IS_SMS_SENDING_ENABLED = process.env.ENABLE_SMS_SENDING === 'true';
export const IS_PAYMENTS_ENABLED = process.env.ENABLE_PAYMENTS === 'true';

export const SUPPORT_EMAIL = process.env.SUPPORT_EMAIL || 'support@itusluga.ru';

export const SECURE_PAYMENTS_API = {
  URL: process.env.SECURE_PAYMENTS_API_URL || 'https://3dsec.sberbank.ru/payment/rest',
  USER: process.env.SECURE_PAYMENTS_API_USER || 'marketplace-service-api',
  PASSWORD: process.env.SECURE_PAYMENTS_API_PASSWORD || 'marketplace-service',
  GET_ORDER_STATUS: 'getOrderStatusExtended.do',
  REGISTER_ORDER: 'register.do',
};
