import httpStatus from 'http-status';
import { SUPPORT_EMAIL, USED_PASSWORDS_LIMIT } from './env';

const ERRORS = {
  COMMON: {
    SERVER_ERROR: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Неизвестная ошибка',
      errorCode: 'SERVER_ERROR',
    },
    ROUTE_NOT_FOUND: {
      statusCode: httpStatus.NOT_FOUND,
      error: 'Эндпоинт не найден',
      errorCode: 'ROUTE_NOT_FOUND',
    },
    VALIDATION_ERROR: (error) => ({
      statusCode: httpStatus.BAD_REQUEST,
      error,
      errorCode: 'VALIDATION_ERROR',
    }),
    UNAUTHORIZED: {
      statusCode: httpStatus.UNAUTHORIZED,
      error: 'Необходимо авторизоваться',
      errorCode: 'UNAUTHORIZED',
    },
    MAIL_ERROR: {
      statusCode: httpStatus.BAD_GATEWAY,
      error: 'Письмо не отправлено',
      errorCode: 'MAIL_ERROR',
    },
    RATE_LIMIT: (time) => ({
      statusCode: httpStatus.BAD_REQUEST,
      error: `Превышено максимальное количество запросов, пожалуйста, повторите через ${time} сек.`,
      errorCode: 'RATE_LIMIT',
    }),
  },
  CLIENT: {
    REGISTRATION_ERROR: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Ошибка регистрации клиента',
      errorCode: 'REGISTRATION_ERROR',
    },
    NOT_FULL_DATA: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Невозможно зарегистрировать клиента: нет данных ФЛ или ЮЛ/ИП',
      errorCode: 'NOT_FULL_DATA',
    },
  },
  FILE: {
    FILE_NOT_FOUND: (id) => ({
      statusCode: httpStatus.NOT_FOUND,
      error: `Файл объекта id='${id}' не найден`,
      errorCode: 'FILE_NOT_FOUND',
    }),
    FILE_IS_REQUIRED: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Отсутствует файл',
      errorCode: 'FILE_IS_REQUIRED',
    },
    FILE_TOO_LARGE: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Файл слишком большой',
      errorCode: 'FILE_TOO_LARGE',
    },
    FILE_UPLOAD_ERROR: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Не удалось загрузить файл. Попробуйте позже',
      errorCode: 'FILE_UPLOAD_ERROR',
    },
  },
  ANTIVIRUS: {
    VIRUS_FOUND: (filename) => ({
      statusCode: httpStatus.BAD_REQUEST,
      error: `Обнаружен вирус в файле ${filename}`,
      errorCode: 'VIRUS_FOUND',
    }),
  },
  RABBITMQ: {
    SMS_SERVICE_NOT_ALLOWED: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Сервис отправки SMS недоступен, попробуйте повторить операцию позже',
      errorCode: 'SMS_SERVICE_NOT_ALLOWED',
    },
  },
  ENTITY: {
    MODEL_NOT_FOUND: (model) => ({
      error: `Модель "${model}" не найдена`,
      errorCode: 'ModelNotFound',
    }),
    OBJECT_NOT_FOUND: (objectId) => ({
      error: `Объект ("${objectId}") не найден`,
      errorCode: 'ObjectNotFound',
    }),
    OBJECT_ID_VALIDATION: (objectId, fields) => ({
      error: fields
        ? `Следующие поля имеют неправильный формат: ${fields}`
        : `Неправильный тип поля id (id = "${objectId}")`,
      errorCode: 'ObjectIdValidation',
    }),
    REQUIRED_FIELDS: (fields = []) => ({
      error: fields.length === 0
        ? 'Отсутствуют необходимые поля для создания или обновления объекта'
        : `Следующие поля необходимы для создания объекта: ${fields}`,
      errorCode: 'RequiredFields',
    }),
    UNIQUE_CONSTRAINT: (objectName) => ({
      statusCode: httpStatus.BAD_REQUEST,
      error: `${objectName} с таким наименованием уже зарегистрирован в системе. Откорректируйте наименование`,
      errorCode: 'UniqueConstraint',
    }),
  },
  AUTH: {
    BAD_VERIFICATION_CODE: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Код подтверждения устарел или не найден, пожалуйста, запросите код повторно',
      errorCode: 'BAD_VERIFICATION_CODE',
    },
    INCORRECT_VERIFICATION_CODE: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Неверный код подтверждения',
      errorCode: 'INCORRECT_VERIFICATION_CODE',
    },
    LOGIN_AND_PASSWORD_SHOULD_NOT_BE_SAME: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Логин и пароль должны отличаться друг от друга',
      errorCode: 'LOGIN_AND_PASSWORD_SHOULD_NOT_BE_SAME',
    },
    NOT_FULL_CREDENTIALS: {
      statusCode: httpStatus.UNAUTHORIZED,
      error: 'Не указано имя пользователя или пароль',
      errorCode: 'NOT_FULL_CREDENTIALS',
    },
    USER_NOT_FOUND: {
      statusCode: httpStatus.UNAUTHORIZED,
      error: 'Пользователь с таким логином и паролем не найден',
      errorCode: 'USER_NOT_FOUND',
    },
    LOGIN_ERROR: {
      statusCode: httpStatus.UNAUTHORIZED,
      error: '',
      errorCode: 'LOGIN_ERROR',
    },
    RESET_PASS_TIMEOUT_ERROR: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Период сброса пароля не истек. Ожидайте 1 минуту',
      errorCode: 'ResetPassTimeoutError',
    },
    USER_IS_NOT_ADMIN: {
      statusCode: httpStatus.UNAUTHORIZED,
      error: 'Недостаточно прав доступа для к панели администратора',
      errorCode: 'USER_IS_NOT_ADMIN',
    },
    PHONE_ALREADY_REGISTERED: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Номер телефона уже зарегистрирован',
      errorCode: 'PHONE_ALREADY_REGISTERED',
    },
    RESET_PASS_BY_ADMIN: {
      statusCode: httpStatus.FORBIDDEN,
      error: `Для получения доступа свяжитесь со службой поддержки ${SUPPORT_EMAIL}`,
      errorCode: 'RESET_PASS_BY_ADMIN',
    },
    REGISTRATION_TOKEN_IS_EXPIRED: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Токен регистрации устарел, повторите попытку',
      errorCode: 'REGISTRATION_TOKEN_IS_EXPIRED',
    },
  },
  USER: {
    REGISTRATION_ERROR: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Ошибка регистрации, повторите попытку',
      errorCode: 'REGISTRATION_ERROR',
    },
    PASSWORD_REPEAT_ERROR: {
      statusCode: httpStatus.BAD_REQUEST,
      error: `Пароль не должен совпадать с ${USED_PASSWORDS_LIMIT} последними`,
      errorCode: 'PASSWORD_REPEAT_ERROR',
    },
    OLD_PASSWORD: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Текущий пароль некорректен',
      errorCode: 'OLD_PASSWORD',
    },
    UPDATE_USER: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Ошибка обновления данных пользователя',
      errorCode: 'UPDATE_USER',
    },
    PHONE_IS_INVALID: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Невалидный номер телефона. Проверьте правильность введенных данных',
      errorCode: 'PHONE_IS_INVALID',
    },
    EMAIL_IS_NOT_REGISTERED: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Электронная почта не зарегистрирована',
      errorCode: 'EMAIL_IS_NOT_REGISTERED',
    },
    MAIL_VERIFICATION_CODE_IS_FAILED: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Не удалось подтвердить адрес электронной почты',
      errorCode: 'MAIL_VERIFICATION_CODE_IS_FAILED',
    },
    DELETE_MAINTAINER: {
      statusCode: httpStatus.UNAUTHORIZED,
      error: 'Невозможно удалить пользователя т.к. он является мэйнтейнером организации',
      errorCode: 'DELETE_MAINTAINER',
    },
  },
  TICKET: {
    SERVICES_SHOULD_BE_UNIQUE: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Список услуг для создания или редактирования ЗНО должен быть уникальным',
      errorCode: 'SERVICES_SHOULD_BE_UNIQUE',
    },
    SERVICE_NOT_PURCHASED: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Услуга не была куплена или была использована ранее',
      errorCode: 'SERVICE_NOT_PURCHASED',
    },
    CREATE_TICKET: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Ошибка создания ЗНО',
      errorCode: 'CREATE_TICKET',
    },
    CANT_UPDATE_SERVICES: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Невозможно обновить состав услуг ЗНО в текущем ее статусе',
      errorCode: 'CANT_UPDATE_SERVICES',
    },
    UPDATE_TICKET: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Ошибка обновления ЗНО',
      errorCode: 'UPDATE_TICKET',
    },
    CREATE_REVIEW: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Не получилось создать отзыв для ЗНО',
      errorCode: 'CREATE_REVIEW',
    },
    TICKET_MUST_HAS_SERVICE_ADDRESS: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Для создания ЗНО необходимо указать адрес обслуживания',
      errorCode: 'TICKET_MUST_HAS_SERVICE_ADDRESS',
    },
  },
  INTEGRATION: {
    CREATE_DUPLICATE_INTEGRATOR: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Интегратор с таким именем или токеном уже существует',
      errorCode: 'CREATE_DUPLICATE_INTEGRATOR',
    },
    CREATE_INTEGRATOR: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Интегратор не создан',
      errorCode: 'CREATE_INTEGRATOR',
    },
    CREATE_PARTNER: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Контрагент не создан',
      errorCode: 'CREATE_PARTNER',
    },
    CREATE_BANK_ACCOUNT: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Банковский счет не добавлен к контрагенту',
      errorCode: 'CREATE_BANK_ACCOUNT',
    },
    UPDATE_DUPLICATE_INTEGRATOR: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Интегратор с таким именем или токеном уже существует',
      errorCode: 'UPDATE_DUPLICATE_INTEGRATOR',
    },
    UPDATE_INTEGRATOR: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Данные интегратора не обновлены',
      errorCode: 'CREATE_INTEGRATOR',
    },
  },
  ORDER: {
    CREATE_ORDER: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Ошибка создания заказа. Пожалуйста, попробуйте позже',
      errorCode: 'CREATE_ORDER',
    },
    PRODUCT_NOT_ALLOWED_FOR_DEVICE: ({ isTariff = true } = {}) => ({
      statusCode: httpStatus.BAD_REQUEST,
      error: `Для выбранного оборудования ${isTariff ? 'данный тариф недоступен' : 'данная услуга недоступна'}`,
      errorCode: 'PRODUCT_NOT_ALLOWED_FOR_DEVICE',
    }),
    PRODUCT_MUST_HAS_SERVICE_ADDRESS: ({ name } = {}) => ({
      statusCode: httpStatus.BAD_REQUEST,
      error: `Для выбранного продукта "${name}" необходимо указать адрес обслуживания на этапе формирования заказа`,
      errorCode: 'PRODUCT_MUST_HAS_SERVICE_ADDRESS',
    }),
    NOT_FOUND: {
      statusCode: httpStatus.NOT_FOUND,
      error: 'Заказ не найден',
      errorCode: 'ORDER_NOT_FOUND',
    },
  },
  ORGANIZATION: {
    REGISTRATION_ERROR: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Ошибка регистрации организации',
      errorCode: 'REGISTRATION_ERROR',
    },
    ORGANIZATION_EXISTS: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Указанные реквизиты уже зарегистрированы в системе. '
        + 'Для выхода в личный кабинет воспользуйтесь формой авторизации. '
        + 'При необходимости регистрации с указанными реквизитами свяжитесь с нашим call-центром',
      errorCode: 'ORGANIZATION_EXISTS',
    },
  },
  DEVICE: {
    CREATE_DEVICE: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Ошибка создания оборудования',
      errorCode: 'CREATE_DEVICE',
    },
    DELETE_ORGANIZATION_DEVICE: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Оборудование нельзя удалить, т.к. оно привязано к действующему тарифу или услуге',
      errorCode: 'DELETE_ORGANIZATION_DEVICE',
    },
  },
  SUPPORT_REQUEST: {
    CREATE_REVIEW: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Не получилось создать отзыв для обращения',
      errorCode: 'CREATE_REVIEW',
    },
  },
  LEAD: {
    CREATE_ERROR: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Ошибка создания лида',
      errorCode: 'CREATE_ERROR',
    },
  },
  TARIFF: {
    UPDATE_ERROR: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Ошибка обновления тарифа',
      errorCode: 'UPDATE_ERROR',
    },
  },
  SERVICE: {
    UPDATE_ERROR: {
      statusCode: httpStatus.INTERNAL_SERVER_ERROR,
      error: 'Ошибка обновления услуги',
      errorCode: 'UPDATE_ERROR',
    },
    DIFFERENT_TYPES_DEVICES: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'В списке услуг есть услуги с разными типами адреса обслуживания',
      errorCode: 'DIFFERENT_TYPES_DEVICES',
    },
    DISCOUNT_SERVICE_NOT_FOUND: {
      statusCode: httpStatus.BAD_REQUEST,
      error: 'Услуг, которые предоставляют скидку, не найдено',
      errorCode: 'SERVICE_NOT_FOUND',
    },
  },
  DISCOUNT: {
    COUPON: {
      NOT_FOUND: {
        statusCode: httpStatus.NOT_FOUND,
        error: 'Купон не найден',
        errorCode: 'COUPON_NOT_FOUND',
      },
      USED: {
        statusCode: httpStatus.BAD_REQUEST,
        error: 'Купон использован',
        errorCode: 'COUPON_USED',
      },
      FAILED_TO_APPLY: {
        statusCode: httpStatus.INTERNAL_SERVER_ERROR,
        error: 'Не удалось применить купон',
        errorCode: 'FAILED_TO_APPLY',
      },
      FAILED_TO_REMOVE: {
        statusCode: httpStatus.INTERNAL_SERVER_ERROR,
        error: 'Не удалось удалить купон',
        errorCode: 'FAILED_TO_REMOVE',
      },
    },
  },
};

export default ERRORS;
