import { TITLE } from './env';

export const GET_LIST_DEFAULT_LIMIT = 10;
export const GET_LIST_DEFAULT_SORT = 'desc';

export const PASSWORD_MESSAGE_TEMPLATE = `${TITLE}. Пароль для входа в личный кабинет:`;

export const TEMPORARY_PASSWORD_EXPIRE = 60 * 60 * 24;

export const VERIFICATION_MAIL = {
  LENGTH_CODE: 15,
  PATTERN: 'aA0',
  EXPIRES_IN: 60 * 60,
};

export const DATE_FORMAT = {
  KEBAB_FULL: 'YYYY-MM-DD HH:mm:ss',
  KEBAB_FULL_WITH_TIME_ZONE: 'YYYY-MM-DDTHH:mm:ssZ',
  KEBAB_NORMAL: 'YYYY-MM-DD HH:mm',
  KEBAB_SHORT: 'YYYY-MM-DD',
  KEBAB_FULL_T_DELIMITER: 'YYYY-MM-DDTHH:mm:ss',
  DOT_NORMAL: 'DD.MM.YYYY HH:mm',
  DOT_SHORT: 'DD.MM.YYYY',
};

export const ANTIVIRUS = {
  TIMEOUT: 10000,
  CHUNK_SIZE: 1024 * 1024,
};

export const TYPE_PAYMENT = {
  INVOICE: 'invoice',
  CARD: 'card',
};
