import RateLimit from 'express-rate-limit';
import RedisStore from 'rate-limit-redis';
import moment from 'moment';
import ERRORS from '../constants/errors';

import RedisService from '../services/RedisService';
import APIError from '../utils/APIError';

/**
 *
 * @param {Number} [timeout=60000] промежуток времени в миллисекундах за которое измеряются кол-во запросов
 * @param {Number} [max=429] максимальное кол-во запросов за указанный промежуток времени
 * @param {Boolean} [skipSuccessfulRequests=false] не учитывать запросы завершившиеся успехом
 * @param {Boolean} [skipFailedRequests=false] не учитывать запросы завершившиеся ошибкой
 * @returns {function(*=, *=, *=): (*|undefined)}
 */
export default ({
  timeout, max, skipSuccessfulRequests = false, skipFailedRequests = false,
}) => new RateLimit({
  store: new RedisStore({ client: new RedisService().client }),
  windowMs: timeout, // defaults to 60 * 1000 (1 minute)
  max,
  keyGenerator: (req) => req.sessionID,
  skipFailedRequests,
  skipSuccessfulRequests,
  handler: (req) => {
    const resetTime = moment(req.rateLimit.resetTime).diff(moment(), 'second');
    const ERRORS_RATE_LIMIT = ERRORS.COMMON.RATE_LIMIT(resetTime);
    throw new APIError(ERRORS_RATE_LIMIT);
  },
});
