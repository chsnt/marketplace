import axios from 'axios';
import path from 'path';
import http from 'http';
import AdmZip from 'adm-zip';
import IntegratorLogModel from '../database/models/IntegratorLogModel';
import APIError from '../utils/APIError';
import COMMON_ERRORS from '../constants/errors';
import { INTEGRATOR } from '../constants/env';

export default class IntegratorClient {
  #instance = null;

  #baseURL = '';

  /**
   * @param {String} baseURL Корневой url
   * @param {String} username Логин
   * @param {String} password Пароль
   */
  constructor({ baseURL, username, password }) {
    this.#instance = axios.create({
      baseURL,
      auth: {
        username,
        password,
      },
    });
    this.#baseURL = baseURL;
  }

  /**
   *
   * @param {String} resource url ресурса относительно корневого адреса
   * @param {Object} data Произвольные данные
   * @returns {Promise<Array>} Массив имен файлов, если метод вернул название фалов
   */
  async send({ resource, data }) {
    const logInstance = await IntegratorLogModel.create({
      resource: new URL(resource, this.#baseURL).href,
      request: data,
      method: 'POST',
    });

    try {
      let response = null;
      let fileNames = [];


      if (resource === INTEGRATOR.RESOURCES.CREATE_ORDER) {
        response = await this.#instance.post(resource, data, {
          responseType: 'arraybuffer',
        });

        if (response?.data) {
          const zip = new AdmZip(response.data);
          const zipEntries = zip.getEntries();

          fileNames = zipEntries.map((file) => file.entryName);

          const dataPath = path.join(process.cwd(), 'files', 'order');

          zip.extractAllTo(dataPath, true);
        }
      } else {
        response = await this.#instance.post(resource, data);
      }

      const {
        data: dataResponse,
        status,
        statusText,
        headers,
      } = response;

      await logInstance.update({
        status,
        response: {
          data: (resource === INTEGRATOR.RESOURCES.CREATE_ORDER) ? fileNames : dataResponse,
          statusText,
          headers,
        },
      });

      return fileNames;
    } catch (error) {
      const response = (error.isAxiosError) ? {
        ...error.toJSON(),
        /* если в data приходит инстанс IncomingMessage возникает ошибка парсинга и в лог записывается как HPE_INVALID_HEADER_TOKEN */
        data: (error.response?.data instanceof http.IncomingMessage) ? null : error.response?.data,
        statusText: error.response?.statusText,
        responseHeaders: error.response?.headers,
      } : {
        ...error,
      };

      if (logInstance) {
        await logInstance.update({
          status: error.response?.status,
          response,
        });
      }

      throw new APIError(COMMON_ERRORS.INTEGRATION.CREATE_PARTNER);
    }
  }
}
