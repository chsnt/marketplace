import sendpulse from 'sendpulse-api';

import logger from '../services/LoggerService';
import APIError from '../utils/APIError';
import COMMON_ERRORS from '../constants/errors';
import { SENDPULSE, SERVICE_DOMAIN } from '../constants/env';


const from = {
  name: SENDPULSE.SENDER_NAME,
  email: SENDPULSE.SENDER_EMAIL,
};

const commonVariables = {
  site: new URL(SERVICE_DOMAIN).href,
  urlSignin: new URL('signin', SERVICE_DOMAIN).href,
  urlPrivateOffice: new URL('private-office', SERVICE_DOMAIN).href,
};

class MailClient {
  constructor() {
    logger.debug('MailClient init');
    logger.debug(`${SENDPULSE.USER_ID}, ${SENDPULSE.SECRET}, ${SENDPULSE.TOKEN_STORAGE}`);
    sendpulse.init(SENDPULSE.USER_ID, SENDPULSE.SECRET, SENDPULSE.TOKEN_STORAGE);

    /* util.promisify не заработал */
    this.smtpSendMail = (data) => new Promise((resolve) => {
      sendpulse.smtpSendMail(resolve, data);
    });
  }

  /**
   *
   * @param {Object} [template] шаблон
   * @param {Number} template.id номер шаблона
   * @param {Object} template.variables Данных для шаблона
   * @param {String} subject заголовок письма
   * @param {Array.<Object>} recipients Массив с получателями
   * @param {String} [recipients.name] Имя пользователя
   * @param {String} recipients.email Почта
   * @param {String} [text] Текст сообщения
   * @param {String} [html] html версия письма, закодированная в base64
   * @param {Object} [attachments] серриализованный объект, в котором ключ - это имя файла, а значение - содержание файла (необязательный параметр) {"название":"содержимое"}
   * @param {Object} [attachmentsBinary] серриализованный объект, в котором ключ - это имя файла, а значение - содержание файла,  закодированная в base64 (необязательный параметр)
   * @returns {Promise<void>}
   */
  async sendMessage({
    template, subject, recipients,
    text, html, attachments, attachmentsBinary,
  }) {
    logger.debug({
      template, subject, from, to: recipients, text, html, attachments_binary: attachmentsBinary, attachments,
    });
    const result = await this.smtpSendMail({
      template, subject, from, to: recipients, text, html, attachments_binary: attachmentsBinary, attachments,
    });

    if (result.error || result.error_code || result.is_error) {
      logger.error(result);
      logger.error((new Error()).stack);
      logger.error(JSON.stringify(result));
      throw new APIError(COMMON_ERRORS.COMMON.MAIL_ERROR);
    }
  }

  /**
   * @async
   * @param {('cancelled'|'status'|'completed'|'created')} name Название шаблона.
   * @param {Array.<Object>} recipients Получатели
   * @param {String} recipients.name Имя пользователя
   * @param {String} recipients.email Почта
   * @param {Object} variables Набор данных передаваемых в шаблон
   * @param {String} variables.serial Номер заявки
   * @param {Object.<Date>} variables.createAt Дата создания
   * @param {String} variables.description Описание или статус
   * @description Доступные шаблоны:
   * cancelled - 14824 - 'Смена статуса обращения "Отменен"'. description - Описание
   * status - 8256 - 'Статус запроса на обслуживание'. description - статус.
   * completed - 8252 - 'Запрос на обслуживание решен'. description - описание решения
   * created - 19476 - 'Запрос на обслуживание решен'. description - Модель (серийный номер)
   */
  ticket({ name, recipients, variables }) {
    const variablesSerial = variables?.serial || '';

    const collection = new Map([[
      'cancelled', {
        subject: `Запрос на обслуживание ${variablesSerial} отменен`,
        templateId: 14824,
      }], [
      'status', {
        subject: `Статус запроса на обслуживание ${variablesSerial} изменен`,
        templateId: 8256,
      }], [
      'completed', {
        subject: `Запрос на обслуживание ${variablesSerial} решен`,
        templateId: 8252,
      }], [
      'created', {
        subject: `Создан запрос на обслуживание ${variablesSerial}`,
        templateId: 19476,
      }],
    ]);

    const dataForTemplate = collection.get(name);
    const { subject } = dataForTemplate;
    const template = {
      id: dataForTemplate.templateId,
      variables: {
        ...variables,
        ...commonVariables,
      },
    };

    return this.sendMessage({ template, subject, recipients });
  }

  /**
   * @async
   * @param {('invoice'|'paid'|'issuedAndPaid'|'issuedAndNotPaid')} name Название шаблона.
   * @param {Array.<Object>} recipients Получатели
   * @param {String} recipients.name Имя пользователя
   * @param {String} recipients.email Почта
   * @param {Object} variables Набор данных передаваемых в шаблон
   * @param {Number} variables.orderNumber Номер заказа
   * @param {Object} [variables.data] Данные заказа
   * @param {Object} [variables.data.orderNumber] Номер заказа
   * @param {Object} [variables.data.totalCost] Общая сумма заказа
   * @param {Object} [variables.data.goods] Товары
   * @param {Object} [attachmentsBinary] Прикреплённый файл вида  {"название":"содержимое"}
   *                  содержание файла закодировано в base64
   * @description Доступные шаблоны:
   * 14823 - 'Счет на оплату'.
   * 14818 - 'Заказ оплачен'.
   * 14817 - 'Заказ оформлен и оплачен'. data - детали заказа
   * 8264 - 'Заказ оформлен и ожидает оплаты'. data - детали заказа
   */
  order({
    name, recipients, variables, attachmentsBinary,
  }) {
    const collection = new Map([[
      'invoice', {
        subject: 'Счет на оплату',
        templateId: 14823,
      }], [
      'paid', {
        subject: 'Заказ оплачен',
        templateId: 14818,
      }], [
      'issuedAndPaid', {
        subject: 'Заказ оформлен и оплачен',
        templateId: 14817,
      }], [
      'issuedAndNotPaid', {
        subject: 'Заказ оформлен и ожидает оплаты',
        templateId: 8264,
      }],
    ]);

    const dataForTemplate = collection.get(name);
    const { subject } = dataForTemplate;
    const template = {
      id: dataForTemplate.templateId,
      variables: {
        ...variables,
        ...commonVariables,
      },
    };

    return this.sendMessage({
      template, subject, recipients, attachmentsBinary,
    });
  }

  /**
   * @async
   * @param {('address'|'operator'|'local')} name Название шаблона.
   * @param {Array.<Object>} recipients Получатели
   * @param {String} recipients.name Имя пользователя
   * @param {String} recipients.email Почта
   * @param {Object} variables Набор данных передаваемых в шаблон
   * @param {Number} variables.orderNumber Номер заказа
   * @param {String} [variables.address] Адрес отправки документов
   * @param {Object} attachmentsBinary Прикреплённый файл вида  {"название":"содержимое"}
   *                  содержание файла закодировано в base64
   * @description Доступные шаблоны:
   * address - 14822 - 'Закрывающие документы'. address (Оригиналы закрывающих документов будут отправлены по адресу: )
   * operator - 14821 - 'Закрывающие документы'. (Оригиналы закрывающих документов будут отправлены через выбранного оператора электронного документооборота (ЭДО))
   * local - 14819 - 'Закрывающие документы'. (Универсальный передаточный документ (УПД) доступен в разделе "Документы" личного кабинета.)
   */
  document({
    name, recipients, variables, attachmentsBinary,
  }) {
    const collection = new Map([[
      'address', {
        subject: 'Закрывающие документы',
        templateId: 14822,
      }], [
      'operator', {
        subject: 'Закрывающие документы',
        templateId: 14821,
      }], [
      'local', {
        subject: 'Закрывающие документы',
        templateId: 14819,
      }],
    ]);

    const dataForTemplate = collection.get(name);
    const { subject } = dataForTemplate;
    const template = {
      id: dataForTemplate.templateId,
      variables: {
        ...variables,
        ...commonVariables,
      },
    };

    return this.sendMessage({
      template, subject, recipients, attachmentsBinary,
    });
  }

  /**
   * @async
   * @param {('login'|'templePassword'|'checkMail'|'resetPassword'|'newPassword')} name Название шаблона.
   * @param {Array.<Object>} recipients Получатели
   * @param {String} recipients.name Имя пользователя
   * @param {String} recipients.email Почта
   * @param {Object} variables Набор данных передаваемых в шаблон
   * @param {Number} [variables.email] email
   * @param {Number} [variables.templePassword] Временный пароль при регистрации через админку
   * @param {String} [variables.url] Ссылка для подтверждения почты
   * @param {String} [variables.resetPassword] Временный пароль при сбросе через клиентский интерфейс
   * @description Доступные шаблоны:
   * login - 8269 - 'Регистрация на сайте'. (Для регистрации через интерфейс)
   * templePassword - 8268 - 'Регистрация на сайте'. templePassword (Для регистрации администратором)
   * checkMail - 8266 - 'Проверка адреса электронной почты' url
   * resetPassword - 8238 - 'Запрос на сброс пароля' email, resetPassword
   * newPassword - 21865 - 'Пароль пользователя изменен' email
   * newEmail - 26540 - 'E-mail пользователя изменен'  email, phone
   */
  registration({ name, recipients, variables }) {
    const collection = new Map([[
      'login', {
        subject: 'Регистрация на сайте',
        templateId: 8269,
      }], [
      'templePassword', {
        subject: 'Регистрация на сайте',
        templateId: 8268,
      }], [
      'checkMail', {
        subject: 'Проверка адреса электронной почты',
        templateId: 8266,
      }], [
      'resetPassword', {
        subject: 'Запрос на сброс пароля',
        templateId: 8238,
      }], [
      'newPassword', {
        subject: 'Пароль пользователя изменен',
        templateId: 21865,
      }], [
      'newEmail', {
        subject: 'E-mail пользователя изменен',
        templateId: 26540,
      }],
    ]);

    const dataForTemplate = collection.get(name);
    const { subject } = dataForTemplate;
    const template = {
      id: dataForTemplate.templateId,
      variables: {
        ...variables,
        ...commonVariables,
      },
    };

    return this.sendMessage({ template, subject, recipients });
  }


  /**
   * @async
   * @param {('created'|'status'|'completed')} name Название шаблона.
   * @param {Array.<Object>} recipients Получатели.
   * @param {String} recipients.name Имя пользователя.
   * @param {String} recipients.email Почта.
   * @param {Object} variables Набор данных передаваемых в шаблон.
   * @param {String} variables.number Номер обращения.
   * @param {String} [variables.topic] Тема обращения.
   * @param {String} [variables.status] Статус обращения.
   * @param {String} [variables.solution] Описание решения.
   * @param {Object.<Date>} variables.createAt Дата создания.
   * @description Доступные шаблоны:
   * created - 21868 - 'Создание обращения "Прочее"'.
   * status - 21873 - 'Смена статуса обращения "Прочее" (кроме решено)'.
   * completed - 21874 - 'Смена статуса обращения "Прочее" "Решено"'.
   */
  supportRequest({ name, recipients, variables }) {
    const variablesNumber = variables?.number || '';

    const collection = new Map([[
      'created', {
        subject: `Создание обращения ${variablesNumber}`,
        templateId: 21868,
      }], [
      'status', {
        subject: `Статус обращения ${variablesNumber} изменен`,
        templateId: 21873,
      }], [
      'completed', {
        subject: `Обращение ${variablesNumber} решено`,
        templateId: 21874,
      }],
    ]);

    const dataForTemplate = collection.get(name);
    const { subject } = dataForTemplate;
    const template = {
      id: dataForTemplate.templateId,
      variables: {
        ...variables,
        ...commonVariables,
      },
    };

    return this.sendMessage({ template, subject, recipients });
  }
}

export default new MailClient();
