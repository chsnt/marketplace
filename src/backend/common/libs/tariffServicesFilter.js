import { TicketStatusModel } from '../database/models/Ticket';
import ServiceAddressAccountingTypeModel from '../database/models/Tariff/ServiceAddressAccountingTypeModel';
import DeviceTypeModel from '../database/models/Device/DeviceTypeModel';

const statusesInProgress = new Set([
  TicketStatusModel.STATUSES.NEW,
  TicketStatusModel.STATUSES.IN_PROGRESS,
  TicketStatusModel.STATUSES.UPDATE_INFORMATION,
]);

/**
 * @param {Array} clientTariffServices массив услуг тарифа
 * @param {Date} [expirationAt] дата завершения тарифа
 * @description форматирует и отдает те услуги, которые можно использовать
 * @returns {Array}
 */
export const tariffServicesFilter = ({ clientTariffServices, expirationAt }) => clientTariffServices.map(
  ({
    id: idClientTariffServices,
    value,
    tariffServiceRelation: {
      service: {
        ...data
      },
      limit,
      limitMeasure: { name: limitMeasure },
    },
    tickets,
  }) => {
    const ticketsInProgress = tickets.reduce((acc, ticket) => (
      (statusesInProgress.has(ticket.ticketStatus.id)) ? acc + 1 : acc),
    0);

    const ticketsUsed = tickets.reduce((acc, ticket) => (
      (TicketStatusModel.STATUSES.COMPLETED === ticket.ticketStatus.id) ? acc + 1 : acc),
    0);

    const newValue = (value === Infinity) ? value : value - (ticketsUsed + ticketsInProgress);

    return {
      id: idClientTariffServices,
      value: newValue,
      progress: ticketsInProgress,
      used: ticketsUsed,
      expirationAt,
      service: {
        ...data,
        serviceAddressType: ServiceAddressAccountingTypeModel.NAMES[data.serviceAddressAccountingTypeId],
        deviceType: DeviceTypeModel.NAMES[data.deviceTypeId],
        limit,
        limitMeasure,
      },
    };
  },
);
