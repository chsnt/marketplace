import multer from 'multer';
import toTranslit from 'cyrillic-to-translit-js';
import path from 'path';
import FileService from '../services/FileService';
import CustomDiskStorage from './CustomDiskStorage';

const DEFAULT_MAX_FILE_SIZE = 10 * 1024 * 1024; // 10 MB

export default class FileStorage {
  constructor({ storagePath, fileSize, isSecure = true }) {
    if (!storagePath) throw new Error('Path parameter is required');

    const storage = path.join(FileService.DEFAULT_FILE_PATH, storagePath);

    const storageOptions = {
      destination(req, file, cb) {
        return cb(null, storage);
      },
      filename(req, file, cb) {
        /* русскоязычные файлы переводим в транслит */
        const name = toTranslit().transform(file.originalname, '_');
        /* таймстемп нужен для защиты от одинаковых названий */
        /* регулярка, что бы вырезать таймстемп ^\-*(\w+)- , что бы убрать расширение \..+$ */
        return cb(null, `${Date.now()}-${name}`);
      },
    };

    const diskStorage = isSecure ? new CustomDiskStorage(storageOptions) : multer.diskStorage(storageOptions);

    FileService.createDirIfNotExists(storage);

    this.upload = multer({
      storage: diskStorage,
      limits: { fileSize: fileSize || DEFAULT_MAX_FILE_SIZE },
      preservePath: true,
    });
  }
}
