import clamd from 'clamdjs';

export class AntivirusScanner {
  #instance;

  constructor({ host, port }) {
    if (!this.#instance) {
      this.#instance = clamd.createScanner(host, port);
      /* Пробрасываем метод проверки ответа от сканера */
      this.#instance.isCleanReply = clamd.isCleanReply;
    }

    return this.#instance;
  }
}
