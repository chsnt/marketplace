import fs from 'fs';
import os from 'os';
import path from 'path';
import crypto from 'crypto';
import FileService from '../services/FileService';
import { ANTIVIRUS_HOST, ANTIVIRUS_PORT } from '../constants/env';
import { AntivirusScanner } from './AntivirusScanner';
import APIError from '../utils/APIError';
import { ANTIVIRUS } from '../constants';
import ERRORS from '../constants/errors';
import { Logger } from '../services/LoggerService';

const logger = new Logger('ANTIVIRUS');

const antivirusScanner = new AntivirusScanner({ host: ANTIVIRUS_HOST, port: ANTIVIRUS_PORT });

/* Кастомный диск сторейдж для мультера
Основан на базовом diskStorage. Добавлена проверка файлов антивирусом */
export default class CustomDiskStorage {
  constructor({ destination, filename } = {}) {
    this.getFilename = (filename || CustomDiskStorage.getFilename);

    if (typeof destination === 'string') {
      FileService.createDirIfNotExists(destination);
      this.getDestination = (...[,, cb]) => { cb(null, destination); };
    } else {
      this.getDestination = (destination || CustomDiskStorage.getDestination);
    }
  }

  /* Метод формирования имени файла по-умолчанию.
  Переопределяется при инициализации класса через свойство filename */
  static getFilename(req, file, cb) {
    crypto.randomBytes(16, (err, raw) => {
      cb(err, err ? undefined : raw.toString('hex'));
    });
  }

  /* Метод формирования пути по-умолчанию
  Переопределяется при инициализации класса через свойство destination */
  static getDestination(req, file, cb) {
    cb(null, os.tmpdir());
  }

  /* Метод обработки полученного файла. Реализован в соответствии с документацией multer */
  _handleFile(req, file, cb) {
    const that = this;

    that.getDestination(req, file, async (destinationError, destination) => {
      if (destinationError) return cb(destinationError);

      that.getFilename(req, file, async (err, filename) => {
        if (err) return cb(err);


        const finalPath = path.join(destination, filename);
        const outStream = fs.createWriteStream(finalPath);

        file.stream.pipe(outStream);
        outStream.on('error', cb);
        outStream.on('finish', async () => {
          /* Сканируем файл на наличие вирусов */
          try {
            const result = await antivirusScanner.scanFile(finalPath, ANTIVIRUS.TIMEOUT, ANTIVIRUS.CHUNK_SIZE);

            if (!antivirusScanner.isCleanReply(result)) {
              throw new APIError(ERRORS.ANTIVIRUS.VIRUS_FOUND(filename));
            }

            cb(null, {
              destination,
              filename,
              path: finalPath,
              size: outStream.bytesWritten,
            });
          } catch (scanError) {
            if (!(scanError instanceof APIError)) {
              logger.error(scanError);
              cb(new APIError(ERRORS.FILE.FILE_UPLOAD_ERROR), null);
            }
            /* В случае наличия вируса удаляем файл */
            FileService.removeFile(finalPath);
            cb(scanError, null);
          }
        });
      });
    });
  }

  /* Метод удаления файла. Реализован в соответствии с документацией multer */
  _removeFile = (req, file, cb) => {
    const { path: filePath } = file;

    FileService.removeFile(filePath);

    cb();
  }
}
