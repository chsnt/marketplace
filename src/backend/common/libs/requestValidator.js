import validator from 'express-joi-validation';

export default validator.createValidator({ passError: true });
