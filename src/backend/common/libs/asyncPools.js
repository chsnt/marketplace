import asyncPool from 'tiny-async-pool';

import { ASYNC_POOL_CONCURRENCY } from '../constants/env';

export default (array, iteratorFn = (promise) => promise) => (
  array.length < ASYNC_POOL_CONCURRENCY
    ? Promise.all(array.map(iteratorFn))
    : asyncPool(ASYNC_POOL_CONCURRENCY, array, iteratorFn)
);
