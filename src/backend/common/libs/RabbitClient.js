/* eslint-disable class-methods-use-this */
import amqp from 'amqplib';
import APIError from '../utils/APIError';
import ERRORS from '../constants/errors';
import is from './is';

import { Logger } from '../services/LoggerService';

const logger = new Logger('RABBIT MQ CLIENT');

let channel;
const url = process.env.AMQP_URL;
const QUEUES = {
  sms: 'send-sms',
  integrationCreateOrder: 'integration-create-order',
  integrationChangedBankDetails: 'integration-changed-bank-details',
};

const createChannel = async () => {
  if (channel || !url) return;
  try {
    const connection = await amqp.connect(`${url}?heartbeat=60`);

    connection.on('error', logger.error);

    ['SIGINT', 'SIGTERM'].forEach((sig) => {
      process.on(sig, async () => { await connection.close(); process.exit(); });
    });

    channel = await connection.createChannel();

    await Promise.all(Object.values(QUEUES).map((queue) => channel.assertQueue(queue)));

    logger.info(`Connected to RabbitMQ broker (${url})`);
  } catch (error) {
    logger.error(`Cannot connect to RabbitMQ broker (${url})`);
    channel = null;
  }
};

export default class {
  constructor() {
    this.queues = QUEUES;
  }

  async sendToQueue({
    queue, message, headers, properties,
  }) {
    await createChannel();
    if (!channel) {
      /* Для локальной разработки отключим отправку, если нет канала */
      if (is.env.development()) {
        logger.warn(ERRORS.RABBITMQ.SMS_SERVICE_NOT_ALLOWED.error);
        logger.verbose(JSON.stringify(message));
        return;
      }
      throw new APIError(ERRORS.RABBITMQ.SMS_SERVICE_NOT_ALLOWED);
    }

    const space = is.env.production() ? undefined : '\t';
    return channel.sendToQueue(queue, Buffer.from(JSON.stringify(message, null, space)), {
      persistent: true,
      headers,
      ...properties,
    });
  }

  async consume(...pars) {
    await createChannel();
    if (!channel) throw new Error(`Канал ${pars[0]} не создан`);
    return channel.consume(...pars);
  }

  async ack(...pars) {
    await createChannel();
    if (!channel) throw new Error(`Канал ${pars[0]} не создан`);
    return channel.ack(...pars);
  }

  async nack(...pars) {
    await createChannel();
    if (!channel) throw new Error(`Канал ${pars[0]} не создан`);
    return channel.nack(...pars);
  }
}
