import { TicketStatusModel } from '../database/models/Ticket';
import { SupportRequestTopicModel } from '../database/models/SupportRequest';
import { UserGroupModel } from '../database/models/User';
import { DeviceTypeModel } from '../database/models/Device';
import { ElectronicDocumentCirculationModel } from '../database/models/Edo';
import DiscountTypeModel from '../database/models/Discount/DiscountTypeModel';
import { OrderStatusModel } from '../database/models/Order';
import { ServiceAddressAccountingTypeModel, AvailablePaymentMethodModel } from '../database/models/Tariff';
import { IS_PAYMENTS_ENABLED } from '../constants/env';
import PaymentTypeModel from '../database/models/Order/PaymentTypeModel';

/* набор функций предикатов */
const is = {
  env: {
    development: () => process.env.NODE_ENV === 'development',
    test: () => process.env.NODE_ENV === 'test',
    production: () => process.env.NODE_ENV === 'production',
  },
  ticket: {
    status: {
      new: (statusId) => TicketStatusModel.STATUSES.NEW === statusId,
      inProgress: (statusId) => TicketStatusModel.STATUSES.IN_PROGRESS === statusId,
      updateInformation: (statusId) => TicketStatusModel.STATUSES.UPDATE_INFORMATION === statusId,
      coordinationOfDeparture: (statusId) => TicketStatusModel.STATUSES.COORDINATION_OF_DEPARTURE === statusId,
      engineerVisit: (statusId) => TicketStatusModel.STATUSES.ENGINEER_VISIT === statusId,
      completed: (statusId) => TicketStatusModel.STATUSES.COMPLETED === statusId,
      cancelled: (statusId) => TicketStatusModel.STATUSES.CANCELLED === statusId,
    },
  },
  supportRequest: {
    topic: {
      workingWithSite: (topicId) => SupportRequestTopicModel.TOPICS.WORKING_WITH_SITE === topicId,
      guaranteeAndRefund: (topicId) => SupportRequestTopicModel.TOPICS.GUARANTEE_AND_REFUND === topicId,
      documentsAndLegal: (topicId) => SupportRequestTopicModel.TOPICS.DOCUMENTS_AND_LEGAL === topicId,
      etc: (topicId) => SupportRequestTopicModel.TOPICS.ETC === topicId,
    },
  },
  user: {
    group: {
      admin: (groupId) => UserGroupModel.GROUPS.ADMIN === groupId,
      salesManager: (groupId) => UserGroupModel.GROUPS.SALES_MANAGER === groupId,
      projectCommittee: (groupId) => UserGroupModel.GROUPS.PROJECT_COMMITTEE === groupId,
      technicalSupport: (groupId) => UserGroupModel.GROUPS.TECHNICAL_SUPPORT === groupId,
      operator: (groupId) => UserGroupModel.GROUPS.OPERATOR === groupId,
    },
  },
  device: {
    type: {
      arm: (deviceTypeId) => DeviceTypeModel.TYPES.ARM === deviceTypeId,
      kkt: (deviceTypeId) => DeviceTypeModel.TYPES.KKT === deviceTypeId,
    },
  },
  electronicDocumentCirculation: {
    notInList: (edcId) => ElectronicDocumentCirculationModel.STATUSES.NOT_IN_LIST === edcId,
    isMissing: (edcId) => ElectronicDocumentCirculationModel.STATUSES.EDO_IS_MISSING === edcId,
  },
  discount: {
    type: {
      discount: (typeId) => DiscountTypeModel.TYPES.DISCOUNT === typeId,
      fixedPrice: (typeId) => DiscountTypeModel.TYPES.FIXED_PRICE === typeId,
    },
  },
  service: {
    serviceAddressAccountingType: {
      inOrder: (typeId) => ServiceAddressAccountingTypeModel.TYPES.IN_ORDER === typeId,
      inTicket: (typeId) => ServiceAddressAccountingTypeModel.TYPES.IN_TICKET === typeId,
      notRequested: (typeId) => ServiceAddressAccountingTypeModel.TYPES.NOT_REQUESTED === typeId,
    },
    availablePaymentMethod: {
      all: (methodId) => AvailablePaymentMethodModel.STATUSES.ALL === methodId,
      card: (methodId) => AvailablePaymentMethodModel.STATUSES.CARD === methodId,
      invoice: (methodId) => AvailablePaymentMethodModel.STATUSES.INVOICE === methodId,
    },
  },
  payments: {
    enabled: IS_PAYMENTS_ENABLED,
    types: {
      id: {
        card: (type) => type === PaymentTypeModel.TYPES.CARD.id,
        invoice: (type) => type === PaymentTypeModel.TYPES.INVOICE.id,
      },
      alias: {
        card: (type) => type === PaymentTypeModel.TYPES.CARD.alias,
        invoice: (type) => type === PaymentTypeModel.TYPES.INVOICE.alias,
      },
    },
  },
  client: {
    type: {
      organization: (type) => type === 'organization',
      individual: (type) => type === 'individual',
    },
  },
  order: {
    status: {
      cart: (status) => status === OrderStatusModel.STATUSES.CART,
      paid: (status) => status === OrderStatusModel.STATUSES.PAID,
      notPaid: (status) => status === OrderStatusModel.STATUSES.NOT_PAID,
    },
  },
};

export default is;
