import RabbitClient from '../common/libs/RabbitClient';
import IntegrationController from '../common/controllers/IntegrationController';
import { Logger } from '../common/services/LoggerService';

const logger = new Logger('INTEGRATION');

const client = new RabbitClient();

const startConsuming = () => {
  client.consume(client.queues.integrationCreateOrder, async (message) => {
    try {
      await IntegrationController.createOrder(JSON.parse(message.content));
      await client.ack(message);
    } catch (error) {
      logger.error(error);
    }
  });

  client.consume(client.queues.integrationChangedBankDetails, async (message) => {
    try {
      await IntegrationController.createOrUpdateBankAccount(JSON.parse(message.content));
      await client.ack(message);
    } catch (error) {
      logger.error(error);
    }
  });
};

startConsuming();
