module.exports = {
  testRegex: '((\\.|/*.)(spec))\\.js?$',
  verbose: true,
  moduleDirectories: [
    'node_modules',
  ],
  setupFilesAfterEnv: ['../setupFilesAfterEnv.js'],
};
