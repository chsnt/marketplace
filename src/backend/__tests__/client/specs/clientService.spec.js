import request from 'supertest';
import app from '../../../client/server';

import { destroySession, getUserSession } from '../../helper/auth';

const server = request.agent(app);

describe('CLIENT_SERVICE: /api/client/allowed-services', () => {
  let session = '';
  beforeAll(async () => {
    const sessionData = await getUserSession({
      emailOrPhone: 'user-1@auto-test.com',
      password: '12Zz3456',
      server,
    });

    session = sessionData.session;
  });

  afterAll(async () => {
    await destroySession(server);
  });

  it('get all allowed services', async () => {
    const res = await server
      .get('/api/client/allowed-services')
      .set('Cookie', session)
      .send({});

    expect(res.statusCode).toEqual(200);
    expect(res.body.data).toHaveLength(5);

    res.body.data.forEach((service) => {
      expect(service).toHaveProperty('services');
      expect(service).toHaveProperty('serviceAddressType');
      expect(service).toHaveProperty('serviceAddress');
    });
  });
});
