import request from 'supertest';
import app from '../../../admin/server';

import { getUserSession, destroySession } from '../../helper/auth';
import { getUserInfo } from '../../helper/user';

const server = request.agent(app);

describe('GET-INFO', () => {
    let session = ``;
    let clientId = '';
    let orderId = '';
    let serviceAddressId = '';

    beforeAll(async () => {
        const sessionData = await getUserSession({
            emailOrPhone: 'admin@auto-test.com',
            password: '12Zz3456',
            server,
        });
        const userInfo = await getUserInfo({ email: 'user-1@auto-test.com' });

        session = sessionData.session;
        clientId = userInfo.organization.clientId;
        serviceAddressId = userInfo.organization.client.clientDevices[0].serviceAddressId;
    });

    afterAll(async () => {
        await destroySession(server);
    });

    it('Create order', async () => {
        const res = await server
            .post('/api/order/create')
            .set('Cookie', session)
            .send({
                clientId,
                tariffs: [{
                    id: '745f43f1-3ed0-42ab-ac1c-fc10f2f7e622',
                    serviceAddressId,
                }],
                services: [{
                    id: 'c4a8f948-a236-4cfd-a818-fdb48ad45785',
                    serviceAddressId,
                },
                    {
                        id: 'c4a8f948-a236-4cfd-a818-fdb48ad45785',
                        serviceAddressId,
                    },
                    {
                        id: '8e1fa111-08fe-4d89-96e8-fb9acbaad0a6',
                    },
                    {
                        id: '499d9ae4-0b11-422f-87e0-95e6d28bcb84',
                    }],
            });

        orderId = res.body.data;

        expect(res.statusCode).toEqual(200);
    });


    it('Get info', async () => {
        const res = await server
            .get(`/api/order/${orderId}/get-info`)
            .set('Cookie', session)
            .send();

        expect(res.statusCode).toEqual(200);
    });

    it('Get order list', async () => {
        const res = await server
            .get(`/api/order/get-list?clientId=${clientId}`)
            .set('Cookie', session)
            .send();

        expect(res.statusCode).toEqual(200);
        expect(res.body.data.count).toEqual(4);
        expect(res.body.data.rows).toHaveLength(4);
    });
});
