import { UserModel } from '../../common/database/models/User';

export const registerAdmin = async ({ transaction }) => {
  const newAdmin = await UserModel.registerUser({
    user: {
      firstName: 'Админ-Автотест',
      lastName: 'Админ-Тестович',
      password: '12Zz3456',
      email: 'admin@auto-test.com',
      phone: '79990007700',
      userGroupRelations: {
        userGroupId: '45f54fd5-28af-40ec-8c4c-a516cbbeb2dd',
      },
    },
    transaction,
  });
  return newAdmin.id;
};

export const destroyUser = async ({ id, transaction }) => {
  const usersInstance = await UserModel.findByPk(id, { transaction });

  if (!usersInstance) return;

  await usersInstance.destroy({
    force: true,
    transaction,
  });
};

export const getUserInfo = async ({ userId, email, transaction }) => {
  const userInstance = await UserModel.findOne({
    where: {
      ...(userId && { id: userId }),
      ...(email && { email }),
    },
    include: [{
      association: 'client',
      include: 'clientDevices',
    },
    {
      association: 'organization',
      include: [
        {
          association: 'legalAddress',
        },
        {
          association: 'client',
          include: 'clientDevices',
        },
      ],
    }],
    transaction,
  });
  return userInstance;
};
