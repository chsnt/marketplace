import { OrganizationModel } from '../../common/database/models/Client/Organization';

export const createOrganization = async ({ clientId, transaction }) => {
  const clientOrganization = await OrganizationModel.registerOrganization({
    organization: {
      name: '-=Test=- Лапландия-удалить',
      inn: '5551285111',
      kpp: '810664111',
      electronicDocumentCirculationId: 'e6514477-fcb0-42a4-a411-1d956cca7bd0',
      digitalSignature: {
        from: '1257-12-08',
        to: '2157-12-08',
      },
      isSameAddress: true,
      legalAddress: {
        federationSubjectId: 'b281935e-8569-4052-80a1-0d8cdf0dd46d',
        cityId: '1b5ecf58-fa72-4ff6-bfdb-7735c7badfde',
        street: '-=Test=- Исакиевская ул.',
        house: '33',
        apartment: '99',
        postalCode: '143814',
      },
      bankDetail: {
        name: '-=Test=- Альфа',
        bankAccount: '96342178985566547111',
        correspondentAccount: '20006543185980054111',
        rcbic: '147333111',
        taxTypeId: 'ad24d66f-eda3-4844-a1de-1c6d055bb4b8',
      },
      clientId,
    },
    transaction,
  });

  return {
    id: clientOrganization.id,
    serviceAddressId: clientOrganization.legalAddress.id,
  };
};

export const destroyOrganization = async ({ id, transaction }) => {
  const organizationInstance = await OrganizationModel.findByPk(id, { transaction });

  if (!organizationInstance) throw new Error('Где организация \'Автотест\'!?');

  await organizationInstance.destroy({
    force: true,
    transaction,
  });
};
