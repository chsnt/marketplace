export const getUserSession = async ({ emailOrPhone, password, server }) => {
  const res = await server
    .post('/api/auth/login')
    .send({ emailOrPhone, password });

  expect(res.status).toEqual(200);

  return {
    session: res.headers['set-cookie'][0],
    userId: res.body.data.id,
  };
};

export const destroySession = async (server) => {
  const res = await server
    .get('/api/auth/logout')
    .send({});

  expect(res.status).toEqual(200);
};
