import { ClientDeviceModel, ClientDeviceParameterModel, ClientModel } from '../../common/database/models/Client';
import organization from './data/organization.json';
import ClientService from '../../client/services/ClientService';

export const createClient = async (transaction) => {
  const clientInstance = await ClientModel.create(organization, {
    include: [
      {
        association: 'organization',
        include: [
          'users',
          'legalAddress',
          'bankDetail',
        ],
      },
      {
        association: 'clientDevices',
        include: 'serviceAddress',
      },
    ],
    returning: true,
    transaction,
  });
  return clientInstance;
};

export const destroyClient = async ({ clientId, transaction }) => {
  await ClientService.deleteClient({ clientId, transaction });
};

export const createClientDeviceParameter = async ({ clientDeviceId, deviceParameterId, transaction }) => {
  const clientDeviceParameterInstance = await ClientDeviceParameterModel.create({
    clientDeviceId,
    deviceParameterId,
    value: '4937345-943',
  }, { transaction });

  return clientDeviceParameterInstance.id;
};

export const destroyClientDeviceParameter = async ({ clientDeviceParameterId, transaction }) => {
  const clientDeviceParameterInstance = await ClientDeviceParameterModel.findByPk(
    clientDeviceParameterId,
    { transaction },
  );

  if (!clientDeviceParameterInstance) throw new Error('Где clientDeviceParameter \'Автотест\'!?');

  await clientDeviceParameterInstance.destroy({
    force: true,
    transaction,
  });
};

export const createClientDevice = async ({ serviceAddressId, clientId, transaction }) => {
  const clientDeviceInstance = await ClientDeviceModel.create({
    deviceModelId: 'e06d062f-54dc-4b37-baaf-9aeed9b20d0f',
    serviceAddressId,
    clientId,
  }, { transaction });

  return clientDeviceInstance.id;
};

export const destroyClientDevice = async ({ clientDeviceId, transaction }) => {
  const clientDeviceInstance = await ClientDeviceModel.findByPk(
    clientDeviceId,
    { transaction },
  );

  if (!clientDeviceInstance) throw new Error('Где  clientDevice \'Автотест\'!?');

  await clientDeviceInstance.destroy({
    force: true,
    transaction,
  });
};
