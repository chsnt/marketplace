import { ServiceModel, TariffModel } from '../../common/database/models/Tariff';
import dataServices from './data/services.json';
import dataTariffs from './data/tariffs.json';

export const createServices = async (transaction) => {
  const serviceInstances = await ServiceModel.bulkCreate(dataServices, {
    include: ['costs'],
    returning: false,
    transaction,
  });

  return serviceInstances.map((serviceInstance) => serviceInstance.id);
};

export const destroyServices = async ({ services, transaction }) => {
  const serviceInstances = await ServiceModel.findAll(
    {
      where: {
        id: services,
      },
      include: 'costs',
      attributes: { exclude: ['service_id'] },
      transaction,
    },
  );

  if (!serviceInstances) return;

  await Promise.all(serviceInstances.map((serviceInstance) => serviceInstance.destroy({
    force: true,
    transaction,
  })));
};

export const createTariff = async (transaction) => {
  const tariffInstances = await TariffModel.bulkCreate(dataTariffs, {
    include: ['costs', 'tariffServiceRelations'],
    returning: false,
    transaction,
  });

  return tariffInstances.map((tariffInstance) => tariffInstance.id);
};

export const destroyTariffs = async ({ tariffs, transaction }) => {
  const tariffInstances = await TariffModel.findAll(
    {
      where: {
        id: tariffs,
      },
      include: ['costs', 'tariffServiceRelations'],
      attributes: { exclude: ['tariff_id'] },
      transaction,
    },
  );

  if (!tariffInstances) return;

  await Promise.all(tariffInstances.map((tariffInstance) => tariffInstance.destroy({
    force: true,
    transaction,
  })));
};
