import moment from 'moment';
import OrderService from '../../admin/services/OrderService';
import { DATE_FORMAT } from '../../common/constants';
import { OrderModel, OrderOfferDocumentModel } from '../../common/database/models/Order';
import order from './data/oreder.json';


export const createOrder = async ({
  clientId, serviceAddressId, offerDocumentId, transaction,
}) => {
  const orderInstance = await OrderModel.create({
    ...order,
    clientId,
    clientServices: order.clientServices.map((clientService) => ({
      ...clientService,
      ...('serviceAddressId' in clientService && { serviceAddressId }),
      clientId,
    })),
    clientTariffs: order.clientTariffs.map((clientTariff) => ({
      ...clientTariff,
      ...('serviceAddressId' in clientTariff && { serviceAddressId }),
      clientId,
    })),
    orderOfferDocumentId: offerDocumentId,
  }, {
    include: [
      {
        association: 'payments',
      },
      {
        association: 'clientServices',
      },
      {
        association: 'clientTariffs',
        include: 'clientTariffServices',
      },
    ],
    returning: true,
    transaction,
  });

  return orderInstance;
};

export const changeOrderStatus = async ({ orderId, transaction }) => {
  const paymentDate = moment().format(DATE_FORMAT.KEBAB_FULL);
  await OrderService.updateOrder({ id: orderId, paymentDate, transaction });
};

export const createOfferDocument = async ({ transaction }) => {
  const offerDocumentInstance = await OrderOfferDocumentModel.create({
    name: '-=Test=- Публичная оферта на приобретение услуг абонентского сервисного обслуживания.pdf',
    alias: '-=Test=-publichnaya-oferta-na-priobretenie-uslug-abonentskogo-servisnogo-obsluzhivaniya.pdf',
  }, {
    returning: true,
    transaction,
  });

  return offerDocumentInstance.id;
};

export const destroyOfferDocument = async ({ offerDocumentId, transaction }) => {
  const offerDocumentInstance = await OrderOfferDocumentModel.findByPk(offerDocumentId, { transaction });

  if (!offerDocumentInstance) throw new Error('Где офер \'Автотест\'!?');

  await offerDocumentInstance.destroy({
    force: true,
    transaction,
  });
};
