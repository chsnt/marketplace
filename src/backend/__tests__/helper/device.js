import { DeviceParameterModel } from '../../common/database/models/Device';

export const createDeviceParameter = async (transaction) => {
  const deviceParameterInstance = await DeviceParameterModel.create({
    deviceModelId: 'e06d062f-54dc-4b37-baaf-9aeed9b20d0f',
    fieldTypeId: 'cd435b53-cc70-469d-aac1-f0425753ef06',
    name: '-=Test=- Серийный номер',
    alias: 'SERIAL_NUMBER',
    isRequired: true,
  }, { transaction });

  return deviceParameterInstance.id;
};

export const destroyDeviceParameter = async ({ deviceParameterId, transaction }) => {
  const deviceParameterInstance = await DeviceParameterModel.findByPk(deviceParameterId, { transaction });

  if (!deviceParameterInstance) throw new Error('Где deviceParameter \'Автотест\'!?');

  await deviceParameterInstance.destroy({
    force: true,
    transaction,
  });
};
