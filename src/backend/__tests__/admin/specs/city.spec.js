import request from 'supertest';
import app from '../../../admin/server';

import {
  getUserSession, destroySession,
} from '../../helper/auth';

const server = request.agent(app);

describe('CITY', () => {
  let session = null;

  beforeAll(async () => {
    const sessionData = await getUserSession({
      emailOrPhone: 'admin@auto-test.com',
      password: '12Zz3456',
      server,
    });
    session = sessionData.session;
  });

  afterAll(async () => {
    await destroySession(server);
  });

  it('Get list', async () => {
    const res = await server
      .get('/api/city/get-list')
      .set('Cookie', session)
      .send({});

    expect(res.statusCode).toEqual(200);
  });
});
