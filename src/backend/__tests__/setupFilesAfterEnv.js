import db from '../common/database';

import {
  createClient, destroyClient,
} from './helper/client';
import { registerAdmin, destroyUser } from './helper/user';
import {
  createServices, createTariff, destroyServices, destroyTariffs,
} from './helper/tariffAndServices';
import { createOrder, createOfferDocument, destroyOfferDocument } from './helper/order';
import logger from '../common/services/LoggerService';

let clientId = '';
let adminId = '';
let offerDocumentId = '';
let tariffs = [];
let services = [];

beforeAll(async () => {
  const transaction = await db.sequelize.transaction();

  try {
    const client = await createClient(transaction);
    services = await createServices(transaction);
    tariffs = await createTariff(transaction);
    adminId = await registerAdmin({ transaction });
    clientId = client.id;
    offerDocumentId = await createOfferDocument({ transaction });
    await createOrder({
      clientId, serviceAddressId: client.clientDevices[0].serviceAddress.id, offerDocumentId, transaction,
    });
    await createOrder({
      clientId, serviceAddressId: client.clientDevices[1].serviceAddress.id, offerDocumentId, transaction,
    });
    await createOrder({
      clientId, serviceAddressId: client.clientDevices[2].serviceAddress.id, offerDocumentId, transaction,
    });
    await transaction.commit();
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error(error);
    logger.error('SETUP_FILES_AFTER_ENV: beforeAll: ', error);
    await transaction.rollback();
    throw error;
  }
});

afterAll(async () => {
  const transaction = await db.sequelize.transaction();
  try {
    await destroyTariffs({ tariffs, transaction });
    await destroyServices({ services, transaction });
    await destroyClient({ clientId, transaction });
    await destroyUser({ id: adminId, transaction });
    await destroyOfferDocument({ offerDocumentId, transaction });
    await transaction.commit();
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error(error);
    logger.error('SETUP_FILES_AFTER_ENV: afterAll: ', error);
    await transaction.rollback();
    throw error;
  }
});
