/* Процесс, в котором запускаются все cron-задачи, вынесен из процесса API */
import { CronJob } from 'cron';
import moment from 'moment';

import FileService from '../common/services/FileService';
import logger from '../common/services/LoggerService';

const runCommands = async () => {
  try {
    const commands = FileService
      .getFiles(__dirname)
      .filter((item) => !item.includes('.map') && item !== 'logs'
        && item.endsWith('.js') && !item.endsWith('index.js'));

    commands.forEach((file) => {
      const command = require(file.replace(/\.js$/g, '')); // eslint-disable-line

      /* Не выполняем задачу, если для нее установлено ограничение по окружению */
      if (command.excludeEnv && command.excludeEnv.includes(process.env.NODE_ENV)) return;

      const job = new CronJob(command.schedule, command.task);
      logger.verbose(`${file} -> ${moment(job.nextDates()).format('DD.MM.YYYY HH:mm:ss')}`);
      job.start();
    });
  } catch (error) {
    logger.error(error);
  }
};

runCommands();
