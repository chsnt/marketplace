import { Op } from 'sequelize';

import is from '../../common/libs/is';
import logger from '../../common/services/LoggerService';
import OrderStatusModel from '../../common/database/models/Order/OrderStatusModel';
import OrderModel from '../../common/database/models/Order/OrderModel';
import OrderService from '../../admin/services/OrderService';
import OrderController from '../../common/controllers/OrderController';

module.exports = {
  async task() {
    try {
      const orders = await OrderModel.findAll({
        where: { orderStatusId: OrderStatusModel.STATUSES.NOT_PAID },
        attributes: ['id', 'orderStatusId'],
        include: {
          association: 'payments',
          attributes: ['id', 'paymentOrderId', 'createdAt'],
          where: {
            paymentDate: {
              [Op.is]: null,
            },
            paymentOrderId: {
              [Op.not]: null,
            },
          },
          separate: true,
          limit: 1,
          order: [['createdAt', 'DESC']],
        },
      });

      const filteredOrders = orders.filter(({ payments }) => payments.length > 0);

      if (!filteredOrders.length) return;

      await Promise.all(filteredOrders.map(async (order) => {
        const statusId = await OrderService.requestStatus({
          order,
          paymentOrderId: order.payments[0].paymentOrderId,
        });
        if (is.order.status.paid(statusId)) {
          await OrderController.completeOrder(order.payments[0].paymentOrderId);
        }
      }));
    } catch (error) {
      logger.error(error);
    }
  },
  schedule: '*/5 * * * *',
};
