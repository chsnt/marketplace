const merge = require('webpack-merge');
const common = require('./webpack.config.common.js');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'source-map',
  output: {
    filename: 'admin.bundle.js',
    publicPath: 'http://127.0.0.1:3014/',
  },
  devServer: {
    historyApiFallback: true,
    port: 3014,
    host: '127.0.0.1',
    hot: true,
    liveReload: true,
    watchOptions: {
      aggregateTimeout: 500,
      poll: 1000,
    },
    headers: { 'Access-Control-Allow-Origin': '*' },
  },
});
