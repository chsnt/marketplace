const withImages = require('next-images');
const withFonts = require('next-fonts');
const withLess = require('@zeit/next-less');
const withSass = require('@zeit/next-sass');
const path = require('path');

// fix: prevents error when .less files are required by node
if (typeof require !== 'undefined') {
  require.extensions['.less'] = file => {};
}

module.exports = withImages(withFonts(withLess(withSass({
  env: {
    environment: process.env.NODE_ENV,
    GTM_VERIFICATION: process.env.GTM_VERIFICATION,
    YANDEX_VERIFICATION: process.env.YANDEX_VERIFICATION,
    GOOGLE_SITE_VERIFICATION: process.env.GOOGLE_SITE_VERIFICATION,
  },
  webpack: (config, { isServer }) => {
    config.module.rules.push({
      test: /\.(ttf|eot|woff|woff2)$/,
      use: [{ loader: 'url-loader' }],
    });
    if (isServer) {
      const antStyles = /antd\/.*?\/style.*?/;
      const origExternals = [...config.externals];
      config.externals = [
        (context, request, callback) => {
          if (request.match(antStyles)) return callback();
          if (typeof origExternals[0] === 'function') {
            origExternals[0](context, request, callback);
          } else {
            callback();
          }
        },
        ...(typeof origExternals[0] === 'function' ? [] : origExternals),
      ];
      config.module.rules.unshift({
        test: antStyles,
        use: 'null-loader',
      });
    }
    return config;
  },
  sassOptions: {
    outputStyle: 'expanded',
    includePaths: [path.join(__dirname, 'styles')],
  },
  trailingSlash: true,
  lessLoaderOptions: {
    javascriptEnabled: true,
  },
}))));
