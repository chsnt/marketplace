#!/bin/sh

now=$(date +"%d-%m-%Y_%H-%M")
pg_dump -U postgres -d marketplace -a -F c -T request_logs -T system_logs  > "/backups/marketplace_db_$now.bak"

# remove all files (type f) modified longer than 30 days ago under /db_backups/backups
find /backups -name "*.bak" -type f -mtime +3 -delete

exit 0
