#!/bin/sh
# ВНИМАНИЕ! Для запуска скрипта введите команду "bash db_backup_job.sh add"
croncmd_backup="/bin/docker exec marketplace_db_1 bash -c '/usr/src/db_backup.sh'"
cronjob_backup="0 3 * * * $croncmd_backup"

if [ $# -eq 0 ]; then
    echo "Введите один из двух аргументов (add/remove) для постановки/снятия задачи"

elif [ "$1" == "add" ]; then
    ( crontab -l | grep -v -F "$croncmd_backup" ; echo "$cronjob_backup" ) | crontab -
    echo "==>>> Задача создания резервных копий добавлена локальное cron-расписание"

elif [ "$1" == "remove" ]; then
    ( crontab -l | grep -v -F "$croncmd_backup" ) | crontab -
    echo "==>>> Задача создания резервных копий удалена из локлаьного расписания cron"
fi
